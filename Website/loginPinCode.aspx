﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginPinCode.aspx.cs" Inherits="imq.kpei.loginPinCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="Styles/login/login-box.css" rel="stylesheet" type="text/css" />    
    <title>Sistem Kliring Derivatif</title>
    <meta  http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 100px 0 0 250px; height: 175px;">
        <div id="loginPinCode-box">
            <table cellpadding="0" cellspacing="2" border="0" width="100%" style="height: 241px">
                <tr><td colspan="2" class="title"><img src="Styles/login/images/keys.png" /><h2>Sistem Kliring Derivatif</h2><br /><br /></td></tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">User :</label></td>
                    <td><input id="txtUser" runat="server" name="q" class="form-txt" title="Username" value="" maxlength="2048" readonly="readonly" /></td>
                </tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">Enter PinCode :</label></td>
                    <td><input id="txtPinCode" runat="server" name="q" class="form-txt" title="Member" value="" type=password maxlength="2048" /></td>
                </tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">Re-enter PinCode :</label></td>
                    <td><input id="txtPinCodeRe" runat="server" name="q" class="form-txt" title="Member" value="" type=password maxlength="2048" /></td>
                </tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">New Password :</label></td>
                    <td><input id="txtPassword" runat="server" name="q" class="form-txt" title="Member" value="" type=password maxlength="2048" /></td>
                </tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">Confirm Password :</label></td>
                    <td><input id="txtPasswordConfrm" runat="server" name="q" class="form-txt" title="Member" value="" type=password maxlength="2048" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="btnLogin" runat="server" OnClick="btnLogin_Click" class="login_button_pin"><img src="Styles/login/images/login_btn.png" /></asp:LinkButton>
                        <asp:LinkButton ID="btnKeyFile" runat="server" OnClick="btnKeyFile_Click" class="login_button_pin"><img src="Styles/login/images/get-keyfile.png" /></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
