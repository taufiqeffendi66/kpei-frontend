﻿<%@ Page Title="Tick Size" Language="C#" AutoEventWireup="true" CodeBehind="TickSize.aspx.cs" Inherits="imq.kpei.cr_ssf.TickSize" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        function clearEditors(s, e) {
            var container = document.getElementsByClassName("clientContainer")[0];
            ASPxClientEdit.ClearEditorsInContainer(container);
        }

        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'tickSizeName;price;tickSize;', OnGetRowValues);
        }

        function OnGetRowValues(values) {
            hfTS.Set("tickSizeName", values[0]);
            hfTS.Set("price", values[1]);
            hfTS.Set("tickSize", values[2]);
        }

        function ConfirmDelete(s, e) {
            e.processOnServer = confirm("Are you sure delete this Tick Size \nTick Size Name = " + hfTS.Get("tickSizeName"));
        }
    </script>
    <dx:ASPxHiddenField ID="hfTickSize" runat="server" ClientInstanceName="hfTS"></dx:ASPxHiddenField>
    <div class="content">
    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Tick Size"  />
        </div>
    </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID">
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNamaTickSize" runat="server" Text="Tick Size Name" Width="90px" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxNamaTickSize" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" Width="30px" AutoPostBack="false" onclick="btnSearch_Click" />
                    </td>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>
                        <dx:ASPxButton ID="btnReset" runat="server" Text="Reset" Width="30px" AutoPostBack="true" OnClick="btnReset_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br /><br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNew" Width="150px" EncodeHtml="false" runat="server" Text="<a href='TickSizeNew.aspx'>+ Add New Tick Size</a>"></dx:ASPxLabel>
                    </td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>
                        <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" Width="100px" AutoPostBack="false" 
                            onclick="btnEdit_Click" ClientInstanceName="btned" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" Width="100px" AutoPostBack="false" 
                            onclick="btnDelete_Click" ClientInstanceName="btndel" ClientSideEvents-Click="ConfirmDelete" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gvTs" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" CssClass="grid" Width="100%" ondatabinding="gvTS_DataBinding" KeyFieldName="tickSize">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Tick Size Name" FieldName="tickSizeName" />
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Price" FieldName="price" />
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Tick Size" FieldName="tickSize" />
                </Columns>
                <SettingsPager AlwaysShowPager="True" />
                <Settings ShowGroupButtons="False"  ShowVerticalScrollBar="True" />
                <SettingsCustomizationWindow Height="100%" />
                <Styles>
                    <AlternatingRow Enabled="True" />
                    <Header CssClass="gridHeader" />
                </Styles>
                <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <SettingsPager PageSize="20" />
            </dx:ASPxGridView>
        </div>        
    </div>
</asp:Content>