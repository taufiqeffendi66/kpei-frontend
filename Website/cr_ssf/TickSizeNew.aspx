﻿<%@ Page Title="Tick Size" Language="C#" AutoEventWireup="true" CodeBehind="TickSizeNew.aspx.cs" Inherits="imq.kpei.cr_ssf.TickSizeNew" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        function OnValidation(s, e) {
            var val = e.value;
            if (val == null || val == '')
                return;
            if (val.length < 1)
                e.isValid = false;
        }
    </script>
   <dx:ASPxHiddenField ID="hfNewTickSize" runat="server" ClientInstanceName="hfNewTS" />
   <div class="content">
        <div style="margin-left: auto; margin-right: auto; text-align: center;">
            <div class="title">
                <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="New Tick Size"  />
            </div>
        </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNamaTickSize" runat="server" Text="Tick Size Name" />
                        &nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxNamaTickSize" runat="server" Width="640px" EnableClientSideAPI="true" ClientInstanceName="cINamaTickSize">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblTickSize" runat="server" Text="Tick Size" />
                        &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                    </td>
                    <td>
                        <asp:GridView ID="Gridview1" runat="server" ShowFooter="true" AutoGenerateColumns="false" OnRowCreated="Gridview1_RowCreated">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="No" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false ItemStyle-Font-Size=Small />
                                <asp:TemplateField HeaderText="Harga Awal" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBoxHargaAwal" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvHargaAwal" runat="server" ControlToValidate="txtBoxHargaAwal" ErrorMessage="Required" Font-Size=Small Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revHargaAwal" runat="server" ControlToValidate="txtBoxHargaAwal" ValidationExpression="^[0-9]*$" ErrorMessage="Only numeric allowed" Font-Size=Small Display="Dynamic" ForeColor=Red />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Harga Akhir" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBoxHargaAkhir" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvHargaAkhir" runat="server" ControlToValidate="txtBoxHargaAkhir" ErrorMessage="Required" Font-Size=Small Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revHargaAkhir" runat="server" ControlToValidate="txtBoxHargaAkhir" ValidationExpression="^[0-9]*$" ErrorMessage="Only numeric allowed" Font-Size=Small Display="Dynamic" ForeColor=Red />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tick Size" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBoxTickSize" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvTickSize" runat="server" ControlToValidate="txtBoxTickSize" ErrorMessage="Required" Font-Size=Small Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revTickSize" runat="server" ControlToValidate="txtBoxTickSize" ValidationExpression="^[0-9]*$" ErrorMessage="Only numeric allowed" Font-Size=Small Display="Dynamic" ForeColor=Red />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkBtnRemove" runat="server" onclick="linkBtnRemove_Click" Font-Size=Small >
                                            Remove
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Right"/>
                                    <FooterTemplate>
                                        <asp:LinkButton ID="ButtonAdd" runat="server" onclick="ButtonAdd_Click" Font-Size=Small >
                                            Add New Row
                                        </asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <table>
            <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                <td></td><td></td><td></td><td></td>
                <td>
                    <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false" onclick="btnSave_Click" Width="70px" /> 
                </td>
                <td>
                    <dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" onclick="btnChecker_Click" Width="70px" />
                </td>
                <td>
                    <dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" onclick="btnApproval_Click" Width="70px" />
                </td>
                <td>
                    <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" onclick="btnReject_Click" Width="70px"/>
                </td>
                <td>
                    <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True" onclick="btnCancel_Click" CausesValidation="false" Width="70px" />
                </td>
            </tr>
        </table>
   </div>  
</asp:Content>
