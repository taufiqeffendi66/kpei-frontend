﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorporateActionNew.aspx.cs" Inherits="imq.kpei.cr_ssf.CorporateActionNew" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        function OnValidation(s, e) {
            var ratioValue = e.value;
            if (e.value == null) {
                e.errorText = "Required";
                e.isValid = false;
            }
            else if (e.value < 1) {
                e.errorText = "Input must be 1 or more";
                e.isValid = false;
            }
            else if (e.isValid && ratioValue.charAt(0) == '0') {
                ratioValue = ratioValue.replace(/^0+/, "");
                if (ratioValue.length == 0)
                    ratioValue = "0";
                e.value = ratioValue;
            }
        }

        function checkUnderlying(s, e) {
            if (e.value == null) {
                e.errorText = "Required";
                e.isValid = false;
            }
        }

        function OnNumberValidation(s, e) {
            var num = e.value;
            if (num == null || num == "")
                return;
            var digits = "0123456789.,";
            for (var i = 0; i < num.length; i++) {
                if (digits.indexOf(num.charAt(i)) == -1) {
                    e.isValid = false;
                    break;
                }
            }
        }

        function checkTheoreticalPrice(s, e) {
            var replaceValue = e.value;
            var digitsAfterDecimal = 2;
            var val = document.getElementById("<%=txtBoxTheoreticalPrice.ClientID%>").value;
            if (e.isValid) {
                replaceValue = replaceValue.replace(/,/g, '.');
                e.value = replaceValue;
            }
            else if (val.length - (val.indexOf(".") + 1) > digitsAfterDecimal) {
                alert("!!!! Please Enter Valid Cash. Only " + digitsAfterDecimal + " Digits Are Allowed After Decimal. !!!!");
                return false;
            }
        }

        function checkJenisCa(s, e) {
            var selectedValue = clJenisCa.GetText();
            if (selectedValue == "Reverse Stock Split") {
                clAnnouncementDate.SetEnabled(false);
                clTheoreticalPrice.SetEnabled(false);
                clRatioN0.SetEnabled(true);
                clRatioNn.SetEnabled(true);
                clAnnouncementDate.SetDate(null);
                clTheoreticalPrice.SetText("");
            }
            else if (selectedValue == "Right Issue") {
                clAnnouncementDate.SetEnabled(true);
                clTheoreticalPrice.SetEnabled(true);
                clRatioN0.SetEnabled(false);
                clRatioNn.SetEnabled(false);
                clRatioN0.SetText("");
                clRatioNn.SetText("");
            }
            else {
                clAnnouncementDate.SetEnabled(false);
                clTheoreticalPrice.SetEnabled(false);
                clRatioN0.SetEnabled(true);
                clRatioNn.SetEnabled(true);
                clAnnouncementDate.SetDate(null);
                clTheoreticalPrice.SetText("");
            }
        }

        function dateValidattion(s, e) {
            var cDateLdayTransaction = new Date();
            var xDateFdayTransaction = new Date();
            var difference;
            var selectedValue = clJenisCa.GetText();
            cDateLdayTransaction = clCdateLdayTransaction.GetDate();
            xDateFdayTransaction = clXdateFdayTransaction.GetDate();
            if (cDateLdayTransaction != null && xDateFdayTransaction != null) {
                var startTime = cDateLdayTransaction.getTime();
                var endTime = xDateFdayTransaction.getTime();
                difference = (endTime - startTime);
                if (difference < 0) {
                    if (selectedValue == "Reverse Stock Split") {
                        e.isValid = false;
                        e.errorText = "First Day Transaction input should not be less than Last Day Transaction";
                    }
                    else if (selectedValue == "Right Issue") {
                        e.isValid = false;
                        e.errorText = "Ex-Date input should not be less than Cum-Date";
                    }
                    else {
                        e.isValid = false;
                        e.errorText = "First Day Transaction input should not be less than Last Day Transaction";
                    }
                }
                else if (difference == 0) {
                    if (selectedValue == "Reverse Stock Split") {
                        e.isValid = false;
                        e.errorText = "First Day Transaction input should not be equal to Last Day Transaction";
                    }
                    else if (selectedValue == "Right Issue") {
                        e.isValid = false;
                        e.errorText = "Ex-Date input should not be equal to Cum-Date";
                    }
                    else {
                        e.isValid = false;
                        e.errorText = "First Day Transaction input should not be equal to Last Day Transaction";
                    }
                }
            }
        }
    </script>
    <div class="content">
    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Corporate Action"  />
        </div>
    </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID">
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCaType" runat="server" Text="Corporate Action Type" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbBoxJenisCa" EnableViewState="false" ClientInstanceName="clJenisCa" runat="server" Width="170px" EnableClientSideAPI="true">
                            <Items>
                                <dx:ListEditItem Text="Reverse Stock Split" Value="1" Selected="False" />
                                <dx:ListEditItem Text="Right Issue" Value="2" Selected="False" />
                                <dx:ListEditItem Text="Stock Split" Value="3" Selected="False" />
                            </Items>
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="checkJenisCa" />
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblStockUnderlying" runat="server" Text="Stock Underlying" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbBoxStockUnderlying" ClientInstanceName="clStockUnderlying" runat="server" Width="170px" EnableClientSideAPI="true">
                            <Items>
                                <dx:ListEditItem Text="BBCA" Value="bbca" Selected="False" />
                                <dx:ListEditItem Text="BMRI" Value="bmri" Selected="False" />
                                <dx:ListEditItem Text="TLKM" Value="tlkm" Selected="False" />
                            </Items>
                            <ClientSideEvents Validation="checkUnderlying" />
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblAnnouncementDate" runat="server" Text="Announcement Date" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditAnnouncementDate" ClientInstanceName="clAnnouncementDate" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true" Caption="Date" EditFormatString="dd/MM/yyyy" EnableClientSideAPI="true">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCdateLdayTransaction" runat="server" Text="Cum-Date / Last Day Transaction" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditCdateLdayTransaction" ClientInstanceName="clCdateLdayTransaction" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true" Caption="Date" EditFormatString="dd/MM/yyyy" EnableClientSideAPI="true">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblXdateFdayTransaction" runat="server" Text="Ex-Date / First Day Transaction" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditXdateFdayTransaction" ClientInstanceName="clXdateFdayTransaction" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true"  Caption="Date" EditFormatString="dd/MM/yyyy" EnableClientSideAPI="true">
                            <ValidationSettings ErrorText="Requiredd">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="dateValidattion" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblRatioN0" runat="server" Text="Ratio N0" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxRatioN0" ClientInstanceName="clRatioN0" runat="server" Width="170px" EnableClientSideAPI="true">
                            <ValidationSettings EnableCustomValidation="True">
                                <RegularExpression ValidationExpression="^[0-9]*$" ErrorText="Only numeric allowed" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblRatioNn" runat="server" Text="Ratio Nn" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxRatioNn" ClientInstanceName="clRatioNn" runat="server" Width="170px" EnableClientSideAPI="true" >
                            <ValidationSettings EnableCustomValidation="True">
                                <RegularExpression ValidationExpression="^[0-9]*$" ErrorText="Only numeric allowed" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblTheoreticalPrice" runat="server" Text="Theoretical Price" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxTheoreticalPrice" MaskSettings-Mask="<0..999999999999999g>.<00..99>" ClientInstanceName="clTheoreticalPrice" runat="server" Width="170px" EnableClientSideAPI="true">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnNumberValidation" />
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false" onclick="btnSave_Click" Width="70px" /> 
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" onclick="btnChecker_Click" Width="70px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" onclick="btnApproval_Click" Width="70px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" onclick="btnReject_Click" Width="70px"/>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True" onclick="btnCancel_Click" CausesValidation="false" Width="70px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
