﻿<%@ Page Title="Corporate Action" Language="C#" AutoEventWireup="true" CodeBehind="CorporateActionRightIssueApproval.aspx.cs" Inherits="imq.kpei.cr_ssf.CorporateActionRightIssueApproval" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Corporate Action"  />
        </div>
    </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID">
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCaType" runat="server" Text="Corporate Action Type" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxJenisCA" ClientInstanceName="clJenisCA" ReadOnly="true" Enabled="false" onkeydown="return false;" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblStockUnderlying" runat="server" Text="Stock Underlying" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxStockUnderlying" ClientInstanceName="clStockUnderlying" ReadOnly="true" Enabled="false" onkeydown="return false;" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblAnnouncementDate" runat="server" Text="Announcement Date" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxAnnouncementDate" ClientInstanceName="clAnnouncementDate" ReadOnly="true" Enabled="false" onkeydown="return false;" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCumDate" runat="server" Text="Cum-Date" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxCumDate" ClientInstanceName="clCumDate" ReadOnly="true" Enabled="false" onkeydown="return false;" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblExDate" runat="server" Text="Ex-Date" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxExDate" ClientInstanceName="clExDate" ReadOnly="true" Enabled="false" onkeydown="return false;" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblTheoreticalPrice" runat="server" Text="Theoretical Price" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxTheoreticalPrice" ClientInstanceName="clTheoreticalPrice" ReadOnly="true" Enabled="false" onkeydown="return false;" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br /><br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNotes" runat="server" style="font-weight" Text="Accept New Corporate Action, Are You Sure ? "  />
                    </td>
                    <td>
                        <dx:ASPxCheckBox ID="chkBoxNotes" runat="server" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxArea" runat="server" Width="300px" Height="50" />
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnAccept" runat="server" Text="Accept" Width="100px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDecline" runat="server" Text="Decline" Width="100px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" Width="100px"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
