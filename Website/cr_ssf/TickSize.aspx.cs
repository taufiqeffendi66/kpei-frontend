﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.wsUserGroupPermission;

namespace imq.kpei.cr_ssf
{
    public partial class TickSize : System.Web.UI.Page
    {
        public string stataddedit = "0";
        private tickSize[] TS;
        private String aUserLogin;
        private String aUserMember;
        public String aTickSizeName;

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtBoxNamaTickSize.Text = string.Empty;
        }

        private void EnableButton(bool enable)
        {
            lblNew.Enabled = enable;
            btnEdit.Enabled = enable;
        }

        private tickSize[] getWSTickSize()
        {
            // source web  service (wsdl)
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "TickSize");

            for (int i = 0; i < dtFMP.Length; i++)
            {
                if (dtFMP[i].editMaker || dtFMP[i].editDirectChecker || dtFMP[i].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[i].removeing;
            }
        }

        protected void gvTS_DataBinding(object sender, EventArgs e)
        {
            gvTs.DataSource = getWSTickSize();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            ImqSession.ValidateAction(this);

            if (!IsPostBack)
            {
                Permission();
                gvTs.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string tickSizeName = txtBoxNamaTickSize.Text.Trim();
            string search = String.Empty;

            if (tickSizeName != String.Empty)
            {
                tickSizeName = tickSizeName.Replace("'", "''");
                tickSizeName = tickSizeName.Replace("--", "-");
                aTickSizeName = tickSizeName;
                tickSizeName = String.Format("[tickSizeName] like '%{0}%'", tickSizeName);
            }

            else
            {
                if (tickSizeName != String.Empty)
                    search = tickSizeName;
                else
                    search = String.Empty;
            }

            gvTs.FilterExpression = search;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string strTickSizeName = hfTickSize.Get("tickSizeName") == null ? " " : hfTickSize.Get("tickSizeName").ToString().Trim();
            string strPrice = hfTickSize.Get("price") == null ? " " : hfTickSize.Get("price").ToString().Trim();
            string strTickSize = hfTickSize.Get("tickSize") == null ? " " : hfTickSize.Get("tickSize").ToString().Trim();
            string strRedirect = String.Format("TickSizeNew.aspx?stat=1&tickSizeName={0}&price={1}&tickSize={2}", Server.UrlEncode(strTickSizeName), Server.UrlEncode(strPrice), Server.UrlEncode(strTickSize));

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strTickSizeName = hfTickSize.Get("tickSizeName") == null ? " " : hfTickSize.Get("tickSizeName").ToString().Trim();
            string strPrice = hfTickSize.Get("price") == null ? " " : hfTickSize.Get("price").ToString().Trim();
            string strTickSize = hfTickSize.Get("tickSize") == null ? " " : hfTickSize.Get("tickSize").ToString().Trim();
            string strRedirect = String.Format("TickSizeNew.aspx?stat=4&tickSizeName={0}&price={1}&tickSize={2}", Server.UrlEncode(strTickSizeName), Server.UrlEncode(strPrice), Server.UrlEncode(strTickSize));

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

    }
}