﻿<%@ Page Title="Corporate Action" Language="C#" AutoEventWireup="true" CodeBehind="CorporateActionRightIssue.aspx.cs" Inherits="imq.kpei.cr_ssf.CorporateActionRightIssue" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Corporate Action"  />
        </div>
    </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID" onselectedindexchanged="rblApproval_SelectedIndexChanged">
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCaType" runat="server" Text="Corporate Action Type" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxJenisCA" ClientInstanceName="clJenisCA" ReadOnly="true" Enabled="false" onkeydown="return false;" runat="server" Text="Right Issue" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblStockUnderlying" runat="server" Text="Stock Underlying" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbBoxStockUnderlying" ClientInstanceName="clStockUnderlying" runat="server" Width="170px" EnableClientSideAPI="true">
                            <Items>
                                <dx:ListEditItem Text="BBCA" Value="bbca" Selected="False" />
                                <dx:ListEditItem Text="BMRI" Value="bmri" Selected="False" />
                                <dx:ListEditItem Text="TLKM" Value="tlkm" Selected="False" />
                            </Items>
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblAnnouncementDate" runat="server" Text="Announcement Date" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditAnnouncementDate" ClientInstanceName="clAnnouncementDate" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true" Caption="Date">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCumDate" runat="server" Text="Cum-Date" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditCumDate" ClientInstanceName="clCumDate" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true" Caption="Date">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblExDate" runat="server" Text="Ex-Date" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditExDate" ClientInstanceName="clEditExDate" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true" Caption="Date">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblTheoreticalPrice" runat="server" Text="Theoretical Price" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxTheoreticalPrice" ClientInstanceName="clTheoreticalPrice" runat="server" Width="170px" EnableClientSideAPI="true">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                                <RegularExpression ValidationExpression="^\d+([,\.]\d{1,2})?$" ErrorText="Only numeric allowed" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false" onclick="btnSave_Click" Width="70px" /> 
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" onclick="btnChecker_Click" Width="70px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" onclick="btnApproval_Click" Width="70px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" onclick="btnReject_Click" Width="70px"/>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True" onclick="btnCancel_Click" CausesValidation="false" Width="70px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
