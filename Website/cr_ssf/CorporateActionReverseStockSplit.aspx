﻿<%@ Page Title="Corporate Action" Language="C#" AutoEventWireup="true" CodeBehind="CorporateActionReverseStockSplit.aspx.cs" Inherits="imq.kpei.cr_ssf.CorporateActionReverseStockSplit" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        function OnValidation(s, e) {
            if (e.value == null) {
                e.errorText = "Required";
                e.isValid = false;
            }
            else if (e.value < 1) {
                e.errorText = "Input must be 1 or more";
                e.isValid = false;
            }
            else if (e.isValid && ratioValue.charAt(0) == '0') {
                ratioValue = ratioValue.replace(/^0+/, "");
                if (ratioValue.length == 0)
                    ratioValue = "0";
                e.value = ratioValue;
            }
        }
    </script>
    <div class="content">
    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Corporate Action"  />
        </div>
    </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID" onselectedindexchanged="rblApproval_SelectedIndexChanged">
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCaType" runat="server" Text="Corporate Action Type" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbBoxJenisCa" ClientInstanceName="clJenisCa" runat="server" Width="170px" EnableClientSideAPI="true">
                            <Items>
                                <dx:ListEditItem Text="Reverse Stock Split" Value="reverseStockSplit" Selected="False" />
                                <dx:ListEditItem Text="Stock Split" Value="stockSplit" Selected="False" />
                            </Items>
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblStockUnderlying" runat="server" Text="Stock Underlying" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbBoxStockUnderlying" ClientInstanceName="clStockUnderlying" runat="server" Width="170px" EnableClientSideAPI="true">
                            <Items>
                                <dx:ListEditItem Text="BBCA" Value="bbca" Selected="False" />
                                <dx:ListEditItem Text="BMRI" Value="bmri" Selected="False" />
                                <dx:ListEditItem Text="TLKM" Value="tlkm" Selected="False" />
                            </Items>
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblLastDayTransaction" runat="server" Text="Last Day Transaction" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditLastDayTransaction" ClientInstanceName="clLastDayTransaction" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true" Caption="Date">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblFirstDayTransaction" runat="server" Text="First Day Transaction" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditFirstDayTransaction" ClientInstanceName="clFirstDayTransaction" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true"  Caption="Date">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblRatioN0" runat="server" Text="Ratio N0" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxRatioN0" ClientInstanceName="clRatioN0" runat="server" Width="170px" EnableClientSideAPI="true">
                            <ValidationSettings EnableCustomValidation="True">
                                <RegularExpression ValidationExpression="^[0-9]*$" ErrorText="Only numeric allowed" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblRatioNn" runat="server" Text="Ratio Nn" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxRatioNn" ClientInstanceName="clRatioNn" runat="server" Width="170px" EnableClientSideAPI="true">
                            <ValidationSettings EnableCustomValidation="True">
                                <RegularExpression ValidationExpression="^[0-9]*$" ErrorText="Only numeric allowed" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false" onclick="btnSave_Click" Width="70px" /> 
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" onclick="btnChecker_Click" Width="70px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" onclick="btnApproval_Click" Width="70px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" onclick="btnReject_Click" Width="70px"/>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True" onclick="btnCancel_Click" CausesValidation="false" Width="70px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
