﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiplierTemplateInput.aspx.cs" Inherits="imq.kpei.cr_ssf.MultiplierTemplateInput" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
            <div  class="content">
                <div style="margin-left: auto; margin-right: auto; text-align: center;">
                    <div class="title">
                        <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Multiplier Template"  />
                    </div>
                </div>  
                <div id="plApproval">
                    <table>
                        <tr>
                            <td>                          
                                <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                                        ClientIDMode="AutoID">
                                </dx:ASPxRadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <table id="tblContract">
                    <tr>
                        <td nowrap="nowrap">
                            ID<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxId" runat="server" MaxLength="20" Size="20" Width="170px" EnableClientSideAPI="true">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="false" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Template Name<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxTemplateName" runat="server" MaxLength="20" Size="20" Width="170px" EnableClientSideAPI="true">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="false" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Underlying<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">                      
                            <dx:ASPxComboBox ID="cmbBoxUnderlying" runat="server" EnableClientSideAPI="true" 
                                ValueType="System.String"></dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Tick Size<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbBoxTickSize" runat="server" EnableClientSideAPI="True">
                                <Items>
                                    <dx:ListEditItem Text="Rp 1.-" Value="1" />
                                    <dx:ListEditItem Text="Rp 2.-" Value="2" />
                                    <dx:ListEditItem Text="Rp 5.-" Value="5" />
                                    <dx:ListEditItem Text="Rp 10.-" Value="10" />
                                    <dx:ListEditItem Text="Rp 25.-" Value="25" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Free Value<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxClearingValue" runat="server" Width="170px" EnableClientSideAPI="true" MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Type<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxClearingFreeType" runat="server" Width="170px" EnableClientSideAPI="true" MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Calculation<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxClearingFeeCalculation" runat="server" Width="170px" EnableClientSideAPI="true" MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                   <tr>
                        <td nowrap="nowrap">
                            GF Fee <span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxGpFee" runat="server" Width="170px" EnableClientSideAPI="true" MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            HPH <span style="color:Red"> </span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxHph" runat="server" Width="170px" EnableClientSideAPI="true" MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Sampling Hph Type<span style="color:Red"> </span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbBoxSamplingHphType" runat="server" EnableClientSideAPI="True">
                                <Items>
                                    <dx:ListEditItem Text="Fixed" Value="F" />
                                    <dx:ListEditItem Text="Range" Value="R" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Start Sampling HPH<span style="color:Red"> </span>
                        </td>
                        <td style="width: 168px;">
                            <dx:ASPxTimeEdit ID="timeEditStartSamplingHph" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true">  
                            </dx:ASPxTimeEdit>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            End Sampling HPH<span style="color:Red"> </span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTimeEdit ID="timeEditEndSamplingHph" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true" >
                            </dx:ASPxTimeEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Trading Hour<span style="color:Red"> </span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTimeEdit ID="timeEditStartTradingHour" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true">   
                            </dx:ASPxTimeEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Trading Hour<span style="color:Red"> </span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTimeEdit ID="timeEditEndTradingHour" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true" >
                            </dx:ASPxTimeEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>Multiplier<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxMultiplier" runat="server" Width="170px" EnableClientSideAPI="true" MaskSettings-Mask="<0..999999999999999g>" HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />                    
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Margin Type<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbBoxMarginType" runat="server" EnableClientSideAPI="True" >
                                <Items>
                                    <dx:ListEditItem Text="Fixed" Value="F" />
                                    <dx:ListEditItem Text="Floating" Value="L" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Margin Value<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtBoxMarginValue" runat="server" Width="170px" MaskSettings-Mask="<0..99999g>" HorizontalAlign="Right">
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Margin Fee
                        </td>
                        <td >
                            <dx:ASPxTextBox ID="txtBoxMarginFee" runat="server" MaskSettings-Mask="<0..999999999999999g>" HorizontalAlign="Right">
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
                </div>
                <div class="clear"></div>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" Width="70px"></dx:ASPxButton>
                            </td>        
                        </tr>
                    </table>
                    <dx:ASPxHiddenField ID="hfMultiplierTemplate" runat="server" 
                        ClientInstanceName="hfMultiplierTemplate">
                    </dx:ASPxHiddenField>
                </div>
</asp:Content>