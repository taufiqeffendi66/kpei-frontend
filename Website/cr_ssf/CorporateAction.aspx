﻿<%@ Page Title="Corporate Action" Language="C#" AutoEventWireup="true" CodeBehind="CorporateAction.aspx.cs" Inherits="imq.kpei.cr_ssf.CorporateAction" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        function clearEditors(s, e) {
            var container = document.getElementsByClassName("clientContainer")[0];
            ASPxClientEdit.ClearEditorsInContainer(container);
        }

        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'no;caType;stockUnderlying;annoucementDate;cumDateOrLastDayTransaction;exDateOrFirstDayTransaction;ratioN0;ratioNn;theoreticalPrice;', OnGetRowValues);
        }

        function OnGetRowValues(values) {
            hfCA.Set("no", values[0]);
            hfCA.Set("caType", values[1]);
            hfCA.Set("stockUnderlying", values[2]);
            hfCA.Set("annoucementDate", values[3]);
            hfCA.Set("cumDateOrLastDayTransaction", values[4]);
            hfCA.Set("exDateOrFirstDayTransaction", values[5]);
            hfCA.Set("ratioN0", values[6]);
            hfCA.Set("ratioNn", values[7]);
            hfCA.Set("theoreticalPrice", values[8]);
        }

        function ConfirmDelete(s, e) {
            e.processOnServer = confirm("Are you sure delete this Corporate Action \nCA Type = " + hfCA.Get("caType") + ",\nStock Underlying = " + hfCA.Get("stockUnderlying"));
        }
    </script>
    <dx:ASPxHiddenField ID="hfCorporateAction" runat="server" ClientInstanceName="hfCA"></dx:ASPxHiddenField>
    <div class="content">
    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Corporate Action"  />
        </div>
    </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID">
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCaType" runat="server" Text="CA Type" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbBoxJenisCa" runat="server" Width="170px">
                            <Items>
                                <dx:ListEditItem Text="Reverse Stock Split" Value="0" Selected="False" />
                                <dx:ListEditItem Text="Right Issue" Value="1" Selected="False" />
                                <dx:ListEditItem Text="Stock Split" Value="2" Selected="False" />
                            </Items>
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblStockUnderlying" runat="server" Text="Stock Underlying" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxStockUnderlying" runat="server" Width="170px" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" Width="30px" AutoPostBack="false" onclick="btnSearch_Click" />
                    </td>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>
                        <dx:ASPxButton ID="btnReset" runat="server" Text="Reset" Width="30px" AutoPostBack="true" OnClick="btnReset_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br /><br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNewCA" Width="250px" EncodeHtml="false" runat="server" Text="<a href='CorporateActionNew.aspx'>+ Add New Corporate Action</a>"></dx:ASPxLabel>
                    </td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>
                        <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" Width="100px" AutoPostBack="false" 
                            onclick="btnEdit_Click" ClientInstanceName="btned" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" Width="100px" AutoPostBack="false" 
                            onclick="btnDelete_Click" ClientInstanceName="btndel" ClientSideEvents-Click="ConfirmDelete" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gvCa" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" CssClass="grid" Width="105%" ondatabinding="gvCA_DataBinding" KeyFieldName="corporateAction">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="No" FieldName="no" />
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="CA Type" FieldName="caType" />
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Stock Underlying" FieldName="stockUnderlying" />
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Annoucement Date" FieldName="annoucementDate" />
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Cum-Date<br />(Last Day Transaction)" FieldName="cumDateOrLastDayTransaction" />
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Ex-Date<br />(First Day Transaction)" FieldName="exDateOrFirstDayTransaction" />
                    <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Ratio N0" FieldName="ratioN0" />
                    <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Ratio Nn" FieldName="ratioNn" />
                    <dx:GridViewDataTextColumn VisibleIndex="8" Caption="Theoretical Price" FieldName="theoreticalPrice" />
                </Columns>
                <SettingsPager AlwaysShowPager="True" />
                <Settings ShowGroupButtons="False"  ShowVerticalScrollBar="True" />
                <SettingsCustomizationWindow Height="100%" />
                <Styles>
                    <AlternatingRow Enabled="True" />
                    <Header CssClass="gridHeader" />
                </Styles>
                <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <SettingsPager PageSize="20" />
            </dx:ASPxGridView>
        </div>        
    </div>
</asp:Content>
