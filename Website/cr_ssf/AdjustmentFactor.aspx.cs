﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;

namespace imq.kpei.cr_ssf
{
    public partial class AdjustmentFactor : System.Web.UI.Page
    {
        public string stataddedit = "0";
        private adjusmentFactor[] af;
        private String aUserLogin;
        private String aUserMember;
        public String aCaType;
        public String aContractName;

        protected void Page_Load(object sender, EventArgs e)
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                gvAf.DataBind();
            }
        }

        private void btnSearch_Click(Object sender, EventArgs e)
        {
            String caType = cmbBoxCaType.Text;
            String contractName = txtBoxContractName.Text.Trim();
            String search = String.Empty;

            if (caType != String.Empty)
            {
                caType = caType.Replace("'", "''");
                caType = caType.Replace("--", "-");
                aCaType = caType;
                caType = String.Format("[caType] like '%{0}%'", caType);
            }
            if (contractName != String.Empty)
            {
                contractName = contractName.Replace("'", "''");
                contractName = contractName.Replace("--", "-");
                aContractName = contractName;
                contractName = String.Format("[contractName] like '%{0}%'", contractName);
            }
            if (caType != String.Empty)
            {
                if (contractName != String.Empty)
                    search = String.Format("{0} and {1}", caType, contractName);
                else
                    search = caType;
            }
            else
            {
                if (contractName != String.Empty)
                    search = contractName;
                else
                    search = String.Empty;
            }
            gvAf.FilterExpression = search;
        }

        protected void gvAf_DataBinding(object sender, EventArgs e)
        {
            gvAf.DataSource = getWSAf();
        }

        private adjusmentFactor[] getWSAf()
        {
            // source web  service (wsdl)
        }
    }
}