﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.wsUserGroupPermission;

namespace imq.kpei.cr_ssf
{
    public partial class CorporateAction : System.Web.UI.Page
    {
        public string stataddedit = "0";
        private corporateAction[] CA;
        private String aUserLogin;
        private String aUserMember;
        public String aCaType;
        public String aStockUnderlying;

        protected void btnReset_Click(object sender, EventArgs e)
        {
            cmbBoxJenisCa.Text = string.Empty;
            txtBoxStockUnderlying.Text = string.Empty;
        }

        private void EnableButton(bool enable)
        {
            lblNewRightIssue.Enabled = enable;
            lblNewStockSplitOrReverserStockSplit.Enabled = enable;
            btnEdit.Enabled = enable;
        }

        private corporateAction[] getWSCorporateAction()
        {
            // source web  service (wsdl)
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "CorporateAction");

            for (int i = 0; i < dtFMP.Length; i++)
            {
                if (dtFMP[i].editMaker || dtFMP[i].editDirectChecker || dtFMP[i].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[i].removeing;
            }
        }

        protected void gvCA_DataBinding(object sender, EventArgs e)
        {
            gvCa.DataSource = getWSCorporateAction();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            ImqSession.ValidateAction(this);

            if (!IsPostBack)
            {
                Permission();
                gvCa.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string caType = cmbBoxJenisCa.Text;
            string stockUnderlying = txtBoxStockUnderlying.Text.Trim();
            string search = String.Empty;

            if (caType != String.Empty)
            {
                caType = caType.Replace("'", "''");
                caType = caType.Replace("--", "-");
                aCaType = caType;
                caType = String.Format("[caType] like '%{0}%'", caType);
            }

            if (stockUnderlying != String.Empty)
            {
                stockUnderlying = stockUnderlying.Replace("'", "''");
                stockUnderlying = stockUnderlying.Replace("--", "-");
                aStockUnderlying = stockUnderlying;
                stockUnderlying = String.Format("[stockUnderlying] like '%{0}%'", stockUnderlying);
            }

            if (caType != String.Empty)
            {
                if (stockUnderlying != String.Empty)
                    search = String.Format("{0} and {1}", caType, stockUnderlying);
                else
                    search = caType;
            }

            else
            {
                if (stockUnderlying != String.Empty)
                    search = stockUnderlying;
                else
                    search = String.Empty;
            }

            gvCa.FilterExpression = search;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string strCaType = hfCorporateAction.Get("caType") == null ? " " : hfCorporateAction.Get("caType").ToString();
            string strStockUnderlying = hfCorporateAction.Get("stockUnderlying") == null ? " " : hfCorporateAction.Get("stockUnderlying").ToString().Trim();
            string strAnnoucementDate = hfCorporateAction.Get("annoucementDate") == null ? " " : hfCorporateAction.Get("annoucementDate").ToString().Trim();
            string strcumDateOrLastDayTransaction = hfCorporateAction.Get("cumDateOrLastDayTransaction") == null ? " " : hfCorporateAction.Get("cumDateOrLastDayTransaction").ToString().Trim();
            string strexDateOrFirstDayTransaction = hfCorporateAction.Get("exDateOrFirstDayTransaction") == null ? " " : hfCorporateAction.Get("exDateOrFirstDayTransaction").ToString().Trim();
            string strRatioN0 = hfCorporateAction.Get("ratioN0") == null ? " " : hfCorporateAction.Get("ratioN0").ToString().Trim();
            string strRatioNn = hfCorporateAction.Get("ratioNn") == null ? " " : hfCorporateAction.Get("ratioNn").ToString().Trim();
            string strTheoreticalPrice = hfCorporateAction.Get("theoreticalPrice") == null ? " " : hfCorporateAction.Get("theoreticalPrice").ToString().Trim();
            string strRedirect = String.Format("CorporateActionNew.aspx?stat=1&caType={0}&stockUnderlying={1}&annoucementDate={2}&cumDateOrLastDayTransaction={3}&exDateOrFirstDayTransaction={4}&ratioN0={5}&ratioNn={6}&theoreticalPrice={7}", Server.UrlEncode(strCaType), Server.UrlEncode(strStockUnderlying), Server.UrlEncode(strAnnoucementDate), Server.UrlEncode(strcumDateOrLastDayTransaction), Server.UrlEncode(strexDateOrFirstDayTransaction), Server.UrlEncode(strRatioN0), Server.UrlEncode(strRatioNn), Server.UrlEncode(strTheoreticalPrice));
            
            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();        
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strCaType = hfCorporateAction.Get("caType") == null ? " " : hfCorporateAction.Get("caType").ToString();
            string strStockUnderlying = hfCorporateAction.Get("stockUnderlying") == null ? " " : hfCorporateAction.Get("stockUnderlying").ToString().Trim();
            string strAnnoucementDate = hfCorporateAction.Get("annoucementDate") == null ? " " : hfCorporateAction.Get("annoucementDate").ToString().Trim();
            string strcumDateOrLastDayTransaction = hfCorporateAction.Get("cumDateOrLastDayTransaction") == null ? " " : hfCorporateAction.Get("cumDateOrLastDayTransaction").ToString().Trim();
            string strexDateOrFirstDayTransaction = hfCorporateAction.Get("exDateOrFirstDayTransaction") == null ? " " : hfCorporateAction.Get("exDateOrFirstDayTransaction").ToString().Trim();
            string strRatioN0 = hfCorporateAction.Get("ratioN0") == null ? " " : hfCorporateAction.Get("ratioN0").ToString().Trim();
            string strRatioNn = hfCorporateAction.Get("ratioNn") == null ? " " : hfCorporateAction.Get("ratioNn").ToString().Trim();
            string strTheoreticalPrice = hfCorporateAction.Get("theoreticalPrice") == null ? " " : hfCorporateAction.Get("theoreticalPrice").ToString().Trim();
            string strRedirect = String.Format("CorporateActionNew.aspx?stat=4&caType={0}&stockUnderlying={1}&annoucementDate={2}&cumDateOrLastDayTransaction={3}&exDateOrFirstDayTransaction={4}&ratioN0={5}&ratioNn={6}&theoreticalPrice={7}", Server.UrlEncode(strCaType), Server.UrlEncode(strStockUnderlying), Server.UrlEncode(strAnnoucementDate), Server.UrlEncode(strcumDateOrLastDayTransaction), Server.UrlEncode(strexDateOrFirstDayTransaction), Server.UrlEncode(strRatioN0), Server.UrlEncode(strRatioNn), Server.UrlEncode(strTheoreticalPrice));
           
            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest(); 
        }
    }
}