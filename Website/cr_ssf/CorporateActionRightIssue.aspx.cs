﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.clientAccountWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.cr_ssf
{
    public partial class CorporateActionRightIssue : System.Web.UI.Page
    {
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        private const String module = "CorporateAction";

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;
            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);

                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else if (!che && mem && (Request.QueryString["status"].ToString() == "C") && (dtFMP.editApproval))
                        {
                            btnApproval.Visible = true;
                            btnReject.Visible = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void ParseQueryString()
        {
            string strStockUnderlying = Server.UrlDecode(Request.QueryString["stockUnderlying"].ToString());
            string strAnnouncementDate = Server.UrlDecode(Request.QueryString["announcementDate"].ToString());
            string strCumDate = Server.UrlDecode(Request.QueryString["cumDate"].ToString());
            string strExDate = Server.UrlDecode(Request.QueryString["exDate"].ToString());
            string strTheoreticalPrice = Server.UrlDecode(Request.QueryString["theoreticalPrice"].ToString().Trim());

            cmbBoxStockUnderlying.Text = strStockUnderlying.Length > 0 ? strStockUnderlying : "";
            dateEditAnnouncementDate.Text = strAnnouncementDate.Length > 0 ? strAnnouncementDate : "";
            dateEditCumDate.Text = strCumDate.Length > 0 ? strCumDate : "";
            dateEditExDate.Text = strExDate.Length > 0 ? strExDate : "";
            txtBoxTheoreticalPrice.Text = strTheoreticalPrice.Length > 0 ? strTheoreticalPrice : "";
        }

        private void GetTemporary()
        {
            // source web  service (wsdl)
        }

        private void DisabledControl()
        {
            cmbBoxStockUnderlying.Enabled = false;
            dateEditAnnouncementDate.Enabled = false;
            dateEditCumDate.Enabled = false;
            dateEditExDate.Enabled = false;
            txtBoxTheoreticalPrice.Enabled = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("CorporateAction.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                default:
                    break;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // source web  service (wsdl)
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            // source web  service (wsdl)
        }

        private void WriteAuditTrail(string Activity)
        {
            // source web  service (wsdl)
        }

        private String Log(corporateActionTmp cat)
        {
            // source web  service (wsdl)
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, corporateActionTmp cat)
        {
            // source web  service (wsdl)
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                if (Session["stat"] == null)
                {
                    Response.Redirect("CorporateAction.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0:
                        lbltitle.Text = "New " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lbltitle.Text = "Edit " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lbltitle.Text = "New " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 3:
                        lbltitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 4:
                        lbltitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                    default: break;
                }
            }
        }

    }
}