﻿<%@ Page Title="Adjustment Factor" Language="C#" AutoEventWireup="true" CodeBehind="AdjustmentFactorDetail.aspx.cs" Inherits="imq.kpei.cr_ssf.AdjustmentFactorDetail" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
    <div style="margin-left: auto; margin-right: auto; text-align: center;">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="AF Detail - "  />
        </div>
    </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCaType" runat="server" Text="CA Type" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxCaType" ClientInstanceName="clCaType" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblContractName" runat="server" Text="Contract Name" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxContractName" ClientInstanceName="clContractName" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblRegularClosePrice" runat="server" Text="Regular Close Price" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxRegularClosePrice" ClientInstanceName="clRegularClosePrice" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblTimeRemarks" runat="server" Text="Time Remarks" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEditTimeRemarks" runat="server" Width="170px" EditFormat="Custom" UseMaskBehavior="true"  Caption="Date" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblRatio" runat="server" Text="Ratio" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxRatio" ClientInstanceName="clRatio" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblTheoreticalPrice" runat="server" Text="Theoretical Price" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxTheoreticalPrice" ClientInstanceName="clTheoreticalPrice" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblOldSettlementPrice" runat="server" Text="Old Settlement Price" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxOldSettlementPrice" ClientInstanceName="clOldSettlementPrice" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblAdjustmentFactor" runat="server" Text="Adjustment Factor" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxAdjustmentFactor" ClientInstanceName="clAdjustmentFactor" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNewContractPrice" runat="server" Text="New Contract Price" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxNewContractPrice" ClientInstanceName="clNewContractPrice" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNewOpenPosition" runat="server" Text="New Open Position" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxNewOpenPosition" ClientInstanceName="clNewOpenPosition" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblContractSize" runat="server" Text="Contract Size" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxContractSize" ClientInstanceName="clContractSize" runat="server" Width="170px" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br /><br />
        <div>
            <table>
                <tr>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>
                        <dx:ASPxButton ID="btnClose" runat="server" Text="Close" onClick="btnClose_Click"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
