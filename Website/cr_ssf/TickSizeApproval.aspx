﻿<%@ Page Title="Tick Size" Language="C#" AutoEventWireup="true" CodeBehind="TickSizeApproval.aspx.cs" Inherits="imq.kpei.cr_ssf.TickSizeApproval" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <dx:ASPxHiddenField ID="hfTickSizeApproval" runat="server" ClientInstanceName="hfTickSizeApproval" />
   <div class="content">
        <div style="margin-left: auto; margin-right: auto; text-align: center;">
            <div class="title">
                <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Approval Tick Size"  />
            </div>
        </div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td>
                        <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNamaTickSize" runat="server" Text="Tick Size Name" />
                        &nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxNamaTickSize" runat="server" Width="640px" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblTickSize" runat="server" Text="Tick Size" />
                        &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                    </td>
                    <td>
                        <asp:GridView ID="Gridview1" runat="server" ShowFooter="true" AutoGenerateColumns="false" OnRowCreated="Gridview1_RowCreated">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="No" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false />
                                <asp:TemplateField HeaderText="Harga Awal" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBoxHargaAwal" runat="server" TextMode="Number"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Harga Akhir" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBoxHargaAkhir" runat="server" TextMode="Number"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tick Size" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBoxTickSize" runat="server" TextMode="Number"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action" HeaderStyle-Font-Size=Small HeaderStyle-Font-Bold=false>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkBtnRemove" runat="server" onclick="linkBtnRemove_Click" Font-Size=Small >
                                            Remove
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Right"/>
                                    <FooterTemplate>
                                        <asp:LinkButton ID="ButtonAdd" runat="server" onclick="ButtonAdd_Click" Font-Size=Small >
                                            Add New Row
                                        </asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblNotes" runat="server" style="font-weight" Text="Accept New Tick Size, Are You Sure ? "  />
                    </td>
                    <td>
                        <dx:ASPxCheckBox ID="chkBoxNotes" runat="server" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxArea" runat="server" Width="307px" Height="50" />
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnAccept" runat="server" Text="Accept" Width="100px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDecline" runat="server" Text="Decline" Width="100px" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" Width="100px"/>
                    </td>
                </tr>
            </table>
        </div>
   </div>  
</asp:Content>
