﻿<%@ Page Title="Adjustment Factor" Language="C#" AutoEventWireup="true" CodeBehind="AdjustmentFactor.aspx.cs" Inherits="imq.kpei.cr_ssf.AdjustmentFactor" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'caType;contractName;regClosePrice;timeRemarks;ratio;theoreticalPrice;oldSettlementPrice;adjustmentFactor;newContractPrice;newOpenPosition;contractSize;', OnGetRowValues);
        }
        function OnGetRowValues(values) {
            hfAF.Set("caType", values[0]);
            hfAF.Set("contractName", values[1]);
            hfAF.Set("regClosePrice", values[2]);
            hfAF.Set("timeRemarks", values[3]);
            hfAF.Set("ratio", values[4]);
            hfAF.Set("theoreticalPrice", values[5]);
            hfAF.Set("oldSettlementPrice", values[6]);
            hfAF.Set("adjustmentFactor", values[7]);
            hfAF.Set("newContractPrice", values[8]);
            hfAF.Set("newOpenPosition", values[9]);
            hfAF.Set("contractSize", values[10]);
        }
    </script>
    <dx:ASPxHiddenField ID="hfAdjustmentFactor" runat="server" ClientInstanceName="hfAF"></dx:ASPxHiddenField>
    <div class="content">
        <div style="margin-left: auto; margin-right: auto; text-align: center;">
            <div class="title">
                <dx:ASPxLabel ID="lbltitle" runat="server" style="font-weight:bold" Text="Adjustment Factor"  />
            </div>
        </div>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblCaType" runat="server" Text="CA Type" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbBoxCaType" runat="server" Width="170px">
                            <Items>
                                <dx:ListEditItem Text="Reverse Stock Split" Value="reverserStockSplit" Selected="False" />
                                <dx:ListEditItem Text="Right Issue" Value="rightIssue" Selected="False" />
                                <dx:ListEditItem Text="Stock Split" Value="stockSplit" Selected="False" />
                            </Items>
                        </dx:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lblContractName" runat="server" Text="Contract Name" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtBoxContractName" ClientInstanceName="clContractName" runat="server" Width="170px" />
                    </td>
                    <td></td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" AutoPostBack="false" onclick="btnSearch_Click" /> 
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <dx:ASPxGridView ID="gvAf" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" CssClass="grid" Width="110%" ondatabinding="gvAf_DataBinding" KeyFieldName="adjusmentFactor">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="CA Type" FieldName="caType" />
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Contract Name" FieldName="contractName" />
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Regular Close Price" FieldName="regClosePrice" />
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Time Remarks" FieldName="timeRemarks" />
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Ratio" FieldName="ratio" />
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Theoretical Price" FieldName="theoreticalPrice" />
                    <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Old Settlement Price" FieldName="oldSettlementPrice" />
                    <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Adjustment Factor" FieldName="adjustmentFactor" />
                </Columns>
                <SettingsPager AlwaysShowPager="True" />
                <Settings ShowGroupButtons="False"  ShowVerticalScrollBar="True" />
                <SettingsCustomizationWindow Height="100%" />
                <Styles>
                    <AlternatingRow Enabled="True" />
                    <Header CssClass="gridHeader" />
                </Styles>
                <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <SettingsPager PageSize="20" />
            </dx:ASPxGridView>  
        </div>        
    </div>
</asp:Content>
