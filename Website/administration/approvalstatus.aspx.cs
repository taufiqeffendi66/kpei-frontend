﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

using Common;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.administration
{
    public partial class approvalstatus : System.Web.UI.Page
    {
        private dtApproval[] aDMs;
        DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack && !IsCallback)
            {
                tbl = CreateTable();
                getData();
                GetTable(tbl);
                Session["tbl"] = tbl;

                //init dtChekerStatus
                dtChekerStatus.Items.Add("*", "");
                dtChekerStatus.Items.Add("Checked", "Checked");
                dtChekerStatus.Items.Add("Rejected", "Rejected");

                //init dtApprovalStatus
                dtApprovalStatus.Items.Add("*", "");
                dtApprovalStatus.Items.Add("Approved", "Approved");
                dtApprovalStatus.Items.Add("Rejected", "Rejected");

                //init dtTopic
                dtTopic.Items.Add("*", "");
                dtTopic.Items.Add("Add Balance Withdrawal", "Add Balance Withdrawal");
                dtTopic.Items.Add("Add Client Account", "Add Client Account");
                dtTopic.Items.Add("Add Contract Group", "Add Contract Group");
                dtTopic.Items.Add("Add Exercise", "Add Exercise");
                dtTopic.Items.Add("Add Member", "Add Member");
                dtTopic.Items.Add("Add Series Contract", "Add Series Contract");
                dtTopic.Items.Add("Edit Member", "Edit Member");
                dtTopic.Items.Add("Edit Series Contract", "Edit Series Contract");
                dtTopic.Items.Add("Edit System Parameter", "Edit System Parameter");
                dtTopic.Items.Add("Give Up", "Give Up");
                dtTopic.Items.Add("Remove Member", "Remove Member");
                dtTopic.Items.Add("Remove Series Contract", "Remove Series Contract");
                dtTopic.Items.Add("Take Up", "Take Up");
            }
            else
                tbl = (DataTable)Session["tbl"];

            gridView.DataSource = tbl;
            gridView.KeyFieldName = "reffNo";
            gridView.DataBind();
        }

        DataTable CreateTable()
        {
            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            table = new DataTable();
            table.Columns.Add("reffNo", typeof(String));
            table.Columns.Add("type", typeof(String));
            table.Columns.Add("makerDate", typeof(DateTime));
            table.Columns.Add("makerName", typeof(String));
            table.Columns.Add("makerStatus", typeof(String));
            table.Columns.Add("checkerDate", typeof(DateTime));
            table.Columns.Add("checkerName", typeof(String));
            table.Columns.Add("checkerStatus", typeof(String));
            table.Columns.Add("approvelDate", typeof(DateTime));
            table.Columns.Add("approvelName", typeof(String));
            table.Columns.Add("approvelStatus", typeof(String));
            table.Columns.Add("topic", typeof(String));
            table.Columns.Add("insertEdit", typeof(String));
            table.Columns.Add("idxTmp", typeof(String));
            table.Columns.Add("form", typeof(String));
            table.Columns.Add("status", typeof(String));
            table.Columns.Add("idTable", typeof(String));
            table.Columns.Add("memberId", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["reffNo"] };
            return table;
        }

        void GetTable(DataTable table)
        {
            //You can store a DataTable in the session state
            if (table.Rows.Count > 0)
                table.Clear();
            if (aDMs != null)
            {
                for (int n = 0; n < aDMs.Length; n++)
                {
                    table.Rows.Add(aDMs[n].reffNo,
                        aDMs[n].type,
                        aDMs[n].makerDate,
                        aDMs[n].makerName,
                        aDMs[n].makerStatus,
                        aDMs[n].checkerDate,
                        aDMs[n].checkerName,
                        aDMs[n].checkerStatus,
                        aDMs[n].approvelDate,
                        aDMs[n].approvelName,
                        aDMs[n].approvelStatus,
                        aDMs[n].topic,
                        aDMs[n].insertEdit,
                        aDMs[n].idxTmp,
                        aDMs[n].form,
                        aDMs[n].status,
                        aDMs[n].idTable,
                        aDMs[n].memberId);
                }
            }
        }

        private void getData()
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();//new ApprovalService();
            try
            {
                //aDMs = wsA.getApprovalByMemberId( aUserMember );
                if (aUserMember.Equals("kpei"))
                    aDMs = wsA.SearchApproval("", "", "A,RC,RA", "", "", "", "");
                else
                    aDMs = wsA.SearchApproval("", "", "A,RC,RA", aUserMember, "", "", "");

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            try
            {
                String cs = "";
                String aps = "";
                String topic = "";
                if (!dtChekerStatus.Text.Equals("*"))
                    cs = dtChekerStatus.Text;
                if (!dtApprovalStatus.Text.Equals("*") )
                    aps = dtApprovalStatus.Text;
                if (!dtTopic.Text.Equals("*"))
                    topic = dtTopic.Text;

                if (aUserMember.Equals("kpei"))
                    aDMs = wsA.SearchApproval(dtReffNo.Text, dtMakerDate.Text, "A,RC,RA", "", cs, aps, topic);
                else
                    aDMs = wsA.SearchApproval(dtReffNo.Text, dtMakerDate.Text, "A,RC,RA", aUserMember.ToString(), cs, aps, topic);
                GetTable(tbl);
                gridView.DataBind();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
        }
    }
}
