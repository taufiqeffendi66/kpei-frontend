﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export;
using DevExpress.Data;

using Common.wsUser;
using System.Data;
using Common;
using Common.wsUserGroupPermission;

namespace imq.kpei.administration
{
    public partial class user : System.Web.UI.Page
    {
        private dtUserSkd[] users;

        private DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            gridExport.FileName = "User-" + DateTime.Now.ToString(ImqSession.GetConfig("FileNameFormat"));

            if (!aUserMember.Equals("kpei"))
            {
                dtMemberId.Text = aUserMember;
                dtMemberId.Enabled = false;
            }

            if (!IsPostBack && !IsCallback)
            {
                if (!aUserLogin.Equals("") && !aUserMember.Equals(""))
                    Permission();
                tbl = CreateTable();
                putData();
                //getData();
                //GetTable(tbl);
                //Session["tbl"] = tbl;
            }
            else
                tbl = (DataTable)Session["tbl"];
            gridView.KeyFieldName = "userId;memberId";
            gridView.DataSource = tbl;
            gridView.DataBind();
        }

        private void putData()
        {
            getData();
            GetTable(tbl);
            Session["tbl"] = tbl;
        }

        DataTable CreateTable( )
        {
            DataTable table = new DataTable();
            table.Columns.Add("userId", typeof(String));
            table.Columns.Add("memberId", typeof(String));
            table.Columns.Add("password", typeof(String));
            //table.Columns.Add("groupName", typeof(String));
            //table.Columns.Add("dateActive", typeof(DateTime));
            //table.Columns.Add("dateExpired", typeof(DateTime));
            //table.Columns.Add("passExpired", typeof(DateTime));
            table.Columns.Add("status", typeof(String));
            DataColumn[] keyColumn = new DataColumn[2];
            keyColumn[0] = table.Columns["userId"];
            keyColumn[1] = table.Columns["memberId"];
            table.PrimaryKey = keyColumn;
            //table.PrimaryKey = new DataColumn[] { table.Columns["userId"], table.Columns["memberId"] };
            return table;
        }


        void GetTable(DataTable table)
        {
            //You can store a DataTable in the session state
            if (table.Rows.Count > 0)
                table.Clear();
            if ( users != null )
                for (int n = 0; n < users.Length; n++)
                    table.Rows.Add(
                        users[n].id.userId,
                        users[n].id.memberId,
                        users[n].password,
                        users[n].status);
        }

        private void getData()
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                if ( aUserMember.Equals("kpei") )
                    users = wsU.View();
                else
                    users = wsU.ListByMember(aUserMember);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        DateTime StrToDateTime(String str)
        {
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            MyDateTime = Convert.ToDateTime(str);
            return MyDateTime;
        }

        protected void gvUser_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = users;
        }
        
        protected void btnNew_Click(object sender, EventArgs e)
        {
            //Session["stat"] = "0";
            UserService wsU = ImqWS.GetUserService();
            Response.Redirect("~/administration/useredit.aspx?stat=0&pinCode=" + wsU.getPinCode());
        }
/*
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            Session["stat"] = "1";
            Session["row"] = row;
            Response.Redirect("~/administration/useredit.aspx");

            UserService wsU = ImqWS.GetUserService();
            dtUserSkd[] aDM;// = new dtUserSkd();
            aDM = wsU.SearchByUserAndMember(row["userId"].ToString(), row["memberId"].ToString());
            if (aDM != null)
            {
                aDM[0].password = "";
                wsU.UpdateMain(aDM[0]);
            }

        }
*/
        protected void btnBlocked_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            if (row["status"].ToString().Equals("Active"))
            {
                UserService wsU = ImqWS.GetUserService();
                wsU.BlockedUser(row["memberId"].ToString(), row["userId"].ToString());
                putData();
                gridView.DataBind();
            }
            else
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "alert('User Status Not Active')", true);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            if (row["status"].ToString().Equals("Blocked"))
            {
                UserService wsU = ImqWS.GetUserService();
                String pinCode = wsU.Reset(row["userId"].ToString(), row["memberId"].ToString());
                //Session["stat"] = "5";
                //Session["row"] = row;


                string strRedirect = "~/administration/useredit.aspx?stat=5&" +
                    "pinCode=" + pinCode + "&" +
                    "userId=" + Server.UrlEncode(row["userId"].ToString()) + "&" +
                    "memberId=" + Server.UrlEncode(row["memberId"].ToString());


                //Response.Redirect("~/administration/useredit.aspx?pinCode=" + pinCode);
                Response.Redirect(strRedirect, false);
                Context.ApplicationInstance.CompleteRequest();
            }
            else
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "alert('User Status Not Blocked')", true);

        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            if (row["status"].ToString().Equals("Blocked"))
            {
                UserService wsU = ImqWS.GetUserService();
                wsU.RemoveByUserIdAndMemberId(row["userId"].ToString(), row["memberId"].ToString());
                putAuditTrail(aUserLogin, "User", "Reset User", "UserId = " + row["userId"].ToString() + ", memberId = " + row["memberId"].ToString());
                putData();
                gridView.DataBind();
            }
            else
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "alert('User Status Not Blocked')", true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                if (dtUserId.Text == "" && dtMemberId.Text == "")
                    users = wsU.View();
                else
                    users = wsU.SearchUserSKD(dtMemberId.Text, dtUserId.Text);

                GetTable(tbl);
                gridView.DataBind();

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        private void putAuditTrail(String aUser, String aModule, String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", aActifity, aMemo);
        }

        protected void btnPdfExport_Click(object sender, EventArgs e)
        {
            gridExport.WritePdfToResponse();
        }
        protected void btnXlsExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsToResponse();
        }
        protected void btnRtfExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteRtfToResponse();
        }
        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteCsvToResponse();
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            enableBtn(false);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "User");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                //                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                //                {
                //                    enableBtn(true);
                //                }
                if (!btnNew.Enabled && dtFMP[n].editMaker)
                    btnNew.Enabled = dtFMP[n].editMaker;

                if (!btnBlocked.Enabled && dtFMP[n].editChecker)
                    btnBlocked.Enabled = dtFMP[n].editChecker;

                if (!btnReset.Enabled && dtFMP[n].editApproval)
                    btnReset.Enabled = dtFMP[n].editApproval;

                if (!btnRemove.Enabled && dtFMP[n].removeing)
                    btnRemove.Enabled = dtFMP[n].removeing;
            }
        }

        private void enableBtn(bool aEnable)
        {
            btnNew.Enabled = aEnable;
            btnBlocked.Enabled = aEnable;
            btnReset.Enabled = aEnable;
            btnRemove.Enabled = aEnable;
        }

    }
}