﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.wsUser;

namespace imq.kpei.administration
{
    public partial class useredit : System.Web.UI.Page
    {
        private dtUserSkd[] users;
        //private dtUserGroup[] dtUG;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow rows;
        private int stat;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (Request.QueryString["stat"] == null)
            {
                Response.Redirect("~/administration/user.aspx");
                stat = -1;
            }
            else
                stat = int.Parse(Request.QueryString["stat"].ToString());

            if (!IsPostBack)
            {
                InitPage();
                switch (stat)
                {
                    case 0:
                        lbltitle.Text = "USER New";
                        dtPinCode.Text = Request.QueryString["pinCode"].ToString();
                        btnOk.Visible = true;                        
                        break;

                    case 1:
                        
                        lbltitle.Text = "Edit USER";
                        btnOk.Visible = true;
                        dtUserId.Enabled = false;
                        dtMemberList.Enabled = false;
                        dtPinCode.Visible = false;
                        toEditor();
                        //FillCityCombo(dtMemberList.Text);                       
                        break;

                    case 2:
                        break;
/*
                    case 3: 
                        lbltitle.Text = "Edit User";
                        btnOk.Visible = false;
                        btnChecker.Visible = false;
                        UserService wsU = ImqWS.GetUserService();
                        try
                        {
                            users = wsU.SearchByID(Session["idx"].ToString() ); //SearchByID("_tmp", Session["idx"]);
                            if (users != null)
                                toEditor();
                        }
                        catch (TimeoutException te)
                        {
                            te.ToString();
                            wsU.Abort();
                            throw;
                        }
                        break;
 */
                    case 5 :
                        lbltitle.Text = "Reset USER";
                        btnOk.Visible = true;
                        btnCancel.Visible = false;
                        dtUserId.Enabled = false;
                        dtMemberList.Enabled = false;
                        //dtUserId.Text = Request.QueryString["userId"].ToString();
                        //dtMemberList.Text = Request.QueryString["memberId"].ToString();
                        dtPinCode.Text = Request.QueryString["pinCode"].ToString();
                        dtPinCode.Enabled = false;
                        toEditor();
                        break;
                    default:
                        break;
                }

            }
        }

        protected void InitPage()
        {
            btnOk.Visible = false;
            btnChecker.Visible = false;
            btnApprovel.Visible = false;

            UserService wsU = ImqWS.GetUserService();
            try
            {
                // get List Member
                dtMemberList.Items.Clear();
                dtMemberList.Items.Add(aUserMember, aUserMember);
                if (aUserMember == "kpei")
                {
                    dtMemberInfo[] dtML = wsU.getListMember();
                    //dtMemberList.Items.Add("kpei", "kpei");
                    //dtMemberList.Items.Add(aUserMember, aUserMember);
                    if (dtML != null)
                        for (int i = 0; i < dtML.Length; i++)
                            dtMemberList.Items.Add(dtML[i].memberId, dtML[i].memberId);
                }
                //else dtMemberList.Items.Add(aUserMember, aUserMember);

                dtMemberList.SelectedIndex = 0;

                //FillCityCombo(dtMemberList.Text);

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        protected void toEditor()
        {
            dtUserId.Text = Request.QueryString["userId"].ToString();
            dtMemberList.Text = Request.QueryString["memberId"].ToString();
//            dtUserId.Text = rows["userId"].ToString();
//            dtMemberList.Text = rows["memberId"].ToString();
            //dtGroupName.Text = rows["groupId"].ToString();
            //dtPass.Text = rows["password"].ToString();
            //dtDataActive.Value = rows["dateActive"];//.ToString();
            //dtDataExpired.Value = rows["dateExpired"];//.ToString();
            //dtPasswordDataExpired.Value = rows["passExpired"];//.ToString();
            //dtStatus.Text = rows["status"].ToString();
        }

        protected void getSearch(String tbl, String userID, String memberID)
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                users = wsU.Search(userID, memberID);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            UserService wsU = ImqWS.GetUserService();
            //ApprovalService wsA = ImqWS.GetApprovalWebService();
            var mid = "";
            try
            {
                //int idRec;
                dtUserSkd aDM = new dtUserSkd();
                dtUserSkd aDMO = new dtUserSkd();
                if (aUserMember == "kpei" || aUserMember == "KPEI")
                {
                    mid = dtMemberList.Value.ToString();
                }
                else
                {
                    mid = Session["SESSION_USERMEMBER"].ToString();
                }

                dtUserSkdPK aUSMPK = new dtUserSkdPK() { userId = dtUserId.Text, memberId = mid };

                aDM.id = aUSMPK;
                aDM.pinCode = dtPinCode.Text;
                aDM.dateActive = DateTime.Now;
                aDM.dateActiveSpecified = true;
                aDM.dateExpired = DateTime.Now.AddYears(50);
                aDM.dateExpiredSpecified = true;
                aDM.passExpired = DateTime.Now.AddYears(50);
                aDM.passExpiredSpecified = true;
                aDM.status = "Active";
                aDM.clientType = aUSMPK.memberId.Equals("kpei") ? "0" : "1";
                aDM.loginStatus = "0";
                aDM.selector = "-";

                // Group
                dtUserGroupList dtUGL = new dtUserGroupList();
                dtUserGroupListPK dtUGLPK = new dtUserGroupListPK() { userId = dtUserId.Text, memberId = mid, groupId = "" };
                dtUGL.id = dtUGLPK;

                switch (stat)
                {
                    case 0 : // Add
                        int result = wsU.AddMain(aDM);
                        if (result == 1)
                        {
                            if (wsU.AddUserGroupList(dtUGL) == 1)
                            {
                                putAuditTrail("Add User", toString(aDM));
                                //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "alert('User Sucsessfully Added')", true);
                                ShowMessage("User Sucsessfully Added", @"../administration/user.aspx");
                            }
                            else
                            {
                                Session["stat"] = 0;
                                ShowMessage("User Not Sucsess Added", @"../administration/useredit.aspx?pinCode=" + dtPinCode.Text);
                            }

                        }
                        else
                        {
                            Session["stat"] = 0;
                            ShowMessage("User and Member Available", @"../administration/useredit.aspx?pinCode=" + dtPinCode.Text);
                        }
                        break;
                
                    case 1 : //Edit
                        dtUserSkd[] aDMs = wsU.SearchByUserAndMember(dtUserId.Text, mid);
                        aDMO = aDMs[0];
                        if (wsU.UpdateMain(aDM))
                        {
                            putAuditTrail("Edit User", String.Format("{0} To {1}", toString(aDMO), toString(aDM)));
                            ShowMessage("User Sucsessfully Edited", @"../administration/user.aspx");
                        }
                        else
                            ShowMessage("User Edit Error", @"../administration/user.aspx");
                        break;

                    case 2 :
                        Response.Redirect("~/account/approval.aspx");
                        break;

                    case 5 :
                        Response.Redirect("~/administration/user.aspx");
                        break;
                    default: break;
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();                
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            switch ( stat )
            {
                case 0:
                case 1:
                    Response.Redirect("~/administration/user.aspx");
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/account/approval.aspx");
                    break;
                default: break;
            }
        }

        private void putAuditTrail(String Actifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", Actifity, aMemo);
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {            
            switch ( stat )
            {
                case 0: 
                case 1: Response.Redirect("~/administration/user.aspx"); break;
                case 2: 
                case 3: Response.Redirect("~/account/approval.aspx"); break;
            }            
        }


        public static String toString(dtUserSkd aDM)
        {
            return String.Format("dtUserSkdMain [id={0}, dateActive={1}, dateExpired={2}, passExpired={3}, password={4}, status={5}, pinCode={6}, keyFile={7}]", aDM.id, String.Format("dd/MM/yyyy", aDM.dateActive), String.Format("dd/MM/yyyy", aDM.dateExpired), String.Format("dd/MM/yyyy", aDM.passExpired), aDM.password, aDM.status, aDM.pinCode, aDM.keyFile);
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        protected void btnApprovel_Click(object sender, EventArgs e)
        {

        }

    }
}
