﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="approvalmanagement.aspx.cs" Inherits="imq.kpei.administration.approvalmanagement" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">APPROVAL MANAGEMENT</div>
        <div>
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" 
                runat="server" AutoGenerateColumns="False" KeyFieldName="ID" Width="500px" 
                oncelleditorinitialize="gridView_CellEditorInitialize" 
                onrowupdating="gridView_RowUpdating">
                <Columns>
                    <dx:GridViewCommandColumn VisibleIndex="0">
                        <EditButton Visible="True"></EditButton>
                        <ClearFilterButton Visible="True"></ClearFilterButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" VisibleIndex="1" FieldName="ID" Visible="False"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Form ID" VisibleIndex="2" FieldName="formId" Visible="False"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Form Type ID" VisibleIndex="3" FieldName="formTypeId" Visible="False"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="4" FieldName="name" ReadOnly="True"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Form Type" VisibleIndex="5" FieldName="formType">
                        <PropertiesComboBox EnableSynchronization="False" IncrementalFilteringMode="StartsWith"></PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True"></SettingsBehavior>
                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="300" />
                <SettingsPager PageSize="15"></SettingsPager>                
                <SettingsEditing Mode="Inline"></SettingsEditing>
            </dx:ASPxGridView>
        </div>
   </div>

</asp:Content>
