﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userAdmin.aspx.cs" Inherits="imq.kpei.administration.userAdmin" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
     <script language="javascript" type="text/javascript">
        // <![CDATA[

         function OnGridFocusedRowChanged() {

             grid.GetRowValues(grid.GetFocusedRowIndex(), 'userId;memberId;', OnGetRowValues);
         }

         function OnGetRowValues(values) {
             clUserId.SetText(values[0]);
             clMemberId.SetText(values[1]);
         }
        // ]]>
    </script> 
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="USER" /></div>
        <div>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                <tr>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnPdfExport" runat="server" Text="Export to PDF" UseSubmitBehavior="False" OnClick="btnPdfExport_Click" />
                    </td>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnXlsExport" runat="server" Text="Export to XLS" UseSubmitBehavior="False" OnClick="btnXlsExport_Click" />
                    </td>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnXlsxExport" runat="server" Text="Export to XLSX" UseSubmitBehavior="False" OnClick="btnXlsxExport_Click" />
                    </td>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnRtfExport" runat="server" Text="Export to RTF" UseSubmitBehavior="False" OnClick="btnRtfExport_Click" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnCsvExport" runat="server" Text="Export to CSV" UseSubmitBehavior="False" OnClick="btnCsvExport_Click" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="User Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtUserId" ClientInstanceName="clUserId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td></td>
                </tr><tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Member Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtMemberId" ClientInstanceName="clMemberId" runat="server" Width="170px"> </dx:ASPxTextBox></td>
                    <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" 
                            onclick="btnSearch_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click"> </dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click"> </dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnRemove" runat="server" Text="Remove" 
                            onclick="btnRemove_Click"> </dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" Width="500px">                
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="User ID" FieldName="userId"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Member ID" FieldName="memberId"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Group" FieldName="groupName"></dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Date" VisibleIndex="3">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Active" FieldName="dateActive" ShowInCustomizationForm="True"><PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn VisibleIndex="1" Caption="Expired" FieldName="dateExpired" ShowInCustomizationForm="True"><PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn VisibleIndex="2" Caption="Pasword Expired" FieldName="passExpired" ShowInCustomizationForm="True"><PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataDateColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Status" FieldName="status"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="ID" FieldName="ID" Visible="False"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowSelectByRowClick="True" AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />                
            </dx:ASPxGridView>
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView"> </dx:ASPxGridViewExporter>
        </div>        
    </div>


</asp:Content>
