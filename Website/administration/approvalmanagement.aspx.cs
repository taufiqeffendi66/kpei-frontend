﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Web.Configuration;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxClasses;

using Common;
//using Common.wsUser;
using Common.wsApprovalManagement;
using Common.wsAuditTrail;

namespace imq.kpei.administration
{
    public partial class approvalmanagement : System.Web.UI.Page
    {
        private dtApprovalManagement[] approvalmanagenets;
        DataTable tbl = null;
        dtFormType[] dtFTs;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack && !IsCallback)
            {
                tbl = GetTable();
                Session["tbl"] = tbl;
            }
            else
                tbl = (DataTable)Session["tbl"];

            gridView.DataSource = tbl;
            gridView.KeyFieldName = "ID";
            gridView.DataBind();
        }

        DataTable GetTable()
        {
            getData();

            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            table = new DataTable();
            table.Columns.Add("ID", typeof(String));
            table.Columns.Add("formId", typeof(String));
            table.Columns.Add("formTypeId", typeof(String));
            table.Columns.Add("name", typeof(String));
            table.Columns.Add("formType", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["ID"] };
            for (int n = 0; n < approvalmanagenets.Length; n++)
            {
                table.Rows.Add(approvalmanagenets[n].id,
                    approvalmanagenets[n].formList.formId,
                    approvalmanagenets[n].formType.id,
                    approvalmanagenets[n].formList.name,
                    approvalmanagenets[n].formType.formType);
            }

            // get data Form Type
            //FormTypeService wsAM = new FormTypeService();
            //dts = wsAM.View();
            //tblFormType = new DataTable();
            //tblFormType.Columns.Add("formId", typeof(String));
            //tblFormType.Columns.Add("formType", typeof(String));
            //tblFormType.PrimaryKey = new DataColumn[] { tblFormType.Columns["ID"] };
            //for (int n = 0; n < dts.Length; n++)
            //    tblFormType.Rows.Add(dts[n].ID, dts[n].formType);

            return table;

        }

        private void getData()
        {
            ApprovalManagementService wsAM = ImqWS.GetApprovalManagementWebService();// CreateWebService();
            try
            {
                approvalmanagenets = wsAM.View();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsAM.Abort();
                throw;
            }

        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            Session["stat"] = "1";
            Session["id"] = row["ID"].ToString();
            Response.Redirect("~/administration/usergroupedit.aspx");
        }

        protected void gridView_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (!gridView.IsEditing || e.Column.FieldName != "formType")
                return;
            if (e.KeyValue == DBNull.Value || e.KeyValue == null)
                return;
            object val = gridView.GetRowValuesByKeyValue(e.KeyValue, "formType");
            if (val == DBNull.Value)
                return;
            string formType = (string)val;
            ASPxComboBox combo = e.Editor as ASPxComboBox;
            FillFormTypeCombo(combo, formType);
        }

        protected void FillFormTypeCombo(ASPxComboBox cmb, string formtype)
        {
            if (string.IsNullOrEmpty(formtype))
                return;
            if (dtFTs == null)
            {
                ApprovalManagementService wsAM = ImqWS.GetApprovalManagementWebService();
                dtFTs = wsAM.GetListFormType();
            }
            cmb.Items.Clear();
            for (int n = 0; n < dtFTs.Length; n++)
                cmb.Items.Add(dtFTs[n].formType);
        }

        protected void gridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridView = (ASPxGridView)sender;
            DataRow row = tbl.Rows.Find(e.Keys[0]);
            IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
            enumerator.Reset();

            while (enumerator.MoveNext())
            {
                row[enumerator.Key.ToString()] = enumerator.Value;
            }
            // save nya blom :D
            ApprovalManagementService wsAM = ImqWS.GetApprovalManagementWebService();
            wsAM.UpdateFormTypeById( int.Parse(row["ID"].ToString()) , e.NewValues["formType"].ToString());
            gridView.CancelEdit();
            e.Cancel = true;
        }

        private void putAuditTrail(String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "Approval Management", aActifity, aMemo);
            //dtAuditTrail dtAT = new dtAuditTrail();
            //dtAT.userSkd = aUser;
            //dtAT.module = aModule;
            //dtAT.actifity = aActifity;
            //dtAT.memo = aMemo;

            //AuditTrailService wsAT = ImqWS.GetAuditTrailService(); //new wsAuditTrail.AuditTrailService();
            //wsAT.AddData(dtAT);
        }
    }
}
