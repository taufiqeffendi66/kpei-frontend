﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Common;
using Common.contractWS;
using Common.wsUserGroupPermission;
using Common.wsTransaction;
using log4net;
using log4net.Config;

namespace imq.kpei.administration
{
    public partial class MultiplierTemplate : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(_Default));
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                if (!aUserMember.Equals("kpei"))
                {
                    //FileUpload1.Visible = false;
                    //btnUpload.Visible = false;
                }
                gvTemplateList.DataBind();
                //FillUnderlying();
                Permission();
            }
        }
        private void Permission()
        {
            log.Info("Permission");
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Contract Template");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                log.Debug("Edit Maker : " + dtFMP[n].editMaker);
                log.Debug("Edit Checker : " + dtFMP[n].editChecker);
                log.Debug("Edit Approval : " + dtFMP[n].editApproval);
                log.Debug("Edit Direct Checker : " + dtFMP[n].editDirectChecker);
                log.Debug("Edit Direct Approval : " + dtFMP[n].editDirectApproval);
                log.Debug("Removing : " + dtFMP[n].removeing);
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }
        private void EnableButton(bool enable)
        {
            log.Info("EnableButton : " + enable);
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        private templateParamMessage[] getTemplate()
        {
            log.Info("getTemplate");
            UiDataService ss = null;
            templateParamMessage[] listSeries = null;
            try
            {                
                ss = ImqWS.GetTransactionService();
                listSeries = ss.getTemplateParams();
                //cmbCode.DataSource = listSeries;
                //cmbCode.DataBind();
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                Debug.WriteLine(e.Message);
                if(ss != null)
                    ss.Abort();                
            }
            finally
            {
                if(ss != null)
                    ss.Dispose();
            }
            return listSeries;
        }

        protected void gvTemplateList_DataBinding(object sender, EventArgs e)
        {
            log.Info("TemplateList DataBinding");
            gvTemplateList.DataSource = getTemplate();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            log.Info("btnEdit_Click : SendResponse 1");
            SendResponse("1");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("multiplierTemplateInput.aspx?stat=0", false);
            log.Info("btnNew_Click : multiplierTemplateInput.aspx?stat=0");
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string templateId = cmbId.Text.Trim();
            string templateName = cmbName.Text.Trim();
            string search = String.Empty;

            if (templateId != String.Empty)
            {
                templateId = templateId.Replace("'", "''");
                templateId = templateId.Replace("--", "-");

                templateId = String.Format("[id] like '%{0}%'", templateId);
            }

            if (templateName != String.Empty)
            {
                templateName = templateName.Replace("'", "''");
                templateName = templateName.Replace("--", "-");

                templateName = String.Format("[templateName] like '%{0}%'", templateName);
            }

            if (templateId != String.Empty)
            {
                if (templateName != String.Empty)
                    search = String.Format("{0} and {1}", templateId, templateName);
                else
                    search = templateId;
            }
            else
            {
                if (templateName != String.Empty)
                    search = templateName;
                else
                    search = String.Empty;
            }
            gvTemplateList.FilterExpression = search;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }

        private void SendResponse(string stat)
        {
            log.Info("SendResponse : " + stat);
            try
            {
                string id = hfTemplate.Get("id").ToString().Trim();
                string templateName = hfTemplate.Get("templateName").ToString().Trim();
                string gfFee = hfTemplate.Get("gfFee").ToString().Trim();
                string clearingFeeType = hfTemplate.Get("clearingFeeType").ToString().Trim();
                string clearingFeeCalculation = hfTemplate.Get("clearingFeeCalculation").ToString().Trim();
                string clearingFeeValue = hfTemplate.Get("clearingFeeValue").ToString().Trim();
                string minimumFee = hfTemplate.Get("minimumFee").ToString().Trim();
                string multiplier = hfTemplate.Get("multiplier").ToString().Trim();
                string marginType = hfTemplate.Get("marginType").ToString().Trim();
                string marginValue = hfTemplate.Get("marginValue").ToString().Trim();
                
                string hph = hfTemplate.Get("hph") != null ?  hfTemplate.Get("hph").ToString().Trim() : "0";
                string underlying = hfTemplate.Get("underlying").ToString().Trim();
                string tickSize = hfTemplate.Get("tickSize") != null ? hfTemplate.Get("tickSize").ToString().Trim() : "0";
                string startTradingHour = hfTemplate.Get("startTradingHour").ToString().Trim();
                string endTradingHour = hfTemplate.Get("endTradingHour").ToString().Trim();
                string samplingHphType = hfTemplate.Get("samplingHphType").ToString().Trim();
                string startSamplingHph = hfTemplate.Get("startSamplingHph").ToString().Trim();
                string endSamplingHph = hfTemplate.Get("endSamplingHph").ToString().Trim();
                string tickValue = hfTemplate.Get("tickValue") != null ? hfTemplate.Get("tickValue").ToString().Trim() : "0";
              
                string strRedirect = String.Format("multiplierTemplateInput.aspx?stat={0}&id={1}&templateName={2}&gfFee={3}&clearingFeeType={4}&clearingFeeCalculation={5}&clearingFeeValue={6}&minimumFee={7}&multiplier={8}&marginType={9}&marginValue={10}&hph={11}&underlying={12}&tickSize={13}&startTradingHour={14}&endTradingHour={15}&samplingHphType={16}&startSamplingHph={17}&endSamplingHph={18}&tickValue={19}",
                    stat, Server.UrlEncode(id), Server.UrlEncode(templateName), Server.UrlEncode(gfFee), Server.UrlEncode(clearingFeeType), Server.UrlEncode(clearingFeeCalculation), Server.UrlEncode(clearingFeeValue), Server.UrlEncode(minimumFee), Server.UrlEncode(multiplier), Server.UrlEncode(marginType), Server.UrlEncode(marginValue), Server.UrlEncode(hph), Server.UrlEncode(underlying), Server.UrlEncode(tickSize), 
                    Server.UrlEncode(startTradingHour), Server.UrlEncode(endTradingHour), Server.UrlEncode(samplingHphType), Server.UrlEncode(startSamplingHph), Server.UrlEncode(endSamplingHph),Server.UrlEncode(tickValue));

                log.Debug("strRedirect : " + strRedirect);
                Response.Redirect(strRedirect, false);
                Context.ApplicationInstance.CompleteRequest();
            }
            catch (NullReferenceException nre)
            {
                Debug.WriteLine(nre.Message);
                log.Error(nre.Message);
            }catch(Exception e){
                log.Error(e.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            log.Info("BtnDelete_Click : Send Response 4");
            SendResponse("4");
        }

        
    }
}
