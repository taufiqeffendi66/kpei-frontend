﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

using Common;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.account
{
    public partial class approval : System.Web.UI.Page
    {
        private dtApproval[] aDMs;
        DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            //if (!string.IsNullOrEmpty(Request.Params["formId"]))
            //{
            //    enableDetail(Request.Params["formId"]);
            //}


            if (!IsPostBack && !IsCallback)
            {
                //btnDetail.Enabled = false;
                //Permission();
                tbl = CreateTable();
                getData();
                GetTable(tbl);
                Session["tbl"] = tbl;

                //init dtChekerStatus
                if (dtChekerStatus != null)
                {
                    dtChekerStatus.Items.Add("*", "");
                    dtChekerStatus.Items.Add("Checked", "Checked");
                    dtChekerStatus.Items.Add("To Be Checked", "To Be Checked");
                }

                //init dtApprovalStatus
                if (dtApprovalStatus != null)
                {
                    dtApprovalStatus.Items.Add("*", "");
                    dtApprovalStatus.Items.Add("To Be Approved", "To Be Approved");
                }

                //init dtTopic
                if (dtTopic != null)
                {
                    dtTopic.Items.Add("*", "");
                    dtTopic.Items.Add("Add Balance Withdrawal", "Add Balance Withdrawal");
                    dtTopic.Items.Add("Add Client Account", "Add Client Account");
                    dtTopic.Items.Add("Add Contract Group", "Add Contract Group");
                    dtTopic.Items.Add("Add Exercise", "Add Exercise");
                    dtTopic.Items.Add("Add Member", "Add Member");
                    dtTopic.Items.Add("Add Series Contract", "Add Series Contract");
                    dtTopic.Items.Add("Edit Member", "Edit Member");
                    dtTopic.Items.Add("Edit Series Contract", "Edit Series Contract");
                    dtTopic.Items.Add("Edit System Parameter", "Edit System Parameter");
                    dtTopic.Items.Add("Give Up", "Give Up");
                    dtTopic.Items.Add("Remove Member", "Remove Member");
                    dtTopic.Items.Add("Remove Series Contract", "Remove Series Contract");
                    dtTopic.Items.Add("Take Up", "Take Up");
                }
            }
            else tbl = (DataTable)Session["tbl"];

            gridView.DataSource = tbl;
            gridView.KeyFieldName = "reffNo";
            gridView.DataBind();
        }

        void enableDetail(string id)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);

            dtFormMenuPermission[] dtFMP;
            btnDetail.Enabled = false;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndFormId(aUserLogin, aUserMember, id);
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editChecker || dtFMP[n].editApproval)
                {
                    btnDetail.Enabled = true;
                }
            }
        }

        DataTable CreateTable()
        {
            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            table = new DataTable();
            table.Columns.Add("reffNo", typeof(String));
            table.Columns.Add("type", typeof(String));
            table.Columns.Add("makerDate", typeof(DateTime));
            table.Columns.Add("makerName", typeof(String));
            table.Columns.Add("makerStatus", typeof(String));
            table.Columns.Add("checkerDate", typeof(DateTime));
            table.Columns.Add("checkerName", typeof(String));
            table.Columns.Add("checkerStatus", typeof(String));
            table.Columns.Add("approvelDate", typeof(DateTime));
            table.Columns.Add("approvelName", typeof(String));
            table.Columns.Add("approvelStatus", typeof(String));
            table.Columns.Add("topic", typeof(String));
            table.Columns.Add("insertEdit", typeof(String));
            table.Columns.Add("idxTmp", typeof(String));
            table.Columns.Add("form", typeof(String));
            table.Columns.Add("status", typeof(String));
            table.Columns.Add("idTable", typeof(String));
            table.Columns.Add("memberId", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["reffNo"] };
            return table;
        }

        void GetTable(DataTable table)
        {
            //You can store a DataTable in the session state
            if (table.Rows.Count > 0) table.Clear();
            if (aDMs != null)
            {
                for (int n = 0; n < aDMs.Length; n++)
                {
                    table.Rows.Add(aDMs[n].reffNo,
                        aDMs[n].type,
                        aDMs[n].makerDate,
                        aDMs[n].makerName,
                        aDMs[n].makerStatus,
                        aDMs[n].checkerDate,
                        aDMs[n].checkerName,
                        aDMs[n].checkerStatus,
                        aDMs[n].approvelDate,
                        aDMs[n].approvelName,
                        aDMs[n].approvelStatus,
                        aDMs[n].topic,
                        aDMs[n].insertEdit,
                        aDMs[n].idxTmp,
                        aDMs[n].form,
                        aDMs[n].status,
                        aDMs[n].idTable,
                        aDMs[n].memberId);
                }
            }
            else btnDetail.Enabled = false;
        }

        private void getData()
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();//new ApprovalService();
            try
            {
                //aDMs = wsA.getApprovalByMemberId( aUserMember );
                if (aUserMember.Equals("kpei") )
                    aDMs = wsA.SearchApproval("", "", "M,C", "", "", "", "");
                else
                    aDMs = wsA.SearchApproval("", "", "M,C", aUserMember, "", "", "");

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
        }

        
        protected void btnChecker_Click(object sender, EventArgs e)
        {
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            //
        }

        protected void btnDetail_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            try
            {
                //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)))
                //{
                    //Session["stat"] = 2;
                    //Session["row"] = row;
                    //Response.Redirect(row["form"].ToString());
                    string strRedirect = row["form"].ToString()+"?stat=2&" +
                        "reffNo=" + Server.UrlEncode(row["reffNo"].ToString()) + "&" +
                        "type=" + Server.UrlEncode(row["type"].ToString()) + "&" +
                        "makerDate=" + Server.UrlEncode(row["makerDate"].ToString()) + "&" +
                        "makerName=" + Server.UrlEncode(row["makerName"].ToString()) + "&" +
                        "makerStatus=" + Server.UrlEncode(row["makerStatus"].ToString()) + "&" +
                        "checkerDate=" + Server.UrlEncode(row["checkerDate"].ToString()) + "&" +
                        "checkerName=" + Server.UrlEncode(row["checkerName"].ToString()) + "&" +
                        "checkerStatus=" + Server.UrlEncode(row["checkerStatus"].ToString()) + "&" +
                        "approvelDate=" + Server.UrlEncode(row["approvelDate"].ToString()) + "&" +
                        "approvelName=" + Server.UrlEncode(row["approvelName"].ToString()) + "&" +
                        "approvelStatus=" + Server.UrlEncode(row["approvelStatus"].ToString()) + "&" +
                        "topic=" + Server.UrlEncode(row["topic"].ToString()) + "&" +
                        "insertEdit=" + Server.UrlEncode(row["insertEdit"].ToString()) + "&" +
                        "idxTmp=" + Server.UrlEncode(row["idxTmp"].ToString()) + "&" +
                        "form=" + Server.UrlEncode(row["form"].ToString()) + "&" +
                        "status=" + Server.UrlEncode(row["status"].ToString()) + "&" +
                        "idTable=" + Server.UrlEncode(row["idTable"].ToString()) + "&" +
                        "memberId=" + Server.UrlEncode(row["memberId"].ToString());

                    Response.Redirect(strRedirect, false);
                    Context.ApplicationInstance.CompleteRequest();

                //}
            }
            catch (TimeoutException te)
            {
                te.ToString();
                throw;
            }
        }

        private void Permission()
        {
            //DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);

            //dtFormMenuPermission[] dtFMP;
            //btnDetail.Enabled = false;
            //UserGroupPermissionService wsUGP = new UserGroupPermissionService();
            //wsUGP.Url = GetConfig("WS_SERVER") + "/wsUserGroupPermission/UserGroupPermission";
            //dtFMP = wsUGP.getPermissionByUserAndMemberAndFormId(aUserLogin, aUserMember, row["formId"].ToString());
            //for (int n = 0; n < dtFMP.Length; n++)
            //{
            //    if (dtFMP[n].editChecker || dtFMP[n].editApproval)
            //    {
            //        btnDetail.Enabled = true;
            //    }
            //}
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            try
            {
                String cs = "";
                String aps = "";
                String topic = "";
                if (!dtChekerStatus.Text.Equals("*"))
                    cs = dtChekerStatus.Text;
                if (!dtApprovalStatus.Text.Equals("*"))
                    aps = dtApprovalStatus.Text;
                if (!dtTopic.Text.Equals("*"))
                    topic = dtTopic.Text;

                if (aUserMember.Equals("kpei"))
                    aDMs = wsA.SearchApproval(dtReffNo.Text, dtMakerDate.Text, "M,C", "", cs, aps, topic);
                else
                    aDMs = wsA.SearchApproval(dtReffNo.Text, dtMakerDate.Text, "M,C", aUserMember.ToString(), cs, aps, topic);
                GetTable(tbl);
                gridView.DataBind();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
        }

        protected void dtChekerStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}