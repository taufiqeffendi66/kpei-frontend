﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="groupedit.aspx.cs" Inherits="imq.kpei.administration.groupedit" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        function onCellClick(rowIndex, fieldName) {
            ASPxGridView1.PerformCallback(rowIndex + "|" + fieldName);
        }
        function OnEditorKeyPress(editor, e) {
            if (e.htmlEvent.keyCode == 13 || e.htmlEvent.keyCode == 9) {
                ASPxGridView1.UpdateEdit();
            }
            else
                if (e.htmlEvent.keyCode == 27)
                    ASPxGridView1.CancelEdit();
        }
    </script>
    <div class="content">
        <div class="title">GROUP EDIT</div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Member Name"></dx:ASPxLabel></td>
                    <td><dx:ASPxComboBox ID="dtMemberList" ClientInstanceName="clMemberId" runat="server"></dx:ASPxComboBox></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Group Name"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtGroupId" ClientInstanceName="clGroupId" runat="server" Width="170px"></dx:ASPxTextBox></td>                    
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Description"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtDes" ClientInstanceName="clDes" runat="server" Width="170px"></dx:ASPxTextBox></td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxGridView ID="gvPermision" ClientInstanceName="grid" runat="server" 
                        AutoGenerateColumns="False" EnableRowsCache="False" Width="670px" 
                        onrowupdating="gvPermision_RowUpdating" 
                            oncustomcallback="gvPermision_CustomCallback">
                            <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True"></SettingsBehavior>
                            <Columns>
                                <dx:GridViewDataTextColumn VisibleIndex="1" Caption="ID" FieldName="Idx" ReadOnly="True" Visible="False"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Menu" FieldName="menuName" GroupIndex="0" SortIndex="0" SortOrder="Ascending"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>                    
                                <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Form Name" FieldName="formName" ReadOnly="True"  Width="150px" ><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn VisibleIndex="4" Caption="Read" FieldName="reading"><HeaderStyle HorizontalAlign="Center"/><DataItemTemplate><dx:ASPxCheckBox ID="checkBox" Width="40px" runat="server" Value='<%# Eval("reading")%>'></dx:ASPxCheckBox></dxe:ASPxTextBox></DataItemTemplate></dx:GridViewDataCheckColumn>
                                <dx:GridViewBandColumn VisibleIndex="5" Caption="Edit">
                                    <Columns>
                                        <dx:GridViewDataCheckColumn VisibleIndex="0" Caption="Maker" FieldName="editMaker" Width="50px"><HeaderStyle HorizontalAlign="Center" /><DataItemTemplate><dx:ASPxCheckBox ID="checkBox" Width="100%" runat="server" Value='<%# Eval("editMaker")%>'></dx:ASPxCheckBox></dxe:ASPxTextBox></DataItemTemplate></dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn VisibleIndex="1" Caption="Checker" FieldName="editChecker" Width="60px"><HeaderStyle HorizontalAlign="Center" /><DataItemTemplate><dx:ASPxCheckBox ID="checkBox" Width="100%" runat="server" Value='<%# Eval("editChecker")%>'></dx:ASPxCheckBox></dxe:ASPxTextBox></DataItemTemplate></dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn VisibleIndex="2" Caption="Approval" FieldName="editApproval" Width="60px"><HeaderStyle HorizontalAlign="Center" /><DataItemTemplate><dx:ASPxCheckBox ID="checkBox" Width="100%" runat="server" Value='<%# Eval("editApproval")%>'></dx:ASPxCheckBox></dxe:ASPxTextBox></DataItemTemplate></dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn VisibleIndex="3" Caption="Direct Checker" FieldName="editDirectChecker" Width="90px"><HeaderStyle HorizontalAlign="Center" /><DataItemTemplate><dx:ASPxCheckBox ID="checkBox" Width="100%" runat="server" Value='<%# Eval("editDirectChecker")%>'></dx:ASPxCheckBox></dxe:ASPxTextBox></DataItemTemplate></dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn VisibleIndex="4" Caption="Direct Approval" FieldName="editDirectApproval" Width="100px"><HeaderStyle HorizontalAlign="Center" /><DataItemTemplate><dx:ASPxCheckBox ID="checkBox" Width="100%" runat="server" Value='<%# Eval("editDirectApproval")%>'></dx:ASPxCheckBox></dxe:ASPxTextBox></DataItemTemplate></dx:GridViewDataCheckColumn>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewBandColumn>
                                <dx:GridViewDataCheckColumn VisibleIndex="6" Caption="Remove" FieldName="removeing" Width="70px"><HeaderStyle HorizontalAlign="Center" /><DataItemTemplate><dx:ASPxCheckBox ID="checkBox" Width="100%" runat="server" Value='<%# Eval("removeing")%>'></dx:ASPxCheckBox></dxe:ASPxTextBox></DataItemTemplate></dx:GridViewDataCheckColumn>
                            </Columns>
                            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AutoExpandAllGroups="True" />
                            <SettingsPager PageSize="50"></SettingsPager>                
                            <SettingsEditing Mode="Inline"></SettingsEditing>
                            <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="290" />
                        </dx:ASPxGridView>                    
                    </td>
                    <td><dx:ASPxButton ID="btnOneRow" runat="server" Text="Grant 1 Row" 
                            onclick="btnOneRow_Click"></dx:ASPxButton></td>
                </tr>
            </table>
        </div>       
        <div>
            <table>
                <tr>
                    <td><dx:ASPxButton ID="btnOk" runat="server" Text="Ok" onclick="btnOk_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="False"><ClientSideEvents Click="function(s, e) {grid.PerformCallback('post');}" /></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnAllRow" runat="server" Text="Grant All" onclick="btnAllRow_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnNext" runat="server" Text="Next" onclick="btnNext_Click"></dx:ASPxButton></td>                    
                </tr>
            </table>
            <table><tr><td>
                <asp:Label ID="lbNote" runat="server" Text="* for menu User : Maker = New, Checker = Blocked, Approval = Reset, Edit = DirectChecker, Remove = Delete"></asp:Label></td></tr></table> 
        </div>     
    </div>
</asp:Content>
