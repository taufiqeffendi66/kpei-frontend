﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="approvaleditlevel.aspx.cs" Inherits="imq.kpei.administration.approvaledit_Level" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <div id="content">
        <div class="title">SYSTEM PARAMETER EDIT</div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Form Name"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Description"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="71px" Width="170px"></dx:ASPxMemo></td>
                    <td></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Form Type"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="ASPxTextBox3" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Maker"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="ASPxTextBox4" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td><dx:ASPxButton ID="ASPxButton1" runat="server" Text="Add"></dx:ASPxButton></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Checker"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="ASPxTextBox5" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td><dx:ASPxButton ID="ASPxButton2" runat="server" Text="Add"></dx:ASPxButton></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Approver"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="ASPxTextBox6" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td><dx:ASPxButton ID="ASPxButton3" runat="server" Text="Add"></dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxButton ID="ASPxButton4" runat="server" Text="Ok"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="ASPxButton5" runat="server" Text="Cancel"></dx:ASPxButton></td>
                </tr>
            </table> 
        </div>

    </div>

</asp:Content>
