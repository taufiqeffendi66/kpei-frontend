﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Web.Configuration;

using DevExpress.Web.ASPxGridView;

using Common;
using Common.wsAuditTrail;
using Common.wsUserGroupPermission;
using Common.wsUser;
using DevExpress.Web.ASPxEditors;

namespace imq.kpei.administration
{
    public partial class groupedit : System.Web.UI.Page
    {
        //private dtUserGroupPermission[] dtUGP;
        //private int addedit;
        private DataTable tblMenu = null;
        private DataTable tblUGP = null;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow rows;
        private int stat;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            //            rows = (DataRow)Session["row"];

            if (Request.QueryString["stat"] == null)
            {
                Response.Redirect("~/administration/group.aspx");
                stat = -1;
            }
            else
                stat = int.Parse(Request.QueryString["stat"].ToString());


            if (!IsPostBack)
            {
                InitPage();

                switch (stat)
                {
                    case 0:
                        gvPermision.Visible = false;
                        btnOk.Visible = false;
                        btnAllRow.Visible = false;
                        btnOneRow.Visible = false;
                        btnSave.Visible = false;
                        lbNote.Visible = false;
                        btnNext.Visible = true;
                        break;

                    case 1:
                        btnNext.Visible = false;
                        if (!IsPostBack && !IsCallback)
                        {
                            dtMemberList.Text = Request.QueryString["memberId"].ToString();
                            dtGroupId.Text = Request.QueryString["groupId"].ToString();
                            dtMemberList.Enabled = false;
                            dtGroupId.Enabled = false;
                            dtDes.Text = Request.QueryString["des"].ToString();
                            tblUGP = GetTable(dtMemberList.Text, dtGroupId.Text);
                            Session["tblUGP"] = tblUGP;
                        }
                        else
                        {
                            tblUGP = (DataTable)Session["tblUGP"];
                            tblMenu = (DataTable)Session["tblMenu"];
                        }
                        break;
                }
            }
            else
                tblUGP = (DataTable)Session["tblUGP"];
            gvPermision.DataSource = tblUGP;
            gvPermision.KeyFieldName = "Idx";
            gvPermision.DataBind();
        }

        protected void InitPage()
        {

            UserService wsU = ImqWS.GetUserService();
            //wsU.Url = GetConfig("WS_SERVER") + "/wsUser/User";

            try
            {
                // get List Member
                dtMemberList.Items.Clear();
                if (aUserMember == "kpei")
                {
                    dtMemberInfo[] dtML;
                    dtML = wsU.getListMember();
                    if (dtML != null)
                    {
                        dtMemberList.Items.Add(aUserMember, aUserMember);
                        for (int i = 0; i < dtML.Length; i++)
                            dtMemberList.Items.Add(dtML[i].memberId, dtML[i].memberId);
                    }
                }
                else
                    dtMemberList.Items.Add(aUserMember, aUserMember);

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        DataTable GetTable(String memberId, String groupId )
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();// CreateWebService();
            try
            {
                dtFMP = wsUGP.getMenuByMemberIdAndGroupId(memberId, groupId);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsUGP.Abort();
                throw;
            }

            //getData(id);

            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            //table = new DataTable();
            table.Columns.Add("Idx", typeof(int));
            table.Columns.Add("memberId", typeof(String));
            table.Columns.Add("groupId", typeof(String));
            table.Columns.Add("formId", typeof(String));
            table.Columns.Add("formName", typeof(String));
            table.Columns.Add("reading", typeof(Boolean));
            table.Columns.Add("editMaker", typeof(Boolean));
            table.Columns.Add("editChecker", typeof(Boolean));
            table.Columns.Add("editApproval", typeof(Boolean));
            table.Columns.Add("editDirectChecker", typeof(Boolean));
            table.Columns.Add("editDirectApproval", typeof(Boolean));
            table.Columns.Add("removeing", typeof(Boolean));
            table.Columns.Add("menuName", typeof(String));
            //            table.PrimaryKey = new DataColumn[] { table.Columns["memberId"], table.Columns["groupId"], table.Columns["formId"] };
            table.PrimaryKey = new DataColumn[] { table.Columns["Idx"] };
            if (dtFMP != null)
                for (int n = 0; n < dtFMP.Length; n++)
                {
                    table.Rows.Add(dtFMP[n].idx,
                    dtFMP[n].memberId,
                    dtFMP[n].groupId,
                    dtFMP[n].formId,
                    dtFMP[n].formName,
                    dtFMP[n].reading,
                    dtFMP[n].editMaker,
                    dtFMP[n].editChecker,
                    dtFMP[n].editApproval,
                    dtFMP[n].editDirectChecker,
                    dtFMP[n].editDirectApproval,
                    dtFMP[n].removeing,
                    dtFMP[n].menuName );
                }
            return table;
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }
 
        protected String ChangeData(String dt)
        {
            if (dt == "True")
                return "1";
            else
                return "0";
        }

        protected void gvPermision_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridView = (ASPxGridView)sender;
            DataRow row = tblUGP.Rows.Find(new object[] { e.Keys[0], e.Keys[1], e.Keys[2] } );
            IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
            enumerator.Reset();

            while (enumerator.MoveNext())
            {
                row[enumerator.Key.ToString()] = enumerator.Value;
            }

            dtUserGroupPermissionPK dtUGPPK = new dtUserGroupPermissionPK();
            dtUGPPK.memberId = row["memberId"].ToString().Trim();
            dtUGPPK.groupId = row["groupId"].ToString().Trim();
            dtUGPPK.formId = int.Parse(row["formId"].ToString().Trim());

            dtUserGroupPermission aData = new dtUserGroupPermission();
            aData.id = dtUGPPK;
            aData.reading = (Boolean)e.NewValues["reading"];
            aData.editMaker = (Boolean)e.NewValues["editMaker"];
            aData.editChecker = (Boolean)e.NewValues["editChecker"];
            aData.editApproval = (Boolean)e.NewValues["editApproval"];
            aData.editDirectChecker = (Boolean)e.NewValues["editDirectChecker"];
            aData.editDirectApproval = (Boolean)e.NewValues["editDirectApproval"];
            aData.removeing = (Boolean)e.NewValues["removeing"];

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService(); //CreateWebService();
            wsUGP.UpdateById(aData);

            gridView.CancelEdit();
            e.Cancel = true;
            putAuditTrail("Updating", toString(aData));
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/administration/group.aspx");
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (!dtMemberList.Equals("") && !dtGroupId.Equals(""))
            {

                btnOk.Visible = true;
                btnNext.Visible = false;
                gvPermision.Visible = true;
                btnAllRow.Visible = true;
                btnOneRow.Visible = true;
                lbNote.Visible = true;
                btnSave.Visible = true;

                dtMemberList.Enabled = false;
                dtGroupId.Enabled = false;

                Common.wsUserGroupPermission.dtUserGroupPK dtUGPK = new Common.wsUserGroupPermission.dtUserGroupPK();
                dtUGPK.memberId = dtMemberList.Text;
                dtUGPK.groupId = dtGroupId.Text;

                Common.wsUserGroupPermission.dtUserGroup aUserGroup = new Common.wsUserGroupPermission.dtUserGroup();
                aUserGroup.id = dtUGPK;
                aUserGroup.des = dtDes.Text;
                UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();

                //int id = wsUGP.AddUserGroup(aUserGroup);
                if (wsUGP.AddUserGroup(aUserGroup) == 0)
                {
                    tblUGP = GetTable(dtMemberList.Text, dtGroupId.Text);
                    DataRow aRow;
                    for (int y = 0; y < tblUGP.Rows.Count; y++)
                    {
                        aRow = tblUGP.Rows[y];
                        if (aRow["menuName"].Equals("Administration"))
                            aRow["reading"] = false;
                    }
                    Session["tblUGP"] = tblUGP;
                    gvPermision.DataSource = tblUGP;
                    gvPermision.DataBind();

                    Session["stat"] = "1";
                }
                else
                {
                    Session["stat"] = "0";
                    //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Member and Group Available", true);
                    ShowMessage("Member and Group Available", @"../administration/groupedit.aspx");
                }
            }
        }

        public String toString(dtUserGroupPermission aData)
        {
            return "dtUserGroupPermission [memberId=" + aData.id.memberId
                + ", groupId=" + aData.id.groupId
                + ", formId=" + aData.id.formId
                + ", editApproval=" + aData.editApproval 
                + ", editChecker=" + aData.editChecker
                + ", editMaker=" + aData.editMaker 
                + ", editDirectApproval="+ aData.editDirectApproval 
                + ", editDirectChecker="+ aData.editDirectChecker 
                + ", reading=" + aData.reading 
                + ", removeing="+ aData.removeing + "]";
        }

        private void putAuditTrail(String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User Group Permission", aActifity, aMemo);
        }

        protected void btnAllRow_Click(object sender, EventArgs e)
        {
            AllRow(true);
        }

        protected void InitGrid()
        {

            for (int i = 0; i < gvPermision.VisibleRowCount; i++)
            {
                ASPxCheckBox cb01 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(i, gvPermision.Columns["reading"] as GridViewDataColumn, "checkBox");
                ASPxCheckBox cb02 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(i, gvPermision.Columns["editMaker"] as GridViewDataColumn, "checkBox");
                ASPxCheckBox cb03 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(i, gvPermision.Columns["editChecker"] as GridViewDataColumn, "checkBox");
                ASPxCheckBox cb04 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(i, gvPermision.Columns["editApproval"] as GridViewDataColumn, "checkBox");
                ASPxCheckBox cb05 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(i, gvPermision.Columns["editDirectChecker"] as GridViewDataColumn, "checkBox");
                ASPxCheckBox cb06 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(i, gvPermision.Columns["editDirectApproval"] as GridViewDataColumn, "checkBox");
                ASPxCheckBox cb07 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(i, gvPermision.Columns["removeing"] as GridViewDataColumn, "checkBox");
                int id = Convert.ToInt32(gvPermision.GetRowValues(i, gvPermision.KeyFieldName));
                if (cb01 != null)
                {
                    DataRow aRow = tblUGP.Rows.Find(id);
                    if (
                        ((Boolean)aRow["reading"] != cb01.Checked) ||
                        ((Boolean)aRow["editMaker"] != cb02.Checked) ||
                        ((Boolean)aRow["editChecker"] != cb03.Checked) ||
                        ((Boolean)aRow["editApproval"] != cb04.Checked) ||
                        ((Boolean)aRow["editDirectChecker"] != cb05.Checked) ||
                        ((Boolean)aRow["editDirectApproval"] != cb06.Checked) ||
                        ((Boolean)aRow["removeing"] != cb07.Checked))
                    {
                        aRow["reading"] = cb01.Checked;
                        aRow["editMaker"] = cb02.Checked;
                        aRow["editChecker"] = cb03.Checked;
                        aRow["editApproval"] = cb04.Checked;
                        aRow["editDirectChecker"] = cb05.Checked;
                        aRow["editDirectApproval"] = cb06.Checked;
                        aRow["removeing"] = cb07.Checked;

                        dtUserGroupPermissionPK dtUGPPK = new dtUserGroupPermissionPK();
                        dtUGPPK.memberId = aRow["memberId"].ToString().Trim();
                        dtUGPPK.groupId = aRow["groupId"].ToString().Trim();
                        dtUGPPK.formId = int.Parse(aRow["formId"].ToString().Trim());

                        dtUserGroupPermission aData = new dtUserGroupPermission();
                        aData.id = dtUGPPK;
                        aData.reading = cb01.Checked;
                        aData.editMaker = cb02.Checked;
                        aData.editChecker = cb03.Checked;
                        aData.editApproval = cb04.Checked;
                        aData.editDirectChecker = cb05.Checked;
                        aData.editDirectApproval = cb06.Checked;
                        aData.removeing = cb07.Checked;

                        UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
                        wsUGP.UpdateById(aData);

                    }
                    
                }
            }
        }

        protected void gvPermision_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (e.Parameters == "post")
            {
                InitGrid();
                gvPermision.DataBind();
                ShowMessage("msgGroupSave", "group.aspx");
            }
        }

        protected void btnOneRow_Click(object sender, EventArgs e)
        {
            int i = gvPermision.FocusedRowIndex;
            OneRow(i, true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            switch (stat)
            {
                case 0:
                    Response.Redirect("~/administration/group.aspx");
                    break;

                case 1:
                    //AllRow(false);
                    gvPermision.DataSource = tblUGP;
                    //gvPermision.KeyFieldName = "memberId;groupId;formId";
                    gvPermision.KeyFieldName = "Idx";
                    gvPermision.DataBind();
                    Response.Redirect("~/administration/group.aspx");
                    break;
            }

        }

        private void OneRow(int idx, bool check)
        {
            ASPxCheckBox cb01 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(idx, gvPermision.Columns["reading"] as GridViewDataColumn, "checkBox");
            ASPxCheckBox cb02 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(idx, gvPermision.Columns["editMaker"] as GridViewDataColumn, "checkBox");
            ASPxCheckBox cb03 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(idx, gvPermision.Columns["editChecker"] as GridViewDataColumn, "checkBox");
            ASPxCheckBox cb04 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(idx, gvPermision.Columns["editApproval"] as GridViewDataColumn, "checkBox");
            ASPxCheckBox cb05 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(idx, gvPermision.Columns["editDirectChecker"] as GridViewDataColumn, "checkBox");
            ASPxCheckBox cb06 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(idx, gvPermision.Columns["editDirectApproval"] as GridViewDataColumn, "checkBox");
            ASPxCheckBox cb07 = (ASPxCheckBox)gvPermision.FindRowCellTemplateControl(idx, gvPermision.Columns["removeing"] as GridViewDataColumn, "checkBox");
            if (cb01 != null)
            {
                cb01.Checked = check;
                cb02.Checked = check;
                cb03.Checked = check;
                cb04.Checked = check;
                cb05.Checked = check;
                cb06.Checked = check;
                cb07.Checked = check;
            }
        }

        private void AllRow(bool check)
        {
            gvPermision.ExpandAll();
            for (int i = 0; i < gvPermision.VisibleRowCount; i++)
            //for (int i = 0; i < tblUGP.Rows.Count; i++)
            {
                GridViewDataColumn column01 = gvPermision.Columns["reading"] as GridViewDataColumn;
                if (column01 != null)
                    try
                    {
                        OneRow(i, check);
                    }
                    catch (TimeoutException te)
                    {

                    }
            }
        }

    }
}