﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using Common;
using Common.wsUser;
using Common.wsApprovalManagement;
using Common.wsUserGroupPermission;

namespace imq.kpei.administration
{
    public partial class userGroup : System.Web.UI.Page
    {
        private dtUserGroupList[] userGroupList;
        private  Common.wsUser.dtUserGroup[] dtGroups;

        private DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (aUserMember != "kpei")
            {
                dtMemberId.Text = aUserMember;
                dtMemberId.Enabled = false;
            }

            if (!IsPostBack && !IsCallback)
            {
                if (!aUserLogin.Equals("") && !aUserMember.Equals(""))
                    Permission();
                getData();
                tbl = CreateTable();
                GetTable(tbl);
                Session["tbl"] = tbl;
            }
            else
                tbl = (DataTable)Session["tbl"];
            gridView.DataSource = tbl;
            gridView.KeyFieldName = "ID";
            gridView.DataBind();

        }

        DataTable CreateTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("memberId", typeof(String));
            table.Columns.Add("userId", typeof(String));
            table.Columns.Add("groupId", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["ID"] };
            //table.PrimaryKey = new DataColumn[] { table.Columns["memberId"], table.Columns["userId"], table.Columns["groupId"]  };
            return table;
        }

        void GetTable(DataTable table)
        {
            if (table.Rows.Count > 0)
                table.Clear();
            if (userGroupList != null)
                for (int n = 0; n < userGroupList.Length; n++)
                    table.Rows.Add( n+1,
                        userGroupList[n].id.memberId,
                        userGroupList[n].id.userId,
                        userGroupList[n].id.groupId);
        }

        private void getData()
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                if (aUserMember.Equals("kpei"))
                    userGroupList = wsU.getUserGroupList("", "", "");
                else
                    userGroupList = wsU.getUserGroupList(aUserMember, "", "");
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        private void putData()
        {
            getData();
            GetTable(tbl);
            Session["tbl"] = tbl;
        }

        protected void gridView_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
        {
            if (!gridView.IsEditing || e.Column.FieldName != "groupId")
                return;

            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            if (row != null)
                Session["row"] = row;
            else
                row = (DataRow)Session["row"];

            if (e.KeyValue == DBNull.Value || e.KeyValue == null)
                return;
            //            object val = gridView.GetRowValuesByKeyValue(e.KeyValue, "groupId");
            object val = gridView.GetRowValues(1, "groupId");
            if (val == DBNull.Value)
                return;
            string groupId = (string)val;

            ASPxComboBox combo = e.Editor as ASPxComboBox;
            FillFormTypeCombo(combo, row["groupId"].ToString(), row["memberId"].ToString());
        }

        protected void FillFormTypeCombo(ASPxComboBox cmb, string groupId, string memberId)
        {
            if (string.IsNullOrEmpty(groupId))
                return;
            if (dtGroups == null)
            {
                UserService wsU = ImqWS.GetUserService();
                dtGroups = wsU.getListUserGroupByMemberId(memberId);
            }
            if (dtGroups != null)
            {
                cmb.Items.Clear();
                for (int n = 0; n < dtGroups.Length; n++)
                    cmb.Items.Add(dtGroups[n].id.groupId);
            }
        }

        protected void gridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridView = (ASPxGridView)sender;
            DataRow row = tbl.Rows.Find(e.Keys[0]);
            IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
            enumerator.Reset();

            while (enumerator.MoveNext())
            {
                row[enumerator.Key.ToString()] = enumerator.Value;
            }
            e.Cancel = true;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "0";
            //Session["row"] = row;
            //Response.Redirect("~/administration/userGroupEdit.aspx?stat=0");
            string strRedirect = "~/administration/userGroupEdit.aspx?stat=0&" +
                    "memberId=" + Server.UrlEncode(row["memberId"].ToString()) + "&" +
                    "userId=" + Server.UrlEncode(row["userId"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "1";
            //Session["row"] = row;
            //Response.Redirect("~/administration/userGroupEdit.aspx?stat=1");
            string strRedirect = "~/administration/userGroupEdit.aspx?stat=1&" +
                    "memberId=" + Server.UrlEncode(row["memberId"].ToString()) + "&" +
                    "groupId=" + Server.UrlEncode(row["groupId"].ToString()) + "&" +
                    "userId=" + Server.UrlEncode(row["userId"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            UserService wsU = ImqWS.GetUserService();
            dtUserGroupList[] UGL = wsU.getUserGroupList(row["memberId"].ToString(), row["userId"].ToString(), "");
            if (UGL.Length > 1)
            {
                wsU.RemoveUserGroup(row["memberId"].ToString(), row["userId"].ToString(), row["groupId"].ToString());
            }
            else
            {
                // Group
                dtUserGroupListPK dtUGLPKNew = new dtUserGroupListPK();
                dtUGLPKNew.userId = row["userId"].ToString();
                dtUGLPKNew.memberId = row["memberId"].ToString();
                dtUGLPKNew.groupId = "";

                dtUserGroupListPK dtUGLPKOld = new dtUserGroupListPK();
                dtUGLPKOld.userId = row["userId"].ToString();
                dtUGLPKOld.memberId = row["memberId"].ToString();
                dtUGLPKOld.groupId = row["groupId"].ToString();

                wsU.UpdateUserGroupList(dtUGLPKOld, dtUGLPKNew);
            }
            putAuditTrail(aUserLogin, "User Group", "Delete User Group", "Member = " + row["memberId"].ToString() + ", User = " + row["userId"].ToString() + ", Group = " + row["groupId"].ToString());
            putData();
            gridView.DataBind();
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                //userGroupList = wsU.SearchUserGroupList(dtMemberId.Text, dtUserId.Text, dtGroupId.Text);
                userGroupList = wsU.getUserGroupList(dtMemberId.Text, dtUserId.Text, dtGroupId.Text);
                GetTable(tbl);
                gridView.DataBind();

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        private void putAuditTrail(String aUser, String aModule, String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", aActifity, aMemo);
        }

        protected void btnPdfExport_Click(object sender, EventArgs e)
        {
            gridExport.WritePdfToResponse();
        }
        protected void btnXlsExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsToResponse();
        }
        protected void btnRtfExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteRtfToResponse();
        }
        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteCsvToResponse();
        }


        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            enableBtn(false);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "User Group");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (!btnNew.Enabled && dtFMP[n].editMaker)
                    btnNew.Enabled = dtFMP[n].editMaker;

                if (!btnEdit.Enabled && dtFMP[n].editDirectChecker)
                    btnEdit.Enabled = dtFMP[n].editDirectChecker;

                if (!btnRemove.Enabled)
                    btnRemove.Enabled = dtFMP[n].removeing;
            }
        }

        private void enableBtn(bool aEnable)
        {
            btnNew.Enabled = aEnable;
            btnEdit.Enabled = aEnable;
            btnRemove.Enabled = aEnable;
        }

    }
}