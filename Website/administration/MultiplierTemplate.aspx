﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="MultiplierTemplate.aspx.cs" Inherits="imq.kpei.administration.MultiplierTemplate" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

        <script type="text/javascript">
        // <![CDATA[
            function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(),
                    'id;templateName;gfFee;clearingFeeType;clearingFeeCalculation;clearingFeeValue;' +
                    'minimumFee;multiplier;marginType;marginValue;hph;underlying;tickSize;' +
                    'startTradingHour;endTradingHour;samplingHphType;startSamplingHph;endSamplingHph;tickValue;', 
                    OnGetRowValues);
            }

            function OnGetRowValues(values) {
                hftemplate.Set("id", values[0]);
                hftemplate.Set("templateName", values[1]);
                hftemplate.Set("gfFee", values[2]);
                hftemplate.Set("clearingFeeType", values[3]);
                hftemplate.Set("clearingFeeCalculation", values[4]);
                hftemplate.Set("clearingFeeValue", values[5]);
                hftemplate.Set("minimumFee", values[6]);
                hftemplate.Set("multiplier", values[7]);
                hftemplate.Set("marginType", values[8]);
                hftemplate.Set("marginValue", values[9]);
                hftemplate.Set("hph", values[10]);
                hftemplate.Set("underlying", values[11]);
                hftemplate.Set("tickSize", values[12]);
                hftemplate.Set("startTradingHour", values[13]);
                hftemplate.Set("endTradingHour", values[14]);
                hftemplate.Set("samplingHphType", values[15]);
                hftemplate.Set("startSamplingHph", values[16]);
                hftemplate.Set("endSamplingHph", values[17]);
                hftemplate.Set("tickValue", values[18]);
                //txtcode.SetText(values[0]);
                //txtunderlying.SetText(values[1]);
                /*
                if (hfseries.Get("seriesCode") != undefined) {
                    btndel.SetEnabled(true);
                    btned.SetEnabled(true);
                } else {
                    btndel.SetEnabled(false);
                    btned.SetEnabled(false);
                }
                */
            }

            function ConfirmDelete(s, e) {
                e.processOnServer = confirm("Are you sure delete this contract \ncode : " + hfseries.Get("seriesCode") + " ?");
            }

            function view(s, e) {
                window.open("SeriesView.aspx?series=" + cmbCode.GetText() + "&underlying=" + cmbUnderlying.GetText(), "", "fullscreen=yes;scrollbars=auto");
            }
        // ]]>
        </script>
       
        <div  class="content">
            <div class="title">
                <h3>
                    Contract Parameter Template</h3>
            </div>
            <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" ></dx:ASPxLabel>
            <div class="maincontent">
                <table id="tblContract">
                    <tr>
                        <td style="width:128px" nowrap="nowrap">
                            ID</td>
                        <td>
                            <dx:ASPxComboBox ID="cmbId" runat="server" IncrementalFilteringMode="Contains"
                            DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbCode" TextField="seriesCode"
                            ValueField="seriesCode" ValueType="System.String" EnableCallbackMode="true" CallbackPageSize="10"
                            >
                                <DropDownButton Visible="false">
                                </DropDownButton>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click"></dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnExport" runat="server" Text="Export" 
                                onclick="btnExport_Click" Visible="False">
                                <ClientSideEvents Click="view"/>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Template Name </td>
                        <td>
                            <dx:ASPxComboBox ID="cmbName" runat="server" IncrementalFilteringMode="Contains"
                            DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbUnderlying" TextField="underlyingCode"
                            EnableCallbackMode="true" CallbackPageSize="10" ValueField="underlyingCode" 
                                ValueType="System.String">
                                <DropDownButton Visible="false">
                                </DropDownButton>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div class="clear" />
                <dx:ASPxHiddenField ID="hfTemplate" runat="server" ClientInstanceName="hftemplate">
                </dx:ASPxHiddenField>
                <br />
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnNew" runat="server" Text="New" AutoPostBack="false" 
                            onclick="btnNew_Click" ></dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnEdit" ClientInstanceName="btned" runat="server" 
                            Text="Edit" AutoPostBack="false" onclick="btnEdit_Click" ></dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnDelete" runat="server" 
                            Text="Delete" AutoPostBack="false" onclick="btnDelete_Click" Visible="False" ></dx:ASPxButton></td>
                        <td width="100%" align="right">
                            <table >
                                <tr >
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <dx:ASPxGridView ID="gvTemplateList" runat="server" AutoGenerateColumns="False" CssClass="grid"
                ClientInstanceName="grid" ondatabinding="gvTemplateList_DataBinding" KeyFieldName="id" >
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="ID" VisibleIndex="0" FieldName="id">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Template Name" VisibleIndex="1" FieldName="templateName">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="GF Fee" VisibleIndex="2" FieldName="gfFee">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Clearing Fee Type" VisibleIndex="3" Visible="false" FieldName="clearingFeeType">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Clearing Fee Calculation" VisibleIndex="4" Visible="false" FieldName="clearingFeeCalculation">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Clearing Fee Value" VisibleIndex="5" FieldName="clearingFeeValue">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Minimum Fee" VisibleIndex="6" FieldName="minimumFee">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Multiplier" VisibleIndex="7" FieldName="multiplier">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Margin Type" VisibleIndex="8" Visible="false" FieldName="marginType">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Margin Value" VisibleIndex="9" FieldName="marginValue">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="HPH" VisibleIndex="10" FieldName="hph">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Underlying" VisibleIndex="11" FieldName="underlying">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Tick Size" VisibleIndex="12" Visible="false" FieldName="tickSize">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Start trading Hour" VisibleIndex="13" Visible="false" FieldName="startTradingHour">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="End Trading Hour" VisibleIndex="14" Visible="false" FieldName="endTradingHour">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Sampling HPH Type" VisibleIndex="15" FieldName="samplingHphType">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Start Sampling HPH" VisibleIndex="16" Visible="false" FieldName="startSamplingHph">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="End Sampling HPH" VisibleIndex="17" Visible="false" FieldName="endSamplingHph">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Tick Value" VisibleIndex="18" Visible="false" FieldName="tickValue">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="true" AllowSelectByRowClick="True" />
                    <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                    <Styles>
                        <AlternatingRow Enabled="True" />
                        <Header CssClass="gridHeader" />
                    </Styles>
                    <SettingsPager PageSize="20">
                    </SettingsPager>
                    <Settings ShowVerticalScrollBar="True" />
                </dx:ASPxGridView>
            </div>
</asp:Content>
