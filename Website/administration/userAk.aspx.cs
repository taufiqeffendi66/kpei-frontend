﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Common;
using Common.wsUser;

namespace imq.kpei.administration
{
    public partial class userAk : System.Web.UI.Page
    {
        private dtUserSkd[] users;

        private DataTable tbl = null;
        private String aUserLogin;
        private String memberId;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            memberId = Session["SESSION_USERMEMBER"].ToString();
            putAuditTrail(aUserLogin,
                "User",
                "Open Module User",
                "Open Module User");

            if (!IsPostBack && !IsCallback)
            {
                getData();
                tbl = CreateTable();
                GetTable(tbl);
                Session["tbl"] = tbl;

            }
            else
                tbl = (DataTable)Session["tbl"];
            gridView.KeyFieldName = "userId;memberId";
            gridView.DataSource = tbl;
            gridView.DataBind();
        }

        DataTable CreateTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("userId", typeof(String));
            table.Columns.Add("memberId", typeof(String));
            table.Columns.Add("password", typeof(String));
            table.Columns.Add("groupName", typeof(String));
            table.Columns.Add("dateActive", typeof(DateTime));
            table.Columns.Add("dateExpired", typeof(DateTime));
            table.Columns.Add("passExpired", typeof(DateTime));
            table.Columns.Add("status", typeof(String));
            DataColumn[] keyColumn = new DataColumn[2];
            keyColumn[0] = table.Columns["userId"];
            keyColumn[1] = table.Columns["memberId"];
            table.PrimaryKey = keyColumn;
            //table.PrimaryKey = new DataColumn[] { table.Columns["userId"], table.Columns["memberId"] };
            return table;
        }


        void GetTable(DataTable table)
        {
            //You can store a DataTable in the session state
            if (table.Rows.Count > 0) table.Clear();
            for (int n = 0; n < users.Length; n++)
                table.Rows.Add(
                    users[n].id.userId,
                    users[n].id.memberId,
                    users[n].password,
                    "Alo",//users[n].userGroup.name,
                    users[n].dateActive,
                    users[n].dateExpired,
                    users[n].passExpired,
                    users[n].status);
        }

        private void getData()
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                if (memberId.Equals("Admin"))
                    users = wsU.View();
                else users = wsU.ListByMember(memberId);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        DateTime StrToDateTime(String str)
        {
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            MyDateTime = Convert.ToDateTime(str);
            return MyDateTime;
        }

        protected void gvUser_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = users;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Session["stat"] = "0";
            UserService wsU = ImqWS.GetUserService();
            Response.Redirect("~/administration/useredit.aspx?pinCode=" + wsU.getPinCode());
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            Session["stat"] = "1";
            Session["row"] = row;
            Response.Redirect("~/administration/useredit.aspx");

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                if (dtUserId.Text == "" && dtMemberId.Text == "")
                    users = wsU.View();
                else
                    users = wsU.Search(dtUserId.Text, dtMemberId.Text);
                GetTable(tbl);
                gridView.DataBind();

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            UserService wsU = ImqWS.GetUserService();
            wsU.RemoveByUserIdAndMemberId(row["userId"].ToString(), row["memberId"].ToString());
            getData();
            GetTable(tbl);
            gridView.DataBind();
        }

        private void putAuditTrail(String aUser, String aModule, String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", aActifity, aMemo);
            //dtAuditTrail dtAT = new dtAuditTrail();
            //dtAT.userSkd = aUser;
            //dtAT.module = aModule;
            //dtAT.actifity = aActifity;
            //dtAT.memo = aMemo;

            //wsAuditTrail.AuditTrailService wsAT = new wsAuditTrail.AuditTrailService();
            //wsAT.Url = GetConfig("WS_SERVER") + "/wsAuditTrail/AuditTrail";
            //wsAT.AddData(dtAT);
        }

        protected void btnPdfExport_Click(object sender, EventArgs e)
        {
            gridExport.WritePdfToResponse();
        }
        protected void btnXlsExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsToResponse();
        }
        protected void btnXlsxExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsxToResponse();
        }
        protected void btnRtfExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteRtfToResponse();
        }
        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteCsvToResponse();
        }

    }
}