﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Data;

using Common;
using Common.wsUser;

//using Common.wsUserGroup;
using Common.wsUserGroupPermission;

namespace imq.kpei.administration
{
    public partial class group : System.Web.UI.Page
    {
        private Common.wsUser.dtUserGroup[] dtUG;
        //private imq.kpei.wsUserGroup.dtUserGroupList [] dtUGL;
        private DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            ShowConfirm(false);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!aUserLogin.Equals("") && !aUserMember.Equals(""))
                Permission();
            Init(false);
        }

        private void Init(bool fromDel)
        {
            if (fromDel)
            {
                getData();
                tbl = CreateTable();
                GetTable(tbl);
                Session["tbl"] = tbl;
            }
            else
            {
                if (!IsPostBack && !IsCallback)
                {
                    getData();
                    tbl = CreateTable();
                    GetTable(tbl);
                    Session["tbl"] = tbl;

                }
                else
                    tbl = (DataTable)Session["tbl"];
            }
            gridView.DataSource = tbl;
            gridView.KeyFieldName = "memberId;groupId";
            gridView.DataBind();

            if (!aUserMember.Equals("kpei"))
            {
                dtMemberId.Enabled = false;
                dtMemberId.Text = aUserMember;
            }
        }

        static DataTable CreateTable()
        {

            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            //table = new DataTable();
            table.Columns.Add("memberId", typeof(String));
            table.Columns.Add("groupId", typeof(String));
            table.Columns.Add("des", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["memberId"], table.Columns["groupId"] };
            return table;
        }

        void GetTable(DataTable table)
        {
            //You can store a DataTable in the session state
            if (table.Rows.Count > 0)
                table.Clear();
            if (dtUG != null)
                for (int n = 0; n < dtUG.Length; n++)
                {
                    table.Rows.Add(
                        dtUG[n].id.memberId,
                        dtUG[n].id.groupId,
                        dtUG[n].des);
                }
        }

        private void getData()
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                if ( aUserMember.Equals("kpei"))
                    dtUG = wsU.getUserGroup("");
                else
                    dtUG = wsU.getUserGroup(aUserMember);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            //Session["stat"] = "0";
            Response.Redirect("~/administration/groupedit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "1";
            //Session["row"] = row;
            //Response.Redirect("~/administration/groupedit.aspx");

            string strRedirect = String.Format("~/administration/groupedit.aspx?stat=1&memberId={0}&groupId={1}&des={2}", Server.UrlEncode(row["memberId"].ToString()), Server.UrlEncode(row["groupId"].ToString()), Server.UrlEncode(row["des"].ToString()));

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            dtLabel.Text = "";
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);

            UserService wsU = ImqWS.GetUserService();
            dtUserGroupList[] ugl = wsU.getUserGroupList(row["memberId"].ToString(), "", row["groupId"].ToString());
            if (ugl == null )
            {
                UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
                wsUGP.RemoveByMemberIdAndGroupId(row["memberId"].ToString(), row["groupId"].ToString());
                Init( true );
                if (wsUGP != null)
                    wsUGP.Dispose();
            }
            else
                ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Alert", "alert('Group Must Be Emptied from User')", true);
        }

        protected void btnDelOk_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            wsUGP.RemoveByMemberIdAndGroupId(row["memberId"].ToString(), row["groupId"].ToString());
            if (wsUGP != null)
                wsUGP.Dispose();
        }

        protected void btnDelCancel_Click(object sender, EventArgs e)
        {
            ShowConfirm(false);
        }

        private void ShowConfirm(Boolean aYes)
        {
            btnNew.Visible = !aYes;
            btnEdit.Visible = !aYes;
            btnRemove.Visible = !aYes;

            dtLabel.Visible = aYes;
            btnDelCancel.Visible = aYes;
            btnDelOk.Visible = aYes;
        }

        private void putAuditTrail(String aUser, String aModule, String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "Group", aActifity, aMemo);
        }

        protected void btnPdfExport_Click(object sender, EventArgs e)
        {
            gridExport.WritePdfToResponse();
        }

        protected void btnXlsExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteXlsToResponse();
        }

        protected void btnRtfExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteRtfToResponse();
        }

        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            gridExport.WriteCsvToResponse();
        }

        private void enableBtn(bool aEnable)
        {
            btnNew.Enabled = aEnable;
            btnEdit.Enabled = aEnable;
            btnRemove.Enabled = aEnable;
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            enableBtn(false);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Group");
            for (int n = 0; n < dtFMP.Length; n++)
            {

                if (!btnNew.Enabled && dtFMP[n].editMaker)
                    btnNew.Enabled = dtFMP[n].editMaker;

                if (!btnEdit.Enabled && dtFMP[n].editDirectChecker)
                    btnEdit.Enabled = dtFMP[n].editDirectChecker;

                if (!btnRemove.Enabled && dtFMP[n].removeing)
                    btnRemove.Enabled = dtFMP[n].removeing;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                dtUG = wsU.SearchUserGroup(dtMemberId.Text, dtGroupId.Text);
                GetTable(tbl);
                gridView.DataBind();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }


    }
}