﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.wsUser;
using Common;

namespace imq.kpei.administration
{
    public partial class userGroupEdit : System.Web.UI.Page
    {
        //private dtUserSkd[] users;
        //private dtUserGroup[] dtUG;
        //private dtUserGroupList[] dtUGL;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow rows;
        private int stat;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (Request.QueryString["stat"] == null)
            {
                Response.Redirect("~/administration/userGroup.aspx");
                stat = -1;
            }
            else
                stat = int.Parse(Request.QueryString["stat"].ToString());

            InitPage();

            if (!IsPostBack)
            {

                switch (stat)
                {
                    case 0:
                        lbltitle.Text = "New USER Group";
                        btnOk.Visible = true;
                        dtUserId.Enabled = false;
                        dtMemberList.Enabled = false;
                        dtUserId.Text = Request.QueryString["userId"].ToString();
                        dtMemberList.Text = Request.QueryString["memberId"].ToString();
                        break;

                    case 1:

                        lbltitle.Text = "Edit USER Group";
                        btnOk.Visible = true;
                        dtUserId.Enabled = false;
                        dtMemberList.Enabled = false;
                        dtUserId.Text = Request.QueryString["userId"].ToString();
                        dtMemberList.Text = Request.QueryString["memberId"].ToString();
                        dtGroupList.Text = Request.QueryString["groupId"].ToString();
                        break;

                    case 2:
                        break;
/*
                    case 3:
                        lbltitle.Text = "Edit User";
                        btnOk.Visible = false;
                        UserService wsU = ImqWS.GetUserService();
                        try
                        {
                            users = wsU.SearchByID(Session["idx"].ToString()); //SearchByID("_tmp", Session["idx"]);
                            if (users != null)
                                toEditor();
                        }
                        catch (TimeoutException te)
                        {
                            te.ToString();
                            wsU.Abort();
                            throw;
                        }
                        break;
                    case 5:
                        lbltitle.Text = "Reset USER";
                        btnOk.Visible = true;
                        btnCancel.Visible = false;
                        dtUserId.Enabled = false;
                        dtMemberList.Enabled = false;
                        dtGroupList.Enabled = false;
                        toEditor();
                        break;
*/
                }

            }
        }

        protected void InitPage()
        {
            btnOk.Visible = false;
            UserService wsU = ImqWS.GetUserService();
            try
            {
                // get List Member
                dtMemberList.Items.Clear();
                dtMemberList.Items.Add(aUserMember, aUserMember);
                if (aUserMember == "kpei")
                {
                    dtMemberInfo[] dtML = wsU.getListMember();
                    if (dtML != null)
                        for (int i = 0; i < dtML.Length; i++)
                            dtMemberList.Items.Add(dtML[i].memberId, dtML[i].memberId);
                }
                else
                    dtMemberList.Enabled = false;

                dtMemberList.Text = Request.QueryString["memberId"].ToString();

                //get list Group by MemberId
                dtGroupList.Items.Clear();
                wsU = ImqWS.GetUserService();
                dtUserGroup[] dtGroups = wsU.getListUserGroupByMemberId(dtMemberList.Text);
                if (dtGroups != null )
                    for (int i = 0; i < dtGroups.Length; i++)
                        dtGroupList.Items.Add(dtGroups[i].id.groupId, dtGroups[i].id.groupId);

                //dtGroupList.Text = rows["groupid"].ToString();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }
/*
        protected void toEditor()
        {
            dtUserId.Text = Request.QueryString["userId"].ToString();
            dtMemberList.Text = Request.QueryString["memberId"].ToString();
            dtGroupList.Text = Request.QueryString["groupId"].ToString();
        }
*/
        //protected void getSearch(String tbl, String userID, String memberID)
        //{
        //    UserService wsU = CreateWebService();
        //    try
        //    {
        //        users = wsU.Search(userID, memberID);
        //    }
        //    catch (TimeoutException te)
        //    {
        //        te.ToString();
        //        wsU.Abort();
        //        throw;
        //    }
        //}

        protected void btnOk_Click(object sender, EventArgs e)
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                // Group
                dtUserGroupListPK dtUGLPKNew = new dtUserGroupListPK() { userId = dtUserId.Text, memberId = dtMemberList.Value.ToString(), groupId = dtGroupList.Value.ToString() };                

                switch (stat)
                {
                    case 0:
                        dtUserGroupList dtUGL = new dtUserGroupList() { id = dtUGLPKNew };
                        wsU.AddUserGroupList( dtUGL );
                        Response.Redirect("~/administration/userGroup.aspx");
                        break;

                    case 1:
                        dtUserGroupListPK dtUGLPKOld = new dtUserGroupListPK() { userId = dtUserId.Text, memberId = dtMemberList.Value.ToString(), groupId = Request.QueryString["groupId"] };

                        wsU.UpdateUserGroupList(dtUGLPKOld, dtUGLPKNew);

                        putAuditTrail("User Group List", String.Format("{0} To {1}", dtUGLPKOld, dtUGLPKNew));
                        Response.Redirect("~/administration/userGroup.aspx");
                        break;

                    case 2:
                        Response.Redirect("~/account/approval.aspx");
                        break;

                    case 5:
                        Response.Redirect("~/administration/userGroup.aspx");
                        break;

                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            switch (stat)
            {
                case 0:
                case 1: Response.Redirect("~/administration/userGroup.aspx"); break;
                case 2:
                case 3: Response.Redirect("~/account/approval.aspx"); break;
            }
        }

        private void putAuditTrail(String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", aActifity, aMemo);
            //dtAuditTrail dtAT = new dtAuditTrail();
            //dtAT.userSkd = aUserLogin;
            //dtAT.module = "User";
            //dtAT.actifity = aActifity;
            //dtAT.memo = aMemo;

            //wsAuditTrail.AuditTrailService wsAT = new wsAuditTrail.AuditTrailService();
            //wsAT.Url = GetConfig("WS_SERVER") + "/wsAuditTrail/AuditTrail";
            //wsAT.AddData(dtAT);
        }

        public static String toString(dtUserSkd aDM)
        {
            return String.Format("dtUserSkdMain [id={0}, dateActive={1}, dateExpired={2}, passExpired={3}, password={4}, status={5}, pinCode={6}, keyFile={7}]", aDM.id, String.Format("dd/MM/yyyy", aDM.dateActive), String.Format("dd/MM/yyyy", aDM.dateExpired), String.Format("dd/MM/yyyy", aDM.passExpired), aDM.password, aDM.status, aDM.pinCode, aDM.keyFile);
        }

//        private void ShowMessage(string info, string targetPage)
//        {
//            string mystring = @"<script type='text/javascript'>
//                                jAlert('" + info + @"','Information',function()
//                                window.location='" + targetPage + @"'
//                                </script>";
//            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
//                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
//        }
    }
}