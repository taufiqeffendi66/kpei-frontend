﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="useredit.aspx.cs" Inherits="imq.kpei.administration.useredit" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="USER Edit" /></div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="User ID"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="dtUserId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Member Id"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td>
                        <dx:ASPxComboBox ID="dtMemberList" runat="server" DropDownStyle="DropDownList" ClientInstanceName="cbMember" EnableSynchronization="False"></dx:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Pin Code"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="dtPinCode" runat="server" Width="170px" ReadOnly="True"></dx:ASPxTextBox></td>
                </tr>
            </table> 
        </div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxButton ID="btnOk" runat="server" Text="Ok" onclick="btnOk_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Checked" onclick="btnChecker_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnApprovel" runat="server" Text="Approved" onclick="btnApprovel_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></dx:ASPxButton></td>
                </tr>
            </table> 
        </div>
    </div>


</asp:Content>
