﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Common.wsApproval;
using Common.wsTransaction;
using Common;
using Common.underlyingWS;
using Common.wsUserGroupPermission;
using log4net;
using log4net.Config;

namespace imq.kpei.administration
{
    public partial class multiplierTemplateInput : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Page));
        private int addedit;
        private String aUserLogin;
        private String aUserMember;
        private templateParamMessage templateTmp;
        //private DataRow row;
        //private seriesTmp aSeriesTemp;
        
        private const String module = "Contract Template";
        //const string Months = "FGHJKMNQUVXZ";
        protected void Page_Load(object sender, EventArgs e)
        {
            log.Info("Page_Load");
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];
            if (!IsPostBack)
            {
                log.Debug("USER : " + aUserLogin);
                log.Debug("MEMBER : " + aUserMember);
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;

                    hfTemplate.Set("InitialCode", " ");
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }

                fillUnderlying();

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = "New " + module; //New Direct 
                        //changeContractPeriod();
                        btnSave.Visible = true;
                        Permission(true);
                        txtMinFee.Text = "0";
                        txtGfFee.Text = "0";
                        break;
                    case 1:
                        lblTitle.Text = "Edit " + module; //Edit Direct
                        btnSave.Visible = true;

                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = "New " + module; //New From Temporary
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 3:
                        lblTitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 4:
                        lblTitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }
            }
        }

        private void GetTemporary()
        {
            log.Info("Get Temporary");
            UiDataService uws = null;
            try
            {
            uws = ImqWS.GetTransactionService();
            templateParamMessage tmp = uws.getTemplateParamTmpById(int.Parse(Request.QueryString["idxTmp"].ToString()));
            //TODO
            txtTmplName.Text = tmp.templateName;
            txtGfFee.Text = tmp.gfFee.ToString();
            txtClearingFeeValue.Text = tmp.clearingFeeValue.ToString();
            string clearingFeeType = tmp.clearingFeeType;
            cmbClearingFeeType.SelectedItem = cmbClearingFeeType.Items.FindByValue(clearingFeeType);
            //txtClearingFeeType.Text = Server.UrlDecode(Request.QueryString["clearingFeeType"].ToString());
            //txtClearingFeeCalculation.Text = Server.UrlDecode(Request.QueryString["clearingFeeCalculation"].ToString());
            string clearingFeeCalc = tmp.clearingFeeCalculation;
            cmbClearingFeeCalculation.SelectedItem = cmbClearingFeeCalculation.Items.FindByValue(clearingFeeCalc);
            txtMinFee.Text = tmp.minimumFee.ToString();
            txtMultiplier.Text = tmp.multiplier.ToString();
            string marginType = tmp.marginType;
            cmbMarginType.SelectedItem = cmbMarginType.Items.FindByValue(marginType);
            //txtMarginType.Text = Server.UrlDecode(Request.QueryString["marginType"].ToString());
            txtMarginValue.Text = tmp.marginValue.ToString();
            txtHph.Text = tmp.hph.ToString();
            string underlying = tmp.underlying;
            cmbUnderlying.SelectedItem = cmbUnderlying.Items.FindByValue(underlying);
            cmbTickSize.SelectedItem = cmbUnderlying.Items.FindByValue(tmp.tickSize.ToString());
            txtSTH.Text = tmp.startTradingHour;
            txtETH.Text = tmp.endTradingHour;
            //txtSamplingHphType.Text = Server.UrlDecode(Request.QueryString["samplingHphType"].ToString());
            string samplHPHType = tmp.samplingHphType;
            cmbSamplingHPHType.SelectedItem = cmbSamplingHPHType.Items.FindByValue(samplHPHType);
            txtSSH.Text = tmp.startSamplingHph;
            txtESH.Text = tmp.endSamplingHph;
            txtTickValue.Text = tmp.tickValue.ToString();
            hfTemplate.Set("id", tmp.id);
            
                
                log.Debug(tmp.ToString());
            }
            catch (Exception e)
            {
                uws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
                log.Error("Get Temporary -> " + e.Message);
            }
            finally
            {
                if (uws != null)
                    uws.Dispose();
            }
        }
        private void DisabledControl()
        {

            txtTmplName.Enabled = false;
            txtGfFee.Enabled = false;
            cmbClearingFeeType.Enabled = false;
            cmbClearingFeeCalculation.Enabled = false;
            txtClearingFeeValue.Enabled = false;
            txtMinFee.Enabled = false;
            txtMultiplier.Enabled = false;
            cmbMarginType.Enabled = false;
            txtMarginValue.Enabled = false;
            txtHph.Enabled = false;
            cmbUnderlying.Enabled = false;
            cmbTickSize.Enabled = false;
            txtSTH.Enabled = false;
            txtETH.Enabled = false;
            cmbSamplingHPHType.Enabled = false;
            txtSSH.Enabled = false;
            txtESH.Enabled = false;
            txtTickValue.Enabled = false;
        }
        private void ParseQueryString()
        {
            txtTmplName.Text = Server.UrlDecode(Request.QueryString["templateName"].ToString());
            txtGfFee.Text = Server.UrlDecode(Request.QueryString["gfFee"].ToString());
            txtClearingFeeValue.Text = Server.UrlDecode(Request.QueryString["clearingFeeValue"].ToString());
            string clearingFeeType = Server.UrlDecode(Request.QueryString["clearingFeeType"].ToString());
            cmbClearingFeeType.SelectedItem = cmbClearingFeeType.Items.FindByValue(clearingFeeType);
            //txtClearingFeeType.Text = Server.UrlDecode(Request.QueryString["clearingFeeType"].ToString());
            //txtClearingFeeCalculation.Text = Server.UrlDecode(Request.QueryString["clearingFeeCalculation"].ToString());
            string clearingFeeCalc = Server.UrlDecode(Request.QueryString["clearingFeeCalculation"].ToString());
            cmbClearingFeeCalculation.SelectedItem = cmbClearingFeeCalculation.Items.FindByValue(clearingFeeCalc);
            txtMinFee.Text = Server.UrlDecode(Request.QueryString["minimumFee"].ToString());
            txtMultiplier.Text = Server.UrlDecode(Request.QueryString["multiplier"].ToString());
            string marginType = Server.UrlDecode(Request.QueryString["marginType"].ToString());
            cmbMarginType.SelectedItem = cmbMarginType.Items.FindByValue(marginType);
            //txtMarginType.Text = Server.UrlDecode(Request.QueryString["marginType"].ToString());
            txtMarginValue.Text = Server.UrlDecode(Request.QueryString["marginValue"].ToString());
            txtHph.Text = Server.UrlDecode(Request.QueryString["hph"].ToString());
            string underlying = Server.UrlDecode(Request.QueryString["underlying"].ToString());
            cmbUnderlying.SelectedItem = cmbUnderlying.Items.FindByText(underlying);
            cmbTickSize.SelectedItem = cmbTickSize.Items.FindByText(Server.UrlDecode(Request.QueryString["tickSize"].ToString()));
            txtSTH.Text = Server.UrlDecode(Request.QueryString["startTradingHour"].ToString());
            txtETH.Text = Server.UrlDecode(Request.QueryString["endTradingHour"].ToString());
            //txtSamplingHphType.Text = Server.UrlDecode(Request.QueryString["samplingHphType"].ToString());
            string samplHPHType = Server.UrlDecode(Request.QueryString["samplingHphType"].ToString());
            cmbSamplingHPHType.SelectedItem = cmbSamplingHPHType.Items.FindByValue(samplHPHType);
            txtSSH.Text = Server.UrlDecode(Request.QueryString["startSamplingHph"].ToString());
            txtESH.Text = Server.UrlDecode(Request.QueryString["endSamplingHph"].ToString());
            string id = Server.UrlDecode(Request.QueryString["id"].ToString());
            txtTickValue.Text = Server.UrlDecode(Request.QueryString["tickValue"]);
            hfTemplate.Set("id", Server.UrlDecode(Request.QueryString["id"].ToString()));
           
        }
       

        private void Permission(bool isEditor)
        {
            log.Info("Permission");
            dtFormMenuPermission[] listdtFMP = null;

            try
            {

                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    log.Debug("Edit Maker : " + dtFMP.editMaker);
                    log.Debug("Edit Checker : " + dtFMP.editChecker);
                    log.Debug("Edit Approval : " + dtFMP.editApproval);
                    log.Debug("Edit Direct Checker : " + dtFMP.editDirectChecker);
                    log.Debug("Edit Direct Approval : " + dtFMP.editDirectApproval);
                    log.Debug("Removing : " + dtFMP.removeing);
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"].ToString() == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                log.Error(e.Message);
            }
        }

        private void fillUnderlying()
        {
            log.Info("FillUnderlying");
            UnderlyingWSService uws = null;
            Common.underlyingWS.underlying[] listUnderlying = null;
            try
            {
                uws = ImqWS.GetUnderlyingWebService();
                listUnderlying = uws.get();

                foreach (Common.underlyingWS.underlying u in listUnderlying)
                {
                    cmbUnderlying.Items.Add(u.code.Trim(), u.code.Trim());
                    log.Debug("Underlying Code " + u.code);
                }
            }
            catch (Exception e)
            {
                uws.Abort();
                Debug.WriteLine(e.Message);
                log.Error(e.Message);
            }
            finally
            {
                uws.Dispose();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }
        private void WriteAuditTrail(string Activity)
        {
            log.Info("Write Audit Trail " + Activity);
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(templateTmp));
        }
        private String Log(templateParamMessage st)
        {
            log.Info("LOG " + st.ToString());
            string strLog = "[Contract Code : " + st.templateName +
            ", Underlying : " + st.underlying +
            "]";
            log.Info("[Contract Code : " + st.templateName +
            ", Underlying : " + st.underlying +
            "]");
            return strLog;
        }
        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, templateParamMessage st, UiDataService cws)
        {
            log.Info("rblApprovalChanged");
            string ret = "0";
            const string target = @"../administration/MultiplierTemplate.aspx";

            if (!templateParamMessage.ReferenceEquals(templateTmp, st))
                templateTmp = st;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            log.Debug("RblApproval : " + selected);
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            log.Info("Multiplier Template Insert : Maker");
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            log.Info("Multiplier Template Error : Maker");
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            log.Info("Multiplier Template Remove : Maker");
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            log.Info("Multiplier Template Insert : Direct Checker");
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            log.Info("Multiplier Template Edit : Direct Checker");
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            log.Info("Multiplier Template Remove : Direct Checker");
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    // ContractWSService cws = ImqWS.GetContractService();
                    //decimal tickSize = Convert.ToDecimal(txtTickSize.Text);
                    //decimal clearing_fee_value = Convert.ToDecimal(txtClearingFeeValue.Text);
                    //decimal minimum_fee = Convert.ToDecimal(txtMinFee.Text);
                    //decimal margin_value = Convert.ToDecimal(txtMarginValue.Text);
                    //decimal gf_fee = Convert.ToDecimal(txtGfFee.Text);
                    
                    templateParamMessage ser = new templateParamMessage() { templateName = templateTmp.templateName, underlying = templateTmp.underlying, tickSize = templateTmp.tickSize, tickSizeSpecified = true, clearingFeeValue = templateTmp.clearingFeeValue,clearingFeeValueSpecified = true, clearingFeeType = templateTmp.clearingFeeType, clearingFeeCalculation = templateTmp.clearingFeeCalculation, gfFee = templateTmp.gfFee, gfFeeSpecified = true, hph = templateTmp.hph, samplingHphType = templateTmp.samplingHphType, startSamplingHph = templateTmp.startSamplingHph, endSamplingHph = templateTmp.endSamplingHph, startTradingHour = templateTmp.startTradingHour, endTradingHour = templateTmp.endTradingHour, multiplier = templateTmp.multiplier, marginType = templateTmp.marginType, marginValue = templateTmp.marginValue, marginValueSpecified = true, minimumFee = templateTmp.minimumFee, minimumFeeSpecified = true };
                    templateTmp = ser;
                    log.Debug(ser.ToString());
                        
                    //ser.idx = aSeriesTemp.idx;
                    int id = 0;
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            log.Info("Multiplier Template Insert : Direct Approval");
                            ret = "0";
                            cws.storeTemplateParam(ser);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "E":
                            log.Info("Multiplier Template Edit : Direct Approval");
                            id = int.Parse(hfTemplate.Get("id").ToString());
                            ser.id = id;
                            cws.storeTemplateParam(ser);
                            ret = "0";
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        //case "R":
                            //id = hfTemplate.Get("id").ToString();
                            //ret = cws.(id);
                            //if (ret.Equals("0"))
                            //{
                            //    wsA.AddDirectApproval(dtA);
                            //    WriteAuditTrail("Direct Approval Delete " + module);
                            //    ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                            //}
                            //else
                            //{
                            //    dtA.approvelStatus = "Failed";
                            //    wsA.AddDirectApproval(dtA);
                            //}
                            //break;
                    }
                    break;
            }
            return ret;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            log.Info("btnSave_Click");
            
            ApprovalService aps = null;
            UiDataService uds = null;
            templateParamMessage data = null;

            addedit = (int)Session["stat"];
            try
            {
                
                if (cmbUnderlying.SelectedItem == null)
                {
                    if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                        ClientScript.RegisterStartupScript(this.GetType(), "clientScript", @"<script type='text/javascript'>
                                jAlert('Underlying Invalid Value','Information');
                                </script>");

                    return;
                }
                
                aps = ImqWS.GetApprovalWebService();
                uds = ImqWS.GetTransactionService();

                int idRec = -1;
                data = new templateParamMessage();
                data.templateName = txtTmplName.Text.Trim();
                data.underlying = cmbUnderlying.SelectedItem.Value.ToString();
                data.tickSize = decimal.Parse(cmbTickSize.SelectedItem.Value.ToString().Trim());
                data.tickSizeSpecified = true;
                data.clearingFeeValue = decimal.Parse(txtClearingFeeValue.Text.Trim());
                data.clearingFeeValueSpecified = true;
                data.clearingFeeType = cmbClearingFeeType.SelectedItem.Value.ToString().Trim();
                //data.clearingFeeCalculation = txtClearingFeeCalculation.Text.Trim();
                data.clearingFeeCalculation = cmbClearingFeeCalculation.SelectedItem.Value.ToString().Trim(); 
                data.gfFee = decimal.Parse(txtGfFee.Text.Trim());
                data.gfFeeSpecified = true;
                string hph = txtHph.Text.Replace(",",String.Empty);
                data.hph = string.IsNullOrEmpty(txtHph.Text) ? 0 : int.Parse(hph); //txtHph.Text.Length > 0 ? int.Parse(txtHph.Text.Trim()) : 0;
                data.samplingHphType = cmbSamplingHPHType.SelectedItem.Value.ToString().Trim();
                data.startSamplingHph = txtSTH.Text.Trim();
                data.endSamplingHph = txtESH.Text.Trim();
                data.startTradingHour = txtSTH.Text.Trim();
                data.endTradingHour = txtETH.Text.Trim();
                data.multiplier = string.IsNullOrEmpty(txtMultiplier.Text) ? 0 : int.Parse(txtMultiplier.Text.Replace(",", String.Empty)); //int.Parse(txtMultiplier.Text.Trim());
                //data.marginType = txtMarginType.Text.Trim();
                data.marginType = cmbMarginType.SelectedItem.Value.ToString().Trim();
                data.marginValue = txtMarginValue.Text.Length > 0 ? decimal.Parse(txtMarginValue.Text.Trim()) : 0;
                data.marginValueSpecified = true;
                data.minimumFee = txtMinFee.Text.Length > 0 ? decimal.Parse(txtMinFee.Text.Trim()) : 0;
                data.minimumFeeSpecified = true;
                data.tickValue = txtTickValue.Text.Length > 0 ? decimal.Parse(txtTickValue.Text.Trim()) : 0;
                data.tickValueSpecified = true;
                log.Debug(data.ToString());
                
                //data = uds.getTemplateParamTmpById
                idRec = uds.storeTemplateParamTmp(data);
                templateTmp = data;
                dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/Administration/multiplierTemplateInput.aspx", status = "M", memberId = aUserMember };
                int idTable;
                switch (addedit)
                {
                    case 0:
                        dtA.topic = "Add " + module;
                        dtA.insertEdit = "I";
                        templateTmp.id = idRec;
                        break;

                    case 1:
                        dtA.topic = "Edit " + module;
                        dtA.insertEdit = "E";
                        idTable = int.Parse(hfTemplate.Get("id").ToString());
                        dtA.idTable = idTable;
                        dtA.idxTmp = idRec;
                        break;

                    case 4:
                        dtA.topic = "Delete " + module;
                        dtA.insertEdit = "R";
                        idTable = int.Parse(hfTemplate.Get("id").ToString());
                        dtA.idTable = idTable;
                        dtA.idxTmp = idRec;
                        break;
                }
                string retVal = rblApprovalChanged(dtA, aps, data, uds);
                if (!retVal.Equals("0"))
                {
                    ShowMessage(retVal, @"../derivatif/MultiplierTemplate.aspx");
                }
                //}
                //else
                //{
                // ShowMessage("message=Failed On Save To Temporary Series Contract", @"../derivatif/seriescontract.aspx");
                //}
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                ex.ToString();
                if (uds != null)
                    uds.Abort();
                if (aps != null)
                    aps.Abort();
            }
            finally
            {
                if (uds != null)
                    uds.Dispose();
                if (aps != null)
                    aps.Dispose();
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("multiplierTemplateInput.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }

        private void ExecCheckerApproval(int cp)
        {
            log.Info("ExecCheckerApproval");
            ApprovalService aps = null;
            UiDataService uws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                uws = ImqWS.GetTransactionService();

                dtA = aps.Search(Request.QueryString["reffNo"].ToString());
                templateTmp = uws.getTemplateParamTmpById(dtA[0].idxTmp);
                //aSeriesTemp = cws.getSeriesTemp(dtA[0].idxTmp);
                

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                int ret = 1;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = uws.insertTemplateParamTmp2TemplateParam(dtA[0].idxTmp);
                            log.Debug("case : I, ret : " + ret);
                            //ret = cws.addFromTemp(dtA[0].idxTmp);
                            if (ret != 0)
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret.ToString(), target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                //ret = cws.deleteTempById(aSeriesTemp.idx);
                                //if (!ret.Equals("0"))
                                //    lblError.Text = ret;

                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            //ret = cws.updateFromTemp(aSeriesTemp, dtA[0].idTable);
                            ret = uws.updateTemplateParamTmp2TemplateParam(templateTmp,dtA[0].idTable);
                            log.Debug("case : E, ret : " + ret);
                            if (ret!=0)
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret.ToString(), target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);

                                //ret = cws.deleteTempById(aSeriesTemp.idx);
                                //if (!ret.Equals("0"))
                                //    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            //ret = cws.deleteById(dtA[0].idTable);
                            if (ret != 0)
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret.ToString(), target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                //ret = cws.deleteTempById(aSeriesTemp.idx);
                                //if (!ret.Equals("0"))
                                //    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (uws != null)
                    uws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
                log.Error("Exec Checker Approval -> " + ex.Message);
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (uws != null)
                    uws.Dispose();
            }
        }
    }
}