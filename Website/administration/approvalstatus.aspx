﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="approvalstatus.aspx.cs" Inherits="imq.kpei.administration.approvalstatus" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
     <script language="javascript" type="text/javascript">
        // <![CDATA[

         function OnGridFocusedRowChanged() {

             grid.GetRowValues(grid.GetFocusedRowIndex(), 'formId', OnGetRowValues);
         }

         function OnGetRowValues(values) {
             window.location("approval.aspx?formId=" + values[0]);
         }
        // ]]>
    </script> 

    <div class="content">
        <div class="title">APPROVAL INQUIRY</div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Reff No"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtReffNo" ClientInstanceName="clReffNo" runat="server" Width="170px"></dx:ASPxTextBox></td>
                </tr>

                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Date Maker"></dx:ASPxLabel></td>
                    <td><dx:ASPxDateEdit ID="dtMakerDate" runat="server" DisplayFormatString="yyyy-MM-dd"></dx:ASPxDateEdit></td>
                </tr>

                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Checker Status"></dx:ASPxLabel></td>
                    <td><dx:ASPxComboBox ID="dtChekerStatus" runat="server" DropDownStyle="DropDownList" ClientInstanceName="cbChekerStatus" EnableSynchronization="False"></dx:ASPxComboBox></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Approval Status"></dx:ASPxLabel></td>
                    <td><dx:ASPxComboBox ID="dtApprovalStatus" runat="server" DropDownStyle="DropDownList" ClientInstanceName="cbApprovalStatus" EnableSynchronization="False"></dx:ASPxComboBox></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Topic"></dx:ASPxLabel></td>
                    <td><dx:ASPxComboBox ID="dtTopic" runat="server" DropDownStyle="DropDownList" ClientInstanceName="cbTopic" EnableSynchronization="False"></dx:ASPxComboBox></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" 
                            onclick="btnSearch_Click"> </dx:ASPxButton></td>
                </tr>
            </table>

        <div>
            <dx:ASPxGridView ID="gridView" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" CssClass="grid">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Reff No" 
                        FieldName="reffNo" Width="50px">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <CellStyle HorizontalAlign="Right" VerticalAlign="Middle">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Type (Check/Approve)" FieldName="type" Visible="False"></dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn VisibleIndex="2" Caption="Maker">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Date" FieldName="makerDate" Width="75px"><PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy"></PropertiesDateEdit></dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Name" FieldName="makerName"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Status" FieldName="makerStatus"></dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn VisibleIndex="3" Caption="Checker">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Date" FieldName="checkerDate" Width="75px"><PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy"></PropertiesDateEdit></dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Name" FieldName="checkerName"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Status" FieldName="checkerStatus"></dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn VisibleIndex="4" Caption="Approval">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Date" FieldName="approvelDate" Width="75px"><PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy"></PropertiesDateEdit></dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Name" FieldName="approvelName"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Status" FieldName="approvelStatus"></dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Topic" FieldName="topic"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Insert/Edit" FieldName="insertEdit" Visible="False"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="8" Caption="Index" FieldName="index" Visible="False"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Status" FieldName="status" Visible="False"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="350" />
                <SettingsPager PageSize="15"></SettingsPager>
                <Styles><AlternatingRow Enabled="True"></AlternatingRow></Styles>
            </dx:ASPxGridView>
        </div>
    </div>

</asp:Content>