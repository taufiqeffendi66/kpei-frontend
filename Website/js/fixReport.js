﻿function fixReportingServices(container) {
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    if (is_chrome) { 
        $('#' + container + ' table').each(function (i, item) {
        if ($(item).attr('id') && $(item).attr('id').match(/fixedTable$/) != null)
            $(item).css('display', 'table');
        else
            $(item).css('display', 'inline-block');
        });

        $('#ReportViewer1_ctl10').attr('style', 'height:100%;width:100%;overflow:visible;position:relative;');
    }
}

Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () { fixReportingServices('ReportViewer1_ctl10'); });