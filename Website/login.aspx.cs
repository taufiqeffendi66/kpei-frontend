﻿using System;
using System.Collections.Generic;
using Common;
using Common.wsUser;
using log4net;

namespace imq.kpei
{
    public partial class login : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (ImqSession.ValidateAction(this))
            //{
            //    Response.Redirect("home.aspx");
            //}
            //else
            //{
           // UserService wsU = ImqWS.GetUserService();
            
            if (!IsPostBack)
            {
                txtUser.Value = "";
                txtMember.Value = "";
                Session.Abandon();
            }
            //}
        }
        private void ShowMessage(string info, string targetPage)
        {
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              alert('{0}');
                                                              window.location='{1}'
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        
        protected void btnLogin_Click(object sender, EventArgs e)
        {

            UserService wsU = null;
            try
            {
                wsU = ImqWS.GetUserService();
                dtUserSkd[] dtU;
                string dtUserId = Server.HtmlEncode(txtUser.Value.Trim());
                string dtMemberId = Server.HtmlEncode(txtMember.Value.Trim());
                dtMemberId = dtMemberId.ToUpper();
                if (dtMemberId.Equals("KPEI"))
                    dtMemberId = dtMemberId.ToLower();

                dtU = wsU.SearchByUserAndMember(dtUserId, dtMemberId);
                if (wsU != null)
                    wsU.Dispose();
                if (dtU == null)
                {
                    ShowMessage(" Member or User not Register !", "login.aspx");
                    //ShowMessage( ImqSession.GetConfig("msgLoginMemberUser") , "login.aspx ");
                    //txtUser.Focus();
                }
                else
                {
                    //Session["SESSION_ID"] = "";
                    if (dtU[0].status == "Active")
                    {
                        Session["SESSION_USER"] = dtUserId;
                        Session["SESSION_USERMEMBER"] = dtMemberId;
                        if (!string.IsNullOrEmpty(dtU[0].password))
                        {
                            Session["SESSION_CountLogin"] = 0;
                            Response.Redirect("loginKeyFile.aspx",false);
                            Context.ApplicationInstance.CompleteRequest();                             
                        }
                        else
                        {
                            Response.Redirect("loginPinCode.aspx",false);
                            Context.ApplicationInstance.CompleteRequest(); 
                        }
                    }
                    else
                    {
                        log.Info(dtUserId + " is BLocked");
                        lbError.Text = "      User Blocked";
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                log.Error("Inner Exception : " + ex.InnerException);
                ShowMessage(ex.Message, "login.aspx");
            }
        }
    }
}
