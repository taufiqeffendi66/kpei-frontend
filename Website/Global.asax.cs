﻿using System;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.SessionState;
using log4net;
using log4net.Config;

namespace imq.kpei
{
    public class Global : HttpApplication
    {
        //private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log = LogManager.GetLogger(typeof(_Default));
        protected void Application_Start(object sender, EventArgs e)
        {
            //String _path = String.Concat(System.Environment.GetEnvironmentVariable("PATH"), ";", ConfigurationSettings.AppSettings["NativePath"]);
            String _path = String.Concat(System.Environment.GetEnvironmentVariable("APP_PATH"), ";", System.AppDomain.CurrentDomain.RelativeSearchPath);
            System.Environment.SetEnvironmentVariable("APP_PATH", _path, EnvironmentVariableTarget.Process);

            XmlConfigurator.Configure();
            log.Info("Application Start");
        }
        

        void regenerateId()
        {

            //StringBuilder sb = new StringBuilder();
            //if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
            //    sb.Append(Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(',')[0]);
            //else
            //    sb.Append(Request.ServerVariables["REMOTE_ADDR"]);
            try
            {
                SessionIDManager manager = new SessionIDManager();
                string oldId = manager.GetSessionID(Context);
                string newId = String.Format("{0}", manager.CreateSessionID(Context)); //manager.CreateSessionID(Context);
                bool isAdd = false, isRedir = false;
                manager.SaveSessionID(Context, newId, out isRedir, out isAdd);
                HttpApplication ctx = (HttpApplication)HttpContext.Current.ApplicationInstance;
                HttpModuleCollection mods = ctx.Modules;
                SessionStateModule ssm = (SessionStateModule)mods.Get("Session");
                FieldInfo[] fields = ssm.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                SessionStateStoreProviderBase store = null;
                FieldInfo rqIdField = null, rqLockIdField = null, rqStateNotFoundField = null;
                foreach (FieldInfo field in fields)
                {
                    if (field.Name.Equals("_store")) store = (SessionStateStoreProviderBase)field.GetValue(ssm);
                    if (field.Name.Equals("_rqId")) rqIdField = field;
                    if (field.Name.Equals("_rqLockId")) rqLockIdField = field;
                    if (field.Name.Equals("_rqSessionStateNotFound")) rqStateNotFoundField = field;
                }
                object lockId = rqLockIdField.GetValue(ssm);
                if ((lockId != null) && (oldId != null)) store.ReleaseItemExclusive(Context, oldId, lockId);
                rqStateNotFoundField.SetValue(ssm, true);
                rqIdField.SetValue(ssm, newId);
            }
            catch (Exception e)
            {
                log.Error("Error Generate ID ",e);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            
            Session.Timeout = 10;
            //if (sysparams.Length > 0) Session.Timeout = int.Parse(sysparams[0].value);
            
            regenerateId();
            
            Session["SESSION_ID"] = Session.SessionID;
            Session["SESSION_USER"] = "";
            Session["SESSION_USERMEMBER"] = "";
            Session["SESSION_USERPASSWORD"] = "";

            //Response.Redirect("~/login.aspx");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Unhandled error occured in application. Sender: ");
            sb.AppendLine(Request.RawUrl);
            sb.Append("Query: ");
            sb.AppendLine(Request.QueryString.ToString());

            Exception ex = Server.GetLastError().GetBaseException();

            log.Error(sb.ToString(), ex);
            Server.ClearError();

            //Response.Redirect("~/Error.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Session["SESSION_ID"] = "";
            Session["SESSION_USER"] = "";
            Session["SESSION_USERMEMBER"] = "";
            Session["SESSION_USERPASSWORD"] = "";
        }

        protected void Application_End(object sender, EventArgs e)
        {
            log.Info("Application End");
        }
    }
}
