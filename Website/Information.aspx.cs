﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Common;

namespace imq.kpei
{
    public partial class Information : System.Web.UI.Page
    {
        
        private String aMessage;
        private String aTarget;
        private int stat = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            if (Request.QueryString["stat"] != null)
                stat = int.Parse(Request.QueryString["stat"]);

            if (Request.QueryString["message"] != null)
                aMessage = Request.QueryString["message"].ToString();

            if (Request.QueryString["target"] != null)
                aTarget = Request.QueryString["target"].ToString();


            switch (stat)
            {
                case 0: lblTitle.Text = "Information";
                        lblMessage.ForeColor = Color.Blue;
                        break;
                case 1: lblTitle.Text = "Error";
                        lblMessage.ForeColor = Color.Red;
                        break;
            }

            lblMessage.Text = aMessage;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {            
            Response.Redirect(aTarget, false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}