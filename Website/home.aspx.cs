﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeView;
using Common;
using Common.wsUser;
using Common.wsUserGroupPermission;
using System.Web;
using log4net;
using log4net.Config;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Net;
using System.IO;
using System.Collections.Generic;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Collections;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Diagnostics;

namespace imq.kpei
{
    public partial class home : Page
    {
        private indikator[] chart;
        private string msg_service;
        int x = 0;
        public int temp = 0;
        //private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log = LogManager.GetLogger(typeof(_Default));
        protected void Page_Load(object sender, EventArgs e)
        {

            //-----Client Timeout -----

            string AppURL;


            if (!Request.Path.Contains("loginKeyFile.aspx") || !Request.Path.Contains("signout.aspx"))
            {
                // AppTimeout = Convert.ToInt32(ImqSession.GetConfig("AppTimeoutMinutes"));
                //WarnTimeout = Convert.ToInt32(ImqSession.GetConfig("AppWarnMinutes"));
                AppURL = ImqSession.GetConfig("AppLogoutURL");

                Page.ClientScript.RegisterStartupScript(GetType(),
                                                        "InitTimeout",
                                                        "StartTimeOut(" + Session.Timeout * 60000 + "," + (Session.Timeout - 1) + 60000 + "," + AppURL + ")",
                                                        true);

            }

            //--END OF Client Timeout -----
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {

                string user = Session["SESSION_USER"].ToString();
                string member = Session["SESSION_USERMEMBER"].ToString();
                idMember.Text = member;
                idUser.Text = user;

                string jmsServer = HttpContext.Current.Request.Url.Host; //ImqSession.GetConfig("JMS_SERVER_WS");
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string infoTopic = ImqSession.GetConfig("JMS_TOPIC_INFO");
                string newsTopic = ImqSession.GetConfig("JMS_TOPIC_NEWS");
                //string password = Session["SESSION_USERPASSWORD"].ToString();
                string password = Session.SessionID;
                string userJMS = String.Format("{0}-{1}", user, member);

                if ((member.Equals(null)) || (!member.Equals("kpei")))
                {
                    textBoxNewsInput.Visible = false;
                    buttonSendNews.Visible = false;
                    newsList.Height = new Unit(83);

                    textBoxInfoInput.Visible = false;
                    buttonSendInfo.Visible = false;
                    infoList.Height = new Unit(83);
                }


                if (user != "" & member != "" & password != "")
                {
                    UserService wsU = ImqWS.GetUserService();
                    dtUserGroupList[] dtUGL = wsU.getUserGroupList(member, user, "");
                    if (dtUGL != null)
                    {
                        UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();

                        dtMenuList[] dtML = wsUGP.getMenu();
                        for (int n = 0; n < dtML.Length; n++)
                        {
                            dtFormMenuPermission[] dtFMP = wsUGP.getMenuByUserIdAndMemberIdAndMenuId(user, member, dtML[n].idMenu);
                            TreeViewNode parentNode;
                            TreeViewNode newNode;
                            if (dtFMP != null)
                            {
                                parentNode = treeMenu.Nodes.Add();
                                parentNode.Text = dtML[n].nameMenu;
                                log.Info("Menu List " + n + " -> " + dtML[n].nameMenu);
                                for (int m = 0; m < dtFMP.Length; m++)
                                {
                                    newNode = parentNode.Nodes.FindByText(dtFMP[m].formName);
                                    if (newNode == null)
                                    {
                                        if (dtFMP[m].reading)
                                        {
                                            newNode = parentNode.Nodes.Add();
                                            newNode.Text = dtFMP[m].formName;
                                            log.Info("Form List " + m + " -> " + dtFMP[m].formName);
                                            newNode.Name = String.Format("{0}?formId={1}", dtFMP[m].formLink, dtFMP[m].formId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                ScriptManager.RegisterStartupScript(this, this.GetType(), "initPage", @"                    
<script type=""text/javascript"" src=""../../js/util/page.js""></script>
<script type=""text/javascript"" src=""js/amq/stomp.js""></script>
<script type=""text/javascript"">
    function addInfo(s) {
        infoList.InsertItem(0, s);
        if (infoList.GetItemCount() > 10)
            infoList.RemoveItem(infoList.GetItemCount() - 1);
    }

    function addNews(s) {
        newsList.InsertItem(0, s);
        if (newsList.GetItemCount() > 10)
            newsList.RemoveItem(newsList.GetItemCount() - 1);
    }

    window.onbeforeunload = function() {
        client.unsubscribe('sub-0');
        client.unsubscribe('sub-1');
        client.unsubscribe('sub-2');
        client.disconnect(function() {
            addInfo('Disconnect client from JMS Server');
        });
    }

    //addInfo('Start');

    var url = """ + jmsServer + @""";
    var n = """ + userJMS + @""";
    var c = """ + password + @""";
    var infoTopic = """ + infoTopic + @""";
    var newsTopic = """ + newsTopic + @""";
    var pingTopic = """ + pingTopic + @""";

    //addInfo('Init client: ' + url);
    var client = Stomp.client(url);
    client.debug = function(str) {
        //addInfo(str);
    };

    var onconnect = function(frame) {
        //client.debug('Connected to Stomp');
        client.subscribe(pingTopic, function(message) {
            //addInfo(message.body);
          });

        client.subscribe(infoTopic, function(message) {
            addInfo(message.body);
          });

        client.subscribe(newsTopic, function(message) {
            addNews(message.body);
          });
        };
    //addInfo('Client connect: ' + url);
    client.connect(n, c, onconnect);
</script>

                    }", false);
            }
        }

        //private static void BuildMenu()
        //{
        //    //if (user != "" & member != "" & password !="")
        //    //{
        //    //    UserService wsU = new UserService();
        //    //    dtUserSkdMain dtUSM = wsU.SearchByUserAndMember(user, member);
        //    //    UserGroupPermissionService wsUGP = new UserGroupPermissionService();

        //    //    dtMenuList[] dtML = wsUGP.getMenu();
        //    //    for (int n = 0; n < dtML.Length; n++)
        //    //    {
        //    //        dtUserGroupPermission[] dtUGP = wsUGP.ViewByGroupIdAndMenuId(dtUSM.groupId, dtML[n].idMenu);
        //    //        TreeViewNode parentNode;
        //    //        TreeViewNode newNode;
        //    //        if (dtUGP != null)
        //    //        {
        //    //            parentNode = treeMenu.Nodes.Add();
        //    //            parentNode.Text = dtML[n].nameMenu;
        //    //            for (int m = 0; m < dtUGP.Length; m++)
        //    //            {
        //    //                if (dtUGP[m].reading)
        //    //                {
        //    //                    newNode = parentNode.Nodes.Add();
        //    //                    newNode.Text = dtUGP[m].formList.name;
        //    //                    newNode.Name = dtUGP[m].formList.url+"?id="+dtUGP[m].id;
        //    //                }
        //    //            }

        //    //        }
        //    //    }
        //    //}
        //}

        protected void treeMenu_NodeClick(object source, DevExpress.Web.ASPxTreeView.TreeViewNodeEventArgs e)
        {
            Session["MENU_CLICK"] = true;

            //TODO get Content URL from database or something
            if (e.Node.Text == "Welcome")
                mainSplitter.GetPaneByName("contentPane").ContentUrl = "~/Welcome.aspx";
            //Navigation.Navigate(this, "Welcome.aspx");
            else
                if (e.Node.Text == "Trade")
                    mainSplitter.GetPaneByName("contentPane").ContentUrl = "~/transaction/trade/trade.aspx";
        }

        protected void buttonLogout_Click(object sender, EventArgs e)
        {
            ImqSession.Logout(this, Session.SessionID);
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
        }

        protected void infoList_DataBinding(object sender, EventArgs e)
        {

        }

        public void GetCurrentTime()
        {

        }

        private indikator[] getIndikator()
        //private void getData()
        {
            var url = "http://192.168.138.19:8090/indikatoralert";
            try {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                // invoke the REST method
                byte[] data = client.DownloadData(url);
                // put the downloaded data in a memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                // deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<indikator>));
                List<indikator> result = ser.ReadObject(ms) as List<indikator>;

                chart = result.ToArray();

                temp = chart.Length;
                // Label1.Text = result[0].actualValue.ToString();

                return chart;
            }
            catch (Exception ex)
            {
                msg_service = ex.Message;
                return null;
            }
           
           
        }

        private void ShowMessage(string info)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information');
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void getNotify()
        {
            getIndikator();

            if (chart != null)
            {
                string[] str1 = new string[temp];
                string[] str2 = new string[temp];
                string[] str3 = new string[temp];


                for (x = 0; x < temp; x++)
                {
                    str1[x] = chart[x].memberId.ToString();
                    str2[x] = chart[x].sid.ToString();
                    str3[x] = chart[x].actualValue.ToString();
                }

                for (int i = 0; i < temp; i++)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify" + i, "notifyMe('" + str1[i] + "','" + str2[i] + "','" + str3[i] + "','" + i + "');", true);
                }
            }
            else {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + msg_service + "\");", true);
            }
            
        }

        protected void timerNotif_Tick(object sender, EventArgs e)
        {
            if (Session["SESSION_USERMEMBER"].ToString() == "kpei")
                getNotify();
        }
    }
}
