﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testDateTime.aspx.cs" Inherits="imq.kpei.testDateTime" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    function startTime() {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            var returned = (new Date).getTime();
            if (request.readyState === 4 && request.status === 200) {
                var timestamp = request.responseText.split('|');
                var d = timestamp[0];
                var Mo = timestamp[1];
                var y = timestamp[2];
                var h = timestamp[3];
                var m = timestamp[4];
                var s = timestamp[5];
                // so the server time will be client time + difference
                document.getElementById('dateTime').innerHTML = d + "/" + Mo + "/" + y + "  " + h + ":" + m;
                return timeoffset;

            }
        }
        request.open("POST", "getDateTime.aspx", true);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.send("original=" + (new Date).getTime());

    }

    function time() {
        startTime();
            setTimeout(time, 1000 * 10);
    }

    setTimeout(time, 100);

</script>
<head runat="server">
    <title></title>
</head>
<body onload="startTime()">
    <form id="form1" runat="server">
    <div>
    <div id="dateTime" style="padding-left:150px; color:#FF0000 "></div>
    </div>
    </form>
</body>
</html>
