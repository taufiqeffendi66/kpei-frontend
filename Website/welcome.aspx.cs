﻿using System;
using System.Collections.Generic;
using Common;

namespace imq.kpei
{
    public partial class welcome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
        }
    }
}
