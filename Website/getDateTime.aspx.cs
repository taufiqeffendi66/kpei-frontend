﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace imq.kpei
{
    public partial class getDateTime : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DateString.Text = string.Format("{0}", DateTime.Now.ToString("dd|MM|yyyy|HH|mm|ss"));
        }
    }
}