﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.tools
{
    public partial class systemparameterReport : DevExpress.XtraReports.UI.XtraReport
    {
        public systemparameterReport()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "param");
            cl02.DataBindings.Add("Text", DataSource, "value");
            cl03.DataBindings.Add("Text", DataSource, "remark");
        }
    }
}
