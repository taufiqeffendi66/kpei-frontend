﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="audittrail.aspx.cs" Inherits="imq.kpei.tools.audittrail" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div id="content">
        <div class="title">AUDIT TRAIL</div>
        <table>
            <tr>
<!--
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnPDF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to PDF"><img src="../Styles/pdf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnXLS" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to XLS"><img src="../Styles/excel.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnRTF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to RTF"><img src="../Styles/rtf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnCSV" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to CSV"><img src="../Styles/csv.png" /></asp:LinkButton></td>
-->
                    <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" AutoPostBack="false" onclick="btnCsvExport_Click"> </dx:ASPxButton></td>

            </tr>
        </table>
        <table>
            <tr>
                <td>User</td>
                <td> <dx:ASPxTextBox ID="dtUser" runat="server" Width="170px"> </dx:ASPxTextBox> </td>
                <td><dx:ASPxLabel ID="dtID" ClientInstanceName="clID" runat="server" Text="" Visible="False"></dx:ASPxLabel></td>
            </tr>
            <tr>
                <td>Module</td>
                <td><dx:ASPxTextBox ID="dtModule" runat="server" Width="170px"> </dx:ASPxTextBox></td>
                <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click"> </dx:ASPxButton></td>
            </tr>
        </table>
       
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView"></dx:ASPxGridViewExporter>
       
        <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" 
            AutoGenerateColumns="False" SettingsPager-PageSize="20" 
            CssClass="grid">
            <Columns>
                <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Date" FieldName="dateTime"><PropertiesTextEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss"></PropertiesTextEdit></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Module" FieldName="module"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="3" Caption="User" FieldName="userSkd"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Actifity" FieldName="activity"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Memo" FieldName="memo"></dx:GridViewDataTextColumn>
            </Columns>                
            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
            <SettingsPager PageSize="15"></SettingsPager>
            <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" 
                VerticalScrollableHeight="300" />
            <Styles>
                <AlternatingRow Enabled="True">
                </AlternatingRow>
            </Styles>
        </dx:ASPxGridView>
    </div>


</asp:Content>
