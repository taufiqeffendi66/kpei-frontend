﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.tools
{
    public partial class scheduleReport : DevExpress.XtraReports.UI.XtraReport
    {
        public scheduleReport()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "time");
            cl02.DataBindings.Add("Text", DataSource, "module");
            cl03.DataBindings.Add("Text", DataSource, "remark");
        }
    }
}
