﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="messages.aspx.cs" Inherits="imq.kpei.tools.messages" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">Message</div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxMemo ID="dtMemo" runat="server" Height="71px" Width="300px"></dx:ASPxMemo></td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><dx:ASPxTextBox ID="dtMessage" runat="server" Width="170px"></dx:ASPxTextBox></td>
                                <td><dx:ASPxButton ID="btnSend" runat="server" Text="Send"></dx:ASPxButton></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
