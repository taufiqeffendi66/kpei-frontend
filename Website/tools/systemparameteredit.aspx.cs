﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Common;
using Common.wsSysParam;
using Common.wsApproval;
using Common.wsUser;
using Common.wsUserGroupPermission;

namespace imq.kpei.tools
{
    public partial class systemparameteredit : System.Web.UI.Page
    {
        private int stat;
        private dtSysParamMain[] sysparams;
        private dtSysParamTmp aDT = new dtSysParamTmp();
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private String moduleName = "System Parameter";
        private String target = @"../tools/systemparameter.aspx";
        private String targetApproval = @"../administration/approval.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (Request.QueryString["stat"] == null)
            {
                Response.Redirect("~/tools/systemparameter.aspx");
                stat = -1;
            }
            else
                stat = int.Parse(Request.QueryString["stat"].ToString());

            if (!IsPostBack)
            {
                btnOk.Visible = false;
                btnChecker.Visible = false;
                btnApprovel.Visible = false;
                btnReject.Visible = false;

                switch (stat)
                {
                    case 0:
                        lbltitle.Text = "System Parameter New";
                        btnOk.Visible = true;
                        Permission(true);
                        break;

                    case 1:
                        lbltitle.Text = "Edit System Parameter";
                        btnOk.Visible = true;
                        Permission(true);

                        dtParam.Value = Request.QueryString["param"].ToString();
                        dtValue.Text = Request.QueryString["value"].ToString();
                        dtRemark.Text = Request.QueryString["remark"].ToString();
                        dtDescription.Text = Request.QueryString["description"].ToString();
                        break;

                    case 2:
                        lbltitle.Text = "System Parameter New";
                        Permission(false);
                        ToEditor();

                        dtParam.Enabled = false;
                        dtRemark.Enabled = false;
                        dtValue.Enabled = false;
                        dtDescription.Enabled = false;
                        break;

                    case 3:
                        lbltitle.Text = "Edit System Parameter";
                        btnOk.Visible = false;
                        btnChecker.Visible = false;
                        ToEditor();
                        Permission(false);

                        dtParam.Enabled = false;
                        dtRemark.Enabled = false;
                        dtValue.Enabled = false;
                        dtDescription.Enabled = false;
                        break;

                    case 4: //remove
                        lbltitle.Text = "Remove System Parameter";
                        btnOk.Visible = true;
                        Permission(true);
                        dtParam.Value = Request.QueryString["param"].ToString();
                        dtValue.Text = Request.QueryString["value"].ToString();
                        dtRemark.Text = Request.QueryString["remark"].ToString();
                        dtDescription.Text = Request.QueryString["description"].ToString();
                        break;
                }
            }
        }

        protected void ToEditor()
        {
            SysParamService wsSP = ImqWS.GetSysParamService();
            //dtSysParamTmp aDT = new dtSysParamTmp();
            try
            {
                aDT = wsSP.SearchTmp( int.Parse(Request.QueryString["idxTmp"].ToString() ));
                dtParam.Value = aDT.param;
                dtValue.Text = aDT.value;
                dtRemark.Text = aDT.remark;
                dtDescription.Text = aDT.description;
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsSP.Abort();
                throw;
            }
        }


        protected void getSearch(String tbl, String id)
        {
            SysParamService wsSP = ImqWS.GetSysParamService();
            try
            {
                sysparams = wsSP.Search(id, "", "");
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsSP.Abort();
                throw;
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            SysParamService wsSP = ImqWS.GetSysParamService();
            ApprovalService wsA = ImqWS.GetApprovalWebService();

            try
            {
                //addedit = int.Parse(Session["stat"].ToString());
                int idRec;

                //dtSysParamTmp aDT = new dtSysParamTmp();
                aDT.param = dtParam.Text;
                aDT.remark = dtRemark.Text;
                aDT.value = dtValue.Text;
                aDT.description = dtDescription.Text;
                idRec = wsSP.AddToTmp(aDT);
                if (idRec != 0)
                {
                    dtApproval dtA = new dtApproval();
                    dtA.type = "c";
                    dtA.makerDate = DateTime.Now;
                    dtA.makerName = aUserLogin;
                    dtA.makerStatus = "Ok";
                    dtA.idxTmp = idRec;
                    dtA.form = "~/tools/systemparameteredit.aspx";
                    dtA.memberId = aUserMember;
                    dtA.status = "M";
                    int idTable;
                    switch (stat)
                    {
                        case 0 :
                            dtA.topic = "Add System Parameter";
                            dtA.insertEdit = "I";
                            aDT.id = idRec;
                            break;

                        case 1 :
                            dtA.topic = "Edit System Parameter";
                            dtA.insertEdit = "E";
                            idTable = int.Parse(Request.QueryString["id"].ToString());
                            dtA.idTable = idTable;
                            aDT.id = idTable;
                            break;

                        case 4 :
                            dtA.topic = "Remove System Parameter";
                            dtA.insertEdit = "R";
                            idTable = int.Parse(Request.QueryString["id"].ToString());
                            dtA.idTable = idTable;
                            aDT.id = idTable;
                            break;
                    }
                    if ((dtParam.Text == "") || (dtValue.Text == "") || (dtRemark.Text == ""))
                    {
                        lblError.Text = "Data should not be blank !";
                        if ((dtParam.Text == ""))
                            lbParamError.Text = "*";
                        else
                            lbParamError.Text = "";

                        if (dtValue.Text == "")
                            lbValueError.Text = "*";
                        else
                            lbValueError.Text = "";

                        if (dtRemark.Text == "")
                            lbRemarkError.Text = "*";
                        else
                            lbRemarkError.Text = "";
                    }
                    else
                    {
                        rblApprovalChanged(dtA, wsA, aDT);
                        //Response.Redirect("~/tools/systemparameter.aspx");
                    }
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsSP.Abort();
                wsA.Abort();
                throw;
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            SysParamService wsSP = ImqWS.GetSysParamService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].checkerName = aUserLogin;
                dtA[0].checkerStatus = "Checked";
                dtA[0].approvelStatus = "To Be Approved";
                dtA[0].status = "C";
                wsA.UpdateByChecker( dtA[0] );

                aDT = wsSP.SearchTmp(dtA[0].idxTmp);
                AuditTrail(dtA[0].insertEdit, "Succes ", "Checker ", aDT, targetApproval);
                //switch (dtA[0].insertEdit)
                //{
                //    case "I": putAuditTrail("Checker New System Parameter", toString(aDT)); break;
                //    case "E": putAuditTrail("Checker Edit System Parameter", toString(aDT)); break;
                //    case "R": putAuditTrail("Checker Remove System Parameter", toString(aDT)); break;
                //}
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }

        protected void btnApprovel_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            SysParamService wsSP = ImqWS.GetSysParamService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].approvelName = aUserLogin;
                dtA[0].approvelStatus = "Approved";
                dtA[0].status = "A";
                wsA.UpdateByApprovel(dtA[0]);

                aDT = wsSP.SearchTmp(dtA[0].idxTmp);
                //AuditTrail(dtA[0].insertEdit, "Succes ", "Approval ", aDT, targetApproval);

                Approval(wsSP, dtA[0], "");

                //switch (dtA[0].insertEdit)
                //{
                //    case "I": 
                //        wsSP.AddFromTmp(dtA[0].idxTmp); 
                //        putAuditTrail("Approval New System Parameter", toString(aDT));
                //        ShowMessage("Approval New  System Parameter", target);
                //        break;

                //    case "E":
                //        wsSP.Edit(dtA[0].idxTmp, dtA[0].idTable); 
                //        putAuditTrail("Approval Edit System Parameter", toString(aDT)); 
                //        ShowMessage("Approval Edit  System Parameter", target);
                //        break;

                //    case "R": 
                //        wsSP.RemoveById(dtA[0].idTable); 
                //        putAuditTrail("Approval Remove System Parameter", toString(aDT));
                //        ShowMessage("Approval Remove  System Parameter", target);
                //        break;
                //}
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                wsSP.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //addedit = int.Parse(Session["stat"].ToString());
            switch (stat)
            {
                case 0:
                case 1:
                    Response.Redirect("~/tools/systemparameter.aspx");
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx");
                    break;
            }
        }

        void AuditTrail(string insertEdit, string actifity, string aS, dtSysParamTmp dT, String aTarget)
        {
            switch (insertEdit)
            {
                case "I":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " New " + moduleName, toString(dT));
                    ShowMessage(actifity + " New " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " New "+moduleName, toString(dT)); break;
                case "E":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Edit " + moduleName, toString(dT));
                    ShowMessage(actifity + " Edit " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Edit Calendar", toString(dT)); break;
                case "R":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Remove " + moduleName, toString(dT));
                    ShowMessage(actifity + " Remove " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Remove Calendar", toString(dT)); break;
            }
        }

        private void putAuditTrail(String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "System Parameter", aActifity, aMemo);
        }

        public String toString(dtSysParamTmp aDT)
        {
            return "[id=" + String.Format("{0}", aDT.id) 
                + ", param=" + aDT.param 
                + ", remark=" + aDT.remark
                + ", value=" + aDT.value + "]";
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Parameter");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (isEditor)
                {
                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editMaker)
                            rblApproval.Items.Add("Maker", 0);

                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editDirectChecker)
                            rblApproval.Items.Add("Direct Checker", 1);

                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editDirectApproval)
                            rblApproval.Items.Add("Direct Approval", 2);

                    if (rblApproval.Items.Count > 0)
                        rblApproval.SelectedIndex = 0;
                }
                else
                {
                    if (!(Request.QueryString["makerName"].Equals(aUserLogin) || Request.QueryString["checkerName"].Equals(aUserLogin)) && (Request.QueryString["memberId"].Equals(aUserMember)))
                    {
                        if ((Request.QueryString["status"].ToString() == "M") && (dtFMP[n].editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if ((Request.QueryString["status"].ToString() == "C") && (dtFMP[n].editApproval))
                            {
                                btnApprovel.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                alert('" + info + @"');
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        protected void rblApprovalChanged(dtApproval dtA, ApprovalService wsA, dtSysParamTmp aDT)
        {
            switch ((String)rblApproval.SelectedItem.Value)
            {
                //Meker
                case "0":
                    wsA.AddMaker(dtA);
                    AuditTrail(dtA.insertEdit, "Succes ", "Maker ", aDT, target);
                    //switch (dtA.insertEdit)
                    //{
                    //    case "I": putAuditTrail("New  System Parameter", toString(aDT)); break;
                    //    case "E": putAuditTrail("Edit  System Parameter", toString(aDT)); break;
                    //    case "R": putAuditTrail("Remove  System Parameter", toString(aDT)); break;
                    //}
                    break;

                case "1":
                    wsA.AddDirectChecker(dtA);
                    AuditTrail(dtA.insertEdit, "Succes ", "Direct Checker ", aDT, target);
                    //switch (dtA.insertEdit)
                    //{
                    //    case "I": putAuditTrail("Direct Checker New  System Parameter", toString(aDT)); break;
                    //    case "E": putAuditTrail("Direct Checker Edit  System Parameter", toString(aDT)); break;
                    //    case "R": putAuditTrail("Direct Checker Remove  System Parameter", toString(aDT)); break;
                    //}
                    break;

                //Direct Approval
                case "2":
                    SysParamService wsSP = ImqWS.GetSysParamService();
                    wsA.AddDirectApproval(dtA);
                    //AuditTrail(dtA.insertEdit, "Succes ", "Direct Approval ", aDT, target);
                    Approval(wsSP, dtA, "Direct ");
                    //switch (dtA.insertEdit)
                    //{
                    //    case "I":
                    //        wsSP.AddFromTmp(dtA.idxTmp); 
                    //        putAuditTrail("Direct Approval New  System Parameter", toString(aDT));
                    //        ShowMessage("Direct Approval New  System Parameter", target);
                    //        break;
                    //    case "E": 
                    //        wsSP.Edit(dtA.idxTmp, dtA.idTable); 
                    //        putAuditTrail("Direct Approval Edit  System Parameter", toString(aDT));
                    //        ShowMessage("Direct Approval Edit  System Parameter", target);
                    //        break;
                    //    case "R": 
                    //        wsSP.RemoveById(dtA.idTable); 
                    //        putAuditTrail("Direct Approval Remove  System Parameter", toString(aDT));
                    //        ShowMessage("Direct Approval Remove  System Parameter", target);
                    //        break;
                    //}
                    break;
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                switch (dtA[0].status)
                {
                    case "M":
                        dtA[0].checkerName = aUserLogin;
                        dtA[0].checkerStatus = "Reject";
                        dtA[0].status = "RC";
                        wsA.UpdateByChecker(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Checker", aDT, targetApproval);
                        //switch (dtA[0].insertEdit)
                        //{
                        //    case "I": putAuditTrail("Checker Reject New System Parameter", toString(aDT)); break;
                        //    case "E": putAuditTrail("Checker Reject Edit System Parameter", toString(aDT)); break;
                        //    case "R": putAuditTrail("Checker Reject Remove System Parameter", toString(aDT)); break;
                        //}
                        break;

                    case "C":
                        dtA[0].approvelName = aUserLogin;
                        dtA[0].approvelStatus = "Reject";
                        dtA[0].status = "RA";
                        wsA.UpdateByApprovel(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Approval", aDT, targetApproval);
                        //switch (dtA[0].insertEdit)
                        //{
                        //    case "I": putAuditTrail("Approvel Reject New System Parameter", toString(aDT)); break;
                        //    case "E": putAuditTrail("Approvel Reject Edit System Parameter", toString(aDT)); break;
                        //    case "R": putAuditTrail("Approvel Reject Remove System Parameter", toString(aDT)); break;
                        //}
                        break;
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }

        private void Approval(SysParamService ws, dtApproval dtA, string aDirect)
        {
            switch (dtA.insertEdit )
            {
                case "I":
                    ws.AddFromTmp(dtA.idxTmp);
                    putAuditTrail(aDirect + "Approval New System Parameter", toString(aDT));
                    ShowMessage(aDirect + "Approval New  System Parameter", target);
                    break;

                case "E":
                    ws.Edit(dtA.idxTmp, dtA.idTable);
                    putAuditTrail(aDirect + "Approval Edit System Parameter", toString(aDT));
                    ShowMessage(aDirect + "Approval Edit  System Parameter", target);
                    break;

                case "R":
                    ws.RemoveById(dtA.idTable);
                    putAuditTrail(aDirect + "Approval Remove System Parameter", toString(aDT));
                    ShowMessage(aDirect + "Approval Remove  System Parameter", target);
                    break;
            }
        }
    }
}
