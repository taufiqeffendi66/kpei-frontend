﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.tools
{
    public partial class audittrailReport : DevExpress.XtraReports.UI.XtraReport
    {
        public audittrailReport()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "dateTime");
            cl02.DataBindings.Add("Text", DataSource, "module");
            cl03.DataBindings.Add("Text", DataSource, "userSkd");
            cl04.DataBindings.Add("Text", DataSource, "actifity");
            cl05.DataBindings.Add("Text", DataSource, "memo");
        }
    }
}
