﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.wsAuditTrail;
using System.Data;
using Common.wsUser;
using Common;

namespace imq.kpei.tools
{
    public partial class audittrail : System.Web.UI.Page
    {
        private dtAuditTrail[] arrayDatas;
        DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            gridExport.FileName = "AuditTrail-" + DateTime.Now.ToString(ImqSession.GetConfig("FileNameFormat"));

            if (!IsPostBack && !IsCallback)
            {
                tbl = GetTable();
                Session["tbl"] = tbl;
            }
            else
                tbl = (DataTable)Session["tbl"];

            gridView.DataSource = tbl;
            gridView.KeyFieldName = "id";
            gridView.DataBind(); 
        }

        DataTable GetTable()
        {
            getData();

            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            table = new DataTable();
            table.Columns.Add("id", typeof(String));
            table.Columns.Add("dateTime", typeof(DateTime));
            table.Columns.Add("module", typeof(String));
            table.Columns.Add("userSkd", typeof(String));
            table.Columns.Add("activity", typeof(String));
            table.Columns.Add("memo", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["id"] };
            arrayToTable(table);

            return table;
        }

        private void arrayToTable(DataTable table)
        {
            table.Clear();
            if (arrayDatas != null)
                for (int n = 0; n < arrayDatas.Length; n++)
                {
                    table.Rows.Add(arrayDatas[n].id,
                        arrayDatas[n].dateTime,
                        arrayDatas[n].module,
                        arrayDatas[n].userSkd,
                        arrayDatas[n].actifity,
                        arrayDatas[n].memo);
                }
        }

        private void getData()
        {
            AuditTrailService wsAT = ImqWS.GetAuditTrailService();
            try
            {
                arrayDatas = wsAT.View();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsAT.Abort();
                throw;
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AuditTrailService wsAT = ImqWS.GetAuditTrailService();
            try
            {
                arrayDatas = wsAT.SearchAuditTrail(dtUser.Text, dtModule.Text);
                arrayToTable(tbl);
                gridView.DataBind();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsAT.Abort();
                throw;
            }

        }

        private void putAuditTrail(String aUser, String aModule, String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", aActifity, aMemo);
        }

        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            Session["data"] = tbl;
            Response.Redirect("~/tools/toolsExport.aspx?type=audittrail");
        }
    }
}