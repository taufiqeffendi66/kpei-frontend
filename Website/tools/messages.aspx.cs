﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace imq.kpei.tools
{
    public partial class messages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
        }
    }
}
