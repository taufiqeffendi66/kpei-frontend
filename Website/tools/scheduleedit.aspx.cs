﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using DevExpress.Web.ASPxEditors;

using Common;
using Common.wsSchedule;
using Common.wsApproval;
using Common.wsUser;
using Common.wsUserGroupPermission;

namespace imq.kpei.tools
{
    public partial class scheduleedit : System.Web.UI.Page
    {
        private int stat;
        private dtScheduleMain[] dtSs = null;
        private dtScheduleTmp aDT = new dtScheduleTmp();
        private ScheduleService wsS = new ScheduleService();
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private String moduleName = "Schedule";
        private String target = @"../tools/schedule.aspx";
        private String targetApproval = @"../administration/approval.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (Request.QueryString["stat"] == null)
            {
                Response.Redirect("~/tools/schedule.aspx");
                stat = -1;
            }
            else
                stat = int.Parse(Request.QueryString["stat"].ToString());

            if (!IsPostBack)
            {
                //addedit = int.Parse(Session["stat"].ToString());
                btnOk.Visible = false;
                btnChecker.Visible = false;
                btnApprovel.Visible = false;
                btnReject.Visible = false;
                InitPage();
                switch (stat)
                {
                    case 0:
                        lbltitle.Text = "Schedule New";
                        btnOk.Visible = true;
                        Permission( true );
                        break;

                    case 1:
                        lbltitle.Text = "Edit Schedule";
                        btnOk.Visible = true;
                        Permission( true );

                        dtTime.Value = Request.QueryString["time"].ToString();
                        //dtModuleList.SelectedIndex = (int)row["idModule"];
                        dtModuleList.SelectedItem = dtModuleList.Items.FindByText(Request.QueryString["module"].ToString());
                        //dtModuleList.Text = row["module"].ToString();
                        dtRemark.Text = Request.QueryString["remark"].ToString();
                        break;

                    case 2:
                        lbltitle.Text = "Schedule New";
                        Permission( false );
                        ToEditor();

                        dtModuleList.Enabled = false;
                        dtRemark.Enabled = false;
                        dtTime.Enabled = false;
                        break;

                    case 3:
                        lbltitle.Text = "Edit Schedule";
                        btnOk.Visible = false;
                        btnChecker.Visible = false;
                        ToEditor();
                        Permission( false );

                        dtModuleList.Enabled = false;
                        dtRemark.Enabled = false;
                        dtTime.Enabled = false;
                        break;

                    case 4: //remove
                        lbltitle.Text = "Remove Schedule";
                        btnOk.Visible = true;
                        Permission(true);
                        dtTime.Value = Request.QueryString["time"].ToString();
                        dtModuleList.Text = Request.QueryString["module"].ToString();
                        dtRemark.Text = Request.QueryString["remark"].ToString();
                        break;
                }
            }
        }

        protected void InitPage()
        {
            ScheduleService wsS = ImqWS.GetScheduleService();
            try
            {
                // get List Member
                dtModuleList.Items.Clear();
                dtListModuleSchedule[] dtLMS;
                dtLMS = wsS.getListModuleSchedule();
                for (int i = 0; i < dtLMS.Length; i++)
                    dtModuleList.Items.Add(dtLMS[i].module, dtLMS[i].id);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsS.Abort();
                throw;
            }
        }


        protected void ToEditor()
        {
            ScheduleService wsS = ImqWS.GetScheduleService();
            try
            {
                aDT = wsS.SearchTmp(int.Parse(Request.QueryString["idxTmp"].ToString()));
                dtTime.Value = aDT.time;
                dtModuleList.SelectedIndex = aDT.idModule;
                dtRemark.Text = aDT.remark;
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsS.Abort();
                throw;
            }
        }

        DateTime StrToDateTime(String str)
        {
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            MyDateTime = Convert.ToDateTime(str);
            return MyDateTime;
        }

        protected void getSearch(String id)
        {
            ScheduleService wsS = ImqWS.GetScheduleService();
            try
            {
                dtSs = wsS.Search("", id);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsS.Abort();
                throw;
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if ((dtTime.Text != "") && (dtModuleList.Text != "") && (dtRemark.Text != ""))
            {
                ApprovalService wsA = ImqWS.GetApprovalWebService();
                ScheduleService wsS = ImqWS.GetScheduleService();

                try
                {
                    //addedit = int.Parse(Session["stat"].ToString());

                    int idRec;

                    dtScheduleTmp aDT = new dtScheduleTmp();
                    aDT.idModule = int.Parse(dtModuleList.SelectedItem.Value.ToString());
                    aDT.time = dtTime.DateTime;
                    aDT.timeSpecified = true;
                    aDT.remark = dtRemark.Text;
                    idRec = wsS.AddToTmp(aDT);
                    if (idRec != 0)
                    {
                        dtApproval dtA = new dtApproval();
                        dtA.type = "c";
                        dtA.makerDate = DateTime.Now;
                        dtA.makerName = aUserLogin;
                        dtA.makerStatus = "Ok";
                        dtA.idxTmp = idRec;
                        dtA.form = "~/tools/scheduleedit.aspx";
                        dtA.memberId = aUserMember;
                        dtA.status = "M";
                        int idTable;// = int.Parse(row["id"].ToString());
                        switch (stat)
                        {
                            case 0:
                                dtA.topic = "Add Schedule";
                                dtA.insertEdit = "I";
                                aDT.id = idRec;
                                break;

                            case 1:
                                dtA.topic = "Edit Schedule";
                                dtA.insertEdit = "E";
                                idTable = int.Parse(Request.QueryString["id"].ToString());
                                dtA.idTable = idTable;
                                aDT.id = idTable;
                                break;

                            case 4:
                                dtA.topic = "Remove Schedule";
                                dtA.insertEdit = "R";
                                idTable = int.Parse(Request.QueryString["id"].ToString());
                                dtA.idTable = idTable;
                                aDT.id = idTable;
                                break;
                        }
                        if ((dtTime.Text == "") || (dtModuleList.Text == "") || (dtRemark.Text == ""))
                        {
                            lblError.Text = "Data should not be blank !";
                            if ((dtTime.Text == ""))
                                lbTimeError.Text = "*";
                            else
                                lbTimeError.Text = "";
                            if (dtModuleList.Text == "")
                                lbModuleError.Text = "*";
                            else
                                lbModuleError.Text = "";
                            if (dtRemark.Text == "")
                                lbRemarkError.Text = "*";
                            else
                                lbRemarkError.Text = "";
                        }
                        else
                        {
                            rblApprovalChanged(dtA, wsA, aDT, target);
                            //Response.Redirect("~/tools/schedule.aspx");
                        }
                    }
                }
                catch (TimeoutException te)
                {
                    te.ToString();
                    wsS.Abort();
                    wsA.Abort();
                    throw;
                }
            }
            else
            {
                lblError.Text = "Data should not be blank !";
                if ((dtTime.Text == ""))
                    lbTimeError.Text = "*";
                else
                    lbTimeError.Text = "";
                if (dtModuleList.Text == "")
                    lbModuleError.Text = "*";
                else
                    lbModuleError.Text = "";
                if (dtRemark.Text == "")
                    lbRemarkError.Text = "*";
                else
                    lbRemarkError.Text = "";
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            ScheduleService wsS = ImqWS.GetScheduleService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].checkerName = aUserLogin;
                dtA[0].checkerStatus = "Checked";
                dtA[0].approvelStatus = "To Be Approved";
                dtA[0].status = "C";
                wsA.UpdateByChecker( dtA[0] );

                aDT = wsS.SearchTmp(dtA[0].idxTmp);
                AuditTrail(dtA[0].insertEdit, "Succes ", "Checker ", aDT, targetApproval);
                //switch (dtA[0].insertEdit)
                //{
                //    case "I": putAuditTrail("Checker New Schedule", toString(aDT)); break;
                //    case "E": putAuditTrail("Checker Edit Schedule", toString(aDT)); break;
                //    case "R": putAuditTrail("Checker Remove Schedule", toString(aDT)); break;
                //}
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                wsS.Abort();
                throw;
            }

            //Response.Redirect("~/account/approval.aspx");
        }

        protected void btnApprovel_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            ScheduleService wsS = ImqWS.GetScheduleService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].approvelName = aUserLogin;
                dtA[0].approvelStatus = "Approved";
                dtA[0].status = "A";
                wsA.UpdateByApprovel(dtA[0]);

                aDT = wsS.SearchTmp(dtA[0].idxTmp);
                Approval(wsS, dtA[0], "");
                //switch (dtA[0].insertEdit)
                //{
                //    case "I": wsS.AddFromTmp(dtA[0].idxTmp);           putAuditTrail("Approval New Schedule", toString(aDT)); break;
                //    case "E": wsS.Edit(dtA[0].idxTmp, dtA[0].idTable); putAuditTrail("Approval Edit Schedule", toString(aDT)); break;
                //    case "R": wsS.RemoveById(dtA[0].idTable);          putAuditTrail("Approval Remove Schedule", toString(aDT)); break;
                //}                
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }

            //Response.Redirect("~/account/approval.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //addedit = int.Parse(Session["stat"].ToString());
            switch (stat)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("~/tools/schedule.aspx");
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx");
                    break;
            }
        }

        void AuditTrail(string insertEdit, string actifity, string aS, dtScheduleTmp dT, String aTarget)
        {
            switch (insertEdit)
            {
                case "I":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " New " + moduleName, toString(dT));
                    ShowMessage(actifity + " New " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " New "+moduleName, toString(dT)); break;
                case "E":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Edit " + moduleName, toString(dT));
                    ShowMessage(actifity + " Edit " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Edit Calendar", toString(dT)); break;
                case "R":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Remove " + moduleName, toString(dT));
                    ShowMessage(actifity + " Remove " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Remove Calendar", toString(dT)); break;
            }
        }

        private void putAuditTrail(String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", aActifity, aMemo);
        }

        public String toString(dtScheduleTmp aDT)
        {
            return "[id=" + String.Format("{0}", aDT.id ) + ", module=" + aDT.idModule + ", remark=" + aDT.remark + ", time=" + String.Format("{0:HH:mm:ss}", aDT.time) + "]";
        }

        private void Permission( bool isEditor )
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Schedule");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (isEditor)
                {
                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editMaker)
                            rblApproval.Items.Add("Maker", 0);

                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editDirectChecker)
                            rblApproval.Items.Add("Direct Checker", 1);

                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editDirectApproval)
                            rblApproval.Items.Add("Direct Approval", 2);

                    if (rblApproval.Items.Count > 0)
                        rblApproval.SelectedIndex = 0;
                }
                else
                {
                    if (!(Request.QueryString["makerName"].Equals(aUserLogin) || Request.QueryString["checkerName"].Equals(aUserLogin)) && (Request.QueryString["memberId"].Equals(aUserMember)))
                    {
                        if ((Request.QueryString["status"].ToString() == "M") && (dtFMP[n].editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if ((Request.QueryString["status"].ToString() == "C") && (dtFMP[n].editApproval))
                            {
                                btnApprovel.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                alert('" + info + @"');
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        protected void rblApprovalChanged(dtApproval dtA, ApprovalService wsA, dtScheduleTmp aDT, String aTarget)
        {
            switch ((String)rblApproval.SelectedItem.Value)
            {
                //Meker
                case "0":
                    wsA.AddMaker(dtA);
                    AuditTrail(dtA.insertEdit, "Succes ", "Maker ", aDT, target);
                    //switch (dtA.insertEdit)
                    //{
                    //    case "I" : putAuditTrail("New Schedule", toString(aDT)); break;
                    //    case "E" : putAuditTrail("Edit Schedule", toString(aDT)); break;
                    //    case "R" : putAuditTrail("Remove Schedule", toString(aDT)); break;
                    //}
                    break;

                case "1":
                    wsA.AddDirectChecker(dtA);
                    AuditTrail(dtA.insertEdit, "Succes ", "Direct Checker ", aDT, target);
                    //switch (dtA.insertEdit)
                    //{
                    //    case "I" : putAuditTrail("Direct Checker New Schedule", toString(aDT)); break;
                    //    case "E" : putAuditTrail("Direct Checker Edit Schedule", toString(aDT)); break;
                    //    case "R" : putAuditTrail("Direct Checker Remove Schedule", toString(aDT)); break;
                    //}
                    break;

                //Direct Approval
                case "2":
                    ScheduleService wsS = ImqWS.GetScheduleService();
                    wsA.AddDirectApproval(dtA);
                    Approval(wsS, dtA, "Direct");
                    //switch (dtA.insertEdit)
                    //{
                    //    case "I": wsS.AddFromTmp(dtA.idxTmp); putAuditTrail("Direct Approval New Schedule", toString(aDT)); break;
                    //    case "E": wsS.Edit(dtA.idxTmp, dtA.idTable); putAuditTrail("Direct Approval Edit Schedule", toString(aDT)); break;
                    //    case "R": wsS.RemoveById(dtA.idTable); putAuditTrail("Direct Approval Remove Schedule", toString(aDT)); break;
                    //}
                    break;
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                switch (dtA[0].status)
                {
                    case "M":
                        dtA[0].checkerName = aUserLogin;
                        dtA[0].checkerStatus = "Reject";
                        dtA[0].status = "RC";
                        wsA.UpdateByChecker(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Checker", aDT, targetApproval);
                        //switch (dtA[0].insertEdit)
                        //{
                        //    case "I": putAuditTrail("Checker Reject New Schedule", toString(aDT)); break;
                        //    case "E": putAuditTrail("Checker Reject Edit Schedule", toString(aDT)); break;
                        //    case "R": putAuditTrail("Checker Reject Remove Schedule", toString(aDT)); break;
                        //}
                        break;

                    case "C":
                        dtA[0].approvelName = aUserLogin;
                        dtA[0].approvelStatus = "Reject";
                        dtA[0].status = "RA";
                        wsA.UpdateByApprovel(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Approval", aDT, targetApproval);
                        //switch (dtA[0].insertEdit)
                        //{
                        //    case "I": putAuditTrail("Approvel Reject New Schedule", toString(aDT)); break;
                        //    case "E": putAuditTrail("Approvel Reject Edit Schedule", toString(aDT)); break;
                        //    case "R": putAuditTrail("Approvel Reject Remove Schedule", toString(aDT)); break;
                        //}
                        break;
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }

        private void Approval(ScheduleService ws, dtApproval dtA, string aDirect)
        {
            switch (dtA.insertEdit)
            {
                case "I":
                    ws.AddFromTmp(dtA.idxTmp);
                    putAuditTrail(aDirect + "Approval New " + moduleName, toString(aDT));
                    ShowMessage(aDirect + "Approval New " + moduleName, target);
                    break;

                case "E":
                    ws.Edit(dtA.idxTmp, dtA.idTable);
                    putAuditTrail(aDirect + "Approval Edit " + moduleName, toString(aDT));
                    ShowMessage(aDirect + "Approval Edit " + moduleName, target);
                    break;

                case "R":
                    ws.RemoveById(dtA.idTable);
                    putAuditTrail(aDirect + "Approval Remove " + moduleName, toString(aDT));
                    ShowMessage(aDirect + "Approval Remove " + moduleName, target);
                    break;
            }
        }
    }
}
