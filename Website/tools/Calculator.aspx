﻿<%@ Page Title="Calculator" Language="C#" AutoEventWireup="true" CodeBehind="Calculator.aspx.cs" Inherits="imq.kpei.tools.Calculator" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content" style="width:600px">
        <div class="title">Calculator</div>
        <div style="width:100%">
            <table>
                <tr>
                    <td>Method</td>
                    <td><dx:ASPxComboBox ID="cmbMethod" runat="server" AutoPostBack="true"
                            onselectedindexchanged="cmbMethod_SelectedIndexChanged">
                        <Items>
                            <dx:ListEditItem Text="Binomial" Value="0" Selected="true"/>
                            <dx:ListEditItem Text="Blackscholes" Value="1" />
                        </Items>
                    </dx:ASPxComboBox></td>
                </tr>
                <tr>
                    <td style="width:240px" nowrap="nowrap" >Enter Price Of Underlying</td>
                    <td style="width: 100px"><dx:ASPxTextBox ID="txtStockPrice" runat="server" /></td>      
                </tr>
                <tr>
                    <td style="width:240px" nowrap="nowrap">Enter Strike Price</td>
                    <td style="width: 100px"><dx:ASPxTextBox ID="txtStrike" runat="server" /></td>
                </tr>
                <tr>
                    <td valign="top" >Options Type</td>
                    <td style="width: 100px" nowrap="nowrap" width="100">
                        <dx:ASPxRadioButtonList ID="rbType" runat="server" ValueType="System.Int32" 
                            RepeatLayout="Flow" SelectedIndex="0">
                            <Border BorderWidth="0px" />
                            <Items>
                                <dx:ListEditItem Text="Call" Value="0" Selected="True" />
                                <dx:ListEditItem Text="Put" Value="1"  />
                                
                            </Items>
                        </dx:ASPxRadioButtonList>
                     </td>
                </tr>
                <tr>
                    <td nowrap="nowrap">Risk-free rate annual(absolute not percent)</td>
                    <td style="width: 100px">
                        <dx:ASPxTextBox id="txtRate" runat="server"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Volatility</td>
                    <td><dx:ASPxTextBox id="txtVol" runat="server"></dx:ASPxTextBox></td>
                </tr>
                <tr>
                    <td nowrap="nowrap">Time To Expiration(Years)</td>
                    <td>
                        <dx:ASPxTextBox id="txtTimeToExpiration" runat="server"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top">Setlement Method</td>
                    <td style="width: 100px" nowrap="nowrap">
                        <dx:ASPxRadioButtonList ID="rbMethod" runat="server" ValueType="System.Int32" 
                            RepeatLayout="Flow" SelectedIndex="0">
                            <Border BorderWidth="0px" />
                            <Items>
                                <dx:ListEditItem Text="Future" Value="0" Selected="True" />
                                <dx:ListEditItem Text="Options" Value="1" />
                            </Items>
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td valign="top">Style</td>
                    <td style="width: 100px" nowrap="nowrap">
                        <dx:ASPxRadioButtonList ID="rbStyle" runat="server" ValueType="System.Int32" 
                            RepeatLayout="Flow" SelectedIndex="0">
                            <Border BorderWidth="0px" />
                            <Items>
                                <dx:ListEditItem Text="American" Value="0" Selected="True" />
                                <dx:ListEditItem Text="European" Value="1" />
                            </Items>
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>Step</td>
                    <td><dx:ASPxTextBox ID="txtSteps" runat="server" Text="12"/></td>
                </tr>
                <tr>
                    <td><dx:ASPxButton ID="btnCalculate" runat="server" Text="Calculate" 
                            onclick="btnCalculate_Click"></dx:ASPxButton></td>
                </tr>
                <tr>
                    <td>Result</td>
                    <td><dx:ASPxTextBox ID="txtResult" runat="server" ReadOnly="true" /> </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
