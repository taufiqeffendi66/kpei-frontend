﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

using Common;
using Common.wsCalendar;
using Common.wsUserGroupPermission;

namespace imq.kpei.administration
{
    public partial class calendar : System.Web.UI.Page
    {
        private dtCalendarMain[] DMs;
        DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            //gridExport.FileName = "Calendar-" + DateTime.Now.ToString(ImqSession.GetConfig("FileNameFormat"));

            if (!IsPostBack && !IsCallback)
            {
                if (!aUserLogin.Equals("") && !aUserMember.Equals(""))
                    Permission();
                tbl = GetTable();
                Session["tbl"] = tbl;
            }
            else
                tbl = (DataTable)Session["tbl"];

            gridView.KeyFieldName = "ID";
            gridView.DataSource = tbl;
            gridView.DataBind();
        }

        DateTime StrToDateTime(String str)
        {
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            MyDateTime = Convert.ToDateTime(str);
            return MyDateTime;
        }

        DataTable GetTable()
        {
            getData();

            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            table = new DataTable();
            table.Columns.Add("ID", typeof(String));
            table.Columns.Add("date", typeof(DateTime));
            table.Columns.Add("description", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["ID"] };
            if (DMs != null )
            {
                for (int n = 0; n < DMs.Length; n++)
                {
                    table.Rows.Add(DMs[n].id,
                        DMs[n].date,
                        DMs[n].description);
                }
            }
            return table;
        }

        private void getData()
        {
            CalendarService wsC = ImqWS.GetCalendarService();
            try
            {
                DMs = wsC.View();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsC.Abort();
                throw;
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Session["stat"] = "0";
            Response.Redirect("~/tools/calendaredit.aspx?stat=0");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "1";
            //Session["row"] = row;
            //Response.Redirect("~/tools/calendaredit.aspx");

            string strRedirect = "~/tools/calendaredit.aspx?stat=1&" +
                "id=" + Server.UrlEncode(row["ID"].ToString()) + "&" +
                "date=" + Server.UrlEncode(row["date"].ToString()) + "&" +
                "description=" + Server.UrlEncode(row["description"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "4";
            //Session["row"] = row;
            //Response.Redirect("~/tools/calendaredit.aspx");

            string strRedirect = "~/tools/calendaredit.aspx?stat=4&" +
                "id=" + Server.UrlEncode(row["ID"].ToString()) + "&" +
                "date=" + Server.UrlEncode(row["date"].ToString()) + "&" +
                "description=" + Server.UrlEncode(row["description"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            enableBtn(false);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Calendar");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    enableBtn(true);

                if (!btnRemove.Enabled)
                    btnRemove.Enabled = dtFMP[n].removeing;
            }
        }

        private void enableBtn(bool aEnable)
        {
            btnNew.Enabled = aEnable;
            btnEdit.Enabled = aEnable;
        }

        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            Session["data"] = tbl;
            Response.Redirect("~/tools/toolsExport.aspx?type=calendar");
        }
    }
}
