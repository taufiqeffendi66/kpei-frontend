﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.tools
{
    public partial class calendarReport : DevExpress.XtraReports.UI.XtraReport
    {
        public calendarReport()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "date");
            cl02.DataBindings.Add("Text", DataSource, "description");
        }
    }
}
