﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using Common;
using Common.wsUserGroupPermission;
using Common.wsSchedule;

namespace imq.kpei.tools
{
    public partial class schedule : System.Web.UI.Page
    {
        private dtScheduleViewStr[] arrayDatas;
        DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack && !IsCallback)
            {
                if (!aUserLogin.Equals("") && !aUserMember.Equals(""))
                    Permission();
                tbl = GetTable();
                Session["tbl"] = tbl;
            }
            else
                tbl = (DataTable)Session["tbl"];

            gridView.DataSource = tbl;
            gridView.KeyFieldName = "ID";
            gridView.DataBind();
        }

        DataTable GetTable()
        {
            getData();

            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            table = new DataTable();
            table.Columns.Add("ID", typeof(String));
            table.Columns.Add("time", typeof(DateTime));
            table.Columns.Add("module", typeof(String));
            table.Columns.Add("remark", typeof(String));
            //table.Columns.Add("idModule", typeof(int));
            table.PrimaryKey = new DataColumn[] { table.Columns["ID"] };
            if (arrayDatas != null)
                for (int n = 0; n < arrayDatas.Length; n++)
                {
                    table.Rows.Add(arrayDatas[n].id,
                        Convert.ToDateTime(arrayDatas[n].time),
                        arrayDatas[n].module,
                        arrayDatas[n].remark);
              //          arrayDatas[n].listModuleSchedule.id);
                }
            return table;
        }

        private void getData()
        {
            ScheduleService wsS = ImqWS.GetScheduleService();
            try
            {
                arrayDatas = wsS.ViewStr();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsS.Abort();
                throw;
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            //Session["stat"] = "0";
            Response.Redirect("~/tools/scheduleedit.aspx?stat=0");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "1";
            //Session["row"] = row;
            //Response.Redirect("~/tools/scheduleedit.aspx");

            string strRedirect = "~/tools/scheduleedit.aspx?stat=1&" +
                "id=" + Server.UrlEncode(row["ID"].ToString()) + "&" +
                "time=" + Server.UrlEncode(row["time"].ToString()) + "&" +
                "module=" + Server.UrlEncode(row["module"].ToString()) + "&" +
                "remark=" + Server.UrlEncode(row["remark"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "4";
            //Session["row"] = row;
            //Response.Redirect("~/tools/scheduleedit.aspx");
            string strRedirect = "~/tools/scheduleedit.aspx?stat=4&" +
                "id=" + Server.UrlEncode(row["ID"].ToString()) + "&" +
                "time=" + Server.UrlEncode(row["time"].ToString()) + "&" +
                "module=" + Server.UrlEncode(row["module"].ToString()) + "&" +
                "remark=" + Server.UrlEncode(row["remark"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            enableBtn(false);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Schedule");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    enableBtn(true);

                //if (!btnRemove.Enabled) btnRemove.Enabled = dtFMP[n].removeing;
            }
        }

        private void enableBtn(bool aEnable)
        {
            //btnNew.Enabled = aEnable;
            btnEdit.Enabled = aEnable;
        }

        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            Session["data"] = tbl;
            Response.Redirect("~/tools/toolsExport.aspx?type=schedule");
        }
    }
}
