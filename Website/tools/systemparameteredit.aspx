﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="systemparameteredit.aspx.cs" Inherits="imq.kpei.tools.systemparameteredit" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <div id="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="System Parameter Edit" /></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td style="width:128px">Param</td>
                    <td><dx:ASPxTextBox ID="dtParam" ClientInstanceName="clParam" runat="server"  Width="120px" Enabled="False"></dx:ASPxTextBox></td>
                    <td><dx:ASPxLabel ID="lbParamError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
                <tr>
                    <td style="width:128px">Value</td>
                    <td><dx:ASPxTextBox ID="dtValue" ClientInstanceName="clValue" runat="server" Width="120px"></dx:ASPxTextBox></td>
                    <td><dx:ASPxLabel ID="lbValueError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
                <tr>
                    <td style="width:128px">Remark</td>
                    <td><dx:ASPxTextBox ID="dtRemark" ClientInstanceName="clRemark" runat="server" Width="200px" Enabled="False"></dx:ASPxTextBox></td>
                    <td><dx:ASPxLabel ID="lbRemarkError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
                <tr>
                    <td style="width:128px">Format</td>
                    <td><dx:ASPxTextBox ID="dtDescription" ClientInstanceName="clDescription" runat="server" Width="200px" Enabled="False"></dx:ASPxTextBox></td>
                    <td><dx:ASPxLabel ID="lbDescription" runat="server" ForeColor="#ff0000" /></td>
                </tr>
            </table>
         </div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxButton ID="btnOk" runat="server" Text="Ok" onclick="btnOk_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" onclick="btnChecker_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnApprovel" runat="server" Text="Approved" onclick="btnApprovel_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" onclick="btnReject_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></dx:ASPxButton></td>
                </tr>
            </table> 
        </div>
    </div>
</asp:Content>
