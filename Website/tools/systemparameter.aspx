﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="systemparameter.aspx.cs" Inherits="imq.kpei.tools.systemparameter" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[

        function init() {
            btnRemove.SetEnabled(false);
            btnEdit.SetEnabled(false);
        }

        window.onload = init;
        // ]]>
    </script>
    <div id="content">
        <div class="title">SYSTEM PARAMETER</div>
        <div>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                <tr>
<!--
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnPDF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to PDF"><img src="../Styles/pdf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnXLS" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to XLS"><img src="../Styles/excel.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnRTF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to RTF"><img src="../Styles/rtf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnCSV" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to CSV"><img src="../Styles/csv.png" /></asp:LinkButton></td>
-->
                    <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" AutoPostBack="false" onclick="btnCsvExport_Click"> </dx:ASPxButton></td>
                </tr>
            </table>
            <table>
                <tr>
                    
                    <td><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click"> </dx:ASPxButton></td>
                    
                </tr>
            </table>
       
            <dx:ASPxGridView ID="gridView" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" Width="100%">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Param" FieldName="param"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Value" FieldName="value"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Remark" FieldName="remark"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Format" FieldName="description"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="ID" FieldName="ID" Visible="false"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="250" />
                <Styles><AlternatingRow Enabled="True"></AlternatingRow></Styles>
            </dx:ASPxGridView>            
        </div>
    </div>

</asp:Content>
