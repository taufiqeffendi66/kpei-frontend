﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BinomialTreeUI;
using Common;

namespace imq.kpei.tools
{
    public partial class Calculator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                Session["_exceptionCount"] = 0;
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {

            #region  GetFormInformation
            //int  _exceptionCount=0;
            double _stockPrice = 0.0;
            double _strike = 0.0;
            double _timeToExpiration = 0.0;
            double _volatility = 0.0;
            double _riskFreeRate = 0.0;

            EPutCall _putCall = EPutCall.Call;
            EnumStyle _style = EnumStyle.American;
            int _steps = 15;

            double _dividendYield = 0;
            bool _isFuture = false;
            double _dividend1 = 0;
            double _dividend1Date = 0;

            double _dividend2 = 0;
            double _dividend2Date = 0;

            double _dividend3 = 0;
            double _dividend3Date = 0;

            double _dividend4 = 0;
            double _dividend4Date = 0;

            //validate information
            if (IsNotNullAndIsPositiveDouble(txtStockPrice.Text))
                _stockPrice = Convert.ToDouble(txtStockPrice.Text);

            if (IsNotNullAndIsPositiveDouble(txtStrike.Text))
                _strike = Convert.ToDouble(txtStrike.Text);

            if (IsNotNullAndIsPositiveDouble(txtTimeToExpiration.Text))
                _timeToExpiration = Convert.ToDouble(txtTimeToExpiration.Text);

            if (IsNotNullAndIsPositiveDouble(txtVol.Text))
                _volatility = Convert.ToDouble(txtVol.Text);

            if (IsNotNullAndIsPositiveDouble(txtRate.Text))
                _riskFreeRate = Convert.ToDouble(txtRate.Text);

            //if(IsNotNullAndIsPositiveDouble(txtYield.Text))
            //    _dividendYield = Convert.ToDouble(txtYield.Text);

            //if (dividend1.Text!=String.Empty && double.TryParse(dividend1.Text, out _dividend1))
            //{
            //    if(date1.Text!=String.Empty)
            //        _dividend1Date = Convert.ToDouble(date1.Text);
            //}
            //if (dividend2.Text!=String.Empty && double.TryParse(dividend2.Text, out _dividend2))
            //{
            //    if(date2.Text!=String.Empty)
            //        _dividend2Date = Convert.ToDouble(date2.Text);
            //}
            //if (dividend3.Text!=String.Empty && double.TryParse(dividend3.Text, out _dividend3))
            //{
            //    if(date3.Text!=String.Empty)
            //        _dividend3Date = Convert.ToDouble(date3.Text);
            //}
            //if (dividend4.Text!=String.Empty && double.TryParse(dividend4.Text, out _dividend4))
            //{
            //    if(date4.Text!=String.Empty)
            //        _dividend4Date = Convert.ToDouble(date4.Text);
            //}


            if (txtSteps.Text != string.Empty && Int32.TryParse(txtSteps.Text, out _steps) && _steps > 0)
            {
            }
            else
            {
                _steps = 12;
            }

            if (rbType.SelectedIndex == 0)
                _putCall = EPutCall.Call;
            else
                _putCall = EPutCall.Put;

            if (rbStyle.SelectedIndex == 0)
                _style = EnumStyle.American;
            else
                _style = EnumStyle.European;
            if (rbMethod.SelectedIndex == 0)
                _isFuture = true;


            #endregion

            txtResult.Text = String.Empty;
            if (cmbMethod.SelectedIndex == 0)
            {
                BinomialTree tree = new BinomialTree(_stockPrice, _strike, _timeToExpiration,
                        _volatility, _riskFreeRate, _putCall, _style, _steps, _dividendYield, _isFuture,
                        _dividend1, _dividend1Date, _dividend2, _dividend2Date,
                        _dividend3, _dividend3Date, _dividend4, _dividend4Date);

                txtResult.Text = tree.ComputeOptionValues().ToString();
            }
            else
            {
                Blackscholes bs = new Blackscholes();
                txtResult.Text = bs.black_scholes(_stockPrice, _strike, _riskFreeRate, _volatility, _timeToExpiration, _putCall).ToString();
            }
        }

        #region UtilityFunctions
        bool IsNotNull(string value)
        {
            int _exceptionCount = int.Parse(Session["_exceptionCount"].ToString());
            if (value != null && value != String.Empty)
            {
                return true;
            }
            else
            {
                _exceptionCount++;
                return false;
            }
        }
        bool IsNotNullAndIsPositiveDouble(string value)
        {
            try
            {
                int _exceptionCount = int.Parse(Session["_exceptionCount"].ToString());
                if (value != null && value != String.Empty && Convert.ToDouble(value) > 0)
                {
                    return true;
                }
                else
                {
                    _exceptionCount++;
                    return false;
                }
            }
            catch (FormatException)
            {
                txtResult.Text = "An input was not in the correct format";
                return false;
            }
        }
        #endregion

        protected void cmbMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMethod.SelectedIndex == 1)
            {
                rbStyle.Enabled = false;
                rbMethod.Enabled = false;
                txtSteps.Enabled = false;
            }
            else
            {
                rbMethod.Enabled = true;
                txtSteps.Enabled = true;
                rbStyle.Enabled = true;
            }
        }
    }
}
