﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using Common;

//using Common.wsCalendar;
using Common.wsSchedule;
using Common.wsSysParam;
using Common.wsAuditTrail;
using System.Data;
using System.Collections;

namespace imq.kpei.tools
{
    public partial class toolsExport : System.Web.UI.Page
    {
        public dtAuditTrail[] dtAuditTrails;
        //public dtCalendarMain[] dtCalendars;
        public dtScheduleMain[] dtSchedules;
        public dtSysParamMain[] dtSysParams;
        private DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;
        private String dataType;
        ArrayList listDataSource = new ArrayList();

        struct dtCalendarMain
        {
            string _date;
            public string date
            {
                get
                {
                    return _date;
                }
                set
                {
                    _date = value;
                }
            }

            string _description;
            public string description
            {
                get
                {
                    return _description;
                }
                set
                {
                    _description = value;
                }
            }
        };

        struct printSchedule
        {
            string _time;
            public string time
            {
                get
                {
                    return _time;
                }
                set
                {
                    _time = value;
                }
            }

            string _module;
            public string module
            {
                get
                {
                    return _module;
                }
                set
                {
                    _module = value;
                }
            }

            string _remark;
            public string remark
            {
                get
                {
                    return _remark;
                }
                set
                {
                    _remark = value;
                }
            }
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //dataType = Request.QueryString["type"];

        }

        DateTime StrToDateTime(String str)
        {
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            MyDateTime = Convert.ToDateTime(str);
            return MyDateTime;
        }

        protected override void OnInit(EventArgs e)
        {
            dataType = Request.QueryString["type"];

            base.OnInit(e);
            if (IsPostBack)
            {
                tbl = (DataTable)Session["tbl"];
            }
            switch (dataType)
            {
                case "audittrail":
                    if (tbl != null)
                        // Create a list.                         
                        for (int n = 0; n < tbl.Rows.Count; n++)
                        {
                            DataRow aRow = tbl.Rows[n];
                            dtAuditTrail aData = new dtAuditTrail();
                            aData.dateTime = (DateTime)aRow["dateTime"];
                            aData.userSkd = aRow["userSkd"].ToString();
                            aData.module = aRow["module"].ToString();
                            aData.memo = aRow["memo"].ToString();
                            aData.actifity = aRow["activity"].ToString();
                            listDataSource.Add( aData );
                        }

                        audittrailReport rpAuditTrail = new audittrailReport();
                        rpAuditTrail.DataSource = listDataSource;
                        rpAuditTrail.Name = ImqSession.getFormatFileSave("AuditTrailReport");
                        rpAuditTrail.SetBoundLabel();
                        ReportViewer1.Report = rpAuditTrail;
                    
                    break;

                case "calendar":
                    if (tbl != null)
                    {
                        for (int n = 0; n < tbl.Rows.Count; n++)
                        {
                            DataRow aRow = tbl.Rows[n];
                            dtCalendarMain aData = new dtCalendarMain();
                            DateTime xx = (DateTime)aRow["date"];
                            aData.date = xx.ToString("dd/MM/yyyy");
                            aData.description = aRow["description"].ToString();
                            listDataSource.Add(aData);
                        }
                    }
                    calendarReport rpCalendar = new calendarReport();
                    rpCalendar.DataSource = listDataSource;
                    rpCalendar.Name = ImqSession.getFormatFileSave("CalendarReport");
                    rpCalendar.SetBoundLabel();
                    ReportViewer1.Report = rpCalendar;
                    break;

                case "schedule":
                    if (tbl != null)
                        for (int n = 0; n < tbl.Rows.Count; n++)
                        {
                            DataRow aRow = tbl.Rows[n];
                            printSchedule aData = new printSchedule();
                            DateTime xx = (DateTime)aRow["time"];
                            aData.time = aRow["time"].ToString();
                            aData.module = aRow["module"].ToString();
                            aData.remark = aRow["remark"].ToString();
                            listDataSource.Add(aData);
                        }
                    scheduleReport rpSchedule = new scheduleReport();
                    rpSchedule.DataSource = listDataSource;
                    rpSchedule.Name = ImqSession.getFormatFileSave("ScheduleReport");
                    rpSchedule.SetBoundLabel();
                    ReportViewer1.Report = rpSchedule;
                    break;

                case "sysparam":
                    if (tbl != null)
                        for (int n = 0; n < tbl.Rows.Count; n++)
                        {
                            DataRow aRow = tbl.Rows[n];
                            dtSysParamMain aData = new dtSysParamMain();
                            aData.param = aRow["param"].ToString();
                            aData.value = aRow["value"].ToString();
                            aData.remark = aRow["remark"].ToString();
                            listDataSource.Add(aData);
                        }
                    systemparameterReport rpSysparam = new systemparameterReport();
                    rpSysparam.DataSource = listDataSource;
                    rpSysparam.Name = ImqSession.getFormatFileSave("SystemParameterReport");
                    rpSysparam.SetBoundLabel();
                    ReportViewer1.Report = rpSysparam;
                    break;
            }
        }
    }
}
