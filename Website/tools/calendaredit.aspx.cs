﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;


using Common;
using Common.wsCalendar;
using Common.wsApproval;
using Common.wsUser;
using Common.wsAuditTrail;
using Common.wsUserGroupPermission;

namespace imq.kpei.administration
{
    public partial class calendaredit : System.Web.UI.Page
    {
        private int stat;
        private dtCalendarMain[] calendars;
        private dtCalendarTmp aDT = new dtCalendarTmp();
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private String aMainLink = "~/tools/calendar.aspx";
        private String moduleName = "Calendar";
        private String target = @"../tools/calendar.aspx";
        private String targetApproval = @"../administration/approval.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (Request.QueryString["stat"] == null)
            {
                Response.Redirect("~/tools/calendar.aspx");
                stat = -1;
            }
            else
                stat = int.Parse(Request.QueryString["stat"].ToString());

            if (!IsPostBack)
            {

                //addedit = int.Parse(Session["stat"].ToString());                
                btnOk.Visible = false;
                btnChecker.Visible = false;
                btnApprovel.Visible = false;
                btnReject.Visible = false;

                switch (stat)
                {
                    case 0:
                        lbltitle.Text = "Calendar New";
                        btnOk.Visible = true;
                        Permission(true);
                        break;

                    case 1:
                        lbltitle.Text = "Edit Calendar";
                        btnOk.Visible = true;
                        Permission(true);

                        getSearch(Request.QueryString["id"].ToString());
                        if (calendars != null)
                        {
                            dtDate.Value = calendars[0].date;
                            dtDescription.Text = calendars[0].description;
                        }
                        break;

                    case 2:
                        lbltitle.Text = "Calendar New";
                        Permission(false);
                        ToEditor(int.Parse(Request.QueryString["idxTmp"].ToString()));
                        dtDate.Enabled = false;
                        dtDescription.Enabled = false;
                        break;

                    case 3:
                        lbltitle.Text = "Edit Schedule";
                        //btnOk.Visible = false;
                        //btnChecker.Visible = false;
                        ToEditor(int.Parse(Request.QueryString["idxTmp"].ToString()));
                        Permission(false);
                        dtDate.Enabled = false;
                        dtDescription.Enabled = false;
                        break;

                    case 4: //remove
                        lbltitle.Text = "Remove Calendar";
                        btnOk.Visible = true;
                        Permission(true);
                        dtDate.Enabled = false;
                        dtDescription.Enabled = false;

                        getSearch(Request.QueryString["id"].ToString());
                        if (calendars != null)
                        {
                            dtDate.Value = calendars[0].date;
                            dtDescription.Text = calendars[0].description;
                        }
                        break;
                }
            }
        }

        protected void ToEditor(int idx)
        {
            CalendarService wsC = ImqWS.GetCalendarService();
            //dtCalendarTmp aDT = new dtCalendarTmp();
            try
            {
                aDT = wsC.SearchTmp(idx);
                dtDate.Date = aDT.date;
                dtDescription.Text = aDT.description;
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsC.Abort();
                throw;
            }
        }

        DateTime StrToDateTime(String str)
        {
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            MyDateTime = Convert.ToDateTime(str);
            return MyDateTime;
        }

        protected void getSearch(String id)
        {
            CalendarService wsC = ImqWS.GetCalendarService();
            try
            {
                calendars = wsC.Search(id);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsC.Abort();
                throw;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            CalendarService wsC = ImqWS.GetCalendarService();
            ApprovalService wsA = ImqWS.GetApprovalWebService();

            try
            {
                //addedit = int.Parse(Session["stat"].ToString());

                int idRec;

                dtCalendarTmp aDT = new dtCalendarTmp();
                aDT.date = dtDate.Date;
                aDT.dateSpecified = true;
                aDT.description = dtDescription.Text;
                idRec = wsC.AddToTmp(aDT);
                if (idRec != 0)
                {
                    dtApproval dtA = new dtApproval();
                    dtA.type = "c";
                    dtA.makerDate = DateTime.Now;
                    dtA.makerName = aUserLogin;
                    dtA.makerStatus = "Ok";
                    dtA.idxTmp = idRec;
                    dtA.form = "~/tools/calendaredit.aspx";
                    dtA.memberId = aUserMember;
                    dtA.status = "M";
                    int idTable;// = int.Parse(row["id"].ToString());
                    switch ( stat )
                    {
                        case 0 :
                            dtA.topic = "Add Calendar";
                            dtA.insertEdit = "I";
                            aDT.id = idRec;
                            break;

                        case 1 :
                            dtA.topic = "Edit Calendar";
                            dtA.insertEdit = "E";
                            idTable = int.Parse(Request.QueryString["id"].ToString());
                            dtA.idTable = idTable;
                            aDT.id = idTable;
                            break;

                        case 4 :
                            dtA.topic = "Remove Calendar";
                            dtA.insertEdit = "R";
                            idTable = int.Parse(Request.QueryString["id"].ToString());
                            dtA.idTable = idTable;
                            aDT.id = idTable;
                            break;
                    }
                    if ((dtDate.Text == "") || (dtDescription.Text == ""))
                    {
                        lblError.Text = "Data should not be blank !";
                        if ((dtDate.Text == ""))
                            lbDateError.Text = "*";
                        else
                            lbDateError.Text = "";

                        if (dtDescription.Text == "")
                            lbDescriptionError.Text = "*";
                        else
                            lbDescriptionError.Text = "";
                    }
                    else
                    {
                        rblApprovalChanged(dtA, wsA, aDT);
                        //Response.Redirect(aMainLink);
                    }
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsC.Abort();
                wsA.Abort();
                throw;
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].checkerName = aUserLogin;
                dtA[0].checkerStatus = "Checked";
                dtA[0].approvelStatus = "To Be Approved";
                dtA[0].status = "C";
                wsA.UpdateByChecker( dtA[0] );
                AuditTrail(dtA[0].insertEdit, "Succes ", "Checker ", aDT, targetApproval);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }

        protected void btnApprovel_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            CalendarService wsC = ImqWS.GetCalendarService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].approvelName = aUserLogin;
                dtA[0].approvelStatus = "Approved";
                dtA[0].status = "A";
                wsA.UpdateByApprovel(dtA[0]);
                Approval(wsC, dtA[0], "");
                //switch(dtA[0].insertEdit)
                //{
                //    case "I": 
                //        wsC.AddFromTmp(dtA[0].idxTmp);
                //        ImqWS.putAuditTrail(aUserLogin, moduleName, "Approval New " + moduleName, toString(aDT));
                //        ShowMessage("Succes New " + moduleName + " as Approval ", targetApproval);
                //        break;// putAuditTrail("Approval New Calendar", toString(aDT)); break;

                //    case "E": 
                //        wsC.Edit(dtA[0].idxTmp, dtA[0].idTable);
                //        ImqWS.putAuditTrail(aUserLogin, moduleName, "Approval Edit " + moduleName, toString(aDT));
                //        ShowMessage("Succes Edit " + moduleName + " as Approval ", targetApproval);
                //        break;// putAuditTrail("Approval Edit Calendar", toString(aDT)); break;

                //    case "R": 
                //        wsC.RemoveById(dtA[0].idTable);
                //        ImqWS.putAuditTrail(aUserLogin, moduleName, "Approval Remove " + moduleName, toString(aDT));
                //        ShowMessage("Succes Remove " + moduleName + " as Approval ", targetApproval);
                //        break;// putAuditTrail("Approval Remove Calendar", toString(aDT)); break;
                //}

            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }

            //Response.Redirect("~/account/approval.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //addedit = int.Parse(Session["stat"].ToString());
            switch (stat)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect(aMainLink);
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx");
                    break;
            }
        }

        void AuditTrail(string insertEdit, string actifity, string aS, dtCalendarTmp dT, String aTarget)
        {
            switch (insertEdit)
            {
                case "I":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " New " + moduleName, toString(dT));
                    ShowMessage(actifity + " New " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " New "+moduleName, toString(dT)); break;
                case "E":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Edit " + moduleName, toString(dT));
                    ShowMessage(actifity + " Edit " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Edit Calendar", toString(dT)); break;
                case "R":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Remove " + moduleName, toString(dT));
                    ShowMessage(actifity + " Remove " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Remove Calendar", toString(dT)); break;
            }
        }

        private void putAuditTrail(String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "Calendar", aActifity, aMemo);
        }

        private String toString(dtCalendarTmp aDT)
        {
            return "[id=" + String.Format("{0}", aDT.id)
                + ", date=" + String.Format("{0:dd/MM/yyyy}", aDT.date)
                + ", description=" + aDT.description + "]";
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Calendar");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (isEditor)
                {
                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                    if (rblApproval.SelectedItem == null )
                        if (dtFMP[n].editMaker)
                            rblApproval.Items.Add("Maker", 0);

                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editDirectChecker)
                            rblApproval.Items.Add("Direct Checker", 1);

                    rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                    if (rblApproval.SelectedItem == null)
                        if (dtFMP[n].editDirectApproval)
                            rblApproval.Items.Add("Direct Approval", 2);

                    if (rblApproval.Items.Count > 0)
                        rblApproval.SelectedIndex = 0;
                }
                else
                {
                    if (!(Request.QueryString["makerName"].Equals(aUserLogin) || Request.QueryString["checkerName"].Equals(aUserLogin)) && (Request.QueryString["memberId"].Equals(aUserMember)))
                    {
                        if ((Request.QueryString["status"].ToString() == "M") && (dtFMP[n].editChecker))
                        {
                            btnReject.Visible = true;
                            btnChecker.Visible = true;
                        }
                        else
                            if ((Request.QueryString["status"].ToString() == "C") && (dtFMP[n].editApproval))
                            {
                                btnReject.Visible = true;
                                btnApprovel.Visible = true;
                            }
                    }
                }
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"');
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        protected void rblApprovalChanged(dtApproval dtA, ApprovalService wsA, dtCalendarTmp aDT)
        {
            switch ((String)rblApproval.SelectedItem.Value)
            {
                //Meker
                case "0":
                    wsA.AddMaker(dtA);
                    AuditTrail(dtA.insertEdit, "Succes ", "Maker ", aDT, target);
                    //ImqWS.putAuditTrail(aUserLogin, moduleName, " New " + moduleName, toString(aDT)); 
                    break;

                // Direct Checker
                case "1":
                    wsA.AddDirectChecker(dtA);
                    AuditTrail(dtA.insertEdit, "Succes ", "Direct Checker ", aDT, target);
                    break;

                //Direct Approval
                case "2":
                    CalendarService wsC = ImqWS.GetCalendarService();
                    wsA.AddDirectApproval(dtA);
                    Approval(wsC, dtA, "Direct");
                    //switch (dtA.insertEdit)
                    //{
                    //    case "I": wsC.AddFromTmp(dtA.idxTmp); ImqWS.putAuditTrail(aUserLogin, moduleName, "Direct Approval New " + moduleName, toString(aDT)); break;// putAuditTrail("Direct Approval New Calendar", toString(aDT)); break;
                    //    case "E": wsC.Edit(dtA.idxTmp, dtA.idTable); ImqWS.putAuditTrail(aUserLogin, moduleName, "Direct Approval Edit " + moduleName, toString(aDT)); break;// putAuditTrail("Direct Approval Edit Calendar", toString(aDT)); break;
                    //    case "R": wsC.RemoveById(dtA.idTable); ImqWS.putAuditTrail(aUserLogin, moduleName, "Direct Approval Remove " + moduleName, toString(aDT)); break;// putAuditTrail("Direct Approval Remove Calendar", toString(aDT)); break;
                    //}
                    break;
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                switch (dtA[0].status)
                {
                    case "M":
                        dtA[0].checkerName = aUserLogin;
                        dtA[0].checkerStatus = "Reject";
                        dtA[0].status = "RC";
                        wsA.UpdateByChecker(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Checker", aDT, targetApproval);
                        break;

                    case "C":
                        dtA[0].approvelName = aUserLogin;
                        dtA[0].approvelStatus = "Reject";
                        dtA[0].status = "RA";
                        wsA.UpdateByApprovel(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Approval", aDT, targetApproval);
                        break;
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }

        private void Approval(CalendarService ws, dtApproval dtA, string aDirect)
        {
            switch (dtA.insertEdit)
            {
                case "I":
                    ws.AddFromTmp(dtA.idxTmp);
                    putAuditTrail(aDirect + "Approval New " + moduleName, toString(aDT));
                    ShowMessage(aDirect + "Approval New " + moduleName, target);
                    break;

                case "E":
                    ws.Edit(dtA.idxTmp, dtA.idTable);
                    putAuditTrail(aDirect + "Approval Edit " + moduleName, toString(aDT));
                    ShowMessage(aDirect + "Approval Edit " + moduleName, target);
                    break;

                case "R":
                    ws.RemoveById(dtA.idTable);
                    putAuditTrail(aDirect + "Approval Remove " + moduleName, toString(aDT));
                    ShowMessage(aDirect + "Approval Remove " + moduleName, target);
                    break;
            }
        }
    }
}
