﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Common;
using Common.wsSysParam;
using Common.wsUser;
using Common.wsUserGroupPermission;

namespace imq.kpei.tools
{
    public partial class systemparameter : System.Web.UI.Page
    {
        private dtSysParamMain[] arrayDatas;
        DataTable tbl = null;
        private String aUserLogin;
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack && !IsCallback)
            {
                if (!aUserLogin.Equals("") && !aUserMember.Equals(""))
                    Permission();
                tbl = GetTable();
                Session["tbl"] = tbl;
            }
            else
                tbl = (DataTable)Session["tbl"];

            gridView.DataSource = tbl;
            gridView.KeyFieldName = "ID";
            gridView.DataBind();
        }

        DataTable GetTable()
        {
            getData();

            //You can store a DataTable in the session state
            DataTable table = new DataTable();
            table = new DataTable();
            table.Columns.Add("ID", typeof(String));
            table.Columns.Add("param", typeof(String));
            table.Columns.Add("value", typeof(String));
            table.Columns.Add("remark", typeof(String));
            table.Columns.Add("description", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["ID"] };
            if (arrayDatas != null )
                for (int n = 0; n < arrayDatas.Length; n++)
                {
                    table.Rows.Add(arrayDatas[n].id,
                        arrayDatas[n].param,
                        arrayDatas[n].value,
                        arrayDatas[n].remark,
                        arrayDatas[n].description);
                }
            return table;
        }
        private void getData()
        {
            SysParamService ws = ImqWS.GetSysParamService();
            try
            {
                arrayDatas = ws.View();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                ws.Abort();
                throw;
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            //Session["stat"] = "0";
            Response.Redirect("~/tools/systemparameteredit.aspx?stat=0");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "1";
            //Session["row"] = row;
            //Response.Redirect("~/tools/systemparameteredit.aspx");

            string strRedirect = "~/tools/systemparameteredit.aspx?stat=1&" +
                            "id=" + Server.UrlEncode(row["ID"].ToString()) + "&" +
                            "param=" + Server.UrlEncode(row["param"].ToString()) + "&" +
                            "value=" + Server.UrlEncode(row["value"].ToString()) + "&" +
                            "remark=" + Server.UrlEncode(row["remark"].ToString()) + "&" +
                            "description=" + Server.UrlEncode(row["description"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            DataRow row = gridView.GetDataRow(gridView.FocusedRowIndex);
            //Session["stat"] = "4";
            //Session["row"] = row;
            //Response.Redirect("~/tools/systemparameteredit.aspx");
            string strRedirect = "~/tools/systemparameteredit.aspx?stat=4&" +
                            "id=" + Server.UrlEncode(row["ID"].ToString()) + "&" +
                            "param=" + Server.UrlEncode(row["param"].ToString()) + "&" +
                            "value=" + Server.UrlEncode(row["value"].ToString()) + "&" +
                            "remark=" + Server.UrlEncode(row["remark"].ToString()) + "&" +
                            "description=" + Server.UrlEncode(row["description"].ToString());

            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            enableBtn(false);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Parameter");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                {
                    enableBtn(true);
                }
                //btnRemove.Enabled = dtFMP[n].removeing;
            }
        }

        private void enableBtn(bool aEnable)
        {
            //btnNew.Enabled = aEnable;
            btnEdit.Enabled = aEnable;
        }

        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            Session["data"] = tbl;
            Response.Redirect("~/tools/toolsExport.aspx?type=sysparam");
        }
    }
}
