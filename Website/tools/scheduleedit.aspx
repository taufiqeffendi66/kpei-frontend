﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="scheduleedit.aspx.cs" Inherits="imq.kpei.tools.scheduleedit" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <div id="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="Schedule Edit" /></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td style="width:128px">Time</td>
                    <td><dx:ASPxTimeEdit ID="dtTime" runat="server"  Width="120px" DisplayFormatString="HH:mm:ss" EditFormat="Custom" EditFormatString="HH:mm:ss"></dx:ASPxTimeEdit></td>
                    <td><dx:ASPxLabel ID="lbTimeError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
                <tr>
                    <td style="width:128px">Modul to Execute</td>
                    <td><dx:ASPxComboBox ID="dtModuleList" runat="server" Width="200px"></dx:ASPxComboBox></td>
                    <td><dx:ASPxLabel ID="lbModuleError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
                <tr>
                    <td style="width:128px">Remark</td>
                    <td><dx:ASPxTextBox ID="dtRemark" ClientInstanceName="clRemark" runat="server" Width="120px"></dx:ASPxTextBox></td>
                    <td><dx:ASPxLabel ID="lbRemarkError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
            </table>
         </div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxButton ID="btnOk" runat="server" Text="Ok" onclick="btnOk_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" onclick="btnChecker_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnApprovel" runat="server" Text="Approved" onclick="btnApprovel_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" onclick="btnReject_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></dx:ASPxButton></td>
                </tr>
            </table> 
        </div>
    </div>


</asp:Content>
