﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Common.wsTransaction;
using Microsoft.Reporting.WebForms;
using System.IO;
using Common.wsSysParam;


namespace imq.kpei.report
{
    public partial class brokerTagihanMinimumDanaPengaman : System.Web.UI.Page
    {
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        private String target = @"../report/brokerTagihanMinimumDanaPengaman.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if (aUserMember.Trim().Equals("kpei"))
                {
                    FillMember();
                }
                else
                {
                    cmbMember.Enabled = false;
                    cmbMember.Text = aUserMember.Trim();
                    //FillSID(aUserMember);
                }
            }

        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            //FillSID(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim());
            RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);
        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillSID(cmbMember.SelectedItem.Text.Trim());
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                alert('" + info + @"');
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void FillSID(string sDate, String MemberId)
        {
            SysParamService wsSP = ImqWS.GetSysParamService();
            dtSysParamMain[] sysparams;
            sysparams = wsSP.Search("", "REPORT_PATH", "");

            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;
            string s = string.Format("../pdf/Broker/MCC Billing_{0}_{1}.PDF", dStart.Date.ToString("yyyyMMdd"), MemberId);
            if (File.Exists(s))
                Response.Write("<iframe src='"+s+"' width='100%' height='100%' border='0'> </iframe>");
            else ShowMessage(" File not Exists ", target);
        }

        private void RenderPdf(string sDate, string sMemberID, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(sDate);

            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/MCC Billing";
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpMemberID });

            if (bSave)
            {
                string sFileName = string.Format("broker_MCC Billing_{0}_{1}.PDF", dStart.Date.ToString("yyyyMMdd"), sMemberID);

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }  


    }
}