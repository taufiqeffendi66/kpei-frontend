﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Common;
using Common.clientAccountWS;
using Common.memberInfoWS;
using System.Diagnostics;
using Common.wsCalendar;

namespace imq.kpei.report
{
    public partial class brokerDHKClient : System.Web.UI.Page
    {
        
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if ((Request.QueryString["sDate"] != null) && (Request.QueryString["sMember"] != null) && (Request.QueryString["sSID"] != null))
                {
                    RenderPdf(Request.QueryString["sDate"].ToString(), Request.QueryString["sMember"].ToString(), Request.QueryString["sSID"].ToString(), true);
                }
                else
                {
                    deDate.Date = DateTime.Now;
                    if (aUserMember.Trim().Equals("kpei"))
                    {
                        FillMember();
                    }
                    else
                    {
                        cmbMember.Enabled = false;
                        cmbMember.Text = aUserMember.Trim();
                        FillSID(aUserMember);
                    }
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), cmbSID.Text.Trim(), false);
                           
        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSID(cmbMember.SelectedItem.Text.Trim());
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void FillSID(String MemberId)
        {
            ClientAccountWSService caws = null;
            try
            {
                caws = Common.ImqWS.GetClientAccountService();
                cmbSID.DataSource = caws.getByMemberId(MemberId);
                cmbSID.DataBind();
            }
            catch (Exception ex)
            {
                if (caws != null)
                    caws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (caws != null)
                    caws.Dispose();
            }
        }


        private void RenderPdf(string sDate, string sMemberID, string sSID, bool bSave )
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(sDate);

            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);

            ReportParameter rpSID = new ReportParameter();
            rpSID.Name = "sid";
            rpSID.Values.Add(sSID);
 
            ReportParameter rpH1 = new ReportParameter();
            rpH1.Name = "h1";
            rpH1.Values.Add(ImqWS.HplusOne(dStart).Date.ToString("yyyy/MM/dd"));

            ReportParameter rpH3 = new ReportParameter();
            rpH3.Name = "h3";
            rpH3.Values.Add(ImqWS.HplusTri(dStart).Date.ToString("yyyy/MM/dd"));

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/CRL Client Level";
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpMemberID, rpSID, rpH1, rpH3 });

            if (bSave)
            {
                string sFileName = "broker_DHK_Client_" + dStart.ToString("yyyyMMdd") + "_" + sMemberID + "_" + sSID + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" +sFileName ));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }  

    }
}