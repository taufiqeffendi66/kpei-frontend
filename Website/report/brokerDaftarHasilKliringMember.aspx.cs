﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;
using Common.wsCalendar;


namespace imq.kpei.report
{
    public partial class brokerDaftarHasilKliringMember : System.Web.UI.Page
    {
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if (aUserMember.Trim().Equals("kpei"))
                {
                    FillMember();
                }
                else
                {
                    cmbMember.Enabled = false;
                    cmbMember.Text = aUserMember.Trim();
                    FillSID(aUserMember);
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);

            //ReportParameter strDate = new ReportParameter();
            //strDate.Name = "strDate";
            //strDate.Values.Add(sDate);

            //ReportParameter rpMemberID = new ReportParameter();
            //rpMemberID.Name = "memberId";
            //rpMemberID.Values.Add(cmbMember.Text.Trim());

            //// H+3
            //DateTime h3 = DateTime.Now;
            //int x = 0;
            //CalendarService wsC = ImqWS.GetCalendarService();
            //for (int y = 0; y < 3; y++)
            //{
            //    x++;
            //    while (true)
            //    {
            //        h3 = DateTime.Now.AddDays(x);
            //        if (h3.DayOfWeek == DayOfWeek.Saturday)
            //            x += 1;
            //        else if (h3.DayOfWeek == DayOfWeek.Sunday)
            //            x += 1;
            //        else if (wsC.SearchByDate(h3.Date.ToString("MM/dd/yyyy"))!=null)
            //            x += 1;
            //        else break;
            //    }
            //}
            
            //ReportParameter rpH3 = new ReportParameter();
            //rpH3.Name = "h3";
            //rpH3.Values.Add(h3.Date.ToString("yyyy-MM-dd"));

            //ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            //ServerReport serverReport = ReportViewer1.ServerReport;

            //serverReport.ReportServerUrl = new Uri("http://localhost:8080/ReportServer");
            //serverReport.ReportPath = "/kpei/broker_Daftar hasil kliring (ALL CLIENT FOR MEMBER)";
            //serverReport.SetParameters(new ReportParameter[] { rpMemberID, rpH3 });

            //ReportViewer1.ServerReport.Refresh();

        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSID(cmbMember.SelectedItem.Text.Trim());
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void FillSID(String MemberId)
        {

        }

        private void RenderPdf(string sDate, string sMemberID, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter strDate = new ReportParameter();
            strDate.Name = "strDate";
            strDate.Values.Add(sDate);

            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);

            DateTime h3 = dStart;
            // H+3
            //DateTime h3 = deDate.Date;// DateTime.Now;
            int x = 0;
            CalendarService wsC = ImqWS.GetCalendarService();
            for (int y = 0; y < 3; y++)
            {
                x++;
                while (true)
                {
                    h3 = DateTime.Now.AddDays(x);
                    if (h3.DayOfWeek == DayOfWeek.Saturday)
                        x += 1;
                    else if (h3.DayOfWeek == DayOfWeek.Sunday)
                        x += 1;
                    else if (wsC.SearchByDate(h3.Date.ToString("MM/dd/yyyy")) != null)
                        x += 1;
                    else break;
                }
            }
            ReportParameter rpH3 = new ReportParameter();
            rpH3.Name = "h3";
            rpH3.Values.Add(h3.Date.ToString("yyyy/MM/dd"));

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerUrl = new Uri("http://localhost:8080/ReportServer");
            serverReport.ReportPath = "/kpei/broker_Daftar hasil kliring (ALL CLIENT FOR MEMBER)";
            serverReport.SetParameters(new ReportParameter[] { strDate, rpMemberID, rpH3 });

            if (bSave)
            {
                string sFileName = "DHK_MEMBER_" + dStart.ToString("yyyyMMdd") + "_" + sMemberID + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }  

    }
}