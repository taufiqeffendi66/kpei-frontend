﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;
using Common.wsCalendar;


namespace imq.kpei.report
{
    public partial class brokerPenggunaanDanaJaminanUntukAK : System.Web.UI.Page
    {
        private String aUserMember;
        //private string reportPathAK = "/SKD/GF Usage_AK";
        private string pathOEM = "/SKD/skdBroker";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if ((Request.QueryString["sDate"] != null) && (Request.QueryString["sMember"] != null))
                {
                    RenderPdf(Request.QueryString["sDate"].ToString(), Request.QueryString["sDate"].ToString(), Request.QueryString["sMember"].ToString(), true);
                }
                else
                {
                    deEndDate.Date = DateTime.Now;

                    if (aUserMember.Trim().Equals("kpei"))
                    {
                        FillMember();
                    }
                    else
                    {
                        cmbMember.Enabled = false;
                        cmbMember.Text = aUserMember.Trim();
                        FillSID(aUserMember);
                    }
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (!cmbMember.Text.Trim().Equals("All"))
                RenderPdf(deStartDate.Date.ToString("yyyy/MM/dd"), deEndDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);
            else
                RenderPdfAll(deStartDate.Date.ToString("yyyy/MM/dd"), deEndDate.Date.ToString("yyyy/MM/dd"), false);
        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillSID(cmbMember.SelectedItem.Text.Trim());
            if (!cmbMember.Text.Trim().Equals("All"))
                RenderPdf(deStartDate.Date.ToString("yyyy/MM/dd"), deEndDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);
            else
                RenderPdfAll(deStartDate.Date.ToString("yyyy/MM/dd"), deEndDate.Date.ToString("yyyy/MM/dd"), false);
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                    cmbMember.Items.Add("All", "All");
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void FillSID(String MemberId)
        {
            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(MemberId);

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/GF Usage_AK";
            serverReport.SetParameters(new ReportParameter[] { rpMemberID });

            ReportViewer1.ServerReport.Refresh();
        }

        private void RenderPdf(string dateStart, string dateEnd, string sMemberID, bool bSave)
        {
            //DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpStartDate = new ReportParameter();
            rpStartDate.Name = "dateStart";
            rpStartDate.Values.Add(dateStart);

            ReportParameter rpEndDate = new ReportParameter();
            rpEndDate.Name = "dateEnd";
            rpEndDate.Values.Add(dateEnd);

            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/GF Usage_AK";
            serverReport.SetParameters(new ReportParameter[] { rpStartDate, rpEndDate, rpMemberID });

            ReportViewer1.ServerReport.Refresh();

        }

        private void RenderPdfAll(string dateStart, string dateEnd, bool bSave)
        {
            ReportParameter rpStartDate = new ReportParameter();
            rpStartDate.Name = "dateStart";
            rpStartDate.Values.Add(dateStart);

            ReportParameter rpEndDate = new ReportParameter();
            rpEndDate.Name = "dateEnd";
            rpEndDate.Values.Add(dateEnd);

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/GF Usage_Kpei";
            serverReport.SetParameters(new ReportParameter[] { rpStartDate, rpEndDate});

            ReportViewer1.ServerReport.Refresh();

        }  
    }
}