namespace NetpositionToArrayList
{
    public class NetpositionRecord
    {

        string memberId;
        string clientId;
        string series;
        char role;
        char netType;
        int prevBuy;
        int prevSell;
        int currOpenBuy;
        int currOpenSell;
        int currCloseBuy;
        int currCloseSell;
        int liquidateBuy;
        int liquidateSell;
        int exercise;
        int assignment;
        int netBuy;
        int netSell;
        float premium;
        float gainLoss;
        float penalty;
        double compensation;
        float settlement;
        float fee;
        float prevClose;
        float prevGainLoss;

        public NetpositionRecord(string memberId,
        string clientId,
        string series, char role,
            char netType, int prevBuy, int prevSell,
            int currOpenBuy, int currOpenSell, int currCloseBuy,
            int currCloseSell, int liquidateBuy, int liquidateSell,
            int exercise, int assignment, int netBuy,
            int netSell, float premium, float gainLoss, float penalty,
            double compensation, float settlement, float fee)
        {
            this.memberId = memberId;
            this.clientId = clientId;
            this.series = series;
            this.role = role;
            this.netType = netType;
            this.prevBuy = prevBuy;
            this.prevSell = prevSell;
            this.currOpenBuy = currOpenBuy;
            this.currOpenSell = currOpenSell;
            this.currCloseBuy = currCloseBuy;
            this.currCloseSell = currCloseSell;
            this.liquidateBuy = liquidateBuy;
            this.liquidateSell = liquidateSell;
            this.exercise = exercise;
            this.assignment = assignment;
            this.netBuy = netBuy;
            this.netSell = netSell;
            this.premium = premium;
            this.gainLoss = gainLoss;
            this.penalty = penalty;
            this.compensation = compensation;
            this.settlement = settlement;
            this.fee = fee;
        }

        public string MemberId
        {
            get { return memberId; }
            set { memberId = value; }
        }

        public string ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        public string Series
        {
            get { return series; }
            set { series = value; }
        }
    }
}
