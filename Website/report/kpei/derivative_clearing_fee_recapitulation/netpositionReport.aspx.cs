﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetpositionToArrayList;
using Common.wsUser;
using System.Collections;
using System.Web.Configuration;

namespace imq.kpei.report.kpei.derivative_clearing_fee_recapitulation
{
    public partial class netpositionReport : System.Web.UI.Page
    {
        private dtUserSkd[] users;

        protected void Page_Load(object sender, EventArgs e)
        {
            netpositionReportViewer.Report = CreateReport();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            netpositionReportViewer.Report = CreateReport();
        }

        private void getData(string memberId)
        {
            //netpositionService wsU = CreateWebService();
            //try
            //{
            //    if (memberId.Equals("Admin"))
            //        users = wsU.View();
            //    else users = wsU.ListByMember(memberId);
            //}
            //catch (TimeoutException te)
            //{
            //    te.ToString();
            //    wsU.Abort();
            //    throw;
            //}
        }

        private UserService CreateWebService()
        {
            UserService ws = new UserService();
            ws.Url = GetConfig("WS_SERVER") + "/wsUser/User";
            return ws;
        }
        public static string GetConfig(string key)
        {
            return WebConfigurationManager.AppSettings.Get(key);
        }

        netpositionXtraReport CreateReport()
        {

            // Create a list. 
            ArrayList listDataSource = new ArrayList();
            //getData("Admin");
            //for (int n = 0; n < users.Length; n++)
            //    listDataSource.Add(new NetpositionRecord(
            //        users[n].id.userId,
            //        users[n].id.memberId,
            //        users[n].userGroup.name));

            // Create a report. 
            netpositionXtraReport report = new netpositionXtraReport();

            // Bind the report to the list. 
            report.DataSource = listDataSource;

            report.SetBoundLabel();
            return report;
        }


    }


}