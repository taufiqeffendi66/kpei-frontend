﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;
using Common.wsCalendar;


namespace imq.kpei.report
{
    public partial class brokerTagihanMarginCallUntukAK : System.Web.UI.Page
    {
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if ((Request.QueryString["sDate"] != null) && (Request.QueryString["sMember"] != null))
                {
                    RenderPdf(Request.QueryString["sDate"].ToString(), Request.QueryString["sMember"].ToString(), true);
                }
                else
                {
                    if (aUserMember.Trim().Equals("kpei"))
                    {
                        FillMember();
                    }
                    else
                    {
                        cmbMember.Enabled = false;
                        cmbMember.Text = aUserMember.Trim();
                        FillSID(aUserMember);
                    }
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if ((deDate != null) && (!cmbMember.Text.Trim().Equals("All")))
                RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);
            else
                RenderPdfAll(deDate.Date.ToString("yyyy/MM/dd"), false);
        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillSID(cmbMember.SelectedItem.Text.Trim());
            if ( (deDate != null) && (!cmbMember.Text.Trim().Equals("All")) )
                RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);
            else
                RenderPdfAll(deDate.Date.ToString("yyyy/MM/dd"), false);
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                    cmbMember.Items.Add("All", "All");
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void FillSID(String MemberId)
        {
            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(MemberId);

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/MC Billing_AK";
            serverReport.SetParameters(new ReportParameter[] { rpMemberID });

            ReportViewer1.ServerReport.Refresh();
        }

        private void RenderPdf(string sDate, string sMemberID, bool bSave)
        {
            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date.ToString("yyyy/MM/dd"));

            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());

            serverReport.ReportPath = path + "/MC Billing_AK";
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpMemberID });

            if (bSave)
            {
                string sFileName = "broker_MC_Billing_AK_" + DateTime.Now.Date.ToString("yyyyMMdd") + "_" + sMemberID + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }

        private void RenderPdfAll(string sDate, bool bSave)
        {

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date.ToString("yyyy/MM/dd"));

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());

            serverReport.ReportPath = path + "/MC Billing_KPEI";
            serverReport.SetParameters(new ReportParameter[] { rpDate });

            if (bSave)
            {
                string sFileName = "Broker_MC_Billing_KPEI_" + DateTime.Now.Date.ToString("yyyyMMdd") + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }

    }
}