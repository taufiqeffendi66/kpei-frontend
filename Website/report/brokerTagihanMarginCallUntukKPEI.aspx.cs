﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;
using Common.wsCalendar;


namespace imq.kpei.report
{
    public partial class brokerTagihanMarginCallUntukKPEI : System.Web.UI.Page
    {
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if (Request.QueryString["sMember"] != null)
                {
                    RenderPdf(true);
                }
                else
                {
                    if (aUserMember.Trim().Equals("kpei"))
                    {
                        RenderPdf(false);
                    }
                }
            }
        }

        private void RenderPdf(bool bSave)
        {
            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportPath = path + "/MC Billing_KPEI";

            if (bSave)
            {
                string sFileName = "Broker_MC_Billing_KPEI_" + DateTime.Now.Date.ToString("yyyyMMdd") + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }  
    }
}