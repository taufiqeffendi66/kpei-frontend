﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;


namespace imq.kpei.report
{
    public partial class brokerDaftarAnggotaGagalBayar : System.Web.UI.Page
    {
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (Request.QueryString["sDate"] != null) 
            {
                RenderPdf(Request.QueryString["sDate"].ToString(), true);
            }
            else if (!IsPostBack)
            {
                deDate.Date = DateTime.Now;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), false);

        }

        private void RenderPdf(string sDate, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(sDate);

            ReportParameter rpH1 = new ReportParameter();
            rpH1.Name = "h1";
            rpH1.Values.Add(ImqWS.HplusOne(dStart).Date.ToString("yyyy/MM/dd"));

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/Default Member Report";
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpH1 });

            if (bSave)
            {
                string sFileName = "broker_Daftar Anggota Gagal Bayar_" + DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date.ToString("yyyyMMdd") + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }

    }
}