﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Common;
using Common.clientAccountWS;
using Common.memberInfoWS;
using System.Diagnostics;
using Common.wsCalendar;

namespace imq.kpei.report
{
    public partial class brokerDaftarHasilKliringClient : System.Web.UI.Page
    {
        
        private String aUserMember;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if ((Request.QueryString["sDate"] != null) && (Request.QueryString["sMember"] != null) && (Request.QueryString["sSID"] != null))
            {
                RenderPdf(Request.QueryString["sDate"].ToString(), Request.QueryString["sMember"].ToString(), Request.QueryString["sSID"].ToString(), true);
            }
            else if (!IsPostBack)
            {
                deDate.Date = DateTime.Now;
                if (aUserMember.Trim().Equals("kpei"))
                {
                    FillMember();
                }
                else
                {
                    cmbMember.Enabled = false;
                    cmbMember.Text = aUserMember.Trim();
                    FillSID(aUserMember);
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), cmbSID.Text.Trim(), false);
                           
        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSID(cmbMember.SelectedItem.Text.Trim());
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void FillSID(String MemberId)
        {
            ClientAccountWSService caws = null;
            try
            {
                caws = Common.ImqWS.GetClientAccountService();
                cmbSID.DataSource = caws.getByMemberId(MemberId);
                cmbSID.DataBind();
            }
            catch (Exception ex)
            {
                if (caws != null)
                    caws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (caws != null)
                    caws.Dispose();
            }
        }


        private void RenderPdf(string sDate, string sMemberID, string sSID, bool bSave )
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter strDate = new ReportParameter();
            strDate.Name = "strDate";
            strDate.Values.Add(sDate);

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(dStart.ToString("MM/dd/yyyy"));

            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);

            ReportParameter rpSID = new ReportParameter();
            rpSID.Name = "sid";
            rpSID.Values.Add(sSID);

            DateTime h3 = dStart;
            // H+3
            //DateTime h3 = deDate.Date;// DateTime.Now;
            int x = 0;
            CalendarService wsC = ImqWS.GetCalendarService();
            for (int y = 0; y < 3; y++)
            {
                x++;
                while (true)
                {
                    h3 = DateTime.Now.AddDays(x);
                    if (h3.DayOfWeek == DayOfWeek.Saturday)
                        x += 1;
                    else if (h3.DayOfWeek == DayOfWeek.Sunday)
                        x += 1;
                    else if (wsC.SearchByDate(h3.Date.ToString("MM/dd/yyyy")) != null)
                        x += 1;
                    else break;
                }
            }
            ReportParameter rpH3 = new ReportParameter();
            rpH3.Name = "h3";
            rpH3.Values.Add(h3.Date.ToString("yyyy/MM/dd"));

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerUrl = new Uri("http://localhost:8080/ReportServer");
            serverReport.ReportPath = "/kpei/broker_Daftar hasil kliring (CLIENT LEVEL)";
            serverReport.SetParameters(new ReportParameter[] { strDate, rpDate, rpMemberID, rpSID, rpH3 });

            if (bSave)
            {
                string sFileName = "DHK_Client_" + dStart.ToString("yyyyMMdd") + "_" + sMemberID + "_" + sSID + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" +sFileName ));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }  

    }
}