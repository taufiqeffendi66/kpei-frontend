﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="brokerDaftarHasilKliringClient.aspx.cs" Inherits="imq.kpei.report.brokerDaftarHasilKliringClient" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td>Date</td>
            <td>
                <dx:ASPxDateEdit ID="deDate" runat="server" DisplayFormatString="yyyy-MM-dd"></dx:ASPxDateEdit>
            </td>
        </tr>
        <tr>
            <td>Member ID</td>
            <td>
                <dx:ASPxComboBox ID="cmbMember" runat="server" AutoPostBack="true" IncrementalFilteringMode="StartsWith"
                    EnableCallbackMode="true" CallbackPageSize="10" DropDownStyle="DropDown" 
                    DropDownRows="10" onselectedindexchanged="cmbMember_SelectedIndexChanged" >
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td>SID</td>
            <td><dx:ASPxComboBox ID="cmbSID" runat="server" AutoPostBack="true" IncrementalFilteringMode="StartsWith"
                EnableCallbackMode="true" CallbackPageSize="10" DropDownStyle="DropDown" DropDownRows="10"
                ValueField="sid" TextField="sid">
                </dx:ASPxComboBox>
            </td>
            <td><dx:ASPxButton ID="btnOK" runat="server" Text="OK" onclick="btnOK_Click"/></td>
        </tr>
    </table>
    
    <div style="width:100%;height:100%">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Width="100%" Height="100%"></rsweb:ReportViewer>    
    </div>
    </form>
</body>
</html>
