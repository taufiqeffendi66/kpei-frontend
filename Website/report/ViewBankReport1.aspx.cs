﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Common.bankWS;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;

namespace imq.kpei.report
{
    public partial class ViewBankReport : System.Web.UI.Page
    {
        private String aUserMember;


        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            FillBank();
            if (!IsPostBack)
            {
                if (Request.QueryString["type"] != null)
                {
                    string req = Request.QueryString["type"].ToString();
                    string types = req.Substring(0, req.IndexOf('?'));
                    hfBankRep.Set("type", types);

                    if (types.Equals("12"))
                    {
                        lblBank.Visible = false;
                        cmbBank.Visible = false;
                        btnOK.Visible = false;
                        ServerReport serverReport = ReportViewer1.ServerReport;
                        Uri uri = new Uri(ImqSession.GetURLReport());
                        serverReport.ReportServerUrl = uri;
                        string reportPath = String.Empty;
                        reportPath = "/kpei/bank_laporan posisi dana (antar bank)";
                        serverReport.ReportPath = reportPath;
                        ReportViewer1.ServerReport.Refresh();
                    }
                }
            }


        }

        private void ShowReport()
        {
            string type = hfBankRep.Get("type").ToString();
            Uri uri = new Uri(ImqSession.GetURLReport());
            string reportPath = String.Empty;
            switch (type)
            {
                case "1": reportPath = "/kpei/bank_Pemenuhan margin call broker dari dana pengaman"; break;
                case "2A": reportPath = "/kpei/bank_Pemenuhan hak terima broker"; break;
                case "2B": reportPath = "/kpei/bank_Pemenuhan hak terima broker (distribusi ke kolateral broker)"; break;
                //case "3A"   : reportPath = "/kpei/bank_Pemenuhan hak terima nasabah"; break;
                case "3B": reportPath = "/kpei/bank_Pemenuhan hak terima nasabah (distribusi ke kolateral nasabah)"; break;
                case "4A": reportPath = "/kpei/bank_Tagihan kewajiban serah broker (penyerahan dari kolateral broker)"; break;
                case "4B": reportPath = "/kpei/bank_Tagihan kewajiban serah broker"; break;
                case "5": reportPath = "/kpei/bank_Tagihan kewajiban serah nasabah (penyerahan dari kolateral nasabah)"; break;
                case "6": reportPath = "/kpei/bank_Penggunaan dana jaminan"; break;
                case "7": reportPath = "/kpei/bank_Penggunaan dana pengaman (untuk kebutuhan settlement)"; break;
                case "8": reportPath = "/kpei/bank_Penarikan free collateral Broker"; break;
                case "9": reportPath = "/kpei/bank_Penarikan free collateral Nasabah"; break;
                case "10A1": reportPath = "/kpei/bank_Pengembalian denda atas penggunaan dana jaminan10A1"; break;
                case "10A2": reportPath = "/kpei/bank_Pengembalian denda atas penggunaan dana jaminan10A2"; break;
                case "10B1": reportPath = "/kpei/bank_Pengembalian denda atas penggunaan dana jaminan (alokasi ke rek denda dan rek pokok)10B1"; break;
                case "10B2": reportPath = "/kpei/bank_Pengembalian denda atas penggunaan dana jaminan10B2"; break;
                case "11": reportPath = "/kpei/bank_Pemindahbukuan posisi dana"; break;
                //case "12"   : reportPath = "/kpei/bank_laporan posisi dana (antar bank)"; break;
                case "13": reportPath = "/kpei/bank_Tanda terima dokumen"; break;
                case "14": reportPath = "/kpei/bank_Daftar Anggota Serah Dana"; break;
                case "15": reportPath = "/kpei/bank_Daftar Anggota Terima Dana"; break;
                case "16": reportPath = "/kpei/bank_Penarikan dana pengaman"; break;
                case "17": reportPath = "/kpei/bank_Alokasi pembayaran  fee dana jaminan (pada bank yg sama)"; break;
                case "18": reportPath = "/kpei/bank_Alokasi pembayaran fee kliring (pada bank yg sama)"; break;
            }

            if (reportPath != String.Empty)
            {
                ReportParameter rptParam = new ReportParameter();
                rptParam.Name = "bank_code";
                rptParam.Values.Add(cmbBank.SelectedItem.Value.ToString());

                ServerReport serverReport = ReportViewer1.ServerReport;
                serverReport.ReportServerUrl = uri;
                serverReport.ReportPath = reportPath;
                serverReport.SetParameters(rptParam);

                ReportViewer1.ServerReport.Refresh();
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void FillBank()
        {
            BankWSService bws = null;
            bank[] list = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                if (bws != null)
                {
                    list = bws.get();
                    if (list != null)
                    {
                        cmbBank.DataSource = list;
                        cmbBank.DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                if (bws != null)
                    bws.Abort();

                Debug.WriteLine(e.Message);
            }
            finally
            {
                if (bws != null)
                    bws.Dispose();
            }

        }
    }
}