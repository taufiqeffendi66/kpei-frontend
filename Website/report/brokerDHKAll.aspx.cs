﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;
using Common.wsCalendar;
using System.Reflection;


namespace imq.kpei.report
{
    public partial class brokerDHKAll : System.Web.UI.Page
    {
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                if (Request.QueryString["sDate"] != null)
                {
                    RenderPdf(Request.QueryString["sDate"].ToString(), true);
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), false);
        }


        private void RenderPdf(string sDate, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(sDate);

            ReportParameter rpH1 = new ReportParameter();
            rpH1.Name = "h1";
            rpH1.Values.Add(ImqWS.HplusOne(dStart).Date.ToString("yyyy/MM/dd"));

            ReportParameter rpH3 = new ReportParameter();
            rpH3.Name = "h3";
            rpH3.Values.Add(ImqWS.HplusTri(dStart).Date.ToString("yyyy/MM/dd"));

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = "/KpeiBroker/CRL All Member";
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpH1, rpH3 });

            if (bSave)
            {
                string sFileName = "broker_DHK_All_" + dStart.ToString("yyyyMMdd") + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            SuppressExportButton(ReportViewer1, "PDF");
            SuppressExportButton(ReportViewer1, "Word");
        }

        private void SuppressExportButton(ReportViewer rv, string optionToSuppress)
        {
            var reList = rv.LocalReport.ListRenderingExtensions();
            foreach (var re in reList)
            {
                if (re.Name.Trim().ToUpper() == optionToSuppress.Trim().ToUpper()) // Hide the option
                {
                    re.GetType().GetField("m_isVisible", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(re, false);
                }
            }
        }

    }
}