﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Common;
using Common.clientAccountWS;
using Common.memberInfoWS;
using System.Diagnostics;

namespace imq.kpei.report
{
    public partial class brokerLaporanPenyelesaianTransaksi : System.Web.UI.Page
    {
        
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if ((Request.QueryString["sDate"] != null) && (Request.QueryString["sMember"] != null) )
            {
                RenderPdf(Request.QueryString["sDate"].ToString(), Request.QueryString["sMember"].ToString(), true);
            }
            else if (!IsPostBack)
            {
                deDate.Date = DateTime.Now;
                if (aUserMember.Trim().Equals("kpei"))
                {
                    FillMember();
                }
                else
                {
                    cmbMember.Enabled = false;
                    cmbMember.Text = aUserMember.Trim();
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);            
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void RenderPdf(string sDate, string sMemberID, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpH1 = new ReportParameter();
            rpH1.Name = "h1";
            rpH1.Values.Add(ImqWS.HplusOne(dStart).Date.ToString("yyyy/MM/dd"));

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(sDate);

            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/Obligation Accomplishment Report"; 
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpH1, rpMemberID });

            if (bSave)
            {
                string sFileName = "broker_Obligation_Accomplishment_Report_" + dStart.ToString("yyyyMMdd") + "_" + sMemberID + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }

    }
}