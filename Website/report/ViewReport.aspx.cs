﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Common;
using Common.clientAccountWS;
using Common.memberInfoWS;
using System.Diagnostics;

using System.Web.Services.Protocols;
using Common.wsCalendar;
namespace imq.kpei.report
{
    public partial class ViewReport : System.Web.UI.Page
    {
        
        private String aUserMember;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if (aUserMember.Trim().Equals("kpei"))
                {
                    FillMember();
                }
                else
                {
                    cmbMember.Enabled = false;
                    cmbMember.Text = aUserMember.Trim();
                    FillSID(aUserMember);
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            //RenderPdf();
            Response.Redirect("~/report/brokerDHKMember.aspx?sDate=2012/08/23&sMember=CC", true);
        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSID(cmbMember.SelectedItem.Text.Trim());
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void FillSID(String MemberId)
        {
            ClientAccountWSService caws = null;
            try
            {
                caws = Common.ImqWS.GetClientAccountService();
                cmbSID.DataSource = caws.getByMemberId(MemberId);
                cmbSID.DataBind();
            }
            catch (Exception ex)
            {
                if (caws != null)
                    caws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (caws != null)
                    caws.Dispose();
            }
        }

        private void RenderPdf()
        {
            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(cmbMember.Text.Trim());

            ReportParameter rpSID = new ReportParameter();
            rpSID.Name = "sid";
            rpSID.Values.Add(cmbSID.Text.Trim());

            // H+3
            DateTime h3 = DateTime.Now;
            int x = 0;
            CalendarService wsC = ImqWS.GetCalendarService();
            for (int y = 0; y < 3; y++)
            {
                x++;
                while (true)
                {
                    h3 = DateTime.Now.AddDays(x);
                    if (h3.DayOfWeek == DayOfWeek.Saturday)
                        x += 1;
                    else if (h3.DayOfWeek == DayOfWeek.Sunday)
                        x += 1;
                    else if (wsC.SearchByDate(h3.Date.ToString("MM/dd/yyyy")) != null)
                        x += 1;
                    else break;
                }
            }
            ReportParameter rpH3 = new ReportParameter();
            rpH3.Name = "h3";
            rpH3.Values.Add(h3.Date.ToString("yyyy/MM/dd"));

            ServerReport serverReport = RptVr.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = "/kpei/broker_Daftar hasil kliring (CLIENT LEVEL)";
            serverReport.SetParameters(new ReportParameter[] { rpMemberID, rpSID, rpH3 });

            Byte[] results = serverReport.Render("PDF");

            Response.ContentType = "Application/pdf";
            Response.AddHeader("content-disposition", "attachment; filename=broker_Daftar hasil kliring (CLIENT LEVEL).pdf");
            Response.OutputStream.Write(results, 0, results.Length);

            //This is very important if you want to directly download from stream instead of file
            Response.End();

        }        

    }
}