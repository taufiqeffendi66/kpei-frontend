namespace UserToArrayList
{
    public class RecordUser
    {
        string userId, memberId, groupId;

        public RecordUser(string userId, string memberId, string groupId)
        {
            this.userId = userId;
            this.memberId = memberId;
            this.groupId = groupId;
        }

        public string UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
            }
        }

        public string MemberId
        {
            get
            {
                return memberId;
            }
            set
            {
                memberId = value;
            }
        }

        public string GroupId
        {
            get
            {
                return groupId;
            }
            set
            {
                groupId = value;
            }
        }
    }
}
