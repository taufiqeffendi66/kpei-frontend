﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.report.user
{
    public partial class userXtraReport : DevExpress.XtraReports.UI.XtraReport
    {
        public userXtraReport()
        {
            InitializeComponent();
        }

        public void AddHeaderLable(string caption, Rectangle bounds)
        {
            XRLabel label = new XRLabel();
            PageHeader.Controls.Add(label);

            // Set its location and size.
            label.Location = bounds.Location;
            label.Size = bounds.Size;

            label.Text = caption;
        }

        public void SetBoundLabel()
        {
            clUserId.DataBindings.Add("Text", DataSource, "UserId");
            clMemberId.DataBindings.Add("Text", DataSource, "MemberId");
            clGroupId.DataBindings.Add("Text", DataSource, "GroupId");
        }

        public void AddBoundLabel(string bindingMember, Rectangle bounds)
        {
            // Create a label.
            XRLabel label = new XRLabel();

            // Add the label to the report's Detail band.
            Detail.Controls.Add(label);

            // Set its location and size.
            label.Location = bounds.Location;
            label.Size = bounds.Size;

            // Bind it to the bindingMember data field.
            label.DataBindings.Add("Text", DataSource, bindingMember);
        }
    }
}
