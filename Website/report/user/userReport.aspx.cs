﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using System.Collections;
using UserToArrayList;

using Common;
using Common.wsUser;
using DevExpress.XtraReports.Web;

namespace imq.kpei.report.user
{
    public partial class userReport : System.Web.UI.Page
    {
        private dtUserGroupList[] UGL;
        private String aUserLogin;
        private String aUserMember;

      
        protected void Page_Load(object sender, EventArgs e)
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            ReportViewer1.Report = CreateReport();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ReportViewer1.Report = CreateReport();
        }

        private void getData( string memberId)
        {
            UserService wsU = ImqWS.GetUserService();
            try
            {
                if (aUserMember.Equals("kpei"))
                    UGL = wsU.getUserGroupList("", "", "");
                else
                    UGL = wsU.getUserGroupList(aUserMember, "", "");
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsU.Abort();
                throw;
            }
        }

        XtraReport CreateReport()
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            // Create a list. 
            ArrayList listDataSource = new ArrayList();
            getData("Admin");
            for (int n = 0; n < UGL.Length; n++)
            {
                dtUserGroupListPK aData = new dtUserGroupListPK();
                aData.memberId = UGL[n].id.memberId;
                aData.userId = UGL[n].id.userId;
                aData.groupId = UGL[n].id.groupId; 
                listDataSource.Add(aData);
            }

                //listDataSource.Add(new RecordUser(
                //    UGL[n].id.userId
                //    , UGL[n].id.memberId
                //    , UGL[n].id.groupId
                //    ));

            // Create a report. 
            userXtraReport report = new userXtraReport();
            
            // Bind the report to the list. 
            report.DataSource = listDataSource;

            report.SetBoundLabel();
            return report;
        }


    }

 
}