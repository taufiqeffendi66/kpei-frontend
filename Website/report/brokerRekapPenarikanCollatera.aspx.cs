﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;
using Common.wsCalendar;


namespace imq.kpei.report
{
    public partial class brokerRekapPenarikanCollatera : System.Web.UI.Page
    {
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if ((Request.QueryString["sDate"] != null) && (Request.QueryString["eDate"] != null))
                {
                    RenderPdf(Request.QueryString["sDate"].ToString(), Request.QueryString["eDate"].ToString(), true);
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            //RenderPdf(deStartDate.Date.ToString("yyyy/MM/dd"), deEndDate.Date.ToString("yyyy/MM/dd"), false);
            if (deStartDate.Date <= deEndDate.Date)
            {
                lbMessage.Text = "";
                RenderPdf(deStartDate.Date.ToString("yyyy/MM/dd"), deEndDate.Date.ToString("yyyy/MM/dd"), false);
            }
            else
            {
                lbMessage.Text = " Start Date > End Date";
            }
        }

        private void RenderPdf(string dateStart, string dateEnd, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(dateStart, "yyyy/MM/dd", null).Date;
            DateTime dEnd = DateTime.ParseExact(dateEnd, "yyyy/MM/dd", null).Date;
            ReportParameter rpStartDate = new ReportParameter();
            rpStartDate.Name = "dateStart";
            rpStartDate.Values.Add(dateStart);

            ReportParameter rpEndDate = new ReportParameter();
            rpEndDate.Name = "dateEnd";
            rpEndDate.Values.Add(dateEnd);

            //int diffResult = dEnd.Date.Subtract(dStart.Date).Days + 1;
            int diffResult = 1;
            CalendarService wsC = ImqWS.GetCalendarService();
            while (!dStart.Date.Equals(dEnd.Date))
            {
                dStart = dStart.AddDays(1);
                if ((dStart.DayOfWeek != DayOfWeek.Saturday) && (dStart.DayOfWeek != DayOfWeek.Sunday) && (wsC.SearchByDate(dStart.Date.ToString("MM/dd/yyyy")) == null))
                    diffResult += 1;
            }

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/COLW Recapitulation";
            serverReport.SetParameters(new ReportParameter[] { rpStartDate, rpEndDate });

            if (bSave)
            {
                string sFileName = "broker_COLW_Recapitulation_" + dStart.ToString("yyyyMMdd") + "-" + dEnd.ToString("yyyyMMdd") + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }  

    }
}