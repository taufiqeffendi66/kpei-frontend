﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.clientAccountWS;
using System.Diagnostics;
using Common.memberInfoWS;
using Common;
using Microsoft.Reporting.WebForms;
using Common.wsCalendar;


namespace imq.kpei.report
{
    public partial class brokerDHKMember : System.Web.UI.Page
    {
        private String aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if ((Request.QueryString["sDate"] != null) && (Request.QueryString["sMember"] != null) )
                {
                    RenderPdf(Request.QueryString["sDate"].ToString(), Request.QueryString["sMember"].ToString(), true);
                }
                else
                {
                    deDate.Date = DateTime.Now;
                    if (aUserMember.Trim().Equals("kpei"))
                    {
                        FillMember();
                    }
                    else
                    {
                        cmbMember.Enabled = false;
                        cmbMember.Text = aUserMember.Trim();
                        FillSID(aUserMember);
                    }
                }
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (!cmbMember.Text.Trim().Equals("All"))
                RenderPdf(deDate.Date.ToString("yyyy/MM/dd"), cmbMember.Text.Trim(), false);
            else
                RenderPdfAll(deDate.Date.ToString("yyyy/MM/dd"), false);
        }

        protected void cmbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSID(cmbMember.SelectedItem.Text.Trim());
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                    cmbMember.Items.Add("All", "All");
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void FillSID(String MemberId)
        {

        }

        private void RenderPdf(string sDate, string sMemberID, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(sDate);
            
            ReportParameter rpMemberID = new ReportParameter();
            rpMemberID.Name = "memberId";
            rpMemberID.Values.Add(sMemberID);
/*
            // H+3
            DateTime h3 = dStart;
            //DateTime h3 = deDate.Date;// DateTime.Now;
            int x = 0;
            CalendarService wsC = ImqWS.GetCalendarService();
            for (int y = 0; y < 3; y++)
            {
                x++;
                while (true)
                {
                    //h3 = DateTime.Now.AddDays(x);
                    h3 = h3.AddDays(1);
                    if (h3.DayOfWeek == DayOfWeek.Saturday)
                        x += 1;
                    else if (h3.DayOfWeek == DayOfWeek.Sunday)
                        x += 1;
                    else if (wsC.SearchByDate(h3.Date.ToString("MM/dd/yyyy")) != null)
                        x += 1;
                    else break;
                }
            }
 */ 

            ReportParameter rpH1 = new ReportParameter();
            rpH1.Name = "h1";
            rpH1.Values.Add(ImqWS.HplusOne(dStart).Date.ToString("yyyy/MM/dd"));

            ReportParameter rpH3 = new ReportParameter();
            rpH3.Name = "h3";
            rpH3.Values.Add(ImqWS.HplusTri(dStart).Date.ToString("yyyy/MM/dd"));

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/CRL All Client For Member";
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpMemberID, rpH1, rpH3 });

            if (bSave)
            {
                string sFileName = "broker_DHK_MEMBER_" + dStart.ToString("yyyyMMdd") + "_" + sMemberID + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }

        private void RenderPdfAll(string sDate, bool bSave)
        {
            DateTime dStart = DateTime.ParseExact(sDate, "yyyy/MM/dd", null).Date;

            ReportParameter rpDate = new ReportParameter();
            rpDate.Name = "date";
            rpDate.Values.Add(sDate);
/*
            // H+3
            DateTime h3 = dStart;
            //DateTime h3 = deDate.Date;// DateTime.Now;
            int x = 0;
            CalendarService wsC = ImqWS.GetCalendarService();
            for (int y = 0; y < 3; y++)
            {
                x++;
                while (true)
                {
                    //h3 = DateTime.Now.AddDays(x);
                    h3 = h3.AddDays(1);
                    if (h3.DayOfWeek == DayOfWeek.Saturday)
                        x += 1;
                    else if (h3.DayOfWeek == DayOfWeek.Sunday)
                        x += 1;
                    else if (wsC.SearchByDate(h3.Date.ToString("MM/dd/yyyy")) != null)
                        x += 1;
                    else break;
                }
            }
 */
            ReportParameter rpH1 = new ReportParameter();
            rpH1.Name = "h1";
            rpH1.Values.Add(ImqWS.HplusOne(dStart).Date.ToString("yyyy/MM/dd"));

            ReportParameter rpH3 = new ReportParameter();
            rpH3.Name = "h3";
            rpH3.Values.Add(ImqWS.HplusTri(dStart).Date.ToString("yyyy/MM/dd"));

            string path = ImqSession.GetDIRReportBroker();
            if (path == "")
                path = pathOEM;

            ServerReport serverReport = ReportViewer1.ServerReport;
            serverReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = path + "/CRL All Member";
            serverReport.SetParameters(new ReportParameter[] { rpDate, rpH1, rpH3 });

            if (bSave)
            {
                string sFileName = "Broker_CRL_All_Member" + dStart.ToString("yyyyMMdd") + ".pdf";

                Byte[] results = serverReport.Render("PDF");

                System.IO.FileStream fs = null;
                fs = System.IO.File.Create(Server.MapPath("Report/" + sFileName));
                fs.Write(results, 0, results.Length);
                fs.Close();
            }
            else
            {
                ReportViewer1.ServerReport.Refresh();
            }

        }


    }
}