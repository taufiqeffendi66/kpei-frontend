﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.XtraReports.Web;

using Common.wsSysParam;
using DevExpress.XtraReports.UI;
using System.Collections;
using Common;

namespace imq.kpei.report.sysparam
{
    public partial class sysparamReport : System.Web.UI.Page
    {
        private dtSysParamMain[] dtSP;
        private String aUserLogin;
        private String aUserMember;


        protected void Page_Load(object sender, EventArgs e)
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            ReportViewer1.Report = CreateReport();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ReportViewer1.Report = CreateReport();
        }

        private void getData()
        {
            SysParamService wsSP = ImqWS.GetSysParamService();
            try
            {
                    dtSP = wsSP.View();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsSP.Abort();
                throw;
            }
        }

        XtraReport CreateReport()
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            // Create a list. 
            ArrayList listDataSource = new ArrayList();
            getData();
            //for (int n = 0; n < dtSP.Length; n++)
            //{
            //    dtSysParamMain aData = new dtSysParamMain();
            //    aData. memberId = dtSP[n].id.memberId;
            //    aData.userId = dtSP[n].id.userId;
            //    aData.groupId = dtSP[n].id.groupId;
            //    listDataSource.Add(aData);
            //}

            //listDataSource.Add(new RecordUser(
            //    UGL[n].id.userId
            //    , UGL[n].id.memberId
            //    , UGL[n].id.groupId
            //    ));

            // Create a report. 
            sysparamXtraReport report = new sysparamXtraReport();

            // Bind the report to the list. 
            report.DataSource = dtSP;

            report.SetBoundLabel();
            return report;
        }


    }

 
}