﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.report.sysparam
{
    public partial class sysparamXtraReport : DevExpress.XtraReports.UI.XtraReport
    {
        public sysparamXtraReport()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            clParam.DataBindings.Add("Text", DataSource, "param");
            clValue.DataBindings.Add("Text", DataSource, "value");
            clRemark.DataBindings.Add("Text", DataSource, "remark");
        }
    }
}
