﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;

namespace imq.kpei.report
{
    public partial class DetailExposure : System.Web.UI.Page
    {
        private string aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        private string nameKPEI = "/Detail_Exposure";
        private string nameAK = "/Detail_Exposure_AK";
        private string reportPath;
        //ReportParameter rptDate = null;
        //ReportParameter rptMember = null;
        DateTime today;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                string path = ImqSession.GetDIRReportBroker();
                if (path == "")
                    path = pathOEM;

                if (aUserMember.Equals("kpei"))
                {
                    reportPath = path + nameKPEI;
                }
                else
                {
                    reportPath = path + nameAK;
                    //rptMember = new ReportParameter();
                    //rptMember.Name = "memberId";
                    //rptMember.Values.Add(aUserMember);
                }

                ShowReport();
            }
        }

        private void ShowReport()
        {
            //today = DateTime.Now;
            //rptDate = new ReportParameter();
            //rptDate.Name = "rpt_date";
            //rptDate.Values.Add(today.ToString("yyyy/MM/dd"));

            ReportParameter rptDate = new ReportParameter();
            rptDate.Name = "rpt_date";
            rptDate.Values.Add(DateTime.Now.Date.ToString("yyyy/MM/dd"));

            //Uri uri = new Uri(ImqSession.GetURLReport());
            string displayName = "Detail Exposure - " + today.ToString("yyyy/MM/dd");
            ServerReport serverReport = ReportViewer1.ServerReport;
            ReportViewer1.ServerReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = new Uri(ImqSession.GetURLReport());
            serverReport.ReportPath = reportPath;


            if (aUserMember.Equals("kpei"))
                serverReport.SetParameters(rptDate);
            else
            {
                ReportParameter rptMember = new ReportParameter();
                rptMember.Name = "memberId";
                rptMember.Values.Add(aUserMember);
                serverReport.SetParameters(new ReportParameter[] { rptDate, rptMember });
            }

            ReportViewer1.ServerReport.DisplayName = displayName;
            ReportViewer1.ServerReport.Refresh();
        }
    }
}