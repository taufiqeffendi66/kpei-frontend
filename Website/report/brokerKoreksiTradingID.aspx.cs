﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Common.memberInfoWS;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;
namespace imq.kpei.report
{
    public partial class brokerKoreksiTradingID : System.Web.UI.Page
    {
        private string aUserMember;
        private string pathOEM = "/SKD/skdBroker";
        private string nameID = "/Laporan_Koreksi_Trading_ID";
        private string nameEN = "/Trading ID Correction Report";
        ReportParameter rptDate = null;
        ReportParameter rptMember = null;
        DateTime today;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {

                cmbMember.Items.Clear();
                if (!aUserMember.Equals("kpei"))
                {
                    cmbMember.Items.Add(aUserMember, aUserMember);
                }
                else
                {
                    FillMember();
                }
                //ShowReport();
            }
        }

        private void FillMember()
        {
            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
            try
            {
                Common.memberInfoWS.memberInfo[] list = mis.get();
                if (list != null)
                {
                    cmbMember.Items.Clear();
                    cmbMember.Items.Add("All", "All");
                    foreach (Common.memberInfoWS.memberInfo mi in list)
                    {
                        if (!mi.memberId.Trim().Equals("kpei"))
                            cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {
            today = DateTime.Now;
            rptDate = new ReportParameter();
            rptDate.Name = "date";
            rptDate.Values.Add(today.ToString("yyyy/MM/dd"));

            Uri uri = new Uri(ImqSession.GetURLReport());
            string displayName = "Detail Exposure - " + today.ToString("yyyy/MM/dd");
            ServerReport serverReport = ReportViewer1.ServerReport;
            ReportViewer1.ServerReport.ReportServerCredentials = new Common.ReportingServiceCredential();
            serverReport.ReportServerUrl = uri;
            string path = ImqSession.GetDIRReportBroker();
            if(path == "")
                path = pathOEM;
            
            if (cmbMember.Text.Trim() == "All")
            {
                if (cmbLanguage.Text.Trim() == "English") serverReport.ReportPath = path + nameEN + "_KPEI";
                else serverReport.ReportPath = path + nameID + "_KPEI";
                serverReport.SetParameters(rptDate);
            }
            else
            {
                rptMember = new ReportParameter();
                rptMember.Name = "memberId";
                rptMember.Values.Add(cmbMember.Text.Trim());

                if (cmbLanguage.Text.Trim() == "English") serverReport.ReportPath = path + nameEN + "_AK";
                else serverReport.ReportPath = path + nameID + "_AK";
                serverReport.SetParameters(new ReportParameter[] { rptDate, rptMember });
            }

            ReportViewer1.ServerReport.DisplayName = displayName;
            ReportViewer1.ServerReport.Refresh();
        }
    }
}