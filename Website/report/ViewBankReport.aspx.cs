﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Common;
using Common.bankWS;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;

namespace imq.kpei.report
{
    public partial class ViewBankReport : System.Web.UI.Page
    {
        private String aUserMember;


        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            FillBank();
            if (!IsPostBack)
            {
                if (Request.QueryString["type"] != null)
                {
                    string req = Request.QueryString["type"].ToString();
                    string types = req.Substring(0, req.IndexOf('?'));
                    hfBankRep.Set("type", types);

                    dtDate.Date = DateTime.Now;

                    if (types.Equals("12"))
                    {
                        lblBank.Visible = false;
                        cmbBank.Visible = false;
                        //btnOK.Visible = false;
                        //ServerReport serverReport = ReportViewer1.ServerReport;
                        //ReportViewer1.ServerReport.ReportServerCredentials = new Common.ReportingServiceCredential();
                        //Uri uri = new Uri(ImqSession.GetConfig("REPORT_URL"));
                        //serverReport.ReportServerUrl = uri;
                        //string reportPath = String.Empty;
                        //reportPath = "/skd/bank_laporan posisi dana (antar bank)";
                        //serverReport.ReportPath = reportPath;
                        //ReportViewer1.ServerReport.Refresh();
                    }
                }
            }


        }

        private void ShowReport()
        {
            string type = hfBankRep.Get("type").ToString();
            string displayName = "";
            string reportPath = String.Empty;
            string dateNow = String.Format("{0:ddMMyyyy hhmmss}", DateTime.Now);

            string path = ImqSession.GetDIRReportBank();
            if (path == "")
                path = "/skd/skdBank";

            switch (type)
            {
                case "1": reportPath = path + "/Margin Call Payment"; displayName = @"Margin Call Payment - " + dateNow; break;
                case "2A": reportPath = path + "/Receive Settlement"; displayName = @"Receive Settlement - " + dateNow; break;
                case "2B": reportPath = path + "/Receive - Member collateral"; displayName = @"Reveive - Member Collateral - " + dateNow; break;
                //case "3A"   : reportPath = "/kpei/bank_Pemenuhan hak terima nasabah"; break;
                case "3B": reportPath = path + "/Receive - Client collateral"; displayName = @"Receive - Client Collateral - " + dateNow; break;
                case "4A": reportPath = path + "/Deliver - Member Collateral"; displayName = @"Receive - Member Collateral - " + dateNow; break;
                case "4B": reportPath = path + "/Deliver Settlement"; displayName = @"Deliver Settlement_" + dateNow; break;
                case "5": reportPath = path + "/Deliver - Client Collateral"; displayName = @"Deliver - Client Collateral - " + dateNow; break;
                case "6": reportPath = path + "/GF Usage"; displayName = @"GF Usage - " + dateNow; break;
                case "7": reportPath = path + "/MCC Usage"; displayName = @"MCC Usage - " + dateNow; break;
                case "8": reportPath = path + "/Member COLW"; displayName = @"Member COLW - " + dateNow; break;
                case "9": reportPath = path + "/Client COLW"; displayName = @"Client COLW - " + dateNow; break;
                case "10A1": reportPath = path + "/Penalty of GF Repayment - Member"; displayName = @"Penalty Of GF Repayment - Member - " + dateNow; break;
                case "10A2": reportPath = path + "/Penalty of GF Repayment - KPEI"; displayName = @"Penalty Of GF Repayment - KPEI - " + dateNow; break;
                case "10B1": reportPath = path + "/Penalty of GF Allocation"; displayName = @"Penalty Of GF Allocation - " + dateNow; break;
                case "10B2": reportPath = path + "/GF Allocation"; displayName = @"GF Allocation - " + dateNow; break;
                case "11": reportPath = path + "/Book Transfer KPEI"; displayName = @"Book Transfer KPEI - " + dateNow; break;
                case "12": reportPath = path + "/Cash Position Report"; displayName = @"Cash Position Report - " + dateNow; break;
                case "13": reportPath = path + "/Document Receipt"; displayName = @"Document Receipt - " + dateNow; break;
                case "14": reportPath = path + "/List of Deliver Member"; displayName = @"List Of Deliver Member - " + dateNow; break;
                case "15": reportPath = path + "/List of Receive Member"; displayName = @"List Of Receive Member - " + dateNow; break;
                case "16": reportPath = path + "/Member MCC Withdraw"; displayName = @"Member MCC Withdraw - " + dateNow; break;
                case "17": reportPath = path + "/Allocation GF Fee"; displayName = @"Allocation GF Fee - " + dateNow; break;
                case "18": reportPath = path + "/Allocation Clearing Fee"; displayName = @"Allocation Clearing Fee - " + dateNow; break;
            }

            if (reportPath != String.Empty)
            {
                Uri uri = new Uri(ImqSession.GetURLReport());

                ReportParameter rptDate = new ReportParameter();
                rptDate.Name = "bank_date";
                rptDate.Values.Add(dtDate.Date.ToString("yyyy/MM/dd"));

                ServerReport serverReport = ReportViewer1.ServerReport;
                ReportViewer1.ServerReport.ReportServerCredentials = new Common.ReportingServiceCredential();
                serverReport.ReportServerUrl = uri;
                serverReport.ReportPath = reportPath;

                if (type.Equals("12"))
                    serverReport.SetParameters(rptDate);
                else
                {
                    ReportParameter rptParam = new ReportParameter();
                    rptParam.Name = "bank_code";
                    rptParam.Values.Add(cmbBank.SelectedItem.Value.ToString());
                    serverReport.SetParameters(new ReportParameter[] { rptParam, rptDate });
                }
                ReportViewer1.ServerReport.DisplayName = displayName;
                ReportViewer1.ServerReport.Refresh();
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            
            if (cmbBank.IsVisible()) 
                if(cmbBank.SelectedIndex < 0)
                    return;
            ShowReport();
        }

        private void FillBank()
        {
            BankWSService bws = null;
            bank[] list = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                if (bws != null)
                {
                    list = bws.get();
                    if (list != null)
                    {
                        cmbBank.DataSource = list;
                        cmbBank.DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                if (bws != null)
                    bws.Abort();

                Debug.WriteLine(e.Message);
            }
            finally
            {
                if (bws != null)
                    bws.Dispose();
            }

        }
    }
}