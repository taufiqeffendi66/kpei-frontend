﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Information.aspx.cs" Inherits="imq.kpei.Information" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div><dx:ASPxLabel ID="lblTitle" runat="server" Wrap="True" Font-Size="24pt" />
        </div>
        <hr />
        <div style="background-color:White;padding:4px;border-top:4px;"><dx:ASPxLabel ID="lblMessage" runat="server" Wrap="True" Font-Size="16pt"/>
        </div>

        <br />
        <dx:ASPxButton ID="btnOK" runat="server" Text="OK" onclick="btnOK_Click"/>
    </div>
</asp:Content>
