﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginKeyFile.aspx.cs" Inherits="imq.kpei.loginKeyFile" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta  http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="Styles/login/login-box.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" src="../../js/util/page.js" ></script>
    <script type="text/javascript">
        var path = new File("~/desktop");
        var file = path.openDlg("Choose File:");
        while (file.alias) {
            file = file.resolve().openDlg("Choose File:");
        }
    </script>
    <title>Sistem Kliring Derivatif</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 100px 0 0 250px; height: 175px;">
        <div id="loginKeyFile-box">
            <table cellpadding="0" cellspacing="2" border="0" width="100%" style="height: 241px">
                <tr><td colspan="2" class="title"><img src="Styles/login/images/keys.png" /><h2>Sistem Kliring Derivatif</h2><br /><br /></td></tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">User :</label></td>
                    <td><input id="txtUser" runat="server" name="q" class="form-txt" title="Username" value="" maxlength="2048" readonly="readonly" /></td>
                </tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">Password :</label></td>
                    <td><input id="txtPassword" runat="server" name="q" class="form-txt" title="Password" value="" maxlength="2048" type="password" /></td>
                </tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">New Password :</label></td>
                    <td><input id="txtPasswordNew" runat="server" name="q" class="form-txt" title="New Password" value="" maxlength="2048" type="password" /></td>
                </tr>
                <tr></tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">Confrm Password :</label></td>
                    <td><input id="txtPasswordConfrm" runat="server" name="q" class="form-txt" title="Member" value="" type=password maxlength="2048" /></td>
                </tr>
                <tr>
                    <td class="labelpin"><label for="txt_name">Key File :</label></td>
                    <td><asp:FileUpload ID="FileUpload1" runat="server" class="form-txt" /></td>
                </tr>
                <tr>
                    <td colspan="2"><asp:LinkButton ID="btnLogin" runat="server" OnClick="btnLogin_Click" class="login_button_pin"><img src="Styles/login/images/login_btn.png" /></asp:LinkButton></td>
                </tr>
                <tr>

                    <td colspan="2"><dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
            </table>
        </div>
    </div>
    <div><asp:Label ID="Label1" runat="server"></asp:Label></div>
    </form>
</body>
</html>
