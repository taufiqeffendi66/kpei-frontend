﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="imq.kpei.login" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Styles/login/login-box.css" rel="stylesheet" type="text/css" />    
    <title>Sistem Kliring Derivatif</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 100px 0 0 300px; height: 175px;">
        <div id="loginPinCode-box">
            <table cellpadding="0" cellspacing="2" border="0" width="100%">
                <tr><td colspan="2" class="title"><img src="Styles/login/images/keys.png" alt="SKD" /><h2>Sistem Kliring Derivatif</h2><br /><br /></td></tr>
                <tr>
                    <td class="label"><label for="txt_name">Member :</label></td>
                    <td><input id="txtMember" runat="server" name="qw" class="form-txt txttoupper" title="Member" value="" maxlength="2048" /></td>
                </tr>
                <tr>
                    <td class="label"><label for="txt_name">User :</label></td>
                    <td><input id="txtUser" runat="server" name="qa" class="form-txt" title="Username" value="" maxlength="2048" /></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>                   
                        <dx:ASPxButton ID="btnLogin" runat="server" OnClick="btnLogin_Click" UseSubmitBehavior="true" AutoPostBack="true" >
                            <Image Url="~/Styles/login/images/login_btn.png"></Image>
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><dx:ASPxLabel ID="lbError" runat="server" ForeColor="#ff0000"></dx:ASPxLabel></td></tr>
            </table>
        </div>
    </div>
    </form>
</body>

</html>


