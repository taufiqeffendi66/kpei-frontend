﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="signout.aspx.cs" Inherits="imq.kpei.account.signout" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title"><h3>Sign Out</h3></div>
        <p>Do You Want To Sign Out ?</p>

        <br />
        <table>
            <tr>
                <td><dx:ASPxButton ID="btnOK" runat="server" Text="OK" Width="60px" onclick="btnOk_Click"/></td>
                <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" Width="60px" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
