﻿<%@ Page Title="Manual Settlement" Language="C#" AutoEventWireup="true" CodeBehind="ManualSettlementPage.aspx.cs" Inherits="imq.kpei.derivatif.manualsettlementpage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
   
     <script language="javascript" type="text/javascript">
        // <![CDATA[

         function OnGridFocusedRowChanged() 
         {
             grid.GetRowValues(grid.GetFocusedRowIndex(), 'date;time;bank;source;target;value;executionDate;status;idx;', OnGetRowValues);

         }
         
         function OnGetRowValues(values) {
             hfbank.Set("date", values[0]);
             hfbank.Set("time", values[1]);
             hfbank.Set("bank", values[2]);
             hfbank.Set("source", values[3]);
             hfbank.Set("target", values[4]);
             hfbank.Set("value", values[5]);
             hfbank.Set("executionDate", values[6]);
             hfbank.Set("status", values[7]);
             hfbank.Set("idx", values[8]);
             
         }

         function view(s, e) {
             window.open("ManualSettlementView.aspx" + "?bank=" + cmbBank.GetText(), "", "fullscreen=yes;scrollbars=auto");
         }

        // ]]>
    </script>   
    <div class="content">
               <div class="title"> Manual Settlement</div>
                
                <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
                <table>
                    <tr>
                        <td style="width:128px" nowrap="nowrap">Bank</td>
                        <td><dx:ASPxComboBox ID="cmbBank" runat="server" IncrementalFilteringMode="Contains"
                            DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbBank">
                                <DropDownButton Visible="false"></DropDownButton>
                            </dx:ASPxComboBox> </td>
                        <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" AutoPostBack="false" 
                                onclick="btnSearch_Click" >
                        
                             </dx:ASPxButton>
                                </td>
                        <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                                <ClientSideEvents Click="view"/>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            
                    <br />
            
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnNew" runat="server" Text="New" AutoPostBack="false" 
                                    onclick="btnNew_Click" />                        
                            </td>
                        </tr>
                    </table>
            
                    <dx:ASPxGridView ID="gvManualSettlement" ClientInstanceName="grid" 
                   runat="server" AutoGenerateColumns="False" KeyFieldName="id.createDate" 
                    CssClass="grid" ondatabinding="gvManualSettlement_DataBinding"
                   oncustomcolumndisplaytext="gvManualSettlement_CustomColumnDisplayText">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Code" VisibleIndex="0" FieldName="bank" 
                            Visible="False" >
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Bank" VisibleIndex="4" FieldName="bank" >
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Source" VisibleIndex="5" 
                            FieldName="id.source"   >
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Target" VisibleIndex="6" 
                            FieldName="id.target"   >
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="7" 
                            FieldName="value"   >
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="10" 
                            FieldName="status" >
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewBandColumn Caption="Execution" VisibleIndex="8">
                            <Columns>
                                <dx:GridViewDataDateColumn Caption="Time" FieldName="executionDate" 
                                    VisibleIndex="1">
                                    <PropertiesDateEdit DisplayFormatString="HH:mm:ss">
                                    </PropertiesDateEdit>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataDateColumn Caption="Date" FieldName="executionDate" 
                                    VisibleIndex="0">
                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                                    </PropertiesDateEdit>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataDateColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Create" VisibleIndex="1">
                            <Columns>
                                <dx:GridViewDataDateColumn Caption="Date" FieldName="id.createDate" 
                                    VisibleIndex="0">
                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                                    </PropertiesDateEdit>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataDateColumn Caption="Time" FieldName="id.createDate" 
                                    VisibleIndex="1">
                                    <PropertiesDateEdit DisplayFormatString="HH:mm:ss">
                                    </PropertiesDateEdit>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataDateColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </dx:GridViewBandColumn>
                    </Columns>
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <Settings ShowGroupButtons="False"  ShowVerticalScrollBar="True" />
                        <SettingsCustomizationWindow Height="100%" />
                    <Styles>
                        <AlternatingRow Enabled="True" />
                        <Header CssClass="gridHeader" />
                    </Styles>
                    <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
                    <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                </dx:ASPxGridView>
           
    </div>
       
        <dx:ASPxHiddenField ID="hfBank" runat="server" ClientInstanceName="hfbank" />
       
    
</asp:Content>
