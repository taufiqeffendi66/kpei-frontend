﻿<%@ Page Title="Client Account" Language="C#"  AutoEventWireup="true" CodeBehind="ClientAccountPage.aspx.cs" Inherits="imq.kpei.derivatif.ClientAccountPage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
    //<![CDATA[
        function OnGridFocusedRowChanged(s, e) {
            
                grid.GetRowValues(grid.GetFocusedRowIndex(),
                    'id.memberId;memberInfo.memberName;sid;id.tradingId;accountId;role;bank.bankCode;collateralAccountNo;bank.bankName;idx;',
                    OnGetRowValues);
            
        }

        function ConfirmDelete(s, e) {
            e.processOnServer = confirm("Are you sure Delete this Client Account with \nMember ID : " + hfclient.Get("member_id") +
                                        "\nTrading ID : " + hfclient.Get("trading_id") + " ?");
        }

        function OnGetRowValues(values) {
            hfclient.Set("member_id", values[0]);
            hfclient.Set("memberName", values[1]);
            hfclient.Set("sid", values[2]);
            hfclient.Set("trading_id", values[3]);
            hfclient.Set("account_id", values[4]);
            hfclient.Set("role", values[5]);
            hfclient.Set("bankCode", values[6]);
            hfclient.Set("collateral_acc_no", values[7]);
            hfclient.Set("bank_name", values[8]);
            hfclient.Set("idx", values[9]);

            /*if (hfclient.Get("member_id") != undefined) {
                btndel.SetEnabled(true);
                btned.SetEnabled(true);
            } else {
                btndel.SetEnabled(false);
                btned.SetEnabled(false);
            }*/
            
        }

        function view(s, e) {
            window.open("ClientAccountView.aspx?bank=" + cmbBank.GetText() + "&member=" + cmbMember.GetText() + "&sid=" + cmbSid.GetText(), "", "fullscreen=yes;scrollbars=auto");
        }
        
    //]]>
    </script>
    <div class="content">
        <div class="title">Client Account Number</div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
         <table>
            <tr>
                <td style="width:128px" nowrap="nowrap">Bank</td>
                <td> <dx:ASPxComboBox ID="cmbBank" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbBank">
                       
                        <DropDownButton Visible="false"></DropDownButton>
                </dx:ASPxComboBox></td>
                <td><dx:ASPxButton ID="btnGo" runat="server" Text="Search" Width="65px" 
                        onclick="btnGo_Click"></dx:ASPxButton> </td>   
                <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                        <ClientSideEvents Click="view"/>
                    </dx:ASPxButton></td>                     
            </tr>
            <tr>
                <td nowrap="nowrap">Member ID</td>
                <td><dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbMember" >
                       
                        <DropDownButton Visible="false"></DropDownButton>
                </dx:ASPxComboBox> </td>
            </tr>
            <tr>
                <td nowrap="nowrap">SID</td>
                <td><dx:ASPxComboBox ID="cmbSID" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbSid">
                       
                        <DropDownButton Visible="false"></DropDownButton>
                </dx:ASPxComboBox> </td>
            </tr>
        </table>

            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />                        
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" 
                            onclick="btnEdit_Click" ClientInstanceName="btned" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" 
                            ClientInstanceName="btndel" onclick="btnDelete_Click" 
                            ClientSideEvents-Click="ConfirmDelete" />
                    </td>
                </tr>
            </table>
            <br />   
        <dx:ASPxGridView ID="gvMember" runat="server" AutoGenerateColumns="false" CssClass="grid" EnableCallBacks="true"
            ClientInstanceName="grid" ondatabinding="gvMember_DataBinding" KeyFieldName="id.memberId">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" FieldName="id.memberId"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="1" FieldName="memberInfo.memberName"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="2" FieldName="sid"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Trading ID" VisibleIndex="3" FieldName="id.tradingId"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Account ID" VisibleIndex="4" FieldName="accountId"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Role" VisibleIndex="5" FieldName="role" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Bank Name" VisibleIndex="6" FieldName="bank.bankName"></dx:GridViewDataTextColumn>                
                <dx:GridViewDataTextColumn Caption="Collateral <br /> Account No" VisibleIndex="8" FieldName="collateralAccountNo"></dx:GridViewDataTextColumn>                
                <dx:GridViewDataTextColumn Caption="Bank Code" VisibleIndex="10" FieldName="bank.bankCode" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="IDX" VisibleIndex="10" FieldName="idx" Visible="false"></dx:GridViewDataTextColumn>
            </Columns>
            <Styles>
                <AlternatingRow Enabled="True" />
                <Header CssClass="gridHeader" />
            </Styles>
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
            <ClientSideEvents FocusedRowChanged="OnGridFocusedRowChanged" RowClick="OnGridFocusedRowChanged"/>
             <SettingsPager PageSize="20">
            </SettingsPager>
            <Settings ShowVerticalScrollBar="True" />
        </dx:ASPxGridView>
        
        <dx:ASPxHiddenField ID="hfClient" runat="server" ClientInstanceName="hfclient"></dx:ASPxHiddenField>
    
    </div>
</asp:Content>
