﻿<%@ Page Title="Member Account Number Edit" Language="C#"  AutoEventWireup="true" CodeBehind="MemberAccountEdit.aspx.cs" Inherits="imq.kpei.derivatif.memberaccountedit" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
   <script type="text/javascript">
   //<![CDATA[
       function OnValidation(s, e) {
           var val = e.value;
           if (val == null)
               return;
           if (val.length < 1)
               e.isValid = false;

       }
       function CheckKey(s, e) {
           if ((e.htmlEvent.keyCode >= 48 && e.htmlEvent.keyCode <= 57) ||
              (e.htmlEvent.keyCode >= 96 && e.htmlEvent.keyCode <= 105) ||
              e.htmlEvent.keyCode == 8 || e.htmlEvent.keyCode == 40 || e.htmlEvent.keyCode == 144 ||
              e.htmlEvent.keyCode == 9)  {
               return true;
           }
           else {
               ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
           }
       }

       function OnSIDValidation(s, e) {
            var val = e.value;
            if (val == null)
               return;

           if (val.length < 15)
               e.isValid = false;
           else
               txtTrading.SetText(val.substr(7, 6));
       }
   //]]>
   </script>
        <div class="content">
            <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" Text="Edit Member Account" CssClass="title" /></div>
            <dx:ASPxHiddenField ID="hdMemberAcc" runat="server">
            </dx:ASPxHiddenField>
            <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
            <div id="plApproval">
                <table>
                    <tr>
                        <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                                ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                    </tr>
                </table>
            </div>
            <table>
                <tr>
                    <td style="width:128px">Member ID<span style="color:Red">*</span></td>
                    <td><dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="StartsWith"
                        DropDownRows="10" DropDownStyle="DropDownList" Width="170px"                       
                        EnableClientSideAPI="true" MaxLength="5">
                       <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />                                    
                            </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                </dx:ASPxComboBox> </td>                        
                </tr>               
                <tr>
                    <td>SID<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxTextBox ID="txtSID" runat="server" Width="170px" 
                            EnableClientSideAPI="true" Size="15" MaxLength="15">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />                                    
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnSIDValidation" />
                        </dx:ASPxTextBox>
                    </td>                        
                </tr>
                 <tr>
                    <td>Trading ID<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxTextBox ID="txtTrading" runat="server" EnableClientSideAPI="true" ClientInstanceName="txtTrading"
                            MaxLength="6" Size="6" ReadOnly="true">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />                                    
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>                        
                </tr>
                <tr>
                    <td>Account ID<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxTextBox ID="txtAccount" runat="server" Width="170px" EnableClientSideAPI="true" MaxLength="20">
                            <ValidationSettings ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />                                    
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>                        
                </tr>
                
                <tr>
                    <td>Bank<span style="color:Red">*</span></td>
                    <td><dx:ASPxComboBox ID="cmbBank" runat="server" Width="170px"  IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" EnableClientSideAPI="true">
                        
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />                                    
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxComboBox></td>                        
                </tr>
               
                <tr>
                    <td>Security Deposit Account No<span style="color:Red">*</span></td>
                    <td><dx:ASPxTextBox ID="txtSecurityDeposit" runat="server" Width="170px" EnableClientSideAPI="true" MaxLength="20">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>                        
                </tr>
                
                <tr>
                    <td>Collateral Account No<span style="color:Red">*</span></td>
                    <td><dx:ASPxTextBox ID="txtCollateral" runat="server" Width="170px" EnableClientSideAPI="true" MaxLength="20">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>                        
                </tr>
                <tr>
                    <td>Settlement Account No<span style="color:Red">*</span></td>
                    <td><dx:ASPxTextBox ID="txtSettlement" runat="server" Width="170px" EnableClientSideAPI="true" MaxLength="20">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>                        
                </tr>
                <tr>
                    <td>Operational Account No<span style="color:Red">*</span></td>
                    <td><dx:ASPxTextBox ID="txtFree" runat="server" Width="170px" EnableClientSideAPI="true" MaxLength="20">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>                        
                </tr>
            </table>

            <br />
            <table>
                <tr>
                    <td> <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false"
                            onclick="btnSave_Click" Width="70px" /> </td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                            onclick="btnChecker_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                            onclick="btnApproval_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True"
                            onclick="btnCancel_Click" CausesValidation="false" Width="70px" /></td>
                            
                </tr>
            </table>
                   
        </div>
       
</asp:Content>
