﻿<%@ Page Title="Balance Withdrawal" Language="C#" AutoEventWireup="true" CodeBehind="withdrawal.aspx.cs" Inherits="imq.kpei.derivatif.withdrawal" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        // <![CDATA[
        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(),
                            'bwNo;bwDate;id.memberId;sid;accountNo;accountDescription;value;status;id.reffNo;reffNoClient;', OnGetRowValues);
        }

        function ConfirmDelete(s, e) {
            var stat = hdwith.Get("status");
            
            if (stat != "W"  && stat != "C")
                e.processOnServer = confirm("Are you sure cancelled this balance withdrawal \nNo Refference : " + hdwith.Get("reffNoClient") + " ?");
            else {
                alert("Unable to cancel, Request has been process");
                return false;
                
            }
        }

        function OnGetRowValues(values) {
            hdwith.Set("bwNo", values[0]);
            hdwith.Set("bwDate", values[1]);
            hdwith.Set("memberId", values[2]);
            hdwith.Set("sid", values[3]);
            hdwith.Set("accountNo",values[4]);
            hdwith.Set("value", values[6]);
            hdwith.Set("status", values[7]);
            hdwith.Set("reff", values[8]);
            hdwith.Set("reffNoClient", values[9]);
            var stat1 = trim(hdwith.Get("status"));

            if (stat1 != "W" && stat1 != "C")
                btnCancel.SetEnabled(true);
            else
                btnCancel.SetEnabled(false);
        }

        function trim(str) {
            if(str !=null)
                return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        }

        function view(s, e) {
            if (hdwith.Get("srcmember") == undefined || hdwith.Get("srcmember") == null)
                hdwith.Set("srcmember", "");

            window.open("withdrawalView.aspx?member=" + hdwith.Get("srcmember") + "&sid=" + cmbSid.GetText() + "&status=" + hdwith.Get("stat"), "", "fullscreen=yes;scrollbars=auto");
        }
        // ]]>
    </script>        
    <dx:ASPxHiddenField ID="hdWithdrawal" runat="server" ClientInstanceName="hdwith"></dx:ASPxHiddenField>
    <div class="content">
        <div class="title">Balance Withdrawal</div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <table>
            <tr>
                <td nowrap="nowrap">Member ID</td>
                <td><dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="StartsWith"
                            DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbMember"
                            ValueField="memberId" TextField="memberId" EnableCallbackMode="true" CallbackPageSize="10">
                        <DropDownButton Visible="false" />
                        
                    </dx:ASPxComboBox></td>
                    <td><dx:ASPxButton ID="btnGO" runat="server" Text="Search" Width="65px" 
                        onclick="btnGO_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" 
                            onclick="btnExport_Click">
                        <ClientSideEvents Click="view"/>
                    </dx:ASPxButton></td>
            </tr>
            <tr>
                <td>SID</td>
                <td><dx:ASPxComboBox ID="cmbSID" runat="server" IncrementalFilteringMode="StartsWith"
                            DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbSid"
                            ValueField="sid" TextField="sid" EnableCallbackMode="true" CallbackPageSize="10">
                            <DropDownButton Visible="false" />
                    </dx:ASPxComboBox></td>
            </tr>
            <tr>
                <td>Status</td>
                <td><dx:ASPxComboBox ID="cmbStatus" runat="server" IncrementalFilteringMode="StartsWith"
                            DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbStatus">
                            <Items>
                                <dx:ListEditItem Text="New" Value="N" />
                                <dx:ListEditItem Text="Repos" Value="P" />
                                <dx:ListEditItem Text="Cancelled" Value="C" />
                                <dx:ListEditItem Text="Failed" Value="F" />
                                <dx:ListEditItem Text="Withdrawn" Value="W" />
                            </Items>
                            <DropDownButton Visible="false" />
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td>Date</td>
                <td><dx:ASPxDateEdit ID="deDate" runat="server" EditFormat="Custom" 
                                 EditFormatString="dd/MM/yyyy">
            </dx:ASPxDateEdit></td>
            </tr>
            
        </table>
            <br />
        <table>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" ClientInstanceName="btnNew" />                        
                </td>
                <td>
                    <dx:ASPxButton ID="btnCancel" runat="server" Text="Request For Cancel" ClientInstanceName="btnCancel"
                        onclick="btnCancel_Click" ClientSideEvents-Click="ConfirmDelete" ClientEnabled="false"/>
                </td>                            
            </tr>
        </table>
        <br />   
               
        <dx:ASPxGridView ID="gvWithdrawal" runat="server" AutoGenerateColumns="False" 
            CssClass="grid" KeyFieldName="id.reffNo" ClientInstanceName="grid"
            ondatabinding="gvWithdrawal_DataBinding" 
            oncustomcolumndisplaytext="gvWithdrawal_CustomColumnDisplayText" >
            <Columns>
                
                <dx:GridViewDataTextColumn Caption="No " ReadOnly="True" UnboundType="String" Width="50"
                    VisibleIndex="0">
                    <Settings AllowAutoFilter="False" AllowAutoFilterTextInputTimer="False" 
                        AllowDragDrop="False" AllowGroup="False" AllowHeaderFilter="False" 
                        AllowSort="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="No Refference" VisibleIndex="1" FieldName="reffNoClient"/>
                <dx:GridViewDataDateColumn Caption="Date" FieldName="bwDate" VisibleIndex="2" Width="70" >                   
                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTimeEditColumn Caption="Time" FieldName="bwDate" Width="50" VisibleIndex="3" >
                    <PropertiesTimeEdit DisplayFormatString="HH:mm"></PropertiesTimeEdit>
                </dx:GridViewDataTimeEditColumn>                
                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="4" FieldName="id.memberId" />
                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="5" FieldName="sid"/>
                <dx:GridViewDataTextColumn Caption="Account <br />Description" VisibleIndex="6" FieldName="accountDescription"/>
                <dx:GridViewDataTextColumn Caption="Account No" VisibleIndex="7" FieldName="accountSourceNo"/>                
                <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="8" FieldName="value">
                    <PropertiesTextEdit  DisplayFormatString="#,##"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>                
                <dx:GridViewBandColumn Caption="Execution" VisibleIndex = "9">
                    <Columns>                    
                        <dx:GridViewDataTextColumn Caption="Date" VisibleIndex="1" FieldName="executionDateDescription" Width="80"/>
                        <dx:GridViewDataTextColumn Caption="Time" VisibleIndex="2" FieldName="executionTimeDescription" Width="75"/>                       
                    </Columns>
                </dx:GridViewBandColumn>                
                <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="10" FieldName="statusDescription" Width="90"/>
                <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="11" FieldName="status" Visible="false"/>
                <dx:GridViewDataTextColumn Caption="No" VisibleIndex="12" FieldName="bwNo" Width="36" Visible="false" />
                <dx:GridViewDataTextColumn Caption="No Refference" VisibleIndex="13" Visible="false" FieldName="id.reffNo"/>
            </Columns>
               
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
            <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
            <SettingsPager PageSize="20">
            </SettingsPager>
            <Styles>
                <AlternatingRow Enabled="True" />
                <Header CssClass="gridHeader" />
            </Styles>
            <Settings ShowVerticalScrollBar="True"  />
        </dx:ASPxGridView>

    </div>

</asp:Content>
