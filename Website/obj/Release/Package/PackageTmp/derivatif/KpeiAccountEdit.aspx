﻿<%@ Page Title="KPEI Account" Language="C#"  AutoEventWireup="true" CodeBehind="KpeiAccountEdit.aspx.cs" Inherits="imq.kpei.derivatif.KpeiAccountEdit" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        function OnValidation(s, e) {
            var val = e.value;
            if (val == null)
                return;
            if (val.length < 1)
                e.isValid = false;

        }

        function CheckKey(s, e) {
            if ((e.htmlEvent.keyCode == 189 || e.htmlEvent.keyCode == 109) ||
              (e.htmlEvent.keyCode >= 48 && e.htmlEvent.keyCode <= 57) ||
              (e.htmlEvent.keyCode >= 96 && e.htmlEvent.keyCode <= 105) ||
              e.htmlEvent.keyCode == 8 || e.htmlEvent.keyCode == 40 || e.htmlEvent.keyCode == 144 ||
              e.htmlEvent.keyCode == 9) {
                return true;
            }
            else {
                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
            }
        }
        //]]>
    </script>
    
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" 
                Text="Edit KPEI Account" CssClass="title" /></div>
        <dx:ASPxHiddenField ID="hfKpeiAcc" runat="server">
        </dx:ASPxHiddenField>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                            ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td>Bank<span style="color:Red">*</span></td>
                <td><dx:ASPxComboBox ID="cmbBank" runat="server" Width="170px"  EnableClientSideAPI="true" 
                    IncrementalFilteringMode="Contains" DropDownRows="10" DropDownStyle="DropDown" 
                        onselectedindexchanged="cmbBank_SelectedIndexChanged">
                        
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />                                    
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxComboBox></td>  
            </tr>
            <tr>
                <td nowrap="nowrap" width="130px">Settlement Account No<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtSettlementAccNo" runat="server" Width="170px" EnableClientSideAPI="true" MaxLength="30">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>
            </tr>
            <tr>
                <td nowrap="nowrap" width="130px">Penalty Account No<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtPenaltyAccNo" runat="server" Width="170px"  EnableClientSideAPI="true">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>
            </tr>
            <tr>
                <td nowrap="nowrap" width="130px">Guarantee Fund Account No<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtGuaranteeAccNo" runat="server" Width="170px"  EnableClientSideAPI="true" MaxLength="30">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>
            </tr>
            <tr>
                <td nowrap="nowrap" width="130px">Fee Fund Account No<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtFeeAccNo" runat="server" Width="170px"  EnableClientSideAPI="true" MaxLength="30">
                            <ValidationSettings ErrorText="Reqired">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings>
                            <ClientSideEvents Validation="OnValidation" KeyDown="CheckKey" />
                    </dx:ASPxTextBox></td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td>
                    <td> <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false"
                            onclick="btnSave_Click" Width="70px" /> </td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                            onclick="btnChecker_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                            onclick="btnApproval_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True"
                            onclick="btnCancel_Click" CausesValidation="false" Width="70px" /></td>
                            
            </tr>
        </table>
    </div>
</asp:Content>
