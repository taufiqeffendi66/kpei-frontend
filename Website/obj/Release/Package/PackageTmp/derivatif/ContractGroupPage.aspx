﻿<%@ Page Title="Contract Group" Language="C#" AutoEventWireup="true" CodeBehind="ContractGroupPage.aspx.cs" Inherits="imq.kpei.derivatif.contractgrouppage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
         // <![CDATA[

        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'groupName;description;id;', OnGetRowValues);
        }

        function ConfirmDelete(s, e) {
            e.processOnServer = confirm("Are you sure Delete this Contract Group \nGroup Name : " + hfcg.Get("groupName") + " ?");
        }

        function OnGetRowValues(values) {            
            hfcg.Set("groupName", values[0]);
            hfcg.Set("description", values[1]);
            hfcg.Set("id", values[2]);
            //btndel.SetEnabled(true);
            //btned.SetEnabled(true);
        }

        function view(s, e) {
            window.open("ContractGroupView.aspx?group=" + cmbGroup.GetText() + "&description=" + cmbDesc.GetText(), "", "fullscreen=yes;scrollbars=auto");
        }
        
        // ]]>
    </script>
        
        <div  class="content">            
            <div class="title"><h3>Contract Group</h3></div>
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" ForeColor="#ff0000" />
            <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
            <table>
                <tr>                        
                    <td style="width:128px" nowrap="nowrap">Group Name</td>
                    <td>
                       <dx:ASPxComboBox ID="cmbGroup" runat="server" IncrementalFilteringMode="Contains"
                            DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbGroup">
                                <DropDownButton Visible="false"></DropDownButton>
                            </dx:ASPxComboBox>
                    </td>                    
                    <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" 
                            onclick="btnSearch_Click" /></td>
                    <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                        <ClientSideEvents Click="view"/>
                    </dx:ASPxButton>
                </td>
                </tr>
                <tr>                        
                    <td style="width:128px" nowrap="nowrap">Description</td>
                    <td><dx:ASPxComboBox ID="cmbDesc" runat="server" IncrementalFilteringMode="Contains"
                            DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbDesc">
                                <DropDownButton Visible="false"></DropDownButton>
                            </dx:ASPxComboBox></td>                    
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" 
                            AutoPostBack="False" />                        
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" AutoPostBack="false"
                            ClientInstanceName="btned" onclick="btnEdit_Click" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" 
                            ClientInstanceName="btndel" AutoPostBack="false"
                            onclick="btnDelete_Click" ClientSideEvents-Click="ConfirmDelete"/>
                    </td>
                </tr>
            </table>
                
            <br />            
            <dx:ASPxGridView ID="gvContractGroup" runat="server" 
                AutoGenerateColumns="False" CssClass="grid" ClientInstanceName="grid" 
                ondatabinding="gvContractGroup_DataBinding" KeyFieldName="groupName" >
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ID" VisibleIndex="2" FieldName="id" Visible="false"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Group Name" VisibleIndex="0" 
                        FieldName="groupName" />
                    <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="1" 
                        FieldName="description" />
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" 
                    AllowSelectSingleRowOnly="True" />
                    
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Styles>
                <AlternatingRow Enabled="True" />
                <Header CssClass="gridHeader" />
            </Styles>
                <Settings ShowVerticalScrollBar="True" />
            </dx:ASPxGridView>
                 
        </div>
        
        <dx:ASPxHiddenField ID="hfCG" runat="server" ClientInstanceName="hfcg" />
</asp:Content>

