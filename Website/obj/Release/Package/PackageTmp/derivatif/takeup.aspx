﻿<%@ Page Title="Take Up" Language="C#" AutoEventWireup="true" CodeBehind="takeup.aspx.cs" Inherits="imq.kpei.derivatif.takeup" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
  
    <script type="text/javascript">
    //<![CDATA[
        function OnGridFocusedRowChanged(s,e) 
         {
             grid.GetRowValues(grid.GetFocusedRowIndex(), 'guNo;tuApproval;kpeiApproval;tuMemberId;guMemberId', OnGetRowValues);             
         }

         function OnGetRowValues(values) {
             hfGu.Set("guNo", values[0]);
             hfGu.Set("tuApproval", values[1]);
             hfGu.Set("kpeiApproval", values[2]);
             hfGu.Set("tuMemberId", values[3]);
             hfGu.Set("guMemberId", values[4]);

             if (trim(hfGu.Get("mb")) == "kpei") {
                 var kpeiApproval = trim(values[2]);
                 if (kpeiApproval.length> 0) {
                     btnApprove.SetEnabled(false);                     
                 } else {
                     btnApprove.SetEnabled(true);
                 }
             } else {
                 var tuApproval = trim(values[1]);
                 if (tuApproval.length > 0 ) {
                     btnApprove.SetEnabled(false);
                 } else {
                     btnApprove.SetEnabled(true);
                 }
             }
         }

         function trim(str) {
             return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
         }
         function view(s, e) {
             window.open("takeupView.aspx?member=" + cmbMember.GetText(), "", "fullscreen=yes;scrollbars=auto");
         }
    //]]>
    </script>
    <div id="searchbox">
            
              
    </div>
    <div class="content">
    <div class="title"><h3>Take Up</h3></div>
    <br />
        <table>
            <tr>
                <td>
                    <dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="Contains"
                    DropDownRows="10" DropDownStyle="DropDown"  MaxLength="5" ClientInstanceName="cmbMember">   
                        <DropDownButton Visible="false" />
                    </dx:ASPxComboBox> 
                </td>
                <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" 
                        onclick="btnSearch_Click" /></td>
                <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                        <ClientSideEvents Click="view"/>
                        </dx:ASPxButton></td>
            </tr>                    
        </table>          
        
                   
        <dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" CssClass="flRight" ClientInstanceName="btnApprove"
            onclick="btnApprove_Click" ClientEnabled="false"/>
        <div class="clear">
            <dx:ASPxGridView ID="gvGiveUp" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" 
            CssClass="grid" KeyFieldName="guNo" ondatabinding="gvGiveUp_DataBinding" Settings-ShowVerticalScrollBar="True">
           <Columns>
            <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="0" FieldName="guDate">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTimeEditColumn Caption="Time" VisibleIndex="1" FieldName="guDate">
                <PropertiesTimeEdit DisplayFormatString="HH:mm" />
            </dx:GridViewDataTimeEditColumn>
            <dx:GridViewBandColumn Caption="Give Up" VisibleIndex="2">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" FieldName="guNo" />
                    <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="1" FieldName="guMemberId"/>                
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="Take Up" VisibleIndex="3">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" FieldName="tuMemberId" />
                    <dx:GridViewDataTextColumn Caption="Approval" VisibleIndex="1" FieldName="tuApprovalDescription"/>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="KPEI <br />Approval" VisibleIndex="4" FieldName="kpeiApprovalDescription" />
            <dx:GridViewBandColumn Caption="Execution" VisibleIndex="5">
                <Columns>
                    <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="0" FieldName="executionTime">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTimeEditColumn Caption="Time" VisibleIndex="1" FieldName="executionTime">
                        <PropertiesTimeEdit DisplayFormatString="HH:mm" />
                    </dx:GridViewDataTimeEditColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="6" FieldName="statusDescription" />
            <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="7" FieldName="status" Visible="false" />
            <dx:GridViewDataTextColumn Caption="Kpei Approval" VisibleIndex="8" FieldName="kpeiApproval" Visible="false" />
            <dx:GridViewDataTextColumn Caption="Take Up Approval" VisibleIndex="7" FieldName="tuApproval" Visible="false" />
        </Columns>
        <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="true" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="detailGrid" runat="server" KeyFieldName="id.sid" Width="100%"
                    ondatabinding="detailGrid_DataBinding" OnLoad="detailGrid_OnLoad" OnBeforePerformDataSelect="detailGrid_DataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="0" FieldName="id.sid"/>
                        <dx:GridViewDataTextColumn Caption="Trading ID" VisibleIndex="1" FieldName="tradingId"/>
                        <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" FieldName="guNo" Visible="false"/>
                    </Columns>
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="true" />
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="detailNet" runat="server" KeyFieldName="sid" Width="100%"
                            ondatabinding="detailNet_DataBinding" OnLoad="detailNet_OnLoad" OnBeforePerformDataSelect="detailNet_DataSelect">
                            <Columns>                        
                                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="0" FieldName="sid" Visible="false"/>
                                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="1" FieldName="memberID" Visible="false"/>
                                <dx:GridViewDataTextColumn Caption="Series Contract" VisibleIndex="2" FieldName="series"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Net Buy" VisibleIndex="3" FieldName="netB"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Net Sell" VisibleIndex="3" FieldName="netS"></dx:GridViewDataTextColumn>
                           </Columns>
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <ClientSideEvents FocusedRowChanged="OnGridFocusedRowChanged" />
        <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
        <Styles>
            <AlternatingRow Enabled="True" />
            <Header CssClass="gridHeader" />
        </Styles>
        <SettingsPager PageSize="20">
        </SettingsPager>
    </dx:ASPxGridView>

        </div>
        <dx:ASPxHiddenField ID="hfGu" runat="server" ClientInstanceName="hfGu" />

    </div>

</asp:Content>
