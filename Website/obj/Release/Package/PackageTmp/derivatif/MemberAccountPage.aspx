﻿<%@ Page Title="Member Account" Language="C#" AutoEventWireup="true" CodeBehind="MemberAccountPage.aspx.cs" Inherits="imq.kpei.derivatif.memberaccountpage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
   
    <script type="text/javascript">
        // <![CDATA[

            function OnGridFocusedRowChanged() {
                grid.GetRowValues(grid.GetFocusedRowIndex(),
                    'memberId;memberInfo.memberName;sid;tradingId;accountId;role;bank.bankCode;securityDepositAccountNo;collateralAccountNo;settlementAccountNo;bank.bankName;idx;freeAccountNo;', 
                    OnGetRowValues);
            }

            function ConfirmDelete(s, e) {
                e.processOnServer = confirm("Are you sure Delete this Client Account \n with Member ID : "+ hdmember.Get("member_id") + " ?");
            }

            function OnGetRowValues(values) {
                hdmember.Set("member_id", values[0]);
                hdmember.Set("memberName", values[1]);
                hdmember.Set("sid", values[2]);
                hdmember.Set("trading_id", values[3]);
                hdmember.Set("account_id", values[4]);
                hdmember.Set("role", values[5]);
                hdmember.Set("bankCode", values[6]);
                hdmember.Set("sec_dep_acc_no", values[7]);
                hdmember.Set("collateral_acc_no", values[8]);
                hdmember.Set("settlement_acc_no", values[9]);
                hdmember.Set("bank_name", values[10]);
                hdmember.Set("idx", values[11]);
                hdmember.Set("free_acc_no", values[12]);
                /*
                if (hdmember.Get("member_id") != undefined) {
                    btndel.SetEnabled(true);
                    btned.SetEnabled(true);
                } else {
                    btndel.SetEnabled(false);
                    btned.SetEnabled(false);
                }
                */
            }

            function view(s, e) {
                window.open("MemberAccountView.aspx?bank=" + cmbBank.GetText() + "&member=" + cmbMember.GetText() + "&sid=" + cmbSID.GetText(), "", "fullscreen=yes;scrollbars=auto");
            }
    // ]]>
    </script>
   
    <div class="content">
        <div class="title">Member Account Number</div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
        <table>
            <tr>
                <td style="width:128px" nowrap="nowrap">Bank</td>
                <td> <dx:ASPxComboBox ID="cmbBank" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbBank">
                       <DropDownButton Visible="false" />
                        
                </dx:ASPxComboBox></td>
                <td><dx:ASPxButton ID="btnGo" runat="server" Text="Search" Width="65px" 
                        onclick="btnGo_Click"></dx:ASPxButton> </td>   
                <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" >
                        <ClientSideEvents Click="view"/>
                        </dx:ASPxButton>
                    </td>                     
            </tr>
            <tr>
                <td nowrap="nowrap">Member ID</td>
                <td><dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbMember">
                       <DropDownButton Visible="false" />
                        
                </dx:ASPxComboBox> </td>
            </tr>
            <tr>
                <td>SID</td>
                <td><dx:ASPxComboBox ID="cmbSID" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbSID">
                        <DropDownButton Visible="false" />
                </dx:ASPxComboBox> </td>
            </tr>
        </table>

            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />                        
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" 
                            onclick="btnEdit_Click" ClientInstanceName="btned" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" 
                            ClientInstanceName="btndel" onclick="btnDelete_Click"
                            ClientSideEvents-Click="ConfirmDelete" />
                    </td>
                </tr>
            </table>
            <br />   
            
        <dx:ASPxGridView ID="gvMember" runat="server" AutoGenerateColumns="False" CssClass="grid"
            ClientInstanceName="grid" ondatabinding="gvMember_DataBinding" 
            KeyFieldName="memberId" Settings-ShowVerticalScrollBar="True" 
            onhtmlrowprepared="gvMember_HtmlRowPrepared">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" FieldName="memberId"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="1" FieldName="memberInfo.memberName"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="2" FieldName="sid"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Trading ID" VisibleIndex="3" FieldName="tradingId"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Account ID" VisibleIndex="4" FieldName="accountId"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Role" VisibleIndex="5" FieldName="role" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Bank Name" VisibleIndex="6" FieldName="bank.bankName"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Security Deposit <br />Account No" 
                    VisibleIndex="7" FieldName="securityDepositAccountNo" Width="110px"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Collateral <br /> Account No" VisibleIndex="8" FieldName="collateralAccountNo"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Settlement <br /> Account No" VisibleIndex="9" FieldName="settlementAccountNo"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Operational<br /> Account No" VisibleIndex="10" FieldName="freeAccountNo"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Bank Code" VisibleIndex="11" FieldName="bank.bankCode" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="IDX" VisibleIndex="12" FieldName="idx" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="13" FieldName="status" Visible="true"></dx:GridViewDataTextColumn>
            </Columns>
            
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
            <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" RowClick="OnGridFocusedRowChanged"/>

<Settings ShowVerticalScrollBar="True"></Settings>

            <Styles>
                <AlternatingRow Enabled="True" />
                <Header CssClass="gridHeader" />
            </Styles>
            <SettingsPager PageSize="20">
            </SettingsPager>
        </dx:ASPxGridView>
        
        <dx:ASPxHiddenField ID="hdMember" runat="server" ClientInstanceName="hdmember"></dx:ASPxHiddenField>
    </div>
</asp:Content>