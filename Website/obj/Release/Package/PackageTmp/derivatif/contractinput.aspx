﻿<%@ Page Title="Contract List & Detail" Language="C#" AutoEventWireup="true" CodeBehind="contractinput.aspx.cs" Inherits="imq.kpei.derivatif.contractInput" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        // <![CDATA[
        function OnValidation(s, e) {
            var val = e.value;
            if (val == null)
                return;

            if (val.length < 1)
                e.isValid = false;
        }

        function OnNumberValidation(s, e) {
            var num = e.value;
            if (num == null || num == "")
                return;

            var digits = "0123456789.,";
            for (var i = 0; i < num.length; i++) {
                if (digits.indexOf(num.charAt(i)) == -1) {
                    e.isValid = false;
                    break;
                }
            }

        }
        //]]>
    </script>
            <div  class="content">   
                <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" Text="Contract List & Detail" /> </div>
                    <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" Font-Bold="true"/>
                    <div id="plApproval">
                        <table>
                            <tr>
                                <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                                        ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                            </tr>
                        </table>
                    </div>
                    <table id="tblContract">
                        <tr>
                            <td nowrap="nowrap">Contract Code<span style="color:Red">*</span></td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtCode" runat="server" MaxLength="20" Size="20" Width="170px" EnableClientSideAPI="true">
                                    <ValidationSettings ErrorText="Required">
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnValidation" />                            
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">Template Parameter<span style="color:Red">*</span></td>
                            <td style="width: 168px">
                                <dx:ASPxComboBox ID="cmbTempParamId" runat="server" EnableClientSideAPI="true" 
                                    ValueType="System.String" AutoPostBack = "true"
                                    onselectedindexchanged="cmbTempParamId_SelectedIndexChanged" ></dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">Underlying Code<span style="color:Red">*</span></td>
                            <td style="width: 168px">
                                <dx:ASPxComboBox ID="cmbUnderlying" runat="server" EnableClientSideAPI="true" 
                                    ValueType="System.String" 
                                    onselectedindexchanged="cmbUnderlying_SelectedIndexChanged"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td style="width: 168px">
                                <dx:ASPxMemo ID="memoDescription" runat="server" Height="71px" Width="170px" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">Series Status<span style="color:Red">*</span></td>
                            <td style="width: 168px">
                                <dx:ASPxComboBox ID="cmbSeriesStatus" runat="server" EnableClientSideAPI="true"
                                    onselectedindexchanged="cmbType_SelectedIndexChanged" AutoPostBack="true">
                                    <Items>
                                        <dx:ListEditItem Text=" " Value="" />
                                        <dx:ListEditItem Text="N - New" Value="N" />
                                        <dx:ListEditItem Text="A - Active" Value="A" />
                                    </Items>
                                    <ValidationSettings>
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>                                   
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Instrument Code<span style="color:Red">*</span>
                            </td>
                            <td >
                                <dx:ASPxTextBox ID="txtInstrumentCode" runat="server" MaxLength="12" Size="12">
                                    <ValidationSettings ErrorText="Required">
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">Type (RF/RO)<span style="color:Red">*</span></td>
                            <td style="width: 168px">
                                <dx:ASPxComboBox ID="cmbType" runat="server" EnableClientSideAPI="true"
                                    onselectedindexchanged="cmbType_SelectedIndexChanged" AutoPostBack="true">
                                    <Items>
                                        <dx:ListEditItem Text="RF - Contract Future" Value="RF" />
                                        <dx:ListEditItem Text="RO - Contract Options" Value="RO" Selected="true" />
                                    </Items>
                                    <ValidationSettings>
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnValidation" />  
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">Strike Price<span style="color:Red">*</span></td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtStrike" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>" HorizontalAlign="Right">
                                    <ValidationSettings ErrorText="Required">
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnNumberValidation" />                    
                                    </dx:ASPxTextBox></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">Opening Price<span style="color:Red">*</span></td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtOpenPrice" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>.<00..99>" HorizontalAlign="Right">

                                    <ValidationSettings ErrorText="Required">
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnNumberValidation" />                    
                                    </dx:ASPxTextBox></td>
                        </tr>
                        <tr>
                            <td>Option Type<span style="color:Red">*</span></td>
                            <td style="width: 168px">
                                <dx:ASPxComboBox ID="cmbOptions" runat="server" AutoPostBack="true">
                                    <Items>
                                        <dx:ListEditItem Text="PUT" Value="PUT" />
                                        <dx:ListEditItem Text="CALL" Value="CALL" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Settlement Method<span style="color:Red">*</span></td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtSettlementMethod" runat="server" Width="170px" 
                                    ReadOnly="True" ClientInstanceName="txtsettlement" EnableClientSideAPI="true">
                                    <ValidationSettings>
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnValidation" />
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Day Transaction<span style="color:Red">*</span></td>
                            <td style="width: 168px"><dx:ASPxDateEdit ID="deLastDay" runat="server" EditFormat="Custom" 
                                 EditFormatString="dd/MM/yyyy" EnableClientSideAPI="true">
                                    <ValidationSettings>
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnValidation" />
                                </dx:ASPxDateEdit></td>
                        </tr>                        
                        <tr>
                            <td>Start Date<span style="color:Red">*</span></td>
                            <td style="width: 168px">
                                <dx:ASPxDateEdit ID="deStartDate" runat="server" EditFormat="Custom" 
                                EditFormatString="dd/MM/yyyy" EnableClientSideAPI="true" 
                                    ondatechanged="deStartDate_DateChanged" AutoPostBack="true">
                                    <ValidationSettings>
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnValidation" />
                                </dx:ASPxDateEdit>                    
                            </td>
                        </tr>
                        <tr>
                            <td>End Date<span style="color:Red">*</span></td>
                            <td style="width: 168px">
                                <dx:ASPxDateEdit ID="deEndDate" runat="server" EditFormat="Custom" 
                                EditFormatString="dd/MM/yyyy" EnableClientSideAPI="true" 
                                    ondatechanged="deStartDate_DateChanged" AutoPostBack="true">
                                    <ValidationSettings>
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnValidation" />
                                </dx:ASPxDateEdit>                    
                            </td>
                        </tr>
                        <tr>
                            <td>Contract Period</td>
                            <td style="width: 168px">
                                <dx:ASPxTextBox ID="txtContractPeriod" runat="server" Width="170px" 
                                    ReadOnly="True" EnableClientSideAPI="true">
                                    <ValidationSettings>
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnValidation" />
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Multiplier<span style="color:Red">*</span>
                            </td>
                             <td style="width: 168px"><dx:ASPxTextBox ID="txtMultiplier" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>" HorizontalAlign="Right">
                                    <ValidationSettings ErrorText="Required">
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnNumberValidation" />                    
                                    </dx:ASPxTextBox></td>
                          
                        </tr>
                        <tr>
                            <td>Exercise Time</td>
                            <td style="width: 168px;">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td><dx:ASPxTimeEdit ID="teExerciseTimeFrom" runat="server" Width="72px" 
                                            EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true" 
                                                Height="19px" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic">
                                                    <RequiredField IsRequired="true" ErrorText="Exercise Time is empty" />
                                                </ValidationSettings>
                                                <ClientSideEvents Validation="OnValidation" />
                                            </dx:ASPxTimeEdit>
                                            </td>
                                       <td>&nbsp;</td><td>To</td><td>&nbsp;</td>
                                        <td><dx:ASPxTimeEdit ID="teExerciseTimeTo" runat="server" Width="72px" 
                                            EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true" 
                                                Height="19px" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic">
                                                    <RequiredField IsRequired="true" ErrorText="Exercise Time is empty" />
                                                </ValidationSettings>
                                                <ClientSideEvents Validation="OnValidation" />
                                            </dx:ASPxTimeEdit>
                                        </td>
                                    </tr>
                                </table>        
                            </td>
                           
                        </tr>
                        <tr>
                            <td>
                                Exercise Style
                            </td>
                            <td style="width: 168px">
                                <dx:ASPxComboBox ID="cmbExerciseStyle" runat="server" 
                                    ClientInstanceName="cmbexercisestyle" Width="170px" ValueType="System.Int32">
                                <Items>
                                    <dx:ListEditItem Text="European Style" Value="0" />
                                    <dx:ListEditItem Text="American Style" Value="1" Selected="true" />
                                </Items>
                            
                                <ValidationSettings ErrorText="Exercise style must be greater than zero">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />                    
                               </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Margining Type
                            </td>
                           
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtMarginingType" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>" HorizontalAlign="Right">                                    
                                <ClientSideEvents Validation="OnNumberValidation" />                    
                            </dx:ASPxTextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Remark
                            </td>
                            <td >
                                <dx:ASPxTextBox ID="txtRemark" runat="server" MaxLength="8">
                                                                        
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Remark 2
                            </td>
                            <td style="width: 168px">
                                <dx:ASPxTextBox ID="txtRemark2" runat="server" Width="170px"  MaxLength="40" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                GF Fee
                            </td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtGfFee" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">                                    
                                <ClientSideEvents Validation="OnNumberValidation" />                    
                            </dx:ASPxTextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Minimum Fee
                            </td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtMinFee" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">                                    
                                <ClientSideEvents Validation="OnNumberValidation" />                    
                            </dx:ASPxTextBox></td>
                        </tr>
                         <tr>
                            <td>
                                Clearing Fee Type
                            </td>
                            <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbClearingFeeType" runat="server" 
                                EnableClientSideAPI="True" SelectedIndex="0">
                                <Items>
                                    <dx:listedititem Selected="True" Text="Fixed" Value="F" />
                                    <dx:listedititem Text="Percentage" Value="P" />
                                </Items>
                            </dx:ASPxComboBox>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                Clearing Fee Calculation
                            </td>
                            <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbClearingFeeCalculation" runat="server" 
                                EnableClientSideAPI="True" SelectedIndex="0">
                                <Items>
                                    <dx:listedititem Selected="True" Text="Per Contract" Value="K" />
                                    <dx:listedititem Text="Per Frequency" Value="F" />
                                </Items>
                            </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Clearing Fee Value
                            </td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtClearingFeeValue" runat="server" Width="170px" EnableClientSideAPI="true"
                               
                                    MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">                                    
                                <ClientSideEvents Validation="OnNumberValidation" />                    

<MaskSettings Mask="&lt;0..999999999999999g&gt;.&lt;0000000..9999999&gt;" IncludeLiterals="DecimalSymbol"></MaskSettings>
                            </dx:ASPxTextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Tick Size
                            </td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtTickSize" runat="server" Width="170px" EnableClientSideAPI="true"
                               
                                    MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">                                    
                                <ClientSideEvents Validation="OnNumberValidation" />                    

<MaskSettings Mask="&lt;0..999999999999999g&gt;.&lt;0000000..9999999&gt;" IncludeLiterals="DecimalSymbol"></MaskSettings>
                            </dx:ASPxTextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Tick Value
                            </td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtTickValue" runat="server" Width="170px" EnableClientSideAPI="true"
                               
                                    MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">                                    
                                <ClientSideEvents Validation="OnNumberValidation" />                    

<MaskSettings Mask="&lt;0..999999999999999g&gt;.&lt;0000000..9999999&gt;" IncludeLiterals="DecimalSymbol"></MaskSettings>
                            </dx:ASPxTextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Margin Type
                            </td>
                            <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbMarginType" runat="server" 
                                EnableClientSideAPI="True" SelectedIndex="0">
                                <Items>
                                    <dx:listedititem Selected="True" Text="Fix" Value="F" />
                                    <dx:listedititem Text="Floating" Value="L" />
                                </Items>
                            </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Margin Value
                            </td>
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtMarginValue" runat="server" Width="170px" EnableClientSideAPI="true"
                               
                                    MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>" HorizontalAlign="Right">                                    
                                <ClientSideEvents Validation="OnNumberValidation" />                    

<MaskSettings Mask="&lt;0..999999999999999g&gt;.&lt;0000000..9999999&gt;" IncludeLiterals="DecimalSymbol"></MaskSettings>
                            </dx:ASPxTextBox></td>
                        </tr>
                    </table>

                     <div class="clear" />
                    <br />
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnSave" runat="server" Text="Save" 
                                    onclick="btnSave_Click" Width="70px"/>                        
                            </td>
                            <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                                onclick="btnChecker_Click" Width="70px"/></td>
                            <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                                    onclick="btnApproval_Click" Width="70px"/></td>
                            <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                            <td>
                                <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                                    onclick="btnCancel_Click" CausesValidation="false" Width="80px"/>
                            </td>                        
                        </tr>
                    </table>
                    
                    <dx:ASPxHiddenField ID="hfContract" runat="server" ClientInstanceName="hfcontract" />
                    
                </div>
     
</asp:Content>
