﻿<%@ Page Title="Withdrawal" Language="C#" AutoEventWireup="true" CodeBehind="withdrawalform.aspx.cs" Inherits="imq.kpei.derivatif.withdrawalform" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        // <![CDATA[
        function OnValidation(s, e) {
            var val = e.value;
            if (val == null)
                return;

            if (val.length < 1)
                e.isValid = false;
        }

        function OnNumberValidation(s, e) {
            var num = e.value;
            if (num == null || num == "")
                return;

            var digits = "0123456789.,";
            for (var i = 0; i < num.length; i++) {
                if (digits.indexOf(num.charAt(i)) == -1) {
                    e.isValid = false;
                    break;
                }
            }
        }
        //]]>
    </script>
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" Text="Balance Withdrawal Form" CssClass="title"/></div>
        <dx:ASPxHiddenField ID="hfWithdrawal" runat="server" ClientInstanceName="hfWithdrawal" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="false" 
                            ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <table>
            <tr>
                <td style="width:128px">No. Refference</td>
                <td><dx:ASPxTextBox ID="txtReff" runat="server" Width="170px" MaxLength="50"/></td>  
            </tr>
            <tr>
                <td style="width:128px">Member ID</td>
                <td><dx:ASPxTextBox ID="txtMember" runat="server" Width="170px" ReadOnly="true"/></td>                        
            </tr>
            <tr>
                <td>SID<span style="color:Red">*</span></td>
                <td><dx:ASPxComboBox ID="cmbSID"  runat="server"  IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" AutoPostBack="true" 
                        EnableClientSideAPI="true" 
                        onselectedindexchanged="cmbSID_SelectedIndexChanged" >                        
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" /> 
                    </dx:ASPxComboBox>
                </td>                        
            </tr>
            <tr>
                <td>Account Source Name<span style="color:Red">*</span></td>
                <td>
                    <dx:ASPxComboBox ID="cmbAccountName" runat="server" Width="170px" AutoPostBack="true" Enabled="false"
                        onselectedindexchanged="cmbAccountName_SelectedIndexChanged" EnableClientSideAPI="true">
                        <Items>
                            <dx:ListEditItem Text="Collateral Account" Value="ca" Selected="true"/>
                            <dx:ListEditItem Text="Security Deposit Account" Value="sd" />                            
                        </Items>
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" /> 
                    </dx:ASPxComboBox>
                    </td>                        
            </tr>
            <tr>
                <td>Account Source No<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtAccNo" runat="server" Width="170px" ReadOnly="true">
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
            <tr>
                <td>Value<span style="color:Red">*</span></td>
                <td>
                    <dx:ASPxTextBox ID="txtValue" runat="server" Width="170px" EnableClientSideAPI="true" 
                    HorizontalAlign="Right" MaskSettings-Mask="<0..999999999999999999999999999g>">
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnNumberValidation" />
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
        </table>
            <br />
        <table>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnContinue" runat="server" Text="Continue" 
                        onclick="btnContinue_Click" />                        
                </td>
                <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                            onclick="btnChecker_Click" Width="70px"/></td>
                <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                        onclick="btnApproval_Click" Width="70px"/></td>
                <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                <td>
                    <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" CausesValidation="false"/>
                </td>                            
            </tr>
        </table>
        <br />  
    </div>
</asp:Content>
