﻿<%@ Page Title="Give Up" Language="C#" AutoEventWireup="true" CodeBehind="giveupform1.aspx.cs" Inherits="imq.kpei.derivatif.giveupform1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    
<asp:Content ID="content1" ContentPlaceHolderID="main" runat="server">        
    
    <div class="content">
        <div class="title"><h3>Give Up Form</h3></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <table>
            <tr>
                <td style="width:128px" nowrap="nowrap">Give Up Member ID</td>
                <td>
                    <dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" ReadOnly="true" >
                       
                        <DropDownButton Visible="true"></DropDownButton>
                    </dx:ASPxComboBox> 
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td><dx:ASPxButton ID="btnContinue" runat="server" Text="Continue" 
                        onclick="btnContinue_Click" /></td>
                <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" /></td>
            </tr>
        </table>
    </div>
       
</asp:Content>
