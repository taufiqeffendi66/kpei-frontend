﻿<%@ Page Title="Give Up Confirmation" Language="C#" AutoEventWireup="true" CodeBehind="giveupform4.aspx.cs" Inherits="imq.kpei.derivatif.giveupform4" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        //<![CDATA[
            function OnGridFocusedRowChanged() {
                grid.GetRowValues(grid.GetFocusedRowIndex(), "tradingId;", OnGetRowValues);
            }

            function OnGetRowValues(values) {
                hfGu.Set("tradingId", values[0]);

            }
        //]]>
    </script>
    <div class="content">
        <div class="title">Give Up Confirmation</div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" Font-Bold="true"/>
        <dx:ASPxGridView ID="gvGiveUp" runat="server" AutoGenerateColumns="false" ClientInstanceName="grid"  
            CssClass="grid" KeyFieldName="tradingId" ondatabinding="gvGiveUp_DataBinding">
            <Columns>
                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="0" FieldName="id.sid" />                
                <dx:GridViewDataTextColumn Caption="Trading ID" VisibleIndex="1" FieldName="tradingId"/>                        
                <dx:GridViewDataTextColumn Caption="Gu No" VisibleIndex="2" FieldName="id.guNo" Visible="false" />                
            </Columns>       
            <Templates>
                <DetailRow>
                    <dx:ASPxGridView ID="detailGrid" runat="server" KeyFieldName="sid" Width="100%"
                    ondatabinding="detailGrid_DataBinding" OnBeforePerformDataSelect="detailGrid_DataSelect" OnLoad="detailGrid_OnLoad">
                    <Columns>                        
                        <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="0" FieldName="sID" Visible="false"/>
                        <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="1" FieldName="memberID" Visible="false"/>
                        <dx:GridViewDataTextColumn Caption="Series Contract" VisibleIndex="2" FieldName="series"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Net Buy" VisibleIndex="3" FieldName="netB"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Net Sell" VisibleIndex="3" FieldName="netS"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
                </DetailRow>
            </Templates>        
            <SettingsDetail ShowDetailRow="true" />
            <SettingsBehavior AllowSelectByRowClick="true" AllowFocusedRow="true" AllowGroup="false"/>
            <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
        </dx:ASPxGridView>
               
        <br />
        <table>
            <tr>
                <td><dx:ASPxButton ID="btnFinish" runat="server" Text="Accept" 
                        onclick="btnFinish_Click" /></td>                   
                        
                <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" /></td>
            </tr>
        </table>        
    </div>
      <dx:ASPxHiddenField ID="hdGiveUp" runat="server" ClientInstanceName="hfGu"></dx:ASPxHiddenField>
</asp:Content>
