﻿<%@ Page Title="Member Edit" Language="C#"  AutoEventWireup="true" CodeBehind="MemberEdit.aspx.cs" Inherits="imq.kpei.derivatif.memberedit" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        // <![CDATA[
        function OnValidation(s, e) {
            var val = e.value;
            if (val == null)
                return;
            if (val.length < 1)
                e.isValid = false;
        }
        //]]>

    </script>
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" Text="Member Edit" /></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                            ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <table id="tblMember">
                <tr>
                    <td style="width:128px">Member ID <span style="color:Red">*</span></td>
                    <td><dx:ASPxTextBox ID="txtMemberID" runat="server" Width="170px" MaxLength="5"
                        EnableClientSideAPI="true" Size="5">
                            <ValidationSettings SetFocusOnError="true" ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox></td>                    
                </tr>
                <tr>
                    <td>Name<span style="color:Red">*</span></td>
                    <td><dx:ASPxTextBox ID="txtName" runat="server" Width="170px" MaxLength="50"
                        EnableClientSideAPI="true">
                            <ValidationSettings SetFocusOnError="true" ErrorText="Requires">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>NPWP<span style="color:Red">*</span></td>
                    <td><dx:ASPxTextBox ID="txtNPWP" runat="server" Width="170px" MaxLength="20"
                         EnableClientSideAPI="true">
                            <ValidationSettings SetFocusOnError="true" ErrorText="Requires">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><dx:ASPxTextBox ID="txtAddress" runat="server" Width="170px" /></td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td><dx:ASPxTextBox ID="txtPhone" runat="server"  Width="170px" /></td>
                </tr>
                <tr>
                    <td>
                        Fax
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtFax" runat="server" Width="170px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Contact Person
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtContact" runat="server" Width="170px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        E-Mail
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtemail" runat="server" Width="170px" EnableClientSideAPI="true">
                        <ValidationSettings SetFocusOnError="True">
                            <RegularExpression ErrorText="Invalid e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                            
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Admin User<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxTextBox ID="txtAdminUser" runat="server" Width="170px" EnableClientSideAPI="true">
                            <ValidationSettings SetFocusOnError="true" ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        No. SKT<span style="color:Red">*</span>
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtNoSKT" runat="server" Width="170px" EnableClientSideAPI="true">
                            <ValidationSettings SetFocusOnError="true" ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        SKT Date<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxDateEdit ID="deSKTDate" runat="server" EditFormat="Custom" 
                                EditFormatString="dd/MM/yyyy">
                            
                         <ValidationSettings SetFocusOnError="true" ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxDateEdit>                    
                    </td>
                </tr>
                <tr>
                    <td>Active Date<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxDateEdit ID="deActiveDate" runat="server" EditFormat="Custom" 
                                EditFormatString="dd/MM/yyyy"> 
                             <ValidationSettings SetFocusOnError="true" ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxDateEdit>                   
                    </td>
                </tr>
                <tr>
                    <td>Member Type<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxComboBox ID="cmbType" runat="server" Width="170px" DropDownRows="4"
                         EnableClientSideAPI="true">
                            <Items>
                                <dx:ListEditItem Text="Client" Value="C" />
                                <dx:ListEditItem Text="Trading Member" Value="T" />
                                <dx:ListEditItem Text="GCM" Value="G" />
                                <dx:ListEditItem Text="ICM" Value="I" />
                            </Items>
                            <ValidationSettings SetFocusOnError="true" ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxComboBox>                                
                    </td>                           
                            
                </tr>
                <tr>
                    <td>Status<span style="color:Red">*</span></td>
                    <td>
                        <dx:ASPxComboBox ID="cmbAsStatus" runat="server" Width="170px" DropDownRows="3"
                        EnableClientSideAPI="true">
                            <Items>
                                <dx:ListEditItem Text="Active" Value="a" />
                                <dx:ListEditItem Text="Suspend" Value="s" />
                                <dx:ListEditItem Text="Cabut SPAB" Value="c" />
                            </Items>
                            <ValidationSettings SetFocusOnError="true" ErrorText="Required">
                                <RequiredField IsRequired="true" ErrorText="Required" />
                            </ValidationSettings> 
                            <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top">Clearing Method</td>
                    <td>
                        <dx:ASPxRadioButtonList ID="rbClearing" runat="server" ValueType="System.Int32" 
                            onselectedindexchanged="rbClearing_SelectedIndexChanged" 
                            AutoPostBack="True" onvaluechanged="rbClearing_ValueChanged">
                            <Border BorderWidth="0px" />
                            <Items>
                                <dx:ListEditItem Text="Nett" Value="0" Selected="true" />
                                <dx:ListEditItem Text="Gross" Value="1" />
                                <dx:ListEditItem Text="Nett for specific contract" Value="2" />
                                <dx:ListEditItem Text="Gross for specific contract" Value="3" />
                            </Items>
                        </dx:ASPxRadioButtonList>                    
                    </td>
                </tr>
                <tr>
                    <td>Specific Contract Group</td>
                    <td><dx:ASPxComboBox ID="cmbSpecific" runat="server" Width="170px" DropDownRows="5" 
                            IncrementalFilteringMode="Contains" AutoPostBack="true" ValueType="System.Int32">
                            
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" /> 
                            </dx:ASPxComboBox></td>
                </tr>     
                <tr>
                    <td valign="top">Description</td>
                    <td><dx:ASPxMemo ID="meDescription" runat="server" Width="170px" Height="50px" ClientInstanceName="medesc">
                    </dx:ASPxMemo>
                    </td>
                </tr>
            </table>
                <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnSave" runat="server" Text="Save" 
                            onclick="btnSave_Click" />                        
                    </td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                            onclick="btnChecker_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                            onclick="btnApproval_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                    <td>
                        <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                            CausesValidation="false" onclick="btnCancel_Click" Width="70px"/>
                    </td>
                           
                </tr>
            </table>
    </div>
    <dx:ASPxHiddenField ID="hfMember" runat="server" />
</asp:Content>
