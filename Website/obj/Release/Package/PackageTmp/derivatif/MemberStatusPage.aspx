﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="MemberStatusPage.aspx.cs" Inherits="imq.kpei.derivatif.MemberStatusPage" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
    // <![CDATA[
    function OnGridFocusedRowChanged() {
        grid.GetRowValues(grid.GetFocusedRowIndex(), 'statusNo;memberId;statusCurrent;statusCurrentName;positionLimit;', OnGetRowValues);
    }
    function OnGetRowValues(values) {
        hfStatus.Set("statusNo", values[0]);
        hfStatus.Set("memberId", values[1]);
        hfStatus.Set("statusCurrent", values[2]);
        hfStatus.Set("statusCurrentName", values[3]);
        hfStatus.Set("positionLimit", values[4]);
    }

    function view(s, e) {
        window.open("MemberStatusView.aspx?member=" + cmbMember.GetText(), "", "fullscreen=yes;scrollbars=auto");
    }
    //]]>
</script>

<div class="content">
    <div class="title"><h3>Status Member</h3></div>
    <dx:ASPxHiddenField ID="hfStatus" runat="server" ClientInstanceName="hfStatus"></dx:ASPxHiddenField>
    <table>
        <tr>
            <td nowrap="nowrap">Member ID</td>
            <td><dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbMember">
                        <DropDownButton Visible="false" />
                </dx:ASPxComboBox></td>
                <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" Width="65px" 
                        onclick="btnSearch_Click"></dx:ASPxButton></td>
               <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                        <ClientSideEvents Click="view"/>
                        </dx:ASPxButton>
                    </td> 
        </tr>
    </table>
    
    <table align="right" >   
    <tr>    
        <td><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" 
        Width="65px" onclick="btnEdit_Click"></dx:ASPxButton></td>
        </tr>
    </table>
    <br />
    <p></p>
    <dx:ASPxGridView ID="gvStatus" runat="server" AutoGenerateColumns="False" CssClass="grid"
        ClientInstanceName="grid" KeyFieldName="statusNo" 
        ondatabinding="gvStatus_DataBinding">
        <Columns>
            <dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" UnboundType="Integer" Visible="false"/>
            <dx:GridViewDataDateColumn Caption="Create Date" FieldName="statusDate" VisibleIndex="1" >
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTimeEditColumn Caption="Time" FieldName="statusDate" 
                VisibleIndex="2" >
<PropertiesTimeEdit DisplayFormatString="HH:mm"></PropertiesTimeEdit>
            </dx:GridViewDataTimeEditColumn>
            <dx:GridViewDataTextColumn Caption="Member ID" FieldName="memberId" VisibleIndex="3" />
            <dx:GridViewBandColumn Caption="Status" VisibleIndex="4">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Before" FieldName="statusBeforeName" VisibleIndex="0" />
                    <dx:GridViewDataTextColumn Caption="Current" FieldName="statusCurrentName" VisibleIndex="1" />
                    
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="Status Current" FieldName="statusCurrent" VisibleIndex="5" Visible="false"/>
            <dx:GridViewDataTextColumn Caption="Status Before" FieldName="statusBefore" VisibleIndex="6" Visible="false" />
            <dx:GridViewDataTextColumn Caption="Position Limit" FieldName="positionLimitDesc" VisibleIndex="7" />
            <dx:GridViewDataTextColumn Caption="Position Limit" FieldName="positionLimit" VisibleIndex="8" Visible="false" />
        </Columns> 
        <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
        <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />       
        <SettingsPager PageSize="20">
        </SettingsPager>
        <Styles>
                <AlternatingRow Enabled="True" />
                <Header CssClass="gridHeader" />
            </Styles>
        <Settings ShowVerticalScrollBar="True" />
    </dx:ASPxGridView> 
</div>
</asp:Content>
