﻿<%@ Page Title="Give Up Form" Language="C#"  AutoEventWireup="true" CodeBehind="giveupform3.aspx.cs" Inherits="imq.kpei.derivatif.giveupform3" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        // <![CDATA[
            var _selectNumber = 0;
           
            var _handle = true;

            function OnGridFocusedRowChanged() {
                grid.GetRowValues(grid.GetFocusedRowIndex(), "id.tradingId;sid;", OnGetRowValues);
            }

            function OnGetRowValues(values) {
                hfGu.Set("tradingId", values[0]);
                hfGu.Set("sid", values[1]);
            }

            function OnPageCheckedChanged(s, e) {
                _handle = false;
                if (s.GetChecked())
                    grid.SelectAllRowsOnPage();
                else
                    grid.UnselectAllRowsOnPage();
            }

            function OnGridSelectionChanged(s, e) {
                //cbAll.SetChecked(s.GetSelectedRowCount() == s.cpVisibleRowCount);
                
                if (e.isChangedOnServer == false) {
                    if (e.isAllRecordsOnPage && e.isSelected)
                        _selectNumber = s.GetVisibleRowsOnPage();
                    else if (e.isAllRecordsOnPage && !e.isSelected)
                        _selectNumber = 0;
                    else if (!e.isAllRecordsOnPage && e.isSelected)
                        _selectNumber++;
                    else if (!e.isAllRecordsOnPage && !e.isSelected)
                        _selectNumber--;

                    if (_handle) {
                        cbPage.SetChecked(_selectNumber == s.GetVisibleRowsOnPage());
                        _handle = false;
                    }
                    _handle = true;
                }
                //else {
                //    cbPage.SetChecked(cbAll.GetChecked());
                //}

                if (_selectNumber > 0) {
                    btnNext.SetEnabled(true);
                    btnAdd.SetEnabled(true);
                } else {
                    btnNext.SetEnabled(false);
                    btnAdd.SetEnabled(false);
                }
                //alert(_selectNumber);
            }

            function OnGridEndCallback(s, e) {
                _selectNumber = s.cpSelectedRowsOnPage;
            }
            
       //]]>
    </script>
   <dx:ASPxHiddenField ID="hdGiveUp" runat="server" ClientInstanceName="hfGu"></dx:ASPxHiddenField>
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" Text="Give Up Form" /></div>
        <br />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                            ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" Font-Bold="true"/>
        <table>
            <tr>
                <td style="width:128px">Give Up Member ID</td>
                <td><dx:ASPxTextBox ID="txtGuMemberId" runat="server" Width="170px" ReadOnly="true"/> </td>
            </tr>
            <tr>
                <td style="width:128px">Take Up Member ID</td>
                <td><dx:ASPxTextBox ID="txtTuMemberId" runat="server" Width="170px" ReadOnly="true"/> </td>
            </tr>            
        </table>
        <dx:ASPxGridView ID="gvGiveUp" runat="server" AutoGenerateColumns="False" KeyFieldName="sID"
            CssClass="grid" ondatabinding="gvGiveUp_DataBinding" ClientInstanceName="grid"            
            OnCustomJSProperties="gvGiveUp_CustomJSProperties"  >
            <Columns>
                <dx:GridViewCommandColumn Caption="Select/<br />Unselect" ShowSelectCheckbox="true" VisibleIndex="0">
                    <HeaderTemplate>
                        <dx:ASPxCheckBox ID="cbPage" runat="server" ClientInstanceName="cbPage" ToolTip="Select All Rows"
                            OnInit="cbPage_Init">
                            <ClientSideEvents CheckedChanged="OnPageCheckedChanged" />    
                        </dx:ASPxCheckBox>
                                            
                    </HeaderTemplate>                    
                </dx:GridViewCommandColumn>
                
                <dx:GridViewDataTextColumn Caption="Give Up<br />SID" VisibleIndex="1" FieldName="sID" />
                <dx:GridViewDataTextColumn Caption="trading ID" VisibleIndex="2" FieldName="clientID"/>
                
            </Columns>
            <Templates>
                <DetailRow>
                    <dx:ASPxGridView ID="detailGrid" runat="server" KeyFieldName="sid" Width="100%"
                    ondatabinding="detailGrid_DataBinding" OnBeforePerformDataSelect="detailGrid_DataSelect" OnLoad="detailGrid_OnLoad">
                   <Columns>                        
                        <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="0" FieldName="sID" Visible="false"/>
                        <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="1" FieldName="memberID" Visible="false"/>
                        <dx:GridViewDataTextColumn Caption="Series Contract" VisibleIndex="2" FieldName="series"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Net Buy" VisibleIndex="3" FieldName="netB"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Net Sell" VisibleIndex="3" FieldName="netS"></dx:GridViewDataTextColumn>
                   </Columns>
                </dx:ASPxGridView>
                </DetailRow>
            </Templates>
            <SettingsDetail ShowDetailRow="true" />
            <SettingsBehavior AllowSelectByRowClick="true" AllowFocusedRow="true" AllowGroup="false"/>
            <ClientSideEvents SelectionChanged="OnGridSelectionChanged" EndCallback="OnGridEndCallback" FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }"/>
        </dx:ASPxGridView>
        <br />
        <br />
        
        <table>
            <tr>
                <td><dx:ASPxButton ID="btnPrev" runat="server" Text="Previous" 
                        onclick="btnPrev_Click" /></td>
                <td><dx:ASPxButton ID="btnNext" runat="server" Text="Next" ClientEnabled="false"
                        onclick="btnNext_Click" ClientInstanceName="btnNext"/></td>
                <td><dx:ASPxButton ID="btnAdd" runat="server" Text="Add More" ClientEnabled="false"
                        onclick="btnAdd_Click" ClientInstanceName="btnAdd"/></td>
                <td></td>
                <td></td>
                <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                                onclick="btnChecker_Click" Width="70px"/></td>
                <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                        onclick="btnApproval_Click" Width="70px"/></td>
                <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                onclick="btnReject_Click" Width="70px"/></td>
                <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
