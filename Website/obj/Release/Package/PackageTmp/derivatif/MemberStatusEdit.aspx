﻿<%@ Page Title="Member Status Form" Language="C#" AutoEventWireup="true" CodeBehind="MemberStatusEdit.aspx.cs" Inherits="imq.kpei.derivatif.MemberStatusEdit" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
   
        <div class="content">
        
        
            <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" CssClass="title" Text="Form Status Member" /></div>
          
            <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
            <div id="plApproval">
                <table>
                    <tr>
                        <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                                ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                    </tr>
                </table>
            </div>
            <table>
                <tr>
                    <td nowrap="nowrap"  >Member ID</td>
                    <td><dx:ASPxTextBox ID="txtMember" runat="server" Enabled="false" /> </td>
                    
                </tr>
                <tr>
                    <td nowrap="nowrap">Current Status</td>
                    <td><dx:ASPxTextBox ID="txtCurrent" runat="server" ReadOnly="true" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap">New Status</td>
                    <td><dx:ASPxComboBox ID="cmbStatus" runat="server" SelectedIndex="0">
                            
                        <Items>
                            <dx:ListEditItem Selected="True" Text="Active" Value="A" />
                            <dx:ListEditItem Text="Pre-Default" Value="P" />
                            <dx:ListEditItem Text="Default" Value="D" />
                        </Items>
                            
                    </dx:ASPxComboBox></td>
                </tr>
                <tr>
                    <td nowrap="nowrap">Position Limit</td>
                    <td><dx:ASPxComboBox ID="cmbPL" runat="server" SelectedIndex="0" ValueType="System.String" >
                            
                        <Items>                           
                            <dx:ListEditItem Selected="true" Text="Yes" Value="Y" />
                            <dx:ListEditItem Text="No" Value="N" />
                        </Items>                            
                    </dx:ASPxComboBox></td>
                </tr>
                </table>

               <br />
               <dx:ASPxLabel ID="lblConfirm" runat="server" ></dx:ASPxLabel>
               <br />
               <table>
                <tr>
                    <td> <dx:ASPxButton ID="btnContinue" runat="server" Text="Continue" AutoPostBack="false"
                            onclick="btnContinue_Click" Width="70px" /> </td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                            onclick="btnChecker_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                            onclick="btnApproval_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                            onclick="btnCancel_Click" CausesValidation="true"/></td>
                </tr>
                </table>
                <dx:ASPxHiddenField ID="hfStatus" runat="server"></dx:ASPxHiddenField>
        </div>
</asp:Content>
