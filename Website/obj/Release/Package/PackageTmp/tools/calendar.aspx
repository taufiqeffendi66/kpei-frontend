﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="calendar.aspx.cs" Inherits="imq.kpei.administration.calendar"%>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
     <script language="javascript" type="text/javascript">
        // <![CDATA[

         function OnGridFocusedRowChanged() {
             grid.GetRowValues(grid.GetFocusedRowIndex(), 'date;description;', OnGetRowValues);
         }

         function OnGetRowValues(values) {
             hf.Set("date", values[0]);
             hf.Set("description", values[1]);
         }

         function ConfirmDelete(s, e) {
             e.processOnServer = confirm("Are you sure delete this Calendar \nDate = " + hf.Get("date") + ",\nDescription = " + hf.Get("description"));
         }

         function init() {
             btnRemove.SetEnabled(false);
             btnEdit.SetEnabled(false);
         }

         window.onload = init;
        // ]]>
    </script>       
    <div class="content">
        <div class="title">CALENDAR</div>
        <div>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                <tr>
<!--
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnPDF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to PDF"><img src="../Styles/pdf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnXLS" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to XLS"><img src="../Styles/excel.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnRTF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to RTF"><img src="../Styles/rtf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnCSV" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to CSV"><img src="../Styles/csv.png" /></asp:LinkButton></td>
-->
                    <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" AutoPostBack="false" onclick="btnCsvExport_Click"> </dx:ASPxButton></td>

                </tr>
            </table>
            <table>
                <tr>
                    <td></td>
                    <td><dx:ASPxButton ID="btnNew" runat="server" Text="New" AutoPostBack="false" onclick="btnNew_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit"  AutoPostBack="false" onclick="btnEdit_Click" > </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnRemove" runat="server" Text="Delete" onclick="btnRemove_Click" ClientSideEvents-Click="ConfirmDelete"></dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gridView" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" CssClass="grid">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="ID" FieldName="ID" Visible="False"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn VisibleIndex="2" Caption="Date" FieldName="date" Width="100px"><PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy"></PropertiesDateEdit></dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Description" FieldName="description"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowSelectByRowClick="true" AllowSelectSingleRowOnly="True" AllowFocusedRow="True" />
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="200" />
                <SettingsCustomizationWindow Height="100%" />
                <Styles>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
            </dx:ASPxGridView>
            <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf"></dx:ASPxHiddenField>
        </div>
    </div>


</asp:Content>
