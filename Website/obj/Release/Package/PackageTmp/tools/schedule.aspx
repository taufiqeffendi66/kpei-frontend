﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="schedule.aspx.cs" Inherits="imq.kpei.tools.schedule" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[

        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'time;module;remark;', OnGetRowValues);

        }

        function OnGetRowValues(values) {
            hf.Set("time", values[0]);
            hf.Set("module", values[1]);
            hf.Set("remark", values[2]);
        }

        function ConfirmDelete(s, e) {
            e.processOnServer = confirm("Are you sure delete this Schedule \Time = " + hf.Get("time") + ",\nModule = " + hf.Get("module") + ",\nRemark = " + hf.Get("remark"));
        }


        function init() {
            btnRemove.SetEnabled(false);
            btnEdit.SetEnabled(false);
        }

    window.onload = init;
        // ]]>
    </script> 
    <div class="content">
        <div class="title">SCHEDULE</div>
        <div>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                <tr>
<!--
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnPDF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to PDF"><img src="../Styles/pdf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnXLS" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to XLS"><img src="../Styles/excel.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnRTF" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to RTF"><img src="../Styles/rtf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnCSV" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to CSV"><img src="../Styles/csv.png" /></asp:LinkButton></td>
-->
                    <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" AutoPostBack="false" onclick="btnCsvExport_Click"> </dx:ASPxButton></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td></td>
                    <td><dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnRemove" runat="server" Text="Delete" onclick="btnRemove_Click" ClientSideEvents-Click="ConfirmDelete"> </dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gridView" runat="server" ClientInstanceName="grid" AutoGenerateColumns="False" CssClass="grid">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="ID" FieldName="ID" Visible="false"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Time" FieldName="time"><PropertiesTextEdit DisplayFormatString="HH:mm:ss"></PropertiesTextEdit></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Modul to Execute" FieldName="module"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Remark" FieldName="remark"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="200" />
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <Styles><AlternatingRow Enabled="True"></AlternatingRow></Styles>
            </dx:ASPxGridView>
             <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf"></dx:ASPxHiddenField>
        </div>
    </div>

</asp:Content>
