﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="calendaredit.aspx.cs" Inherits="imq.kpei.administration.calendaredit" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <div id="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="Calendar Edit" /></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Date"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxDateEdit ID="dtDate" runat="server" DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy"></dx:ASPxDateEdit></td>
                    <td><dx:ASPxLabel ID="lbDateError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Description"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxMemo ID="dtDescription" runat="server" Height="71px" Width="170px"></dx:ASPxMemo></td>
                    <td><dx:ASPxLabel ID="lbDescriptionError" runat="server" ForeColor="#ff0000" /></td>
                </tr>
            </table>    
        </div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxButton ID="btnOk" runat="server" Text="Ok" onclick="btnOK_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" onclick="btnChecker_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnApprovel" runat="server" Text="Approved" onclick="btnApprovel_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" onclick="btnReject_Click"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></dx:ASPxButton></td>
                </tr>
            </table> 
        </div>
    </div>
 

</asp:Content>
