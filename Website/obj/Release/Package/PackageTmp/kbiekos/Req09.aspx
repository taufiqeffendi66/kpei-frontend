﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true"
    CodeBehind="Req09.aspx.cs" Inherits="imq.kpei.kbiekos.Req09" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div id="content">
        <div class="title">
            Log Likuidasi</div>
        <%-- <table>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnExport" runat="server" Text="Export" AutoPostBack="false" OnClick="btnCsvExport_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>--%>
        <table>
            <tr>
                <td>
                    Tanggal Data
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dateEdit" runat="server" EditFormat="Custom" EditFormatString="yyyy-MM-dd"
                        Width="200" OnDateChanged="dateEdit_DateChanged" AllowNull="False" 
                        AllowUserInput="False">
                        <TimeSectionProperties>
                            <TimeEditProperties EditFormatString="hh:mm tt" />
                        </TimeSectionProperties>
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView">
        </dx:ASPxGridViewExporter>
        <dx:ASPxTimer ID="ASPxTimer2" runat="server" Interval="60000" OnTick="ASPxTimer1_Tick">
        </dx:ASPxTimer>
        <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
            SettingsPager-PageSize="20" CssClass="grid" KeyFieldName="id" 
            OnDataBinding="gridView_DataBinding" 
            >
            <Columns>
                <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Broker Code" 
                    FieldName="memberId">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="2" Caption="SID" FieldName="sid">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Free Collateral" FieldName="freeCollateral">
                <PropertiesTextEdit DisplayFormatString="#,##0.00;(#,##0.00)">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Exposure" FieldName="exposure">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Data Timestamp" 
                    FieldName="strDataTimeLikuidasi" VisibleIndex="5">
                    <PropertiesDateEdit DisplayFormatString="dd-MM-yyyy HH:mm:ss">
                    </PropertiesDateEdit>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataDateColumn>
            </Columns>
            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
            <SettingsPager PageSize="20">
            </SettingsPager>
            <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" VerticalScrollableHeight="300" />
            <Styles>
                <AlternatingRow Enabled="True">
                </AlternatingRow>
            </Styles>
        </dx:ASPxGridView>
    </div>
</asp:Content>
