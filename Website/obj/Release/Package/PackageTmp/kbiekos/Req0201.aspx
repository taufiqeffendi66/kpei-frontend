﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true"
    CodeBehind="Req0201.aspx.cs" Inherits="imq.kpei.kbiekos.Req0201" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[
        function view(s, e) {
            window.open("reportView1.aspx" + "?hari=" + cmDate.GetText() + "&kbroker=" + cmBroker.GetValue() + "&settle=" + cmSettle.GetValue() + "&req=0201", "", "");
        }
        // ]]>
    </script>
    <div id="content">
        <div class="title">
            Laporan Status Settlement</div>
        <div style="float: left; margin-top: 5px">
            <table>
                <tr>
                    <td>
                        Settlement Status
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbStatus" runat="server" ValueType="System.String" DropDownStyle="DropDownList"
                            TextField="Status Intruksi" OnSelectedIndexChanged="cmbStatus_SelectedIndexChanged"
                            ClientInstanceName="cmSettle">
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <%--<dx:ASPxLabel ID="ASPxLabel1" ClientInstanceName="clID" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>--%>
                        <dx:ASPxLabel ID="Label1" runat="server" Text="ASPxLabel" Visible="false">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Broker Code
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbKbroker" runat="server" ValueType="System.String" DropDownStyle="DropDownList"
                            TextField="Kode Broker" OnSelectedIndexChanged="cmbKbroker_SelectedIndexChanged"
                            ClientInstanceName="cmBroker">
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <dx:ASPxLabel ID="dtID" ClientInstanceName="clID" runat="server" Text="" Visible="False">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Settlement Date
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEdit" runat="server" EditFormat="Custom" EditFormatString="yyyy-MM-dd"
                            Width="200" OnDateChanged="dateEdit_DateChanged" AllowNull="False" AllowUserInput="False"
                            ClientInstanceName="cmDate">
                            <TimeSectionProperties>
                                <TimeEditProperties EditFormatString="hh:mm tt" />
                            </TimeSectionProperties>
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right; margin-top: 50px">
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                            <ClientSideEvents Click="view" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 90px">
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView">
            </dx:ASPxGridViewExporter>
            <dx:ASPxTimer ID="ASPxTimer1" runat="server" Interval="60000" OnTick="ASPxTimer1_Tick">
            </dx:ASPxTimer>
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" KeyFieldName="codeOfBroker"
                AutoGenerateColumns="False" SettingsPager-PageSize="20" CssClass="grid" OnDataBinding="gridView_DataBinding">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Broker Code" FieldName="codeOfBroker" ShowInCustomizationForm="True"
                        VisibleIndex="0">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Instruction Type" FieldName="instructionType">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Settlement Date" FieldName="strDateStatus" VisibleIndex="2">
                        <PropertiesDateEdit DisplayFormatString="dd-MM-yyyy">
                        </PropertiesDateEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Debit Acc. Number" FieldName="debitAccNumber">
                        <PropertiesTextEdit DisplayFormatString="n0">
                        </PropertiesTextEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Credit Acc. Number" FieldName="creditAccNumber">
                        <PropertiesTextEdit DisplayFormatString="n0">
                        </PropertiesTextEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Settlement Status" FieldName="settlementStatus">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" VerticalScrollableHeight="300" />
                <Styles>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
