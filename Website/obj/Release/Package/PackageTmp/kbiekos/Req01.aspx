﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true"
    CodeBehind="Req01.aspx.cs" Inherits="imq.kpei.kbiekos.Req0101" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[
        function view(s, e) {
            window.open("reportView1.aspx" + "?hari=" + cmDate.GetText() + "&kbroker=" + cmBroker.GetValue() + "&req=01", "", "");
        }

        // ]]>
    </script>
    <div id="content">
        <div class="title">
            Laporan Status Pengiriman Balance Anggota Kliring Derivatif</div>
        <div style="float: left">
            <table>
                <tr>
                    <td>
                        Broker Code
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbKbroker" runat="server" ValueType="System.String" DropDownStyle="DropDownList"
                            TextField="Kode Broker" OnSelectedIndexChanged="cmbKbroker_SelectedIndexChanged"
                            ClientInstanceName="cmBroker">
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <dx:ASPxLabel ID="Label1" runat="server" Visible="False">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxLabel ID="dtID" ClientInstanceName="clID" runat="server" Text="" Visible="False">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Data Date
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEdit" runat="server" EditFormat="Custom" EditFormatString="yyyy-MM-dd"
                            Width="200" OnDateChanged="dateEdit_DateChanged" AllowUserInput="False" AllowNull="False"
                            ClientInstanceName="cmDate">
                            <TimeSectionProperties>
                                <TimeEditProperties EditFormatString="hh:mm tt" />
                            </TimeSectionProperties>
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click">
                        </dx:ASPxButton>
                    </td>
                    <td>
                        <dx:ASPxLabel ID="Label2" Width="300px" runat="server" Visible="False">
                        </dx:ASPxLabel>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right">
            <table style="margin-bottom: 12px">
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click">
                            <ClientSideEvents Click="view" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 60px">
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView">
                <PageFooter>
                    <Font Bold="True" Size="Larger"></Font>
                </PageFooter>
            </dx:ASPxGridViewExporter>
            <dx:ASPxTimer ID="ASPxTimer1" runat="server" OnTick="ASPxTimer1_Tick" Interval="60000">
            </dx:ASPxTimer>
            <dx:ASPxGridView ID="gridView" runat="server" KeyFieldName="id" AutoGenerateColumns="False"
                SettingsPager-PageSize="20" CssClass="grid" OnDataBinding="gridView_DataBinding">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Broker Code" FieldName="kodeBroker">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Bank" FieldName="namaBank">
                        <Settings AllowSort="True" />
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Account no" FieldName="nomerRekening">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Balance" FieldName="balance">
                        <PropertiesTextEdit DisplayFormatString="n0">
                        </PropertiesTextEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Data Timestamp" FieldName="strDataTimeStamp"
                        VisibleIndex="5">
                        <PropertiesDateEdit DisplayFormatString="dd-MM-yyyy HH:mm:ss">
                        </PropertiesDateEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="6" Caption="ID" FieldName="id" Visible="false">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" VerticalScrollableHeight="300" />
                <Styles>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
