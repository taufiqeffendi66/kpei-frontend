﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true"
    CodeBehind="Req07.aspx.cs" Inherits="imq.kpei.kbiekos.Req07" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[
        function view(s, e) {
            window.open("reportView1.aspx" + "?hari=" + cmDate.GetText() + "&kbroker=" + cmBroker.GetValue() + "&req=07", "", "");
        }

        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'id;newStr;sid;cmCode;accountId;currentId;status;action;', OnGetRowValues);

        }

        function OnGetRowValues(values) {
            hfAudit.Set("id", values[0]);
            hfAudit.Set("newStr", values[1]);
            hfAudit.Set("sid", values[2]);
            hfAudit.Set("cmCode", values[3]);
            hfAudit.Set("accountId", values[4]);
            hfAudit.Set("currentId", values[5]);
            hfAudit.Set("status", values[6]);
            hfAudit.Set("action", values[7]);

        }
    </script>
    <div id="content">
        <div class="title">
            Tool Validasi, Rekonsiliasi Data Account, SID Mapping Terbaru</div>
        <div style="float: left; margin-top: 5px">
            <table>
                <tr>
                    <td>
                        SID
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="tbSid" runat="server" Width="200px" OnTextChanged="tbSid_TextChanged" ClientInstanceName="cmBroker">
                        </dx:ASPxTextBox>
                    </td>
                    <td>
                        <%-- <dx:ASPxLabel ID="dtID" ClientInstanceName="clID" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>--%>
                        <dx:ASPxLabel ID="Label1" runat="server" Text="ASPxLabel" Visible="false">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tanggal Data
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEdit" runat="server" EditFormat="Custom" EditFormatString="yyyy-MM-dd"
                            Width="200" OnDateChanged="dateEdit_DateChanged" AllowUserInput="true" 
                            AllowNull="true" ClientInstanceName="cmDate">
                            <TimeSectionProperties>
                                <TimeEditProperties EditFormatString="hh:mm tt" />
                            </TimeSectionProperties>
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right; margin-top: 30px">
            <table>
                <tr>
                    <%--<td>
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="true" Width="75px" Font-Size="12px"
                            Visible="true" Text="Export to :    ">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left">
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnCsv_Click" class="login_button"
                            ToolTip="Export to CSV"><img src="../Styles/new/xls.png" width="40px" height="35px"/></asp:LinkButton>
                    </td>
                    <td align="left">
                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="btnPdf_Click" class="login_button"
                            ToolTip="Export to Pdf"><img src="../Styles/new/pdf.png" width="40px" height="35px"/></asp:LinkButton>
                    </td>--%>
                    <td>
                        <dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                            <ClientSideEvents Click="view" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top:60px">
        <table>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnApprove" runat="server" Text="Aprroval" OnClick="btnApprove_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView">
        </dx:ASPxGridViewExporter>
        <dx:ASPxTimer ID="ASPxTimer1" runat="server" OnTick="ASPxTimer1_Tick" Interval="60000">
        </dx:ASPxTimer>
        <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" KeyFieldName="id"
            AutoGenerateColumns="False" SettingsPager-PageSize="20" CssClass="grid" EnableRowsCache="false" OnDataBinding="gridView_DataBinding">
            <Columns>
                <dx:GridViewDataTextColumn VisibleIndex="1" Caption="ID" FieldName="id" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Waktu" FieldName="strDataTimeAudit" VisibleIndex="2">
                    <PropertiesDateEdit DisplayFormatString="dd-MM-yyyy HH:mm:ss">
                    </PropertiesDateEdit>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn VisibleIndex="3" Caption="SID" FieldName="sid">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="4" Caption="CM Code" FieldName="cmCode">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Account ID" FieldName="accountId">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Current ID" FieldName="currentId">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Status" FieldName="status">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="8" Caption="Action" FieldName="action">
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="true" AllowSelectByRowClick="True" />
            <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
            <SettingsPager PageSize="20">
            </SettingsPager>
            <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" VerticalScrollableHeight="300" />
            <Styles>
                <AlternatingRow Enabled="True">
                </AlternatingRow>
                <Header CssClass="gridHeader" />
            </Styles>
        </dx:ASPxGridView>
        </div>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    </div>
    <dx:ASPxHiddenField ID="hfAudit" runat="server" ClientInstanceName="hfAudit" />
</asp:Content>
