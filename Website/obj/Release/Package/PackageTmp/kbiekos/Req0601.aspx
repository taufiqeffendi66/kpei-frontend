﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true"
    CodeBehind="Req0601.aspx.cs" Inherits="imq.kpei.kbiekos.Req0601" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[
        function view(s, e) {
            window.open("Req0601View.aspx" + "?hari=" + cmDate.GetText() + "&title=" + cmNoSurat.GetText() + "&req=060101" + "&kbie1=" + lbl_jmlCol1.GetText() + "&kbie2=" + lbl_jmlPengaman.GetText(), "", "");
        }

        function view2(s, e) {
            window.open("Req0601View.aspx" + "?hari=" + cmDate.GetText() + "&title=" + cmNoSurat.GetText() + "&req=060102" + "&kbie3=" + lbl_jmlCol2.GetText() + "&kbie4=" + lbl_shmInduk.GetText(), "", "");
        }


        // ]]>
    </script>
    <div id="content">
        <div class="title"></div>
        <div style="float: left">
            <table>
                <tr>
                    <td>
                        Tanggal Data
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEdit" runat="server" EditFormat="Custom" EditFormatString="yyyy-MM-dd"
                            Width="200" OnDateChanged="dateEdit_DateChanged" AllowUserInput="False" ClientInstanceName="cmDate"
                            AllowNull="False">
                            <TimeSectionProperties>
                                <TimeEditProperties EditFormatString="hh:mm tt" />
                            </TimeSectionProperties>
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click">
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nomor Surat
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="tbNoSurat" runat="server" Width="200px" OnTextChanged="tbNoSurat_TextChanged"
                            ClientInstanceName="cmNoSurat">
                        </dx:ASPxTextBox>
                    </td>
                    <%--                <td>
                    <dx:ASPxLabel ID="dtID" ClientInstanceName="clID" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>
                </td>--%>
                </tr>
            </table>
        </div>
        <div style="float: right;margin-top: 50px">
            <table style="margin-bottom: 12px">
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click">
                            <ClientSideEvents Click="view" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 50px">
            <table style="margin-bottom: 5px">
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lbl_titleLabel1" runat="server" Font-Bold="true" Font-Size="12px"
                            Visible="true" Text="Rekapitulasi Perdangangan KBIE per Tanggal ">
                        </dx:ASPxLabel>
                        <dx:ASPxLabel ID="lbl_titleLabel2" runat="server" Font-Bold="true" Font-Size="12px"
                            Visible="true">
                        </dx:ASPxLabel>
                    </td>
                </tr>
            </table>
            <dx:ASPxGridViewExporter ID="GridExporter1" runat="server" GridViewID="gridView">
            </dx:ASPxGridViewExporter>
            <dx:ASPxTimer ID="ASPxTimer1" runat="server" OnTick="ASPxTimer1_Tick" Interval="60000">
            </dx:ASPxTimer>
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" KeyFieldName="id"
                AutoGenerateColumns="False" SettingsPager-PageSize="10" CssClass="grid" OnDataBinding="gridView_DataBinding">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Kontrak ID" FieldName="kontrakId">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Frekuensi" FieldName="frekuensi">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Kontrak Awal" VisibleIndex="2">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Short" FieldName="kontrakAwalShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Long" FieldName="kontrakAwalLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Kontrak Baru" VisibleIndex="5">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Short" FieldName="kontrakBaruShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Long" FieldName="kontrakBaruLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Tutup Kontrak" VisibleIndex="8">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="9" Caption="Short" FieldName="tutupKontrakShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="10" Caption="Long" FieldName="tutupKontrakLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Likuidasi" VisibleIndex="11">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="12" Caption="Short" FieldName="likuidasiShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="13" Caption="Long" FieldName="likuidasiLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Nett Open" VisibleIndex="14">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="15" Caption="Short" FieldName="netOpenShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="16" Caption="Long" FieldName="netOpenLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" VerticalScrollableHeight="300" />
                <Styles>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
            </dx:ASPxGridView>
            <table style="margin-bottom: 5px">
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lbl_Col1" runat="server" Font-Bold="true" Font-Size="12px" Visible="true"
                            Text="Jumlah AK Setor Collateral : ">
                        </dx:ASPxLabel>
                        <dx:ASPxLabel ID="lbl_jmlCol1" runat="server" Font-Bold="true" Font-Size="12px" Visible="true" 
                        ClientInstanceName="lbl_jmlCol1">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lbl_pengaman" runat="server" Font-Bold="true" Font-Size="12px"
                            Visible="true" Text="Jumlah AK Setor Dana Pengaman : ">
                        </dx:ASPxLabel>
                        <dx:ASPxLabel ID="lbl_jmlPengaman" runat="server" Font-Bold="true" Font-Size="12px"
                            Visible="true" ClientInstanceName="lbl_jmlPengaman">
                        </dx:ASPxLabel>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right;margin-top:15px">
            <table style="margin-bottom: 12px">
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnExport2" runat="server" Text="Export">
                            <ClientSideEvents Click="view2" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table style="margin-bottom: 5px; margin-top: 10px">
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lbl_titleLabel3" runat="server" Font-Bold="true" Font-Size="12px"
                            Visible="true" Text="Rekapitulasi Perdangangan KOS per Tanggal ">
                        </dx:ASPxLabel>
                        <dx:ASPxLabel ID="lbl_titleLabel4" runat="server" Font-Bold="true" Font-Size="12px"
                            Visible="true">
                        </dx:ASPxLabel>
                    </td>
                </tr>
            </table>
            <dx:ASPxGridViewExporter ID="GridExporter2" runat="server" GridViewID="gridView">
            </dx:ASPxGridViewExporter>
            <dx:ASPxGridView ID="gridView2" ClientInstanceName="grid" runat="server" KeyFieldName="id"
                AutoGenerateColumns="False" SettingsPager-PageSize="10" CssClass="grid" OnDataBinding="gridView2_DataBinding"
                >
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Series" FieldName="series">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Frekuensi" FieldName="frekuensi">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Kontrak Awal" VisibleIndex="2">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Short" FieldName="kontrakAwalShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Long" FieldName="kontrakAwalLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Kontrak Baru" VisibleIndex="5">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Short" FieldName="kontrakBaruShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Long" FieldName="kontrakBaruLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Tutup Kontrak" VisibleIndex="8">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="9" Caption="Short" FieldName="tutupKontrakShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="10" Caption="Long" FieldName="tutupKontrakLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Likuidasi" VisibleIndex="11">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="12" Caption="Short" FieldName="likuidasiShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="13" Caption="Long" FieldName="likuidasiLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Exercise/Assignment" VisibleIndex="14">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="15" Caption="Short" FieldName="netOpenShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="16" Caption="Long" FieldName="netOpenLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Nett Open" VisibleIndex="17">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="18" Caption="Short" FieldName="netOpenShort">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="19" Caption="Long" FieldName="netOpenLong">
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" VerticalScrollableHeight="300" />
                <Styles>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
            </dx:ASPxGridView>
            <table style="margin-bottom: 5px">
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lbl_Col2" runat="server" Font-Bold="true" Font-Size="12px" Visible="true"
                            Text="Jumlah AK Setor Collateral :">
                        </dx:ASPxLabel>
                        <dx:ASPxLabel ID="lbl_jmlCol2" runat="server" Font-Bold="true" Font-Size="12px" Visible="true"
                        ClientInstanceName="lbl_jmlCol2">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxLabel ID="lbl_shm" runat="server" Font-Bold="true" Font-Size="12px" Visible="true"
                            Text="Saham Induk : ">
                        </dx:ASPxLabel>
                        <dx:ASPxLabel ID="lbl_shmInduk" runat="server" Font-Bold="true" Font-Size="12px"
                            Visible="true" ClientInstanceName="lbl_shmInduk">
                        </dx:ASPxLabel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
