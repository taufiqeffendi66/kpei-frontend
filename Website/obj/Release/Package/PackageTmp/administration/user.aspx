﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="user.aspx.cs" Inherits="imq.kpei.administration.user" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
     <script language="javascript" type="text/javascript">
        // <![CDATA[

         function ConfirmDelete(s, e) {
             e.processOnServer = confirm("Are you sure Delete this user ?");
         }

         function ConfirmReset(s, e) {
             e.processOnServer = confirm("Are you sure Reset this user ?");
         }

         function ConfirmBlocked(s, e) {
             e.processOnServer = confirm("Are you sure Blocked this user ?");
         }

         function OnGridFocusedRowChanged() {

             grid.GetRowValues(grid.GetFocusedRowIndex(), 'userId;memberId;', OnGetRowValues);
         }

         function OnGetRowValues(values) {
             clUserId.SetText(values[0]);
             clMemberId.SetText(values[1]);
         }
        // ]]>
    </script> 
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="USER" /></div>
        <div>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                <tr>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnPDF" runat="server" OnClick="btnPdfExport_Click" class="login_button" ToolTip="Export to PDF"><img src="../Styles/pdf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnXLS" runat="server" OnClick="btnXlsExport_Click" class="login_button" ToolTip="Export to XLS"><img src="../Styles/excel.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnRTF" runat="server" OnClick="btnRtfExport_Click" class="login_button" ToolTip="Export to RTF"><img src="../Styles/rtf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnCSV" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to CSV"><img src="../Styles/csv.png" /></asp:LinkButton></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="User Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtUserId" ClientInstanceName="clUserId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td></td>
                </tr><tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Member Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtMemberId" ClientInstanceName="clMemberId" runat="server" Width="170px"> </dx:ASPxTextBox></td>
                    <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click"> </dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnBlocked" runat="server" Text="Blocked" ClientSideEvents-Click="ConfirmBlocked" onclick="btnBlocked_Click"> </dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnReset" runat="server" Text="Reset" onclick="btnReset_Click" ClientSideEvents-Click="ConfirmReset"> </dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnRemove" runat="server" Text="Delete" onclick="btnRemove_Click" ClientSideEvents-Click="ConfirmDelete"> </dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"  CssClass="grid">                
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="User ID" FieldName="userId"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Member ID" FieldName="memberId"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Status" FieldName="status"></dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowSelectByRowClick="True" AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />                
                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="200" />
                <Styles><AlternatingRow Enabled="True"></AlternatingRow></Styles>
            </dx:ASPxGridView>
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView"> </dx:ASPxGridViewExporter>
        </div>        
    </div>
    <dx:ASPxPopupControl ID="popUp" runat="server"></dx:ASPxPopupControl>

</asp:Content>
