﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userGroup.aspx.cs" Inherits="imq.kpei.administration.userGroup" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
     <script language="javascript" type="text/javascript">
        // <![CDATA[

         function ConfirmDelete(s, e) {
             e.processOnServer = confirm("Are you sure Remove this user from Group ?");
         }

         function ConfirmReset(s, e) {
             e.processOnServer = confirm("Are you sure Reset this user ?");
         }

         function OnGridFocusedRowChanged() {

             grid.GetRowValues(grid.GetFocusedRowIndex(), 'userId;memberId;', OnGetRowValues);
         }

         function OnGetRowValues(values) {
             clUserId.SetText(values[0]);
             clMemberId.SetText(values[1]);
         }
        // ]]>
    </script> 
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="USER GROUP" /></div>
        <div>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                <tr>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnPDF" runat="server" OnClick="btnPdfExport_Click" class="login_button" ToolTip="Export to PDF"><img src="../Styles/pdf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnXLS" runat="server" OnClick="btnXlsExport_Click" class="login_button" ToolTip="Export to XLS"><img src="../Styles/excel.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnRTF" runat="server" OnClick="btnRtfExport_Click" class="login_button" ToolTip="Export to RTF"><img src="../Styles/rtf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnCSV" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to CSV"><img src="../Styles/csv.png" /></asp:LinkButton></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Member Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtMemberId" ClientInstanceName="clMemberId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="User Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtUserId" ClientInstanceName="clUserId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Group Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtGroupId" ClientInstanceName="clGroupId" runat="server" Width="170px"> </dx:ASPxTextBox></td>
                    <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click"> </dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click"> </dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="btnRemove" runat="server" Text="Delete" onclick="btnRemove_Click" ClientSideEvents-Click="ConfirmDelete"> </dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" 
                AutoGenerateColumns="False"  CssClass="grid">                
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Member ID" FieldName="memberId" ReadOnly="True"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="User ID" FieldName="userId" ReadOnly="True"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Group ID" FieldName="groupId">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True"></SettingsBehavior>
                <SettingsPager PageSize="15"></SettingsPager>                
                <SettingsEditing Mode="Inline"></SettingsEditing>
                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="200" ShowGroupPanel="True" />
                <Styles><AlternatingRow Enabled="True"></AlternatingRow></Styles>
            </dx:ASPxGridView>
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView"> </dx:ASPxGridViewExporter>
        </div>        
    </div>
    </asp:Content>