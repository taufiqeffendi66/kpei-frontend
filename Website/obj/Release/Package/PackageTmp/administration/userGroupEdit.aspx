﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userGroupEdit.aspx.cs" Inherits="imq.kpei.administration.userGroupEdit" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="USER GROUP EDIT" /></div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Member Id"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxComboBox ID="dtMemberList" runat="server" DropDownStyle="DropDownList" ClientInstanceName="cbMember" EnableSynchronization="False"></dx:ASPxComboBox></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="User ID"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="dtUserId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Group Id"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td>
                        <dx:ASPxComboBox ID="dtGroupList" runat="server" DropDownStyle="DropDownList" ClientInstanceName="cbGroupId" EnableSynchronization="False">
                        </dx:ASPxComboBox>
                    </td>
                </tr>
            </table> 
        </div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxButton ID="btnOk" runat="server" Text="Ok" onclick="btnOk_Click"></dx:ASPxButton></td>
                    <td>&nbsp;</td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></dx:ASPxButton></td>
                </tr>
            </table> 
        </div>
    </div>


</asp:Content>
