﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="TemplateInput.aspx.cs" Inherits="imq.kpei.administration.TemplateInput" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div  class="content">
                <div class="title">
                    <dx:ASPxLabel ID="lblTitle" runat="server" Text="Multiplier Template"></dx:ASPxLabel>
                </div>
                <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" 
                    Font-Bold="true">
                 </dx:ASPxLabel>
                <div id="plApproval">
                    <table>
                        <tr>
                            <td>
                            
                                <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                                        ClientIDMode="AutoID">
                                </dx:ASPxRadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <table id="tblContract">
                    <tr>
                        <td nowrap="nowrap">
                            ID<span style="color:Red">*</span>
                        </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtId" runat="server" MaxLength="20" Size="20" 
                                Width="170px" EnableClientSideAPI="true">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Template Name<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtTmplName" runat="server" MaxLength="20" Size="20" 
                                Width="170px" EnableClientSideAPI="true">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Underlying<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                       
                            <dx:ASPxComboBox ID="cmbUnderlying" runat="server" EnableClientSideAPI="true" 
                                ValueType="System.String"></dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Tick Size<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtTickSize" runat="server" MaxLength="20" Size="20" 
                                Width="170px" EnableClientSideAPI="true" 
                                MaskSettings-Mask="<0..999999999999999g>.<00..99>">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Value<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtClearingFeeValue" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>.<00..99>" 
                                HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Type<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtClearingFeeType" runat="server" Width="170px" EnableClientSideAPI="true"
                                   
                                HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Calculation<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtClearingFeeCalculation" runat="server" Width="170px" EnableClientSideAPI="true"
                                    
                                HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            GF Fee<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtGfFee" runat="server" Width="170px" 
                                    ReadOnly="True" ClientInstanceName="txtGfFee" 
                                EnableClientSideAPI="true">
                                <ValidationSettings>
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="onNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            HPH<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtHph" runat="server" Width="170px" 
                                    ReadOnly="True" ClientInstanceName="txtHph" 
                                EnableClientSideAPI="true">
                                <ValidationSettings>
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Sampling Hph Type<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtSamplingHphType" runat="server" MaxLength="20" Size="20" 
                                Width="170px" EnableClientSideAPI="true">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Sampling HPH<span style="color:Red"> *</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtSSH" runat="server" Width="170px" 
                                    ReadOnly="True" EnableClientSideAPI="true" >
                                
                                <ValidationSettings>
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Sampling HPH<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtESH" runat="server" Width="170px" 
                                    ReadOnly="True" EnableClientSideAPI="true" >
                                
                                <ValidationSettings>
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Trading Hour<span style="color:Red"> *</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtSTH" runat="server" Width="170px" 
                                    ReadOnly="True" EnableClientSideAPI="true" >
                                
                                <ValidationSettings>
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Trading Hour<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                        
                            <dx:ASPxTextBox ID="txtETH" runat="server" Width="170px" 
                                    ReadOnly="True" EnableClientSideAPI="true" 
                                >
                                <ValidationSettings>
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                                Multiplier
                            </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtMultiplier" runat="server" Width="170px" 
                                    ReadOnly="True" EnableClientSideAPI="true" 
                                MaskSettings-Mask="<0..999999999999999g>">
                                <ValidationSettings>
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                                Margin Type
                            </td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtMarginType" runat="server" Width="170px"  
                                />
                        </td>
                    </tr>
                    <tr>
                            <td>
                                Margin Value
                            </td>
                            <td style="width: 168px">
                                <dx:ASPxTextBox ID="txtMarginValue" runat="server" Width="170px">
                                    <ClientSideEvents Validation="OnNumberValidation" />
                                  </dx:ASPxTextBox>
                            </td>
                        </tr>
                    <tr>
                        <td>
                                Minimum Fee
                            </td>
                        <td >
                            <dx:ASPxTextBox ID="txtMinFee" runat="server" MaxLength="8">
                            <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
                </div>
                <div class="clear"></div>
                    <br />
                    <table>
                        <tr>
                            <td>
                            
                                <dx:ASPxButton ID="btnSave" runat="server" Text="Save" 
                                    onclick="btnSave_Click" Width="70px"></dx:ASPxButton>
                            </td>
                            
                        </tr>
                    </table>
                    <dx:ASPxHiddenField ID="hfTemplate" runat="server" 
                        ClientInstanceName="hfTemplate">
                        </dx:ASPxHiddenField>
                </div>
</asp:Content>
