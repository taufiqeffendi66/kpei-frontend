﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="group.aspx.cs" Inherits="imq.kpei.administration.group" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<script language="javascript" type="text/javascript">
        // <![CDATA[

    function OnGridFocusedRowChanged() {
        grid.GetRowValues(grid.GetFocusedRowIndex(), 'memberId;groupId;', OnGetRowValues);

    }

    function OnGetRowValues(values) {
        //clID.SetText(values[0]);
        //clName.SetText(values[1]);
        hf.Set("memberId", values[0]);
        hf.Set("groupId", values[1]);
    }

    function ConfirmDelete(s, e) {
        e.processOnServer = confirm("Are you sure delete this Group \nMemberId = " + hf.Get("memberId") + ",\nGroupId = " + hf.Get("groupId"));
    }

        // ]]>
    </script>  
    <div id="content">
        <div class="title">GROUP</div>
         <div>
         <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                <tr>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnPDF" runat="server" OnClick="btnPdfExport_Click" class="login_button" ToolTip="Export to PDF"><img src="../Styles/pdf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnXLS" runat="server" OnClick="btnXlsExport_Click" class="login_button" ToolTip="Export to XLS"><img src="../Styles/excel.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnRTF" runat="server" OnClick="btnRtfExport_Click" class="login_button" ToolTip="Export to RTF"><img src="../Styles/rtf.png" /></asp:LinkButton></td>
                    <td style="padding-right: 4px"><asp:LinkButton ID="btnCSV" runat="server" OnClick="btnCsvExport_Click" class="login_button" ToolTip="Export to CSV"><img src="../Styles/csv.png" /></asp:LinkButton></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Member Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtMemberId" ClientInstanceName="clMemberId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Group Id"></dx:ASPxLabel></td>
                    <td><dx:ASPxTextBox ID="dtGroupId" ClientInstanceName="clGroupId" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click"> </dx:ASPxButton></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td></td>
                    <td><dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click"> </dx:ASPxButton></td>
                    <td></td>
                    <td><dx:ASPxButton ID="btnRemove" runat="server" Text="Delete" onclick="btnRemove_Click" ClientSideEvents-Click="ConfirmDelete"> </dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>       
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"  CssClass="grid">
                    <Columns>
                        <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Member Name" FieldName="memberId"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Group Name" FieldName="groupId"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Description" FieldName="des"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                    </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />                
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />

                <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="200" ShowGroupPanel="True" />
                <Styles><AlternatingRow Enabled="True"></AlternatingRow></Styles>

            </dx:ASPxGridView>            
            <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf"></dx:ASPxHiddenField>
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView"> </dx:ASPxGridViewExporter>
        </div>
    </div>
    <div>
        <dx:ASPxLabel ID="dtLabel" runat="server" Text=""></dx:ASPxLabel>
        <table>
            <tr>
                <td><dx:ASPxButton ID="btnDelOk" runat="server" Text="Ok" onclick="btnDelOk_Click"></dx:ASPxButton></td>
                <td><dx:ASPxButton ID="btnDelCancel" runat="server" Text="Cancel" onclick="btnDelCancel_Click"></dx:ASPxButton></td>
            </tr>
        </table>              
    </div>

</asp:Content>
