﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="multiplierTemplateInput.aspx.cs" Inherits="imq.kpei.administration.multiplierTemplateInput" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        // <![CDATA[
            function OnValidation(s, e) {
                var val = e.value;
                if (val == null)
                    return;

                if (val.length < 1)
                    e.isValid = false;
            }

            function OnNumberValidation(s, e) {
                var num = e.value;
                if (num == null || num == "")
                    return;

                var digits = "0123456789.,";
                for (var i = 0; i < num.length; i++) {
                    if (digits.indexOf(num.charAt(i)) == -1) {
                        e.isValid = false;
                        break;
                    }
                }
            }
        //]]>
    </script>
            <div  class="content">
                <div class="title">
                    <dx:ASPxLabel ID="lblTitle" runat="server" Text="Multiplier Template"></dx:ASPxLabel>
                </div>
                <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" 
                    Font-Bold="true">
                 </dx:ASPxLabel>
                <div id="plApproval">
                    <table>
                        <tr>
                            <td>                          
                                <dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                                        ClientIDMode="AutoID">
                                </dx:ASPxRadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <table id="tblContract">
              
                    <tr>
                        <td nowrap="nowrap">
                            Template Name<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtTmplName" runat="server" MaxLength="20" Size="20" 
                                Width="170px" EnableClientSideAPI="true">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="false" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Underlying<span style="color:Red">*</span></td>
                        <td style="width: 168px">                      
                            <dx:ASPxComboBox ID="cmbUnderlying" runat="server" EnableClientSideAPI="true" 
                                ValueType="System.String"></dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Tick Size<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbTickSize" runat="server" EnableClientSideAPI="True">
                                <Items>
                                    <dx:ListEditItem Text="0.05" Value="0.05" Selected="true" />
                                    <dx:ListEditItem Text="0.01" Value="0.01" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Tick Value<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtTickValue" runat="server" Width="170px" EnableClientSideAPI="true"
                                 MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>"  
                                HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Value<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtClearingFeeValue" runat="server" Width="170px" EnableClientSideAPI="true"
                                 MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>"  
                                HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Type<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbClearingFeeType" runat="server" 
                                EnableClientSideAPI="True" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Fixed" Value="F" />
                                    <dx:ListEditItem Text="Percentage" Value="P" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Clearing Fee Calculation<span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbClearingFeeCalculation" runat="server" 
                                EnableClientSideAPI="True" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Per Contract" Value="K" />
                                    <dx:ListEditItem Text="Per Frequency" Value="F" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                   <tr>
                        <td nowrap="nowrap">
                            GF Fee <span style="color:Red">*</span></td>
                        <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtGfFee" runat="server" Width="170px" EnableClientSideAPI="true"
                                MaskSettings-Mask="<0..999999999999999g>.<0000000..9999999>"  
                                HorizontalAlign="Right">
                                <ValidationSettings ErrorText="Required">
                                    <RequiredField IsRequired="true" ErrorText="Required" />
                                </ValidationSettings>
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            HPH</td>
                         <td style="width: 168px">
                            <dx:ASPxTextBox ID="txtHph" runat="server" Width="170px" EnableClientSideAPI="true"
                                MaskSettings-Mask="<0..999999999999999g>"  
                                HorizontalAlign="Right">                                
                                <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Sampling Hph Type</td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbSamplingHPHType" runat="server" 
                                EnableClientSideAPI="True" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Fixed" Value="F" />
                                    <dx:ListEditItem Text="Range" Value="R" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Start Sampling HPH<span style="color:Red"> </span></td>
                        <td style="width: 168px;">
                            <dx:ASPxTimeEdit ID="txtSSH" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true">
                               
                            </dx:ASPxTimeEdit>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            End Sampling HPH</td>
                        <td style="width: 168px">
                            <dx:ASPxTimeEdit ID="txtESH" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true" >
                                
                            </dx:ASPxTimeEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Trading Hour<span style="color:Red"> </span></td>
                        <td style="width: 168px">
                            <dx:ASPxTimeEdit ID="txtSTH" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true">
                                
                            </dx:ASPxTimeEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Trading Hour</td>
                        <td style="width: 168px">
                        
                            <dx:ASPxTimeEdit ID="txtETH" runat="server" AutoPostBack="true" 
                                EditFormat="Custom" EditFormatString="HH:mm" EnableClientSideAPI="true" >
                                
                            </dx:ASPxTimeEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>Multiplier<span style="color:Red">*</span>
                            </td>
                        
                            <td style="width: 168px"><dx:ASPxTextBox ID="txtMultiplier" runat="server" Width="170px" EnableClientSideAPI="true"
                                    MaskSettings-Mask="<0..999999999999999g>" HorizontalAlign="Right">
                                    <ValidationSettings ErrorText="Required">
                                        <RequiredField IsRequired="true" ErrorText="Required" />
                                    </ValidationSettings>
                                    <ClientSideEvents Validation="OnNumberValidation" />                    
                                    </dx:ASPxTextBox></td>
                    </tr>
                    <tr>
                        <td>
                                Margin Type<span style="color:Red">*</span>
                            </td>
                        <td style="width: 168px">
                            <dx:ASPxComboBox ID="cmbMarginType" runat="server" 
                                EnableClientSideAPI="True" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Fixed" Value="F" />
                                    <dx:ListEditItem Text="Floating" Value="L" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                            <td>
                                Margin Value<span style="color:Red">*</span>
                            </td>
                            <td style="width: 168px">
                                <dx:ASPxTextBox ID="txtMarginValue" runat="server" Width="170px" MaskSettings-Mask="<0..99999g>" HorizontalAlign="Right">
                                    <ClientSideEvents Validation="OnNumberValidation" />
                                  </dx:ASPxTextBox>
                            </td>
                        </tr>
                    <tr>
                        <td>
                                Minimum Fee
                            </td>
                        <td >
                            <dx:ASPxTextBox ID="txtMinFee" runat="server" MaskSettings-Mask="<0..999999999999999g>" HorizontalAlign="Right">
                            <ClientSideEvents Validation="OnNumberValidation" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
                </div>
                <div class="clear"></div>
                    <br />
                    <table>
                        <tr>
                            <td>
                            
                                <dx:ASPxButton ID="btnSave" runat="server" Text="Save" 
                                    onclick="btnSave_Click" Width="70px"></dx:ASPxButton>
                            </td>
                             <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                                onclick="btnChecker_Click" Width="70px"></dx:ASPxButton>
                             </td>
                            <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                                    onclick="btnApproval_Click" Width="70px"></dx:ASPxButton></td>
                            <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"></dx:ASPxButton></td>
                            <td>
                                <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                                    onclick="btnCancel_Click" CausesValidation="false" Width="80px"></dx:ASPxButton>
                            </td>         
                        </tr>
                    </table>
                    <dx:ASPxHiddenField ID="hfTemplate" runat="server" 
                        ClientInstanceName="hfTemplate">
                        </dx:ASPxHiddenField>
                </div>
</asp:Content>
