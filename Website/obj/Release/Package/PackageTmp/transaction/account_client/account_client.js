﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.ProgressBar',
    'Ext.window.Window'
]);

var accountSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var accountInitialData = [];
var cb;
var storeCb;
var recordIndex = -1;
var arrAccount = [];
var endProses = true;
var pageSize = 20;
var tick = 200;

function addArrAccount(s, force) {
    data = eval('(' + s + ')');
    arrAccount.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrAccount.length > 0) {
        endProses = false;
        if (arrAccount.length > pageSize)
            max = pageSize;
        else
            max = arrAccount.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrAccount.shift()
            addToAccount(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToAccount(data) {
    if (accountInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.sID === "undefined") || (data.sID == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });


    //is data exists
    var recordIndex = -1;
    for (i = 0; i < accountInitialData.length; i++) {
        if (data.memberID == accountInitialData[i].memberID && data.sID == accountInitialData[i].sID) {
            recordIndex = i;
            break;
        }
    }

    if (recordIndex == -1) {
        accountInitialData.push(data);
    } else {
        accountInitialData[recordIndex].memberID = data.memberID;
        accountInitialData[recordIndex].sID = data.sID;
        accountInitialData[recordIndex].tradingID = data.tradingID;
        accountInitialData[recordIndex].initialMargin = data.initialMargin;
        accountInitialData[recordIndex].rInitialMargin = data.rInitialMargin;
        accountInitialData[recordIndex].marginCall = data.marginCall;
        accountInitialData[recordIndex].exposure = data.exposure;
        accountInitialData[recordIndex].indicator = data.indicator;
        accountInitialData[recordIndex].procentage = data.procentage;
        accountInitialData[recordIndex].collateralAccountNo = data.collateralAccountNo;
        accountInitialData[recordIndex].collateralBlock = data.collateralBlock;
        accountInitialData[recordIndex].collateralFree = data.collateralFree;
        accountInitialData[recordIndex].collateralTotal = data.collateralTotal;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}

function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addaccount(s, force) {
    if (accountInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.sID === "undefined") || (data.sID == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });


    //is data exists
    var recordIndex = -1;
    for (i = 0; i < accountInitialData.length; i++) {
        if (data.memberID == accountInitialData[i].memberID && data.sID == accountInitialData[i].sID) {
            recordIndex = i;
            break;
        }
    }
     
    if (recordIndex == -1) {
        accountInitialData.push(data);
    } else {
        accountInitialData[recordIndex].memberID = data.memberID;
        accountInitialData[recordIndex].sID = data.sID;
        accountInitialData[recordIndex].tradingID = data.tradingID;
        accountInitialData[recordIndex].initialMargin = data.initialMargin;
        accountInitialData[recordIndex].rInitialMargin = data.rInitialMargin;
        accountInitialData[recordIndex].marginCall = data.marginCall;
        accountInitialData[recordIndex].exposure = data.exposure;
        accountInitialData[recordIndex].indicator = data.indicator;
        accountInitialData[recordIndex].procentage = data.procentage;
        accountInitialData[recordIndex].collateralAccountNo = data.collateralAccountNo;
        accountInitialData[recordIndex].collateralBlock = data.collateralBlock;
        accountInitialData[recordIndex].collateralFree = data.collateralFree;
        accountInitialData[recordIndex].collateralTotal = data.collateralTotal;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeaccountTopic(direction) {
    if ((!(typeof accountSubscriptionId === "undefined")) && (accountSubscriptionId != null)) {
        client.unsubscribe(accountSubscriptionId);
        accountSubscriptionId = null;
    }

    for (; accountInitialData.length > 0; )
        accountInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";

    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var accountSubscritptionHeader = { 'selector': selector };
        accountSubscriptionId = client.subscribe(accountTopic, function(message) {
            addArrAccount(message.body, true);
        }, accountSubscritptionHeader);
    } else {
        accountSubscriptionId = client.subscribe(accountTopic, function(message) {
            addArrAccount(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=account_client&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue(),
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrAccount(obj[ia].accountData, false);
            }
        },
        failure: function(response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof accountSubscriptionId === "undefined")) && (accountSubscriptionId != null)) {
        client.unsubscribe(accountSubscriptionId);
        accountSubscriptionId = null;
    }

    var selector = "";

    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var accountSubscritptionHeader = { 'selector': selector };
        accountSubscriptionId = client.subscribe(accountTopic, function (message) {
            addArrAccount(message.body, true);
        }, accountSubscritptionHeader);
    } else {
        accountSubscriptionId = client.subscribe(accountTopic, function (message) {
            addArrAccount(message.body, true);
        });
    }
    window.open('./accountClientReportView.aspx?type=accountClient&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction );
}

var detailIndicatorFormWin;
var detailIndicatorForm;

var pro = Ext.create('Ext.ProgressBar', {
    name: 'mybar',
    renderTo: Ext.getBody(),
    width: 300
});

function createDetailIndicatorFormWin() {
     if (!detailIndicatorFormWin) {
        detailIndicatorForm = new Ext.FormPanel( {
        //detailIndicatorForm = Ext.widget('form', {
            border: false,
            //bodyPadding: 10,
            bodyStyle: 'padding:5px 5px 0',
            //width: 188,
            defaultType: 'displayfield',
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaults: {
                margins: '0 0 10 0',
                anchor: '100%'
            },
            items: [
                {
                    //xtype: 'displayfield',
                    name: 'displayfieldSid',
                    id: 'displayfieldSid',
                    fieldLabel: 'SID',
                    value: ''
                },
                {
                    //xtype: 'displayfield',
                    name: 'displayfieldExposure',
                    id: 'displayfieldExposure',
                    fieldLabel: 'Exposure',
                    value: ''
                },
                {
                    //xtype: 'displayfield',
                    name: 'displayfieldInitialMargin',
                    id: 'displayfieldInitialMargin',
                    fieldLabel: 'Initial Margin',
                    value: ''
                },
                {
                    //xtype: 'displayfield',
                    name: 'displayfieldIndicator',
                    id: 'displayfieldIndicator',
                    fieldLabel: 'Indicator',
                    value: ''
                },
                Ext.create('Ext.ProgressBar', {
                    name: 'mybar',
                    id: 'mybar',
                    renderTo: Ext.getBody(),
                    width: 200
                })
            ],

            buttons:
                [
                    {
                        text: 'Cancel',
                        handler: function () {
                            this.up('form').getForm().reset();
                            this.up('window').hide();
                        }
                    }
                ]
        });
        detailIndicatorFormWin = Ext.widget('window', {
            title: 'Detail Indicator',
            closeAction: 'hide',
            width: 225,
            height: 200,
            minHeight: 160,
            layout: 'fit',
            resizable: false,
            modal: true,
            items: detailIndicatorForm
        });
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var Runner = function () {
    var f = function (v, pbar) {
        return function () {
            pbar.updateProgress(v / 100,  v + ' of ' + 100 + '%');          
        };
    };
    return {
        run: function (pbar, count) {
            setTimeout(f(count, pbar), 10);
        }
    };
} ();


function showDetailIndicator(record) {
	createDetailIndicatorFormWin();
	Ext.getCmp('displayfieldSid').setValue(record.get('sID'));
	Ext.getCmp('displayfieldInitialMargin').setValue(record.get('initialMargin'));
	Ext.getCmp('displayfieldExposure').setValue(record.get('exposure'));
	Ext.getCmp('displayfieldIndicator').setValue(record.get('procentage'));
	var dt = record.get('procentage') * 1;
	var pro = Ext.getCmp('mybar');
	Runner.run(pro, dt);
	detailIndicatorFormWin.show();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var accountModel = Ext.define('accountModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'memberID' },
               { name: 'sID' },
               { name: 'tradingID' },
               { name: 'initialMargin' },
               { name: 'rInitialMargin' },
               { name: 'marginCall' },
               { name: 'exposure' },
               { name: 'indicator' },
               { name: 'procentage' },
               { name: 'collateralAccountNo' },
               { name: 'collateralBlock' },
               { name: 'collateralFree' },
               { name: 'collateralTotal' },
               { name: 'collateralWithdrawRequest' }
            ]
    });

    var accountReader = new Ext.data.reader.Json({
        model: 'accountModel'
    }, accountModel);

    var accountStore = Ext.create('Ext.data.ArrayStore', {
        id: 'accountStore',
        model: 'accountModel',
        data: accountInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(accountInitialData),
        autoLoad: true,
        autoSync: true,
        reader: accountReader
    });

    // create the Grid
    var accountGrid = Ext.create('Ext.grid.Panel', {
        store: accountStore,
        id: 'accountGrid',
        stateful: false,
        stateId: 'accountGrid',
        columns: [
                { text: 'Member ID', dataIndex: 'memberID', width: 75, sortable: true, align: 'center', draggable: false },
                { text: 'SID', dataIndex: 'sID', width: 100, sortable: true, align: 'left', draggable: false },
                { text: 'Trading ID', dataIndex: 'tradingID', width: 100, sortable: true, align: 'left', draggable: false },
                { text: 'Initial<br/>Margin', dataIndex: 'initialMargin', width: 100, sortable: true, align: 'right', draggable: false },
                // diah 20130408 { text: 'R-Initial<br/>Margin', dataIndex: 'rInitialMargin', width: 100, sortable: false, align: 'right', draggable: false },
                {text: 'Margin<br/>Call', dataIndex: 'marginCall', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Exposure', dataIndex: 'exposure', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Indicator', dataIndex: 'procentage', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Collateral', columns: [
                    { text: 'Bank Account No', dataIndex: 'collateralAccountNo', width: 100, sortable: true, align: 'center', draggable: false },
                    // diah 20120711                    { text: 'Block', dataIndex: 'collateralBlock', width: 100, sortable: false, align: 'center', draggable: false },
                    { text: 'Free', dataIndex: 'collateralFree', width: 100, sortable: true, align: 'center', draggable: false },
                    { text: 'Value', dataIndex: 'collateralTotal', width: 100, sortable: true, align: 'center', draggable: false },
                    { text: 'Withdraw Request', dataIndex: 'collateralWithdrawRequest', flex: 1, sortable: true, align: 'center', draggable: false }
					]
                }
        ],
		listeners: {
			itemdblclick: function(dv, record, item, index, e) {
                if (record) {
					showDetailIndicator(record);
                }
            }
        },
		viewConfig: {
            stripeRows: false
            , forceFit: true
            , loadMask: false
            , getRowClass: function (record) {
                var indi = record.get('indicator');
                if (indi <= 1) return 'green-row';
                else if (indi <= 2) return 'green-row';
                else if (indi <= 3) return 'green-row';
                else if (indi <= 4) return 'yellow-row';
                else if (indi <= 5) return 'orange-row';
                else if (indi > 5) return 'red-row';
            }
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: accountStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    accountGrid.setLoading(false, false);
    accountStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        //store: memberStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function(combo, value) {
                var sidCombo = Ext.getCmp('sidCombo');
                sidCombo.clearValue();
                //TODO reload/filter sid combobox data
                //sidCombo.store.filter('cid', combo.getValue());
            }
            }
        }
    });

    memberCombo.on('select', function(box, record, index) {

    });

//    Ext.define('ContractId', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        //store: contractIdStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        //store: sidStore,
        //store: contractIdStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
//                    { xtype: 'label', text: 'Contract ID: ' },
//                    contractIdCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
//                    { xtype: 'label', text: 'Contract Type: ' },
//                    contractTypeCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function() {
                            subscribeaccountTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    },
                    { xtype: 'button', text: ' Detail Indicator ',
                        handler: function () {
                            selectedRecords = Ext.getCmp('accountGrid').getSelectionModel().getSelection();
                            if (selectedRecords.length > 0) {
								showDetailIndicator(selectedRecords[0]);
							}
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Account - Client',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: accountGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function(str) {
    };
    var onconnect = function(frame) {
        Ext.getCmp('accountGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function(message) {
        });

        subscribeaccountTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function() {
        if ((!(typeof accountSubscriptionId === "undefined")) && (accountSubscriptionId != null)) {
            client.unsubscribe(accountSubscriptionId);
            accountSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function() {
        });
    }
});
