﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);
var tick = 100;
var tradeSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
//var currentPage = 0;
//var minId = 0;
//var maxId = 0;
//var maxRow = 30;
//var stage = 'last';
//var arrTrade;
//var posArrTrade = 0;
//var adaData = false;
//var firstData = true;
//var endProsesTrade = true;
//var count = 0;


var tradeSummaryInitialData = [];
var arrTradeSummary = [];
var pageSizeTradeSummary = 4;
var adaDataTradeSummary = false;
var endProsesTradeSummary = true;

var contractSubscriptionId = null;
var contractInitialData = [];
var arrContract = [];
var pageSizeContract = 20;
var adaDataContract = false;
var endProsesContract = true;

var dPop;


function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function loadToStoreTradeSummary() {

    if (arrTradeSummary.length > 0) {
        endProsesTradeSummary = false;
        if (arrTradeSummary.length > pageSizeTradeSummary)
            max = pageSizeTradeSummary;
        else
            max = arrTradeSummary.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrTradeSummary.shift()
            addToTradeSummary(dPop);
        }
        Ext.getCmp('pagingToolbarTS').doRefresh();
        endProsesTradeSummary = true;
    }
}

function addToTradeSummary(dt) {
    if (tradeSummaryInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    if ((typeof dt.category === "undefined") || (dt.category == null)) {
        return;
    }

    //is data exists
    var recordIndexTradeSummary = -1;
    for (i = 0; i < tradeSummaryInitialData.length; i++) {
        if (dt.category == tradeSummaryInitialData[i].category) {
            recordIndexTradeSummary = i;
            break;
        }
    }
    if (recordIndexTradeSummary == -1) {
        tradeSummaryInitialData.push(dt);
    } else {
        tradeSummaryInitialData[recordIndexTradeSummary] = dt;
    }
}

function subscribeTradeSummaryTopic(direction, from) {

    tradeSummarySubscriptionId = client.subscribe(tt, function (message) {
        data = eval('(' + message.body + ')');
        arrTradeSummary.push(data);
    });

    Ext.Ajax.request({
        url: '../request.aspx?type=trade_summary&memberId=&contractId=&sid=&contractType=&direction=last&minId=&maxId=',
        success: function (response, opts) {
            var obj = eval('(' + response.responseText + ')');

            for (ii = 0; ii < obj.length; ii++) {
                data = eval('(' + obj[ii].tradeSummaryData + ')');
                //addArrContract(obj[ii].contractSummaryData, 1);
                arrTradeSummary.push(data);
            }
            adaDataTradeSummary = true;

        },
        failure: function (response, opts) {

        }
    });
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function loadToStoreContract() {

    if (arrContract.length > 0) {
        endProsesContract = false;
        if (arrContract.length > pageSizeContract)
            max = pageSizeContract;
        else
            max = arrContract.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrContract.shift()
            addToContract(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProsesContract = true;
    }
}

function addToContract(dt) {
    if (contractInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    if ((typeof dt.series === "undefined") || (dt.series == null)) {
        return;
    }

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', dt.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: dt.series, name: dt.series });

    //is data exists
    var recordIndexContract = -1;
    for (i = 0; i < contractInitialData.length; i++) {
        if (dt.series == contractInitialData[i].series) {
            recordIndexContract = i;
            break;
        }
    }
    if (recordIndexContract == -1) {
        contractInitialData.push(dt);
    } else {
        contractInitialData[recordIndexContract] = dt;
    }
}

function subscribeContractTopic(direction, from) {
    if ((!(typeof contractSubscriptionId === "undefined")) && (contractSubscriptionId != null)) {
        client.unsubscribe(contractSubscriptionId);
        contractSubscriptionId = null;
    }

    for (; contractInitialData.length > 0; )
        contractInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";

    if (Ext.getCmp('contractIdCombo').getValue() != null)
        selector = "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";

    if (selector != "") {
        var contractSubscritptionHeader = { 'selector': selector };
        contractSubscriptionId = client.subscribe(ss, function (message) {
            //addArrContract(message.body, true);
            data = eval('(' + message.body + ')');
            arrContract.push(data);
        }, contractSubscritptionHeader);
    } else {
        contractSubscriptionId = client.subscribe(ss, function (message) {
            //addArrContract(message.body, true);
            data = eval('(' + message.body + ')');
            arrContract.push(data);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=contract_summary&memberId=&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=&contractType=&direction=last&minId=&maxId=',
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')'); 

            for (ii = 0; ii < obj.length; ii++) {
                data = eval('(' + obj[ii].contractSummaryData + ')');
                //addArrContract(obj[ii].contractSummaryData, 1);
                arrContract.push(data);
            }
            adaDataContract = true;

        },
        failure: function(response, opts) {

        }
    });
}

function checkData() {
    if (endProsesTradeSummary) loadToStoreTradeSummary()
    if (endProsesContract) loadToStoreContract()
}

Ext.onReady(function () {

    setInterval(function () { checkData() }, tick);

    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

//////////////////////////////////////////////////////////////////////////////////////////////
    var tradeSummaryModel = Ext.define('tradeSummaryModel', {
        extend: 'Ext.data.Model',
        fields: [
           { name: 'category' },
           { name: 'freq' },
           { name: 'vol' },
           { name: 'val' }
        ]
    });

    var tradeSummaryReader = new Ext.data.reader.Json({
        model: 'tradeSummaryModel'
    }, tradeSummaryModel);

    var tradeSummaryStore = Ext.create('Ext.data.Store', {
        id: 'tradeSummaryStore',
        model: 'tradeSummaryModel',
        data: tradeSummaryInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(tradeSummaryInitialData),
        autoLoad: true,
        autoSync: true,
        reader: tradeSummaryReader
    });


    var tradeSummaryGrid = Ext.create('Ext.grid.Panel', {
        store: tradeSummaryStore,
        id: 'tradeSummaryGrid',
        stateful: false,
        stateId: 'tradeSummaryGrid',
        columns: [
                { text: 'Category', dataIndex: 'category', width: 110, sortable: false, align: 'left', draggable: false },
                { text: 'Freq', dataIndex: 'freq', width: 110, sortable: false, align: 'right', draggable: false },
                { text: 'Volume', dataIndex: 'vol', width: 110, sortable: false, align: 'right', draggable: false },
                { text: 'Value', dataIndex: 'val', width: 200, sortable: false, align: 'right', draggable: false }
        ],
        viewConfig: {
            stripeRows: true,
            forceFit: true,
            loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbart: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbarTS',
            store: tradeSummaryStore,
            pageSize: pageSizeTradeSummary,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })

    });
    tradeSummaryGrid.setLoading(false, false);
    tradeSummaryStore.load({ params: { start: 0, limit: pageSizeTradeSummary} });

//////////////////////////////////////////////////////////////////////////////////////////////

    var contractModel = Ext.define('contractModel', {
        extend: 'Ext.data.Model',
        fields: [
           { name: 'series' },
           { name: 'freq' },
           { name: 'vol' },
           { name: 'val' }
        ]
    });

    var contractReader = new Ext.data.reader.Json({
        model: 'contractModel'
    }, contractModel);

    var contractStore = Ext.create('Ext.data.Store', {
        id: 'contractStore',
        model: 'contractModel',
        data: contractInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(contractInitialData),
        autoLoad: true,
        autoSync: true,
        reader: contractReader
    });


    var contractGrid = Ext.create('Ext.grid.Panel', {
        store: contractStore,
        id: 'contractGrid',
        stateful: false,
        stateId: 'contractGrid',
        columns: [
                { text: 'Contract', dataIndex: 'series', width: 110, sortable: false, align: 'left', draggable: false },
                { text: 'Freq', dataIndex: 'freq', width: 110, sortable: false, align: 'right', draggable: false },
                { text: 'Volume', dataIndex: 'vol', width: 110, sortable: false, align: 'right', draggable: false },
                { text: 'Value', dataIndex: 'val', width: 200, sortable: false, align: 'right', draggable: false }
        ],
        viewConfig: {
            stripeRows: true,
            forceFit: true,
            loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: contractStore,
            pageSize: pageSizeContract,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })

    });
    contractGrid.setLoading(false, false);
    contractStore.load({ params: { start: 0, limit: pageSizeContract} });
//////////////////////////////////////////////////////////////////////////////////////////////


    Ext.define('ContractId', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractIdStore = Ext.create('Ext.data.Store', {
        model: 'ContractId',
        data: ci
    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 150,
        store: contractIdStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Contract ID: ' },
                    contractIdCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function () {
                            currentPage = 0;
                            Ext.getCmp('pagingToolbar').moveFirst();
                            subscribeContractTopic("last", 1);
                        }
                    },
                ]
    });
   var contractPanle = Ext.create('Ext.panel.Panel', {
        id: 'contractPanel',
        bodyPadding: 5,
        title: 'Contract',
        layout: 'border',
        items: [
            {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            },{
                region: 'center', 
                layout: 'fit',                
                frame: false, 
                border: false, 
                items: contractGrid
            }
        ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Trade Summary',
        layout: 'border',
        items: [
            {
                region: 'north'
                , height: 150
                , frame: false
                , border: false
                , items: tradeSummaryGrid
            },{
                region: 'center', 
                layout: 'fit',                
                frame: false, 
                border: false,
                items: contractPanle
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: [contentPanel]
    });

    //Ext.getCmp('pagingPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(u);
    client.debug = function (str) {
    };
    var onconnect = function (frame) {
        Ext.getCmp('tradeSummaryGrid').getStore().loadData([], false);
        pingSubscriptionId = client.subscribe(pt, function (message) {
        });

        subscribeTradeSummaryTopic("last", 1);
        subscribeContractTopic("last", 1);
    };

    client.connect(l, p, onconnect);


    window.onbeforeunload = function () {
        if ((!(typeof tradeSubscriptionId === "undefined")) && (tradeSubscriptionId !== null)) {
            client.unsubscribe(tradeSubscriptionId);
            tradeSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId !== null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function () {
        });
    }
});
