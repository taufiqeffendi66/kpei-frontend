﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var blockedCollateralSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var blockedCollateralInitialData = [];
var cb;
var storeCb;
var recordIndex = -1;

var arrBlockedCollateral = [];
var endProses = true;
var pageSize = 20;
var tick = 200;

function addArrBlockedCollateral(s, force) {
    data = eval('(' + s + ')');
    arrBlockedCollateral.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrBlockedCollateral.length > 0) {
        endProses = false;
        if (arrBlockedCollateral.length > pageSize)
            max = pageSize;
        else
            max = arrBlockedCollateral.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrBlockedCollateral.shift()
            addToBlockedCollateral(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToBlockedCollateral(data) {
    if (blockedCollateralInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.memberID === "undefined") || (data.memberID == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < blockedCollateralInitialData.length; i++) {
        if (data.memberID == blockedCollateralInitialData[i].memberID && data.sID == blockedCollateralInitialData[i].sID) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        blockedCollateralInitialData.push(data);
    } else {
        blockedCollateralInitialData[recordIndex].memberID = data.memberID;
        blockedCollateralInitialData[recordIndex].sID = data.sID;
        blockedCollateralInitialData[recordIndex].role = data.role;
        blockedCollateralInitialData[recordIndex].netSettlement = data.netSettlement;
        blockedCollateralInitialData[recordIndex].mtm = data.mtm;
        blockedCollateralInitialData[recordIndex].initialMargin = data.initialMargin;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}


function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addblockedCollateral(s, force) {
    if (blockedCollateralInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.memberID === "undefined") || (data.memberID == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < blockedCollateralInitialData.length; i++) {
        if (data.memberID == blockedCollateralInitialData[i].memberID && data.sID == blockedCollateralInitialData[i].sID) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        blockedCollateralInitialData.push(data);
    } else {
        blockedCollateralInitialData[recordIndex].memberID = data.memberID;
        blockedCollateralInitialData[recordIndex].sID = data.sID;
        blockedCollateralInitialData[recordIndex].role = data.role;
        blockedCollateralInitialData[recordIndex].netSettlement = data.netSettlement;
        blockedCollateralInitialData[recordIndex].mtm = data.mtm;
        blockedCollateralInitialData[recordIndex].initialMargin = data.initialMargin;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeblockedCollateralTopic(direction) {
    if ((!(typeof blockedCollateralSubscriptionId === "undefined")) && (blockedCollateralSubscriptionId != null)) {
        client.unsubscribe(blockedCollateralSubscriptionId);
        blockedCollateralSubscriptionId = null;
    }

    for (; blockedCollateralInitialData.length > 0 ; )
        blockedCollateralInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var blockedCollateralSubscritptionHeader = { 'selector': selector };
        blockedCollateralSubscriptionId = client.subscribe(blockedCollateralTopic, function(message) {
            addArrBlockedCollateral(message.body, true);
        }, blockedCollateralSubscritptionHeader);
    } else {
        blockedCollateralSubscriptionId = client.subscribe(blockedCollateralTopic, function(message) {
            addArrBlockedCollateral(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=blockedCollateral_client&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue(),
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrBlockedCollateral(obj[ia].blockedCollateralData, false);
            }
        },
        failure: function(response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof blockedCollateralSubscriptionId === "undefined")) && (blockedCollateralSubscriptionId != null)) {
        client.unsubscribe(blockedCollateralSubscriptionId);
        blockedCollateralSubscriptionId = null;
    }

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var blockedCollateralSubscritptionHeader = { 'selector': selector };
        blockedCollateralSubscriptionId = client.subscribe(blockedCollateralTopic, function (message) {
            addArrBlockedCollateral(message.body, true);
        }, blockedCollateralSubscritptionHeader);
    } else {
        blockedCollateralSubscriptionId = client.subscribe(blockedCollateralTopic, function (message) {
            addArrBlockedCollateral(message.body, true);
        });
    }

    window.open('./blockedCollateralReportView.aspx?type=blockedCollateral&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}


Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var blockedCollateralModel = Ext.define('blockedCollateralModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'memberID' },
               { name: 'sID' },
               { name: 'role' },
               { name: 'netSettlement' },
               { name: 'mtm' },
               { name: 'initialMargin' }
            ]
    });

    var blockedCollateralReader = new Ext.data.reader.Json({
        model: 'blockedCollateralModel'
    }, blockedCollateralModel);

    var blockedCollateralStore = Ext.create('Ext.data.ArrayStore', {
        id: 'blockedCollateralStore',
        model: 'blockedCollateralModel',
        data: blockedCollateralInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(blockedCollateralInitialData),
        autoLoad: true,
        autoSync: true,
        reader: blockedCollateralReader
    });

    // create the Grid
    var blockedCollateralGrid = Ext.create('Ext.grid.Panel', {
        store: blockedCollateralStore,
        id: 'blockedCollateralGrid',
        stateful: false,
        stateId: 'blockedCollateralGrid',
        columns: [
                { text: 'Member<br>ID', dataIndex: 'memberID', width: 80, sortable: true, align: 'center', draggable: false },
                { text: 'SID', dataIndex: 'sID', flex: 1, sortable: true, align: 'left', draggable: false },
                { text: 'Role', dataIndex: 'role', width: 40, sortable: true, align: 'center', draggable: false },
                { text: 'Variation Margin', columns: [
                    { text: 'Net Settlement', dataIndex: 'netSettlement', width: 100, sortable: true, align: 'right', draggable: false },
                    { text: 'MTM', dataIndex: 'mtm', width: 100, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Initial Margin', dataIndex: 'initialMargin', width: 100, sortable: true, align: 'right', draggable: false }
        ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: blockedCollateralStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    blockedCollateralGrid.setLoading(false, false);
    blockedCollateralStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function(combo, value) {
                var sidCombo = Ext.getCmp('sidCombo');
                sidCombo.clearValue();
                //TODO reload/filter sid combobox data
                //sidCombo.store.filter('cid', combo.getValue());
            }
            }
        }
    });

    memberCombo.on('select', function(box, record, index) {

    });

//    Ext.define('ContractId', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
//                    { xtype: 'label', text: 'Contract ID: ' },
//                    contractIdCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
//                    { xtype: 'label', text: 'Contract Type: ' },
//                    contractTypeCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function() {
                            subscribeblockedCollateralTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Detail Blocked Collateral',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: blockedCollateralGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function(str) {
    };
    var onconnect = function(frame) {
        Ext.getCmp('blockedCollateralGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function(message) {
        });

        subscribeblockedCollateralTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function() {
        if ((!(typeof blockedCollateralSubscriptionId === "undefined")) && (blockedCollateralSubscriptionId != null)) {
            client.unsubscribe(blockedCollateralSubscriptionId);
            blockedCollateralSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function() {
        });
    }
});
