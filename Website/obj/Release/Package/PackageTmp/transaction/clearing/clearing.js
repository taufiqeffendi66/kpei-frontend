﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var clearingSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var clearingInitialData = [];
var cb;
var storeCb;
var recordIndex = -1;
var arrClearing = [];
var endProses = true;
var pageSize = 20;
var tick = 200;


function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addArrClearing(s, force) {
    data = eval('(' + s + ')');
    arrClearing.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrClearing.length > 0) {
        endProses = false;
        if (arrClearing.length > pageSize)
            max = pageSize;
        else
            max = arrClearing.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrClearing.shift()
            addToClearing(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToClearing(data) {
    if (clearingInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.series === "undefined") || (data.series == null)) {
        return;
    }
    //is data exists


    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });

    recordIndex = -1;
    for (i = 0; i < clearingInitialData.length; i++) {
        if (data.series == clearingInitialData[i].series && data.memberID == clearingInitialData[i].memberID && data.sID == clearingInitialData[i].sID) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        clearingInitialData.push(data);
    } else {
        clearingInitialData[recordIndex].memberID = data.memberID;
        clearingInitialData[recordIndex].sID = data.sID;
        clearingInitialData[recordIndex].clientID = data.clientID;
        clearingInitialData[recordIndex].role = data.role;
        clearingInitialData[recordIndex].contractType = data.contractType;
        clearingInitialData[recordIndex].series = data.series;
        clearingInitialData[recordIndex].prevBuy = data.prevBuy;
        clearingInitialData[recordIndex].prevSell = data.prevSell;
        clearingInitialData[recordIndex].openB = data.openB;
        clearingInitialData[recordIndex].openS = data.openS;
        clearingInitialData[recordIndex].closeB = data.closeB;
        clearingInitialData[recordIndex].closeS = data.closeS;
        clearingInitialData[recordIndex].liquidationB = data.liquidationB;
        clearingInitialData[recordIndex].liquidationS = data.liquidationS;
        clearingInitialData[recordIndex].exercise = data.exercise;
        clearingInitialData[recordIndex].assignment = data.assignment;
        clearingInitialData[recordIndex].netB = data.netB;
        clearingInitialData[recordIndex].netS = data.netS;
        clearingInitialData[recordIndex].premium = data.premium;
        clearingInitialData[recordIndex].gainLoss = data.gainLoss;
        clearingInitialData[recordIndex].penalty = data.penalty;
        clearingInitialData[recordIndex].compensation = data.compensation;
        clearingInitialData[recordIndex].settlement = data.settlement;
        clearingInitialData[recordIndex].fee = data.fee;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}

function addclearing(s, force) {
    if (clearingInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.series === "undefined") || (data.series == null)) {
        return;
    }
    //is data exists


    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });

    recordIndex = -1;
    for (i = 0; i < clearingInitialData.length; i++) {
        if (data.series == clearingInitialData[i].series && data.memberID == clearingInitialData[i].memberID && data.sID == clearingInitialData[i].sID) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        clearingInitialData.push(data);
    } else {
        clearingInitialData[recordIndex].memberID = data.memberID;
        clearingInitialData[recordIndex].sID = data.sID;
        clearingInitialData[recordIndex].clientID = data.clientID;
        clearingInitialData[recordIndex].role = data.role;
        clearingInitialData[recordIndex].contractType = data.contractType;
        clearingInitialData[recordIndex].series = data.series;
        clearingInitialData[recordIndex].prevBuy = data.prevBuy;
        clearingInitialData[recordIndex].prevSell = data.prevSell;
        clearingInitialData[recordIndex].openB = data.openB;
        clearingInitialData[recordIndex].openS = data.openS;
        clearingInitialData[recordIndex].closeB = data.closeB;
        clearingInitialData[recordIndex].closeS = data.closeS;
        clearingInitialData[recordIndex].liquidationB = data.liquidationB;
        clearingInitialData[recordIndex].liquidationS = data.liquidationS;
        clearingInitialData[recordIndex].exercise = data.exercise;
        clearingInitialData[recordIndex].assignment = data.assignment;
        clearingInitialData[recordIndex].netB = data.netB;
        clearingInitialData[recordIndex].netS = data.netS;
        clearingInitialData[recordIndex].premium = data.premium;
        clearingInitialData[recordIndex].gainLoss = data.gainLoss;
        clearingInitialData[recordIndex].penalty = data.penalty;
        clearingInitialData[recordIndex].compensation = data.compensation;
        clearingInitialData[recordIndex].settlement = data.settlement;
        clearingInitialData[recordIndex].fee = data.fee;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeclearingTopic(direction) {
    if ((!(typeof clearingSubscriptionId === "undefined")) && (clearingSubscriptionId != null)) {
        client.unsubscribe(clearingSubscriptionId);
        clearingSubscriptionId = null;
    }

    for (; clearingInitialData.length > 0 ; )
        clearingInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();         

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var clearingSubscritptionHeader = { 'selector': selector };
        clearingSubscriptionId = client.subscribe(clearingTopic, function(message) {
            addArrClearing(message.body, true);
        }, clearingSubscritptionHeader);
    } else {
        clearingSubscriptionId = client.subscribe(clearingTopic, function(message) {
            addArrClearing(message.body, true);
        });
    }

    Ext.Ajax.request({        
        url: '../request.aspx?type=clearing&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=Last&minId=""&maxId=""',
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')'); //Ext.decode(response.responseText);
            for (ia = 0; ia < obj.length; ia++) {
                addArrClearing(obj[ia].clearingData, false);
            }
            //Ext.getCmp('clearingGrid').getStore().load();
            //Ext.getCmp('clearingGrid').getView().refresh();
        },
        failure: function(response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof clearingSubscriptionId === "undefined")) && (clearingSubscriptionId != null)) {
        client.unsubscribe(clearingSubscriptionId);
        clearingSubscriptionId = null;
    }

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var clearingSubscritptionHeader = { 'selector': selector };
        clearingSubscriptionId = client.subscribe(clearingTopic, function (message) {
            addclearing(message.body, true);
        }, clearingSubscritptionHeader);
    } else {
        clearingSubscriptionId = client.subscribe(clearingTopic, function (message) {
            addclearing(message.body, true);
        });
    }
    window.open('./clearingReportView.aspx?type=clearing&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}

Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var ClearingModel = Ext.define('ClearingModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'memberID' },
               { name: 'sID' },
               { name: 'clientID' },
               { name: 'role' },
               { name: 'contractType' },
               { name: 'series' },
               { name: 'prevBuy' },
               { name: 'prevSell' },
               { name: 'openB' },
               { name: 'openS' },
               { name: 'closeB' },
               { name: 'closeS' },
               { name: 'liquidationB' },
               { name: 'liquidationS' },
               { name: 'exercise' },
               { name: 'assignment' },
               { name: 'netB' },
               { name: 'netS' },
               { name: 'premium' },
               { name: 'gainLoss' },
               { name: 'penalty' },
               { name: 'compensation' },
               { name: 'settlement' },
               { name: 'fee' }
            ]
    });

    var clearingReader = new Ext.data.reader.Json({
        model: 'ClearingModel'
    }, ClearingModel);

    var clearingStore = Ext.create('Ext.data.ArrayStore', {
        id: 'clearingStore',
        model: 'ClearingModel',
        data: clearingInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(clearingInitialData),
        autoLoad: true,
        autoSync: true,
        reader: clearingReader
    });

    // create the Grid    flex: 1
    var clearingGrid = Ext.create('Ext.grid.Panel', {
        store: clearingStore,
        id: 'clearingGrid',
        stateful: false,
        stateId: 'clearingGrid',
        columns: [
                { text: 'Member<br>ID', dataIndex: 'memberID', width: 50, sortable: true, align: 'center', draggable: false },
                { text: 'SID', dataIndex: 'sID', width: 110, sortable: true, align: 'left', draggable: false },
                { text: 'Role<br/>(C/H)', dataIndex: 'role', width: 40, sortable: true, align: 'center', draggable: false },
                { text: 'Contract ID', dataIndex: 'series', width: 150, sortable: true, align: 'left', draggable: false },
                { text: 'Contract<br/>Type(O/F)', dataIndex: 'contractType', width: 65, sortable: true, align: 'center', draggable: false },
                { text: 'Previous', columns: [
                    { text: 'Sell', dataIndex: 'prevSell', width: 50, sortable: true, align: 'right', draggable: false },
                    { text: 'Buy', dataIndex: 'prevBuy', width: 50, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Current', columns: [
                    { text: 'Open S', dataIndex: 'openS', width: 50, sortable: true, align: 'right', draggable: false },
                    { text: 'Open B', dataIndex: 'openB', width: 50, sortable: true, align: 'right', draggable: false },
                    { text: 'Close S', dataIndex: 'closeS', width: 50, sortable: true, align: 'right', draggable: false, hidden: true, hideable: true },
                    { text: 'Close B', dataIndex: 'closeB', width: 50, sortable: true, align: 'right', draggable: false, hidden: true, hideable: true }
                ]
                },
                { text: 'Liquidate', columns: [
                    { text: 'Sell', dataIndex: 'liquidationS', width: 50, sortable: true, align: 'right', draggable: false },
                    { text: 'Buy', dataIndex: 'liquidationB', width: 50, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Exercise', dataIndex: 'exercise', width: 50, sortable: true, align: 'right', draggable: false },
                { text: 'Assignment', dataIndex: 'assignment', width: 75, sortable: true, align: 'right', draggable: false },
                { text: 'Net', columns: [
                    { text: 'Sell', dataIndex: 'netS', width: 50, sortable: true, align: 'right', draggable: false },
                    { text: 'Buy', dataIndex: 'netB', width: 50, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Premium', dataIndex: 'premium', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Gain  /<br/>Loss', dataIndex: 'gainLoss', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Penalty', dataIndex: 'penalty', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Compensation', dataIndex: 'compensation', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Settlement', dataIndex: 'settlement', width: 100, sortable: true, align: 'right', draggable: false }
        ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom

        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: clearingStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })

    });

    clearingGrid.setLoading(false, false);
    clearingStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        //store: memberStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function(combo, value) {
                combo = Ext.getCmp('sidCombo');
                combo.clearValue();
                combo.getStore().removeAll();
                Ext.Ajax.request({
                    url: '../request.aspx?type=sid&memberId=' + Ext.getCmp('memberCombo').getValue(),
                    success: function (response, opts) {
                        var obj = eval('(' + response.responseText + ')');
                        for (i = 0; i < obj.length; i++) {
                            newData = eval('(' + obj[i].sidData + ')');
                            combo.getStore().add({ abbr: newData.abbr, name: newData.name });
                        }
                    },
                    failure: function (response, opts) {
                    }
                });
            }
            }
        }
    });

    memberCombo.on('select', function(box, record, index) {

    });

//    Ext.define('ContractId', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        //store: contractIdStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        //store: sidStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
                    { xtype: 'label', text: 'Contract ID: ' },
                    contractIdCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
                    { xtype: 'label', text: 'Contract Type: ' },
                    contractTypeCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function() {
                            subscribeclearingTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Clearing Result List',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: clearingGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });


    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function(str) {
    };
    var onconnect = function(frame) {
        Ext.getCmp('clearingGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function(message) {
        });

        subscribeclearingTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function() {
        if ((!(typeof clearingSubscriptionId === "undefined")) && (clearingSubscriptionId != null)) {
            client.unsubscribe(clearingSubscriptionId);
            clearingSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function() {
        });
    }
});
