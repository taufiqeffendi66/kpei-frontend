﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var priceSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var priceInitialData = [];
var arrPrice=[];
var endProses = true;
var dPop;
var pageSize = 20;
var count = 1;
var tick = 100;
function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addArrPrice(s, force) {
    data = eval('(' + s + ')');
    arrPrice.push(data);
    //Ext.getCmp('cId').setText(count += 1);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrPrice.length > 0) {
        endProses = false;
        if(arrPrice.length > pageSize)
            max = pageSize;
        else 
            max = arrPrice.length;

        for (xx = 0; xx < max; xx++ ) {
            dPop = arrPrice.shift()
            addToPrice(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToPrice(dt) {
    if (priceInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    if ((typeof dt.series === "undefined") || (dt.series == null)) {
        return;
    }

    //contractTypeCombo
/*    cb = Ext.getCmp('contractTypeCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', dt.contractType);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: dt.contractType, name: dt.contractType });
*/
    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', dt.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: dt.series, name: dt.series });

    //sidCombo
/*
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', dt.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: dt.sID });
*/
    //is data exists
    var recordIndex = -1;
    for (i = 0; i < priceInitialData.length; i++) {
        if (dt.series == priceInitialData[i].series) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        priceInitialData.push(dt);
    } else {
        priceInitialData[recordIndex] = dt;
    }
}

function addprice(s, force) {
    if (priceInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.series === "undefined") || (data.series == null)) {
        return;
    }

    //memberCombo
/*
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });
*/
    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
/*
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });
*/
    //is data exists
    var recordIndex = -1;
    for (i = 0; i < priceInitialData.length; i++) {
        if (data.series == priceInitialData[i].series) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        priceInitialData.push(data);
    } else {
        priceInitialData[recordIndex].series = data.series;
        priceInitialData[recordIndex].open_price = data.open_price;
        priceInitialData[recordIndex].last_price = data.last_price;
        priceInitialData[recordIndex].open_interest = data.open_interest;
        priceInitialData[recordIndex].hph = data.hph;
        priceInitialData[recordIndex].hpf = data.hpf;
//        priceInitialData[recordIndex].change = data.last_price - data.closing_price;  //data.change;
        priceInitialData[recordIndex].change = data.change;
        priceInitialData[recordIndex].contractType = data.contractType;
        priceInitialData[recordIndex].lastDayTransaction = data.lastDayTransaction; //maturityDate
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribepriceTopic(direction) {
    if ((!(typeof priceSubscriptionId === "undefined")) && (priceSubscriptionId != null)) {
        client.unsubscribe(priceSubscriptionId);
        priceSubscriptionId = null;
    }

    for (; priceInitialData.length > 0; )
        priceInitialData.pop();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";
/*
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
*/
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
/*
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
*/
    ct = Ext.getCmp('contractTypeCombo').getValue();
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var priceSubscritptionHeader = { 'selector': selector };
        priceSubscriptionId = client.subscribe(priceTopic, function(message) {
            addArrPrice(message.body, true);
        }, priceSubscritptionHeader);
    } else {
        priceSubscriptionId = client.subscribe(priceTopic, function(message) {
            addArrPrice(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=price&memberId=""&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=""&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=last&minId=""&maxId=""',
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrPrice(obj[ia].priceData, false);
            }
        },
        failure: function(response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof priceSubscriptionId === "undefined")) && (priceSubscriptionId != null)) {
        client.unsubscribe(priceSubscriptionId);
        priceSubscriptionId = null;
    }

    var selector = "";
/*
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
*/
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
/*
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
*/
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var priceSubscritptionHeader = { 'selector': selector };
        priceSubscriptionId = client.subscribe(priceTopic, function (message) {
            addprice(message.body, true);
        }, priceSubscritptionHeader);
    } else {
        priceSubscriptionId = client.subscribe(priceTopic, function (message) {
            addprice(message.body, true);
        });
    }
    window.open('./priceReportView.aspx?type=price&memberId=""&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=""&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}

Ext.onReady(function () {

    setInterval(function () { checkData() }, tick);

    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var priceModel = Ext.define('priceModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'series' },
               { name: 'contractType' },
               { name: 'open_price' },
               { name: 'last_price' },
               { name: 'open_interest' },
               { name: 'hph' },
               { name: 'hpf' },
               { name: 'change' },
               { name: 'lastDayTransaction' }
            ]
    });

    var priceReader = new Ext.data.reader.Json({
        model: 'priceModel'
    }, priceModel);

    var priceStore = Ext.create('Ext.data.Store', {
        id: 'priceStore',
        model: 'priceModel',
        data: priceInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(priceInitialData),
        autoLoad: true,
        autoSync: true,
        reader: priceReader
    });

    // create the Grid
    var priceGrid = Ext.create('Ext.grid.Panel', {
        store: priceStore,
        id: 'priceGrid',
        stateful: false,
        stateId: 'priceGrid',
        columns: [
                { text: 'Contract ID', dataIndex: 'series', flex: 1, sortable: true, align: 'left', draggable: false },
                { text: 'Contract<br/>Type (O/F)', dataIndex: 'contractType', width: 75, sortable: true, align: 'center', draggable: false },
                { text: 'Opening<br/>Price', dataIndex: 'open_interest', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Last<br/>Price', dataIndex: 'last_price', width: 100, sortable: true, align: 'right', draggable: false },
                { text: '+/-', dataIndex: 'change', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Maturity<br/>Date', dataIndex: 'lastDayTransaction', width: 100, sortable: true, align: 'center', draggable: false }
        ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: priceStore,
            pageSize: pageSize,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    priceGrid.setLoading(false, false);
    priceStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });
/*
    var memberStore = Ext.create('Ext.data.Store', {
        model: 'Member',
        data: members
    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function(combo, value) {
                var sidCombo = Ext.getCmp('sidCombo');
                sidCombo.clearValue();
                //TODO reload/filter sid combobox data
                //sidCombo.store.filter('cid', combo.getValue());
            }
            }
        }
    });

    memberCombo.on('select', function(box, record, index) {

    });
*/
    Ext.define('ContractId', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractIdStore = Ext.create('Ext.data.Store', {
        model: 'ContractId',
        data: contractIds
    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 110,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });
/*
    Ext.define('Sid', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var sidStore = Ext.create('Ext.data.Store', {
        model: 'Sid',
        data: sids
    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });
*/
    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: new Ext.data.SimpleStore({
                        fields: ['abbr','name'],
                        data: [["O", "Option"], ["F", "Future"]]
                        }),
        queryMode: 'local',
        typeAhead: true
    });
    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
        //{ xtype: 'label', text: 'Member ID: ' },
                    {xtype: 'label', text: 'Contract ID: ', id:'cId' },
                    contractIdCombo,
        //{ xtype: 'label', text: 'SID: ' },
                    {xtype: 'label', text: 'Contract Type: ' },
                    contractTypeCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function () {
                            Ext.getCmp('pagingToolbar').moveFirst();
                            subscribepriceTopic("last");
                            //combo = Ext.getCmp('contractTypeCombo');
                            //combo.clearValue();
                            //combo.getStore().removeAll();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Price Summary',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: priceGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function(str) {
    };
    var onconnect = function(frame) {
        Ext.getCmp('priceGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function(message) {
        });

        subscribepriceTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function() {
        if ((!(typeof priceSubscriptionId === "undefined")) && (priceSubscriptionId != null)) {
            client.unsubscribe(priceSubscriptionId);
            priceSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function() {
        });
    }
});
