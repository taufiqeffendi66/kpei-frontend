﻿<%@ Page Title="Settlement Price" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="settlementprice.aspx.cs" Inherits="imq.kpei.transaction.settlementprice" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">Settlement Price</div>
        <table width="100%">
            <tr>               
                <td align="right" width="100%"><dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" AutoPostBack="false"/></td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>Daily Settlement Price</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="gdDaily" runat="server" AutoGenerateColumns="false" CssClass="grid">
                                                <Columns>
                                                    <dx:GridViewBandColumn Caption="Sampling" VisibleIndex="0">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Contract ID" VisibleIndex="0" />
                                                            <dx:GridViewDataTextColumn Caption="Point" VisibleIndex="1" />                     
                                                            <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="2" />
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                                <Styles>
                                                    <Header CssClass="gridHeader" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="gvDailyMean" runat="server" AutoGenerateColumns="false" CssClass="grid">
                                                <Columns>
                                                    <dx:GridViewBandColumn Caption="Mean" VisibleIndex="0">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Contract ID" VisibleIndex="0" />
                                                            <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="1" />                     
                                                            <dx:GridViewDataTextColumn Caption="" VisibleIndex="2" />
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                                <Styles>
                                                    <Header CssClass="gridHeader" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                           <td>
                                  <table>
                                    <tr>
                                        <td>Final Settlement Price</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="gvFinal" runat="server" AutoGenerateColumns="false" CssClass="grid">
                                                <Columns>
                                                    <dx:GridViewBandColumn Caption="Sampling" VisibleIndex="0">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Contract ID" VisibleIndex="0" />
                                                            <dx:GridViewDataTextColumn Caption="Point" VisibleIndex="1" />                     
                                                            <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="2" />
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                                <Styles>
                                                    <Header CssClass="gridHeader" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="gvFinalMean" runat="server" AutoGenerateColumns="false" CssClass="grid">
                                                <Columns>
                                                    <dx:GridViewBandColumn Caption="Mean" VisibleIndex="0">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Contract ID" VisibleIndex="0" />
                                                            <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="1" />                     
                                                            <dx:GridViewDataTextColumn Caption="" VisibleIndex="2" />
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                                <Styles>
                                                    <Header CssClass="gridHeader" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                           </td>
                        </tr>
                    </table>
                </td>
            </tr>                            
        </table>

    </div>
    <br />
</asp:Content>
