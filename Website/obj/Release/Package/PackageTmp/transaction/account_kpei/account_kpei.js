﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var accountSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var accountInitialData = [];
var arrAccount = [];
var endProses = true;
var pageSize = 20;
var tick = 200;

function addArrAccount(s, force) {
    data = eval('(' + s + ')');
    arrAccount.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrAccount.length > 0) {
        endProses = false;
        if (arrAccount.length > pageSize)
            max = pageSize;
        else
            max = arrAccount.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrAccount.shift()
            addToAccount(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToAccount(data) {
    if (accountInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.bankCode === "undefined") || (data.bankCode == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });


    //is data exists
    var recordIndex = -1;
    for (i = 0; i < accountInitialData.length; i++) {
        if (data.bankCode == accountInitialData[i].bankCode) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        accountInitialData.push(data);
    } else {
        accountInitialData[recordIndex].bankCode = data.bankCode;
        accountInitialData[recordIndex].bankName = data.bankName;
        accountInitialData[recordIndex].settlementAccNo = data.settlementAccNo;
        accountInitialData[recordIndex].settlementAccValue = data.settlementAccValue;
        accountInitialData[recordIndex].penaltyAccNo = data.penaltyAccNo;
        accountInitialData[recordIndex].penaltyAccValue = data.penaltyAccValue;
        accountInitialData[recordIndex].guaranteeAccNo = data.guaranteeAccNo;
        accountInitialData[recordIndex].guaranteeAccValue = data.guaranteeAccValue;
        accountInitialData[recordIndex].feeAccNo = data.feeAccNo;
        accountInitialData[recordIndex].feeAccValue = data.feeAccValue;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}

function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addaccount(s, force) {
    if (accountInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.bankCode === "undefined") || (data.bankCode == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });


    //is data exists
    var recordIndex = -1;
    for (i = 0; i < accountInitialData.length; i++) {
        if (data.bankCode == accountInitialData[i].bankCode) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        accountInitialData.push(data);
    } else {
        accountInitialData[recordIndex].bankCode = data.bankCode;
        accountInitialData[recordIndex].bankName = data.bankName;
        accountInitialData[recordIndex].settlementAccNo = data.settlementAccNo;
        accountInitialData[recordIndex].settlementAccValue = data.settlementAccValue;
        accountInitialData[recordIndex].penaltyAccNo = data.penaltyAccNo;
        accountInitialData[recordIndex].penaltyAccValue = data.penaltyAccValue;
        accountInitialData[recordIndex].guaranteeAccNo = data.guaranteeAccNo;
        accountInitialData[recordIndex].guaranteeAccValue = data.guaranteeAccValue;
        accountInitialData[recordIndex].feeAccNo = data.feeAccNo;
        accountInitialData[recordIndex].feeAccValue = data.feeAccValue;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeaccountTopic(direction) {
    if ((!(typeof accountSubscriptionId === "undefined")) && (accountSubscriptionId != null)) {
        client.unsubscribe(accountSubscriptionId);
        accountSubscriptionId = null;
    }

    for (; accountInitialData.length > 0; )
        accountInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";

    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var accountSubscritptionHeader = { 'selector': selector };
        accountSubscriptionId = client.subscribe(accountTopic, function(message) {
            addArrAccount(message.body, true);
        }, accountSubscritptionHeader);
    } else {
        accountSubscriptionId = client.subscribe(accountTopic, function(message) {
            addArrAccount(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=account_kpei&memberId=""&contractId=""&sid=""&contractType=""',
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrAccount(obj[ia].accountData, false);
            }
        },
        failure: function(response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof accountSubscriptionId === "undefined")) && (accountSubscriptionId != null)) {
        client.unsubscribe(accountSubscriptionId);
        accountSubscriptionId = null;
    }

    var selector = "";

    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var accountSubscritptionHeader = { 'selector': selector };
        accountSubscriptionId = client.subscribe(accountTopic, function (message) {
            addArrAccount(message.body, true);
        }, accountSubscritptionHeader);
    } else {
        accountSubscriptionId = client.subscribe(accountTopic, function (message) {
            addArrAccount(message.body, true);
        });
    }
    window.open('./accountKpeiReportView.aspx?type=accountKpei&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}

Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var accountModel = Ext.define('accountModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'bankCode' },
               { name: 'bankName' },
               { name: 'settlementAccNo' },
               { name: 'settlementAccValue' },
               { name: 'penaltyAccNo' },
               { name: 'penaltyAccValue' },
               { name: 'guaranteeAccNo' },
               { name: 'guaranteeAccValue' },
               { name: 'feeAccNo' },
               { name: 'feeAccValue' }
            ]
    });

    var accountReader = new Ext.data.reader.Json({
        model: 'accountModel'
    }, accountModel);

    var accountStore = Ext.create('Ext.data.ArrayStore', {
        id: 'accountStore',
        model: 'accountModel',
        data: accountInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(accountInitialData),
        autoLoad: true,
        autoSync: true,
        reader: accountReader
    });

    // create the Grid
    var accountGrid = Ext.create('Ext.grid.Panel', {
        store: accountStore,
        id: 'accountGrid',
        stateful: false,
        stateId: 'accountGrid',
        columns: [
                { text: 'Bank', columns: [
                    { text: 'Code', dataIndex: 'bankCode', width: 100, sortable: true, align: 'left', draggable: false },
                    { text: 'Name', dataIndex: 'bankName', width: 200, sortable: true, align: 'left', draggable: false }
                ]
                },
                { text: 'Settlement', columns: [
                    { text: 'Account. No', dataIndex: 'settlementAccNo', width: 110, sortable: true, align: 'left', draggable: false },
                    { text: 'Value', dataIndex: 'settlementAccValue', width: 110, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Penalty', columns: [
                    { text: 'Account. No', dataIndex: 'penaltyAccNo', width: 100, sortable: true, align: 'left', draggable: false },
                    { text: 'Value', dataIndex: 'penaltyAccValue', width: 100, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Guarantee Fund', columns: [
                    { text: 'Account. No', dataIndex: 'guaranteeAccNo', width: 100, sortable: true, align: 'left', draggable: false },
                    { text: 'Value', dataIndex: 'guaranteeAccValue', width: 100, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Fee', columns: [
                    { text: 'Account. No', dataIndex: 'feeAccNo', width: 100, sortable: true, align: 'left', draggable: false },
                    { text: 'Value', dataIndex: 'feeAccValue', width: 100, sortable: true, align: 'right', draggable: false }
                ]
                }
        ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: accountStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    accountGrid.setLoading(false, false);
    accountStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        //store: memberStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function(combo, value) {
                var sidCombo = Ext.getCmp('sidCombo');
                sidCombo.clearValue();
                //TODO reload/filter sid combobox data
                //sidCombo.store.filter('cid', combo.getValue());
            }
            }
        }
    });

    memberCombo.on('select', function(box, record, index) {

    });

//    Ext.define('ContractId', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        //store: contractIdStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        //store: sidStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
        //            { xtype: 'label', text: 'Member ID: ' },
        //            memberCombo,
        //                    { xtype: 'label', text: 'Contract ID: ' },
        //                    contractIdCombo,
        //                    { xtype: 'label', text: 'SID: ' },
        //                    sidCombo,
        //                    { xtype: 'label', text: 'Contract Type: ' },
        //                    contractTypeCombo,
                    {xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
        //            { xtype: 'button', text: ' Search ',
        //                handler: function() {
        //                    subscribeaccountTopic("last");
        //                    Ext.getCmp('pagingToolbar').moveFirst();
        //                }
        //            },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Account - KPEI',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: accountGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function(str) {
    };
    var onconnect = function(frame) {
        Ext.getCmp('accountGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function(message) {
        });

        subscribeaccountTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function() {
        if ((!(typeof accountSubscriptionId === "undefined")) && (accountSubscriptionId != null)) {
            client.unsubscribe(accountSubscriptionId);
            accountSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function() {
        });
    }
});
