﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="settlementPrice.aspx.cs" Inherits="imq.kpei.transaction.settlementPrice.settlementPrice" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[

        function ConfirmDelete(s, e) {
            e.processOnServer = confirm("Are you sure Delete this data ?");
        }

        function init() {
            btnRemove.SetEnabled(false);
            btnEdit.SetEnabled(false);
        }

        window.onload = init;
        // ]]>
    </script>
    <div id="content">
        <div>      
            <table>
                <tr><td colspan="2" class="title">Settlement Price</td></tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td><asp:FileUpload ID="FileUploadHPH" runat="server"/></td>
                                            <td><dx:ASPxButton ID="btnUploadHPH" runat="server" Text="Upload" onclick="btnUploadHPH_Click"> </dx:ASPxButton></td>
                                            <td><dx:ASPxButton ID="btnEditHPH" runat="server" Text="Edit" onclick="btnEditHPH_Click"> </dx:ASPxButton></td>                    
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gridDaily" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" Width="320px" CssClass="grid">
                                        <Columns>
                                            <dx:GridViewBandColumn VisibleIndex="0" Caption="Daily Settlement Price">
                                                <Columns>
                                                    <dx:GridViewBandColumn Caption="Sampling" ShowInCustomizationForm="True" VisibleIndex="0">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Sequence" FieldName="sequence" ShowInCustomizationForm="True" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Contract ID" FieldName="series" ShowInCustomizationForm="True" VisibleIndex="1" GroupIndex="0" SortIndex="0" SortOrder="Ascending"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Point" FieldName="id" Width="30px" ShowInCustomizationForm="True" VisibleIndex="2"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Value" FieldName="price" Width="85px" ShowInCustomizationForm="True" VisibleIndex="3"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Time" FieldName="datetime" Width="85px" ShowInCustomizationForm="True" VisibleIndex="4" Visible="true"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                            </dx:GridViewBandColumn>
                                        </Columns>
                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                                        <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="250" />
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gridMeanDaily" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" Width="320px" CssClass="grid">
                                        <Columns>
                                            <dx:GridViewBandColumn VisibleIndex="0" Caption="Mean">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Contract ID" FieldName="series" ShowInCustomizationForm="True" VisibleIndex="1" Width="225"></dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Value" FieldName="value" ShowInCustomizationForm="True" VisibleIndex="1" Width="75px"></dx:GridViewDataTextColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                        </Columns>
                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                                        <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="250" />
                                    </dx:ASPxGridView> 
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td><asp:FileUpload ID="FileUploadHPF" runat="server"/></td>
                                            <td><dx:ASPxButton ID="btnUploadHPF" runat="server" Text="Upload" onclick="btnUploadHPF_Click"> </dx:ASPxButton></td>
                                            <td><dx:ASPxButton ID="btnEditHPF" runat="server" Text="Edit" onclick="btnEditHPF_Click"> </dx:ASPxButton></td>                    
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gridFinal" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" Width="320px" CssClass="grid">
                                        <Columns>
                                            <dx:GridViewBandColumn VisibleIndex="0" Caption="Final Settlement Price">
                                                <Columns>
                                                    <dx:GridViewBandColumn Caption="Sampling" ShowInCustomizationForm="True" VisibleIndex="0">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Sequence" FieldName="sequence" ShowInCustomizationForm="True" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Contract ID" FieldName="series" ShowInCustomizationForm="True" VisibleIndex="1" GroupIndex="0" SortIndex="0" SortOrder="Ascending"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Point" FieldName="id" Width="30px" ShowInCustomizationForm="True" VisibleIndex="2"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Value" FieldName="price" Width="85px" ShowInCustomizationForm="True" VisibleIndex="3"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Time" FieldName="datetime" Width="85px" ShowInCustomizationForm="True" VisibleIndex="4" Visible="true"><HeaderStyle HorizontalAlign="Center" /></dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                            </dx:GridViewBandColumn>
                                        </Columns>
                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                                        <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="250" />
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gridMeanFinal" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" Width="320px" CssClass="grid">
                                        <Columns>
                                            <dx:GridViewBandColumn VisibleIndex="0" Caption="Mean">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Contract ID" FieldName="series" ShowInCustomizationForm="True" VisibleIndex="1" Width="225"></dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Value" FieldName="value" ShowInCustomizationForm="True" VisibleIndex="1" Width="75px"></dx:GridViewDataTextColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                        </Columns>
                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                                        <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="250" />
                                    </dx:ASPxGridView> 
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>            
        </div>
    </div>

</asp:Content>

