﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var assignmentSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var assignmentInitialData = [];
var cb;
var storeCb;
var recordIndex = -1;

var arrAssignment = [];
var endProses = true;
var pageSize = 20;
var tick = 200;

function addArrAssignment(s, force) {
    data = eval('(' + s + ')');
    arrAssignment.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrAssignment.length > 0) {
        endProses = false;
        if (arrAssignment.length > pageSize)
            max = pageSize;
        else
            max = arrAssignment.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrAssignment.pop()
            addToAssignment(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToAssignment(data) {
    if (assignmentInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.assignmentNo === "undefined") || (data.assignmentNo == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sellMemberId);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sellMemberId, name: data.sellMemberId });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.assignmentSid);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.assignmentSid, name: data.assignmentSid });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < assignmentInitialData.length; i++) {
        if (data.assignmentNo == assignmentInitialData[i].assignmentNo) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        assignmentInitialData.push(data);
    } else {
        assignmentInitialData[recordIndex].assignmentNo = data.assignmentNo;
        assignmentInitialData[recordIndex].assignmentRole = data.assignmentRole;
        assignmentInitialData[recordIndex].assignmentSid = data.assignmentSid;
        assignmentInitialData[recordIndex].assignmentVolume = data.assignmentVolume;
        assignmentInitialData[recordIndex].buyMemberId = data.buyMemberId;
        assignmentInitialData[recordIndex].dataTime = data.dataTime;
        assignmentInitialData[recordIndex].exerciseNo = data.exerciseNo;
        assignmentInitialData[recordIndex].exeRole = data.exeRole;
        assignmentInitialData[recordIndex].exeSid = data.exeSid;
        assignmentInitialData[recordIndex].exeVolume = data.exeVolume;
        assignmentInitialData[recordIndex].sellMemberId = data.sellMemberId;
        assignmentInitialData[recordIndex].series = data.series;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}


function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addassignment(s, force) {
    if (assignmentInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.assignmentNo === "undefined") || (data.assignmentNo == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sellMemberId);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sellMemberId, name: data.sellMemberId });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.assignmentSid);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.assignmentSid, name: data.assignmentSid });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < assignmentInitialData.length; i++) {
        if (data.assignmentNo == assignmentInitialData[i].assignmentNo) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        assignmentInitialData.push(data);
    } else {
        assignmentInitialData[recordIndex].assignmentNo = data.assignmentNo;
        assignmentInitialData[recordIndex].assignmentRole = data.assignmentRole;
        assignmentInitialData[recordIndex].assignmentSid = data.assignmentSid;
        assignmentInitialData[recordIndex].assignmentVolume = data.assignmentVolume;
        assignmentInitialData[recordIndex].buyMemberId = data.buyMemberId;
        assignmentInitialData[recordIndex].dataTime = data.dataTime;
        assignmentInitialData[recordIndex].exerciseNo = data.exerciseNo;
        assignmentInitialData[recordIndex].exeRole = data.exeRole;
        assignmentInitialData[recordIndex].exeSid = data.exeSid;
        assignmentInitialData[recordIndex].exeVolume = data.exeVolume;
        assignmentInitialData[recordIndex].sellMemberId = data.sellMemberId;
        assignmentInitialData[recordIndex].series = data.series;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeassignmentTopic(direction) {
    if ((!(typeof assignmentSubscriptionId === "undefined")) && (assignmentSubscriptionId != null)) {
        client.unsubscribe(assignmentSubscriptionId);
        assignmentSubscriptionId = null;
    }

    for (; assignmentInitialData.length > 0; )
        assignmentInitialData.pop();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var assignmentSubscritptionHeader = { 'selector': selector };
        assignmentSubscriptionId = client.subscribe(assignmentTopic, function (message) {
            addArrAssignment(message.body, true);
        }, assignmentSubscritptionHeader);
    } else {
        assignmentSubscriptionId = client.subscribe(assignmentTopic, function (message) {
            addArrAssignment(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=assignment&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue(),
        success: function (response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrAssignment(obj[ia].assignmentData, false);
            }
        },
        failure: function (response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof assignmentSubscriptionId === "undefined")) && (assignmentSubscriptionId != null)) {
        client.unsubscribe(assignmentSubscriptionId);
        assignmentSubscriptionId = null;
    }

    for (; assignmentInitialData.length > 0; )
        assignmentInitialData.pop();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var assignmentSubscritptionHeader = { 'selector': selector };
        assignmentSubscriptionId = client.subscribe(assignmentTopic, function (message) {
            addArrAssignment(message.body, true);
        }, assignmentSubscritptionHeader);
    } else {
        assignmentSubscriptionId = client.subscribe(assignmentTopic, function (message) {
            addArrAssignment(message.body, true);
        });
    }
    window.open('./assigmentReportView.aspx?type=assigment&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}

Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var assignmentModel = Ext.define('assignmentModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'assignmentNo' },
               { name: 'exerciseNo' },
               { name: 'buyMemberId' },
               { name: 'exeRole' },
               { name: 'exeSid' },
               { name: 'exeVolume' },
               { name: 'dataTime' },
               { name: 'sellMemberId' },
               { name: 'assignmentRole' },
               { name: 'assignmentSid' },
               { name: 'series' },
               { name: 'assignmentVolume' }
            ]
    });

    var assignmentReader = new Ext.data.reader.Json({
        model: 'assignmentModel'
    }, assignmentModel);

    var assignmentStore = Ext.create('Ext.data.Store', {
        id: 'assignmentStore',
        model: 'assignmentModel',
        data: assignmentInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(assignmentInitialData),
        autoLoad: true,
        autoSync: true,
        reader: assignmentReader
    });

    // create the Grid
    var assignmentGrid = Ext.create('Ext.grid.Panel', {
        store: assignmentStore,
        id: 'assignmentGrid',
        stateful: false,
        stateId: 'assignmentGrid',
        columns: [
                new Ext.grid.RowNumberer({ header: "No" }),
                { text: 'Exercise', dataIndex: 'exercise', width: 55, sortable: false, align: 'right', draggable: false },
                { text: 'Time', dataIndex: 'dataTime', width: 60, sortable: false, align: 'center', draggable: false },
                { text: 'Assignment', columns: [
                    { text: 'Sell Member ID', dataIndex: 'sellMemberId', width: 100, sortable: false, align: 'left', draggable: false },
                    { text: 'Role', dataIndex: 'assignmentRole', width: 40, sortable: false, align: 'center', draggable: false },
                    { text: 'SID', dataIndex: 'assignmentSid', width: 120, sortable: false, align: 'left', draggable: false },
                    { text: 'Contract ID', dataIndex: 'series', width: 120, sortable: false, align: 'left', draggable: false },
                    { text: 'Quantity', dataIndex: 'assignmentVolume', width: 55, sortable: false, align: 'right', draggable: false }
                ]
                }
            ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: assignmentStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    assignmentGrid.setLoading(false, false);
    assignmentStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function (combo, value) {
                combo = Ext.getCmp('sidCombo');
                combo.clearValue();
                combo.getStore().removeAll();
                Ext.Ajax.request({
                    url: '../request.aspx?type=sid&memberId=' + Ext.getCmp('memberCombo').getValue(),
                    success: function (response, opts) {
                        var obj = eval('(' + response.responseText + ')');
                        for (i = 0; i < obj.length; i++) {
                            newData = eval('(' + obj[i].sidData + ')');
                            combo.getStore().add({ abbr: newData.abbr, name: newData.name });
                        }
                    },
                    failure: function (response, opts) {
                    }
                });
            }
            }
        }
    });

    memberCombo.on('select', function (box, record, index) {

    });

//    Ext.define('ContractId', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
                    { xtype: 'label', text: 'Contract ID: ' },
                    contractIdCombo,
        //{xtype: 'label', text: 'Contract Type: ' },
        //contractTypeCombo,
                    {xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function () {
                            subscribeassignmentTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                            subscribeassignmentTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Assignment List',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: assignmentGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function (str) {
    };
    var onconnect = function (frame) {
        Ext.getCmp('assignmentGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function (message) {
        });

        subscribeassignmentTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function () {
        if ((!(typeof assignmentSubscriptionId === "undefined")) && (assignmentSubscriptionId != null)) {
            client.unsubscribe(assignmentSubscriptionId);
            assignmentSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function () {
        });
    }
});
