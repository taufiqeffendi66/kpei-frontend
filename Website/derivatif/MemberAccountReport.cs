﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using DevExpress.XtraReports.UI;
using Common;
using Common.memberAccountWS;

namespace imq.kpei.derivatif
{
    public partial class MemberAccountReport : DevExpress.XtraReports.UI.XtraReport
    {
        private memberAccount[] listmember;

        public String aMember;
        public String aBank;
        public String aSID;

        public MemberAccountReport()
        {
            InitializeComponent();
        }

        private void GetMemberAccount()
        {
            MemberAccountWSService mcas = null;
            try
            {
                mcas = ImqWS.getMemberAccountService();
                listmember = mcas.Search(aBank, aMember, aSID);
                if (listmember != null)
                {
                    foreach (memberAccount ma in listmember)
                    {
                        if (ma.status.ToLower() == "a") ma.status = "Active";
                        if (ma.status.ToLower() == "c") ma.status = "Default by SD";
                        if (ma.status.ToLower() == "d") ma.status = "Default by GF";
                    }
                    DataSource = listmember;

                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "memberId");
                    cellMember.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "memberInfo.memberName");
                    cellName.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sid");
                    cellSID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tradingId");
                    cellTradingID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "accountId");
                    cellAccount.DataBindings.Add(binding);
                    //binding = new XRBinding("Text", DataSource, "role");
                    //cellRole.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "bank.bankName");
                    cellBank.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "securityDepositAccountNo");
                    cellSecurity.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "collateralAccountNo");
                    cellCollateral.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "settlementAccountNo");
                    cellSettlement.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "freeAccountNo");
                    cellFree.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "status");
                    cellStatus.DataBindings.Add(binding);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                mcas.Abort();
            }
            finally
            {
                mcas.Dispose();
            }
        }

        private void MemberAccountReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetMemberAccount();
        }
    }
}
