﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using DevExpress.XtraReports.UI;
using Common;
using Common.contractWS;

namespace imq.kpei.derivatif
{
    public partial class SeriesReport : DevExpress.XtraReports.UI.XtraReport
    {
        private series[] listSeries;
        public String aSeries;
        public String aUnderlying;

        public SeriesReport()
        {
            InitializeComponent();
        }

        private void GetSeries()
        {
            ContractWSService cws = null;
            try
            {
                cws = ImqWS.GetContractService();
                listSeries = cws.Search(aSeries, aUnderlying);
                if (listSeries != null)
                {
                    DataSource = listSeries;
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "seriesCode");
                    cellCode.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "underlyingCode");
                    cellUnderlying.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "seriesType");
                    cellType.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "startDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellStart.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "endDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellEnd.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "period");
                    cellPeriod.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "strikePrice");
                    cellPrice.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "optionType");
                    cellOption.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "settlementMethod");
                    cellMethod.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "lastDayTransaction");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellLastDay.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "multiplier");
                    cellMultiplier.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "exerciseTimeFrom");
                    binding.FormatString = "{0:HH:mm}";
                    cellTimeFrom.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "exerciseTimeTo");
                    binding.FormatString = "{0:HH:mm}";
                    cellTimeTo.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "exerciseStyleDesc");
                    cellStyle.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "marginingType");
                    cellMargining.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "description");
                    cellDescription.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "openinterest");
                    cellOpenPrice.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "seriesStatus");
                    cellSeriesStatus.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "instrumentCode");
                    cellInstrument.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "remark");
                    cellRemark.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "remark2");
                    cellRemark2.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                if (cws != null)
                    cws.Abort();

                Debug.WriteLine(e.Message);
            }
            finally
            {
                cws.Dispose();
            }
        }

        private void SeriesReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetSeries();
        }
    }
}
