﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using DevExpress.Web.ASPxClasses;
using Common;
using Common.kpeiAccountWS;
using Common.bankWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class KpeiAccountEdit : System.Web.UI.Page
    {
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private kpeiAccountTemp aKpeiTemp;
        private const String module = "Kpei Account";
        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                //btnSave.Visible = false;

                if (Session["stat"] == null)
                {
                    Response.Redirect("KpeiAccountPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                FillBank();
                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = String.Format("New {0} Number", module); //New Direct
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lblTitle.Text = String.Format("Edit {0} Number", module); //Edit Direct
                        cmbBank.Enabled = false;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = String.Format("New {0} Number", module); //New From Temporary
                        Permission(false);

                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3:
                        lblTitle.Text = String.Format("Edit {0} Number", module);
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 4:
                        lblTitle.Text = String.Format("Delete {0} Number", module);
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }
            }
        }

        private void DisabledControl()
        {
            cmbBank.Enabled = false;
            txtFeeAccNo.Enabled = false;
            txtGuaranteeAccNo.Enabled = false;
            txtPenaltyAccNo.Enabled = false;
            txtSettlementAccNo.Enabled = false;
        }

        private void GetTemporary()
        {
            KpeiAccountWSService kas = null;
            try
            {
                kas = ImqWS.GetKpeiAccountService();
                aKpeiTemp = kas.getTemp(int.Parse(Request.QueryString["idxTmp"]));
                cmbBank.Text = aKpeiTemp.bankCode;
                txtFeeAccNo.Text = aKpeiTemp.feeAccountNo;
                txtGuaranteeAccNo.Text = aKpeiTemp.guaranteeFundAccountNo;
                txtPenaltyAccNo.Text = aKpeiTemp.penaltyAccountNo;
                txtSettlementAccNo.Text = aKpeiTemp.settlementAccountNo;
            }
            catch (Exception e)
            {
                if (kas != null)
                    kas.Abort();

                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                kas.Dispose();
            }
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            KpeiAccountWSService kas = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                kas = ImqWS.GetKpeiAccountService();

                dtA = aps.Search(Request.QueryString["reffNo"]);
                aKpeiTemp = kas.getTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }

                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New  {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = kas.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = kas.deleteTempById(aKpeiTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = kas.updateFromTemp(aKpeiTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = kas.deleteTempById(aKpeiTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = kas.deleteById(dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = kas.deleteTempById(aKpeiTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();
                if (kas != null)
                    kas.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
                if (kas != null)
                    kas.Dispose();
            }
        }

        private void FillBank()
        {
            BankWSService bws = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                Common.bankWS.bank[] lbank = bws.get();
                foreach (Common.bankWS.bank b in lbank)
                {
                    cmbBank.Items.Add(b.name.Trim(), b.bankCode.Trim());
                }
            }
            catch (Exception e)
            {
                if (bws != null)
                    bws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                if (bws != null)
                    bws.Dispose();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            string bankCode = Server.UrlDecode(Request.QueryString["bankCode"].Trim());
            string settlementAccountNo = Server.UrlDecode(Request.QueryString["settlementAccountNo"].Trim());
            string penaltyAccountNo = Server.UrlDecode(Request.QueryString["penaltyAccountNo"].Trim());
            string guaranteeFundAccountNo = Server.UrlDecode(Request.QueryString["guaranteeFundAccountNo"].Trim());
            string feeAccountNo = Server.UrlDecode(Request.QueryString["feeAccountNo"].Trim());
            string idx = Server.UrlDecode(Request.QueryString["idx"].Trim());
            hfKpeiAcc.Set("bankCode", bankCode);
            hfKpeiAcc.Set("idx", idx);
            cmbBank.Text = bankCode;
            txtSettlementAccNo.Text = settlementAccountNo;
            txtPenaltyAccNo.Text = penaltyAccountNo;
            txtGuaranteeAccNo.Text = guaranteeFundAccountNo;
            txtFeeAccNo.Text = feeAccountNo;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            KpeiAccountWSService kas = null;
            ApprovalService aps = null;

            string ret = String.Empty;
            try
            {
                if (cmbBank.Items.FindByText(cmbBank.Text.Trim()) != null)
                {
                    kas = ImqWS.GetKpeiAccountService();
                    aps = ImqWS.GetApprovalWebService();

                    addedit = (int)Session["stat"];
                    int idRec;

                    kpeiAccountTemp kat = new kpeiAccountTemp() { bankCode = cmbBank.SelectedItem.Value.ToString(), feeAccountNo = txtFeeAccNo.Text, guaranteeFundAccountNo = txtGuaranteeAccNo.Text, penaltyAccountNo = txtPenaltyAccNo.Text, settlementAccountNo = txtSettlementAccNo.Text };


                    idRec = kas.addToTemp(kat);
                    aKpeiTemp = kat;
                    if (idRec > -1)
                    {
                        dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To be Check", idxTmp = idRec, form = "~/derivatif/KpeiAccountEdit.aspx", status = "M", memberId = aUserMember };
                        int idTable;
                        switch (addedit)
                        {
                            case 0:
                                dtA.topic = "Add " + module;
                                dtA.insertEdit = "I";
                                aKpeiTemp.idx = idRec;
                                break;

                            case 1:
                                dtA.topic = "Edit " + module;
                                dtA.insertEdit = "E";
                                idTable = int.Parse(hfKpeiAcc.Get("idx").ToString());
                                dtA.idTable = idTable;
                                dtA.idxTmp = idRec;
                                break;

                            case 4:
                                dtA.topic = "Delete " + module;
                                dtA.insertEdit = "R";
                                idTable = int.Parse(hfKpeiAcc.Get("idx").ToString());
                                dtA.idTable = idTable;
                                dtA.idxTmp = idRec;
                                break;
                        }
                        string retVal = "0";

                        retVal = rblApprovalChanged(dtA, aps, aKpeiTemp);
                        if (!retVal.Equals("0"))
                        {
                            ShowMessage(retVal, @"../derivatif/KpeiAccountPage.aspx");
                        }
                    }
                    else
                    {
                        ShowMessage("Failed On Save To Temporary Kpei Account Number", @"../derivatif/KpeiAccountPage.aspx");
                    }
                }
                else
                    lblError.Text = String.Format("Bank {0} doesn't exist", cmbBank.Text.Trim());
            }
            catch (Exception ex)
            {
                ex.ToString();
                if (kas != null)
                    kas.Abort();
                if (aps != null)
                    aps.Abort();

                lblError.Text = ex.Message;
            }
            finally
            {
                if (kas != null)
                    kas.Dispose();
                if (aps != null)
                    aps.Dispose();
            }
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, kpeiAccountTemp kat)
        {
            string ret = "0";
            string target = @"../derivatif/KpeiAccountPage.aspx";
            if (!kpeiAccountTemp.ReferenceEquals(aKpeiTemp, kat))
                aKpeiTemp = kat;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    KpeiAccountWSService kas = ImqWS.GetKpeiAccountService();

                    kpeiAccount ka = new kpeiAccount() { bankCode = aKpeiTemp.bankCode, feeAccountNo = aKpeiTemp.feeAccountNo, guaranteeFundAccountNo = aKpeiTemp.guaranteeFundAccountNo, penaltyAccountNo = aKpeiTemp.penaltyAccountNo, settlementAccountNo = aKpeiTemp.settlementAccountNo };

                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = kas.add(ka);
                            if (ret.Equals("0"))
                            {
                                WriteAuditTrail("Direct Approval New " + module);
                                wsA.AddDirectApproval(dtA);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "E":
                            ret = kas.update(hfKpeiAcc.Get("bankCode").ToString(), ka);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "R":
                            ret = kas.delete(hfKpeiAcc.Get("bankCode").ToString());
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Delete " + module);
                                ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }
                    break;
            }

            return ret;
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aKpeiTemp));
        }

        private static String Log(kpeiAccountTemp kat)
        {
            string strLog = "[Bank Code : " + kat.bankCode +
            ", Fee Account No : " + kat.feeAccountNo +
            ", Guarantee Funn Account No : " + kat.guaranteeFundAccountNo +
            ", Penalty Account No : " + kat.penaltyAccountNo +
            ", Settlement Account No : " + kat.settlementAccountNo +
            "]";
            return strLog;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("KpeiAccountPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }
    }
}
