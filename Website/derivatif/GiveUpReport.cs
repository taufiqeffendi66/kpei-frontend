﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.giveUpWS;

namespace imq.kpei.derivatif
{
    public partial class GiveUpReport : DevExpress.XtraReports.UI.XtraReport
    {
        private giveUp[] listGiveUp;
        public String aMember;
        public GiveUpReport()
        {
            InitializeComponent();
        }

        private void GetGiveUp()
        {
            GiveUpWSService gws = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                listGiveUp = gws.Search(aMember);
                if (listGiveUp != null)
                {
                    DataSource = listGiveUp;
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "guDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "guDate");
                    binding.FormatString = "{0:HH:mm}";
                    cellTime.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "guNo");
                    cellNo.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "guMemberId");
                    cellGuMemberID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tuMemberId");
                    cellTuMemberID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tuApprovalDescription");
                    cellTuApproval.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "kpeiApprovalDescription");
                    cellKpeiApproval.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "executionTime");
                    binding.FormatString = "{0:dd/MM/yyyy HH:mm}";
                    cellExecution.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "status");
                    cellStatus.DataBindings.Add(binding);
                }
            }
            catch (Exception ex)
            {
                if (gws != null)
                    gws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                gws.Dispose();
            }
        }

        private void GiveUpReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetGiveUp();
        }
    }
}
