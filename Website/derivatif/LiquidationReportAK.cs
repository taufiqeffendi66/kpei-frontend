﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.liquidationWS;
using System.Web;

namespace imq.kpei.derivatif
{
    public partial class LiquidationReportAK : DevExpress.XtraReports.UI.XtraReport
    {
        private liquidation[] listLiquidation;
        public String aUserMember;
        public String aMember;
        public String aSID;
        public String aSeries;

        public LiquidationReportAK()
        {
            InitializeComponent();
        }

        private void GetLiquidation()
        {
            LiquidationWSService lws = null;
            try
            {
                lws = ImqWS.GetLiquidationService();
                //string aMember = aUserMember.ToLower();
                //if (aMember.Contains("kpei"))
                //    listLiquidation = lws.get();
                //else
                //    listLiquidation = lws.getByMemberID(aUserMember);
                listLiquidation = lws.Search(aSeries, aMember, aSID);
                if (listLiquidation != null)
                {
                    DataSource = listLiquidation;
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "role");
                    cellRole.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sid");
                    cellSID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "pos");
                    cellPos.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.liquidationDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.liquidationDate");
                    binding.FormatString = "{0:HH:mm}";
                    cellTime.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "series");
                    cellSeries.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "contractType");
                    cellType.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "quantity");
                    cellQuantity.DataBindings.Add(binding);
                }
            }
            catch (Exception ex)
            {
                lws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                lws.Dispose();
            }
        }

        private void LiquidationReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetLiquidation();
        }
    }
}
