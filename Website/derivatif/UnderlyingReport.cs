﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using DevExpress.XtraReports.UI;
using Common;
using Common.underlyingWS;

namespace imq.kpei.derivatif
{
    public partial class UnderlyingReport : DevExpress.XtraReports.UI.XtraReport
    {
        private underlying[] listUnderlying;
        public String aUnderlying;

        public UnderlyingReport()
        {
            InitializeComponent();
        }

        private void GetUnderlying()
        {
            UnderlyingWSService uws = null;
            try
            {
                uws = ImqWS.GetUnderlyingWebService();
                listUnderlying = uws.Search(aUnderlying);
                if (listUnderlying != null)
                {
                    DataSource = listUnderlying;

                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "code");
                    cellCode.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "description");
                    cellDescription.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                if (uws != null)
                    uws.Abort();

                Debug.WriteLine(e.Message);
            }
            finally
            {
                uws.Dispose();
            }
        }

        private void UnderlyingReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetUnderlying();
        }
    }
}
