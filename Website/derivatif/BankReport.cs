﻿using System;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.bankWS;

namespace imq.kpei.derivatif
{
    public partial class BankReport : XtraReport
    {
        private bank[] listBank;
        public String aBankCode;
        public String aBankName;

        public BankReport()
        {
            InitializeComponent();
        }

        private void getBanks()
        {
            BankWSService bws = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                if (bws != null)
                {
                    listBank = bws.Search(aBankCode, aBankName);
                    DataSource = listBank;

                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "bankCode");
                    cellCode.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "name");
                    cellName.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "branch");
                    cellBranch.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "contactPerson");
                    cellContact.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "address");
                    cellAddress.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "phone");
                    cellPhone.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "fax");
                    cellFax.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "email");
                    cellEmail.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                if (bws != null)
                    bws.Abort();

                Debug.WriteLine(e.Message);
            }
            finally
            {
                bws.Dispose();
            }
        }

        private void BankReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            getBanks();
        }
    }
}
