﻿<%@ Page Title="Group Edit" Language="C#" AutoEventWireup="true" CodeBehind="groupedit.aspx.cs" Inherits="imq.kpei.derivatif.groupedit" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        // <![CDATA[
           

           function OnNameValidation(s, e) {
               var groupname = e.value;
               if (groupname == null)
                   return;

               if (groupname.length < 1)
                   e.isValid = false;
           }

          
           }
         //]]>
    </script>
    <div class="content">            
        <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server"  Text="Group Edit" /></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                            ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <table>
            <tr>                        
                <td style="width:128px">Group Name<span style="color:Red">*</span></td>
                <td>
                    <dx:ASPxTextBox ID="txtGroup" runat="server"  Width="170px" EnableClientSideAPI="true"
                     ClientInstanceName="txtgroup" onvalidation="txtGroup_Validation" MaxLength="20">
                        <ValidationSettings ErrorText="Required" >
                            <RequiredField IsRequired="true" ErrorText="Required" />                                    
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnNameValidation" />
                    </dx:ASPxTextBox>
                </td>                    
            </tr>
            <tr>                        
                <td style="width:128px">Description</td>
                <td>
                    <dx:ASPxTextBox ID="txtDescription" runat="server"  Width="170px" MaxLength="200" />
                </td>                    
            </tr>
        </table>
        <br />
        <p><dx:ASPxLabel ID="lblConf" runat="server" Text="" /></p>
        <br />
        <table >
            <tr>
                <td><dx:ASPxButton ID="btnContinue" runat="server" Text="Continue" Width="70px" 
                        onclick="btnContinue_Click" /></td>
                <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                            onclick="btnChecker_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                            onclick="btnApproval_Click" Width="70px"/></td>
                <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" Width="70px" 
                        AutoPostBack="false" CausesValidation="false" onclick="btnCancel_Click"/></td>
            </tr>
        </table>
        <dx:ASPxPanel ID="pnlChild" runat="server" Width="400px" BackColor="GradientInactiveCaption" >
            
            <PanelCollection>
            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                <table style="width:100%; border:0px solid black;border-collapse:collapse; padding-left:4px;">
                    <tr>
                        <td>Series Contract</td>
                        <td nowrap="nowrap"><dx:ASPxComboBox ID="cmbSeries" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" ValueField="seriesCode" ValueType="System.String"
                        CallbackPageSize="10" EnableCallbackMode="true" TextFormatString="{0}" 
                        DropDownStyle="DropDown" Width="170px" 
                                OnItemRequestedByValue="cmbSeries_ItemRequestedByValue" 
                                OnItemsRequestedByFilterCondition="cmbSeries_ItemsRequestedByFilterCondition">
                            <Columns>
                                <dx:ListBoxColumn Caption="Series Code" FieldName="seriesCode" />
                                <dx:ListBoxColumn Caption="Underlying Code" FieldName="underlyingCode" />
                            </Columns>
                            <ValidationSettings Display="Dynamic"></ValidationSettings>
                            </dx:ASPxComboBox>
                            <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" ></dx:ASPxButton></td>
                        
                    </tr>
                    <tr>
                        <td colspan="2">
                        <dx:ASPxGridView ID="gvChild" runat="server" Width="100%" AutoGenerateColumns="false"
                            KeyFieldName="id.seriesCode" 
                                OnCustomButtonCallback="gvChild_CustomButtonCallback" 
                                OnDataBinding="gvChild_DataBinding" >
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Series Code" FieldName="id.seriesCode" VisibleIndex="0"></dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Action" VisibleIndex="1">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnDelete" Text="Delete"></dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn Caption="ID" FieldName="id.id" VisibleIndex="2" Visible="false"></dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridView></td>
                        
                    </tr>
                   
                </table>
                            </dx:PanelContent>
            </PanelCollection>
            
        </dx:ASPxPanel>


        <dx:ASPxHiddenField ID="hfCG" runat="server" ClientInstanceName="hfcg" />
         
    </div>
      
</asp:Content>

