﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.kpeiAccountWS;
using Common.bankWS;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class KpeiAccountPage : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                Permission();
                FillBank();
                gvKpei.DataBind();
            }
        }

        protected void FillBank()
        {
            BankWSService bws = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                Common.bankWS.bank[] banks = bws.get();
                foreach (Common.bankWS.bank bk in banks)
                {
                    cmbBank.Items.Add(bk.name, bk.bankCode);
                }
                //cmbBank.DataSource = bws.get();
                //cmbBank.DataBind();
            }
            catch (Exception ex)
            {
                bws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                bws.Dispose();
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Kpei Account");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        private kpeiAccount[] getKpeiAccount()
        {
            KpeiAccountWSService kas = null;
            kpeiAccount[] list = null;
            try
            {
                kas = ImqWS.GetKpeiAccountService();
                list = kas.get();
            }
            catch (Exception e)
            {
                kas.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                kas.Dispose();
            }
            return list;
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("KpeiAccountEdit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string strResp = String.Format("KpeiAccountEdit.aspx?stat=1&bankCode={0}&settlementAccountNo={1}&penaltyAccountNo={2}&guaranteeFundAccountNo={3}&idx={4}&feeAccountNo={5}", Server.UrlEncode(hfKpei.Get("bankCode").ToString().Trim()), Server.UrlEncode(hfKpei.Get("settlementAccountNo").ToString().Trim()), Server.UrlEncode(hfKpei.Get("penaltyAccountNo").ToString().Trim()), Server.UrlEncode(hfKpei.Get("guaranteeFundAccountNo").ToString().Trim()), Server.UrlEncode(hfKpei.Get("idx").ToString().Trim()), Server.UrlEncode(hfKpei.Get("feeAccountNo").ToString().Trim()));
            Response.Redirect(strResp, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strResp = String.Format("KpeiAccountEdit.aspx?stat=4&bankCode={0}&settlementAccountNo={1}&penaltyAccountNo={2}&guaranteeFundAccountNo={3}&idx={4}&feeAccountNo={5}", Server.UrlEncode(hfKpei.Get("bankCode").ToString().Trim()), Server.UrlEncode(hfKpei.Get("settlementAccountNo").ToString().Trim()), Server.UrlEncode(hfKpei.Get("penaltyAccountNo").ToString().Trim()), Server.UrlEncode(hfKpei.Get("guaranteeFundAccountNo").ToString().Trim()), Server.UrlEncode(hfKpei.Get("idx").ToString().Trim()), Server.UrlEncode(hfKpei.Get("feeAccountNo").ToString().Trim()));
            Response.Redirect(strResp, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            string bankCode = cmbBank.Text;

            bankCode.Replace("'", "''");
            bankCode.Replace("--", "-");

            bankCode = String.Format("[bank.bankName] like '%{0}%'", bankCode);

            gvKpei.FilterExpression = bankCode;
        }

        protected void gvKpei_DataBinding(object sender, EventArgs e)
        {
            gvKpei.DataSource = getKpeiAccount();
        }
    }
}
