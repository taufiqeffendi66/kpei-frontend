﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Diagnostics;
using DevExpress.Web.ASPxClasses.Internal;
using DevExpress.Web.ASPxEditors;
using Common;
using Common.contractGroupWS;
using Common.contractWS;
using Common.memberInfoWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class groupedit : System.Web.UI.Page
    {
        private int stat;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private contractGroupTmp aCGTemp;
        private const String module = "Contract Group";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (!IsPostBack)
            {

                if (Request.QueryString["stat"] == null)
                {
                    Response.Redirect("ContractGroupPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    stat = -1;
                }
                else
                { 
                    Session["stat"] = Request.QueryString["stat"];
                    stat = int.Parse(Session["stat"].ToString());
                }

                btnContinue.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;
                switch (stat)
                {
                    case 0: lblTitle.Text = "New " + module; //New Direct
                        pnlChild.Visible = false;
                        btnContinue.Visible = true;
                        Permission(true);
                        break;
                    case 1: lblTitle.Text = "Edit " + module; //Edit Direct
                        btnContinue.Visible = true;                        
                        Permission(true);
                        ParseQueryString();
                        gvChild.DataBind();
                        break;
                    case 2: lblTitle.Text = Request.QueryString["topic"].ToString(); //New From Temporary
                        pnlChild.Visible = false;
                        Permission(false);

                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3: lblTitle.Text = Request.QueryString["topic"].ToString();
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 4: lblTitle.Text = "Delete " + module;
                        btnContinue.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnContinue.Text = "Delete";
                        break;

                }
                
            }
        }

        private void ParseQueryString()
        {
            int inId = int.Parse(Server.UrlDecode(Request.QueryString["id"].ToString()));
            string strGroup = Server.UrlDecode(Request.QueryString["groupName"].ToString());
            string strDescription = Server.UrlDecode(Request.QueryString["description"].ToString());
            hfCG.Set("id", inId);
            hfCG.Set("groupName", strGroup);
            txtGroup.Text = strGroup.Length > 0 ? strGroup : "";
            txtDescription.Text = strDescription.Length > 0 ? strDescription : "";
        }

        private void DisabledControl()
        {
            txtGroup.Enabled = false;
            txtDescription.Enabled = false;
        }

        private void GetTemporary()
        {
            ContractGroupWSService cgs = null;
            try
            {
                cgs = ImqWS.GetContractGroupService();
                contractGroupTmp cgt = new contractGroupTmp();
                cgt = cgs.getContractGroupTemp(int.Parse(Request.QueryString["idxTmp"].ToString()));
                txtGroup.Text = cgt.groupName;
                txtDescription.Text = cgt.description;
            }
            catch (Exception e)
            {
                cgs.Abort();
                Debug.WriteLine(e.Message);
                //lblError.Text = e.Message;
            }
            finally
            {
                cgs.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aCGTemp));
        }

        private static String Log(contractGroupTmp cgt)
        {
            string strLog = "[Group Name : " + cgt.groupName +
            ", Descriptiom : " + cgt.description +
            "]";
            return strLog;
        }

        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, contractGroupTmp cgt)
        {
            string ret = "0";
            const string target = @"../derivatif/ContractGroupPage.aspx";
            if (!contractGroupTmp.ReferenceEquals(aCGTemp, cgt))
                aCGTemp = cgt;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    ContractGroupWSService cgs = ImqWS.GetContractGroupService();
                    contractGroup cg = new contractGroup() { groupName = aCGTemp.groupName, description = aCGTemp.description };

                    string groupName = String.Empty;
                    int idx = -1;

                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = cgs.add(cg);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "E":
                            idx = int.Parse(hfCG.Get("id").ToString());
                            ret = cgs.update(idx, cg);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "R":
                            idx = int.Parse(hfCG.Get("id").ToString());
                            int count = 0;
                            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
                            count = mis.getGroupCount(idx);
                            if (count == 0)
                            {
                                ret = cgs.delete(idx);
                                if (ret.Equals("0"))
                                {
                                    wsA.AddDirectApproval(dtA);
                                    WriteAuditTrail("Direct Approval Delete " + module);
                                    ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                                }
                                else
                                {
                                    dtA.approvelStatus = "Failed";
                                    wsA.AddDirectApproval(dtA);
                                }
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                                ShowMessage("The Contract Group is still in use by Member ", target);
                            }
                            break;
                    }
                    break;
            }

            return ret;
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {

                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else if (!che && mem && (Request.QueryString["status"].ToString() == "C") && (dtFMP.editApproval))
                        {
                            btnApproval.Visible = true;
                            btnReject.Visible = true;
                        }
                       
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private contractGroupChild[] getChild()
        {
            ContractGroupWSService cgs = null;
            contractGroup cg = null;
            contractGroupChild[] cgcs = null;
            try
            {
                cgs = ImqWS.GetContractGroupService();
                cg = cgs.getContractGroup(int.Parse(hfCG.Get("id").ToString()));
                cgcs = cg.contractGroupChild;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                cgs.Abort();
            }
            finally
            {
                cgs.Dispose();
            }

            return cgcs;
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (btnContinue.Text == "Continue")
            {
                string cgName = txtGroup.Text.Trim();
                if (cgName.Length > 0)
                {
                    lblConf.Text = "Are you sure ?";
                    btnContinue.Text = "Submit";
                }
            }
            else
            {
                //string strResp = "";
                ApprovalService aps = ImqWS.GetApprovalWebService();
                ContractGroupWSService cgs = ImqWS.GetContractGroupService();
                try
                {
                    //addedit = (int)Session["stat"];
                    int idRec = -1;
                    contractGroupTmp cgt = new contractGroupTmp() { groupName = txtGroup.Text.Trim(), description = txtDescription.Text.Trim() };
                    idRec = cgs.addToTemp(cgt);
                    aCGTemp = cgt;
                    if (idRec > -1)
                    {
                        stat = int.Parse(Session["stat"].ToString());
                        dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/groupedit.aspx", status = "M", memberId = aUserMember };
                        int idTable = -1;
                        switch (stat)
                        {
                            case 0:
                                dtA.topic = "Add " + module;
                                dtA.insertEdit = "I";
                                cgt.id = idRec;
                                break;

                            case 1:
                                dtA.topic = "Edit " + module;
                                dtA.insertEdit = "E";
                                idTable = int.Parse(hfCG.Get("id").ToString());
                                dtA.idTable = idTable;
                                dtA.idxTmp = idRec;
                                break;

                            case 4:
                                dtA.topic = "Delete " + module;
                                dtA.insertEdit = "R";
                                idTable = int.Parse(hfCG.Get("id").ToString());
                                dtA.idTable = idTable;
                                dtA.idxTmp = idRec;
                                break;
                        }

                        string retVal = "0";
                        retVal = rblApprovalChanged(dtA, aps, cgt);
                        if (!retVal.Equals("0"))
                        {
                            ShowMessage(retVal, @"../derivatif/ContractGroupPage.aspx");
                        }
                    }
                    else
                    {
                        ShowMessage("Failed On Save To Temporary Series Contract", @"../derivatif/ContractGroupPage.aspx");
                    }
                }
                catch (TimeoutException te)
                {
                    Debug.WriteLine(te.Message);
                    cgs.Abort();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    cgs.Abort();
                }
                finally
                {
                    cgs.Dispose();
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //addedit = int.Parse(Session["stat"].ToString());
            switch (stat)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("ContractGroupPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }
        Hashtable copiedValues = null;
        string[] copiedFields = new string[] { "id.id", "id.seriesCode" };

        protected void gvChild_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (e.ButtonID != "btnDelete")
                return;

            copiedValues = new Hashtable();
            foreach (string fieldName in copiedFields)
            {
                copiedValues[fieldName] = gvChild.GetRowValues(e.VisibleIndex, fieldName);
            }

            ContractGroupWSService cgs = ImqWS.GetContractGroupService();
            try
            {
                int iId = int.Parse(copiedValues["id.id"].ToString());
                string sSeries = copiedValues["id.seriesCode"].ToString();
                cgs.deleteChild(iId, sSeries );
                gvChild.DataBind();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                cgs.Abort();
            }
            finally
            {
                cgs.Dispose();
            }
        }

        protected void gvChild_DataBinding(object sender, EventArgs e)
        {
            gvChild.DataSource = getChild();
        }

        protected void cmbSeries_ItemRequestedByValue(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
        {
            ASPxComboBox combo = (ASPxComboBox)source;
            ContractWSService c = null;
            try
            {
                c = ImqWS.GetContractService();
                series[] contracts = c.get();
                combo.DataSource = contracts;
                combo.DataBind();
            }
            catch (TimeoutException te)
            {
                c.Abort();
                Debug.WriteLine(te.Message);
            }
            catch (Exception ex)
            {
                c.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                c.Dispose();
            }
        }

        protected void cmbSeries_ItemsRequestedByFilterCondition(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
        {
            ASPxComboBox combo = (ASPxComboBox)source;
            ContractWSService c = null;
            try
            {
                c = ImqWS.GetContractService();
                series[] contracts = c.Search(cmbSeries.Text.Trim(), String.Empty);
                combo.DataSource = contracts;
                combo.DataBind();
            }
            catch (TimeoutException te)
            {
                c.Abort();
                Debug.WriteLine(te.Message);
            }
            catch (Exception ex)
            {
                c.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                c.Dispose();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (cmbSeries.Text.Length > 0)
            {
                ContractGroupWSService cgs = null;
                contractGroupChild cgc = null;
                contractGroupChildPK cgcpk = null;
                try
                {
                    cgs = ImqWS.GetContractGroupService();
                    int iId = int.Parse(hfCG.Get("id").ToString());
                    cgcpk = new contractGroupChildPK();

                    cgc = new contractGroupChild();
                    cgc.id = cgcpk;
                    cgc.id.id = iId;
                    cgc.id.seriesCode = cmbSeries.Text.Trim();
                    cgs.addChild(cgc);

                    gvChild.DataBind();
                }
                catch (TimeoutException te)
                {
                    cgs.Abort();
                    Debug.WriteLine(te.Message);
                }
                catch (Exception ex)
                {
                    cgs.Abort();
                    Debug.WriteLine(ex.Message);
                }
                finally
                {
                    cgs.Dispose();
                }
            }
        }

        protected void txtGroup_Validation(object sender, ValidationEventArgs e)
        {
            if (CommonUtils.IsNullValue(e.Value) || ((string)e.Value == ""))
                e.IsValid = false;
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            ContractGroupWSService cgs = null;
            //DataRow row = null;
            dtApproval[] dtA = null;
            const string target = @"../administration/approval.aspx";

            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                cgs = ImqWS.GetContractGroupService();

                dtA = aps.Search(Request.QueryString["reffNo"].ToString());


                aCGTemp = cgs.getContractGroupTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else if (cp == 1)
                {
                    activity = "Approval";
                    
                }
                else
                {
                    if (btnChecker.Visible == true)
                    {
                        activity = "Reject Checker";
                        dtA[0].checkerName = aUserLogin;
                        dtA[0].checkerStatus = "Rejected";
                        dtA[0].approvelStatus = "";
                        dtA[0].memberId = aUserMember;
                        dtA[0].status = "RC";
                        aps.UpdateByChecker(dtA[0]);
                    }
                    else
                    {
                        activity = "Reject Approval";
                        dtA[0].approvelName = aUserLogin;
                        dtA[0].approvelStatus = "Rejected";
                        dtA[0].memberId = aUserMember;
                        dtA[0].status = "RA";
                        aps.UpdateByApprovel(dtA[0]);
                    }

                }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cgs.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cgs.deleteTempById(aCGTemp.id);
                                //if (!ret.Equals("0"))
                                //    lblError.Text = ret;

                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cgs.updateFromTemp(aCGTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cgs.deleteTempById(aCGTemp.id);
                                //if (!ret.Equals("0"))
                                //    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            MemberInfoWSService mis = ImqWS.GetMemberInfoWebService();
                            int count = mis.getGroupCount(dtA[0].idTable);
                            if (count == 0)
                            {
                                ret = cgs.deleteById(dtA[0].idTable);
                                if (!ret.Equals("0"))
                                {
                                    dtA[0].approvelStatus = "Failed";
                                    dtA[0].approvelName = aUserLogin;
                                    dtA[0].memberId = aUserMember;
                                    dtA[0].status = "A";
                                    aps.UpdateByApprovel(dtA[0]);
                                    ShowMessage(ret, target);
                                }
                                else
                                {
                                    dtA[0].approvelStatus = "Approved";
                                    dtA[0].approvelName = aUserLogin;
                                    dtA[0].memberId = aUserMember;
                                    dtA[0].status = "A";
                                    aps.UpdateByApprovel(dtA[0]);
                                    ret = cgs.deleteTempById(aCGTemp.id);
                                    //if (!ret.Equals("0"))
                                    //    lblError.Text = ret;

                                    ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                                }
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage("The Contract Group is still in use by Member ", target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();
                if (cgs != null)
                    cgs.Abort();
                Debug.WriteLine(ex.Message);
                //lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (cgs != null)
                    cgs.Dispose();
            }
        }
    }
}
