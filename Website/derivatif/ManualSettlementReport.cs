﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Common.wsManualSettlement;
using Common;

namespace imq.kpei.derivatif
{
    public partial class ManualSettlementReport : DevExpress.XtraReports.UI.XtraReport
    {
        private manualSettlement[] listMS;

        public String aBank;

        public ManualSettlementReport()
        {
            InitializeComponent();
        }

        private void GetManualSettlement()
        {
            ManualSettlementWSService bs = null;
            try
            {
                bs = ImqWS.GetManualSettlementService();
                if (aBank != "" )
                    listMS = bs.get();
                else
                    listMS = bs.search(aBank);
                if (listMS != null)
                {

                    DataSource = listMS;

                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "id.createDate", "{0:dd/MM/yyyy}");
                    cellDate.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "id.createDate", "{0:HH:mm:ss}");
                    cellTime.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "bank");
                    cellBank.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "id.source");
                    cellSource.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "id.target");
                    cellTarget.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "executionDate", "{0:dd/MM/yyyy}");
                    cellExecutionDate.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "executionDate", "{0:HH:mm:ss}");
                    cellExecutionTime.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "value", "{0:#,##}");
                    cellValue.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "status");
                    cellStatus.DataBindings.Add(binding);
                }
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.Message);
                bs.Abort();
            }
            finally
            {
                bs.Dispose();
            }
        }

        private void ManualSettlementReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetManualSettlement();
        }
    }
}
