﻿namespace imq.kpei.derivatif
{
    partial class LiquidationReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellDefMember = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDefRole = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDefSID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOpMember = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOpRole = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOpSID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellSeries = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellType = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellQuantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellHeaderDef = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderOpp = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellHeaderDefMemberID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderDefRole = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderDefSID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderOppMemberID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderOppRole = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellHeaderOppSID = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDefPos = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOpPos = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableDetail});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableDetail
            // 
            this.tableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableDetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableDetail.Name = "tableDetail";
            this.tableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tableDetail.SizeF = new System.Drawing.SizeF(1026F, 31.25F);
            this.tableDetail.StylePriority.UseBorders = false;
            this.tableDetail.StylePriority.UseFont = false;
            this.tableDetail.StylePriority.UseTextAlignment = false;
            this.tableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellDefMember,
            this.cellDefRole,
            this.cellDefSID,
            this.cellDefPos,
            this.cellOpMember,
            this.cellOpRole,
            this.cellOpSID,
            this.cellOpPos,
            this.cellDate,
            this.cellTime,
            this.cellSeries,
            this.cellType,
            this.cellQuantity});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellDefMember
            // 
            this.cellDefMember.Name = "cellDefMember";
            this.cellDefMember.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellDefMember.StylePriority.UsePadding = false;
            this.cellDefMember.StylePriority.UseTextAlignment = false;
            this.cellDefMember.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellDefMember.Weight = 0.17819407957622085D;
            // 
            // cellDefRole
            // 
            this.cellDefRole.Name = "cellDefRole";
            this.cellDefRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellDefRole.StylePriority.UsePadding = false;
            this.cellDefRole.StylePriority.UseTextAlignment = false;
            this.cellDefRole.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellDefRole.Weight = 0.13653409645670345D;
            // 
            // cellDefSID
            // 
            this.cellDefSID.Name = "cellDefSID";
            this.cellDefSID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellDefSID.StylePriority.UsePadding = false;
            this.cellDefSID.StylePriority.UseTextAlignment = false;
            this.cellDefSID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellDefSID.Weight = 0.36963664121916151D;
            // 
            // cellOpMember
            // 
            this.cellOpMember.Name = "cellOpMember";
            this.cellOpMember.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellOpMember.StylePriority.UsePadding = false;
            this.cellOpMember.StylePriority.UseTextAlignment = false;
            this.cellOpMember.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellOpMember.Weight = 0.17075725646387477D;
            // 
            // cellOpRole
            // 
            this.cellOpRole.Name = "cellOpRole";
            this.cellOpRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellOpRole.StylePriority.UsePadding = false;
            this.cellOpRole.StylePriority.UseTextAlignment = false;
            this.cellOpRole.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellOpRole.Weight = 0.12435661690467831D;
            // 
            // cellOpSID
            // 
            this.cellOpSID.Name = "cellOpSID";
            this.cellOpSID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellOpSID.StylePriority.UsePadding = false;
            this.cellOpSID.StylePriority.UseTextAlignment = false;
            this.cellOpSID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellOpSID.Weight = 0.39204657272534793D;
            // 
            // cellDate
            // 
            this.cellDate.Name = "cellDate";
            this.cellDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellDate.StylePriority.UsePadding = false;
            this.cellDate.StylePriority.UseTextAlignment = false;
            this.cellDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellDate.Weight = 0.26650839370403D;
            // 
            // cellTime
            // 
            this.cellTime.Name = "cellTime";
            this.cellTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellTime.StylePriority.UsePadding = false;
            this.cellTime.StylePriority.UseTextAlignment = false;
            this.cellTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellTime.Weight = 0.17939582668406662D;
            // 
            // cellSeries
            // 
            this.cellSeries.Name = "cellSeries";
            this.cellSeries.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellSeries.StylePriority.UsePadding = false;
            this.cellSeries.Weight = 0.41324315586956945D;
            // 
            // cellType
            // 
            this.cellType.Name = "cellType";
            this.cellType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellType.StylePriority.UsePadding = false;
            this.cellType.Weight = 0.21836235457403411D;
            // 
            // cellQuantity
            // 
            this.cellQuantity.Name = "cellQuantity";
            this.cellQuantity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellQuantity.StylePriority.UsePadding = false;
            this.cellQuantity.Weight = 0.30608178498503441D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 53.95834F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(875F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 62.5F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow3});
            this.tblHeader.SizeF = new System.Drawing.SizeF(1026F, 62.5F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellHeaderDef,
            this.cellHeaderOpp,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell5});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // cellHeaderDef
            // 
            this.cellHeaderDef.Name = "cellHeaderDef";
            this.cellHeaderDef.Text = "Default";
            this.cellHeaderDef.Weight = 0.80132385578779863D;
            // 
            // cellHeaderOpp
            // 
            this.cellHeaderOpp.Name = "cellHeaderOpp";
            this.cellHeaderOpp.Text = "Opposite";
            this.cellHeaderOpp.Weight = 0.81508461006114818D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Date";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell17.Weight = 0.26650809743065523D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Time";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell18.Weight = 0.17939607289519691D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "Contract ID";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell19.Weight = 0.41324333815102671D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Contract";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell20.Weight = 0.2183623580740596D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Quantity";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell5.Weight = 0.30608161001969414D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellHeaderDefMemberID,
            this.cellHeaderDefRole,
            this.cellHeaderDefSID,
            this.xrTableCell2,
            this.cellHeaderOppMemberID,
            this.cellHeaderOppRole,
            this.cellHeaderOppSID,
            this.xrTableCell3,
            this.xrTableCell6,
            this.xrTableCell4,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell11});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // cellHeaderDefMemberID
            // 
            this.cellHeaderDefMemberID.Name = "cellHeaderDefMemberID";
            this.cellHeaderDefMemberID.Text = "Member ID";
            this.cellHeaderDefMemberID.Weight = 0.1781940816428475D;
            // 
            // cellHeaderDefRole
            // 
            this.cellHeaderDefRole.Name = "cellHeaderDefRole";
            this.cellHeaderDefRole.Text = "Role";
            this.cellHeaderDefRole.Weight = 0.1365340673607478D;
            // 
            // cellHeaderDefSID
            // 
            this.cellHeaderDefSID.Name = "cellHeaderDefSID";
            this.cellHeaderDefSID.Text = "SID";
            this.cellHeaderDefSID.Weight = 0.36963668501695524D;
            // 
            // cellHeaderOppMemberID
            // 
            this.cellHeaderOppMemberID.Name = "cellHeaderOppMemberID";
            this.cellHeaderOppMemberID.Text = "Member ID";
            this.cellHeaderOppMemberID.Weight = 0.17075735440413739D;
            // 
            // cellHeaderOppRole
            // 
            this.cellHeaderOppRole.Name = "cellHeaderOppRole";
            this.cellHeaderOppRole.Text = "Role";
            this.cellHeaderOppRole.Weight = 0.12435662770912435D;
            // 
            // cellHeaderOppSID
            // 
            this.cellHeaderOppSID.Name = "cellHeaderOppSID";
            this.cellHeaderOppSID.Text = "SID";
            this.cellHeaderOppSID.Weight = 0.39204655280761108D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.26650807412577576D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.17939642982592841D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 0.41324298122029524D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "Type (O/F)";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell8.Weight = 0.21836253653942531D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.30608143155432843D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.StylePriority.UseTextAlignment = false;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lbTitle,
            this.xrPageInfo2});
            this.ReportHeader.HeightF = 84F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 75F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(1026F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle
            // 
            this.lbTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lbTitle.ForeColor = System.Drawing.Color.Black;
            this.lbTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle.SizeF = new System.Drawing.SizeF(196.93F, 38F);
            this.lbTitle.StylePriority.UseForeColor = false;
            this.lbTitle.Text = "Liquidation";
            this.lbTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.Black;
            this.xrPageInfo2.Format = "{0:\"Current Date: \" dddd, dd MMMM yyyy}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(734F, 50.99999F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(292F, 23F);
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Buy /Sell";
            this.xrTableCell2.Weight = 0.1169590623924727D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Buy / Sell";
            this.xrTableCell3.Weight = 0.12792405781993033D;
            // 
            // cellDefPos
            // 
            this.cellDefPos.Name = "cellDefPos";
            this.cellDefPos.StylePriority.UseTextAlignment = false;
            this.cellDefPos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellDefPos.Weight = 0.11695910782736188D;
            // 
            // cellOpPos
            // 
            this.cellOpPos.Name = "cellOpPos";
            this.cellOpPos.StylePriority.UseTextAlignment = false;
            this.cellOpPos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellOpPos.Weight = 0.12792405542949653D;
            // 
            // LiquidationReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(40, 34, 54, 51);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "11.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.LiquidationReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTable tableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellDefMember;
        private DevExpress.XtraReports.UI.XRTableCell cellDefRole;
        private DevExpress.XtraReports.UI.XRTableCell cellDefSID;
        private DevExpress.XtraReports.UI.XRTableCell cellOpMember;
        private DevExpress.XtraReports.UI.XRTableCell cellOpRole;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRTableCell cellOpSID;
        private DevExpress.XtraReports.UI.XRTableCell cellTime;
        private DevExpress.XtraReports.UI.XRTableCell cellQuantity;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderDefMemberID;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderDefRole;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderDefSID;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderOppMemberID;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderOppRole;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderOppSID;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderDef;
        private DevExpress.XtraReports.UI.XRTableCell cellHeaderOpp;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell cellDate;
        private DevExpress.XtraReports.UI.XRTableCell cellSeries;
        private DevExpress.XtraReports.UI.XRTableCell cellType;
        private DevExpress.XtraReports.UI.XRTableCell cellDefPos;
        private DevExpress.XtraReports.UI.XRTableCell cellOpPos;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
    }
}
