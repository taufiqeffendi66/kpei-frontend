﻿<%@ Page Title="Payment Bank Edit" Language="C#" AutoEventWireup="true" CodeBehind="BankEdit.aspx.cs" Inherits="imq.kpei.derivatif.bankedit" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    
    <script type="text/javascript">
        // <![CDATA[
            function OnValidation(s, e) {
                var val = e.value;
                if (val == null || val == '')
                    return;
                if (val.length < 1)
                    e.isValid = false;

            }

            function alertMsg(msg) {
                alert(msg);
            }
            
        //]]>
    </script>

    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="Payment Bank Edit" /></div>
        
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                            ClientIDMode="AutoID" 
                            onselectedindexchanged="rblApproval_SelectedIndexChanged"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>

        <table>
            <tr>
                <td style="width:128px">Code<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtCode" runat="server" Width="170px" EnableClientSideAPI="true"
                        ClientInstanceName="txtid" MaxLength="10">
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxTextBox></td>                        
            </tr>
            <tr>
                <td style="width:128px">Name<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtBankName" runat="server" Width="170px" EnableClientSideAPI="true"
                        ClientInstanceName="txtbank" >
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />                                    
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
            <tr>
                <td style="width:128px">Contact Person<span style="color:Red">*</span></td>
                <td><dx:ASPxTextBox ID="txtContact" runat="server" Width="170px">
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />                                    
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
            <tr>
                <td>Branch</td>
                <td><dx:ASPxMemo ID="memoBranch" runat="server" Width="170px" Height="60px" />
                </td>
            </tr>
            <tr>
                <td>Address</td>
                <td><dx:ASPxMemo ID="memoAddress" runat="server" Width="170px" Height="60px" /></td>
            </tr>
            <tr>
                <td style="width:128px">Phone</td>
                <td><dx:ASPxTextBox ID="txtPhone" runat="server" Width="170px">
                        
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
            <tr>
                <td style="width:128px">Fax</td>
                <td><dx:ASPxTextBox ID="txtFax" runat="server" Width="170px">
                        
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
            <tr>
                <td style="width:128px">Email</td>
                <td><dx:ASPxTextBox ID="txtEmail" runat="server" Width="170px">
                        <ValidationSettings SetFocusOnError="True">
                            <RegularExpression ErrorText="Invalid e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                            
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
        </table>

            <br />
           
        <div>
            <table>
                <tr>
                    <td> <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false"
                            onclick="btnSave_Click" Width="70px" /> </td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" 
                            onclick="btnChecker_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" 
                            onclick="btnApproval_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" 
                            onclick="btnReject_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True"
                            onclick="btnCancel_Click" CausesValidation="false" Width="70px" /></td>
                   
                </tr>
            </table> 
        </div>
            <br />
            
            <dx:ASPxHiddenField ID="hfBank" runat="server" ClientInstanceName="hfbank" />           
            
    </div>
       
</asp:Content>
