﻿using System;
using System.Diagnostics;
using DevExpress.XtraReports.UI;
using Common.inquiryTradeWS;
using Common;

namespace imq.kpei.derivatif
{
    public partial class InquiryTradeReport : DevExpress.XtraReports.UI.XtraReport
    {
        private trade[] tradelist;

        public string memberId;
        public string contractId;
        public string sid;
        public string contractType;

        public InquiryTradeReport()
        {
            InitializeComponent();
        }

        private void GetData()
        {
            InquiryTradeWSService its = null;
            try
            {
                its = ImqWS.GetInquiryTradeService();
                if (its != null)
                {

                    string filter = String.Empty;
                    if (!string.IsNullOrEmpty(memberId) && (!memberId.Equals("KPEI")))
                        if (!string.IsNullOrEmpty(contractId))
                            if (!string.IsNullOrEmpty(sid))
                                if (!string.IsNullOrEmpty(contractType) && (!contractType.Equals("A")))
                                    filter += "where memberId like '%" + memberId + "%' and id.series like '%" + contractId + "%' and " +
                                              "sid like '%" + sid + "%' and contractType = '" + contractType + "'";
                                else
                                    filter += "where memberId like '%" + memberId + "%' and id.series like '%" + contractId + "%' and " +
                                              "sid like '%" + sid + "%'";
                            else
                                filter += "where memberId like '%" + memberId + "%' and id.series like '%" + contractId + "%'";
                        else
                            filter += "where memberId like '%" + memberId + "%'";
                    else
                        if (!string.IsNullOrEmpty(contractId))
                            if (!string.IsNullOrEmpty(sid))
                                if (!string.IsNullOrEmpty(contractType) && (!contractType.Equals("A")))
                                    filter += "where id.series like '%" + contractId + "%' and " +
                                              "sid like '%" + sid + "%' and contractType = '" + contractType + "'";
                                else
                                    filter += "where id.series like '%" + contractId + "%' and " +
                                              "sid like '%" + sid + "%'";
                            else
                                if (!string.IsNullOrEmpty(contractType) && (!contractType.Equals("A")))
                                    filter += "where id.series like '%" + contractId + "%' and contractType = '" + contractType + "'";
                                else
                                    filter += "where id.series like '%" + contractId + "%'";
                        else
                            if (!string.IsNullOrEmpty(sid))
                                if (!string.IsNullOrEmpty(contractType) && (!contractType.Equals("A")))
                                    filter += "where sid like '%" + sid + "%' and contractType = '" + contractType + "'";
                                else
                                    filter += "where sid like '%" + sid + "%'";
                            else
                                if (!string.IsNullOrEmpty(contractType) && (!contractType.Equals("A")))
                                    filter += "where contractType = '" + contractType + "'";


                    
                    tradelist = its.get(filter);
                                      
                    DataSource = tradelist;
                    XRBinding binding = new XRBinding("Text", DataSource, "id.tradeNo");
                    cl01.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tradeDate", "{0:dd/MM/yyyy}");
                    cl02.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tradeTime", "{0:##:##:##}");
                    cl03.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "memberId");
                    cl04.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sid");
                    cl05.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tradingId");
                    cl06.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "role");    
                    cl07.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.series");
                    cl08.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "contractType");
                    cl09.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.buySell");
                    cl10.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "price", "{0:#,#0.#0}");
                    cl11.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "quantity", "{0:#,#0}");
                    cl12.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "premium", "{0:#,#0.#0}");
                    cl13.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "fee", "{0:#,#0.#0}");
                    cl14.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "deltaPrice", "{0:#,#0.#0}");
                    cl15.DataBindings.Add(binding);
                }
            }
            catch (Exception ex)
            {
                if(its != null)
                    its.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (its != null)
                    its.Dispose();
            }
        }

        private void InquiryTradeReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetData();
            
        }

       

    }
}
