﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class LiquidationView : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private String aMember;
        private String aSeries;
        private String aSID;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.Validate(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (Request.QueryString["series"] != null)
                aSeries = Request.QueryString["series"].Trim();

            if (Request.QueryString["member"] != null)
                aMember = Request.QueryString["member"].Trim();

            if (Request.QueryString["sid"] != null)
                aSID = Request.QueryString["sid"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            XtraReport report = null;
            if (aUserMember.Equals("kpei"))
            {
                //LiquidationReport report = null;

                report = new LiquidationReport();
                (report as LiquidationReport).aUserMember = aUserMember;
                (report as LiquidationReport).aMember = string.Empty;
                (report as LiquidationReport).aSeries = aSeries;
                (report as LiquidationReport).aSID = aSID;
                (report as LiquidationReport).Name = ImqSession.getFormatFileSave("Contract Liquidation");
            }
            else
            {
                report = new LiquidationReportAK();
                (report as LiquidationReportAK).aUserMember = aUserMember;
                (report as LiquidationReportAK).aMember = aMember;
                (report as LiquidationReportAK).aSeries = aSeries;
                (report as LiquidationReportAK).aSID = aSID;
                (report as LiquidationReportAK).Name = ImqSession.getFormatFileSave("Contract Liquidation");
            }

            return report;
        }
    }
}
