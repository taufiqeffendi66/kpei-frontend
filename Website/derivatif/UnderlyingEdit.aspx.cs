﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.underlyingWS;
using Common.contractWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class UnderlyingEdit : System.Web.UI.Page
    {
        private int addedit;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private underlyingTmp aUnderTemp;
        private const String module = "Underlying";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"]));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = "New " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lblTitle.Text = "Edit " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = "New " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 3:
                        lblTitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 4:
                        lblTitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }
            }
        }

        private void GetTemporary()
        {
            UnderlyingWSService uws = null;
            try
            {
                uws = ImqWS.GetUnderlyingWebService();
                underlyingTmp ut = uws.getUnderlyingTemp(int.Parse(Request.QueryString["idxTmp"]));
                txtCode.Text = ut.code;
                memoDescription.Text = ut.description;
            }
            catch (Exception e)
            {
                uws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                uws.Dispose();
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 4:
                case 1:
                    Response.Redirect("UnderlyingPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            UnderlyingWSService uws = null;
            ContractWSService cws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                uws = ImqWS.GetUnderlyingWebService();
                cws = ImqWS.GetContractService();
                dtA = aps.Search(Request.QueryString["reffNo"]);
                aUnderTemp = uws.getUnderlyingTemp(dtA[0].idxTmp);
                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approve";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }

                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = uws.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                uws.deleteTempById(dtA[0].idxTmp);
                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            underlying u = uws.getUnderLying(dtA[0].idTable);
                            ret = uws.updateFromTemp(aUnderTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                uws.deleteTempById(dtA[0].idxTmp);
                                cws.updateUnderlying(aUnderTemp.code, u.code);

                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);

                                uws.Dispose();
                                cws.Dispose();
                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            underlying u = uws.getUnderLying(dtA[0].idTable);
                            int count = cws.getUnderlyingCount(u.code);
                            if (count == 0)
                            {
                                ret = uws.deleteById(dtA[0].idTable);
                                if (!ret.Equals("0"))
                                {
                                    dtA[0].approvelName = aUserLogin;
                                    dtA[0].approvelStatus = "Failed";
                                    dtA[0].memberId = aUserMember;
                                    dtA[0].status = "A";
                                    aps.UpdateByApprovel(dtA[0]);
                                    ShowMessage(ret, target);
                                }
                                else
                                {
                                    dtA[0].approvelName = aUserLogin;
                                    dtA[0].approvelStatus = "Approved";
                                    dtA[0].memberId = aUserMember;
                                    dtA[0].status = "A";
                                    aps.UpdateByApprovel(dtA[0]);
                                    uws.deleteTempById(dtA[0].idxTmp);
                                    ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                                }
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(String.Format("Underlying {0} is still in use by Client Account", u.code), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();
                if (uws != null)
                    uws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
                if (uws != null)
                    uws.Dispose();
            }
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void DisabledControl()
        {
            txtCode.Enabled         = false;
            memoDescription.Enabled = false;
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            string code         = Server.UrlDecode(Request.QueryString["code"].Trim());
            string description  = Server.UrlDecode(Request.QueryString["description"].Trim());
            string idx          = Server.UrlDecode(Request.QueryString["idx"].Trim());
            hfUnderlying.Set("code", code);
            hfUnderlying.Set("idx", idx);
            txtCode.Text = code.Trim() ;
            memoDescription.Text = description.Trim();
        }

        private static String Log(underlyingTmp ut)
        {
            string strLog = String.Format("[Code : {0}, Description : {1}]", ut.code, ut.description);
            return strLog;
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aUnderTemp));
        }

        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, underlyingTmp ut)
        {
            string ret = "0";
            const string target = @"../derivatif/UnderlyingPage.aspx";
            if (!underlyingTmp.ReferenceEquals(aUnderTemp, ut))
                aUnderTemp = ut;
            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    UnderlyingWSService uws = ImqWS.GetUnderlyingWebService();
                    ContractWSService cws = ImqWS.GetContractService();
                    underlying u = new underlying() { code = aUnderTemp.code.Trim(), description = aUnderTemp.description.Trim() };
                    //u.idx = aUnderTemp.idx;
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = uws.Add(u);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                uws.Dispose();
                                cws.Dispose();
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "E":
                            ret = uws.Update(hfUnderlying.Get("code").ToString(), u);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                cws.updateUnderlying(u.code, hfUnderlying.Get("code").ToString());
                                uws.Dispose();
                                cws.Dispose();
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "R":
                            int count = cws.getUnderlyingCount(hfUnderlying.Get("code").ToString());
                            if (count == 0)
                            {
                                ret = uws.delete(hfUnderlying.Get("code").ToString());
                                if (ret.Equals("0"))
                                {
                                    wsA.AddDirectApproval(dtA);
                                    WriteAuditTrail("Direct Approval Delete " + module);
                                    cws.Dispose();
                                    uws.Dispose();
                                    ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                                }
                                else
                                {
                                    dtA.approvelStatus = "Failed";
                                    wsA.AddDirectApproval(dtA);
                                }
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                                uws.Dispose();
                                cws.Dispose();
                                ShowMessage(String.Format("Underlying {0} is still in use by Client Account", hfUnderlying.Get("code")), target);
                            }
                            break;
                    }
                    break;
            }
            return ret;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            UnderlyingWSService uws = null;
            ApprovalService aps = null;

            try
            {
                uws = ImqWS.GetUnderlyingWebService();
                aps = ImqWS.GetApprovalWebService();
                addedit = (int)Session["stat"];
                int idRec = -1;

                underlyingTmp ut = new underlyingTmp() { code = txtCode.Text, description = memoDescription.Text };

                idRec = uws.addToTemp(ut);
                aUnderTemp = ut;
                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/UnderlyingEdit.aspx", status = "M", memberId = aUserMember };
                    int idTable = -1;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            ut.idx = idRec;
                            break;

                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfUnderlying.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfUnderlying.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }
                    string retVal = rblApprovalChanged(dtA, aps, ut);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/UnderlyingPage.aspx");
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Underlying", @"../derivatif/UnderlyingPage.aspx");
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                if (uws != null)
                    uws.Abort();
                if (aps != null)
                    aps.Abort();
            }
            finally
            {
                if (uws != null)
                    uws.Dispose();
                if (aps != null)
                    aps.Dispose();
            }
        }
    }
}
