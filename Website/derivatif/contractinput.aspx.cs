﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Common;
using Common.contractWS;
using Common.underlyingWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;
using Common.wsTransaction;
using System.Data;
using log4net;
using log4net.Config;
using DevExpress.Web.ASPxEditors;

namespace imq.kpei.derivatif
{
    public partial class contractInput : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(_Default));
        
        private int addedit;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private seriesTmp aSeriesTemp;
        private const String module = "Series Contract";
        const string Months = "FGHJKMNQUVXZ";
        private int paramId;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;

                    hfContract.Set("InitialCode", " ");
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }

                fillUnderlying();
                fillTemplateParam();
                deStartDate.Date = DateTime.Now;
                deEndDate.Date = DateTime.Now;

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;
                log.Debug("stat : " + addedit);
                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = "New " + module; //New Direct 
                        changeContractPeriod();
                        btnSave.Visible = true;
                        Permission(true);
                        teExerciseTimeFrom.DateTime = DateTime.Now;
                        teExerciseTimeTo.DateTime = DateTime.Now;
                        txtOpenPrice.Text = "0";
                        break;
                    case 1:
                        lblTitle.Text = "Edit " + module; //Edit Direct
                        btnSave.Visible = true;
                        txtCode.Enabled = false;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = "New " + module; //New From Temporary
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 3:
                        lblTitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 4:
                        lblTitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }
            }
        }

        private string GenerateSeries()
        {
            log.Info("Generate Series");
            string result = string.Empty;
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append(cmbUnderlying.Text);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                log.Error("Generate Series -> " + ex.Message);
            }
            return result;
        }

        private void fillUnderlying()
        {
            log.Info("Fill Underlying");
            UnderlyingWSService uws = null;
            Common.underlyingWS.underlying[] listUnderlying = null;
            try
            {
                uws = ImqWS.GetUnderlyingWebService();
                listUnderlying = uws.get();

                foreach (Common.underlyingWS.underlying u in listUnderlying)
                {
                    cmbUnderlying.Items.Add(u.code.Trim(), u.code.Trim());
                    log.Debug(u.code);
                }
            }
            catch (Exception e)
            {
                uws.Abort();
                Debug.WriteLine(e.Message);
                log.Error("Fill Underlying -> " + e.Message);
            }
            finally
            {
                if(uws != null)
                    uws.Dispose();
            }
        }

        private void fillTemplateParam()
        {
            log.Info("Fill Template Param");
            UiDataService uds = null;
            Common.wsTransaction.templateParamMessage[] listParam = null;
            try
            {
                uds = ImqWS.GetTransactionService();
                listParam = uds.getTemplateParams();

                foreach (Common.wsTransaction.templateParamMessage u in listParam)
                {
                    cmbTempParamId.Items.Add(u.templateName, u.id);
                    log.Debug(u.templateName);
                }
            }
            catch (Exception e)
            {
                uds.Abort();
                Debug.WriteLine(e.Message);
                log.Error("Fill Template Param -> " + e.Message);
            }
            finally
            {
                if(uds != null)
                    uds.Dispose();
            }
        }

        private void ParamChange()
        {
            log.Info("Param Change");
            UiDataService uds = null;
            templateParamMessage listSeries = null;
            try{
                uds = ImqWS.GetTransactionService();
                
                int val = 0;
                val = int.Parse(cmbTempParamId.SelectedItem.Value.ToString());
                listSeries = uds.getTemplateParamById(val);
                string underlying = listSeries.underlying;
                ListEditItem item = cmbUnderlying.Items.FindByValue(underlying);
                if (item == null)
                {
                    item.Text =  underlying.ToUpper();
                    item.Value = underlying.ToUpper();
                }
                cmbUnderlying.SelectedItem = item;
                paramId = listSeries.id;
                txtGfFee.Text = listSeries.gfFee.ToString();
                txtMinFee.Text = listSeries.minimumFee.ToString();
                string clearingFeeType = listSeries.clearingFeeType;
                cmbClearingFeeType.SelectedItem = cmbClearingFeeType.Items.FindByValue(clearingFeeType);                
                string clearingFeeCalc = listSeries.clearingFeeCalculation;
                cmbClearingFeeCalculation.SelectedItem = cmbClearingFeeCalculation.Items.FindByValue(clearingFeeCalc);               
                txtClearingFeeValue.Text = listSeries.clearingFeeValue.ToString();
                txtMarginingType.Text = listSeries.marginValue.ToString();
                txtMultiplier.Text = listSeries.multiplier.ToString();
                txtTickValue.Text = listSeries.tickValue.ToString();
                txtTickSize.Text = listSeries.tickSize.ToString();
                cmbMarginType.SelectedItem = cmbClearingFeeType.Items.FindByValue(listSeries.marginType);
                txtMarginValue.Text = listSeries.marginValue.ToString();

            }catch(Exception e){
                uds.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
                log.Error("Param Change -> " + e.Message);
            }finally{
                if(uds != null)
                    uds.Dispose();
            }
            //UiDataService uds = null;

            //string a = cmbTempParamId.SelectedItem.Value.ToString();
            //uds.getTemplateParamByTemplateName(a);
 
            
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void GetTemporary()
        {
            log.Info("Get Temporary");
            ContractWSService cws = null;
            try
            {
                cws = ImqWS.GetContractService();
                string tmp;
                seriesTmp st = cws.getSeriesTemp(int.Parse(Request.QueryString["idxTmp"].ToString()));
                txtCode.Text = st.seriesCode;
                cmbUnderlying.SelectedItem = cmbUnderlying.Items.FindByValue(st.underlyingCode.Trim());
                memoDescription.Text = st.description;
                cmbType.SelectedItem = cmbType.Items.FindByValue(st.seriesType);
                txtStrike.Text = st.strikePrice.ToString();
                tmp = st.optionType.Trim().ToUpper();
                cmbOptions.SelectedItem = cmbOptions.Items.FindByValue(tmp);
                txtSettlementMethod.Text = st.settlementMethod;
                deLastDay.Date = st.lastDayTransaction;
                txtContractPeriod.Text = st.contractPeriod.ToString();
                deStartDate.Date = st.startDate;
                deEndDate.Date = st.endDate;
                txtMultiplier.Text = st.multiplier.ToString();
                teExerciseTimeFrom.DateTime = st.exerciseTimeFrom;
                teExerciseTimeTo.DateTime = st.exerciseTimeTo;
                cmbExerciseStyle.SelectedItem = cmbExerciseStyle.Items.FindByValue(st.exerciseStyle);
                txtMarginingType.Text = st.marginingType.ToString();
                txtOpenPrice.Text = st.openinterest.ToString();
                if (cmbSeriesStatus.Items.FindByValue(st.seriesStatus) != null)
                    cmbSeriesStatus.SelectedItem = cmbSeriesStatus.Items.FindByValue(st.seriesStatus);
                txtInstrumentCode.Text = st.instrumentCode;
                txtRemark.Text = st.remark;
                txtRemark2.Text = st.remark2;
                cmbTempParamId.SelectedItem = cmbTempParamId.Items.FindByValue(st.template_param_id);
                txtGfFee.Text = st.gf_fee.ToString();
                txtMinFee.Text = st.minimum_fee.ToString();
                //txtClearingFeeType.Text = st.clearing_fee_type;
                //txtClearingFeeCalculation.Text = st.clearing_fee_calculation;
                string clearingFeeType = st.clearing_fee_type;
                cmbClearingFeeType.SelectedItem = cmbClearingFeeType.Items.FindByValue(clearingFeeType);                
                string clearingFeeCalc = st.clearing_fee_calculation;
                cmbClearingFeeCalculation.SelectedItem = cmbClearingFeeCalculation.Items.FindByValue(clearingFeeCalc);
                txtClearingFeeValue.Text = st.clearing_fee_value.ToString();
                txtTickValue.Text = st.tick_value.ToString();
                txtTickSize.Text = st.tick_size.ToString();
                cmbMarginType.SelectedItem = cmbMarginType.Items.FindByValue(st.margin_type);
                txtMarginValue.Text = st.margin_value.ToString();
                log.Debug(st.ToString());
            }
            catch (Exception e)
            {
                cws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
                log.Error("Get Temporary -> " + e.Message);
            }
            finally
            {
                if(cws != null)
                    cws.Dispose();
            }
        }

        private void DisabledControl()
        {
            txtCode.Enabled = false;
            cmbUnderlying.Enabled = false;
            memoDescription.Enabled = false;
            cmbType.Enabled = false;
            txtStrike.Enabled = false;
            cmbOptions.Enabled = false;
            txtSettlementMethod.Enabled = false;
            deLastDay.Enabled = false;
            txtContractPeriod.Enabled = false;
            deStartDate.Enabled = false;
            deEndDate.Enabled = false;
            txtMultiplier.Enabled = false;
            teExerciseTimeFrom.Enabled = false;
            teExerciseTimeTo.Enabled = false;
            cmbExerciseStyle.Enabled = false;
            txtMarginingType.Enabled = false;
            txtOpenPrice.Enabled = false;
            cmbSeriesStatus.Enabled = false;
            txtInstrumentCode.Enabled = false;
            txtRemark.Enabled = false;
            txtRemark2.Enabled = false;
            cmbTempParamId.Enabled= false;
            txtGfFee.Enabled = false;
            txtMinFee.Enabled = false;
            cmbClearingFeeType.Enabled = false;
            cmbClearingFeeCalculation.Enabled = false;
            txtClearingFeeValue.Enabled = false;
            txtTickValue.Enabled = false;
            txtTickSize.Enabled = false;
            cmbMarginType.Enabled = false;
            txtMarginValue.Enabled = false;

        }

        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, seriesTmp st, ContractWSService cws)
        {
            string ret = "0";
            const string target = @"../derivatif/seriescontract.aspx";

            if (!seriesTmp.ReferenceEquals(aSeriesTemp, st))
                aSeriesTemp = st;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                   // ContractWSService cws = ImqWS.GetContractService();
                    series ser = new series() { underlyingCode = aSeriesTemp.underlyingCode, contractPeriod = aSeriesTemp.contractPeriod, 
                        description = aSeriesTemp.description, 
                        endDate = aSeriesTemp.endDate, endDateSpecified = true, 
                        exerciseStyle = aSeriesTemp.exerciseStyle, exerciseTimeFrom = aSeriesTemp.exerciseTimeFrom, 
                        exerciseTimeFromSpecified = true, exerciseTimeTo = aSeriesTemp.exerciseTimeTo, 
                        exerciseTimeToSpecified = true, seriesCode = aSeriesTemp.seriesCode, 
                        lastDayTransaction = aSeriesTemp.lastDayTransaction, lastDayTransactionSpecified = true, 
                        marginingType = aSeriesTemp.marginingType, marginingTypeSpecified = true,
                        multiplier = aSeriesTemp.multiplier, 
                        optionType = aSeriesTemp.optionType, seriesType = aSeriesTemp.seriesType, 
                        settlementMethod = aSeriesTemp.settlementMethod, startDate = aSeriesTemp.startDate, 
                        startDateSpecified = true, strikePrice = aSeriesTemp.strikePrice, 
                        openinterest = aSeriesTemp.openinterest, seriesStatus = aSeriesTemp.seriesStatus, 
                        instrumentCode = aSeriesTemp.instrumentCode, remark = aSeriesTemp.remark, 
                        remark2 = aSeriesTemp.remark2, template_param_id = aSeriesTemp.template_param_id, 
                        template_param_idSpecified = true, 
                        gf_fee = aSeriesTemp.gf_fee, gf_feeSpecified = true, 
                        minimum_fee = aSeriesTemp.minimum_fee, minimum_feeSpecified = true, 
                        clearing_fee_type = aSeriesTemp.clearing_fee_type, 
                        clearing_fee_calculation= aSeriesTemp.clearing_fee_calculation, 
                        clearing_fee_value = aSeriesTemp.clearing_fee_value, clearing_fee_valueSpecified = true,
                        tick_size = aSeriesTemp.tick_size, tick_sizeSpecified = true,
                        tick_value = aSeriesTemp.tick_value, tick_valueSpecified = true, 
                        margin_type = aSeriesTemp.margin_type, 
                        margin_value = aSeriesTemp.margin_value, margin_valueSpecified = true};

                    log.Info("rblApproval Series -> " + ser.seriesCode + "; tick size : " + ser.tick_size + "; tick value : " +
                        ser.tick_value + "; margin type : " + ser.margin_type + "; margin value : " + ser.margin_value);
                    log.Debug("rblApproval Series -> " + ser.ToString());
                    //ser.idx = aSeriesTemp.idx;
                    string code = String.Empty;
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = cws.add(ser);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "E":
                            code = hfContract.Get("code").ToString();
                            ret = cws.update(code, ser);
                            log.Info("Return Value : " + ret);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "R":
                            code = hfContract.Get("code").ToString();
                            ret = cws.delete(code);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Delete " + module);
                                ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }
                    break;
            }
            return ret;
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;
            log.Info("Permission");
            try
            {

                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember,module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"].ToString() == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                log.Error("Permission -> " + e.Message);
            }
        }

        private void ParseQueryString()
        {
                       
            txtCode.Text = Server.UrlDecode(Request.QueryString["code"].ToString());
            string underlying = Server.UrlDecode(Request.QueryString["underlyingcode"].ToString());
            cmbUnderlying.SelectedItem = cmbUnderlying.Items.FindByText(underlying);
            memoDescription.Text = Server.UrlDecode(Request.QueryString["description"].ToString());
            string tmp = Server.UrlDecode(Request.QueryString["type"]);
            cmbType.SelectedItem = cmbType.Items.FindByValue(tmp); 

            txtStrike.Text = Server.UrlDecode(Request.QueryString["strikeprice"].ToString());
            tmp = Server.UrlDecode(Request.QueryString["option"].ToString().ToUpper());
            cmbOptions.SelectedItem = cmbOptions.Items.FindByValue(tmp);
            
            txtSettlementMethod.Text = Server.UrlDecode(Request.QueryString["settlement"].ToString());
            deLastDay.Value = DateTime.Parse(Server.UrlDecode(Request.QueryString["lastday"]).ToString());
            txtContractPeriod.Text = Server.UrlDecode(Request.QueryString["period"].ToString());
            deStartDate.Value = DateTime.Parse(Server.UrlDecode(Request.QueryString["start"]).ToString());
            deEndDate.Value = DateTime.Parse(Server.UrlDecode(Request.QueryString["end"]).ToString());
            string multiplier = Server.UrlDecode(Request.QueryString["multiplier"].ToString());
            txtMultiplier.Text = multiplier;            
            teExerciseTimeFrom.DateTime = DateTime.Parse(Server.UrlDecode(Request.QueryString["exerciseTimeFrom"]).ToString());
            teExerciseTimeTo.DateTime = DateTime.Parse(Server.UrlDecode(Request.QueryString["exerciseTimeTo"]).ToString());
            tmp = Server.UrlDecode(Request.QueryString["exercisestyle"].ToString());
            cmbExerciseStyle.SelectedItem = cmbExerciseStyle.Items.FindByValue(int.Parse(tmp));
            txtMarginingType.Text = Server.UrlDecode(Request.QueryString["margining"].ToString());
            txtOpenPrice.Text = Server.UrlDecode(Request.QueryString["openinterest"].ToString());
            hfContract.Set("code", Server.UrlDecode(Request.QueryString["code"].ToString()));
            
            hfContract.Set("idx", Server.UrlDecode(Request.QueryString["idx"].ToString()));

            tmp = Server.UrlDecode(Request.QueryString["seriesStatus"]).ToString().Trim();
            if (!string.IsNullOrEmpty(tmp))
                cmbSeriesStatus.SelectedItem = cmbSeriesStatus.Items.FindByValue(tmp);

            txtInstrumentCode.Text = Server.UrlDecode(Request.QueryString["instrumentCode"]).ToString().Trim();
            txtRemark.Text = Server.UrlDecode(Request.QueryString["remark"]).ToString().Trim();
            txtRemark2.Text = Server.UrlDecode(Request.QueryString["remark2"]).ToString().Trim();

            string paramId = Server.UrlDecode(Request.QueryString["template_param_id"].ToString());
            cmbTempParamId.SelectedItem = cmbTempParamId.Items.FindByValue(paramId);

            txtGfFee.Text = Server.UrlDecode(Request.QueryString["gf_fee"].ToString());
            txtMinFee.Text = Server.UrlDecode(Request.QueryString["minimum_fee"].ToString());
           
            string clearingFeeType = Server.UrlDecode(Request.QueryString["clearing_fee_type"].ToString());
            cmbClearingFeeType.SelectedItem = cmbClearingFeeType.Items.FindByValue(clearingFeeType);

            string clearingFeeCalc = Server.UrlDecode(Request.QueryString["clearing_fee_calculation"]);
            cmbClearingFeeCalculation.SelectedItem = cmbClearingFeeCalculation.Items.FindByValue(clearingFeeCalc);
            txtClearingFeeValue.Text = Server.UrlDecode(Request.QueryString["clearing_fee_value"].ToString());
            txtTickSize.Text = Server.UrlDecode(Request.QueryString["tick_size"].ToString());
            txtTickValue.Text = Server.UrlDecode(Request.QueryString["tick_value"].ToString());
            cmbMarginType.SelectedItem = cmbMarginType.Items.FindByValue(Server.UrlDecode(Request.QueryString["margin_type"].ToString()));
            txtMarginValue.Text = Server.UrlDecode(Request.QueryString["margin_value"].ToString());

            changeType();
        }

        private void changeContractPeriod()
        {
            //TimeSpan elapsed = deEndDate.Date.Subtract(deStartDate.Date);
            
            //int day = elapsed.Days;
            //txtContractPeriod.Text = day.ToString();
            DateTime dt = DateTime.Now;
            int month = MonthDiff(deEndDate.Date, dt);
            txtContractPeriod.Text = month.ToString();
        }
        private void GenerateCode()
        {
            string strUnderlying = cmbUnderlying.Text;
            string strMonth = MonthCode(deEndDate.Date.Month);
        }

        private static string MonthCode(int month)
        {
            string result = Months.Substring(month, 1);
            return result;
        }

        private static int MonthDiff(DateTime dtTo, DateTime dtFrom)
        {
            int yearFrom = dtFrom.Year;
            int yearTo = dtTo.Year;
            int monthFrom = dtFrom.Month;
            int monthTo = dtTo.Month;
            int retMonths = 0;
            int year = 0;
            if (yearFrom == yearTo)
            {
                if (monthTo == monthFrom)
                    retMonths = 0;
                else
                    retMonths = monthTo - monthFrom;
            }
            else
                if (yearTo > yearFrom)
                {
                    year = yearTo - yearFrom;
                    retMonths = Math.Abs(year * 12);

                    retMonths = Math.Abs(retMonths - (monthTo - monthFrom));
                }
                else
                {
                    retMonths = 0;
                }
            return retMonths + 1;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ApprovalService aps = null;
            ContractWSService cws = null;
            seriesTmp data = null;

            addedit = (int)Session["stat"];
            try
            {
                if (cmbUnderlying.SelectedItem == null)
                {
                    if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                        ClientScript.RegisterStartupScript(this.GetType(), "clientScript", @"<script type='text/javascript'>
                                jAlert('Underlying Invalid Value','Information');
                                </script>");

                    return;
                }

                if (addedit != 4 && int.Parse(txtContractPeriod.Text.Trim()) <= 0)
                {
                    if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                        ClientScript.RegisterStartupScript(this.GetType(), "clientScript", @"<script type='text/javascript'>
                                jAlert('Contract Period Invalid Value','Information');
                                </script>");
                    //lblError.Text = "Contract Period Invalid Value,  End Date must bigger than Start Date";
                    return;
                }

                if (teExerciseTimeFrom.DateTime.Hour > teExerciseTimeTo.DateTime.Hour)
                {
                    if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                        ClientScript.RegisterStartupScript(this.GetType(), "clientScript", @"<script type='text/javascript'>
                                jAlert('Exercise Time Invalid Value','Information');
                                </script>");
                    //lblError.Text = "Exercise Time Invalid Value";
                    return;
                }
                aps = ImqWS.GetApprovalWebService();
                cws = ImqWS.GetContractService();
                
                int idRec = -1;
                data = new seriesTmp();
                data.seriesCode = txtCode.Text.Trim();
                data.underlyingCode = cmbUnderlying.SelectedItem.Value.ToString();
                data.description = memoDescription.Text.Trim();
                data.seriesType = cmbType.SelectedItem.Value.ToString();
                if (txtStrike.Text.Length <= 0)
                    txtStrike.Text = "0";
                data.strikePrice = float.Parse(txtStrike.Text.Trim());

                if (cmbOptions.Text.Length <= 0)
                    data.optionType = "";
                else
                    data.optionType = cmbOptions.SelectedItem.Value.ToString().ToUpper() ;
                data.settlementMethod = txtSettlementMethod.Text.Trim();
                data.lastDayTransaction = deLastDay.Date;
                data.lastDayTransactionSpecified = true;
                changeContractPeriod();
                data.contractPeriod = int.Parse(txtContractPeriod.Text.Trim());

                data.startDate = deStartDate.Date;
                data.startDateSpecified = true;
                data.endDate = deEndDate.Date;
                data.endDateSpecified = true;
                DateTime dtFrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                if (data.seriesType.Equals("RO"))
                {
                    dtFrom = teExerciseTimeFrom.DateTime;
                    dtTo = teExerciseTimeTo.DateTime;
                    //data.multiplier = 10000;
                }
                
                data.exerciseTimeFrom = dtFrom;
                data.exerciseTimeFromSpecified = true;
                data.exerciseTimeTo = dtTo;
                data.exerciseTimeToSpecified = true;
                data.exerciseStyle = int.Parse(cmbExerciseStyle.SelectedItem.Value.ToString());
                data.marginingType = Convert.ToDecimal(txtMarginingType.Text.Trim());
                data.marginingTypeSpecified = true;
                data.openinterest = txtOpenPrice.Text.Length <= 0 ? 0 : float.Parse(txtOpenPrice.Text.Trim());
                data.seriesStatus = cmbSeriesStatus.SelectedItem.Value.ToString();
                data.instrumentCode = txtInstrumentCode.Text;
                data.remark = txtRemark.Text;
                data.remark2 = txtRemark2.Text;

                data.template_param_id = int.Parse(cmbTempParamId.SelectedItem.Value.ToString());
                data.template_param_idSpecified = true;
                data.gf_fee = Convert.ToDecimal(txtGfFee.Text);
                data.gf_feeSpecified = true;
                data.minimum_fee = Convert.ToDecimal(txtMinFee.Text);
                data.minimum_feeSpecified = true;
                data.clearing_fee_type = cmbClearingFeeType.SelectedItem.Value.ToString().Trim();
                data.clearing_fee_calculation = cmbClearingFeeCalculation.SelectedItem.Value.ToString().Trim();
                data.clearing_fee_value = Convert.ToDecimal(txtClearingFeeValue.Text);
                data.clearing_fee_valueSpecified = true;
                data.tick_size = Convert.ToDecimal(txtTickSize.Text.Trim());
                data.tick_sizeSpecified = true;
                data.tick_value = Convert.ToDecimal(txtTickValue.Text.Trim());
                data.tick_valueSpecified = true;
                data.margin_type = cmbMarginType.SelectedItem.Value.ToString().Trim();
                data.margin_value = Convert.ToDecimal(txtMarginValue.Text.Trim());
                data.margin_valueSpecified = true;
                data.multiplier = string.IsNullOrEmpty(txtMultiplier.Text) ? 0 : int.Parse(txtMultiplier.Text.Replace(",", String.Empty)); //int.Parse(txtMultiplier.Text.Trim());
                //data.marginingType = int.Parse(txtMarginingType.Text.Trim());

                /*
                if (data.contractPeriod <= 0)
                {
                    string mystring = @"<script type='text/javascript'>
                                jAlert('Contract Period Invalid Value','Information');
                                </script>";
                    if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                        ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
                    //lblError.Text = "Contract Period Invalid Value,  End Date must bigger than Start Date";
                    return;
                }

                if(data.exerciseTimeFrom.Hour > data.exerciseTimeTo.Hour)
                {
                    string mystring = @"<script type='text/javascript'>
                                jAlert('Exercise Time Invalid Value','Information');
                                </script>";
                    if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                        ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
                    //lblError.Text = "Exercise Time Invalid Value";
                    return;
                }
                */
                idRec = cws.addToTemp(data);
                aSeriesTemp = data;
                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/contractinput.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            aSeriesTemp.idx = idRec;
                            break;

                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfContract.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfContract.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }
                    string retVal = rblApprovalChanged(dtA, aps, aSeriesTemp,cws);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/seriescontract.aspx");
                    }
                }
                else
                {
                    ShowMessage("message=Failed On Save To Temporary Series Contract", @"../derivatif/seriescontract.aspx");
                }
            }
            catch (Exception ex)
            {
                log.Error("BtnSave Click -> " + ex.Message);
                if (cws != null)
                    cws.Abort();
                if (aps != null)
                    aps.Abort();

            }
            finally
            {
                if (cws != null)
                    cws.Dispose();
                if (aps != null)
                    aps.Dispose();
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            ContractWSService cws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                cws = ImqWS.GetContractService();

                dtA = aps.Search(Request.QueryString["reffNo"].ToString());
                aSeriesTemp = cws.getSeriesTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cws.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cws.deleteTempById(aSeriesTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cws.updateFromTemp(aSeriesTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cws.deleteTempById(aSeriesTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cws.deleteById(dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cws.deleteTempById(aSeriesTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (cws != null)
                    cws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
                log.Error("Exec Checker Approval -> " + ex.Message);
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (cws != null)
                    cws.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aSeriesTemp));
        }

        private String Log(seriesTmp st)
        {
            string strLog = "[Contract Code : " + st.seriesCode +
            ", Underlying : " + st.underlyingCode +
            "]";
            return strLog;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("seriescontract.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }

        protected void changeType()
        {
            string val = cmbType.SelectedItem.Value.ToString();
            if (val.Equals("RF"))
            {
                txtSettlementMethod.Text = "CASH";
                //txtMultiplier.Text = "500000";
                //txtMarginingType.Text = "4";                
                txtStrike.Enabled = false;
                cmbExerciseStyle.Enabled = false;
                teExerciseTimeFrom.Enabled = false;
                teExerciseTimeTo.Enabled = false;
                cmbOptions.Enabled = false;
            }
            else
            {
                txtSettlementMethod.Text = "PHYSICAL";
                //txtMultiplier.Text = "10000";
                //txtMarginingType.Text = "20";
                txtStrike.Enabled = true;
                //teExerciseTimeFrom.DateTime = DateTime.Now;
                //teExerciseTimeTo.DateTime = DateTime.Now;
                cmbExerciseStyle.Enabled = true;
                teExerciseTimeFrom.Enabled = true;
                teExerciseTimeTo.Enabled = true;
                cmbOptions.Enabled = true;
            }
        }

        protected void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeType();
        }

        protected void deStartDate_DateChanged(object sender, EventArgs e)
        {
            changeContractPeriod();
        }

        protected void rblApproval_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmbExerciseStyle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmbTempParamId_SelectedIndexChanged(object sender, EventArgs e)
        {
            ParamChange();
        }

        protected void cmbUnderlying_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

