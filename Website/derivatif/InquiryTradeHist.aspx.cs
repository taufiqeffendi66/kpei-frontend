﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.inquiryTradeWS;

namespace imq.kpei.derivatif
{
    public partial class InquiryTradeHist : System.Web.UI.Page
    {
        private trade[] tradelist = null;
        private string aUserMember;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString().Trim();
            if (!IsPostBack)
            {
                gvTradeHistory.DataBind();
                if (!aUserMember.Equals("kpei"))
                {
                    cmbMember.Enabled = false;
                    cmbMember.Text = aUserMember;
                }
            }
        }

        private trade[] getInquiry()
        {
            InquiryTradeWSService its = null;
            try
            {
                its = ImqWS.GetInquiryTradeService();
                
                
                //tradelist = its.GetTrade(cmbMember.Text, cmbContract.Text, cmbSID.Text, cmbType.Value.ToString().Substring(0, 1));
                if (aUserMember.Equals("kpei"))
                    tradelist = its.get("");
                else
                    tradelist = its.get("where memberId='" + aUserMember + "'");
                trade[] tradetemp = null;
                if (tradelist != null)
                {
                    tradetemp = (trade[])tradelist.Clone();
                    cmbMember.DataSource =  tradetemp.GroupBy(p => p.memberId)
                                            .Select(g => g.First()).ToArray();
                    cmbContract.DataSource = tradetemp.GroupBy(p => p.id.series)
                                             .Select(g => g.First()).ToArray();
                    cmbSID.DataSource   =   tradetemp.GroupBy(p => p.sid)
                                            .Select(g => g.First()).ToArray();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                its.Abort();
            }
            finally
            {
                its.Dispose();
            }

            return tradelist;
        }

        protected void gvTradeHistory_DataBinding(object sender, EventArgs e)
        {
            gvTradeHistory.DataSource = getInquiry();
        }

        protected void btnGO_Click(object sender, EventArgs e)
        {
            string memberId = cmbMember.Text.Trim();
            string contractId = cmbContract.Text.Trim();
            string sid = cmbSID.Text.Trim();
            string ctype = cmbType.Value.ToString();

            string filter = string.Empty;

            if (!string.IsNullOrEmpty(memberId)) filter += String.Format("memberId like '%{0}%'", memberId);
            if (!string.IsNullOrEmpty(contractId))
            {
                if (string.IsNullOrEmpty(filter))
                    filter += String.Format(" id.series like '%{0}%'", contractId);
                else
                    filter += String.Format(" and id.series like '%{0}%'", contractId);
            }
            if (!string.IsNullOrEmpty(sid))
            {
                if (string.IsNullOrEmpty(filter))
                    filter += String.Format(" sid like '%{0}%'", sid);
                else
                    filter += String.Format(" and sid like '%{0}%'", sid);
            }
            if (!string.IsNullOrEmpty(ctype) && (!ctype.Equals("A")))
            {
                if (string.IsNullOrEmpty(filter))
                    filter += String.Format(" contractType = '{0}'", ctype);
                else
                    filter += String.Format(" and contractType = '{0}'", ctype);
            }

/*
            if (!string.IsNullOrEmpty(memberId))
            {
                if (!string.IsNullOrEmpty(contractId))
                {
                    if (!string.IsNullOrEmpty(sid))
                    {
                        if (!string.IsNullOrEmpty(ctype) && (!ctype.Equals("A")))
                            filter += String.Format("memberId like '%{0}%' and id.series like '%{1}%' and sid like '%{2}%' and contractType = '{3}'", memberId, contractId, sid, ctype);
                        else
                            filter += String.Format("memberId like '%{0}%' and id.series like '%{1}%' and sid like '%{2}%'", memberId, contractId, sid);
                    }
                    else
                        filter += String.Format("memberId like '%{0}%' and id.series like '%{1}%'", memberId, contractId);
                }
                else
                    filter += String.Format("memberId like '%{0}%'", memberId);
            }
            else
                if (!string.IsNullOrEmpty(contractId))
                {
                    if (!string.IsNullOrEmpty(sid))
                    {
                        if (!string.IsNullOrEmpty(ctype) && (!ctype.Equals("A")))
                            filter += String.Format("id.series like '%{0}%' and sid like '%{1}%' and contractType = '{2}'", contractId, sid, ctype);
                        else
                            filter += String.Format("id.series like '%{0}%' and sid like '%{1}%'", contractId, sid);
                    }
                    else
                        if (!string.IsNullOrEmpty(ctype) && (!ctype.Equals("A")))
                            filter += String.Format("id.series like '%{0}%' and contractType = '{1}'", contractId, ctype);
                        else
                            filter += String.Format("id.series like '%{0}%'", contractId);
                }
                else
                    if (!string.IsNullOrEmpty(sid))
                    {
                        if (!string.IsNullOrEmpty(ctype) && (!ctype.Equals("A")))
                            filter += String.Format("sid like '%{0}%' and contractType = '{1}'", sid, ctype);
                        else
                            filter += String.Format("sid like '%{0}%'", sid);
                    }
                    else
                        if (!string.IsNullOrEmpty(ctype) && (!ctype.Equals("A")))
                            filter += String.Format("contractType = '{0}'", ctype);
*/
            gvTradeHistory.FilterExpression = filter;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            String strJSExport = "<script>window.open('InquiryTradeView.aspx?memberId=" + cmbMember.Text +
                "&contractId=" + cmbContract.Text + "&sid=" + cmbSID.Text +
                "&contractType=" + cmbType.Value.ToString() + "','','fullscreen=yes;scrollbars=auto');</script>";
            Page.ClientScript.RegisterStartupScript(GetType(), "strJSExport", strJSExport);
            /*Response.Write("<script>window.open('InquiryTradeView.aspx?memberId=" + cmbMember.Text +
                "&contractId=" + cmbContract.Text + "&sid=" + cmbSID.Text +
                "&contractType=" + cmbType.Value.ToString() + "','','fullscreen=yes;scrollbars=auto');</script>");
             * /
            /*Response.Redirect("InquiryTradeView.aspx?memberId=" + cmbMember.Text + 
                "&contractId="+cmbContract.Text + "&sid=" + cmbSID.Text +
                "&&contractType=" + cmbType.Value.ToString(), false);
             * */
            //Context.ApplicationInstance.CompleteRequest();
        }
    }
}
