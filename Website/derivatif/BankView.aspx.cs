﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.UI;
using Common;

namespace imq.kpei.derivatif
{
    public partial class BankView : System.Web.UI.Page
    {
        public String bankCode;
        public String bankName;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            
            if (Request.QueryString["code"] != null)
                bankCode = Request.QueryString["code"].ToString().Trim();

            if (Request.QueryString["bankName"] != null)
                bankName = Request.QueryString["bankName"].ToString().Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            BankReport report = new BankReport() { aBankCode = bankCode, aBankName = bankName, Name = ImqSession.getFormatFileSave("Bank") };
            return report;
        }

        
    }
}
