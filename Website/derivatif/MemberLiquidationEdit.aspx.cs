﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.memberLiquidationWS;
using Common.wsUserGroupPermission;
using Common.wsApproval;
using System.Globalization;

namespace imq.kpei.derivatif
{
    public partial class MemberLiquidationEdit : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private const String module = "Member Liquidation";
        private memberLiquidationTmp tmp;
        private int addedit = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"]));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                //btnSave.Visible = false;

                if (Session["stat"] == null)
                {
                    Response.Redirect("MemberLiquidationPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }


                if (Request.QueryString["idTable"] == null)
                {
                    Response.Redirect("MemberLiquidationPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    hfML.Set("id", Server.UrlDecode(Request.QueryString["idTable"]));
                    hfML.Set("memberId", Server.UrlDecode(Request.QueryString["memberId"]));
                    hfML.Set("liqType", Server.UrlDecode(Request.QueryString["liqType"]));
                    hfML.Set("indicator", Server.UrlDecode(Request.QueryString["indicator"]));
                    hfML.Set("fakeindicator", Server.UrlDecode(Request.QueryString["fi"]));
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                //gvLiq.DataBind();

                switch (addedit)
                {
                    case 1:
                        lbltitle.Text = "Update " + module;
                        btnSave.Visible = true;
                        ParseQueryString();
                        Permission(true);
                        break;
                    case 2:
                        lbltitle.Text = "Update " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    default: break;
                }
            }
        }

        //protected void gvLiq_DataBinding(object sender, EventArgs e)
        //{
        //    gvLiq.DataSource = getMemberLiquidation();
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ApprovalService aps = null;
            MemberLiquidationWSService ws = null;

            try
            {
                aps = ImqWS.GetApprovalWebService();
                ws = ImqWS.GetMemberLiquidationService();
                CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                ci.NumberFormat.CurrencyDecimalSeparator = ".";
                var indica = float.Parse(hfML.Get("indicator").ToString(), NumberStyles.Any, ci);
                var fakeindica = float.Parse(hfML.Get("fakeindicator").ToString(), NumberStyles.Any, ci);
                memberLiquidationTmp data = new memberLiquidationTmp() { approved = rbLiquidated.SelectedItem.Value.ToString(), date = deDate.Date, dateSpecified = true, id = long.Parse(lblId.Text),memberId = lblMemberId.Text,indicator = indica,liqType=hfML.Get("liqType").ToString(),fakeindicator=fakeindica};

                addedit = (int) Session["stat"];

                long idRec = ws.addToTemp(data);
                tmp = data;

                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = Convert.ToInt32(idRec), form = "~/derivatif/MemberLiquidationEdit.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            tmp.idx = idRec;
                            break;
                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfML.Get("id").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = Convert.ToInt32(idRec);
                            break;
                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfML.Get("id").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = Convert.ToInt32(idRec);
                            break;
                        default: break;
                    }
                    string retVal = "0";
                    retVal = rblApprovalChanged(dtA, aps, tmp);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/MemberLiquidationPage.aspx");
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Member", @"../derivatif/MemberLiquidationPage.aspx");
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (ws != null)
                    ws.Abort();

                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (ws != null)
                    ws.Dispose();
            }
        }

        private memberLiquidation[] getMemberLiquidation()
        {
            MemberLiquidationWSService mlws = null;
            memberLiquidation[] data = null;

            try
            {
                mlws = ImqWS.GetMemberLiquidationService();
                if (hfML.Get("id") != null)
                    data = mlws.get(" where approved='U' and id.id =" + hfML.Get("id") + " and id.memberId = '" + hfML.Get("memberId").ToString());
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                if (mlws != null)
                    mlws.Abort();
            }
            finally
            {
                if (mlws != null)
                    mlws.Dispose();
            }
            return data;
        }

        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, memberLiquidationTmp mlt)
        {
            string ret = "0";
            const string target = @"../derivatif/MemberLiquidationPage.aspx";

            if (!memberLiquidationTmp.ReferenceEquals(tmp, mlt))
                tmp = mlt;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                        default: break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                        default: break;
                    }
                    break;
                case 2:
                    MemberLiquidationWSService ws = ImqWS.GetMemberLiquidationService();

                    memberLiquidationPK mpk = new memberLiquidationPK() { id = tmp.id, memberId = tmp.memberId };
                    memberLiquidation mp = new memberLiquidation() { approved = tmp.approved, date = tmp.date, dateSpecified = tmp.dateSpecified, id = mpk, indicator = tmp.indicator,liqType=tmp.liqType,fakeindicator = tmp.fakeindicator};



                    switch (dtA.insertEdit)
                    {
                        case "I":
                            break;
                        case "E":
                            ret = ws.update(mp);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "R":
                            break;
                    }
                    break;
            }
            return ret;
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            lblId.Text = Server.UrlDecode(Request.QueryString["idTable"]);
            deDate.Value = DateTime.Parse(Server.UrlDecode(Request.QueryString["dates"]));
            lblMemberId.Text = Server.UrlDecode(Request.QueryString["memberId"]);
            lblIndicator.Text = Server.UrlDecode(Request.QueryString["fi"]);
        }

        private void DisabledControl()
        {
            rbLiquidated.Enabled = false;
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(tmp));
        }

        private static String Log(memberLiquidationTmp ml)
        {
            string strLog = String.Format("[ID : {0}; Date : {1:dd/MM/yyyy}]", ml.id, ml.date);
            return strLog;
        }

        private void GetTemporary()
        {
            MemberLiquidationWSService ws = null;
            try
            {
                ws = ImqWS.GetMemberLiquidationService();
                memberLiquidationTmp mlt = ws.getTemp(int.Parse(Request.QueryString["idxTmp"]));
                lblId.Text = mlt.id.ToString();
                deDate.Date = mlt.date;
                lblMemberId.Text = mlt.memberId;
                lblIndicator.Text = mlt.fakeindicator.ToString();
                rbLiquidated.SelectedItem = rbLiquidated.Items.FindByValue(mlt.approved.Trim());
                hfML.Set("id", mlt.id);
                hfML.Set("memberId", mlt.memberId);
                hfML.Set("indicator", mlt.indicator);
                hfML.Set("fakeindicator", mlt.fakeindicator);
                hfML.Set("liqType", mlt.liqType);
                //gvLiq.DataBind();
            }
            catch (Exception e)
            {
                if (ws != null)
                    ws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                if (ws != null)
                    ws.Dispose();
            }
        }




        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            MemberLiquidationWSService ws = null;

            dtApproval[] dtA = null;
            const string target = @"../administration/approval.aspx";

            try
            {
                aps = ImqWS.GetApprovalWebService();
                ws = ImqWS.GetMemberLiquidationService();

                dtA = aps.Search(Request.QueryString["reffNo"]);
                tmp = ws.getTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = ws.updateFromTemp(tmp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                //ret = ws.deleteTempById(ts.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (ws != null)
                    ws.Abort();

                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (ws != null)
                    ws.Dispose();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Session["stat"] != null)
            {
                addedit = int.Parse(Session["stat"].ToString());
                switch (addedit)
                {
                    case 0:
                    case 1:
                    case 4:
                        Response.Redirect("MemberLiquidationPage.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                        break;
                    case 2:
                    case 3:
                        Response.Redirect("~/administration/approval.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                        break;
                }
            }
            else
            {
                Response.Redirect("MemberLiquidationPage.aspx", false);
                Context.ApplicationInstance.CompleteRequest();                
            }
        }
    }
}
