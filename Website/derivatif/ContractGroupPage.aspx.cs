﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.contractGroupWS;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class contractgrouppage : System.Web.UI.Page
    {
        
        public int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        public contractGroup[] contgroups;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                Permission();
                gvContractGroup.DataBind();
            }
        }

        public contractGroup[] getWSCG()
        {
            ContractGroupWSService wcg = null;
            try
            {
                wcg = ImqWS.GetContractGroupService();
                contgroups = wcg.get();
                if (contgroups != null)
                {
                    foreach (contractGroup cg in contgroups)
                    {
                        cmbGroup.Items.Add(cg.groupName, cg.groupName);
                        cmbDesc.Items.Add(cg.description, cg.description);
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
                if (wcg != null)
                    wcg.Abort();
            }
            finally
            {
                wcg.Dispose();
            }
            return contgroups;
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Contract Group");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("groupedit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void gvContractGroup_DataBinding(object sender, EventArgs e)
        {
            gvContractGroup.DataSource = getWSCG();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string strResp = String.Format("groupedit.aspx?stat=1&id={0}&groupName={1}&description={2}", hfCG.Get("id").ToString().Trim(), hfCG.Get("groupName").ToString().Trim(), hfCG.Get("description").ToString().Trim());
            Response.Redirect(strResp, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strResp = String.Format("groupedit.aspx?stat=4&id={0}&groupName={1}&description={2}", hfCG.Get("id").ToString().Trim(), hfCG.Get("groupName").ToString().Trim(), hfCG.Get("description").ToString().Trim());

            Response.Redirect(strResp, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string GroupName = cmbGroup.Text.Trim();
            string Description = cmbDesc.Text.Trim();
            string search = String.Empty;

            if (GroupName != String.Empty)
            {
                GroupName = GroupName.Replace("'", "''");
                GroupName = GroupName.Replace("--", "-");

                GroupName = String.Format("[groupName] like '%{0}%'", GroupName);
            }

            if (Description != String.Empty)
            {
                Description = Description.Replace("'", "''");
                Description = Description.Replace("--", "-");

                Description = String.Format("[description] like '%{0}%'", Description);
            }

            if (GroupName != String.Empty)
            {
                if (Description != String.Empty)
                    search = String.Format("{0} and {1}", GroupName, Description);
                else
                    search = GroupName;
            }
            else
            {
                if (Description != String.Empty)
                    search = Description;
                else
                    search = String.Empty;
            }
            gvContractGroup.FilterExpression = search;
        }
    }
}
