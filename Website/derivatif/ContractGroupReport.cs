﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Common;
using Common.contractGroupWS;

namespace imq.kpei.derivatif
{
    public partial class ContractGroupReport : DevExpress.XtraReports.UI.XtraReport
    {
        private contractGroup[] listContractGroup;
        public String aGroupName;
        public String aDescription;

        public ContractGroupReport()
        {
            InitializeComponent();
        }

        private void ContractGroupReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetContractGroup();
        }

        public void GetContractGroup()
        {
            ContractGroupWSService wcg = null;
            try
            {
                wcg = ImqWS.GetContractGroupService();
                listContractGroup = wcg.Search(aGroupName, aDescription);

                if (listContractGroup != null)
                {
                    DataSource = listContractGroup;
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "id");
                    cellID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "groupName");
                    cellName.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "description");
                    cellDescription.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                e.ToString();
                if (wcg != null)
                    wcg.Abort();
            }
            finally
            {
                wcg.Dispose();
            }
        }
    }
}
