﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class ContractGroupView : System.Web.UI.Page
    {
        private String aGroupName;
        private String aDescription;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.Validate(this);
            if (Request.QueryString["group"] != null)
                aGroupName = Request.QueryString["group"].ToString().Trim();
            if (Request.QueryString["description"] != null)
                aDescription = Request.QueryString["description"].ToString().Trim();
            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            ContractGroupReport report = new ContractGroupReport() { aGroupName = aGroupName, aDescription = aDescription, Name = ImqSession.getFormatFileSave("Contract Group") };
            return report;
        }
    }
}
