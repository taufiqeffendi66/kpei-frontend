﻿namespace imq.kpei.derivatif
{
    partial class MemberReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellMember = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellName = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellType = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellNPWP = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAddress = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellPhone = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellFax = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellContact = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.header1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.cellEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.header11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAdmin = new DevExpress.XtraReports.UI.XRTableCell();
            this.header12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellSktNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.header13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellSktDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.header14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellActive = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableDetail});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableDetail
            // 
            this.tableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableDetail.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.tableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableDetail.Name = "tableDetail";
            this.tableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tableDetail.SizeF = new System.Drawing.SizeF(1063F, 31.25F);
            this.tableDetail.StylePriority.UseBorders = false;
            this.tableDetail.StylePriority.UseFont = false;
            this.tableDetail.StylePriority.UseTextAlignment = false;
            this.tableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellMember,
            this.cellName,
            this.cellType,
            this.cellStatus,
            this.cellNPWP,
            this.cellAddress,
            this.cellPhone,
            this.cellFax,
            this.cellContact,
            this.cellEmail,
            this.cellAdmin,
            this.cellSktNo,
            this.cellSktDate,
            this.cellActive});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellMember
            // 
            this.cellMember.Name = "cellMember";
            this.cellMember.StylePriority.UseTextAlignment = false;
            this.cellMember.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellMember.Weight = 0.16133078059982275D;
            // 
            // cellName
            // 
            this.cellName.Name = "cellName";
            this.cellName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellName.StylePriority.UsePadding = false;
            this.cellName.Weight = 0.38656220671023339D;
            // 
            // cellType
            // 
            this.cellType.Name = "cellType";
            this.cellType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellType.StylePriority.UsePadding = false;
            this.cellType.Weight = 0.14497408351065316D;
            // 
            // cellStatus
            // 
            this.cellStatus.Name = "cellStatus";
            this.cellStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellStatus.StylePriority.UsePadding = false;
            this.cellStatus.Weight = 0.18517139633430529D;
            // 
            // cellNPWP
            // 
            this.cellNPWP.Name = "cellNPWP";
            this.cellNPWP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellNPWP.StylePriority.UsePadding = false;
            this.cellNPWP.Weight = 0.27369020968225094D;
            // 
            // cellAddress
            // 
            this.cellAddress.Multiline = true;
            this.cellAddress.Name = "cellAddress";
            this.cellAddress.Weight = 0.49525387401081089D;
            // 
            // cellPhone
            // 
            this.cellPhone.Name = "cellPhone";
            this.cellPhone.StylePriority.UseTextAlignment = false;
            this.cellPhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellPhone.Weight = 0.3120599437355388D;
            // 
            // cellFax
            // 
            this.cellFax.Name = "cellFax";
            this.cellFax.Weight = 0.32816755651276874D;
            // 
            // cellContact
            // 
            this.cellContact.Name = "cellContact";
            this.cellContact.Weight = 0.31515773435486427D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 54F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(911.9999F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 31.25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.tblHeader.SizeF = new System.Drawing.SizeF(1063F, 31.25F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.header1,
            this.header2,
            this.header3,
            this.header4,
            this.header5,
            this.header6,
            this.header7,
            this.header8,
            this.header9,
            this.header10,
            this.header11,
            this.header12,
            this.header13,
            this.header14});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // header1
            // 
            this.header1.Name = "header1";
            this.header1.Text = "Member ID";
            this.header1.Weight = 0.16133079534735845D;
            // 
            // header2
            // 
            this.header2.Name = "header2";
            this.header2.Text = "Name";
            this.header2.Weight = 0.38656225095286723D;
            // 
            // header3
            // 
            this.header3.Name = "header3";
            this.header3.Text = "Type";
            this.header3.Weight = 0.14497408351063973D;
            // 
            // header4
            // 
            this.header4.Name = "header4";
            this.header4.Text = "Status";
            this.header4.Weight = 0.1851712783539933D;
            // 
            // header5
            // 
            this.header5.Name = "header5";
            this.header5.Text = "NPWP";
            this.header5.Weight = 0.27369015069209479D;
            // 
            // header6
            // 
            this.header6.Name = "header6";
            this.header6.Text = "Address";
            this.header6.Weight = 0.49525397724360942D;
            // 
            // header7
            // 
            this.header7.Name = "header7";
            this.header7.Text = "Phone";
            this.header7.Weight = 0.31205993636171392D;
            // 
            // header8
            // 
            this.header8.Name = "header8";
            this.header8.Text = "Fax";
            this.header8.Weight = 0.32816755282591081D;
            // 
            // header9
            // 
            this.header9.Name = "header9";
            this.header9.Text = "Contact Person";
            this.header9.Weight = 0.31515775363150639D;
            // 
            // header10
            // 
            this.header10.Name = "header10";
            this.header10.Text = "Email";
            this.header10.Weight = 0.42739193928351243D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lbTitle,
            this.xrPageInfo2});
            this.ReportHeader.HeightF = 84F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 75F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(1063F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle
            // 
            this.lbTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lbTitle.ForeColor = System.Drawing.Color.Black;
            this.lbTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle.SizeF = new System.Drawing.SizeF(196.93F, 38F);
            this.lbTitle.StylePriority.UseForeColor = false;
            this.lbTitle.Text = "Member";
            this.lbTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.Black;
            this.xrPageInfo2.Format = "{0:\"Current Date: \" dddd, dd MMMM yyyy}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(771F, 50.99999F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(292F, 23F);
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // cellEmail
            // 
            this.cellEmail.Name = "cellEmail";
            this.cellEmail.Weight = 0.42739193601773567D;
            // 
            // header11
            // 
            this.header11.Name = "header11";
            this.header11.Text = "Admin User";
            this.header11.Weight = 0.26202058745256196D;
            // 
            // cellAdmin
            // 
            this.cellAdmin.Name = "cellAdmin";
            this.cellAdmin.Weight = 0.26202058581967358D;
            // 
            // header12
            // 
            this.header12.Name = "header12";
            this.header12.Text = "SKT No";
            this.header12.Weight = 0.28605246900361569D;
            // 
            // cellSktNo
            // 
            this.cellSktNo.Name = "cellSktNo";
            this.cellSktNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.cellSktNo.StylePriority.UsePadding = false;
            this.cellSktNo.Weight = 0.28605246818717156D;
            // 
            // header13
            // 
            this.header13.Name = "header13";
            this.header13.Text = "SKT Date";
            this.header13.Weight = 0.28800017641945908D;
            // 
            // cellSktDate
            // 
            this.cellSktDate.Name = "cellSktDate";
            this.cellSktDate.StylePriority.UseTextAlignment = false;
            this.cellSktDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellSktDate.Weight = 0.28800017601123706D;
            // 
            // header14
            // 
            this.header14.Name = "header14";
            this.header14.Text = "Active Date";
            this.header14.Weight = 0.24370258059780917D;
            // 
            // cellActive
            // 
            this.cellActive.Name = "cellActive";
            this.cellActive.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.cellActive.StylePriority.UsePadding = false;
            this.cellActive.Weight = 0.2437025801895871D;
            // 
            // MemberReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(16, 21, 54, 51);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "11.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MemberReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell header1;
        private DevExpress.XtraReports.UI.XRTableCell header2;
        private DevExpress.XtraReports.UI.XRTableCell header3;
        private DevExpress.XtraReports.UI.XRTableCell header4;
        private DevExpress.XtraReports.UI.XRTableCell header5;
        private DevExpress.XtraReports.UI.XRTable tableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellMember;
        private DevExpress.XtraReports.UI.XRTableCell cellName;
        private DevExpress.XtraReports.UI.XRTableCell cellType;
        private DevExpress.XtraReports.UI.XRTableCell cellStatus;
        private DevExpress.XtraReports.UI.XRTableCell cellNPWP;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRTableCell header6;
        private DevExpress.XtraReports.UI.XRTableCell header7;
        private DevExpress.XtraReports.UI.XRTableCell header8;
        private DevExpress.XtraReports.UI.XRTableCell cellAddress;
        private DevExpress.XtraReports.UI.XRTableCell cellPhone;
        private DevExpress.XtraReports.UI.XRTableCell cellFax;
        private DevExpress.XtraReports.UI.XRTableCell header9;
        private DevExpress.XtraReports.UI.XRTableCell cellContact;
        private DevExpress.XtraReports.UI.XRTableCell header10;
        private DevExpress.XtraReports.UI.XRTableCell cellEmail;
        private DevExpress.XtraReports.UI.XRTableCell header11;
        private DevExpress.XtraReports.UI.XRTableCell cellAdmin;
        private DevExpress.XtraReports.UI.XRTableCell header12;
        private DevExpress.XtraReports.UI.XRTableCell cellSktNo;
        private DevExpress.XtraReports.UI.XRTableCell cellSktDate;
        private DevExpress.XtraReports.UI.XRTableCell header13;
        private DevExpress.XtraReports.UI.XRTableCell cellActive;
        private DevExpress.XtraReports.UI.XRTableCell header14;
    }
}
