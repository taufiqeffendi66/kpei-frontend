﻿<%@ Page Title="Manual Settlement Edit" Language="C#" AutoEventWireup="true" CodeBehind="ManualSettlementEdit.aspx.cs" Inherits="imq.kpei.derivatif.manualsettlementedit" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    
    <script type="text/javascript">
        // <![CDATA[
            function OnValidation(s, e) {
                var val = e.value;
                if (val == null || val == '')
                    return;
                if (val.length < 1)
                    e.isValid = false;

            }

            function alertMsg(msg) {
                alert(msg);
            }
            
        //]]>
    </script>

    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" 
                Text="Manual Settlement" /></div>
        
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="True" 
                            ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>

        <table>
            <tr>
                <td style="width:128px">Type<span style="color:Red">*</span></td>
                <td><dx:ASPxComboBox ID="cbType" runat="server" 
                        ClientInstanceName="cbType" EnableSynchronization="False" 
                        SelectedIndex="0" AutoPostBack="True" 
                        onselectedindexchanged="cbType_SelectedIndexChanged">
                    <Items>
                    </Items>
                    <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxComboBox></td>                 
            </tr>
            <tr>
                <td style="width:128px">AK<span style="color:Red">*</span></td>
                <td><dx:ASPxComboBox ID="cbMember" runat="server" DropDownStyle="DropDownList" 
                        ClientInstanceName="cbMember" EnableSynchronization="False" AutoPostBack="True" 
                        onselectedindexchanged="cbMember_SelectedIndexChanged">
                        <ClientSideEvents Validation="OnValidation" />
                        </dx:ASPxComboBox></td>                 
            </tr>
            <tr>
                <td style="width:128px">Bank<span style="color:Red">*</span></td>
                <td><dx:ASPxComboBox ID="cbBank" runat="server" DropDownStyle="DropDownList" 
                        ClientInstanceName="cbBank" EnableSynchronization="False" AutoPostBack="True" 
                        onselectedindexchanged="cbBank_SelectedIndexChanged">
                        <ClientSideEvents Validation="OnValidation" /></dx:ASPxComboBox></td>                 
            </tr>
            <tr>
                <td style="width:128px"><span lang="EN-US">Account Source</span><span style="color:Red">*</span></td>
                <td>
                    <dx:ASPxComboBox ID="cbSource" runat="server" DropDownStyle="DropDownList" 
                        ClientInstanceName="cbSource" EnableSynchronization="False" AutoPostBack="True"
                        onselectedindexchanged="cbSource_SelectedIndexChanged">
                        <ClientSideEvents Validation="OnValidation" /></dx:ASPxComboBox>
                </td>                 
                        
            </tr>
            <tr>
                <td style="width:128px"><span lang="EN-US">Account Target</span><span style="color:Red">*</span></td>
                <td>
                    <dx:ASPxComboBox ID="cbTarget" runat="server" DropDownStyle="DropDownList" 
                        ClientInstanceName="cbTarget" EnableSynchronization="False" AutoPostBack="True"
                        onselectedindexchanged="cbTarget_SelectedIndexChanged">
                        <ClientSideEvents Validation="OnValidation" /></dx:ASPxComboBox>
                </td>                        
            </tr>
            <tr>
                <td style="width:128px"><span lang="EN-US">Value</span></td>
                <td>
                    <dx:ASPxSpinEdit ID="txtValue" runat="server" Height="21px" Number="0"/>
                    <dx:ASPxTextBox ID="txtValue1" runat="server" Width="170px" Visible=false>
                        
                    </dx:ASPxTextBox>
                </td>                        
            </tr>
        </table>

            <br />
           
        <div>
            <table>
                <tr>
                    <td> <dx:ASPxButton ID="btnSave" runat="server" Text="Continue" AutoPostBack="false" onclick="btnSave_Click" Width="70px" /> </td>
                    <td><dx:ASPxButton ID="btnChecker" runat="server" Text="Check" AutoPostBack="false" onclick="btnChecker_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnApproval" runat="server" Text="Approve" AutoPostBack="false" onclick="btnApproval_Click" Width="70px" /></td>
                    <td><dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" onclick="btnReject_Click" Width="70px"/></td>
                    <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="True" onclick="btnCancel_Click" CausesValidation="false" Width="70px" /></td>
                   
                </tr>
            </table> 
        </div>
            <br />
            
            <dx:ASPxHiddenField ID="hfManualSettlement" runat="server" ClientInstanceName="hfManualSettlement" />
            
            
    </div>
       
</asp:Content>
