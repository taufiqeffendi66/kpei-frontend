﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.giveUpWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using Common.wsTransaction;

namespace imq.kpei.derivatif
{
    public partial class giveupform3 : System.Web.UI.Page
    {
        //private List<giveUpChildTemp> dataGUCT = null;

        private int addedit;
        ASPxGridView detailView;
        //private DataRow row;
        private String aUserLogin;
        private String aUserMember;
        private giveUpTemp aGiveUpTemp;
        private const String module = "Give Up";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }

                btnAdd.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnNext.Visible = false;
                btnPrev.Visible = false;
                btnReject.Visible = false;
                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = "New " + module; //New Direct        
                        ParseQueryString();
                        gvGiveUp.DataBind();
                        btnAdd.Visible = true;
                        btnNext.Visible = true;
                        btnPrev.Visible = true;
                        Permission(true);
                        break;

                    case 2:
                        lblTitle.Text = "New " + module; //New From Temporary
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 3:
                        lblTitle.Text = module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                }
            }
        }

        private void DisabledControl()
        {
            txtGuMemberId.Enabled = false;
            txtTuMemberId.Enabled = false;
            //gvGiveUp.Visible = false;
        }

        private void GetTemporary()
        {
            GiveUpWSService gws = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                giveUpTemp gut = gws.getTemporary(int.Parse(Request.QueryString["idxTmp"].ToString()));
                txtGuMemberId.Text = gut.guMemberId;
                txtTuMemberId.Text = gut.tuMemberId;

                gvGiveUp.Columns[0].Visible = false;
                gvGiveUp.DataSource = gut.giveUpChildTmps;
                gvGiveUp.DataBind();

                gvGiveUp.SettingsDetail.ShowDetailRow = true;
            }
            catch (Exception e)
            {
                if (gws != null)
                    gws.Abort();

                lblError.Text = e.Message;
            }
            finally
            {
                gws.Dispose();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, giveUpTemp gut)
        {
            string ret = "0";
            //string messages = String.Empty;
            const string target = @"../derivatif/GiveUpPage.aspx";
            //string resp = String.Empty;
            if (!giveUpTemp.ReferenceEquals(aGiveUpTemp, gut))
                aGiveUpTemp = gut;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2: //wsA.AddDirectApproval(dtA);
                    //string code = String.Empty;

                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = "1";
                            break;
                    }
                    break;
            }
            return ret;
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }


        protected void detailGrid_OnLoad(object sender, EventArgs e)
        {
            detailView = sender as ASPxGridView;
        }
        protected void detailGrid_DataBinding(object sender, EventArgs e)
        {
            try
            {
                detailView = sender as ASPxGridView;
                if (hdGiveUp.Get("sid") != null)
                    detailView.DataSource = getNetposition(hdGiveUp.Get("sid").ToString(), aUserMember);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        protected void detailGrid_DataSelect(object sender, EventArgs e)
        {
            hdGiveUp.Set("sid", (sender as ASPxGridView).GetMasterRowKeyValue());
        }

        private void ParseQueryString()
        {
            string guMemberId = Server.UrlDecode(Request.QueryString["guMemberId"].ToString());
            string tuMemberId = Server.UrlDecode(Request.QueryString["tuMemberId"].ToString());

            hdGiveUp.Set("guMemberId", guMemberId);
            hdGiveUp.Set("tuMemberId", tuMemberId);
            txtGuMemberId.Text = guMemberId;
            txtTuMemberId.Text = tuMemberId;
        }

        private clearingDto[] getNetposition(String sid, String memberId)
        {
            clearingDto[] list = null;
            UiDataService ws = null;
            try
            {
                ws = ImqWS.GetTransactionService();
                list = ws.getClearing(aUserMember.Trim(), memberId.Trim(), "", sid, "", "", "", "last");
            }
            catch (Exception ex)
            {
                if (ws != null)
                    ws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (ws != null)
                    ws.Dispose();
            }

            return list;
        }

        private clearingDto[] getValue()
        {
            clearingDto[] list = null;
            UiDataService ws = null;
            try
            {
                string guMemberId = txtGuMemberId.Text.Trim();
                ws = ImqWS.GetTransactionService();
                list = ws.getClearing(aUserMember.Trim(), guMemberId.Trim(), "", "", "", "", "", "last");

                list = list.GroupBy(m => m.sID).Select(g => g.First()).ToArray();
            }
            catch (Exception ex)
            {
                if (ws != null)
                    ws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (ws != null)
                    ws.Dispose();
            }

            return list;
        }


        //private clientAccount[] getValue()
        //{
        //    GiveUpWSService gws = null;
        //    clientAccount[] list = null;

        //    try
        //    {
        //        gws = ImqWS.GetGiveUpService();
        //        string guMemberId = txtGuMemberId.Text.Trim();

        //        list = gws.getAccount(guMemberId);
        //    }
        //    catch (Exception e)
        //    {
        //        gws.Abort();
        //        Debug.WriteLine(e.Message);
        //    }
        //    finally
        //    {
        //        gws.Dispose();
        //    }
        //    return list;
        //}

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            Response.Redirect("giveupform1.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            saveGiveUp(0);
        }

        private void saveGiveUp(int add)
        {
            GiveUpWSService gws = null;
            ApprovalService aps = null;
            giveUpTemp gut = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                aps = ImqWS.GetApprovalWebService();

                addedit = (int)Session["stat"];
                int idRec = -1;

                gut = new giveUpTemp();
                gut.tuMemberId = txtTuMemberId.Text;
                gut.guMemberId = txtGuMemberId.Text;
                idRec = gws.addToTemp(gut);
                //Nambah Child di Temp                
                aGiveUpTemp = gut;
                processSelection();
                saveSelection(idRec, gws);



                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/giveupform3.aspx", status = "M", memberId = aUserMember };

                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = module;
                            dtA.insertEdit = "I";
                            aGiveUpTemp.guNo = idRec;
                            break;
                    }

                    string retVal = "0";
                    string target = String.Empty;
                    retVal = rblApprovalChanged(dtA, aps, aGiveUpTemp);
                    if (retVal.Equals("1"))
                    {
                        giveUp gu = new giveUp() { guMemberId = aGiveUpTemp.guMemberId, tuMemberId = aGiveUpTemp.tuMemberId };

                        List<giveUpChild> listGuc = new List<giveUpChild>();
                        if (aGiveUpTemp.giveUpChildTmps.Length > 0)
                        {
                            giveUpChild guc = null;
                            giveUpChildPK gucpk = null;
                            foreach (giveUpChildTemp guct in aGiveUpTemp.giveUpChildTmps)
                            {
                                gucpk = new giveUpChildPK();
                                gucpk.sid = guct.id.sid;
                                guc = new giveUpChild();
                                guc.id = gucpk;
                                guc.tradingId = guct.tradingId;

                                listGuc.Add(guc);
                            }

                            gu.giveUpChilds = listGuc.ToArray();
                        }

                        if (add == 1)
                        {
                            Session["dtA"] = dtA;
                            Session["GIVE_UP"] = gu;
                            Response.Redirect("giveupform4.aspx?stat=0&approval=0", false);
                            Context.ApplicationInstance.CompleteRequest();
                        }
                        else
                        {
                            string strRet = "0";
                            strRet = gws.add(gu);
                            if (strRet.Equals("0"))
                            {
                                WriteAuditTrail("Direct Approval New " + module);
                                target = @"../derivatif/giveupform2.aspx?memberId=" + gu.guMemberId;
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                target = @"../derivatif/giveupform2.aspx?memberId=" + gu.guMemberId;
                                ShowMessage(retVal, target);
                            }
                            aps.AddDirectApproval(dtA);
                        }
                    }
                    else
                    {
                        ShowMessage("Failed On Save To Temporary Give Up", @"../derivatif/GiveUpPage.aspx");
                    }
                }
            }
            catch (Exception e)
            {
                if (gws != null)
                    gws.Abort();

                if (aps != null)
                    aps.Abort();

                Debug.WriteLine(e.Message);
            }
            finally
            {
                if (gws != null)
                    gws.Dispose();

                if (aps != null)
                    aps.Dispose();
            }
        }

        List<Object> selectedValues;
        List<giveUpChildTemp> listGCValues;
        private void processSelection()
        {
            List<String> fieldNames = new List<string>();
            foreach (GridViewColumn column in gvGiveUp.Columns)
                if (column is GridViewDataColumn)
                    fieldNames.Add(((GridViewDataColumn)column).FieldName);

            selectedValues = gvGiveUp.GetSelectedFieldValues(fieldNames.ToArray());
        }

        private void saveSelection(int  guNo, GiveUpWSService gws)
        {
            if (selectedValues == null)
                return;

            listGCValues = new List<giveUpChildTemp>();
            foreach (object[] item in selectedValues)
            {
                giveUpChildTemp gc = new giveUpChildTemp();
                giveUpChildTempPK gck = new giveUpChildTempPK() { guNo = guNo, sid = item[0].ToString().Trim() };

                gc.id = gck;
                gc.tradingId = item[1].ToString().Trim();
                listGCValues.Add(gc);
            }
            //giveUpChildTemp[] listgct = listGCValues.ToArray<giveUpChildTemp>();
            gws.addChildTemp(guNo, listGCValues.ToArray());
            aGiveUpTemp.giveUpChildTmps = listGCValues.ToArray();
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("GiveUpPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            saveGiveUp(1);
        }

        protected void gvGiveUp_DataBinding(object sender, EventArgs e)
        {
            gvGiveUp.DataSource = getValue();
        }

        protected void cbPage_Init(object sender, EventArgs e)
        {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;

            Boolean cbChecked = true;
            Int32 start = grid.VisibleStartIndex;
            Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
            end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);

            for (int i = start; i < end; i++)
                if (!grid.Selection.IsRowSelected(i))
                {
                    cbChecked = false;
                    break;
                }

            chk.Checked = cbChecked;
        }

        protected void gvGiveUp_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            Int32 start = grid.VisibleStartIndex;
            Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
            Int32 selectNumbers = 0;
            end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);

            for (int i = start; i < end; i++)
                if (grid.Selection.IsRowSelected(i))
                    selectNumbers++;

            e.Properties["cpSelectedRowsOnPage"] = selectNumbers;
            e.Properties["cpVisibleRowCount"] = grid.VisibleRowCount;
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            GiveUpWSService gws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;
            const string target = @"../account/approval.aspx";

            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                gws = ImqWS.GetGiveUpService();

                dtA = aps.Search(Request.QueryString["reffNo"].ToString());
                aGiveUpTemp = gws.getGiveUpTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = gws.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                //lblError.Text = ret;
                                //btnApproval.Visible = false;
                                //btnReject.Visible = false;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                //Response.Redirect("~/account/approval.aspx", false);
                                //Context.ApplicationInstance.CompleteRequest();
                                //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "alert('Success Add " + module + "!!')", true);
                                gws.deleteTempById(aGiveUpTemp.guNo);
                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();
                if (gws != null)
                    gws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                aps.Dispose();
                gws.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aGiveUpTemp));
        }

        private static string Log(giveUpTemp gut)
        {
            string strLog = String.Format("[Give Up Np : {0}, Give Up Member ID : {1}, Take Up Member ID : {2}]", gut.guNo, gut.guMemberId, gut.tuMemberId);
            return strLog;
        }
    }
}
