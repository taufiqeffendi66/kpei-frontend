﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.contractWS;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class seriescontract : System.Web.UI.Page
    {
        private series[] listSeries;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                if (!aUserMember.Equals("kpei"))
                {
                    //FileUpload1.Visible = false;
                    btnUpload.Visible = false;
                }
                gvContractList.DataBind();
                FillUnderlying();
                Permission();
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Series Contract");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        private series[] getWSContract()
        {
            ContractWSService ss = null;
            try
            {
                ss = ImqWS.GetContractService();
                listSeries = ss.get();
                cmbCode.DataSource = listSeries;
                cmbCode.DataBind();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                ss.Abort();
            }
            finally
            {
                ss.Dispose();
            }
            return listSeries;
        }

        private void FillUnderlying()
        {
            //UnderlyingWSService uws = null;
            ContractWSService cws = null;
            try
            {
                //uws = ImqWS.GetUnderlyingWebService();
                //underlying[] list = uws.get();
                cws = ImqWS.GetContractService();
                string[] list = cws.getUnderlyingDistinct();
                foreach (string s in list)
                    cmbUnderlying.Items.Add(s, s);
                //cmbUnderlying.DataSource = list;
                //cmbUnderlying.DataBind();
                //if (list != null)
                //{
                //    foreach (underlying u in list)
                //        cmbUnderlying.Items.Add(u.code, u.code);
                //}
            }
            catch (Exception e)
            {
                if (cws != null)
                    cws.Abort();
            }
            finally
            {
                if (cws != null)
                    cws.Dispose();
            }
        }

        protected void gvContractList_DataBinding(object sender, EventArgs e)
        {
            gvContractList.DataSource = getWSContract();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("contractinput.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            SendResponse("1");
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SendResponse("4");
        }


        private void SendResponse(string stat)
        {
            try
            {               
                string seriesCode = hfSeries.Get("seriesCode").ToString().Trim();
                string underlyingCode = hfSeries.Get("underlyingCode").ToString().Trim();
                string description;
                if (hfSeries.Get("description") != null)
                    description = hfSeries.Get("description").ToString().Trim();
                else
                    description = "";
                string seriesType = hfSeries.Get("seriesType").ToString().Trim();
                string strikePrice = hfSeries.Get("strikePrice").ToString().Trim();
                string openinterest = hfSeries.Get("openinterest").ToString().Trim();
                string optionType;
                if (hfSeries.Get("optionType") != null)
                    optionType = hfSeries.Get("optionType").ToString().Trim();
                else
                    optionType = "";

                string settlementMethod;
                if (hfSeries.Get("settlementMethod") != null)
                    settlementMethod = hfSeries.Get("settlementMethod").ToString().Trim();
                else
                    settlementMethod = "";

                string last_day_trans;
                if (hfSeries.Get("last_day_trans") != null)
                    last_day_trans = hfSeries.Get("last_day_trans").ToString().Trim();
                else
                    last_day_trans = "";
                string contractPeriod = hfSeries.Get("contractPeriod").ToString().Trim();
                string start_date = hfSeries.Get("start_date").ToString().Trim();
                string end_date = hfSeries.Get("end_date").ToString().Trim();
                string multiplier = hfSeries.Get("multiplier").ToString().Trim();

                string exerciseTimeFrom;
                if (hfSeries.Get("exerciseTimeFrom") != null)
                    exerciseTimeFrom = hfSeries.Get("exerciseTimeFrom").ToString().Trim();
                else
                    exerciseTimeFrom = "";


                string exerciseTimeTo;
                if (hfSeries.Get("exerciseTimeTo") != null)
                    exerciseTimeTo = hfSeries.Get("exerciseTimeTo").ToString().Trim();
                else
                    exerciseTimeTo = "";
                string exerciseStyle = hfSeries.Get("exerciseStyle").ToString().Trim();
                string marginingType = hfSeries.Get("marginingType").ToString().Trim();
                string idx = hfSeries.Get("idx").ToString().Trim();
                string seriesStatus;
                if (hfSeries.Get("seriesStatus") != null)
                    seriesStatus = hfSeries.Get("seriesStatus").ToString().Trim();
                else
                    seriesStatus = "";

                string instrumentCode;
                if (hfSeries.Get("instrumentCode") != null)
                    instrumentCode = hfSeries.Get("instrumentCode").ToString().Trim();
                else
                    instrumentCode = "";

                string remark;
                if (hfSeries.Get("remark") != null)
                    remark = hfSeries.Get("remark").ToString().Trim();
                else
                    remark = "";

                string remark2;
                if (hfSeries.Get("remark2") != null)
                    remark2 = hfSeries.Get("remark2").ToString().Trim();
                else
                    remark2 = "";

                string template_param_id = hfSeries.Get("template_param_id") != null ? hfSeries.Get("template_param_id").ToString().Trim() : "";
                string gf_fee = hfSeries.Get("gf_fee") != null ? hfSeries.Get("gf_fee").ToString().Trim() : "";
                string minimum_fee = hfSeries.Get("minimum_fee") != null ? hfSeries.Get("minimum_fee").ToString().Trim() : "";
                string clearing_fee_type = hfSeries.Get("clearing_fee_type") != null ? hfSeries.Get("clearing_fee_type").ToString().Trim() : "F";
                string clearing_fee_calculation = hfSeries.Get("clearing_fee_calculation") != null ? hfSeries.Get("clearing_fee_calculation").ToString().Trim() : "";
                string clearing_fee_value = hfSeries.Get("clearing_fee_value") != null ? hfSeries.Get("clearing_fee_value").ToString().Trim() : "";
                string tick_size = hfSeries.Get("tick_size") != null ? hfSeries.Get("tick_size").ToString().Trim() : "";
                string tick_value = hfSeries.Get("tick_value") != null ? hfSeries.Get("tick_value").ToString().Trim() : "";
                string margin_type = hfSeries.Get("margin_type") != null ? hfSeries.Get("margin_type").ToString().Trim() : "F";
                string margin_value = hfSeries.Get("margin_value") != null ? hfSeries.Get("margin_value").ToString().Trim() : "";

                string strRedirect = String.Format("contractinput.aspx?stat={0}&code={1}&underlyingcode={2}&description={3}" + 
                    "&type={4}&strikeprice={5}&openinterest={6}&option={7}&settlement={8}&lastday={9}&period={10}&start={11}"+ 
                    "&end={12}&multiplier={13}&exerciseTimeFrom={14}&exerciseTimeTo={15}&exercisestyle={16}&idx={17}"+
                    "&seriesStatus={18}&instrumentCode={19}&remark={20}&remark2={21}&margining={22}" +
                    "&template_param_id={23}&gf_fee={24}&minimum_fee={25}&clearing_fee_type={26}&clearing_fee_calculation={27}"+
                    "&clearing_fee_value={28}&tick_size={29}&tick_value={30}&margin_type={31}&margin_value={32}",
                    stat, Server.UrlEncode(seriesCode), Server.UrlEncode(underlyingCode), Server.UrlEncode(description), 
                    Server.UrlEncode(seriesType), Server.UrlEncode(strikePrice), Server.UrlEncode(openinterest), Server.UrlEncode(optionType), 
                    Server.UrlEncode(settlementMethod), Server.UrlEncode(last_day_trans), Server.UrlEncode(contractPeriod), 
                    Server.UrlEncode(start_date), Server.UrlEncode(end_date), Server.UrlEncode(multiplier), 
                    Server.UrlEncode(exerciseTimeFrom), Server.UrlEncode(exerciseTimeTo), Server.UrlEncode(exerciseStyle), 
                    Server.UrlEncode(idx), Server.UrlEncode(seriesStatus), Server.UrlEncode(instrumentCode), Server.UrlEncode(remark), 
                    Server.UrlEncode(remark2), Server.UrlEncode(marginingType), Server.UrlEncode(template_param_id),Server.UrlEncode(gf_fee),
                    Server.UrlEncode(minimum_fee),Server.UrlEncode(clearing_fee_type),Server.UrlEncode(clearing_fee_calculation),
                    Server.UrlEncode(clearing_fee_value), Server.UrlEncode(tick_size), Server.UrlEncode(tick_value), Server.UrlEncode(margin_type), Server.UrlEncode(margin_value));

                Response.Redirect(strRedirect, false);
                Context.ApplicationInstance.CompleteRequest();
            }
            catch (NullReferenceException nre)
            {
                Debug.WriteLine(nre.Message);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string contractCode = cmbCode.Text.Trim();
            string underlyingCode = cmbUnderlying.Text.Trim();
            string search = String.Empty;

            if (contractCode != String.Empty)
            {
                contractCode = contractCode.Replace("'", "''");
                contractCode = contractCode.Replace("--", "-");

                contractCode = String.Format("[seriesCode] like '%{0}%'", contractCode);
            }

            if (underlyingCode != String.Empty)
            {
                underlyingCode = underlyingCode.Replace("'", "''");
                underlyingCode = underlyingCode.Replace("--", "-");

                underlyingCode = String.Format("[underlyingCode] like '%{0}%'", underlyingCode);
            }

            if (contractCode != String.Empty)
            {
                if (underlyingCode != String.Empty)
                    search = String.Format("{0} and {1}", contractCode, underlyingCode);
                else
                    search = contractCode;
            }
            else
            {
                if (underlyingCode != String.Empty)
                    search = underlyingCode;
                else
                    search = String.Empty;
            }
            gvContractList.FilterExpression = search;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Response.Redirect("ContractUpload.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
            //            byte[] data = null;
            //            ContractWSService ws = null;
            //            string strRet = "0";

            //            try
            //            {
            //                if (FileUpload1.HasFile)
            //                {
            //                    data = FileUpload1.FileBytes;

            //                    if (data != null)
            //                    {
            //                        ws = new ContractWSService();
            //                        strRet = ws.UploadFile(data);
            //                        if (!strRet.Equals("0"))
            //                        {
            //                            string mystring = @"<script type='text/javascript'>
            //                                jAlert('" + strRet + @"','Information',function()
            //                                {window.location='seriescontract.aspx'});
            //                                </script>";
            //                            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
            //                                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
            //                        }
            //                        gvContractList.DataBind();
            //                    }
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                if(ws != null)
            //                    ws.Abort();
            //                lblError.Text = ex.Message;
            //                string mystring = @"<script type='text/javascript'>
            //                                jAlert('" + ex.Message + @"','Information',function()
            //                                {window.location='seriescontract.aspx'});
            //                                </script>";
            //                if (!ClientScript.IsStartupScriptRegistered("clientScript"))
            //                    ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
            //            }
            //            finally
            //            {
            //                if (ws != null)
            //                    ws.Dispose();
            //                data = null;
            //            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }
    }
}
