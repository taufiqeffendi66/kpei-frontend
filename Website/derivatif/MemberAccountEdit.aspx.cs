﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

using DevExpress.Web.ASPxClasses;
using Common;
using Common.memberInfoWS;
using Common.bankWS;
using Common.memberAccountWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class memberaccountedit : System.Web.UI.Page
    {
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private memberAccountTmp aMemberAccountTemp;
        private const String module = "Member Account";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            ASPxWebControl.RegisterBaseScript(Page);
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"]));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                Session["stat"] = addedit;
                if (Session["stat"] == null)
                {
                    Response.Redirect("MemberStatusPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                FillBank();
                FillMember();

                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = String.Format("New {0} Number", module); //New Direct
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lblTitle.Text = String.Format("Edit {0} Number", module); //Edit Direct
                        cmbMember.Enabled = false;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = String.Format("New {0} Number", module); //New From Temporary
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3:
                        lblTitle.Text = String.Format("Edit {0} Number", module);
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 4:
                        lblTitle.Text = String.Format("Delete {0} Number", module);
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }
            }
        }

        private void DisabledControl()
        {
            cmbMember.Enabled = false;
            cmbBank.Enabled = false;
            txtAccount.Enabled = false;
            txtCollateral.Enabled = false;
            txtSecurityDeposit.Enabled = false;
            txtSettlement.Enabled = false;
            txtSID.Enabled = false;
            txtTrading.Enabled = false;
            txtFree.Enabled = false;
        }

        private void GetTemporary()
        {
            MemberAccountWSService mas = null;
            try
            {
                mas = ImqWS.getMemberAccountService();
                memberAccountTmp mat = mas.getMemberAccountTemp(int.Parse(Request.QueryString["idxTmp"]));
                //cmbMember.SelectedItem = cmbMember.Items.FindByValue(mat.memberId.Trim());
                cmbMember.Text = mat.memberId.Trim();
                cmbBank.SelectedItem = cmbBank.Items.FindByValue(mat.bankCode.Trim());
                txtSID.Text = mat.sid;
                txtTrading.Text = mat.tradingId;
                txtAccount.Text = mat.accountId;
                txtSecurityDeposit.Text = mat.securityDepositAccountNo;
                txtCollateral.Text = mat.collateralAccountNo;
                txtSettlement.Text = mat.settlementAccountNo;
                txtFree.Text = mat.freeAccountNo;
            }
            catch (Exception e)
            {
                if (mas != null)
                    mas.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                mas.Dispose();
            }
        }

        private void FillBank()
        {
            BankWSService bs = null;
            try
            {
                bs = ImqWS.GetBankWebService();
                Common.bankWS.bank[] banks = bs.get();
                foreach (Common.bankWS.bank bk in banks)
                {
                    cmbBank.Items.Add(bk.bankCode.Trim(), bk.bankCode.Trim());
                }
            }
            catch (Exception e)
            {
                e.ToString();
                bs.Abort();
            }
            finally
            {
                bs.Dispose();
            }
        }

        protected void FillMember()
        {
            MemberInfoWSService mis = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                Common.memberInfoWS.memberInfo[] members = mis.get();
                foreach (Common.memberInfoWS.memberInfo member in members)
                {
                    cmbMember.Items.Add(member.memberId.Trim(), member.memberId.Trim());
                }
            }
            catch (Exception ex)
            {
                mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                mis.Dispose();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            string member_id = Server.UrlDecode(Request.QueryString["member_id"].ToString().Trim());
            string sid = Server.UrlDecode(Request.QueryString["sid"].ToString().Trim());
            string trading_id = Server.UrlDecode(Request.QueryString["trading_id"].ToString().Trim());
            string account_id = Server.UrlDecode(Request.QueryString["account_id"].ToString().Trim());
            string role = Server.UrlDecode(Request.QueryString["role"].Trim());
            string bank_id = Server.UrlDecode(Request.QueryString["bankcode"].ToString().Trim());
            string sec_dep_acc_no = Server.UrlDecode(Request.QueryString["sec_dep_acc_no"].ToString().Trim());
            string collateral_acc_no = Server.UrlDecode(Request.QueryString["collateral_acc_no"].ToString().Trim());
            string settlement_acc_no = Server.UrlDecode(Request.QueryString["settlement_acc_no"].ToString().Trim());
            string free_acc_no = Server.UrlDecode(Request.QueryString["free_acc_no"].ToString().Trim());
            string idx = Server.UrlDecode(Request.QueryString["idx"].ToString().Trim());

            hdMemberAcc.Set("member_id", member_id);
            hdMemberAcc.Set("idx", idx);

            cmbMember.Text = member_id;
            txtSID.Text = sid;
            txtTrading.Text = trading_id;
            txtAccount.Text = account_id;
            cmbBank.Text = bank_id;
            txtSecurityDeposit.Text = sec_dep_acc_no;
            txtCollateral.Text = collateral_acc_no;
            txtSettlement.Text = settlement_acc_no;
            txtFree.Text = free_acc_no;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ApprovalService aps = null;
            MemberAccountWSService mas = null;

            try
            {
                mas = ImqWS.getMemberAccountService();
                aps = ImqWS.GetApprovalWebService();

                addedit = (int)Session["stat"];
                int idRec;

                memberAccountTmp mat = new memberAccountTmp() { memberId = cmbMember.Text, tradingId = txtTrading.Text.Trim(), sid = txtSID.Text.Trim(), accountId = txtAccount.Text.Trim(), bankCode = cmbBank.Text, role = "H", securityDepositAccountNo = txtSecurityDeposit.Text.Trim(), collateralAccountNo = txtCollateral.Text.Trim(), settlementAccountNo = txtSettlement.Text.Trim(), freeAccountNo = txtFree.Text.Trim() };
                idRec = mas.addToTemp(mat);
                aMemberAccountTemp = mat;

                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/MemberAccountEdit.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            aMemberAccountTemp.idx = idRec;
                            break;

                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hdMemberAcc.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hdMemberAcc.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }
                    string retVal = "0";
                    retVal = rblApprovalChanged(dtA, aps, mat);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/MemberAccountPage.aspx");
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Member Account", @"../derivatif/MemberAccountPage.aspx");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
                if (mas != null)
                    mas.Abort();

                if (aps != null)
                    aps.Abort();
            }
            finally
            {
                if (mas != null)
                    mas.Dispose();
                if (aps != null)
                    aps.Dispose();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("MemberAccountPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            MemberAccountWSService mas = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            string target = @"../administration/approval.aspx";

            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                mas = ImqWS.getMemberAccountService();

                dtA = aps.Search(Request.QueryString["reffNo"]);
                aMemberAccountTemp = mas.getMemberAccountTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                        //dtA[0].approvelName = aUserLogin;
                        //dtA[0].approvelStatus = "Approved";
                        //dtA[0].memberId = aUserMember;
                        //dtA[0].status = "A";
                        //aps.UpdateByApprovel(dtA[0]);

                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }

                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New  {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = mas.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = mas.deleteTempById(aMemberAccountTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;
                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = mas.updateFromTemp(aMemberAccountTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = mas.deleteTempById(aMemberAccountTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = mas.deleteById(dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = mas.deleteTempById(aMemberAccountTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;
                                ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (mas != null)
                    mas.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
                if (mas != null)
                    mas.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aMemberAccountTemp));
        }

        private static String Log(memberAccountTmp mat)
        {
            string strLog = String.Format("[Member ID : {0}, SID : {1}, trading ID : {2}, Collateral Account No : {3}, Security Deposit Account No : {4}, Settlement Account No : {5}, Operational Account No : {6}]", mat.memberId, mat.sid, mat.tradingId, mat.collateralAccountNo, mat.securityDepositAccountNo, mat.settlementAccountNo, mat.freeAccountNo);
            return strLog;
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, memberAccountTmp mat)
        {
            string ret = "0";
            string messages = String.Empty;
            const string target = @"../derivatif/MemberAccountPage.aspx";
            string resp = String.Empty;
            if (!memberAccountTmp.ReferenceEquals(aMemberAccountTemp, mat))
                aMemberAccountTemp = mat;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    MemberAccountWSService mas = ImqWS.getMemberAccountService();

                    Common.memberAccountWS.bank b = new Common.memberAccountWS.bank() { bankCode = aMemberAccountTemp.bankCode };
                    memberAccount ma = new memberAccount() { accountId = aMemberAccountTemp.accountId, bank = b, collateralAccountNo = aMemberAccountTemp.collateralAccountNo, memberId = aMemberAccountTemp.memberId, role = "H", securityDepositAccountNo = aMemberAccountTemp.securityDepositAccountNo, settlementAccountNo = aMemberAccountTemp.settlementAccountNo, freeAccountNo = aMemberAccountTemp.freeAccountNo, sid = aMemberAccountTemp.sid, status = "a", tradingId = aMemberAccountTemp.tradingId };

                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = mas.add(ma);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "E":
                            ret = mas.update(hdMemberAcc.Get("member_id").ToString(), ma);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "R":
                            ret = mas.delete(hdMemberAcc.Get("member_id").ToString());
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Delete " + module);
                                ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }
                    break;
            }

            return ret;
        }
    }
}
