﻿<%@ Page Title="Payment Bank" Language="C#" AutoEventWireup="true" CodeBehind="BankPage.aspx.cs" Inherits="imq.kpei.derivatif.bankpage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
   
     <script language="javascript" type="text/javascript">
        // <![CDATA[

         function OnGridFocusedRowChanged() 
         {
             grid.GetRowValues(grid.GetFocusedRowIndex(), 'bankCode;name;contactPerson;branch;address;phone;fax;email;idx;', OnGetRowValues);

         }
         
         function OnGetRowValues(values) {
             //clID.SetText(values[0]);
             //clName.SetText(values[1]);
             hfbank.Set("code", values[0]);
             hfbank.Set("name", values[1]);
             hfbank.Set("contact", values[2]);
             hfbank.Set("branch", values[3]);
             hfbank.Set("address", values[4]);
             hfbank.Set("phone", values[5]);
             hfbank.Set("fax", values[6]);
             hfbank.Set("email", values[7]);
             hfbank.Set("idx", values[8]);

             /*if (hfbank.Get("code") != undefined) {
                 btndel.SetEnabled(true);
                 btned.SetEnabled(true);
             } else {
                 btndel.SetEnabled(false);
                 btned.SetEnabled(false);
             }*/
             
         }

         function ConfirmDelete(s, e) {
             e.processOnServer = confirm("Are you sure delete this Bank \nCode = " + hfbank.Get("code") + ",\nName = " + hfbank.Get("name"));            
         }

         function view(s, e) {             
             window.open("BankView.aspx" + "?code=" + cmbCode.GetText() + "&bankName=" + cmbName.GetText(), "", "fullscreen=yes;scrollbars=auto");
         }

        // ]]>
    </script>   
    <div class="content">
               <div class="title"> Payment Bank</div>
                
                <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
                <table>
                    <tr>
                        <td style="width:128px" nowrap="nowrap">Code</td>
                        <td><dx:ASPxComboBox ID="cmbID" runat="server" IncrementalFilteringMode="Contains"
                            DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbCode">
                                <DropDownButton Visible="false"></DropDownButton>
                            </dx:ASPxComboBox> </td>
                        <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" AutoPostBack="false" 
                                onclick="btnSearch_Click" >
                        
                             </dx:ASPxButton>
                                </td>
                        <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" 
                                onclick="btnExport_Click">
                                <ClientSideEvents Click="view"/>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:128px" nowrap="nowrap">Bank Name</td>
                        <td><dx:ASPxComboBox ID="cmbName" runat="server" IncrementalFilteringMode="Contains"
                            DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbName">
                            <DropDownButton Visible="false"></DropDownButton>
                        </dx:ASPxComboBox></td>                        
                    </tr>            
                </table>
            
                    <br />
            
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnNew" runat="server" Text="New" AutoPostBack="false" 
                                    onclick="btnNew_Click" />                        
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" AutoPostBack="false"
                                    onclick="btnEdit_Click" ClientInstanceName="btned" />
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" AutoPostBack="false" 
                                    onclick="btnDelete_Click" ClientInstanceName="btndel" 
                                    ClientSideEvents-Click="ConfirmDelete" />
                            </td>
                    
                        </tr>
                    </table>
            
                    <dx:ASPxGridView ID="gvBank" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                    CssClass="grid" ondatabinding="gvBank_DataBinding" KeyFieldName="bankCode">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Code" VisibleIndex="0" FieldName="bankCode" />
                        <dx:GridViewDataTextColumn Caption="Bank Name" VisibleIndex="1" FieldName="name" />           
                        <dx:GridViewDataTextColumn Caption="Contact Person" VisibleIndex="2" FieldName="contactPerson" 
                            PropertiesTextEdit-NullText=" ">
                            <PropertiesTextEdit NullText=" "></PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Branch" VisibleIndex="3" FieldName="branch" 
                              PropertiesTextEdit-NullText=" ">
                            <PropertiesTextEdit NullText=" "></PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Address" VisibleIndex="4" 
                            FieldName="address"   />
                        <dx:GridViewDataTextColumn Caption="Phone" VisibleIndex="5" FieldName="phone" 
                            PropertiesTextEdit-NullText=" ">
                            <PropertiesTextEdit NullText=" "></PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Fax" VisibleIndex="6" FieldName="fax" 
                            PropertiesTextEdit-NullText=" ">
                            <PropertiesTextEdit NullText=" "></PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email" VisibleIndex="7" FieldName="email" 
                            PropertiesTextEdit-NullText=" ">   
                            <PropertiesTextEdit NullText=" "></PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="idx" VisibleIndex="8" FieldName="idx" Visible="false"
                            PropertiesTextEdit-NullText=" ">            
                            <PropertiesTextEdit NullText=" "></PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <Settings ShowGroupButtons="False"  ShowVerticalScrollBar="True" />
                        <SettingsCustomizationWindow Height="100%" />
                    <Styles>
                        <AlternatingRow Enabled="True" />
                        <Header CssClass="gridHeader" />
                    </Styles>
                    <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
                    <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                     <SettingsPager PageSize="20">
                    </SettingsPager>
                </dx:ASPxGridView>
           
    </div>
       
        <dx:ASPxHiddenField ID="hfBank" runat="server" ClientInstanceName="hfbank" />
       
    
</asp:Content>
