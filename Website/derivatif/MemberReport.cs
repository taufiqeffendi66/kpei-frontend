﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using DevExpress.XtraReports.UI;
using Common;
using Common.memberInfoWS;

namespace imq.kpei.derivatif
{
    public partial class MemberReport : DevExpress.XtraReports.UI.XtraReport
    {
        private memberInfo[] listmember;
        public String aMember;
        public String aName;

        public MemberReport()
        {
            InitializeComponent();
        }

        private void GetMember()
        {
            MemberInfoWSService mis = null;

            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                listmember = mis.Search(aMember, aName);
                if (listmember != null)
                {
                    DataSource = listmember;
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "memberId");
                    cellMember.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "memberName");
                    cellName.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "memberTypeName");
                    cellType.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "statusName");
                    cellStatus.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "npwp");
                    cellNPWP.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "address");
                    cellAddress.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "phone");
                    cellPhone.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "fax");
                    cellFax.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "contactPerson");
                    cellContact.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "email");
                    cellEmail.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "adminUser");
                    cellAdmin.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sktNo");
                    cellSktNo.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sktDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellSktDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "activeDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellActive.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(e.ToString());
                //lblError.Text = e.Message;
            }
            finally
            {
                mis.Dispose();
            }
        }

        private void MemberReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetMember();
        }
    }
}
