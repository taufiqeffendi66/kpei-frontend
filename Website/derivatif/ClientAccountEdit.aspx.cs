﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using DevExpress.Web.ASPxClasses;
using Common;

//using Common.memberInfoWS;
using Common.bankWS;
using Common.clientAccountWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class ClientAccountEdit : System.Web.UI.Page
    {
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private clientAccountTmp aClientAccountTemp;
        private String module = "Client Account";
        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxWebControl.RegisterBaseScript(Page);
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                
                if (Session["stat"] == null)
                {
                    Response.Redirect("ClientAccountPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                FillBank();
                FillMember();

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = String.Format("New {0} Number", module); //New Direct
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lblTitle.Text = String.Format("Edit {0} Number", module); //Edit Direct
                        btnSave.Visible = true;
                        cmbMember.Enabled = false;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = String.Format("New {0} Number", module); //New From Temporary
                        Permission(false);

                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3:
                        lblTitle.Text = String.Format("Edit {0} Number", module);
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 4:
                        lblTitle.Text = String.Format("Delete {0} Number", module);
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }
            }
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {

                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void DisabledControl()
        {
            cmbMember.Enabled = false;
            txtAccount.Enabled = false;
            txtCollateral.Enabled = false;
            txtSID.Enabled = false;
            txtTrading.Enabled = false;
            cmbBank.Enabled = false;
        }

        private void GetTemporary()
        {
            ClientAccountWSService cas = null;
            try
            {
                cas = ImqWS.GetClientAccountService();
                clientAccountTmp cat = cas.getClientAccountTemp(int.Parse(Request.QueryString["idxTmp"].ToString()));
                cmbMember.Text = cat.memberId;
                cmbBank.Text = cat.bankCode;
                txtAccount.Text = cat.accountId;
                txtCollateral.Text = cat.collateralAccountNo;
                txtSID.Text = cat.sid;
                txtTrading.Text = cat.tradingId;

                
            }
            catch (Exception e)
            {
                if (cas != null)
                    cas.Abort();

                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                cas.Dispose();
            }
        }

        private void FillBank()
        {
            BankWSService bs = null;
            try
            {
                bs = ImqWS.GetBankWebService();
                Common.bankWS.bank[] lbank = bs.get();
                if (lbank != null)
                {
                    foreach (Common.bankWS.bank b in lbank)
                        cmbBank.Items.Add(b.bankCode.Trim(), b.bankCode.Trim());
                }
            }
            catch (Exception e)
            {
                if (bs != null)
                    bs.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                bs.Dispose();
            }
        }

        private void FillMember()
        {
            ClientAccountWSService cas = null;
            try
            {
                cas = ImqWS.GetClientAccountService();
                clientAccount[] members = cas.getMemberRoleHouse();
                if (members != null)
                {
                    foreach (clientAccount member in members)
                        cmbMember.Items.Add(member.id.memberId.Trim(), member.id.memberId.Trim());
                }
              
            }
            catch (Exception ex)
            {
                if (cas != null)
                    cas.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                cas.Dispose();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            string member_id = Server.UrlDecode(Request.QueryString["member_id"].ToString().Trim());
            string sid = Server.UrlDecode(Request.QueryString["sid"].ToString().Trim());
            string trading_id = Server.UrlDecode(Request.QueryString["trading_id"].ToString().Trim());
            string account_id = Server.UrlDecode(Request.QueryString["account_id"].ToString().Trim());
            string role = Server.UrlDecode(Request.QueryString["role"].ToString().Trim());
            string bank_id = Server.UrlDecode(Request.QueryString["bankcode"].ToString().Trim());
            string idx = Server.UrlDecode(Request.QueryString["idx"].ToString().Trim());            
            string collateral_acc_no = Server.UrlDecode(Request.QueryString["collateral_acc_no"].ToString().Trim());            

            hfClientAcc.Set("member_id", member_id);
            hfClientAcc.Set("trading_id", trading_id);
            hfClientAcc.Set("idx", idx);
            cmbMember.Text = member_id;

            txtSID.Text = sid;
            txtTrading.Text = trading_id;
            txtAccount.Text = account_id;
            cmbBank.Text = bank_id;
            
            txtCollateral.Text = collateral_acc_no;
            
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            ClientAccountWSService cas = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";

            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                cas = ImqWS.GetClientAccountService();

                dtA = aps.Search(Request.QueryString["reffNo"].ToString());
                aClientAccountTemp = cas.getClientAccountTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New  {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cas.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cas.deleteTempById(aClientAccountTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cas.updateFromTemp(aClientAccountTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cas.deleteTempById(aClientAccountTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = cas.deleteById(dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = cas.deleteTempById(aClientAccountTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (cas != null)
                    cas.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (cas != null)
                    cas.Dispose();
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            ApprovalService aps = null;
            ClientAccountWSService cas = null;
            try
            {
                aps = ImqWS.GetApprovalWebService();
                cas = ImqWS.GetClientAccountService();
                clientAccountTmp cat = new clientAccountTmp() { memberId = cmbMember.Text, tradingId = txtTrading.Text.Trim(), bankCode = cmbBank.Text, accountId = txtAccount.Text.Trim(), sid = txtSID.Text.Trim(), collateralAccountNo = txtCollateral.Text.Trim(), role = "C" };

                addedit = (int)Session["stat"];
                int idRec = cas.addToTemp(cat);
                aClientAccountTemp = cat;

                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval();
                    dtA.type = "c";
                    dtA.makerDate = DateTime.Now;
                    dtA.makerName = aUserLogin;
                    dtA.makerStatus = "Maked";
                    dtA.checkerStatus = "To Be Check";
                    dtA.idxTmp = idRec;
                    dtA.form = "~/derivatif/ClientAccountEdit.aspx";
                    dtA.status = "M";
                    dtA.memberId = aUserMember;
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            aClientAccountTemp.idx = idRec;
                            break;

                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfClientAcc.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfClientAcc.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }
                    string retVal = "0";
                    retVal = rblApprovalChanged(dtA, aps, aClientAccountTemp);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/ClientAccountPage.aspx");
                    }
                }
                else
                {
                    ShowMessage("message=Failed On Save To Temporary Client Account", @"../derivatif/ClientAccountPage.aspx");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                lblError.Text = ex.Message;
                if (cas != null)
                    cas.Abort();

                if (aps != null)
                    aps.Abort();
            }
            finally
            {
                if (cas != null)
                    cas.Dispose();
                if (aps != null)
                    aps.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aClientAccountTemp));
        }

        private String Log(clientAccountTmp cat)
        {
            string strLog = String.Empty;
            strLog = "[Member ID : " + cat.memberId +
                     ", SID : " + cat.sid +
                     ", Trading ID : " + cat.tradingId +
                     ", Collateral Account No : " + cat.collateralAccountNo +
                     "]";
            return strLog;
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, clientAccountTmp cat)
        {
            string ret = "0";
            const string target = @"../derivatif/ClientAccountPage.aspx";

            if (!clientAccountTmp.ReferenceEquals(aClientAccountTemp, cat))
                aClientAccountTemp = cat;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    ClientAccountWSService cas = ImqWS.GetClientAccountService();
                    Common.clientAccountWS.bank b = new Common.clientAccountWS.bank();
                    b.bankCode = cat.bankCode;
                    clientAccountPK cap = new clientAccountPK() { memberId = cat.memberId, tradingId = cat.tradingId };
                    clientAccount ca = new clientAccount() { accountId = cat.accountId, bank = b, collateralAccountNo = cat.collateralAccountNo, id = cap, role = "C", sid = cat.sid };
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = cas.add(ca);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "E":
                            ret = cas.update(hfClientAcc.Get("member_id").ToString(), hfClientAcc.Get("trading_id").ToString(), ca);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "R":
                            ret = cas.delete(hfClientAcc.Get("member_id").ToString(), hfClientAcc.Get("trading_id").ToString());
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Delete " + module);
                                ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }

                    break;
            }

            return ret;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("ClientAccountPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }

        }
    }
}
