﻿<%@ Page Language="C#"  Title="Contract List & Detail" AutoEventWireup="true" CodeBehind="seriescontract.aspx.cs" Inherits="imq.kpei.derivatif.seriescontract" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
    
    <asp:Content ID="content1" ContentPlaceHolderID="main" runat="server">       
        <script type="text/javascript">
        // <![CDATA[
            function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(),
                    'seriesCode;underlyingCode;description;seriesType;strikePrice;optionType;' +
                    'settlementMethod;lastDayTransaction;period;startDate;endDate;multiplier;exerciseTimeFrom;' +
                    'exerciseTimeTo;exerciseStyle;marginingType;idx;openinterest;seriesStatus;instrumentCode;remark;remark2;' +
                    'template_param_id;gf_fee;minimum_fee;clearing_fee_type;clearing_fee_calculation;clearing_fee_value;' +
                    'tick_size;tick_value;margin_type;margin_value;',  
                    OnGetRowValues);
            }

            function OnGetRowValues(values) {
                hfseries.Set("seriesCode", values[0]);
                hfseries.Set("underlyingCode", values[1]);
                hfseries.Set("description", values[2]);
                hfseries.Set("seriesType", values[3]);
                hfseries.Set("strikePrice", values[4]);
                hfseries.Set("optionType", values[5]);
                hfseries.Set("settlementMethod", values[6]);
                hfseries.Set("last_day_trans", values[7]);
                hfseries.Set("contractPeriod", values[8]);
                hfseries.Set("start_date", values[9]);
                hfseries.Set("end_date", values[10]);
                hfseries.Set("multiplier", values[11]);
                hfseries.Set("exerciseTimeFrom", values[12]);
                hfseries.Set("exerciseTimeTo", values[13]);
                hfseries.Set("exerciseStyle", values[14]);
                hfseries.Set("marginingType", values[15]);
                hfseries.Set("idx", values[16]);
                hfseries.Set("openinterest", values[17]);
                hfseries.Set("seriesStatus", values[18]);
                hfseries.Set("instrumentCode", values[19]);
                hfseries.Set("remark", values[20]);
                hfseries.Set("remark2", values[21]);
                hfseries.Set("template_param_id", values[22]);
                hfseries.Set("gf_fee", values[23]);
                hfseries.Set("minimum_fee", values[24]);
                hfseries.Set("clearing_fee_type", values[25]);
                hfseries.Set("clearing_fee_calculation", values[26]);
                hfseries.Set("clearing_fee_value", values[27]);
                hfseries.Set("tick_size", values[28]);
                hfseries.Set("tick_value", values[29]);
                hfseries.Set("margin_type", values[30]);
                hfseries.Set("margin_value", values[31]);
                //txtcode.SetText(values[0]);
                //txtunderlying.SetText(values[1]);
                /*
                if (hfseries.Get("seriesCode") != undefined) {
                    btndel.SetEnabled(true);
                    btned.SetEnabled(true);
                } else {
                    btndel.SetEnabled(false);
                    btned.SetEnabled(false);
                }
                */
            }

            function ConfirmDelete(s, e) {
                e.processOnServer = confirm("Are you sure delete this contract \ncode : " + hfseries.Get("seriesCode") + " ?");
            }

            function view(s, e) {
                window.open("SeriesView.aspx?series=" + cmbCode.GetText() + "&underlying=" + cmbUnderlying.GetText(), "", "fullscreen=yes;scrollbars=auto");
            }
        // ]]>
        </script>
       
        <div  class="content">
            
            <div class="title"><h3>Contract List & Detail</h3></div>
            <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
            <div class="maincontent">
                <table id="tblContract">
                    <tr>
                        <td style="width:128px" nowrap="nowrap">Contract Code</td>
                        <td><dx:ASPxComboBox ID="cmbCode" runat="server" IncrementalFilteringMode="Contains"
                            DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbCode" TextField="seriesCode"
                            ValueField="seriesCode" ValueType="System.String" EnableCallbackMode="true" CallbackPageSize="10"
                            >
                                <DropDownButton Visible="false"></DropDownButton>
                            </dx:ASPxComboBox> 
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
                        </td>
                        <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export" 
                                onclick="btnExport_Click">
                                <ClientSideEvents Click="view"/>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">Underlying Code</td>
                        <td>
                           <dx:ASPxComboBox ID="cmbUnderlying" runat="server" IncrementalFilteringMode="Contains"
                            DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbUnderlying" TextField="underlyingCode"
                            EnableCallbackMode="true" CallbackPageSize="10" ValueField="underlyingCode" ValueType="System.String">
                                <DropDownButton Visible="false"></DropDownButton>
                            </dx:ASPxComboBox> 
                        </td>
                    </tr>                    
                </table>
            </div>
            <br />
            <div class="clear" />
                <dx:ASPxHiddenField ID="hfSeries" runat="server" ClientInstanceName="hfseries">
                </dx:ASPxHiddenField>
            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnNew" runat="server" Text="New" AutoPostBack="false" 
                            onclick="btnNew_Click"/>                        
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnEdit" ClientInstanceName="btned" runat="server" 
                            Text="Edit" AutoPostBack="false" onclick="btnEdit_Click"/>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDelete" ClientInstanceName="btndel" runat="server" 
                            Text="Delete" AutoPostBack="false" onclick="btnDelete_Click"
                            ClientSideEvents-Click="ConfirmDelete" />
                    </td>
                    <td width="100%" align="right">
                        <table >
                            <tr >                                
                                <td><dx:ASPxButton ID="btnUpload" ClientInstanceName="btnupload" runat="server" 
                            Text="Upload" AutoPostBack="false" onclick="btnUpload_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
            </table>
            <span>
                
                
            </span>
            <br />
            
            <dx:ASPxGridView ID="gvContractList" runat="server" AutoGenerateColumns="False" CssClass="grid"
                ClientInstanceName="grid" ondatabinding="gvContractList_DataBinding" KeyFieldName="seriesCode" >
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Code" VisibleIndex="0" FieldName="seriesCode">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Underlying" VisibleIndex="1" FieldName="underlyingCode">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Type" VisibleIndex="2" FieldName="seriesType">
                    </dx:GridViewDataTextColumn>                    
                    <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="3" FieldName="description">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Start Date" VisibleIndex="4" FieldName="startDate">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn Caption="End Date" VisibleIndex="5" FieldName="endDate">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                    </dx:GridViewDataDateColumn>                    
                    <dx:GridViewDataTextColumn Caption="Contract Period" VisibleIndex="6" FieldName="period">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Strike Price" FieldName="strikePrice" 
                        Visible="False" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Option Type" FieldName="optionType" 
                        Visible="False" VisibleIndex="8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Settlement Method" 
                        FieldName="settlementMethod" Visible="False" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Last Day&lt;br /&gt; Transaction" 
                        FieldName="lastDayTransaction" Visible="False" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Multiplier" FieldName="multiplier" 
                        Visible="False" VisibleIndex="11">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Exercise Time From" FieldName="exerciseTimeFrom" 
                        Visible="False" VisibleIndex="12" />
                    <dx:GridViewDataTextColumn Caption="Exercise Time To" FieldName="exerciseTimeTo" 
                        Visible="False" VisibleIndex="13">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Exercise Style" FieldName="exerciseStyle" 
                        Visible="False" VisibleIndex="14">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Margining Type" FieldName="marginingType" 
                        Visible="False" VisibleIndex="15">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="IDX" FieldName="idx" 
                        Visible="False" VisibleIndex="16">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Status" FieldName="status" 
                        VisibleIndex="17">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Opening Price" FieldName="openinterest" 
                        Visible="false" VisibleIndex="18">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Series Status" FieldName="seriesStatus" 
                        Visible="false" VisibleIndex="19">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Instrument Code" FieldName="instrumentCode" 
                        Visible="false" VisibleIndex="20">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Remark" FieldName="remark" 
                        Visible="false" VisibleIndex="21">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Remark 2" FieldName="remark2" 
                        Visible="false" VisibleIndex="22">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Template Param ID" FieldName="template_param_id" 
                        Visible="false" VisibleIndex="23">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="GF Fee" FieldName="gf_fee" 
                        Visible="false" VisibleIndex="24">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Minimum Fee" FieldName="minimum_fee" 
                        Visible="false" VisibleIndex="25">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Clearing Fee type" FieldName="clearing_fee_type" 
                        Visible="false" VisibleIndex="26">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Clearing Fee Calculation" FieldName="clearing_fee_calculation" 
                        Visible="false" VisibleIndex="27">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Clearing Fee Value" FieldName="clearing_fee_value" 
                        Visible="false" VisibleIndex="28">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Tick Size" FieldName="tick_size" 
                        Visible="false" VisibleIndex="29">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Tick Value" FieldName="tick_value" 
                        Visible="false" VisibleIndex="30">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Margin Type" FieldName="margin_type" 
                        Visible="false" VisibleIndex="31">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Margin Value" FieldName="margin_value" 
                        Visible="false" VisibleIndex="32">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="true" AllowSelectByRowClick="True" />
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <Styles>
                    <AlternatingRow Enabled="True" />
                    <Header CssClass="gridHeader" />
                </Styles>
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Settings ShowVerticalScrollBar="True" />
               
            </dx:ASPxGridView>
         </div>
</asp:Content>