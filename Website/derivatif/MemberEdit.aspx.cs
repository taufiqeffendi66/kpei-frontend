﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.memberInfoWS;
using Common.contractGroupWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class memberedit : System.Web.UI.Page
    {
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        private memberInfoTemp aMemberTemp;
        //private DataRow row;
        private const String module = "Member";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"]));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                //btnSave.Visible = false;

                if (Session["stat"] == null)
                {
                    Response.Redirect("Member.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                fillCombo();

                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = "New " + module; //New Direct
                        btnSave.Visible = true;
                        cmbSpecific.Enabled = false;
                        Permission(true);
                        break;
                    case 1:
                        lblTitle.Text = "Edit " + module; //Edit Direct
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = "New " + module; //New From Temporary
                        Permission(false);

                        GetTemporary();
                        DisabledControl();
                        break;
                    case 3:
                        lblTitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 4:
                        lblTitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }
            }
        }

        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, memberInfoTemp mit)
        {
            string ret = "0";
            const string target = @"../derivatif/Member.aspx";
            if (!memberInfoTemp.ReferenceEquals(aMemberTemp, mit))
                aMemberTemp = mit;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    MemberInfoWSService bws = ImqWS.GetMemberInfoWebService();
                    //memberInfo mi = new memberInfo() { activeDate = mit.activeDate, activeDateSpecified = true, address = mit.address, adminUser = mit.adminUser, clearingMethod = mit.clearingMethod, contactPerson = mit.contactPerson, contractGroup = mit.contractGroup, description = mit.description, email = mit.email, fax = mit.fax, idx = mit.idx, memberId = mit.memberId, memberName = mit.memberName, memberType = mit.memberType, npwp = mit.npwp, phone = mit.phone, sktDate = mit.sktDate, sktDateSpecified = true, sktNo = mit.sktNo, status = mit.status };
                    memberInfo mi = new memberInfo() { activeDate = mit.activeDate, activeDateSpecified = true, address = mit.address, adminUser = mit.adminUser, clearingMethod = mit.clearingMethod, contactPerson = mit.contactPerson, contractGroup = mit.contractGroup, description = mit.description, email = mit.email, fax = mit.fax, memberId = mit.memberId, memberName = mit.memberName, memberType = mit.memberType, npwp = mit.npwp, phone = mit.phone, sktDate = mit.sktDate, sktDateSpecified = true, sktNo = mit.sktNo, status = mit.status };
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = bws.add(mi);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "E":
                            ret = bws.update(hfMember.Get("currentMemberId").ToString(), mi);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Edit " + module);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "R":
                            ret = bws.delete(hfMember.Get("currentMemberId").ToString());
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval Delete " + module);
                                ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }
                    break;
            }
            return ret;
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            string tmp = "";
            tmp = Server.UrlDecode(Request.QueryString["memberId"]);
            hfMember.Set("currentMemberId", tmp);
            string idx = Server.UrlDecode(Request.QueryString["idx"]);
            hfMember.Set("idx", idx);
            txtMemberID.Text = tmp.Trim();
            txtMemberID.Enabled = false;
            txtName.Text = Server.UrlDecode(Request.QueryString["name"]);
            txtNPWP.Text = Server.UrlDecode(Request.QueryString["npwp"]);
            txtAddress.Text = Server.UrlDecode(Request.QueryString["address"]);
            txtPhone.Text = Server.UrlDecode(Request.QueryString["phone"]);
            txtFax.Text = Server.UrlDecode(Request.QueryString["fax"]);
            txtContact.Text = Server.UrlDecode(Request.QueryString["contactPerson"]);
            txtemail.Text = Server.UrlDecode(Request.QueryString["email"]);
            txtAdminUser.Text = Server.UrlDecode(Request.QueryString["adminUser"]);
            txtNoSKT.Text = Server.UrlDecode(Request.QueryString["sktNo"]);
            deSKTDate.Value = DateTime.Parse(Server.UrlDecode(Request.QueryString["sktDate"]));
            deActiveDate.Value = DateTime.Parse(Server.UrlDecode(Request.QueryString["activeDate"]));
            tmp = Server.UrlDecode(Request.QueryString["memberType"]);
            cmbType.SelectedItem = cmbType.Items.FindByValue(tmp);
            tmp = Server.UrlDecode(Request.QueryString["status"]);
            cmbAsStatus.SelectedItem = cmbAsStatus.Items.FindByValue(tmp);
            int selected = int.Parse(Server.UrlDecode(Request.QueryString["clearingMethod"]));
            if (selected > 1)
            {
                cmbSpecific.Enabled = true;
                //cmbSpecific.Text = Server.UrlDecode(Request.QueryString["contractGroup"].ToString());
                int contractGroup = int.Parse(Server.UrlDecode(Request.QueryString["contractGroup"]));
                cmbSpecific.SelectedItem = cmbSpecific.Items.FindByValue(contractGroup);
            }
            else
            {
                cmbSpecific.Enabled = false;
                cmbSpecific.Text = "";
            }
            rbClearing.Value = selected;
        }

        private void DisabledControl()
        {
            txtMemberID.Enabled = false;
            txtName.Enabled = false;
            txtNPWP.Enabled = false;
            txtAddress.Enabled = false;
            txtPhone.Enabled = false;
            txtFax.Enabled = false;
            txtContact.Enabled = false;
            txtemail.Enabled = false;
            txtAdminUser.Enabled = false;
            txtNoSKT.Enabled = false;
            deSKTDate.Enabled = false;
            deActiveDate.Enabled = false;
            cmbType.Enabled = false;
            cmbAsStatus.Enabled = false;
            cmbSpecific.Enabled = false;
            rbClearing.Enabled = false;
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aMemberTemp));
        }

        private static String Log(memberInfoTemp mit)
        {
            string strLog = String.Format("[Member ID : {0}, Name : {1}, Address : {2}, Email : {3}, Phone : {4}, Contact : {5}]", mit.memberId, mit.memberName, mit.address, mit.email, mit.phone, mit.contactPerson);
            return strLog;
        }

        private void GetTemporary()
        {
            MemberInfoWSService mis = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                memberInfoTemp mit = mis.getMemberInfoTemp(int.Parse(Request.QueryString["idxTmp"]));
                txtMemberID.Text = mit.memberId;
                txtName.Text = mit.memberName;
                txtNPWP.Text = mit.npwp;
                txtAddress.Text = mit.address;
                txtPhone.Text = mit.phone;
                txtFax.Text = mit.fax;
                txtContact.Text = mit.contactPerson;
                txtemail.Text = mit.email;
                txtAdminUser.Text = mit.adminUser;
                txtNoSKT.Text = mit.sktNo;
                deSKTDate.Date = mit.sktDate;
                deActiveDate.Date = mit.activeDate;
                cmbAsStatus.SelectedItem = cmbAsStatus.Items.FindByValue(mit.status);
                cmbType.SelectedItem = cmbType.Items.FindByValue(mit.memberType);
                int clearing = mit.clearingMethod;
                rbClearing.SelectedItem = rbClearing.Items.FindByValue(mit.clearingMethod);
                if (clearing > 1)
                {
                    if (cmbSpecific.Items.FindByValue(mit.contractGroup) != null)
                        cmbSpecific.SelectedItem = cmbSpecific.Items.FindByValue(mit.contractGroup);
                    else
                        cmbSpecific.Text = "";
                }
                else
                    cmbSpecific.Text = "";
            }
            catch (Exception e)
            {
                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        private void fillCombo()
        {
            ContractGroupWSService c = null;
            try
            {
                c = ImqWS.GetContractGroupService();
                contractGroup[] contracts = c.get();

                foreach (contractGroup cg in contracts)
                    cmbSpecific.Items.Add(cg.groupName, cg.id);

                //for (int i = 0; i < contracts.Length; i++)
                //{
                //    cmbSpecific.Items.Add(contracts[i].groupName, contracts[i].id);
                //}
            }
            catch (TimeoutException te)
            {
                c.Abort();
                Debug.WriteLine(te.Message);
            }
            catch (Exception ex)
            {
                c.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                c.Dispose();
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            MemberInfoWSService mis = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                mis = ImqWS.GetMemberInfoWebService();

                dtA = aps.Search(Request.QueryString["reffNo"]);
                aMemberTemp = mis.getMemberInfoTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = mis.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = mis.deleteTempById(aMemberTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = mis.updateFromTemp(aMemberTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = mis.deleteTempById(aMemberTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = mis.deleteById(dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = mis.deleteTempById(aMemberTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (mis != null)
                    mis.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (mis != null)
                    mis.Dispose();
            }
        }

        protected void rbClearing_SelectedIndexChanged(object sender, EventArgs e)
        {
            int val = int.Parse(rbClearing.SelectedItem.Value.ToString());
            if (val > 1)
            {
                cmbSpecific.Enabled = true;
            }
            else
            {
                cmbSpecific.Enabled = false;
                cmbSpecific.Text = "";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Session["stat"] != null)
            {
                addedit = int.Parse(Session["stat"].ToString());
                switch (addedit)
                {
                    case 0:
                    case 1:
                    case 4:
                        Response.Redirect("Member.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                        break;
                    case 2:
                    case 3:
                        Response.Redirect("~/administration/approval.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                        break;
                }
            }
            else
            {
                Response.Redirect("Member.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
                ;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            MemberInfoWSService mis = null;
            ApprovalService aps = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                aps = ImqWS.GetApprovalWebService();

                memberInfoTemp data = new memberInfoTemp() { memberId = txtMemberID.Text.Trim(), memberName = txtName.Text.Trim(), npwp = txtNPWP.Text.Trim(), address = txtAddress.Text.Trim(), phone = txtPhone.Text.Trim(), fax = txtFax.Text.Trim(), contactPerson = txtContact.Text.Trim(), email = txtemail.Text.Trim(), adminUser = txtAdminUser.Text.Trim(), sktNo = txtNoSKT.Text.Trim(), sktDate = deSKTDate.Date, sktDateSpecified = true, activeDate = deActiveDate.Date, activeDateSpecified = true, memberType = cmbType.SelectedItem.Value.ToString(), status = cmbAsStatus.SelectedItem.Value.ToString(), description = meDescription.Text, clearingMethod = int.Parse(rbClearing.SelectedItem.Value.ToString()) };
                if (data.clearingMethod > 1)
                {
                    if (cmbSpecific.Items.FindByValue(data.contractGroup) != null)
                        data.contractGroup = int.Parse(cmbSpecific.SelectedItem.Value.ToString().Trim());
                    else
                        data.contractGroup = -1;
                }
                else
                    data.contractGroup = -1;

                addedit = (int)Session["stat"];
                int idRec = mis.addToTemp(data);
                aMemberTemp = data;

                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/MemberEdit.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            aMemberTemp.idx = idRec;
                            break;
                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfMember.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfMember.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }
                    string retVal = "0";
                    retVal = rblApprovalChanged(dtA, aps, aMemberTemp);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/Member.aspx");
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Member", @"../derivatif/Member.aspx");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                if (mis != null)
                    mis.Abort();
            }
            finally
            {
                if (mis != null)
                    mis.Dispose();
            }
        }

        protected void rbClearing_ValueChanged(object sender, EventArgs e)
        {
            int val = int.Parse(rbClearing.SelectedItem.Value.ToString());
            if (val > 1)
                cmbSpecific.Enabled = true;
            else
                cmbSpecific.Enabled = false;
        }
    }
}
