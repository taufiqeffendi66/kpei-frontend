﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class MemberAccountView : System.Web.UI.Page
    {
        private String aMember;
        private String aBank;
        private String aSID;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            if (Request.QueryString["member"] != null)
                aMember = Request.QueryString["member"].Trim();

            if (Request.QueryString["bank"] != null)
                aBank = Request.QueryString["bank"].Trim();

            if (Request.QueryString["sid"] != null)
                aSID = Request.QueryString["sid"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            MemberAccountReport report = new MemberAccountReport() { aMember = aMember, aBank = aBank, aSID = aSID, Name = ImqSession.getFormatFileSave("Member Account") };
            return report;
        }
    }
}
