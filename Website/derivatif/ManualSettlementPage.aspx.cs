﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Web.Configuration;
using Common;
using Common.wsManualSettlement;
using Common.wsUserGroupPermission;
using System.Globalization;

namespace imq.kpei.derivatif
{
    public partial class manualsettlementpage : System.Web.UI.Page
    {
        
        public string stataddedit = "0";
        private manualSettlement[] manualSettlements;
        private String aUserLogin;
        private String aUserMember;
        public String aBankCode;
        public String aBankName;
        private String module = "Manual Settlement";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                Permission();
                gvManualSettlement.DataBind();
                
            }
        }

        private manualSettlement[] getWSManualSettlement()
        {
            ManualSettlementWSService bs = null;
            try
            {
                bs = ImqWS.GetManualSettlementService();
                manualSettlements = bs.get();
                
                if (!IsPostBack)
                {
                    cmbBank.Items.Clear();
                    foreach (manualSettlement b in manualSettlements)
                    {
                        cmbBank.Items.Add(b.bank, b.bank);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                bs.Abort();
            }
            finally
            {
                bs.Dispose();
            }
            return manualSettlements;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string bankCode = cmbBank.Text.Trim();
            string search = String.Empty;

            if (bankCode != String.Empty)
            {               
                bankCode = bankCode.Replace("'", "''");
                bankCode = bankCode.Replace("--", "-");
                aBankCode = bankCode;
                bankCode = "[bank] like '%" + bankCode + "%'";                
            }

            if (bankCode != String.Empty)
            {
                search = bankCode;
            }else{
                search = String.Empty;
            }
            gvManualSettlement.FilterExpression = search;
 
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManualSettlementEdit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void gvManualSettlement_DataBinding(object sender, EventArgs e)
        {
            gvManualSettlement.DataSource = getWSManualSettlement();            
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, module);
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
             }
        }

        private void EnableButton(bool enable)
        {            
            btnNew.Enabled = enable;
        }

        protected void gvManualSettlement_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "value")
            {
                decimal currencyValue;

                if (Decimal.TryParse(e.Value.ToString(), out currencyValue))
                {
                    //CultureInfo elGR = CultureInfo.CreateSpecificCulture("id-IDR");
                    //e.DisplayText = string.Format("{0:0,0}", currencyValue);
                    e.DisplayText = string.Format(CultureInfo.InvariantCulture, "{0:0,0.00}", currencyValue);
                }
            }
        }
    }
}