﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class KpeiAccountView : System.Web.UI.Page
    {
        private String aBank;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.Validate(this);
            if (Request.QueryString["bank"] != null)
                aBank = Request.QueryString["bank"].Trim();
            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            KpeiAccountReport report = new KpeiAccountReport() { aBank = aBank, Name = ImqSession.getFormatFileSave("Kpei Account") };
            return report;
        }
    }
}
