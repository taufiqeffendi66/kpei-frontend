﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class MemberStatusView : System.Web.UI.Page
    {
        private String aMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (Request.QueryString["member"] != null)
                aMember = Request.QueryString["member"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            MemberStatusReport report = new MemberStatusReport() { aMember = aMember, Name = ImqSession.getFormatFileSave("Member Status") };
            return report;
        }
    }
}
