﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.memberAccountWS;
using Common.balanceWithdrawalWS;
using Common.wsApproval;
using System.Diagnostics;
using log4net;
using log4net.Config;

namespace imq.kpei.derivatif
{
    public partial class withdrawalconfirm : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(_Default));
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private balanceWithdrawalTmp aBalanceTemp;
        private const String module = "Balance Withdrawal";
        private dtApproval dtA;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];
            log.Debug("USER : " + aUserLogin);
            log.Debug("MEMBER : " + aUserMember);

            dtA = (dtApproval)Session["bwdtA"];
            aBalanceTemp = (balanceWithdrawalTmp)Session["bwTemp"];

            if (!IsPostBack)
            {
                ParseQueryString();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
//            string mystring = @"<script type='text/javascript'>
//                                alert('" + info + @"');
//                                window.location='" + targetPage + @"'
//                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            log.Info("ParseQueryString");
            string memberId = Server.UrlDecode(Request.QueryString["memberId"]);
            string sid = Server.UrlDecode(Request.QueryString["sid"]);
            string accountDesc = Server.UrlDecode(Request.QueryString["accountDesc"]);
            string accountSource = Server.UrlDecode(Request.QueryString["accountSource"]);
            string accountSourceNo = Server.UrlDecode(Request.QueryString["accountSourceNo"]);
            string role = Server.UrlDecode(Request.QueryString["role"]);
            string value = Server.UrlDecode(Request.QueryString["value"]);
            string approval = Server.UrlDecode(Request.QueryString["approval"]);
            string reffNo = Server.UrlDecode(Request.QueryString["reff"]);
            string reffNoClient = Server.UrlDecode(Request.QueryString["reffClient"]);
            string AccountNo = "";
            log.Debug("memberId : " + memberId);
            log.Debug("sid : " + sid);
            log.Debug("accountDesc : " + accountDesc);
            log.Debug("accountSource : " + accountSource);
            log.Debug("accountSourceNo : " + accountSourceNo);
            log.Debug("role : " + role);
            log.Debug("value : " + value);
            log.Debug("approval : " + approval);
            log.Debug("reffNo : " + reffNo);
            log.Debug("reffNoClient : " + reffNoClient);
            hdWithdraw.Set("memberId", memberId);
            hdWithdraw.Set("sid", sid);
            hdWithdraw.Set("approval", approval);
            hdWithdraw.Set("accountSource", accountSource);
            hdWithdraw.Set("role", role);
            hdWithdraw.Set("reffNo", reffNo);
            hdWithdraw.Set("reffNoClient", reffNoClient);
            if ((memberId.Length > 0) && (sid.Length > 0))
            {
                AccountNo = getAccount(memberId);
                log.Debug("Operational Account : " + AccountNo);

                txtAccNoDb.Text = accountSourceNo;
                txtAccNameDb.Text = accountDesc;
                txtValueDb.Text = value;

                txtAccNameCr.Text = "Operational Account";
                txtAccNoCr.Text = AccountNo;
                txtValueCr.Text = value;
            }
            else
            {
                Response.Redirect("withdrawal.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
            }
            
        }

        private static String getAccount(string memberId)
        {
            log.Info("Get Account");
            string val = "";
            MemberAccountWSService mas = null;
            try
            {
                mas = ImqWS.getMemberAccountService();
                val = mas.getAccountNo(memberId, "fr");
                log.Debug("Account No : " + val);
            }
            catch (Exception ex)
            {
                mas.Abort();
                Debug.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
            finally
            {
                mas.Dispose();
            }
            return val;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            log.Info("Submit Click");
            BalanceWithdrawalWSService bws = null;
            ApprovalService aps = null;
            String ret = "0";
            
            const string target = @"../derivatif/withdrawal.aspx";
            
            try
            {
                bws = ImqWS.GetBalanceWithdrawalService();
                aps = ImqWS.GetApprovalWebService();
                balanceWithdrawal data = new balanceWithdrawal();
                balanceWithdrawalPK id = new balanceWithdrawalPK() { reffNo = hdWithdraw.Get("reffNo").ToString().Trim(), memberId = hdWithdraw.Get("memberId").ToString().Trim() };
                data.id = id;
                data.sid = hdWithdraw.Get("sid").ToString().Trim();
                data.accountSource = hdWithdraw.Get("accountSource").ToString();
                data.role = hdWithdraw.Get("role").ToString();
                data.accountSourceNo = txtAccNoDb.Text.Trim();
                data.accountName = "fr";
                data.accountNo = txtAccNoCr.Text.Trim();
                data.value =  decimal.Parse(txtValueCr.Text.Trim());
                data.valueSpecified = true;           
                data.bwDate = DateTime.Now;
                data.bwDateSpecified = true;
                data.status = "N";
                data.reffNoClient = hdWithdraw.Get("reffNoClient").ToString().Trim();
                string approval = hdWithdraw.Get("approval").ToString().Trim();
                log.Debug(data.ToString());
                switch (approval)
                {
                    case "1":   ret = bws.add(data); //Direct Approval
                                if (ret.Equals("0"))
                                {
                                    dtA.approvelStatus = "Approved";
                                    dtA.memberId = aUserMember;
                                    aps.AddDirectApproval(dtA);
                                    WriteAuditTrail("Direct Approval New " + module);
                                    ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                                }
                                else
                                {
                                    dtA.approvelStatus = "Failed";
                                    aps.AddDirectApproval(dtA);
                                    ShowMessage(ret, target);
                                }
                                break;
                    case "2":   aps.AddDirectChecker(dtA); //Direct Checker
                                WriteAuditTrail("Direct Checker New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                                break;
                    case "3": aps.AddMaker(dtA);  //Maker
                                WriteAuditTrail("New " + module);
                                ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                                break;
                    case "4":    ret = bws.addFromTemp(dtA.idxTmp);
                                if (!ret.Equals("0"))
                                {
                                    dtA.approvelName = aUserLogin;
                                    dtA.approvelStatus = "Failed";
                                    dtA.memberId = aUserMember;
                                    dtA.status = "A";
                                    aps.UpdateByApprovel(dtA);
                                    ShowMessage(ret, target);
                                }
                                else
                                {
                                    dtA.approvelName = aUserLogin;
                                    dtA.approvelStatus = "Approved";
                                    dtA.memberId = aUserMember;
                                    dtA.status = "A";
                                    aps.UpdateByApprovel(dtA);
                                    ret = bws.deleteTempById(aBalanceTemp.bwNo);
                                    if (!ret.Equals("0"))
                                        lblError.Text = ret;
                                    ShowMessage(String.Format("Succes New {0} as Approval", module), @"../administration/approval.aspx");

                                }
                                break;
                    case "5":   dtA.checkerName = aUserLogin;
                                dtA.checkerStatus = "Checked";
                                dtA.approvelStatus = "To Be Approve";
                                dtA.memberId = aUserMember;
                                dtA.status = "C";
                                aps.UpdateByChecker(dtA);
                                ShowMessage(String.Format("Succes New {0} as Checker", module), @"../administration/approval.aspx");
                                break;
                    default: break;
                            
                }
                
            }
            catch (Exception ex)
            {               
                Debug.WriteLine(ex.Message);
                log.Error(ex.Message);
                if(bws != null)
                    bws.Abort();
            }
            finally
            {
                if(bws != null)
                    bws.Dispose();
            }
            
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aBalanceTemp));
        }

        private static String Log(balanceWithdrawalTmp bwt)
        {
            string strLog = String.Format("[Member ID : {0}, SID: {1}, Account Source: {2}, Account Source No: {3}, Operational Account No: {4}, Value : {5}]", bwt.memberId, bwt.sid, bwt.accountSource, bwt.accountSourceNo, bwt.accountNo, String.Format("{0:#,##}", bwt.value));
            return strLog;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session.Remove("bwdtAP");
            Response.Redirect("withdrawal.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}