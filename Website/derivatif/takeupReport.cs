﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Common;
using Common.giveUpWS;
using System.Diagnostics;

namespace imq.kpei.derivatif
{
    public partial class takeupReport : DevExpress.XtraReports.UI.XtraReport
    {
        public String aUserMember;
        public String aMember;

        private giveUp[] listTakeUp;
        public takeupReport()
        {
            InitializeComponent();
        }

        private void GetTakeUp()
        {
            GiveUpWSService gws = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                //string member = aUserMember.ToLower();
                //if (member.Contains("kpei"))
                //    listTakeUp = gws.getByKPEI();
                //else
                //    listTakeUp = gws.Search(aMember);

                listTakeUp = gws.SearchTU(aMember, aUserMember);
                if (listTakeUp != null)
                {
                    DataSource = listTakeUp;

                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "guDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellDate.DataBindings.Add(binding);

                    binding = new XRBinding("Text", DataSource, "guDate");
                    binding.FormatString = "{0:HH:mm}";
                    cellTime.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "guNo");
                    cellNo.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "guMemberId");
                    cellGuMemberID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tuMemberId");
                    cellTuMemberID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tuApprovalDescription");
                    cellTuApproval.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "kpeiApprovalDescription");
                    cellKpeiApproval.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "executionTime");
                    binding.FormatString = "{0:dd/MM/yyyy HH:mm}";
                    cellExecution.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "status");
                    cellStatus.DataBindings.Add(binding);
                }
            }
            catch (Exception ex)
            {
                if (gws != null)
                    gws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                gws.Dispose();
            }
        }

        private void takeupReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetTakeUp();
        }
    }
}
