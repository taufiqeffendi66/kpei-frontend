﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.contractWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;
using System.IO;

namespace imq.kpei.derivatif
{
    public partial class ContractUpload : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private const String module = "Series Contract";
        private ContractWSService cws = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
            {
                Permission(true);
            }
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            //btnChecker.Visible = true;
                            //btnReject.Visible = true;
                            FileUpload1.Visible = true;
                            btnUpload.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                //btnApproval.Visible = true;
                                //btnReject.Visible = true;
                                FileUpload1.Visible = true;
                                btnUpload.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("seriescontract.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (FileUpload1.FileContent == null)
                return;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());

            switch (selected)
            {
                case 0:
                    readFile(FileUpload1.FileContent, 'M');
                    break;
                case 1:
                    readFile(FileUpload1.FileContent, 'C');
                    break;
                case 2:
                    readFile(FileUpload1.FileContent, 'A');
                    break;
            }
        }

        private void readFile(Stream stream, char stat)
        {
            ApprovalService  aps = null;

            dtApproval dtA = null;
            try
            {
                aps = ImqWS.GetApprovalWebService();

                if (cws == null)
                    cws = ImqWS.GetContractService();
                StreamReader sr = new StreamReader(stream);
                string row = "";
                int linenumber = 0;
                while ((row = sr.ReadLine()) != null)
                {
                    linenumber++;
                    if (linenumber > 1)
                    {
                        seriesTmp data = new seriesTmp();
                        string[] fields = row.Split('|');
                        int tokennumber = 0;
                        DateTime endDate = DateTime.Now;
                        string srType = "RO";

                        foreach (string field in fields)
                        {
                            tokennumber++;
                            string value = field.Trim();
                            switch (tokennumber)
                            {
                                case 1:
                                    data.seriesCode = value;
                                    break;
                                case 2:
                                    data.description = value;
                                    break;
                                case 3:
                                    data.seriesStatus = value;
                                    break;
                                case 4:
                                    data.instrumentCode = value;
                                    break;
                                case 5:
                                    DateTime startDate = DateTime.ParseExact(value, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                                    data.startDate = startDate;
                                    data.startDateSpecified = true;
                                    break;
                                case 6:
                                    data.openinterest = float.Parse(value);
                                    break;
                                case 7:
                                    data.multiplier = int.Parse(value);
                                    break;
                                case 8:
                                    data.underlyingCode = value;
                                    break;
                                case 9:
                                    data.strikePrice = float.Parse(value);
                                    break;
                                case 10:
                                    if (value.Equals("P") || value.Equals("p"))
                                        value = "Put";
                                    else
                                    {
                                        if (value.Equals("C") || value.Equals("c"))
                                            value = "Call";
                                        else
                                        {
                                            if (!value.Equals("Put") || !value.Equals("put"))
                                            {
                                                if (!value.Equals("Call") || !value.Equals("call"))
                                                    value = "";
                                                else
                                                    value = "Call";
                                            }
                                            else
                                                value = "Put";
                                        }
                                    }
                                    data.optionType = value;
                                    break;
                                case 11:
                                    data.seriesType = value;
                                    srType = value;
                                    break;
                                case 12:
                                    endDate = DateTime.ParseExact(value, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                                    data.endDate = endDate;
                                    data.endDateSpecified = true;

                                    data.lastDayTransaction = endDate;
                                    data.lastDayTransactionSpecified = true;
                                    break;

                                case 13:
                                    data.remark = value;
                                    break;
                                case 14:
                                    data.remark2 = value;
                                    break;
                            }
                        }

                        if (srType.Equals("RO") || srType.Equals("ro"))
                            data.settlementMethod = "PHYSICAL";
                        else
                            data.settlementMethod = "CASH";

                        data.exerciseStyle = 1;
                        data.marginingType = 20;
                        data.contractPeriod = MonthDiff(endDate, DateTime.Now);
                        //data.idx = cws.getMaxValue();

                        if (stat.Equals('M'))
                        {
                            dtA = makerApproval(data);
                            aps.AddMaker(dtA);
                        }
                        else
                            if (stat.Equals('C'))
                            {
                                dtA = checkerApproval(data);
                                aps.AddDirectChecker(dtA);
                            }
                            else
                            {
                                series s = directApproval(data);
                                cws.add(s);
                            }
                        tokennumber = 0;
                    }
                }
            }
            catch (Exception e)
            {
                if (cws != null)
                    cws.Abort();

                if (aps != null)
                    aps.Abort();
                Debug.WriteLine(e.Message);

                if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                    ClientScript.RegisterStartupScript(this.GetType(), "clientScript", @"<script type='text/javascript'>
                                jAlert('Invalid Data','Information',function()
                               {window.location='ContractUpload.aspx'});
                               </script>");
            }
            finally
            {
                if (cws != null)
                    cws.Dispose();

                if (aps != null)
                    aps.Dispose();
            }
        }

        private static int MonthDiff(DateTime dtTo, DateTime dtFrom)
        {
            int yearFrom = dtFrom.Year;
            int yearTo = dtTo.Year;
            int monthFrom = dtFrom.Month;
            int monthTo = dtTo.Month;
            int retMonths = 0;
            int year = 0;
            if (yearFrom == yearTo)
            {
                if (monthTo == monthFrom)
                    retMonths = 0;
                else
                    retMonths = monthTo - monthFrom;
            }
            else
                if (yearTo > yearFrom)
                {
                    year = yearTo - yearFrom;
                    retMonths = Math.Abs(year * 12);

                    retMonths = Math.Abs(retMonths - (monthTo - monthFrom));
                }
                else
                {
                    retMonths = 0;
                }

            return retMonths + 1;
        }

        private int saveTemp(seriesTmp tmp)
        {
            int idRec = -1;
            try
            {
                if (tmp != null)
                {
                    if (cws == null)
                        cws = new ContractWSService();
                    idRec = cws.addToTemp(tmp);
                }
            }
            catch (Exception e)
            {
                if (cws != null)
                    cws.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                if (cws != null)
                    cws.Dispose();
            }
            return idRec;
        }

        private dtApproval makerApproval(seriesTmp tmp)
        {
            int idRec = -1;
            idRec = saveTemp(tmp);
            dtApproval dtA = null;
            if (idRec > -1)
            {
                dtA = new dtApproval();
                dtA.type = "c";
                dtA.makerDate = DateTime.Now;
                dtA.makerName = aUserLogin;
                dtA.makerStatus = "Maked";
                dtA.checkerStatus = "To Be Check";
                dtA.idxTmp = idRec;
                dtA.form = "~/derivatif/contractinput.aspx";
                dtA.status = "M";
                dtA.memberId = aUserMember;
                dtA.topic = "Add " + module;
                dtA.insertEdit = "I";
            }
            return dtA;
        }

        private dtApproval checkerApproval(seriesTmp tmp)
        {
            int idRec = -1;
            idRec = saveTemp(tmp);
            dtApproval dtA = null;
            if (idRec > -1)
            {
                dtA = new dtApproval();
                dtA.type = "c";
                dtA.makerDate = DateTime.Now;
                dtA.makerName = aUserLogin;
                dtA.makerStatus = "Maked";
                dtA.checkerStatus = "Checked";
                dtA.approvelStatus = "To Be Approved";
                dtA.idxTmp = idRec;
                dtA.form = "~/derivatif/contractinput.aspx";
                dtA.status = "C";
                dtA.memberId = aUserMember;
                dtA.topic = "Add " + module;
                dtA.insertEdit = "I";
            }
            return dtA;
        }

        private series directApproval(seriesTmp tmp)
        {
            series data = new series() { seriesCode = tmp.seriesCode, description = tmp.description, contractPeriod = tmp.contractPeriod, startDate = tmp.startDate, startDateSpecified = true, endDate = tmp.endDate, endDateSpecified = true, exerciseStyle = tmp.exerciseStyle, exerciseTimeFrom = tmp.exerciseTimeFrom, exerciseTimeFromSpecified = true, exerciseTimeTo = tmp.exerciseTimeTo, exerciseTimeToSpecified = true, idx = cws.getMaxValue(), instrumentCode = tmp.instrumentCode, lastDayTransaction = tmp.lastDayTransaction, lastDayTransactionSpecified = true, marginingType = tmp.marginingType, multiplier = tmp.multiplier, openinterest = tmp.openinterest, optionType = tmp.optionType, period = tmp.period, remark = tmp.remark, remark2 = tmp.remark2, seriesStatus = tmp.seriesStatus, seriesType = tmp.seriesType, settlementMethod = tmp.settlementMethod, status = tmp.status, strikePrice = tmp.strikePrice, underlyingCode = tmp.underlyingCode };
            return data;
        }
    }
}
