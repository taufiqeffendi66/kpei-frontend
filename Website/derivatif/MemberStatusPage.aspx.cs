﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using DevExpress.Web.ASPxClasses.Internal;
using Common;
using Common.memberStatusWS;

namespace imq.kpei.derivatif
{
    public partial class MemberStatusPage : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            if (!IsPostBack)
                gvStatus.DataBind();
        }

        //private void Permission()
        //{
        //    dtFormMenuPermission[] dtFMP;

        //    UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
        //    dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Member Status");
        //    for (int n = 0; n < dtFMP.Length; n++)
        //    {
        //        if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
        //            EnableButton(true);
        //        else
        //            EnableButton(false);
        //    }
        //}

        //private void EnableButton(bool enable)
        //{
        //    btnEdit.Enabled = enable;
        //}

        //private void FillMember()
        //{
        //    MemberAccountWSService mas = null;
        //    try
        //    {
        //        mas = ImqWS.getMemberAccountService();
        //        memberAccount[] members = mas.get();
        //        foreach (memberAccount ma in members)
        //        {
        //            cmbMember.Items.Add(ma.memberId.Trim(), ma.memberId.Trim());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mas.Abort();
        //        Debug.WriteLine(ex.Message);
        //    }
        //    finally
        //    {
        //        mas.Dispose();
        //    }
        //}

        private static memberStatus[] getMemberStatus()
        {
            MemberStatusWSService mss = null;
            memberStatus[] list = null;
            try
            {
                //string statusBefore = "";

                mss = ImqWS.GetMemberStatusService();
                list = mss.get();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                mss.Abort();
            }
            finally
            {
                mss.Dispose();
            }

            return list;
        }

        protected void gvStatus_DataBinding(object sender, EventArgs e)
        {
            gvStatus.DataSource = getMemberStatus();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string memberId = cmbMember.Text.Trim();
            memberId = memberId.Replace("'", "''");
            memberId = memberId.Replace("--", "-");
            gvStatus.FilterExpression = String.Format("[memberId] like '%{0}%'", memberId);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string statusNo = CommonUtils.IsNullValue(hfStatus.Get("statusNo")) ? "" : CommonUtils.ValueToString(hfStatus.Get("statusNo"));
            string memberId = CommonUtils.IsNullValue(hfStatus.Get("memberId")) ? "" : CommonUtils.ValueToString(hfStatus.Get("memberId"));
            string statusCurrent = CommonUtils.IsNullValue(hfStatus.Get("statusCurrent")) ? "" : CommonUtils.ValueToString(hfStatus.Get("statusCurrent"));
            string statusCurrentName = CommonUtils.IsNullValue(hfStatus.Get("statusCurrentName")) ? "" : CommonUtils.ValueToString(hfStatus.Get("statusCurrentName"));
            string positionLimit = CommonUtils.IsNullValue(hfStatus.Get("positionLimit")) ? "" : CommonUtils.ValueToString(hfStatus.Get("positionLimit"));
            string strRedirect = String.Format("MemberStatusEdit.aspx?stat=0&statusNo={0}&memberId={1}&statusCurrentName={2}&statusCurrent={3}&positionLimit={4}", Server.UrlEncode(statusNo), Server.UrlEncode(memberId), Server.UrlEncode(statusCurrentName), Server.UrlEncode(statusCurrent), Server.UrlEncode(positionLimit));
            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}
