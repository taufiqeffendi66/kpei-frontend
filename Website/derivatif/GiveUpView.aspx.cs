﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class GiveUpView : System.Web.UI.Page
    {
        private String aMemberId;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.Validate(this);
            if (Request.QueryString["member"] != null)
                aMemberId = Request.QueryString["member"].ToString().Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            GiveUpReport report = new GiveUpReport() { aMember = aMemberId, Name = ImqSession.getFormatFileSave("Give Up") };
            return report;
        }
    }
}
