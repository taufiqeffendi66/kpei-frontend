﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.memberInfoWS;

namespace imq.kpei.derivatif
{
    public partial class giveupform1 : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if (aUserMember.Contains("kpei"))
                {
                    FillMember();
                }
                else
                {
                    cmbMember.Text = aUserMember;
                    cmbMember.DropDownButton.Visible = false;
                    cmbMember.ReadOnly = true;
                }
            }
        }

        private void FillMember()
        {
            MemberInfoWSService mis = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                if (mis != null)
                {
                    memberInfo[] list = mis.get();
                    if (list != null)
                    {
                        foreach (memberInfo mi in list)
                        {
                            if (!mi.memberId.Equals("kpei"))
                                cmbMember.Items.Add(mi.memberId.Trim(), mi.memberId.Trim());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (mis != null)
                    mis.Abort();
                lblError.Text = e.Message;
            }
            finally
            {
                mis.Dispose();
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (cmbMember.Text.Length > 0)
            {
                Response.Redirect("giveupform2.aspx?memberId=" + cmbMember.Text, false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("GiveUpPage.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}
