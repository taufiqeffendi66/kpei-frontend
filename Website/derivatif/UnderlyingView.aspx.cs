﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class UnderlyingView : System.Web.UI.Page
    {
        private String aUnderlying;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (Request.QueryString["search"] != null)
                aUnderlying = Request.QueryString["search"].Trim();
            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            UnderlyingReport report = new UnderlyingReport() { aUnderlying = aUnderlying, Name = ImqSession.getFormatFileSave("Underlying") };
            return report;
        }
    }
}
