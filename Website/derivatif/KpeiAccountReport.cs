﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.kpeiAccountWS;

namespace imq.kpei.derivatif
{
    public partial class KpeiAccountReport : DevExpress.XtraReports.UI.XtraReport
    {
        private kpeiAccount[] listkpei;
        public String aBank;

        public KpeiAccountReport()
        {
            InitializeComponent();
        }

        private void GetKpeiAccount()
        {
            KpeiAccountWSService kas = null;
            try
            {
                kas = ImqWS.GetKpeiAccountService();
                listkpei = kas.Search(aBank);
                if (listkpei != null)
                {
                    DataSource = listkpei;
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "bank.bankName");
                    cellBank.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "settlementAccountNo");
                    cellSettlement.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "penaltyAccountNo");
                    cellPenalty.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "guaranteeFundAccountNo");
                    cellGuarantee.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "feeAccountNo");
                    cellFee.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                if (kas != null)
                    kas.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                kas.Dispose();
            }
        }

        private void KpeiAccountReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetKpeiAccount();
        }
    }
}
