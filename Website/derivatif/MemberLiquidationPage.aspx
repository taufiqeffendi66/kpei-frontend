﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="MemberLiquidationPage.aspx.cs" Inherits="imq.kpei.derivatif.MemberLiquidationPage" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[
           function OnGridFocusedRowChanged() {
               grid.GetRowValues(grid.GetFocusedRowIndex(), 'id.id;date;id.memberId;indicator;liqType;fakeindicator;', OnGetRowValues);
           }

           function OnGetRowValues(values) {
               hfML.Set("id", values[0]);
               hfML.Set("dates", values[1]);
               hfML.Set("memberId", values[2]);
               hfML.Set("indicator", values[3]);
               hfML.Set("liqType", values[4]);
               hfML.Set("fakeindicator", values[5]); 

               if (values[0] !== null || values[0] !== 'undefined') {
                   btnLqd.SetEnabled(true);                 
               }
               else {
                   btnLqd.SetEnabled(false);                   
               }
           }
        //]]>
    </script>  
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lbltitle" runat="server" Text="Member Liquidation" /></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        
        <table>
            <tr>
                <td nowrap="nowrap">Member ID</td>
                <td><dx:ASPxComboBox ID="cmbID" runat="server" IncrementalFilteringMode="Contains"
                        DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbID">
                       <DropDownButton Visible="false" />
                        
                </dx:ASPxComboBox> </td>
                <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" Width="65px" 
                        onclick="btnSearch_Click"></dx:ASPxButton></td>                   
            </tr>
        </table>
                
        <dx:ASPxButton ID="btnLiquidated" runat="server" Text="Liquidated" ClientInstanceName="btnLqd" 
            Enabled="false" onclick="btnLiquidated_Click" EnableCallBacks="true"/>
        
        <br />
        <dx:ASPxGridView ID="gvMemLiq" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" 
           CssClass="grid" KeyFieldName="id.id" EnableCallBacks="true" 
            ondatabinding="gvMemLiq_DataBinding">
           <Columns>
                <dx:GridViewDataTextColumn Caption="ID" VisibleIndex="0" FieldName="id.id" Visible="false"/>
                <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="1" FieldName="date" Visible="false">
                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                </dx:GridViewDataDateColumn>    
                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="2" FieldName="id.memberId"/>
                <dx:GridViewDataTextColumn Caption="liqType" VisibleIndex="3" FieldName="liqType" Visible="false"/>
                <dx:GridViewDataTextColumn Caption="Indicator" VisibleIndex="4" FieldName="indicator" Visible="false"/>
                <dx:GridViewDataTextColumn Caption="Indicator" VisibleIndex="5" FieldName="fakeindicator"/>
            </Columns>
            <Settings ShowVerticalScrollBar="True" />
            
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True"  />
            <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" /> 
            <Styles>
                <AlternatingRow Enabled="True" />
                <Header CssClass="gridHeader" />
            </Styles>
            <SettingsPager PageSize="20"></SettingsPager>
        </dx:ASPxGridView>
        <dx:ASPxHiddenField ID="hfML" runat="server" ClientInstanceName="hfML" />
    </div>
</asp:Content>
