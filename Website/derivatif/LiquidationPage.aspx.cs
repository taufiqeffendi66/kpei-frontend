﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.liquidationWS;

namespace imq.kpei.derivatif
{
    public partial class liquidationPage : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private liquidation[] liquidations;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            hfLiquidation.Set("UserMember", aUserMember);
            if (!IsPostBack)
            {
                String member = aUserMember.ToLower();
                if (!member.Equals("kpei"))
                {
                    cmbMember.Text = aUserMember;
                    cmbMember.Enabled = false;
                    cmbMember.DropDownButton.Visible = false;
                    //gvLiquidation.Columns["memberId"].Visible = true;
                    gvLiquidation.Columns["role"].Visible = true;
                    gvLiquidation.Columns["sid"].Visible = true;
                    gvLiquidation.Columns["colDefault"].Visible = false;
                    gvLiquidation.Columns["colOpposite"].Visible = false;
                    gvLiquidation.Columns["colPos"].Visible = false;
                }
                else
                {
                    cmbMember.Enabled = true;
                    cmbMember.DropDownButton.Visible = true;
                    //gvLiquidation.Columns["memberId"].Visible = false;
                    gvLiquidation.Columns["role"].Visible = false;
                    gvLiquidation.Columns["sid"].Visible = false;
                    gvLiquidation.Columns["colDefault"].Visible = true;
                    gvLiquidation.Columns["colOpposite"].Visible = true;
                    //gvLiquidation.Columns["colPos"].Visible = true;
                }
                //else
                //    FillMember();

                //FillContract();
                //FillSID();

                gvLiquidation.DataBind();
            }
        }

        //protected void FillSID()
        //{
        //    MemberAccountWSService mas = null;
        //    try
        //    {
        //        mas = ImqWS.getMemberAccountService();
        //        memberAccount[] listMA = mas.get();
        //    }
        //    catch (Exception e)
        //    {
        //        if(mas != null)
        //            mas.Abort();
        //        Debug.WriteLine(e.Message);
        //    }
        //    finally
        //    {
        //        mas.Dispose();
        //    }
        //}

        //protected void FillContract()
        //{
        //    ContractWSService c = null;
        //    try
        //    {
        //        c = ImqWS.GetContractService();
        //        series[] contracts = c.get();
        //        foreach (series s in contracts)
        //        {
        //            cmbContract.Items.Add(s.seriesCode, s.seriesCode);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        c.Abort();
        //        Debug.WriteLine(ex.Message);
        //    }
        //    finally
        //    {
        //        c.Dispose();
        //    }
        //}

        //protected void FillMember()
        //{
        //    MemberInfoWSService mis = null;
        //    try
        //    {
        //        mis = ImqWS.GetMemberInfoWebService();      
        //        Common.memberInfoWS.memberInfo[] list = mis.get(); ;
        //        foreach (Common.memberInfoWS.memberInfo m in list)
        //        {
        //            cmbMember.Items.Add(m.memberId, m.memberId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mis.Abort();
        //        Debug.WriteLine(ex.Message);
        //    }
        //    finally
        //    {
        //        mis.Dispose();
        //    }
        //}
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string contract = cmbContract.Text.Trim();
            string member = cmbMember.Text.Trim();
            string sid = cmbSID.Text.Trim();
            string search = String.Empty;

            if (contract != String.Empty)
            {
                contract = contract.Replace("'", "''");
                contract = contract.Replace("--", "-");
                contract = String.Format("[series] like '%{0}%'", contract);
            }

            if (member != String.Empty)
            {
                member = member.Replace("'", "''");
                member = member.Replace("--", "-");
                member = String.Format("([defMemberId] like '%{0}%' or [oppMemberId] like '%{0}%')", member);
            }

            if (sid != String.Empty)
            {
                sid = sid.Replace("'", "''");
                sid = sid.Replace("--", "-");
                sid = String.Format("([defSID] like '%{0}%' or [oppSID] like '%{0}%')", sid);
            }

            if (contract != String.Empty)
            {
                if (member != String.Empty)
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1} and {2}", contract, member, sid);
                    else
                        search = String.Format("{0} and {1}", contract, member);
                }
                else
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1}", contract, sid);
                    else
                        search = contract;
                }
            }
            else
            {
                if (member != String.Empty)
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1}", member, sid);
                    else
                        search = member;
                }
                else
                {
                    if (sid != String.Empty)
                        search = sid;
                    else
                        search = String.Empty;
                }
            }
            gvLiquidation.FilterExpression = search;

            hfLiquidation.Set("UserMember", cmbMember.Text);
        }

        private liquidation[] getLiquidation()
        {
            LiquidationWSService lws = null;
            try
            {
                lws = ImqWS.GetLiquidationService();
                string aMember = aUserMember.ToLower();
                if (aMember.Contains("kpei"))
                    liquidations = lws.get();
                else
                    liquidations = lws.getByMemberID(aUserMember);

                if (liquidations != null)
                {
                    liquidation[] listTemp = (liquidation[])liquidations.Clone();
                    cmbContract.DataSource = listTemp.GroupBy(p => p.series)
                    .Select(g => g.First()).ToArray();
                    cmbContract.DataBind();

                    foreach (liquidation li in listTemp)
                    {
                        if (cmbMember.Items.FindByValue(li.defMemberId.Trim()) == null)
                        {
                            cmbMember.Items.Add(li.defMemberId.Trim(), li.defMemberId.Trim());
                        }
                        if (cmbMember.Items.FindByValue(li.oppMemberId.Trim()) == null)
                        {
                            cmbMember.Items.Add(li.oppMemberId.Trim(), li.oppMemberId.Trim());
                        }

                        if (cmbSID.Items.FindByValue(li.defSID.Trim()) == null)
                        {
                            cmbSID.Items.Add(li.defSID.Trim(), li.defSID.Trim());
                        }
                        if (cmbSID.Items.FindByValue(li.oppSID.Trim()) == null)
                        {
                            cmbSID.Items.Add(li.oppSID.Trim(), li.oppSID.Trim());
                        }
                    }
                    /*cmbMember.DataSource = listTemp.GroupBy(p => p.id.memberId)
                                           .Select(g => g.First()).ToArray();
                    cmbMember.DataBind();
                     

                    cmbSID.DataSource = listTemp.GroupBy(p => p.sid)
                                        .Select(g => g.First()).ToArray();
                    cmbSID.DataBind();
                     */
                }
            }
            catch (Exception ex)
            {
                lws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                lws.Dispose();
            }

            return liquidations;
        }

        protected void gvLiquidation_DataBinding(object sender, EventArgs e)
        {
            gvLiquidation.DataSource = getLiquidation();
        }
    }
}
