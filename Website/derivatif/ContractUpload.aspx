﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="ContractUpload.aspx.cs" Inherits="imq.kpei.derivatif.ContractUpload" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title"><dx:ASPxLabel ID="lblTitle" runat="server" Text="Upload Series Contract" /> </div>
        <div id="plApproval">
            <table>
                <tr>
                    <td><dx:ASPxRadioButtonList ID="rblApproval" runat="server" AutoPostBack="False" 
                            ClientIDMode="AutoID"></dx:ASPxRadioButtonList></td>
                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td>Browse File : </td>
                <td><asp:FileUpload ID="FileUpload1" runat="server" class="form-txt"  />
                    
                    <asp:RegularExpressionValidator ID="uploadValidator" runat="server" ErrorMessage="CSV File Only"
                        ValidationExpression="([a-zA-Z]:|)(\\{2}|\\).*\.csv$" ControlToValidate="FileUpload1" ValidationGroup="up">
                    </asp:RegularExpressionValidator>
                
                </td>
                
            </tr>
            <tr><td><dx:ASPxButton ID="btnUpload" ClientInstanceName="btnupload" runat="server" 
                    Text="Upload" AutoPostBack="false"  Width="100px" 
                    onclick="btnUpload_Click"></dx:ASPxButton></td>
                <td><dx:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="false" 
                        onclick="btnBack_Click"></dx:ASPxButton></td>
            </tr>            
        </table>
    </div>
</asp:Content>
