﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.giveUpWS;
using Common.wsApproval;
using DevExpress.Web.ASPxGridView;
using Common.wsTransaction;

namespace imq.kpei.derivatif
{
    public partial class giveupform4 : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private dtApproval dtA;
        private const String module = "Give Up";
        private giveUp aGiveUp;
        ASPxGridView detailView;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            dtA = (dtApproval)Session["dtA"];
            aGiveUp = (giveUp)Session["GIVE_UP"];
            btnCancel.Attributes.Add("onClick", "javascript:history.back(); return false;");

            if (!IsPostBack)
            {
                ParseQueryString();
                gvGiveUp.DataBind();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            string approval = Server.UrlDecode(Request.QueryString["approval"].ToString());

            hdGiveUp.Set("approval", approval);
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aGiveUp));
        }

        private static String Log(giveUp gut)
        {
            string strLog = String.Format("[Give Up Np : {0}, Give Up Member ID : {1}, Take Up Member ID : {2}]", gut.guNo, gut.guMemberId, gut.tuMemberId);
            return strLog;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session.Remove("dtA");
            Session.Remove("GIVE_UP");
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            GiveUpWSService gws = null;
            ApprovalService aps = null;
            string ret = String.Empty;
            string target = String.Empty;
            try
            {
                gws = ImqWS.GetGiveUpService();
                aps = ImqWS.GetApprovalWebService();

                string approval = hdGiveUp.Get("approval").ToString();

                if (approval.Equals("0"))
                {
                    ret = gws.add(aGiveUp);
                    if (ret.Equals("0"))
                    {
                        dtA.approvelStatus = "Approved";
                        aps.AddDirectApproval(dtA);
                        WriteAuditTrail("Direct Approval New " + module);
                        target = @"../derivatif/GiveUpPage.aspx";
                        ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                    }
                    else
                    {
                        dtA.approvelStatus = "Failed";
                        aps.AddDirectApproval(dtA);
                        target = @"../derivatif/GiveUpPage.aspx";
                        ShowMessage(ret, target);
                    }
                }
                else
                {
                    ret = gws.add(aGiveUp);
                    if (ret.Equals("0"))
                    {
                        dtA.approvelStatus = "Approved";
                        aps.AddDirectApproval(dtA);
                        WriteAuditTrail("Approval New " + module);
                        target = @"../administration/approval.aspx";
                        ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                    }
                    else
                    {
                        dtA.approvelStatus = "Failed";
                        aps.AddDirectApproval(dtA);
                        target = @"../administration/approval.aspx";
                        ShowMessage(ret, target);
                    }
                }
            }
            catch (Exception ex)
            {
                if (gws != null)
                    gws.Abort();
                if (aps != null)
                    aps.Abort();
                lblError.Text = ex.Message;
            }
            finally
            {
                gws.Dispose();
                aps.Dispose();
            }
        }

        protected void detailGrid_OnLoad(object sender, EventArgs e)
        {
            detailView = sender as ASPxGridView;
        }
        protected void detailGrid_DataBinding(object sender, EventArgs e)
        {
            try
            {
                detailView = sender as ASPxGridView;
                if (hdGiveUp.Get("sid") != null)
                    detailView.DataSource = getNetposition(hdGiveUp.Get("sid").ToString(), aGiveUp.guMemberId.Trim());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        protected void detailGrid_DataSelect(object sender, EventArgs e)
        {
            hdGiveUp.Set("sid", (sender as ASPxGridView).GetMasterRowKeyValue());
        }
        private clearingDto[] getNetposition(String sid, String memberId)
        {
            clearingDto[] list = null;
            UiDataService ws = null;
            try
            {
                ws = ImqWS.GetTransactionService();
                list = ws.getClearing(aUserMember.Trim(), memberId.Trim(), "", sid, "", "", "", "last");
            }
            catch (Exception ex)
            {
                if (ws != null)
                    ws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (ws != null)
                    ws.Dispose();
            }

            return list;
        }

        protected void gvGiveUp_DataBinding(object sender, EventArgs e)
        {
            gvGiveUp.DataSource = aGiveUp.giveUpChilds;
        }
    }
}
