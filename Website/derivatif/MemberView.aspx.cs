﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.UI;
using Common;

namespace imq.kpei.derivatif
{
    public partial class MemberView : System.Web.UI.Page
    {
        private String aMember;
        private String aName;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (Request.QueryString["member"] != null)
                aMember = Request.QueryString["member"].Trim();

            if (Request.QueryString["name"] != null)
                aName = Request.QueryString["name"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            MemberReport report = new MemberReport() { aMember = aMember, aName = aName, Name = ImqSession.getFormatFileSave("Member") };
            return report;
        }
    }
}
