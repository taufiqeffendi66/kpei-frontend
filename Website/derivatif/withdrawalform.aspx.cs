﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.balanceWithdrawalWS;
using Common.clientAccountWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;
using log4net;
using log4net.Config;

namespace imq.kpei.derivatif
{
    public partial class withdrawalform : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(_Default));
        private int addedit;
        private String aUserLogin;
        private String aUserMember;
        private string reffNo;
        //private DataRow row;
        private balanceWithdrawalTmp aBalanceTemp;
        private const String module = "Balance Withdrawal";
        //private String role;
        private clientAccount ClientAccount;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            log.Debug("USER : " + aUserLogin);
            log.Debug("MEMBER : " + aUserMember);
            //row = (DataRow)Session["row"];
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"]));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                log.Debug("addedit / stat : " + addedit);
                
                btnContinue.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0: lblTitle.Text = "New " + module; //New Direct
                        
                        btnContinue.Visible = true;
                        Permission(true);
                        string memberId;
                        if (Session["SESSION_USERMEMBER"] != null)
                        {
                            memberId = aUserMember;
                            txtMember.Text = memberId;
                            FillSID();
                            int rowCount = int.Parse(Server.UrlDecode(Request.QueryString["rowcount"])) + 1;
                            reffNo = string.Format("{0}/{1}/{2}",rowCount,memberId,DateTime.Now.ToString("dd/MM/yyyy"));
                            txtReff.Text = reffNo;
                            hfWithdrawal.Add("reffNo", reffNo);

                        }
                        break;
                    case 1: lblTitle.Text = "Edit " + module; //Edit Direct
                        btnContinue.Visible = true;
                        Permission(true);                        
                        
                        break;
                    case 2: lblTitle.Text = "New " + module; //New From Temporary
                        
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3: lblTitle.Text = module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 4: lblTitle.Text = "Cancel " + module;
                        btnContinue.Visible = true;
                        Permission(true);                        
                        DisabledControl();
                        btnContinue.Text = "OK";
                        ParseQueryString();
                        break;

                }
                
            }

        }

        private void ShowMessage(string info, string targetPage)
        {
//            string mystring = @"<script type='text/javascript'>
//                                alert('" + info + @"');
//                                window.location='" + targetPage + @"'
//                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            if (Request.QueryString["bwNo"] != null)
            {
                int bwNo = int.Parse(Request.QueryString["bwNo"].Trim());
                hfWithdrawal.Set("bwNo", bwNo);
                hfWithdrawal.Set("role", " ");
                BalanceWithdrawalWSService bws = ImqWS.GetBalanceWithdrawalService();
                balanceWithdrawal bw = bws.getBalanceWithdrawal(bwNo);

                txtReff.Text = bw.reffNoClient;
                hfWithdrawal.Set("reffNo", bw.id.reffNo);

                txtMember.Text = bw.id.memberId;
                txtAccNo.Text = bw.accountSourceNo;
                txtValue.Text = String.Format("{0:#,##}", bw.value);
                cmbAccountName.SelectedItem = cmbAccountName.Items.FindByValue(bw.accountSource);
                cmbSID.Text = bw.sid;

                txtReff.Enabled = false;
                txtMember.Enabled = false;
                txtAccNo.Enabled = false;
                txtValue.Enabled = false;
                cmbSID.Enabled = false;
                cmbAccountName.Enabled = false;
            }
        }

        private void GetTemporary()
        {
            BalanceWithdrawalWSService bws = null;
            try
            {
                bws = ImqWS.GetBalanceWithdrawalService();
                balanceWithdrawalTmp bwt = bws.getBalanceWithdrawalTemp(int.Parse(Request.QueryString["idxTmp"]));
                cmbAccountName.SelectedItem = cmbAccountName.Items.FindByValue(bwt.accountSource);
                cmbSID.Text = bwt.sid.Trim();
                txtAccNo.Text = bwt.accountSourceNo;
                txtMember.Text = bwt.memberId;
                txtValue.Text = String.Format("{0:#,##}",bwt.value);
                txtReff.Text = bwt.reffNoClient;
                hfWithdrawal.Set("reffNo", bwt.reffNo);
            }
            catch (Exception e)
            {
                if (bws != null)
                    bws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                bws.Dispose();
            }
        }

        protected void DisabledControl()
        {
            cmbAccountName.Enabled = false;
            cmbSID.Enabled = false;
            txtAccNo.Enabled = false;
            txtMember.Enabled = false;
            txtValue.Enabled = false;
            txtReff.Enabled = false;
        }

        protected void cmbAccountName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["client"] != null)
            {
                ClientAccount = (clientAccount)Session["client"];
                string val = cmbAccountName.SelectedItem.Value.ToString();
                //getAccountNo(val);
                if (val.Equals("sd"))
                    txtAccNo.Text = ClientAccount.securityDepositAccountNo;
                else
                    txtAccNo.Text = ClientAccount.collateralAccountNo;
            }
        }

        //private String getAccountNo(string val)
        //{
        //    string ret = String.Empty;
        //    ClientAccountWSService cas = null;
        //    clientAccount ca = null;
        //    try
        //    {
        //        cas = ImqWS.GetClientAccountService();
        //        ca = cas.getClient(txtMember.Text.Trim(), cmbSID.SelectedItem.Value.ToString());
        //        if (ca != null)
        //        {
        //            if (ca.role.Equals("C"))
        //            {
                        
        //            }
        //        }
        //        //txtAccNo.Text = cas.getAccountNo(txtMember.Text.Trim(), cmbSID.SelectedItem.Value.ToString(), val);
        //    }
        //    catch (Exception ex)
        //    {
        //        if(cas != null)
        //            cas.Abort();
        //        Debug.WriteLine(ex.Message);
        //    }
        //    finally
        //    {
        //        if(cas != null)
        //            cas.Dispose();
        //    }
        //    return ret;
        //}

        private void Permission(bool isEditor)
        {
            log.Info("Permission");
            dtFormMenuPermission[] listdtFMP = null;

            try
            {

                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    log.Debug("Edit Maker : " + dtFMP.editMaker);
                    log.Debug("Edit Checker : " + dtFMP.editChecker);
                    log.Debug("Edit Approval : " + dtFMP.editApproval);
                    log.Debug("Edit Direct Checker : " + dtFMP.editDirectChecker);
                    log.Debug("Edit Direct Approval : " + dtFMP.editDirectApproval);
                    log.Debug("Removing : " + dtFMP.removeing);

                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker) rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker) rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval) rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                        {
                            btnApproval.Visible = true;
                            btnReject.Visible = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {

                Debug.WriteLine(e.Message);
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4: Response.Redirect("withdrawal.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                        break;
                case 2:
                case 3: Response.Redirect("~/administration/approval.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                        break;
            }
           
        }
        
        private void FillSID()
        {
                ClientAccountWSService cas = null;
                clientAccount[] list = null;
                try
                {
                    cas = ImqWS.GetClientAccountService();
                    list = cas.getByMemberId(txtMember.Text.Trim());
                    if (list != null)
                    {
                        foreach (clientAccount ca in list)
                            cmbSID.Items.Add(ca.sid, ca.sid);
                    }
                }
                catch (Exception e)
                {
                    if (cas != null)
                        cas.Abort();
                    Debug.WriteLine(e.Message);
                }
                finally
                {
                    cas.Dispose();
                }            
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, balanceWithdrawalTmp bwt)
        {
            string ret = "0";            
            const string target = @"../derivatif/withdrawal.aspx";
            
            if (!balanceWithdrawalTmp.ReferenceEquals(aBalanceTemp, bwt))
                aBalanceTemp = bwt;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0: //wsA.AddMaker(dtA);
                        switch (dtA.insertEdit)
                        {
                            case "I":   //WriteAuditTrail("New " + module);
                                        //ShowMessage("Succes New " + module + " as Maker", target);
                                        ret = "3";
                                        break;

                            case "R":   wsA.AddMaker(dtA);
                                        WriteAuditTrail("Cancel " + module);
                                        ShowMessage(String.Format("Succes Cancel {0} as Maker", module), target);
                                        break;
                        }
                        break;
                case 1: //wsA.AddDirectChecker(dtA);
                        switch (dtA.insertEdit)
                        {
                            case "I":   //WriteAuditTrail("Direct Checker New " + module);
                                        //ShowMessage("Succes New " + module + " as Direct Checker", target);
                                        ret = "2";
                                        break;

                            case "R":   wsA.AddDirectChecker(dtA);
                                        WriteAuditTrail("Direct Checker Cancel " + module);
                                        ShowMessage(String.Format("Succes Cancel {0} as Direct Checker", module), target);
                                        break;
                        }
                        break;
                case 2: //wsA.AddDirectApproval(dtA);
                        switch (dtA.insertEdit)
                        {
                            case "I":  ret = "1";
                                        break;                      

                            case "R":
                                        BalanceWithdrawalWSService bws = ImqWS.GetBalanceWithdrawalService();
                                        ret = bws.deleteById(int.Parse(hfWithdrawal.Get("bwNo").ToString()));
                                        if (ret.Equals("0"))
                                        {
                                            WriteAuditTrail("Direct Approval Cancel " + module);
                                            ShowMessage(String.Format("Succes Cancel {0} as Direct Approval", module), target);
                                        }
                                        break;
                        }
                        break;
            }

            return ret;
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            BalanceWithdrawalWSService bws = null;
            ApprovalService aps = null;

            //string ret = String.Empty;
            try
            {
                bws = ImqWS.GetBalanceWithdrawalService();
                aps = ImqWS.GetApprovalWebService();

                addedit = (int)Session["stat"];
                int idRec;

                balanceWithdrawalTmp bwt = new balanceWithdrawalTmp() { memberId = txtMember.Text.Trim(), sid = cmbSID.Text.Trim(), accountSource = cmbAccountName.SelectedItem.Value.ToString(), accountSourceNo = txtAccNo.Text.Trim(), value = decimal.Parse(txtValue.Text),valueSpecified=true,  role = hfWithdrawal.Get("role").ToString(), reffNo = hfWithdrawal.Get("reffNo").ToString() };

                bwt.reffNoClient = txtReff.Text.Trim();
                idRec = bws.addToTemp(bwt);

                if (bwt.accountSource.Equals("sd"))
                    bwt.accountDescription = "Security Deposit Account";
                else
                    bwt.accountDescription = "Collateral Account";

                aBalanceTemp = bwt;
                
                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/withdrawalform.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            aBalanceTemp.bwNo = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfWithdrawal.Get("bwNo").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }

                    string retVal = "0";
                    
                    retVal = rblApprovalChanged(dtA, aps, aBalanceTemp);
                    if (retVal.Equals("1") || retVal.Equals("2") || retVal.Equals("3"))
                    {
                        Session["bwdtA"] = dtA;
                        Session["bwTemp"] = aBalanceTemp;
                        string strRedirect = String.Format("withdrawalconfirm.aspx?stat=0&memberId={0}&sid={1}&accountDesc={2}&accountSource={3}&accountSourceNo={4}&role={5}&bwNo={6}&approval={7}&value={8}&reff={9}&reffClient={10}", Server.UrlEncode(aBalanceTemp.memberId.Trim()), Server.UrlEncode(aBalanceTemp.sid.Trim()), Server.UrlEncode(aBalanceTemp.accountDescription.Trim()), Server.UrlEncode(aBalanceTemp.accountSource.Trim()), Server.UrlEncode(aBalanceTemp.accountSourceNo.Trim()), Server.UrlEncode(aBalanceTemp.role.Trim()), Server.UrlEncode(aBalanceTemp.bwNo.ToString()), Server.UrlEncode(retVal), Server.UrlEncode(String.Format("{0:n0}", aBalanceTemp.value)), Server.UrlEncode(aBalanceTemp.reffNo.Trim()), Server.UrlEncode(aBalanceTemp.reffNoClient.Trim()));


                        Response.Redirect(strRedirect, false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        ShowMessage(retVal, @"../derivatif/withdrawal.aspx");                         
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Balance Withdrawal", @"../derivatif/withdrawal.aspx");                    
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                if (bws != null)
                    bws.Abort();
                if (aps != null)
                    aps.Abort();

                lblError.Text = ex.Message;
            }
            finally
            {
                if(bws != null)
                    bws.Dispose();

                if(aps != null)
                    aps.Dispose();
            }
            

        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {


            ApprovalService aps = null;
            BalanceWithdrawalWSService bws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            string approvalStat = "4";
            //string messages = String.Empty;
            const string target = @"../administration/approval.aspx";
           // string resp = String.Empty;

            try
            {

                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                bws = ImqWS.GetBalanceWithdrawalService();

                dtA = aps.Search(Request.QueryString["reffNo"]);


                aBalanceTemp = bws.getBalanceWithdrawalTemp(dtA[0].idxTmp);

                string activity;
                
                if (cp == 0)
                {
                    activity = "Checker";
                    //dtA[0].checkerName = aUserLogin;
                    //dtA[0].checkerStatus = "Checked";
                    //dtA[0].approvelStatus = "To Be Approve";
                    //dtA[0].memberId = aUserMember;
                    //dtA[0].status = "C";
                    //aps.UpdateByChecker(dtA[0]);

                    
                }
                else if (cp == 1)
                {
                    activity = "Approval";

                    
                }
                else
                {
                    if (btnChecker.Visible == true)
                    {
                        activity = "Reject Checker";
                        dtA[0].checkerName = aUserLogin;
                        dtA[0].checkerStatus = "Rejected";
                        dtA[0].approvelStatus = "";
                        dtA[0].memberId = aUserMember;
                        dtA[0].status = "RC";
                        aps.UpdateByChecker(dtA[0]);
                    }
                    else
                    {
                        activity = "Reject Approval";
                        dtA[0].approvelName = aUserLogin;
                        dtA[0].approvelStatus = "Rejected";
                        dtA[0].memberId = aUserMember;
                        dtA[0].status = "RA";
                        aps.UpdateByApprovel(dtA[0]);
                    }

                }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I": //WriteAuditTrail(activity + " New " + module);
                            if (cp == 1)
                            {
                                
                                //ret = bws.addFromTemp(dtA[0].idxTmp);
                                //if (!ret.Equals("0"))
                                //{
                                //    dtA[0].approvelName = aUserLogin;
                                //    dtA[0].approvelStatus = "Failed";
                                //    dtA[0].memberId = aUserMember;
                                //    dtA[0].status = "A";
                                //    aps.UpdateByApprovel(dtA[0]);
                                //    ShowMessage(ret, target);
                                //}
                                //else
                                //{
                                //    dtA[0].approvelName = aUserLogin;
                                //    dtA[0].approvelStatus = "Approved";
                                //    dtA[0].memberId = aUserMember;
                                //    dtA[0].status = "A";
                                //    aps.UpdateByApprovel(dtA[0]);
                                //    ret = bws.deleteTempById(aBalanceTemp.bwNo);
                                //    if (!ret.Equals("0"))
                                //        lblError.Text = ret;
                                //    ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                               
                                //}
                                approvalStat = "4";
                                Session["bwdtA"] = dtA[0];
                                Session["bwTemp"] = aBalanceTemp;
                                string strRedirect = String.Format("withdrawalconfirm.aspx?stat=0&memberId={0}&sid={1}&accountDesc={2}&accountSource={3}&accountSourceNo={4}&role={5}&bwNo={6}&approval={7}&value={8}&reff={9}&reffClient={10}", Server.UrlEncode(aBalanceTemp.memberId.Trim()), Server.UrlEncode(aBalanceTemp.sid.Trim()), Server.UrlEncode(aBalanceTemp.accountDescription.Trim()), Server.UrlEncode(aBalanceTemp.accountSource.Trim()), Server.UrlEncode(aBalanceTemp.accountSourceNo.Trim()), Server.UrlEncode(aBalanceTemp.role.Trim()), Server.UrlEncode(aBalanceTemp.bwNo.ToString()), Server.UrlEncode(approvalStat), Server.UrlEncode(String.Format("{0:n0}", aBalanceTemp.value)), Server.UrlEncode(aBalanceTemp.reffNo.Trim()), Server.UrlEncode(aBalanceTemp.reffNoClient.Trim()));


                                Response.Redirect(strRedirect, false);
                                Context.ApplicationInstance.CompleteRequest();
                            }
                            else if (cp == 0)
                            {
                                //ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                                approvalStat = "5";
                                Session["bwdtA"] = dtA[0];
                                Session["bwTemp"] = aBalanceTemp;
                                string strRedirect = String.Format("withdrawalconfirm.aspx?stat=0&memberId={0}&sid={1}&accountDesc={2}&accountSource={3}&accountSourceNo={4}&role={5}&bwNo={6}&approval={7}&value={8}&reff={9}&reffClient={10}", Server.UrlEncode(aBalanceTemp.memberId.Trim()), Server.UrlEncode(aBalanceTemp.sid.Trim()), Server.UrlEncode(aBalanceTemp.accountDescription.Trim()), Server.UrlEncode(aBalanceTemp.accountSource.Trim()), Server.UrlEncode(aBalanceTemp.accountSourceNo.Trim()), Server.UrlEncode(aBalanceTemp.role.Trim()), Server.UrlEncode(aBalanceTemp.bwNo.ToString()), Server.UrlEncode(approvalStat), Server.UrlEncode(String.Format("{0:n0}", aBalanceTemp.value)), Server.UrlEncode(aBalanceTemp.reffNo.Trim()), Server.UrlEncode(aBalanceTemp.reffNoClient.Trim()));


                                Response.Redirect(strRedirect, false);
                                Context.ApplicationInstance.CompleteRequest();
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                            break;
                    case "R": WriteAuditTrail(String.Format("{0} Cancel {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = bws.deleteById(dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);

                                ret = bws.deleteTempById(aBalanceTemp.bwNo);
                                //if (!ret.Equals("0"))
                                //    lblError.Text = ret;
                                ShowMessage(String.Format("Succes Cancel {0} as Approval", module), target);
                            }
                        }
                        else if (cp == 0)
                        {
                            activity = "Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Checked";
                            dtA[0].approvelStatus = "To Be Approved";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "C";
                            aps.UpdateByChecker(dtA[0]);
                            ShowMessage(String.Format("Succes Cancel {0} as Checker", module), target);

                        }
                        else
                        {
                            if (btnChecker.Visible == true)
                            {
                                activity = "Reject Checker";
                                dtA[0].checkerName = aUserLogin;
                                dtA[0].checkerStatus = "Rejected";
                                dtA[0].approvelStatus = "";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "RC";
                                aps.UpdateByChecker(dtA[0]);
                                ShowMessage(String.Format("Rejected Cancel {0} as Checker", module), target);
                            }
                            else
                            {
                                activity = "Reject Approval";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Rejected";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "RA";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(String.Format("Rejected Cancel {0} as Approval", module), target);
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                if(aps != null)
                    aps.Abort();

                if(bws != null)

                bws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if(aps != null)
                    aps.Dispose();

                if(bws != null)
                    bws.Dispose();

                
            }

           
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aBalanceTemp));
        }

        private static String Log(balanceWithdrawalTmp bwt)
        {
            string strLog = String.Format("[Member ID : {0}, SID: {1}, Account Source: {2}, Account Source No: {3}, Settlement Account No: {4}, Value : {5}]", bwt.memberId, bwt.sid, bwt.accountSource, bwt.accountSourceNo, bwt.accountNo, bwt.value);
            return strLog;
        }

        protected void cmbSID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getAccountNo(cmbAccountName.SelectedItem.Value.ToString());
            ClientAccountWSService cas = null;
            try
            {
                cas = ImqWS.GetClientAccountService();
                ClientAccount = cas.getClient(txtMember.Text.Trim(), cmbSID.SelectedItem.Value.ToString());

                if (ClientAccount != null)
                {
                    Session["client"] = ClientAccount;
                    hfWithdrawal.Set("role", ClientAccount.role.Trim());
                    //role = ClientAccount.role.Trim();
                    if (ClientAccount.role.Equals("C"))
                    {
                        cmbAccountName.DropDownButton.Visible = false;
                        cmbAccountName.SelectedIndex = 0;
                        cmbAccountName.ReadOnly = true;
                        txtAccNo.Text = ClientAccount.collateralAccountNo;
                        cmbAccountName.Enabled = false;
                    }
                    else
                    {
                        cmbAccountName.Enabled = true;
                        cmbAccountName.DropDownButton.Visible = true;
                        cmbAccountName.SelectedIndex = 0;
                        cmbAccountName.ReadOnly = false;
                        txtAccNo.Text = ClientAccount.collateralAccountNo;

                    }
                }
            }
            catch (Exception ex)
            {
                if (cas != null)
                {
                    cas.Abort();
                    Debug.WriteLine(ex.Message);
                }
            }
            finally
            {
                if (cas != null)
                {
                    cas.Dispose();
                }
            }
        }

    }
}