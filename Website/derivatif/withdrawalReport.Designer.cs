﻿namespace imq.kpei.derivatif
{
    partial class withdrawalReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellReff = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellMember = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellSID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAccountDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAccountNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellExecDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellExecTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.header1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xtraReport1 = new DevExpress.XtraReports.UI.XtraReport();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableDetail});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableDetail
            // 
            this.tableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableDetail.Font = new System.Drawing.Font("Tahoma", 8F);
            this.tableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableDetail.Name = "tableDetail";
            this.tableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tableDetail.SizeF = new System.Drawing.SizeF(809F, 31.25F);
            this.tableDetail.StylePriority.UseBorders = false;
            this.tableDetail.StylePriority.UseFont = false;
            this.tableDetail.StylePriority.UseTextAlignment = false;
            this.tableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellNo,
            this.cellReff,
            this.cellDate,
            this.cellTime,
            this.cellMember,
            this.cellSID,
            this.cellAccountDesc,
            this.cellAccountNo,
            this.cellValue,
            this.cellExecDate,
            this.cellExecTime,
            this.cellStatus});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellNo
            // 
            this.cellNo.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellNo.Name = "cellNo";
            this.cellNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellNo.StylePriority.UseFont = false;
            this.cellNo.StylePriority.UsePadding = false;
            this.cellNo.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.cellNo.Summary = xrSummary1;
            this.cellNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellNo.Weight = 0.16395255750199164D;
            // 
            // cellReff
            // 
            this.cellReff.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellReff.Name = "cellReff";
            this.cellReff.StylePriority.UseFont = false;
            this.cellReff.StylePriority.UseTextAlignment = false;
            this.cellReff.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellReff.Weight = 0.43182940421903415D;
            // 
            // cellDate
            // 
            this.cellDate.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellDate.Name = "cellDate";
            this.cellDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellDate.StylePriority.UseFont = false;
            this.cellDate.StylePriority.UsePadding = false;
            this.cellDate.Weight = 0.19082815150979185D;
            // 
            // cellTime
            // 
            this.cellTime.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellTime.Name = "cellTime";
            this.cellTime.StylePriority.UseFont = false;
            this.cellTime.Weight = 0.15959361419131002D;
            // 
            // cellMember
            // 
            this.cellMember.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellMember.Name = "cellMember";
            this.cellMember.StylePriority.UseFont = false;
            this.cellMember.StylePriority.UseTextAlignment = false;
            this.cellMember.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellMember.Weight = 0.14986276942249824D;
            // 
            // cellSID
            // 
            this.cellSID.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellSID.Name = "cellSID";
            this.cellSID.StylePriority.UseFont = false;
            this.cellSID.StylePriority.UseTextAlignment = false;
            this.cellSID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellSID.Weight = 0.33757178805253D;
            // 
            // cellAccountDesc
            // 
            this.cellAccountDesc.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellAccountDesc.Name = "cellAccountDesc";
            this.cellAccountDesc.StylePriority.UseFont = false;
            this.cellAccountDesc.Weight = 0.2737259271456674D;
            // 
            // cellAccountNo
            // 
            this.cellAccountNo.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellAccountNo.Name = "cellAccountNo";
            this.cellAccountNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellAccountNo.StylePriority.UseFont = false;
            this.cellAccountNo.StylePriority.UsePadding = false;
            this.cellAccountNo.Weight = 0.30533108304133644D;
            // 
            // cellValue
            // 
            this.cellValue.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellValue.Name = "cellValue";
            this.cellValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellValue.StylePriority.UseFont = false;
            this.cellValue.StylePriority.UsePadding = false;
            this.cellValue.StylePriority.UseTextAlignment = false;
            this.cellValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellValue.Weight = 0.33417088479342216D;
            // 
            // cellExecDate
            // 
            this.cellExecDate.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellExecDate.Name = "cellExecDate";
            this.cellExecDate.NullValueText = "-";
            this.cellExecDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.cellExecDate.StylePriority.UseFont = false;
            this.cellExecDate.StylePriority.UsePadding = false;
            this.cellExecDate.StylePriority.UseTextAlignment = false;
            this.cellExecDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellExecDate.Weight = 0.21292993229645107D;
            // 
            // cellExecTime
            // 
            this.cellExecTime.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellExecTime.Name = "cellExecTime";
            this.cellExecTime.NullValueText = "-";
            this.cellExecTime.StylePriority.UseFont = false;
            this.cellExecTime.StylePriority.UseTextAlignment = false;
            this.cellExecTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellExecTime.Weight = 0.15357841083863283D;
            // 
            // cellStatus
            // 
            this.cellStatus.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cellStatus.Name = "cellStatus";
            this.cellStatus.StylePriority.UseFont = false;
            this.cellStatus.Weight = 0.2866254769873337D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 54F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(658F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 31.25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.tblHeader.SizeF = new System.Drawing.SizeF(809F, 31.25F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.header1,
            this.header2,
            this.header3,
            this.xrTableCell1,
            this.header4,
            this.header5,
            this.header6,
            this.header7,
            this.header8,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // header1
            // 
            this.header1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header1.Name = "header1";
            this.header1.StylePriority.UseFont = false;
            this.header1.Text = "No";
            this.header1.Weight = 0.16395255750199164D;
            // 
            // header2
            // 
            this.header2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header2.Name = "header2";
            this.header2.StylePriority.UseFont = false;
            this.header2.Text = "No Refference";
            this.header2.Weight = 0.43182940719488894D;
            // 
            // header3
            // 
            this.header3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header3.Name = "header3";
            this.header3.StylePriority.UseFont = false;
            this.header3.Text = "Date";
            this.header3.Weight = 0.19082818201230323D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Time";
            this.xrTableCell1.Weight = 0.15956735918877357D;
            // 
            // header4
            // 
            this.header4.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header4.Name = "header4";
            this.header4.StylePriority.UseFont = false;
            this.header4.Text = "Member ID";
            this.header4.Weight = 0.14988902427008527D;
            // 
            // header5
            // 
            this.header5.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header5.Name = "header5";
            this.header5.StylePriority.UseFont = false;
            this.header5.Text = "SID";
            this.header5.Weight = 0.33757190442858914D;
            // 
            // header6
            // 
            this.header6.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header6.Name = "header6";
            this.header6.StylePriority.UseFont = false;
            this.header6.Text = "Account Description";
            this.header6.Weight = 0.27372570751846309D;
            // 
            // header7
            // 
            this.header7.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header7.Name = "header7";
            this.header7.StylePriority.UseFont = false;
            this.header7.Text = "Account No";
            this.header7.Weight = 0.30533132417910491D;
            // 
            // header8
            // 
            this.header8.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.header8.Name = "header8";
            this.header8.StylePriority.UseFont = false;
            this.header8.Text = "Value";
            this.header8.Weight = 0.33417111431910806D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "Execution Date";
            this.xrTableCell7.Weight = 0.36650834954269368D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Status";
            this.xrTableCell8.Weight = 0.28662506984399844D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xtraReport1
            // 
            this.xtraReport1.Name = "xtraReport1";
            this.xtraReport1.PageHeight = 1100;
            this.xtraReport1.PageWidth = 850;
            this.xtraReport1.Version = "12.2";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrLine1,
            this.lbTitle});
            this.ReportHeader.HeightF = 59F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.Black;
            this.xrPageInfo2.Format = "{0:\"Current Date: \" dddd, dd MMMM yyyy}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(517F, 26.37501F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(292F, 23F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(809F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle
            // 
            this.lbTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lbTitle.ForeColor = System.Drawing.Color.Black;
            this.lbTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle.SizeF = new System.Drawing.SizeF(196.93F, 38F);
            this.lbTitle.StylePriority.UseForeColor = false;
            this.lbTitle.Text = "Withdrawal";
            this.lbTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // withdrawalReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(20, 21, 54, 51);
            this.Version = "12.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.withdrawalReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell header1;
        private DevExpress.XtraReports.UI.XRTableCell header2;
        private DevExpress.XtraReports.UI.XRTable tableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellDate;
        private DevExpress.XtraReports.UI.XtraReport xtraReport1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTableCell header5;
        private DevExpress.XtraReports.UI.XRTableCell header3;
        private DevExpress.XtraReports.UI.XRTableCell header4;
        private DevExpress.XtraReports.UI.XRTableCell header6;
        private DevExpress.XtraReports.UI.XRTableCell header7;
        private DevExpress.XtraReports.UI.XRTableCell header8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell cellReff;
        private DevExpress.XtraReports.UI.XRTableCell cellMember;
        private DevExpress.XtraReports.UI.XRTableCell cellSID;
        private DevExpress.XtraReports.UI.XRTableCell cellAccountDesc;
        private DevExpress.XtraReports.UI.XRTableCell cellAccountNo;
        private DevExpress.XtraReports.UI.XRTableCell cellValue;
        private DevExpress.XtraReports.UI.XRTableCell cellStatus;
        private DevExpress.XtraReports.UI.XRTableCell cellExecTime;
        private DevExpress.XtraReports.UI.XRTableCell cellExecDate;
        private DevExpress.XtraReports.UI.XRTableCell cellNo;
        private DevExpress.XtraReports.UI.XRTableCell cellTime;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
    }
}
