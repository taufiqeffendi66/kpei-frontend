﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using DevExpress.Web.ASPxGridView;
using Common;
using Common.giveUpWS;
using Common.wsTransaction;

//using Common.memberInfoWS;
using Common.wsUserGroupPermission;


namespace imq.kpei.derivatif
{
    public partial class giveUpPage : System.Web.UI.Page
    {
        ASPxGridView detailView;
        ASPxGridView detailViewNet;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if (aUserMember.Trim().Equals("kpei"))
                    btnNew.Enabled = false;
                else
                    Permission();

                //FillMember();
                gvGiveUp.DataBind();
                try
                {
                    UiDataService uds = ImqWS.GetTransactionService();
                    bool validate = uds.validateGutuTime();
                    if (!validate)
                    {
                        btnNew.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    string mystring = String.Format(@"<script type='text/javascript'>
                                                                      jAlert('{0}','Information');
                                                                      </script>", ex.Message);
                    if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                        ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);

                    btnNew.Enabled = false;
                }
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Give Up");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
            }
        }

        private void EnableButton(bool enable)
        {
            btnNew.Enabled = enable;
        }

        protected void detailGrid_OnLoad(object sender, EventArgs e)
        {
            detailView = sender as ASPxGridView;
        }
        protected void detailGrid_DataBinding(object sender, EventArgs e)
        {
            try
            {
                detailView = sender as ASPxGridView;
                if (hfGu.Get("guNo") != null)
                    detailView.DataSource = getGiveUpChild(int.Parse(hfGu.Get("guNo").ToString()));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        protected void detailNet_OnLoad(object sender, EventArgs e)
        {
            detailViewNet = sender as ASPxGridView;
        }
        protected void detailNet_DataBinding(object sender, EventArgs e)
        {
            try
            {
                detailViewNet = sender as ASPxGridView;
                if (hfGu.Get("sid") != null)
                    detailViewNet.DataSource = getNetposition(hfGu.Get("sid").ToString(), hfGu.Get("guMemberId").ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        protected void detailNet_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView theGrid = (sender as ASPxGridView);
            hfGu.Set("sid", theGrid.GetMasterRowKeyValue());
            //int rowIndex = theGrid.FindVisibleIndexByKeyValue(hfGu.Get("tradingId"));
            //theGrid.FocusedRowIndex = rowIndex;
            //hfGu.Set("memberId",(sender as ASPxGridView).GetRowValues(
        }
        protected void gvGiveUp_DataBinding(object sender, EventArgs e)
        {
            gvGiveUp.DataSource = getGiveUp();
        }

        private clearingDto[] getNetposition(String sid, String memberId)
        {
            clearingDto[] list = null;
            UiDataService ws = null;
            try
            {
                ws = ImqWS.GetTransactionService();
                list = ws.getClearing(aUserMember.Trim(), memberId.Trim(), "", sid, "", "", "", "last");
            }
            catch (Exception ex)
            {
                if (ws != null)
                    ws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (ws != null)
                    ws.Dispose();
            }

            return list;
            //netposition[] list = null;
            //GiveUpWSService gws = null;
            //try
            //{
            //    gws = ImqWS.GetGiveUpService();
            //    list = gws.getNetposition(tradingId.Trim(), memberId.Trim());
            //}
            //catch (Exception e)
            //{
            //    if (gws != null)
            //        gws.Abort();

            //    Debug.WriteLine(e.Message);
            //}
            //finally
            //{
            //    if (gws != null)
            //        gws.Dispose();
            //}

            //return list;
        }
        private static giveUpChild[] getGiveUpChild(long guNo)
        {
            GiveUpWSService gws = null;
            giveUpChild[] list = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                list = gws.getChild(guNo);
            }
            catch (Exception e)
            {
                if (gws != null)
                    gws.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                gws.Dispose();
            }

            return list;
        }

        private giveUp[] getGiveUp()
        {
            GiveUpWSService gws = null;
            giveUp[] list = null;
            try
            {
                if (!IsPostBack)
                {
                    gws = ImqWS.GetGiveUpService();
                    string member = aUserMember.ToLower();
                    if (member.Contains("kpei"))
                        list = gws.get();
                    else
                        list = gws.getByMemberID(aUserMember.Trim(), 0);

                    if (list != null)
                    {
                        foreach (giveUp gu in list)
                        {
                            if (cmbMember.Items.FindByValue(gu.guMemberId.Trim()) == null)
                            {
                                cmbMember.Items.Add(gu.guMemberId.Trim(), gu.guMemberId.Trim());
                            }

                            if (cmbMember.Items.FindByValue(gu.tuMemberId.Trim()) == null)
                            {
                                cmbMember.Items.Add(gu.tuMemberId.Trim(), gu.tuMemberId.Trim());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (gws != null)
                    gws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (gws != null)
                    gws.Dispose();
            }
            return list;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("giveupform1.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void detailGrid_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView theGrid = (sender as ASPxGridView);
            hfGu.Set("guNo", theGrid.GetMasterRowKeyValue());

            int rowIndex = gvGiveUp.FindVisibleIndexByKeyValue(hfGu.Get("guNo"));
            gvGiveUp.FocusedRowIndex = rowIndex;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string search;
            if (cmbMember.Text != String.Empty)
                search = String.Format("[guMemberId] like '%{0}%' OR [tuMemberId] like '%{0}%'", cmbMember.Text.Trim());
            else
                search = String.Empty;
            gvGiveUp.FilterExpression = search;
        }

        protected void btnDetail_Click(object sender, EventArgs e)
        {
        }

        //protected void FillMember()
        //{
        //    MemberInfoWSService mis = null;
        //    try
        //    {
        //        mis = ImqWS.GetMemberInfoWebService();
        //        Common.memberInfoWS.memberInfo[] members = mis.get();
        //        foreach (Common.memberInfoWS.memberInfo member in members)
        //        {
        //            cmbMember.Items.Add(member.memberId.Trim(), member.memberId.Trim());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mis.Abort();
        //        Debug.WriteLine(ex.Message);
        //    }
        //    finally
        //    {
        //        mis.Dispose();
        //    }
        //}
    }
}
