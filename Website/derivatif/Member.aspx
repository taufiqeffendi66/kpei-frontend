﻿<%@ Page Title="Member List & Detail" Language="C#" AutoEventWireup="true" CodeBehind="Member.aspx.cs" Inherits="imq.kpei.derivatif.member" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <% if (DesignMode){ %>
        <script type="text/javascript" src="../ASPxScriptIntelliSense.js"></script>
    <%} %>
    
    <script type="text/javascript">
        // <![CDATA[
        
        function OnGridFocusedRowChanged() {
            grid.GetRowValues(grid.GetFocusedRowIndex(),
                            'memberId;memberName;npwp;address;phone;fax;contactPerson;email;adminUser;sktNo;sktDate;activeDate;memberType;status;clearingMethod;contractGroup;description;idx;', OnGetRowValues);
        }

        function ConfirmDelete(s, e) {
            e.processOnServer = confirm("Are you sure Delete this Member \nMember ID : " + hfmember.Get("memberId") + " \nName : " + hfmember.Get("name") + " ?");
        }

        function OnGetRowValues(values) {
            hfmember.Set("memberId", values[0]);
            hfmember.Set("name", values[1]);
            hfmember.Set("npwp", values[2]);
            hfmember.Set("address", values[3]);
            hfmember.Set("phone", values[4]);
            hfmember.Set("fax", values[5]);
            hfmember.Set("contactPerson", values[6]);
            hfmember.Set("email", values[7]);
            hfmember.Set("adminUser", values[8]);
            hfmember.Set("sktNo", values[9]);
            hfmember.Set("sktDate", values[10]);
            hfmember.Set("activeDate", values[11]);
            hfmember.Set("memberType", values[12]);
            hfmember.Set("status", values[13]);
            hfmember.Set("clearingMethod", values[14]);
            hfmember.Set("contractGroup", values[15]);
            hfmember.Set("description", values[16]);
            hfmember.Set("idx", values[17]);
                       
            //btndel.SetEnabled(true);
            //btned.SetEnabled(true);
        }

        function formatDate(d) {
            var dt = new Date();
            dt = d;
            var mm = dt.getMonth() + 1;
            var dd = dt.getDate();
            var yyyy = dt.getFullYear();
            var date = mm + '/' + dd + '/' + yyyy;
            return date;
        }

        function view(s, e) {
            window.open("MemberView.aspx?member=" + cmbMember.GetText() + "&name=" + cmbName.GetText(), "", "fullscreen=yes;scrollbars=auto");
        }
        // ]]>
    </script>
    

    <div class="content">
                
        <div class="title"><dx:ASPxLabel ID="txtTitle" runat="server" Text="Member List & Detail" CssClass="title" /></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <table id="tblMember">
                <tr>
                    <td nowrap="nowrap">Member ID</td>
                    <td><dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="Contains"
                            DropDownRows="10" DropDownStyle="DropDown" Width="170px" 
                            ClientInstanceName="cmbMember" 
                            onselectedindexchanged="cmbMember_SelectedIndexChanged" >
                       
                            <DropDownButton Visible="false"></DropDownButton>
                    </dx:ASPxComboBox> </td>
                    <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" 
                            onclick="btnSearch_Click" /></td>
                    <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                        <ClientSideEvents Click="view"/>
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><dx:ASPxComboBox ID="cmbName" runat="server" IncrementalFilteringMode="Contains"
                            DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbName">
                       
                            <DropDownButton Visible="false"></DropDownButton>
                    </dx:ASPxComboBox></td>
                </tr>
                                    
            </table>
                <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />                        
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" 
                            ClientInstanceName="btned" onclick="btnEdit_Click" />
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" 
                            ClientInstanceName="btndel" onclick="btnDelete_Click" 
                            ClientSideEvents-Click="ConfirmDelete"/>
                    </td>
                </tr>
            </table>
            <br />   
           
            <dx:ASPxGridView ID="gvMemberList" runat="server" AutoGenerateColumns="false" CssClass="grid"
                KeyFieldName="memberId" ondatabinding="gvMemberList_DataBinding" ClientInstanceName="grid"
                EnableCallBacks="true">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" FieldName="memberId"/>                            
                    <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="1" FieldName="memberName" />
                    <dx:GridViewDataTextColumn Caption="Member Type" VisibleIndex="2" FieldName="memberTypeName"/>
                    <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="3" FieldName="statusName"/>
                    <dx:GridViewDataTextColumn Caption="NPWP" VisibleIndex="4" Visible="false" FieldName="npwp"/>
                    <dx:GridViewDataTextColumn Caption="Address" VisibleIndex="5" Visible="false" FieldName="address" />
                    <dx:GridViewDataTextColumn Caption="Phone" VisibleIndex="6" Visible="false" FieldName="phone" />
                    <dx:GridViewDataTextColumn Caption="Fax" VisibleIndex="7" Visible="false" FieldName="fax" />
                    <dx:GridViewDataTextColumn Caption="Contact Person" VisibleIndex="8" Visible="false" FieldName="contactPerson" />
                    <dx:GridViewDataTextColumn Caption="E-Mail" VisibleIndex="9" Visible="false" FieldName="email" />
                    <dx:GridViewDataTextColumn Caption="Admin User" VisibleIndex="10" Visible="false" FieldName="adminUser" />
                    <dx:GridViewDataTextColumn Caption="No. SKT" VisibleIndex="11" Visible="false" FieldName="sktNo" />
                    <dx:GridViewDataDateColumn Caption="SKT Date" VisibleIndex="12" Visible="false" FieldName="sktDate" />
                    <dx:GridViewDataDateColumn Caption="Active Date" VisibleIndex="13" Visible="false" FieldName="activeDate" />
                    <dx:GridViewDataTextColumn Caption="Clearing Method" VisibleIndex="14" Visible="false" FieldName="clearingMethod" />
                    <dx:GridViewDataTextColumn Caption="Specific Contract" VisibleIndex="15" Visible="false" FieldName="contractGroup" />
                    <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="16" Visible="false" FieldName="description" />
                    <dx:GridViewDataTextColumn Caption="MemberType" VisibleIndex="17" Visible="false" FieldName="memberType" />
                    <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="18" Visible="false" FieldName="status" />
                    <dx:GridViewDataTextColumn Caption="IDX" VisibleIndex="19" Visible="false" FieldName="idx" />                    
                </Columns>
                   
                <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <Settings ShowVerticalScrollBar="True" />
            </dx:ASPxGridView>
            <dx:ASPxHiddenField ID="hfMember" runat="server" ClientInstanceName="hfmember" />  
            
    </div>
                               
</asp:Content>
