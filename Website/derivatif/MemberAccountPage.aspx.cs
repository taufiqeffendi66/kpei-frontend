﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.bankWS;
using Common.memberInfoWS;
using Common.memberAccountWS;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class memberaccountpage : System.Web.UI.Page
    {
        private memberAccount[] list;
        public int addedit = 0;
        private String aUserLogin;
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                Permission();
                String member = aUserMember.ToLower();
                if (!member.Contains("kpei"))
                {
                    cmbMember.Text = aUserMember;
                    cmbMember.Enabled = false;
                    cmbMember.DropDownButton.Visible = false;
                }
                else
                    FillMember();
                FillBank();
                gvMember.DataBind();
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Member Account");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        private memberAccount[] getMemberAccount()
        {
            MemberAccountWSService mcas = null;
            try
            {
                mcas = ImqWS.getMemberAccountService();
                
                list = mcas.get();

                foreach (memberAccount ma in list)
                {
                    if (ma.status.ToLower() == "a") ma.status = "Active";
                    if (ma.status.ToLower() == "c") ma.status = "Default by SD";
                    if (ma.status.ToLower() == "d") ma.status = "Default by GF";
                }
                
                if (!IsPostBack)
                {
                    foreach (memberAccount ma in list)
                    {
                        //if (ma.status == "c") ma.status = "Default by DS";
                        //if (ma.status == "d") ma.status = "Default by GF";
                        cmbSID.Items.Add(ma.sid, ma.sid);
                    }
                }
            }           
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                mcas.Abort();
            }
            finally
            {
                mcas.Dispose();
            }
            return list;
        }

       
        protected void gvMember_DataBinding(object sender, EventArgs e)
        {
            gvMember.DataSource = getMemberAccount();
         
            
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("MemberAccountEdit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            SendResponse("1");

        }

        private void SendResponse(string stat)
        {
            string idx = hdMember.Get("idx") == null ? "" : hdMember.Get("idx").ToString();
            string memberId = hdMember.Get("member_id") == null ? "" : hdMember.Get("member_id").ToString();
            string sid = hdMember.Get("sid") == null ? "" : hdMember.Get("sid").ToString();
            string tradingId = hdMember.Get("trading_id") == null ? "" : hdMember.Get("trading_id").ToString();
            string accountId = hdMember.Get("account_id") == null ? "" : hdMember.Get("account_id").ToString();
            string role = hdMember.Get("role") == null ? "" : hdMember.Get("role").ToString();
            string bankCode = hdMember.Get("role") == null ? "" : hdMember.Get("bankCode").ToString();
            string sec_dep_acc_no = hdMember.Get("sec_dep_acc_no") == null ? "" : hdMember.Get("sec_dep_acc_no").ToString();
            string collateral_acc_no = hdMember.Get("collateral_acc_no") == null ? "" : hdMember.Get("collateral_acc_no").ToString();
            string settlement_acc_no = hdMember.Get("settlement_acc_no") == null ? "" : hdMember.Get("settlement_acc_no").ToString();
            string free_acc_no = hdMember.Get("free_acc_no") == null ? "" : hdMember.Get("free_acc_no").ToString();
            string strResp = String.Format("MemberAccountEdit.aspx?stat={0}&idx={1}&member_id={2}&sid={3}&trading_id={4}&account_id={5}&role={6}&bankCode={7}&sec_dep_acc_no={8}&collateral_acc_no={9}&settlement_acc_no={10}&free_acc_no={11}", stat, idx, memberId, sid, tradingId, accountId, role, bankCode, sec_dep_acc_no, collateral_acc_no, settlement_acc_no, free_acc_no);
            Response.Redirect(strResp, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void FillBank()
        {
            BankWSService bws = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                Common.bankWS.bank[] banks = bws.get();
                foreach (Common.bankWS.bank bk in banks)
                {
                    cmbBank.Items.Add(bk.name, bk.bankCode);
                }
            }
            catch (Exception ex)
            {
                bws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                bws.Dispose();
            }
        }

        protected void FillMember()
        {
            MemberInfoWSService mis = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                Common.memberInfoWS.memberInfo[] members = mis.get();
                foreach (Common.memberInfoWS.memberInfo member in members)
                {
                    cmbMember.Items.Add(member.memberId, member.memberId);
                }
            }
            catch (Exception ex)
            {
                mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                mis.Dispose();
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SendResponse("4");
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            string bank = cmbBank.Text.Trim();
            string member = cmbMember.Text.Trim();
            string sid = cmbSID.Text.Trim();
            string search = String.Empty;

            if (bank != String.Empty)
            {
                bank = bank.Replace("'", "''");
                bank = bank.Replace("--", "-");
                bank = String.Format("[bank.bankName] like '%{0}%'", bank);
            }

            if (member != String.Empty)
            {
                member = member.Replace("'", "");
                member = member.Replace("-", "");
                member = String.Format("[memberId] like '%{0}%'", member);
            }

            if (sid != String.Empty)
            {
                sid = sid.Replace("'", "''");
                sid = sid.Replace("--", "-");
                sid = String.Format("[sid] like '%{0}%'", sid);
            }

            if (bank != String.Empty)
            {
                if (member != String.Empty)
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1} and {2}", bank, member, sid);
                    else
                        search = String.Format("{0} and {1}", bank, member);
                }
                else
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1}", bank, sid);
                    else
                        search = bank;
                }
            }
            else
            {
                if (member != String.Empty)
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1}", member, sid);
                    else
                        search = member;
                }
                else
                {
                    if (sid != String.Empty)
                        search = sid;
                    else
                        search = String.Empty;
                }
            }
            gvMember.FilterExpression = search;
           
        }

        protected void gvMember_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
            string Status = gvMember.GetRowValues(Convert.ToInt32(e.VisibleIndex), "status").ToString();
            if (Status == "Default by SD")
            {
                e.Row.BackColor = System.Drawing.Color.Red;
            }
            else if (Status == "Default by GF")
            {
                e.Row.BackColor = System.Drawing.Color.Red;
            }
        }
               
    }
}