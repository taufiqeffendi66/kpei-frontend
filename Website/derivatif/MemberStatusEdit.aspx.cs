﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using DevExpress.Web.ASPxClasses.Internal;
using Common;
using Common.memberAccountWS;
using Common.memberStatusWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class MemberStatusEdit : System.Web.UI.Page
    {
        private int addedit;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private memberStatusTmp aMemberStatusTemp;
        private const String module = "Member Status";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"]));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }

                btnContinue.Visible = false;
                btnChecker.Visible = false;
                btnApproval.Visible = false;
                btnReject.Visible = false;
                switch (addedit)
                {
                    case 0:
                        lblTitle.Text = "New " + module; //New Direct                        
                        btnContinue.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lblTitle.Text = "Edit " + module; //Edit Direct
                        btnContinue.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = "New " + module; //New From Temporary
                        Permission(false);

                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3:
                        lblTitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                }

                ParseQueryString();
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            if (Request.QueryString["memberId"] != null)
            {
                string statusCurrent = CommonUtils.IsNullValue(Request.QueryString["statusCurrent"]) ? "" : Server.UrlDecode(Request.QueryString["statusCurrent"].Trim());
                //string memberId = CommonUtils.IsNullValue(Request.QueryString["memberId"]) ? "" : Server.UrlDecode(Request.QueryString["memberId"].Trim());
                txtMember.Text = CommonUtils.IsNullValue(Request.QueryString["statusCurrent"]) ? "" : Server.UrlDecode(Request.QueryString["memberId"].Trim());

                string statusName = CommonUtils.IsNullValue(Request.QueryString["statusCurrent"]) ? "" : Server.UrlDecode(Request.QueryString["statusCurrentName"].Trim());
                string positionLimit = CommonUtils.IsNullValue(Request.QueryString["positionLimit"]) ? "" : Server.UrlDecode(Request.QueryString["positionLimit"].Trim());
                txtCurrent.Text = statusName;
                hfStatus.Set("statusCurrent", statusCurrent);
                hfStatus.Set("positionLimit", positionLimit);
                cmbPL.Items.FindByValue(positionLimit);
            }
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void GetTemporary()
        {
            MemberStatusWSService mss = null;
            try
            {
                mss = ImqWS.GetMemberStatusService();

                memberStatusTmp mst = mss.getMemberStatusTemp(int.Parse(Request.QueryString["idxTmp"]));
                txtMember.Text = mst.memberId;
                cmbStatus.Text = mst.statusCurrentName;
                txtCurrent.Text = mst.statusBeforeName;
                cmbPL.Items.FindByValue(mst.positionLimit.Trim());
            }
            catch (Exception e)
            {
                if (mss != null)
                    mss.Abort();
                lblError.Text = e.Message;
                Debug.WriteLine(e.Message);
            }
            finally
            {
                mss.Dispose();
            }
        }

        private void DisabledControl()
        {
            txtMember.Enabled = false;
            cmbStatus.Enabled = false;
            txtCurrent.Enabled = false;
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            MemberStatusWSService mss = null;
            MemberAccountWSService mas = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                mss = ImqWS.GetMemberStatusService();
                mas = ImqWS.getMemberAccountService();
                dtA = aps.Search(Request.QueryString["reffNo"]);
                aMemberStatusTemp = mss.getMemberStatusTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = mss.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                mas.updateStatus(txtMember.Text.Trim(), cmbStatus.SelectedItem.Value.ToString().Trim());

                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);

                                ret = mss.deleteTempById(aMemberStatusTemp.statusNo);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (mss != null)
                    mss.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
                if (mss != null)
                    mss.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aMemberStatusTemp));
        }

        private static String Log(memberStatusTmp mst)
        {
            string strLog = String.Format("[Member ID {0}, Status Current : {1}, Status Before : {2}]", mst.memberId, mst.statusCurrent, mst.statusBefore);
            return strLog;
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("MemberStatusPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }

        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, memberStatusTmp mst)
        {
            string ret = "0";
            const string target = @"../derivatif/MemberStatusPage.aspx";
            if (!memberStatusTmp.ReferenceEquals(aMemberStatusTemp, mst))
                aMemberStatusTemp = mst;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    MemberStatusWSService mss = ImqWS.GetMemberStatusService();

                    memberStatus ms = new memberStatus() { memberId = mst.memberId, statusBefore = mst.statusBefore, statusCurrent = mst.statusCurrent, positionLimit = mst.positionLimit };
                    //ser.idx = aSeriesTemp.idx;

                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = mss.Add(ms);
                            if (ret.Equals("0"))
                            {
                                using (MemberAccountWSService mas = ImqWS.getMemberAccountService())
                                {
                                    mas.updateStatus(ms.memberId, ms.statusCurrent);
                                }
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }
                    break;
            }
            return ret;
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (btnContinue.Text == "Continue")
            {
                lblConfirm.Text = "Are you sure ?";
                btnContinue.Text = "Submit";
            }
            else
            {
                lblConfirm.Text = "";
                saveStatus();
            }
        }

        private void saveStatus()
        {
            if (cmbStatus.Text.Length > 0)
            {
                if (!cmbStatus.SelectedItem.Value.ToString().Trim().Equals(hfStatus.Get("statusCurrent").ToString().Trim()))
                {
                    ApprovalService aps = null;
                    MemberStatusWSService mss = null;
                    try
                    {
                        aps = ImqWS.GetApprovalWebService();
                        mss = ImqWS.GetMemberStatusService();

                        addedit = (int)Session["stat"];
                        int idRec = -1;

                        memberStatusTmp mst = new memberStatusTmp() { memberId = txtMember.Text.Trim(), statusCurrent = cmbStatus.SelectedItem.Value.ToString(), statusBefore = hfStatus.Get("statusCurrent").ToString().Trim(), positionLimit = hfStatus.Get("positionLimit").ToString().Trim() };

                        idRec = mss.addToTemp(mst);
                        aMemberStatusTemp = mst;
                        //ms.statusDate = DateTime.Now;
                        //ms.statusDateSpecified = true;

                        if (idRec > -1)
                        {
                            dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/MemberStatusEdit.aspx", status = "M", memberId = aUserMember };
                            //int idTable;
                            switch (addedit)
                            {
                                case 0:
                                    dtA.topic = "Add " + module;
                                    dtA.insertEdit = "I";
                                    aMemberStatusTemp.statusNo = idRec;
                                    break;
                            }
                            string retVal = rblApprovalChanged(dtA, aps, aMemberStatusTemp);
                            if (!retVal.Equals("0"))
                            {
                                ShowMessage(retVal, @"../derivatif/MemberStatusPage.aspx");
                            }
                        }
                        else
                        {
                            ShowMessage("Failed On Save To Temporary Member Status", @"../derivatif/MemberStatusPage.aspx");
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                        if (mss != null)
                            mss.Abort();
                    }
                    finally
                    {
                        if (mss != null)
                            mss.Dispose();
                    }
                }
            }
        }
    }
}
