﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.liquidationWS;
using System.Web;

namespace imq.kpei.derivatif
{
    public partial class LiquidationReport : DevExpress.XtraReports.UI.XtraReport
    {
        private liquidation[] listLiquidation;
        public String aUserMember;
        public String aMember;
        public String aSID;
        public String aSeries;

        public LiquidationReport()
        {
            InitializeComponent();
        }

        private void GetLiquidation()
        {
            LiquidationWSService lws = null;
            try
            {
                lws = ImqWS.GetLiquidationService();
                //string aMember = aUserMember.ToLower();
                //if (aMember.Contains("kpei"))
                //    listLiquidation = lws.get();
                //else
                //    listLiquidation = lws.getByMemberID(aUserMember);
                listLiquidation = lws.Search(aSeries, aMember, aSID);
                if (listLiquidation != null)
                {
                    DataSource = listLiquidation;
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "defMemberId");
                    cellDefMember.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "defRole");
                    cellDefRole.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "defSID");
                    cellDefSID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "defPos");
                    cellDefPos.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.liquidationDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.liquidationDate");
                    binding.FormatString = "{0:HH:mm}";
                    cellTime.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "oppMemberId");
                    cellOpMember.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "oppRole");
                    cellOpRole.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "oppSID");
                    cellOpSID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "oppPos");
                    cellOpPos.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "series");
                    cellSeries.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "contractType");
                    cellType.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "quantity");
                    cellQuantity.DataBindings.Add(binding);
                }
            }
            catch (Exception ex)
            {
                lws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                lws.Dispose();
            }
        }

        private void LiquidationReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetLiquidation();
        }
    }
}
