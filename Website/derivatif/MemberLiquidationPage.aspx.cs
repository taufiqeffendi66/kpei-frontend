﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web.ASPxGridView;
using Common;
using Common.wsUserGroupPermission;
using Common.memberLiquidationWS;

namespace imq.kpei.derivatif
{
    public partial class MemberLiquidationPage : System.Web.UI.Page
    {
        ASPxGridView detailView;
        private string aUserLogin;
        private string aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                if (!aUserMember.Trim().Equals("kpei"))
                    btnLiquidated.Enabled = false;
                else
                    Permission();

                gvMemLiq.DataBind();
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Member Liquidation");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
            }
        }

        private void EnableButton(bool enable)
        {
            btnLiquidated.Enabled = enable;
           
        }

        protected void gvMemLiq_DataBinding(object sender, EventArgs e)
        {
            memberLiquidation[] data = getMemberLiquidation("");
            if (data != null)
                gvMemLiq.DataSource = data.GroupBy(p => p.id.id).Select(g => g.First()).ToArray();
            else
                btnLiquidated.Enabled = false;
        }

        private memberLiquidation[] getMemberLiquidation(string condition)
        {
            MemberLiquidationWSService mlws = null;
            memberLiquidation[] list = null;
            try
            {
                mlws = ImqWS.GetMemberLiquidationService();
                if (string.IsNullOrEmpty(condition))
                    condition = string.Empty;

                list = mlws.get(condition);
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                if (mlws != null)
                    mlws.Abort();
            }
            finally
            {
                if (mlws != null)
                    mlws.Dispose();
            }

            return list;
        }

        protected void detailGrid_OnLoad(object sender, EventArgs e)
        {
            detailView = sender as ASPxGridView;
        }

        protected void detailGrid_DataBinding(object sender, EventArgs e)
        {
            try
            {
                detailView = sender as ASPxGridView;
                if (hfML.Get("id") != null)
                    detailView.DataSource = getMemberLiquidation(" where id.id = " + hfML.Get("id").ToString().Trim());
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        protected void detailGrid_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView theGrid = (sender as ASPxGridView);
            hfML.Set("id", theGrid.GetMasterRowKeyValue());

            int rowIndex = gvMemLiq.FindVisibleIndexByKeyValue(hfML.Get("id"));
            gvMemLiq.FocusedRowIndex = rowIndex;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string id = cmbID.Text.Trim();

            if (id != string.Empty)
            {
                id = id.Replace("'", "''");
                id = id.Replace("--", "");
                id = "[id.memberId] =" + id ;
            }

            gvMemLiq.FilterExpression = id;
        }

        protected void btnLiquidated_Click(object sender, EventArgs e)
        {
            if (hfML.Get("id") != null)
            {
                //string id = hfML.Get("id").ToString();
                string strRedirect = String.Format("MemberLiquidationEdit.aspx?stat=1&idTable={0}&memberId={1}&indicator={2}&liqType={3}&dates={4}&fi={5}", 
                    Server.UrlEncode(hfML.Get("id").ToString()), Server.UrlEncode(hfML.Get("memberId").ToString()),
                    Server.UrlEncode(hfML.Get("indicator").ToString()),
                    Server.UrlEncode(hfML.Get("liqType").ToString()),
                    Server.UrlEncode(hfML.Get("dates").ToString()),
                    Server.UrlEncode(hfML.Get("fakeindicator").ToString()));
                Response.Redirect(strRedirect, false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
    }
}
