﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="InquiryTradeHist.aspx.cs" Inherits="imq.kpei.derivatif.InquiryTradeHist" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        function view(s, e) {
            /*window.open("InquiryTradeView.aspx?memberId=" + cmbMember.GetText() + "&contractId=" + cmbcontract.GetText() +
                        "&sid=" + cmbsid.GetText() + "&contractType=" + cmbtype.GetValue(), "", "fullscreen=yes;scrollbars=auto");
                        */
        }
    </script>
    <div class="content">
        <div class="title">Inquiry Trade History</div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" /> 
        <table>
            <tr>
                <td>Member ID</td>
                <td>
                    <dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="StartsWith"
                            DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbMember"
                            ValueField="memberId" TextField="memberId" EnableCallbackMode="true" CallbackPageSize="10">
                        <DropDownButton Visible="false" />                        
                    </dx:ASPxComboBox>
                </td>
                <td>
                    <dx:ASPxButton ID="btnGO" runat="server" Text="Search" Width="65px" 
                        onclick="btnGO_Click"></dx:ASPxButton>
                    
                </td>
                <td>
                    <dx:ASPxButton ID="btnExport" runat="server" Text="Export" 
                        onclick="btnExport_Click">
                    <ClientSideEvents Click="view"/>
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td>Contract ID</td>
                <td>
                    <dx:ASPxComboBox ID="cmbContract" runat="server" IncrementalFilteringMode="StartsWith"
                            DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbcontract"
                            ValueField="series" TextField="series" EnableCallbackMode="true" CallbackPageSize="10">
                        <DropDownButton Visible="false" />                        
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td>SID</td>
                <td>
                    <dx:ASPxComboBox ID="cmbSID" runat="server" IncrementalFilteringMode="StartsWith"
                            DropDownRows="10"  DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbsid"
                            ValueField="sid" TextField="sid" EnableCallbackMode="true" CallbackPageSize="10">
                        <DropDownButton Visible="false" />                        
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td>Contract Type</td>
                <td>
                    <dx:ASPxComboBox ID="cmbType" runat="server" IncrementalFilteringMode="StartsWith"
                            DropDownRows="3"  DropDownStyle="DropDownList" Width="170px" ClientInstanceName="cmbtype">                        
                        <Items>
                            <dx:ListEditItem Text="ALL" Value="A" Selected="true"/>
                            <dx:ListEditItem Text="Option" Value="O" />
                            <dx:ListEditItem Text="Future" Value="F" />
                        </Items>                      
                    </dx:ASPxComboBox>
                </td>
            </tr>
            </table>
        
        <br />

        <dx:ASPxGridView ID="gvTradeHistory" runat="server" KeyFieldName="id.buySell;id.series;id.tradeNo"
            AutoGenerateColumns="False" ondatabinding="gvTradeHistory_DataBinding" >
            <Columns>
                <dx:GridViewBandColumn Caption="Trade" Visible="true" VisibleIndex="0">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id.tradeNo" Caption="No" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="tradeDate" Caption="Date" VisibleIndex="1">
                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="tradeTime" Caption="Time" VisibleIndex="2">
                            <PropertiesTextEdit DisplayFormatString="##:##:##"></PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewDataTextColumn Caption="Member ID" FieldName="memberId" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="SID" FieldName="sid" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Trading ID" FieldName="tradingId" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Role(C/H)" FieldName="role" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Contract ID" FieldName="id.series" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Contract Type (O/F)" FieldName="contractType" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Buy/Sell" FieldName="id.buySell" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Price" FieldName="price" VisibleIndex="8">
                    <PropertiesTextEdit  DisplayFormatString="#,##"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn Caption="Quantity" FieldName="quantity" VisibleIndex="9">
                    <PropertiesTextEdit  DisplayFormatString="#,##0"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Premium" FieldName="premium" VisibleIndex="10">
                    <PropertiesTextEdit  DisplayFormatString="#,##0.#0"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>                
                <dx:GridViewDataTextColumn Caption="Fee" FieldName="fee" VisibleIndex="11">
                    <PropertiesTextEdit  DisplayFormatString="#,##0.#0"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Delta Price" VisibleIndex="12" FieldName="deltaPrice">
                    <PropertiesTextEdit  DisplayFormatString="#,##0.#0"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
            </Columns>
            
            <Styles>
                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                </Header>
            </Styles>
            
        </dx:ASPxGridView>
        
    </div>
</asp:Content>

