﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.memberInfoWS;

namespace imq.kpei.derivatif
{
    public partial class giveupform2 : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                FillMember();
                ParseQueryString();
            }
        }

        private void ParseQueryString()
        {
            string guMemberID = Request.QueryString["memberId"].ToString();
            txtGuMemberId.Text = guMemberID;
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (cmbMember.Text.Trim().Length == 0)
                return;
            if (cmbMember.Text.Equals(txtGuMemberId.Text))
                return;
            string strRedirect = String.Format("giveupform3.aspx?stat=0&guMemberId={0}&tuMemberId={1}", txtGuMemberId.Text.Trim(), cmbMember.Text.Trim());
            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("GiveUpPage.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void FillMember()
        {
            MemberInfoWSService mis = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                memberInfo[] members = mis.get();
                foreach (memberInfo member in members)
                {
                    if (!member.memberId.Trim().Equals("kpei"))
                        cmbMember.Items.Add(member.memberId.Trim(), member.memberId.Trim());
                }
            }
            catch (Exception ex)
            {
                mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                mis.Dispose();
            }
        }
    }
}
