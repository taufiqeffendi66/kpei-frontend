﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using Common;

namespace imq.kpei.derivatif
{
    public partial class InquiryTradeView : System.Web.UI.Page
    {
        private string memberId;
        private string contractId;
        private string sid;
        private string contractType;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
              
            
            if (Request.QueryString["memberId"] != null)
                memberId = Request.QueryString["memberId"].ToString().Trim();

            if (Request.QueryString["contractId"] != null)
                contractId = Request.QueryString["contractId"].ToString().Trim();

            if (Request.QueryString["sid"] != null)
                sid = Request.QueryString["sid"].ToString().Trim();

            if (Request.QueryString["contractType"] != null)
                contractType = Request.QueryString["contractType"].ToString().Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            InquiryTradeReport report = new InquiryTradeReport();

            report.memberId = memberId.ToUpper();
            report.contractId = contractId.ToUpper();
            report.sid = sid.ToUpper();
            report.contractType = contractType.ToUpper();

            report.Name = ImqSession.getFormatFileSave("Inquiry Trade Report");
            return report;
        }


    }
}