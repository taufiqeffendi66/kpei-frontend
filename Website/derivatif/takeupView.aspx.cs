﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class takeupView : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private String aMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (Request.QueryString["member"] != null)
                aMember = Request.QueryString["member"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            takeupReport report = new takeupReport() { aUserMember = aUserMember, aMember = aMember, Name = ImqSession.getFormatFileSave("Take Up") };
            return report;
        }
    }
}
