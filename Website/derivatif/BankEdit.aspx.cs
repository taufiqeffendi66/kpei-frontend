﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.bankWS;
using Common.clientAccountWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class bankedit : System.Web.UI.Page
    {
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private bankTmp aBankTemp;
        private const String module = "Bank";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                //btnSave.Visible = false;
                if (Session["stat"] == null)
                {
                    Response.Redirect("BankPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0:
                        lbltitle.Text = "New " + module; //New Direct
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lbltitle.Text = "Edit " + module; //Edit Direct
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lbltitle.Text = "New " + module; //New From Temporary
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3:
                        lbltitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();
                        break;
                    case 4:
                        lbltitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                    default: break;
                }
            }
        }

        private void DisabledControl()
        {
            txtBankName.Enabled = false;
            memoBranch.Enabled = false;
            txtCode.Enabled = false;
            txtContact.Enabled = false;
            txtEmail.Enabled = false;
            txtFax.Enabled = false;
            txtPhone.Enabled = false;
            memoAddress.Enabled = false;
        }

        private void ParseQueryString()
        {
            string strID = Server.UrlDecode(Request.QueryString["code"].ToString().Trim());
            string strBank = Server.UrlDecode(Request.QueryString["bankName"].ToString().Trim());
            string strContact = Server.UrlDecode(Request.QueryString["contact"].ToString().Trim());
            string strBranch = Server.UrlDecode(Request.QueryString["branch"].ToString().Trim());
            string strAddress = Server.UrlDecode(Request.QueryString["address"].ToString().Trim());
            string strPhone = Server.UrlDecode(Request.QueryString["phone"].ToString().Trim());
            string strFax = Server.UrlDecode(Request.QueryString["fax"].ToString().Trim());
            string strEmail = Server.UrlDecode(Request.QueryString["email"].ToString().Trim());
            string strIdx = Server.UrlDecode(Request.QueryString["idx"].ToString().Trim());
            hfBank.Set("code", strID);
            hfBank.Set("idx", strIdx);

            txtCode.Text = strID.Length > 0 ? strID : "";
            txtBankName.Text = strBank.Length > 0 ? strBank : "";
            txtContact.Text = strContact.Length > 0 ? strContact : "";
            memoBranch.Text = strBranch.Length > 0 ? strBranch : "";
            memoAddress.Text = strAddress.Length > 0 ? strAddress : "";
            txtPhone.Text = strPhone.Length > 0 ? strPhone : "";
            txtFax.Text = strFax.Length > 0 ? strFax : "";
            txtEmail.Text = strEmail.Length > 0 ? strEmail : "";
        }

        private void GetTemporary()
        {
            BankWSService bws = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                bankTmp bt = bws.getBankTemp(int.Parse(Request.QueryString["idxTmp"].ToString()));
                txtCode.Text = bt.bankCode;
                txtBankName.Text = bt.bankName;
                memoBranch.Text = bt.branch;
                txtContact.Text = bt.contactPerson;
                txtEmail.Text = bt.email;
                txtFax.Text = bt.fax;
                txtPhone.Text = bt.phone;
                memoAddress.Text = bt.address;
            }
            catch (Exception e)
            {
                bws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                bws.Dispose();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/derivatif/paymentbank.aspx");

            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("BankPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                default: break;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BankWSService bs = null;
            ApprovalService aps = null;

            try
            {
                bs = ImqWS.GetBankWebService();
                aps = ImqWS.GetApprovalWebService();

                addedit = (int)Session["stat"];
                int idRec;

                bankTmp data = new bankTmp() { bankCode = txtCode.Text.Trim(), bankName = txtBankName.Text.Trim(), contactPerson = txtContact.Text.Trim(), branch = memoBranch.Text.Trim(), address = memoAddress.Text.Trim(), phone = txtPhone.Text.Trim(), fax = txtFax.Text.Trim(), email = txtEmail.Text.Trim() };

                idRec = bs.addToTemp(data);
                aBankTemp = data;
                //if (addedit == 0)                    
                //    ret = bs.add(data);                   
                //else
                //    ret = bs.update(hfBank.Get("code").ToString(),data);

                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/BankEdit.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            aBankTemp.idx = idRec;
                            break;

                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfBank.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfBank.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                        default:
                            break;
                    }
                    string retVal = "0";
                    retVal = rblApprovalChanged(dtA, aps, aBankTemp);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/BankPage.aspx");
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Bank", @"../derivatif/BankPage.aspx");
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                if (bs != null)
                    bs.Abort();

                if (bs != null)
                    aps.Abort();
            }
            finally
            {
                if (bs != null)
                    bs.Dispose();

                if (aps != null)
                    aps.Dispose();
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            BankWSService bws = null;
            ClientAccountWSService cws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                bws = ImqWS.GetBankWebService();

                //dtA = aps.Search(row["reffNo"].ToString());
                dtA = aps.Search(Request.QueryString["reffNo"].ToString());
                aBankTemp = bws.getBankTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }

                string ret = String.Empty;
                //string message = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New  {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = bws.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = bws.deleteTempById(aBankTemp.idx);
                                if (!ret.Equals("0"))
                                {
                                    lblError.Text = ret;
                                }
                                ShowMessage(String.Format("Succes New {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes New {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected New {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = bws.updateFromTemp(aBankTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                //lblError.Text = ret;
                                //btnApproval.Visible = false;
                                //btnReject.Visible = false;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ret = bws.deleteTempById(aBankTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        if (cp == 1)
                        {
                            cws = ImqWS.GetClientAccountService();
                            int count = cws.getBankCount(txtCode.Text.Trim());

                            if (count == 0)
                            {
                                ret = bws.deleteById(dtA[0].idTable);
                                if (!ret.Equals("0"))
                                {
                                    dtA[0].approvelStatus = "Failed";
                                    dtA[0].approvelName = aUserLogin;
                                    dtA[0].memberId = aUserMember;
                                    dtA[0].status = "A";
                                    aps.UpdateByApprovel(dtA[0]);
                                    ShowMessage(ret, target);
                                }
                                else
                                {
                                    dtA[0].approvelStatus = "Approved";
                                    dtA[0].approvelName = aUserLogin;
                                    dtA[0].memberId = aUserMember;
                                    dtA[0].status = "A";
                                    aps.UpdateByApprovel(dtA[0]);
                                    ret = bws.deleteTempById(aBankTemp.idx);
                                    if (!ret.Equals("0"))
                                        lblError.Text = ret;
                                    ShowMessage(String.Format("Succes Delete {0} as Approval", module), target);
                                }
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                //lblError.Text = "The Bank is still in use by Client Account / Member Account";
                                ShowMessage(String.Format("Bank {0} still in use by Client Account / Member Account", aBankTemp.bankName), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Delete {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Delete {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();

                if (bws != null)
                    bws.Abort();

                if (cws != null)
                    cws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
                if (bws != null)
                    bws.Dispose();
                if (cws != null)
                    cws.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aBankTemp));
        }

        private String Log(bankTmp bt)
        {
            string strLog = String.Empty;
            strLog = String.Format("[Code : {0}, Name : {1}, Address : {2}, Contact : {3}]", bt.bankCode, bt.bankName, bt.address, bt.contactPerson);
            return strLog;
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, bankTmp bt)
        {
            string ret = "0";
            string target = @"../derivatif/BankPage.aspx";

            if (!bankTmp.ReferenceEquals(aBankTemp, bt))
                aBankTemp = bt;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    dtA.checkerStatus = "To Be Check";
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:
                    BankWSService bws = ImqWS.GetBankWebService();

                    Common.bankWS.bank bk = new Common.bankWS.bank() { bankCode = aBankTemp.bankCode, address = aBankTemp.address, branch = aBankTemp.branch, contactPerson = aBankTemp.contactPerson, email = aBankTemp.email, fax = aBankTemp.fax, name = aBankTemp.bankName, phone = aBankTemp.phone };
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            ret = bws.add(bk);
                            if (ret.Equals("0"))
                            {
                                wsA.AddDirectApproval(dtA);
                                WriteAuditTrail("Direct Approval New " + module);
                                ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                        case "E":
                            ret = bws.update(hfBank.Get("code").ToString(), bk);
                            if (ret.Equals("0"))
                            {
                                WriteAuditTrail("Direct Approval Edit " + module);
                                wsA.AddDirectApproval(dtA);
                                ShowMessage(String.Format("Succes Edit {0} as Direct Approval", module), target);
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;

                        case "R":
                            ClientAccountWSService cws = ImqWS.GetClientAccountService();
                            int count = cws.getBankCount(txtCode.Text.Trim());

                            if (count == 0)
                            {
                                ret = bws.delete(hfBank.Get("code").ToString());
                                if (ret.Equals("0"))
                                {
                                    WriteAuditTrail("Direct Approval Delete " + module);
                                    wsA.AddDirectApproval(dtA);
                                    ShowMessage(String.Format("Succes Delete {0} as Direct Approval", module), target);
                                }
                                else
                                {
                                    dtA.approvelStatus = "Failed";
                                    wsA.AddDirectApproval(dtA);
                                }
                            }
                            else
                            {
                                ShowMessage(String.Format("Bank {0} is still in use by Client Account / Member Account", hfBank.Get("code")), target);
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }
                    break;
            }
            return ret;
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;
            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;

                        //if (dtFMP.editMaker) rblApproval.Items.Add("Maker", 0);
                        //if (dtFMP.editDirectChecker) rblApproval.Items.Add("Direct Checker", 1);
                        //if (dtFMP.editDirectApproval) rblApproval.Items.Add("Direct Approval", 2);
                        //if (rblApproval.Items.Count > 0)
                        //    rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"].ToString() == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        protected void rblApproval_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
    }
}