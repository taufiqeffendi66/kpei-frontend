﻿namespace imq.kpei.derivatif
{
    partial class BankReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellName = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellContact = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellBranch = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAddress = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellPhone = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellFax = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellCodeHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellNameHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellBranchHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellContactHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAddressHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellPhoneHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellFaxHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellEmailHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xtraReport1 = new DevExpress.XtraReports.UI.XtraReport();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableDetail});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableDetail
            // 
            this.tableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableDetail.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableDetail.Name = "tableDetail";
            this.tableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tableDetail.SizeF = new System.Drawing.SizeF(776F, 31.25F);
            this.tableDetail.StylePriority.UseBorders = false;
            this.tableDetail.StylePriority.UseFont = false;
            this.tableDetail.StylePriority.UseTextAlignment = false;
            this.tableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellCode,
            this.cellName,
            this.cellContact,
            this.cellBranch,
            this.cellAddress,
            this.cellPhone,
            this.cellFax,
            this.cellEmail});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellCode
            // 
            this.cellCode.Name = "cellCode";
            this.cellCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellCode.StylePriority.UsePadding = false;
            this.cellCode.Weight = 0.27006145928138975D;
            // 
            // cellName
            // 
            this.cellName.Name = "cellName";
            this.cellName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellName.StylePriority.UsePadding = false;
            this.cellName.Weight = 0.49126571399754115D;
            // 
            // cellContact
            // 
            this.cellContact.Multiline = true;
            this.cellContact.Name = "cellContact";
            this.cellContact.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellContact.StylePriority.UsePadding = false;
            this.cellContact.Weight = 0.39422404438632375D;
            // 
            // cellBranch
            // 
            this.cellBranch.Name = "cellBranch";
            this.cellBranch.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellBranch.StylePriority.UsePadding = false;
            this.cellBranch.Weight = 0.398384748000111D;
            // 
            // cellAddress
            // 
            this.cellAddress.Multiline = true;
            this.cellAddress.Name = "cellAddress";
            this.cellAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellAddress.StylePriority.UsePadding = false;
            this.cellAddress.Weight = 0.41140957130803812D;
            // 
            // cellPhone
            // 
            this.cellPhone.Name = "cellPhone";
            this.cellPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellPhone.StylePriority.UsePadding = false;
            this.cellPhone.Weight = 0.33771799118865381D;
            // 
            // cellFax
            // 
            this.cellFax.Name = "cellFax";
            this.cellFax.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellFax.StylePriority.UsePadding = false;
            this.cellFax.Weight = 0.33604927854498562D;
            // 
            // cellEmail
            // 
            this.cellEmail.Name = "cellEmail";
            this.cellEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellEmail.StylePriority.UsePadding = false;
            this.cellEmail.Weight = 0.3608871932929566D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 53.95834F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(625F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 31.25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.tblHeader.SizeF = new System.Drawing.SizeF(776F, 31.25F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellCodeHeader,
            this.cellNameHeader,
            this.cellBranchHeader,
            this.cellContactHeader,
            this.cellAddressHeader,
            this.cellPhoneHeader,
            this.cellFaxHeader,
            this.cellEmailHeader});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // cellCodeHeader
            // 
            this.cellCodeHeader.Name = "cellCodeHeader";
            this.cellCodeHeader.Text = "Bank Code";
            this.cellCodeHeader.Weight = 0.27006142978630782D;
            // 
            // cellNameHeader
            // 
            this.cellNameHeader.Name = "cellNameHeader";
            this.cellNameHeader.Text = "Bank Name";
            this.cellNameHeader.Weight = 0.49126589096803291D;
            // 
            // cellBranchHeader
            // 
            this.cellBranchHeader.Name = "cellBranchHeader";
            this.cellBranchHeader.Text = "Contact Person";
            this.cellBranchHeader.Weight = 0.394223896910914D;
            // 
            // cellContactHeader
            // 
            this.cellContactHeader.Name = "cellContactHeader";
            this.cellContactHeader.Text = "Branch";
            this.cellContactHeader.Weight = 0.398384748000111D;
            // 
            // cellAddressHeader
            // 
            this.cellAddressHeader.Name = "cellAddressHeader";
            this.cellAddressHeader.Text = "Address";
            this.cellAddressHeader.Weight = 0.41140957130803812D;
            // 
            // cellPhoneHeader
            // 
            this.cellPhoneHeader.Name = "cellPhoneHeader";
            this.cellPhoneHeader.Text = "Phone";
            this.cellPhoneHeader.Weight = 0.33771799118865381D;
            // 
            // cellFaxHeader
            // 
            this.cellFaxHeader.Name = "cellFaxHeader";
            this.cellFaxHeader.Text = "Fax";
            this.cellFaxHeader.Weight = 0.33604951450564119D;
            // 
            // cellEmailHeader
            // 
            this.cellEmailHeader.Name = "cellEmailHeader";
            this.cellEmailHeader.Text = "Email";
            this.cellEmailHeader.Weight = 0.36088695733230103D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xtraReport1
            // 
            this.xtraReport1.Name = "xtraReport1";
            this.xtraReport1.PageHeight = 1100;
            this.xtraReport1.PageWidth = 850;
            this.xtraReport1.Version = "12.2";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrLine1,
            this.lbTitle});
            this.ReportHeader.HeightF = 59.375F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.Black;
            this.xrPageInfo2.Format = "{0:\"Current Date: \" dddd, dd MMMM yyyy}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(484F, 26.37501F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(292F, 23F);
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(775.9999F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle
            // 
            this.lbTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lbTitle.ForeColor = System.Drawing.Color.Black;
            this.lbTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle.SizeF = new System.Drawing.SizeF(196.93F, 38F);
            this.lbTitle.StylePriority.UseForeColor = false;
            this.lbTitle.Text = "Payment Banks";
            this.lbTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // BankReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(40, 34, 54, 51);
            this.Version = "12.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.BankReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell cellCodeHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellNameHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellBranchHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellContactHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellAddressHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellPhoneHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellFaxHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellEmailHeader;
        private DevExpress.XtraReports.UI.XRTable tableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellCode;
        private DevExpress.XtraReports.UI.XRTableCell cellName;
        private DevExpress.XtraReports.UI.XRTableCell cellContact;
        private DevExpress.XtraReports.UI.XRTableCell cellBranch;
        private DevExpress.XtraReports.UI.XRTableCell cellAddress;
        private DevExpress.XtraReports.UI.XRTableCell cellPhone;
        private DevExpress.XtraReports.UI.XRTableCell cellFax;
        private DevExpress.XtraReports.UI.XRTableCell cellEmail;
        private DevExpress.XtraReports.UI.XtraReport xtraReport1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
    }
}
