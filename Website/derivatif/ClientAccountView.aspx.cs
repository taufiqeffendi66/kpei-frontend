﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.UI;
using Common;

namespace imq.kpei.derivatif
{
    public partial class ClientAccountView : System.Web.UI.Page
    {
        private String aBank;
        private String aMember;
        private String aSid;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (Request.QueryString["bank"] != null)
                aBank = Request.QueryString["bank"].ToString().Trim();
            if (Request.QueryString["member"] != null)
                aMember = Request.QueryString["member"].ToString().Trim();
            if (Request.QueryString["sid"] != null)
                aSid = Request.QueryString["sid"].ToString().Trim();
            ReportViewer1.Report = CreateReport();

        }

        XtraReport CreateReport()
        {
            ClientAccountReport report = new ClientAccountReport() { aBank = aBank, aMember = aMember, aSID = aSid, Name = ImqSession.getFormatFileSave("ClientAccount") };
            return report;
        }
    }
}
