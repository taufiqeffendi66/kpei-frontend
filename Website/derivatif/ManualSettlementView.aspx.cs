﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class manualsettlementView : System.Web.UI.Page
    {
        private String aBank;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["bank"] != null)
                aBank = Request.QueryString["bank"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            ManualSettlementReport report = new ManualSettlementReport() { aBank = aBank };
            return report;
        }
    }
}