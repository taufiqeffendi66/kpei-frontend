﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class withdrawalView : System.Web.UI.Page
    {
        private string aUserMember = string.Empty;
        private string aMember = string.Empty;
        private string aSID = string.Empty;
        private string aStat = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserMember = Session["SESSION_USERMEMBER"].ToString().ToUpper();
            if (Request.QueryString["member"] != null)
            {
                if (aMember.Equals("undefined"))
                    aMember = "";
                else
                {
                    if (aUserMember.Contains("KPEI"))
                        aMember = Request.QueryString["member"].Trim();
                    else
                        aMember = aUserMember;
                }
            }
            else
                aMember = "";

            if (Request.QueryString["sid"] != null)
                aSID = Request.QueryString["sid"].Trim();

            if (Request.QueryString["status"] != null)
                aStat = Request.QueryString["status"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            withdrawalReport report = new withdrawalReport() { aUserMember = aUserMember, aMember = aMember, aSID = aSID, aStat = aStat, Name = ImqSession.getFormatFileSave("Balance Withdrawal") };
            return report;
        }
    }
}
