﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Diagnostics;
using DevExpress.Web.ASPxPopupControl;
using Common;
using Common.wsAuditTrail;
using Common.wsApproval;
using Common.wsUserGroupPermission;
using Common.wsManualSettlement;
using Common.bankWS;
using Common.clientAccountWS;
using Common.memberInfoWS;
using DevExpress.Web.ASPxEditors;
using Common.kpeiAccountWS;


namespace imq.kpei.derivatif
{
    public partial class manualsettlementedit : System.Web.UI.Page
    {
       
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private manualSettlementTmp aManualSettlementTemp;
        private String target = @"../derivatif/ManualSettlementPage.aspx";
        private String targetApproval = @"../administration/approval.aspx";

        private String module = "Manual Settlement";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];

            if (!IsPostBack)
            {
                getBank();
                getMember();
                getType();

                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                //btnSave.Visible = false;

                if (Session["stat"] == null)
                {
                    Response.Redirect("ManualSettlementPage.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                //cbType.Enabled = false;
                cbBank.Enabled = false;
                cbMember.Enabled = false;
                cbSource.Enabled = false;
                cbTarget.Enabled = false;

                switch (addedit)
                {
                    case 0: lbltitle.Text = "New " + module; //New Direct
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1: lbltitle.Text = "Edit " + module; //Edit Direct
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2: lbltitle.Text = "New " + module; //New From Temporary
                        Permission(false);

                        GetTemporary();
                        DisabledControl();

                        break;
                    case 3: lbltitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        DisabledControl();

                        break;
                    case 4: lbltitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                }

            }
            else
            {

                if (cbMember.Text != "")
                {
                    
                }
            }
        }


        private void getMember()
        {
            ManualSettlementWSService ms = null;
            try
            {
                ms = ImqWS.GetManualSettlementService();
                memberAccount[] memberInfos;
                memberInfos = ms.getMemberId();
                if (memberInfos.Length > 0)
                {
                    foreach (memberAccount m in memberInfos)
                    {
                        if (!IsPostBack)
                        {
                            cbMember.Items.Add(m.memberId, m.memberId);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ms.Abort();
                Debug.WriteLine(e.ToString());
            }
            finally
            {
                ms.Dispose();
            }
        }

        private void getType()
        {
            ManualSettlementWSService bs = null;
            manualSettlementType[] manualSettlementTypes;
            try
            {
                bs = ImqWS.GetManualSettlementService();
                manualSettlementTypes = bs.getType();
                
                if (!IsPostBack)
                {
                    cbType.Items.Clear();
                    foreach (manualSettlementType b in manualSettlementTypes)
                    {
                        cbType.Items.Add(b.text + " - " + b.type, b.type);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                bs.Abort();
            }
            finally
            {
                bs.Dispose();
            }
        }

        private void getBank()
        {
            BankWSService bs = null;
            Common.bankWS.bank[] banks;
            try
            {
                bs = ImqWS.GetBankWebService();
                banks = bs.get();
                if (!IsPostBack)
                {
                    cbBank.Items.Clear();
                    foreach (Common.bankWS.bank b in banks)
                    {
                        cbBank.Items.Add(b.bankCode, b.bankCode);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                bs.Abort();
            }
            finally
            {
                bs.Dispose();
            }
        }


        private void DisabledControl()
        {
            cbType.Enabled = false;
            cbMember.Enabled = false;
            cbBank.Enabled = false;
            cbSource.Enabled = false;
            cbTarget.Enabled = false;
            txtValue.Enabled = false;
        }

        private void ParseQueryString()
        {
            string strID = Server.UrlDecode(Request.QueryString["code"].ToString().Trim());
            string strBank = Server.UrlDecode(Request.QueryString["bankName"].ToString().Trim());
            string strContact = Server.UrlDecode(Request.QueryString["contact"].ToString().Trim());
            string strBranch = Server.UrlDecode(Request.QueryString["branch"].ToString().Trim());
            string strAddress = Server.UrlDecode(Request.QueryString["address"].ToString().Trim());
            string strPhone = Server.UrlDecode(Request.QueryString["phone"].ToString().Trim());
            string strFax = Server.UrlDecode(Request.QueryString["fax"].ToString().Trim());
            string strEmail = Server.UrlDecode(Request.QueryString["email"].ToString().Trim());
            string strIdx = Server.UrlDecode(Request.QueryString["idx"].ToString().Trim());
            hfManualSettlement.Set("code", strID);
            hfManualSettlement.Set("idx", strIdx);

            cbBank.Text = strID.Length > 0 ? strID : "";
            cbSource.Text = strBank.Length > 0 ? strBank : "";
            cbTarget.Text = strContact.Length > 0 ? strContact : "";
            txtValue.Text = strPhone.Length > 0 ? strPhone : "";
                 
        }

        private void GetTemporary()
        {
            ManualSettlementWSService mws = null;
            try
            {
                mws = ImqWS.GetManualSettlementService();
                manualSettlementTmp mt = mws.searchTemp(int.Parse(Request.QueryString["idxTmp"].ToString()));
                string Text="";
                switch (mt.type)
                {
                    case "DCCL" : Text="AK(C) to AK(H) - DCCL"; break;
                    case "FCCL" : Text="AK(C) to AK(H) - FCCL"; break;
                    case "RCCL" : Text="AK(H) to AK(C) - RCCL"; break;
                    case "RCCM" : Text="AK(H) to AK(H) - RCCM"; break;
                    case "DSSD" : Text="AK(H) to AK(H) - DSSD"; break;
                    case "SDCM" : Text="AK(H) to AK(H) - SDCM"; break;
                    case "DCCM" : Text="AK(H) to AK(H) - DCCM"; break;
                    case "FCCM" : Text="AK(H) to AK(H) - FCCM"; break;
                    case "COLM" : Text="AK(H) to AK(H) - COLM"; break;
                    case "DSCM" : Text="AK(H) to KPEI - DSCM"; break;
                    case "GFCM" : Text="KPEI to KPEI - GFCM"; break;
                    case "PGCM" : Text="KPEI to KPEI - PGCM"; break;
                    case "IGCM" : Text="KPEI to KPEI - IGCM"; break;
                    case "GFFK" : Text="KPEI to KPEI - GFFK"; break;
                    case "CLFK" : Text="KPEI to KPEI - CLFK"; break;
                    case "RSCM": Text = "KPEI to AK(H) - RSCM"; break;
                    case "SLCL": Text = "AK(H) to AK(C) - SLCL"; break;
                    case "SLCM": Text = "AK(H) to AK(H) - SLCM"; break;
                }
                cbType.Text = Text;
                cbMember.Text = mt.memberId;
                cbBank.Text = mt.bank;
                cbSource.Text = mt.source;
                cbTarget.Text = mt.target;
                txtValue.Text = mt.value.ToString();
            }
            catch (Exception e)
            {
                mws.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                mws.Dispose();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4: Response.Redirect("ManualSettlementPage.aspx", false);
                        Context.ApplicationInstance.CompleteRequest(); break;
                case 2:
                case 3: Response.Redirect("~/administration/approval.aspx", false);
                        Context.ApplicationInstance.CompleteRequest(); 
                        break;
            }
           
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            /*
            if(){}
            else if(){}
            else if(){}
            else if(){}
            */

            ManualSettlementWSService ms = null;
            ApprovalService aps = null;

            try
            {
                ms = ImqWS.GetManualSettlementService();
                aps = ImqWS.GetApprovalWebService();

                addedit = (int)Session["stat"];
                int idRec;

                manualSettlementTmp data = new manualSettlementTmp();
                data.type = cbType.Value.ToString();
                data.memberId = cbMember.Text;
                data.bank = cbBank.Text.Trim();
                data.createDate = DateTime.Now;
                string[] words = cbSource.Value.ToString().Trim().Split('|');
                data.sourceTradingId = words[1];
                data.source = words[0];
                words = cbTarget.Value.ToString().Trim().Split('|');
                data.target = words[0];
                data.targetTradingId = words[1];
                data.value = System.Convert.ToDecimal(txtValue.Text.Trim());
                data.valueSpecified = true;
                idRec = ms.addToTemp(data);
                aManualSettlementTemp = data; 
                if (idRec > -1) aManualSettlementTemp = ms.searchTemp(idRec);


                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval();
                    dtA.type = "c";
                    dtA.makerDate = DateTime.Now;
                    dtA.makerName = aUserLogin;
                    dtA.makerStatus = "Maked";
                    dtA.checkerStatus = "To Be Check";
                    dtA.idxTmp = idRec;
                    dtA.form = "~/derivatif/ManualSettlementEdit.aspx";
                    dtA.status = "M";
                    dtA.memberId = aUserMember;
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            aManualSettlementTemp.idx = idRec;
                            break;

                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfManualSettlement.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfManualSettlement.Get("idx").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }
                    string retVal = "0";

                    retVal = rblApprovalChanged(dtA, aps, aManualSettlementTemp);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/ManualSettlementPage.aspx");
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Manual Settlement", @"../derivatif/ManualSettlementPage.aspx");
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                if(ms != null)
                    ms.Abort();

                if(ms != null)
                    aps.Abort();
            }
            finally
            {
                if(ms != null)
                    ms.Dispose();

                if(aps != null)
                    aps.Dispose();
            }
            
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            ManualSettlementWSService mws = null;
            ClientAccountWSService cws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                mws = ImqWS.GetManualSettlementService();

                //dtA = aps.Search(row["reffNo"].ToString());
                dtA = aps.Search(Request.QueryString["reffNo"].ToString());
                aManualSettlementTemp = mws.searchTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                    
                }
                else if (cp == 1)
                {
                    activity = "Approval";
                    

                }
                else
                {
                    if (btnChecker.Visible == true)
                    {
                        activity = "Reject Checker";
                        dtA[0].checkerName = aUserLogin;
                        dtA[0].checkerStatus = "Rejected";
                        dtA[0].approvelStatus = "";
                        dtA[0].memberId = aUserMember;
                        dtA[0].status = "RC";
                        aps.UpdateByChecker(dtA[0]);
                    }
                    else
                    {
                        activity = "Reject Approval";
                        dtA[0].approvelName = aUserLogin;
                        dtA[0].approvelStatus = "Rejected";
                        dtA[0].memberId = aUserMember;
                        dtA[0].status = "RA";
                        aps.UpdateByApprovel(dtA[0]);
                    }
                }                

                string ret = String.Empty;
                //string message = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I": WriteAuditTrail(activity + " New  " + module);
                        if (cp == 1)
                        {
                           
                            ret = mws.addFromTemp(dtA[0].idxTmp);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, targetApproval);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
/*
                                ret = mws.deleteTempById(aManualSettlementTemp.idx);
                                if (!ret.Equals("0"))
                                {
                                    lblError.Text = ret;
                                }
 */ 
                                ShowMessage("Succes New " + module + " as Approval", target);
                            }
                            
                        }
                        else if (cp == 0)
                        {
                            ShowMessage("Succes New " + module + " as Checker", targetApproval);                           
                        }
                        else
                        {
                            if (btnChecker.Visible == true)
                            {
                                ShowMessage("Rejected New " + module + " as Checker", targetApproval);                                
                            }
                            else
                            {
                                ShowMessage("Rejected New " + module + " as Approval", targetApproval); 
                            }
                            
                        }
                                break;
                    case "E": WriteAuditTrail(activity + " Edit " + module);
                                if (cp == 1)
                                {
                                    ret = mws.updateFromTemp(aManualSettlementTemp, dtA[0].idTable);
                                    if (!ret.Equals("0"))
                                    {
                                        //lblError.Text = ret;
                                        //btnApproval.Visible = false;
                                        //btnReject.Visible = false;
                                        dtA[0].approvelStatus = "Failed";
                                        dtA[0].approvelName = aUserLogin;
                                        dtA[0].memberId = aUserMember;
                                        dtA[0].status = "A";
                                        aps.UpdateByApprovel(dtA[0]);
                                        ShowMessage(ret, targetApproval);
                                        
                                    }
                                    /*
                                    else
                                    {
                                        dtA[0].approvelStatus = "Approved";
                                        dtA[0].approvelName = aUserLogin;
                                        dtA[0].memberId = aUserMember;
                                        dtA[0].status = "A";
                                        aps.UpdateByApprovel(dtA[0]);
                                        ret = mws.deleteTempById(aManualSettlementTemp.idx);
                                        if (!ret.Equals("0"))
                                            lblError.Text = ret;

                                        ShowMessage("Succes Edit " + module + " as Approval", target);                                        
                                    }
                                    */ 
                                    
                                }
                                else if (cp == 0)
                                {
                                    ShowMessage("Succes Edit " + module + " as Checker", targetApproval);                                   
                                }
                                else
                                {
                                    if (btnChecker.Visible == true)
                                    {
                                        ShowMessage("Rejected Edit " + module + " as Checker", targetApproval);
                                    }
                                    else
                                    {
                                        ShowMessage("Rejected Edit " + module + " as Approval", targetApproval);
                                    }
                                }
                              break;
                    case "R": WriteAuditTrail(activity + " Delete " + module);
                              if (cp == 1)
                              {
                                  cws = ImqWS.GetClientAccountService();
                                  int count = cws.getBankCount(cbBank.Text.Trim());

                                  if (count == 0)
                                  {
                                      ret = mws.deleteById(dtA[0].idTable);
                                      if (!ret.Equals("0"))
                                      {
                                          dtA[0].approvelStatus = "Failed";
                                          dtA[0].approvelName = aUserLogin;
                                          dtA[0].memberId = aUserMember;
                                          dtA[0].status = "A";
                                          aps.UpdateByApprovel(dtA[0]);
                                          ShowMessage(ret, targetApproval);                                          
                                      }
                                      /*
                                      else
                                      {
                                          dtA[0].approvelStatus = "Approved";
                                          dtA[0].approvelName = aUserLogin;
                                          dtA[0].memberId = aUserMember;
                                          dtA[0].status = "A";
                                          aps.UpdateByApprovel(dtA[0]);
                                          ret = mws.deleteTempById(aManualSettlementTemp.idx);
                                          if (!ret.Equals("0"))
                                              lblError.Text = ret;
                                          ShowMessage("Succes Delete " + module + " as Approval", target);                                           
                                      }
                                      */
                                      
                                  }
                                  else
                                  {
                                      dtA[0].approvelStatus = "Failed";
                                      dtA[0].approvelName = aUserLogin;
                                      dtA[0].memberId = aUserMember;
                                      dtA[0].status = "A";
                                      aps.UpdateByApprovel(dtA[0]);
                                      //lblError.Text = "The Bank is still in use by Client Account / Member Account";
                                      ShowMessage("Bank " + aManualSettlementTemp.bank + " still in use by Client Account / Member Account", targetApproval); 
                                    
                                  }
                              }
                              else if (cp == 0)
                              {
                                  ShowMessage("Succes Delete " + module + " as Checker", targetApproval);                                 
                              }
                              else
                              {
                                  if (btnChecker.Visible == true)
                                  {
                                      ShowMessage("Rejected Delete " + module + " as Checker", targetApproval);
                                  }
                                  else
                                  {
                                      ShowMessage("Rejected Delete " + module + " as Approval", targetApproval);
                                  }
                              }
                              break;
                }
                
            }
            catch (Exception ex)
            {
                if(aps != null)
                    aps.Abort();

                if (mws != null)
                    mws.Abort();

                if (cws != null)
                    cws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
                if (mws != null)
                    mws.Dispose();
                if (cws != null)
                    cws.Dispose();
            }

            
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aManualSettlementTemp));
        }

        private String Log(manualSettlementTmp ms)
        {
            string strLog = String.Empty;
            strLog = "[bank : " + ms.bank +
                     ", createDate : " + ms.createDate +
                     ", source : " + ms.source +
                     ", target : " + ms.target +
                     ", value : " + ms.value +
                     ", status : " + ms.status +
                     ", executionDate : " + ms.executionDate +
                     "]";
            return strLog;
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, manualSettlementTmp mst)
        {
            string ret = "0";

            if (!manualSettlementTmp.ReferenceEquals(aManualSettlementTemp, mst))
                aManualSettlementTemp = mst;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                    //Maker
                case 0: 
                        wsA.AddMaker(dtA);
                        dtA.checkerStatus = "To Be Check";
                        switch (dtA.insertEdit)
                        {
                            case "I":   WriteAuditTrail("New " + module);
                                        ShowMessage("Succes New " + module + " as Maker", target);                                        
                                        break;
                            case "E":   WriteAuditTrail("Edit " + module);
                                        ShowMessage("Succes Edit " + module + " as Maker", target);
                                        break;
                            case "R":   WriteAuditTrail("Delete " + module);
                                        ShowMessage("Succes Delete " + module + " as Maker", target);
                                        break;
                        }
                        break;
                case 1: 
                        wsA.AddDirectChecker(dtA);
                        switch (dtA.insertEdit)
                        {
                            case "I":   WriteAuditTrail("Direct Checker New " + module);                                        
                                        ShowMessage("Succes New " + module + " as Direct Checker", target);
                                        break;
                            case "E":   WriteAuditTrail("Direct Checker Edit " + module);                                        
                                        ShowMessage("Succes Edit " + module + " as Direct Checker", target);
                                        break;
                            case "R":   WriteAuditTrail("Direct Checker Delete " + module);                                        
                                        ShowMessage("Succes Delete " + module + " as Direct Checker", target);
                                        break;
                        }
                        break;
                case 2: 
                        ManualSettlementWSService mws = ImqWS.GetManualSettlementService();

                        Common.wsManualSettlement.manualSettlement ms = new Common.wsManualSettlement.manualSettlement();
                        Common.wsManualSettlement.manualSettlementPK msPK = new Common.wsManualSettlement.manualSettlementPK();

                        msPK.type = aManualSettlementTemp.type;
                        msPK.memberId = aManualSettlementTemp.memberId;
                        msPK.createDate = aManualSettlementTemp.createDate;
                        msPK.createDateSpecified = true;
                        msPK.source = aManualSettlementTemp.source;
                        msPK.target = aManualSettlementTemp.target;

                        ms.id               = msPK;
                        ms.bank             = aManualSettlementTemp.bank;
                        ms.sourceTradingId  = aManualSettlementTemp.sourceTradingId;
                        ms.targetTradingId  = aManualSettlementTemp.targetTradingId;
                        ms.value            = aManualSettlementTemp.value;
                        ms.valueSpecified   = true;
                        ms.status           = aManualSettlementTemp.status;
                        ms.executionDate    = aManualSettlementTemp.executionDate;
                        
                        switch (dtA.insertEdit)
                        {
                            case "I": ret = mws.add(ms);
                                        if (ret.Equals("0"))
                                        {
                                            wsA.AddDirectApproval(dtA);
                                            WriteAuditTrail("Direct Approval New " + module);
                                            ShowMessage("Succes New " + module + " as Direct Approval", target);
                                        }
                                        else
                                        {
                                            dtA.approvelStatus = "Failed";
                                            wsA.AddDirectApproval(dtA);
                                        }
                                        break;
                            case "E": ret = mws.update(hfManualSettlement.Get("code").ToString(), ms);
                                        if (ret.Equals("0"))
                                        {
                                            WriteAuditTrail("Direct Approval Edit " + module);
                                            wsA.AddDirectApproval(dtA);
                                            ShowMessage("Succes Edit " + module + " as Direct Approval", target);                                            
                                        }
                                        else
                                        {
                                            dtA.approvelStatus = "Failed";
                                            wsA.AddDirectApproval(dtA);
                                        }
                                        break;

                            case "R": ClientAccountWSService cws = ImqWS.GetClientAccountService();
                                        int count = cws.getBankCount(cbBank.Text.Trim());
                                /*
                                  if (count == 0)
                                  {

                                      ret = mws.delete(hfBank.Get("code").ToString());
                                      if (ret.Equals("0"))
                                      {
                                          WriteAuditTrail("Direct Approval Delete " + module);
                                          wsA.AddDirectApproval(dtA);
                                          ShowMessage("Succes Delete " + module + " as Direct Approval", target);
                                      }
                                      else
                                      {
                                          dtA.approvelStatus = "Failed";
                                          wsA.AddDirectApproval(dtA);
                                      }
                                  }
                                  else
                                  {
                                        ShowMessage("Bank " + hfBank.Get("code").ToString() + " is still in use by Client Account / Member Account", target);                                     
                                        dtA.approvelStatus = "Failed";
                                        wsA.AddDirectApproval(dtA);
                                  }
                                 * */
                                  break;
                        }
                        
                        break;
            }
            return ret;
        }
        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                alert('" + info + @"');
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;
            
            try
            {

                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach(dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker) rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker) rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval) rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;

                        //if (dtFMP.editMaker) rblApproval.Items.Add("Maker", 0);
                        //if (dtFMP.editDirectChecker) rblApproval.Items.Add("Direct Checker", 1);
                        //if (dtFMP.editDirectApproval) rblApproval.Items.Add("Direct Approval", 2);
                        //if (rblApproval.Items.Count > 0)
                        //    rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                            {
                                btnChecker.Visible = true;
                                btnReject.Visible = true;
                            }
                        else if (!che && mem && (Request.QueryString["status"].ToString() == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }            
        }

        private listAccount[] getClientAccouunt(string memberId)
        {
            listAccount[] list = null;
            ManualSettlementWSService mss = null;

            try
            {
                mss = ImqWS.GetManualSettlementService();
                list = mss.getClientAccount(memberId);

            }
            catch (Exception e)
            {
                mss.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                mss.Dispose();
            }
            return list;
        }

        private listAccount[] getMemberAccouunt(string memberId)
        {
            listAccount[] list = null;
            ManualSettlementWSService mss = null;

            try
            {
                mss = ImqWS.GetManualSettlementService();
                list = mss.getMemberAccount(memberId);

            }
            catch (Exception e)
            {
                mss.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                mss.Dispose();
            }
            return list;
        }

        private listAccount[] getAllAccouunt(string memberId)
        {
            listAccount[] list = null;
            ManualSettlementWSService mss = null;

            try
            {
                mss = ImqWS.GetManualSettlementService();
                list = mss.getAccount(memberId);

            }
            catch (Exception e)
            {
                mss.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                mss.Dispose();
            }
            return list;
        }

        private kpeiAccount[] getKpeiAccount()
        {
            KpeiAccountWSService kas = null;
            kpeiAccount[] list = null;
            try
            {
                kas = ImqWS.GetKpeiAccountService();
                list = kas.get();
            }
            catch (Exception e)
            {
                kas.Abort();
                Debug.WriteLine(e.Message);
                lblError.Text = e.Message;
            }
            finally
            {
                kas.Dispose();
            }
            return list;
        }

        // Type
        // 0 = Guarantee Fund 
        // 1 = Penalty
        // 2 = Settlement
        // 3 = Fee
        //
        private void pushKPEI(kpeiAccount[] list, int type, ASPxComboBox cb)
        {
            cb.Enabled = true;
            cb.Items.Clear();
            cb.Text = "";
            foreach (kpeiAccount ca in list)
            {
                if (ca.bankCode == cbBank.Text)
                {
                    switch(type) {
                        case -1 :
                            cb.Items.Add("Guarantee Fund - " + ca.guaranteeFundAccountNo, ca.guaranteeFundAccountNo+"|kpei");
                            cb.Items.Add("Penalty - " + ca.penaltyAccountNo, ca.penaltyAccountNo + "|kpei");
                            cb.Items.Add("Settlement - " + ca.settlementAccountNo, ca.settlementAccountNo + "|kpei");
                            cb.Items.Add("Fee - " + ca.feeAccountNo, ca.feeAccountNo + "|kpei");
                        break;
                        case 0:
                        cb.Items.Add("Guarantee Fund - " + ca.guaranteeFundAccountNo, ca.guaranteeFundAccountNo + "|kpei");
                        break;
                        case 1:
                        cb.Items.Add("Penalty - " + ca.penaltyAccountNo, ca.penaltyAccountNo + "|kpei");
                        break;
                        case 2:
                        cb.Items.Add("Settlement - " + ca.settlementAccountNo, ca.settlementAccountNo + "|kpei");
                        break;
                        case 3:
                        cb.Items.Add("Fee - " + ca.feeAccountNo, ca.feeAccountNo + "|kpei");
                        break;
                    }
                }
            }
        }

        // Type
        // 0 = Collateral
        // 1 = Security Deposit
        // 2 = Settlement
        // 3 = Free
        //
        private void pushClient(listAccount[] list, int type, ASPxComboBox cb)
        {
            cb.Enabled = true;
            cb.Items.Clear();
            cb.Text = "";
            if(list!= null)
                foreach (listAccount ca in list)
                {
                    if (ca.accountNo != "0")
                    {
                        if (type == -1)
                            cb.Items.Add(ca.accountName + " - " + ca.accountNo, ca.accountNo + "|" + ca.tradingId);
                        else if (type == ca.accountType)
                            cb.Items.Add(ca.accountName + " - " + ca.accountNo, ca.accountNo + "|" + ca.tradingId);
                    }
                }
        }

        protected void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbType.Value.ToString())
            {
                case "RCCL": //AK(H) to AK(C) - RCCL
                case "SLCL": //AK(H) to AK(C) - SLCL
                case "DCCL": //AK(C) to AK(H) - DCCL
                case "FCCL": //AK(C) to AK(H) - FCCL
                case "DSSD": //AK(H) to AK(H) - DSSD
                case "SDCM": //AK(H) to AK(H) - SDCM
                case "DCCM": //AK(H) to AK(H) - DCCM
                case "FCCM": //AK(H) to AK(H) - FCCM
                case "COLM": //AK(H) to AK(H) - COLM
                case "RCCM": //AK(H) to AK(H) - RCCM                    
                case "SLCM": //AK(H) to AK(H) - SLCM                    
                case "DSCM": //AK(H) to KPEI - DSCM
                case "RSCM": //KPEI to AK(H) - RSCM
                    cbBank.Text = "";
                    cbBank.Enabled = false;
                    cbMember.Text = "";
                    cbMember.Enabled = true;
                    break;
                case "GFCM": //KPEI to KPEI - GFCM
                case "PGCM": //KPEI to KPEI - PGCM
                case "IGCM": //KPEI to KPEI - IGCM
                case "GFFK": //KPEI to KPEI - GFFK
                case "CLFK": //KPEI to KPEI - CLFK
                    cbBank.Text = "";
                    cbBank.Enabled = true;
                    cbMember.Text = "";
                    cbMember.Enabled = false;
                    break;
            }


        }

        protected void cbMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbMember.Text != "")
            {

                listAccount[] list;
                kpeiAccount[] listz;
                switch (cbType.Value.ToString())
                {
                    case "RCCL": //AK(H) to AK(C) - RCCL
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 2, cbSource);
                        list = getClientAccouunt(cbMember.Text);
                        pushClient(list, -1, cbTarget);
                        break;
                    case "SLCL": //AK(H) to AK(C) - SLCL
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 2, cbSource);
                        list = getClientAccouunt(cbMember.Text);
                        pushClient(list, -1, cbTarget);
                        break;
                    case "DCCL": //AK(C) to AK(H) - DCCL
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 2, cbTarget);
                        cbBank.Text = list[1].bank;
                        list = getClientAccouunt(cbMember.Text);
                        pushClient(list, -1, cbSource);
                        break;
                    case "FCCL": //AK(C) to AK(H) - FCCL
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 3, cbTarget);
                        cbBank.Text = list[1].bank;
                        list = getClientAccouunt(cbMember.Text);
                        pushClient(list, -1, cbSource);
                        break;
                    case "DSSD": //AK(H) to AK(H) - DSSD
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 1, cbSource);
                        pushClient(list, 0, cbTarget);
                        break;
                    case "SDCM": //AK(H) to AK(H) - SDCM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 1, cbSource);
                        pushClient(list, 3, cbTarget);
                        break;
                    case "DCCM": //AK(H) to AK(H) - DCCM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 0, cbSource);
                        pushClient(list, 2, cbTarget);
                        break;
                    case "FCCM": //AK(H) to AK(H) - FCCM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 0, cbSource);
                        pushClient(list, 3, cbTarget);
                        break;
                    case "COLM": //AK(H) to AK(H) - COLM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 0, cbSource);
                        pushClient(list, 1, cbTarget);
                        break;
                    case "DSCM": //AK(H) to KPEI - DSCM
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 2, cbSource);
                        cbBank.Text = list[1].bank;
                        listz = getKpeiAccount();
                        pushKPEI(listz, 2, cbTarget);
                        break;
                    case "GFCM": //KPEI to KPEI - GFCM
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 0, cbSource);
                        pushKPEI(listz, 2, cbTarget);
                        break;
                    case "PGCM": //KPEI to KPEI - PGCM
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 1, cbTarget);
                        break;
                    case "IGCM": //KPEI to KPEI - IGCM
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 0, cbTarget);
                        break;
                    case "GFFK": //KPEI to KPEI - GFFK
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 0, cbTarget);
                        break;
                    case "CLFK": //KPEI to KPEI - CLFK
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 3, cbTarget);
                        break;
                    case "RSCM": //KPEI to AK(H) - RSCM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        listz = getKpeiAccount();
                        pushKPEI(listz, 2, cbSource);
                        pushClient(list, 2, cbTarget);
                        break;
                    case "RCCM": //AK(H) to AK(H) - RCCM                    
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 2, cbSource);
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 0, cbTarget);
                        break;
                    case "SLCM": //AK(H) to AK(H) - SLCM                    
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 2, cbSource);
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 0, cbTarget);
                        break;
                }
            }

/*
            if (cbMember.Text != "")
            {
                listAccount[] list;
                kpeiAccount[] listz;
                switch (cbType.Value.ToString())
                {
                    case "0": // AK(H) 2 AK(C)
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, cbSource);
                        list = getClientAccouunt(cbMember.Text);
                        pushClient(list, cbTarget);
                        break;
                    case "1": // AK(C) 2 AK(H)
                        list = getClientAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, cbSource);
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, cbTarget);
                        break;
                    case "2": // AK(H) 2 AK(H)
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, cbSource);
                        pushClient(list, cbTarget);
                        break;
                    case "3": // AK(H) 2 KPEI
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, cbSource);
                        cbBank.Text = list[1].bank;
                        listz = getKpeiAccount();
                        pushKPEI(listz, cbTarget);
                        break;
                    case "4": // KPEI 2 AK(H)
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        listz = getKpeiAccount();
                        pushKPEI(listz, cbSource);
                        pushClient(list, cbTarget);
                        break;
                    case "5": // KPEI 2 KPEI
                        listz = getKpeiAccount();
                        pushKPEI(listz, cbSource);
                        pushKPEI(listz, cbTarget);
                        break;
                }
            }
 */ 
        }

        protected void cbBank_SelectedIndexChanged(object sender, EventArgs e)
        {
                listAccount[] list;
                kpeiAccount[] listz;
                switch (cbType.Value.ToString())
                {
                    case "RCCL": //AK(H) to AK(C) - RCCL
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 2, cbSource);
                        list = getClientAccouunt(cbMember.Text);
                        pushClient(list, -1, cbTarget);
                        break;
                    case "DCCL": //AK(C) to AK(H) - DCCL
                        list = getClientAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, -1, cbSource);
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 2, cbTarget);
                        break;
                    case "DSSD": //AK(H) to AK(H) - DSSD
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 1, cbSource);
                        pushClient(list, 0, cbTarget);
                        break;
                    case "DCCM": //AK(H) to AK(H) - DCCM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 0, cbSource);
                        pushClient(list, 2, cbTarget);
                        break;
                    case "COLM": //AK(H) to AK(H) - COLM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 0, cbSource);
                        pushClient(list, 1, cbTarget);
                        break;
                    case "DSCM": //AK(H) to KPEI - DSCM
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 2, cbSource);
                        cbBank.Text = list[1].bank;
                        listz = getKpeiAccount();
                        pushKPEI(listz, 2, cbTarget);
                        break;
                    case "GFCM": //KPEI to KPEI - GFCM
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 0, cbSource);
                        pushKPEI(listz, 2, cbTarget);
                        break;
                    case "PGCM": //KPEI to KPEI - PGCM
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 1, cbTarget);
                        break;
                    case "IGCM": //KPEI to KPEI - IGCM
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 0, cbTarget);
                        break;
                    case "GFFK": //KPEI to KPEI - GFFK
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 0, cbTarget);
                        break;
                    case "CLFK": //KPEI to KPEI - CLFK
                        listz = getKpeiAccount();
                        cbMember.Text = "KPEI";
                        pushKPEI(listz, 2, cbSource);
                        pushKPEI(listz, 3, cbTarget);
                        break;
                    case "RSCM": //KPEI to AK(H) - RSCM
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        listz = getKpeiAccount();
                        pushKPEI(listz, 2, cbSource);
                        pushClient(list, 2, cbTarget);
                        break;
                    case "RCCM": //AK(H) to AK(H) - RCCM                    
                        list = getMemberAccouunt(cbMember.Text);
                        cbBank.Text = list[1].bank;
                        pushClient(list, 2, cbSource);
                        list = getMemberAccouunt(cbMember.Text);
                        pushClient(list, 0, cbTarget);
                        break;
                }            
        }

        protected void cbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSource.Text == cbTarget.Text)
            {
                ShowMessage("Source = Target", @"../derivatif/ManualSettlementEdit.aspx");
                cbTarget.Text = "";
            }
        }

        protected void cbTarget_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSource.Text == cbTarget.Text)
            {
                ShowMessage("Source = Target", @"../derivatif/ManualSettlementEdit.aspx");
                cbTarget.Text = "";
            }
        }

    }
}