﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using DevExpress.XtraReports.UI;
using Common;
using Common.clientAccountWS;

namespace imq.kpei.derivatif
{
    public partial class ClientAccountReport : DevExpress.XtraReports.UI.XtraReport
    {
        private clientAccount[] listAccount;
        public String aBank;
        public String aMember;
        public String aSID;

        public ClientAccountReport()
        {
            InitializeComponent();
        }

        private void ClientAccountReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetClientAccount();
            string filename = DateTime.Now.ToString("HHmmss ddMMyyyy");
            ((XtraReport)sender).ExportOptions.PrintPreview.DefaultFileName = "Client Account " + filename;
        }

        private void GetClientAccount()
        {
            ClientAccountWSService cas = null;

            try
            {
                cas = ImqWS.GetClientAccountService();
                listAccount = cas.Search(aBank, aMember, aSID);
                if (listAccount != null)
                {
                    DataSource = listAccount;

                    XRBinding binding = new XRBinding("Text", DataSource, "id.memberId");
                    cellMember.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "memberInfo.memberName");
                    cellName.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sid");
                    cellSID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.tradingId");
                    cellTradingID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "accountId");
                    cellAccount.DataBindings.Add(binding);
                    //binding = new XRBinding("Text", DataSource, "role");
                    //cellRole.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "bank.bankName");
                    cellBank.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "collateralAccountNo");
                    cellCollateral.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                if (cas != null)
                    cas.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                cas.Dispose();
            }
        }
    }
}
