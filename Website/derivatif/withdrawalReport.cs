﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.balanceWithdrawalWS;

namespace imq.kpei.derivatif
{
    public partial class withdrawalReport : DevExpress.XtraReports.UI.XtraReport
    {
        public string aUserMember;
        public balanceWithdrawal[] listBalance;

        public string aMember = string.Empty;
        public string aSID = string.Empty;
        public string aStat = string.Empty;

        public withdrawalReport()
        {
            InitializeComponent();
        }

        public void GetBalanceWithdrawal()
        {
            BalanceWithdrawalWSService bws = null;
            try
            {
                bws = ImqWS.GetBalanceWithdrawalService();
                //if (aUserMember.Contains("Kpei"))
                //    listBalance = bws.get();
                //else
                //    listBalance = bws.getByMemberId(aUserMember);

                listBalance = bws.Search(aMember, aSID, aStat);

                if (listBalance != null)
                {
                    DataSource = listBalance;
                    XRBinding binding = null;
                    //binding = new XRBinding("Text", DataSource, "bwNo");
                    //cellNo.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "bwDate", "{0:dd/MM/yyyy}");
                    cellDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "bwDate", "{0:HH:mm}");                    
                    cellTime.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "reffNoClient");                    
                    cellReff.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "id.memberId");
                    cellMember.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sid");
                    cellSID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "accountDescription");
                    cellAccountDesc.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "accountSourceNo");
                    cellAccountNo.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "value", "{0:#,##}");
                    cellValue.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "executionDateDescription");
                    cellExecDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "executionTimeDescription");
                    cellExecTime.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "statusDescription");
                    cellStatus.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                bws.Abort();
            }
            finally
            {
                bws.Dispose();
            }
        }

        private void withdrawalReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetBalanceWithdrawal();
        }
    }
}
