﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.giveUpWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;
using DevExpress.Web.ASPxGridView;
using Common.wsTransaction;

namespace imq.kpei.derivatif
{
    public partial class takeupconfirm : System.Web.UI.Page
    {
        private int addedit;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private giveUpTemp aGiveUpTemp;
        private giveUp aGiveUp;
        private const String module = "Take Up";
        ASPxGridView detailView;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            //row = (DataRow)Session["row"];
            if (!IsPostBack)
            {
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"]));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }

                btnAccept.Visible = false;
                btnRejectTakeUp.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;

                switch (addedit)
                {
                    case 0:
                        lblTitle.Text =  module; //New Direct                        
                        btnAccept.Visible = true;
                        if (aUserMember.Contains("kpei"))
                            btnAccept.Text = "Approve";
                        btnRejectTakeUp.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lblTitle.Text = module; //Edit Direct                        
                        if (aUserMember.Contains("kpei"))
                            btnAccept.Text = "Approve";

                        btnRejectTakeUp.Visible = true;
                        btnAccept.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lblTitle.Text = module; //New From Temporary
                        Permission(false);

                        GetTemporary();

                        break;
                    case 3:
                        lblTitle.Text = module;
                        Permission(false);
                        GetTemporary();

                        break;
                }
            }
        }

        private void GetTemporary()
        {
            GiveUpWSService gws = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                aGiveUpTemp = gws.getTemporary(int.Parse(Request.QueryString["idxTmp"]));

                aGiveUp = gws.getGiveUp(int.Parse(Request.QueryString["idTable"]));
                if (aGiveUp != null)
                {
                    txtMemberGU.Text = aGiveUp.guMemberId;
                    txtMemberTU.Text = aGiveUp.tuMemberId;
                    gvTakeUp.DataSource = aGiveUp.giveUpChilds;
                    gvTakeUp.DataBind();
                }
            }
            catch (Exception e)
            {
                if (gws != null)
                    gws.Abort();

                lblError.Text = e.Message;
            }
            finally
            {
                if (gws != null)
                    gws.Dispose();
            }
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;

            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"] == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"] == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            //            string mystring = @"<script type='text/javascript'>
            //                                alert('" + info + @"');
            //                                window.location='" + targetPage + @"'
            //                                </script>";
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              jAlert('{0}','Information',function()
                                                              {{window.location='{1}'}});
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private void ParseQueryString()
        {
            int guNo =  int.Parse(Server.UrlDecode(Request.QueryString["guNo"]));

            hfTakeUp.Set("guNo", guNo);

            GiveUpWSService gws = ImqWS.GetGiveUpService();
            aGiveUp = gws.getGiveUp(guNo);
            if (aGiveUp != null)
            {
                txtMemberGU.Text = aGiveUp.guMemberId;
                txtMemberTU.Text = aGiveUp.tuMemberId;

                gvTakeUp.DataSource = aGiveUp.giveUpChilds;
                gvTakeUp.DataBind();
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            GiveUpWSService gws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;


            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                gws = ImqWS.GetGiveUpService();
                dtA = aps.Search(Request.QueryString["reffNo"]);
                aGiveUpTemp = gws.getGiveUpTemp(dtA[0].idxTmp);

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approve";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }
                string ret = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "I":
                        WriteAuditTrail(String.Format("{0} New {1}", activity, module));
                        break;
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            ret = gws.updateFromTemp(aGiveUpTemp, dtA[0].idTable);
                            if (!ret.Equals("0"))
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                gws.deleteTempById(dtA[0].idxTmp);
                                ShowMessage(String.Format("Succes Accepted {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Accepted {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    if (aUserMember.Contains("kpei"))
                                        aGiveUpTemp.kpeiApproval = "N";
                                    else
                                        aGiveUpTemp.tuApproval = "N";
                                    ret = gws.updateFromTemp(aGiveUpTemp, dtA[0].idTable);
                                    if (!ret.Equals("0"))
                                    {
                                        dtA[0].checkerName = aUserLogin;
                                        dtA[0].checkerStatus = "Failed";
                                        dtA[0].approvelStatus = "";
                                        dtA[0].memberId = aUserMember;
                                        dtA[0].status = "RC";
                                        aps.UpdateByChecker(dtA[0]);
                                        ShowMessage(ret, target);
                                    }
                                    else
                                    {
                                        activity = "Reject Checker";
                                        dtA[0].checkerName = aUserLogin;
                                        dtA[0].checkerStatus = "Rejected";
                                        dtA[0].approvelStatus = "";
                                        dtA[0].memberId = aUserMember;
                                        dtA[0].status = "RC";
                                        aps.UpdateByChecker(dtA[0]);
                                        ShowMessage(String.Format("Rejected {0} as Checker", module), target);
                                    }
                                }
                                else
                                {
                                    if (aUserMember.Contains("kpei"))
                                        aGiveUpTemp.kpeiApproval = "N";
                                    else
                                        aGiveUpTemp.tuApproval = "N";
                                    ret = gws.updateFromTemp(aGiveUpTemp, dtA[0].idTable);
                                    if (!ret.Equals("0"))
                                    {
                                        dtA[0].approvelName = aUserLogin;
                                        dtA[0].approvelStatus = "Failed";
                                        dtA[0].memberId = aUserMember;
                                        dtA[0].status = "RA";
                                        aps.UpdateByApprovel(dtA[0]);
                                        ShowMessage(ret, target);
                                    }
                                    else
                                    {
                                        dtA[0].approvelName = aUserLogin;
                                        dtA[0].approvelStatus = "Rejected";
                                        dtA[0].memberId = aUserMember;
                                        dtA[0].status = "RA";
                                        aps.UpdateByApprovel(dtA[0]);
                                        ShowMessage(String.Format("Rejected {0} as Approval", module), target);
                                    }
                                }
                            }
                        break;
                    case "R":
                        WriteAuditTrail(String.Format("{0} Delete {1}", activity, module));
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();
                if (gws != null)
                    gws.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();

                if (gws != null)
                    gws.Dispose();
            }
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(aGiveUpTemp));
        }

        private static String Log(giveUpTemp gut)
        {
            string strLog = String.Format("[Give Up Np : {0}, Give Up Member ID : {1}, Take Up Member ID : {2}]", gut.guNo, gut.guMemberId, gut.tuMemberId);
            return strLog;
        }

        protected void detailGrid_OnLoad(object sender, EventArgs e)
        {
            detailView = sender as ASPxGridView;
        }
        protected void detailGrid_DataBinding(object sender, EventArgs e)
        {
            try
            {
                detailView = sender as ASPxGridView;
                if (hfTakeUp.Get("sid") != null)
                    detailView.DataSource = getNetposition(hfTakeUp.Get("sid").ToString(), txtMemberGU.Text.Trim());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        protected void detailGrid_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView tmp = (sender as ASPxGridView);
            hfTakeUp.Set("sid", tmp.GetMasterRowKeyValue());
        }

        private clearingDto[] getNetposition(String sid, String memberId)
        {
            clearingDto[] list = null;
            UiDataService ws = null;
            try
            {
                ws = ImqWS.GetTransactionService();
                list = ws.getClearing(aUserMember.Trim(), memberId.Trim(), "", sid, "", "", "", "last");
            }
            catch (Exception ex)
            {
                if (ws != null)
                    ws.Abort();

                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (ws != null)
                    ws.Dispose();
            }

            return list;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("takeup.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }


        protected string rblApprovalChanged(dtApproval dtA, ApprovalService wsA, giveUpTemp gut)
        {
            string ret = "0";
            const string target = @"../derivatif/takeup.aspx";
            GiveUpWSService gws = ImqWS.GetGiveUpService();
            giveUp gu = null;

            if (!giveUpTemp.ReferenceEquals(aGiveUpTemp, gut))
                aGiveUpTemp = gut;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    if (aUserMember.Contains("kpei"))
                    {
                        if (gut.kpeiApproval.Equals("N"))
                        {
                            gws = ImqWS.GetGiveUpService();
                            gu = new giveUp();
                            gu.tuApproval = aGiveUpTemp.tuApproval;
                            gu.kpeiApproval = aGiveUpTemp.kpeiApproval;
                            int guNo = (int)hfTakeUp.Get("guNo");
                            ret = gws.update(guNo, gu);
                            if (ret.Equals("0"))
                            {
                                dtA.makerName = aUserLogin;
                                dtA.makerStatus = "Rejected";
                                dtA.checkerStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                            }
                            else
                            {
                                dtA.makerName = aUserLogin;
                                dtA.makerStatus = "Rejected";
                                dtA.checkerStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                            }
                            if (gws != null)
                                gws.Dispose();
                        }
                    }
                    else
                    {
                        if (gut.tuApproval.Equals("N"))
                        {
                            gws = ImqWS.GetGiveUpService();
                            gu = new giveUp();
                            gu.tuApproval = aGiveUpTemp.tuApproval;
                            gu.kpeiApproval = aGiveUpTemp.kpeiApproval;
                            int guNo = (int)hfTakeUp.Get("guNo");
                            ret = gws.update(guNo, gu);
                            if (ret.Equals("0"))
                            {
                                dtA.makerName = aUserLogin;
                                dtA.makerStatus = "Rejected";
                                dtA.checkerStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                            }
                            else
                            {
                                dtA.makerName = aUserLogin;
                                dtA.makerStatus = "Rejected";
                                dtA.checkerStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                            }
                            if (gws != null)
                                gws.Dispose();
                        }
                    }
                    wsA.AddMaker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            if (aUserMember.Contains("kpei"))
                            {
                                if (gut.kpeiApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Maker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Maker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Maker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Maker", module), target);
                                }
                            }
                            else
                            {
                                if (gut.tuApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Maker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Maker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Maker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Maker", module), target);
                                }
                            }
                            break;
                        case "E":
                            if (aUserMember.Contains("kpei"))
                            {
                                if (gut.kpeiApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Maker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Maker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Maker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Maker", module), target);
                                }
                            }
                            else
                            {
                                if (gut.tuApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Maker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Maker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Maker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Maker", module), target);
                                }
                            }
                            break;
                        case "R":
                            if (aUserMember.Contains("kpei"))
                            {
                                if (gut.kpeiApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Maker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Maker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Maker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Maker", module), target);
                                }
                            }
                            else
                            {
                                if (gut.tuApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Maker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Maker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Maker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Maker", module), target);
                                }
                            }
                            break;
                    }
                    break;
                case 1:
                    if (aUserMember.Contains("kpei"))
                    {
                        if (gut.kpeiApproval.Equals("N"))
                        {
                            gws = ImqWS.GetGiveUpService();
                            gu = new giveUp();
                            gu.tuApproval = aGiveUpTemp.tuApproval;
                            gu.kpeiApproval = aGiveUpTemp.kpeiApproval;
                            int guNo = (int)hfTakeUp.Get("guNo");
                            ret = gws.update(guNo, gu);
                            if (ret.Equals("0"))
                            {
                                dtA.checkerName = aUserLogin;
                                dtA.checkerStatus = "Rejected";
                                dtA.approvelStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                                wsA.UpdateByChecker(dtA);
                            }
                            else
                            {
                                dtA.checkerName = aUserLogin;
                                dtA.checkerStatus = "Failed";
                                dtA.approvelStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                                wsA.UpdateByChecker(dtA);
                            }
                            if (gws != null)
                                gws.Dispose();
                        }
                        else
                            wsA.AddDirectChecker(dtA);
                    }
                    else
                    {
                        if (gut.tuApproval.Equals("N"))
                        {
                            gws = ImqWS.GetGiveUpService();
                            gu = new giveUp();
                            gu.tuApproval = aGiveUpTemp.tuApproval;
                            gu.kpeiApproval = aGiveUpTemp.kpeiApproval;
                            int guNo = (int) hfTakeUp.Get("guNo");
                            ret = gws.update(guNo, gu);
                            if (ret.Equals("0"))
                            {
                                dtA.checkerName = aUserLogin;
                                dtA.checkerStatus = "Rejected";
                                dtA.approvelStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                                wsA.UpdateByChecker(dtA);
                            }
                            else
                            {
                                dtA.checkerName = aUserLogin;
                                dtA.checkerStatus = "Failed";
                                dtA.approvelStatus = "";
                                dtA.memberId = aUserMember;
                                dtA.status = "RC";
                                wsA.UpdateByChecker(dtA);
                            }
                            if (gws != null)
                                gws.Dispose();
                        }
                        else
                            wsA.AddDirectChecker(dtA);
                    }
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            if (aUserMember.Contains("kpei"))
                            {
                                if (gut.kpeiApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Direct Checker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Direct Checker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Direct Checker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Direct Checker", module), target);
                                }
                            }
                            else
                            {
                                if (gut.tuApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Direct Checker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Direct Checker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Direct Checker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Direct Checker", module), target);
                                }
                            }
                            break;
                        case "E":
                            if (aUserMember.Contains("kpei"))
                            {
                                if (gut.kpeiApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Direct Checker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Direct Checker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Direct Checker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Direct Checker", module), target);
                                }
                            }
                            else
                            {
                                if (gut.tuApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Direct Checker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Direct Checker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Direct Checker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Direct Checker", module), target);
                                }
                            }
                            break;
                        case "R":
                            if (aUserMember.Contains("kpei"))
                            {
                                if (gut.kpeiApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Direct Checker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Direct Checker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Direct Checker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Direct Checker", module), target);
                                }
                            }
                            else
                            {
                                if (gut.tuApproval.Equals("N"))
                                {
                                    WriteAuditTrail("Direct Checker Rejected " + module);
                                    ShowMessage(String.Format("Rejected {0} as Direct Checker", module), target);
                                }
                                else
                                {
                                    WriteAuditTrail("Direct Checker Accepted " + module);
                                    ShowMessage(String.Format("Succes Accepted {0} as Direct Checker", module), target);
                                }
                            }
                            break;
                    }
                    break;
                case 2:
                    gu = new giveUp();
                    gu.tuApproval = aGiveUpTemp.tuApproval;
                    gu.kpeiApproval = aGiveUpTemp.kpeiApproval;
                    //ser.idx = aSeriesTemp.idx;

                    switch (dtA.insertEdit)
                    {
                        case "E":
                            int guNo = (int) hfTakeUp.Get("guNo");
                            ret = gws.update(guNo, gu);
                            if (ret.Equals("0"))
                            {
                                gws.Dispose();
                                if (aUserMember.Contains("kpei"))
                                {
                                    if (gut.kpeiApproval.Equals("N"))
                                    {
                                        dtA.approvelName = aUserLogin;
                                        dtA.approvelStatus = "Rejected";
                                        dtA.memberId = aUserMember;
                                        dtA.status = "RA";
                                        wsA.UpdateByApprovel(dtA);
                                        ShowMessage(String.Format("Rejected {0} as Direct Approval", module), target);
                                        WriteAuditTrail("Direct Approval Rejected " + module);
                                    }
                                    else
                                    {
                                        wsA.AddDirectApproval(dtA);
                                        ShowMessage(String.Format("Succes Accepted {0} as Direct Approval", module), target);
                                        WriteAuditTrail("Direct Approval Accepted " + module);
                                    }
                                }
                                else
                                {
                                    if (gut.tuApproval.Equals("N"))
                                    {
                                        dtA.approvelName = aUserLogin;
                                        dtA.approvelStatus = "Rejected";
                                        dtA.memberId = aUserMember;
                                        dtA.status = "RA";
                                        wsA.UpdateByApprovel(dtA);
                                        ShowMessage(String.Format("Rejected {0} as Direct Approval", module), target);
                                        WriteAuditTrail("Direct Approval Rejected " + module);
                                    }
                                    else
                                    {
                                        wsA.AddDirectApproval(dtA);
                                        ShowMessage(String.Format("Succes Accepted {0} as Direct Approval", module), target);
                                        WriteAuditTrail("Direct Approval Accepted " + module);
                                    }
                                }
                            }
                            else
                            {
                                dtA.approvelStatus = "Failed";
                                wsA.AddDirectApproval(dtA);
                            }
                            break;
                    }
                    break;
            }
            return ret;
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            updateApproval(0);
        }

        protected void btnRejectTakeUp_Click(object sender, EventArgs e)
        {
            updateApproval(1);
        }

        private void updateApproval(int app)
        {
            GiveUpWSService gws = null;
            ApprovalService aps = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                aps = ImqWS.GetApprovalWebService();

                addedit = (int)Session["stat"];
                int idRec;
                aGiveUp = gws.getGiveUp(int.Parse(hfTakeUp.Get("guNo").ToString()));
                giveUpTemp gut = new giveUpTemp() { guMemberId = aGiveUp.guMemberId, tuMemberId = aGiveUp.tuMemberId, tuApproval = aGiveUp.tuApproval.Trim(), kpeiApproval = aGiveUp.kpeiApproval.Trim() };

                if (aUserMember.Contains("kpei"))
                {
                    if (!gut.tuApproval.Equals("Y"))
                        return;

                    if (app == 0)
                        gut.kpeiApproval = "Y";
                    else
                        gut.kpeiApproval = "N";
                }
                else
                {
                    if (app == 0)
                        gut.tuApproval = "Y";
                    else
                        gut.tuApproval = "N";
                }

                idRec = gws.addToTemp(gut);
                aGiveUpTemp = gut;
                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/derivatif/takeupconfirm.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = module;
                            dtA.insertEdit = "I";
                            aGiveUpTemp.guNo = idRec;
                            break;

                        case 1:
                            dtA.topic =  module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfTakeUp.Get("guNo").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfTakeUp.Get("guNo").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                    }
                    string retVal = "0";
                    retVal = rblApprovalChanged(dtA, aps, aGiveUpTemp);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../derivatif/takeup.aspx");
                    }
                }
                else
                {
                    ShowMessage("message=Failed On Save To Temporary Take Up", @"../derivatif/takeup.aspx");
                }
            }
            catch (Exception e)
            {
                if (gws != null)
                    gws.Abort();
                if (aps != null)
                    aps.Abort();

                lblError.Text = e.Message;
            }
            finally
            {
                if (gws != null)
                    gws.Dispose();

                if (aps != null)
                    aps.Dispose();
            }
        }

        protected void gvTakeUp_DataBinding(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("takeup.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/account/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }

        private static giveUpChild[] getGiveUpChild(long guNo)
        {
            GiveUpWSService gws = null;
            giveUpChild[] list = null;
            try
            {
                gws = ImqWS.GetGiveUpService();
                list = gws.getChild(guNo);
            }
            catch (Exception e)
            {
                if (gws != null)
                    gws.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                gws.Dispose();
            }

            return list;
        }

        protected void btnCancel_Click1(object sender, EventArgs e)
        {
        }
    }
}
