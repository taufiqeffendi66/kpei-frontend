﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.derivatif
{
    public partial class SeriesView : System.Web.UI.Page
    {
        private String aSeries;
        private String aUnderlying;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (Request.QueryString["series"] != null)
                aSeries = Request.QueryString["series"].Trim();

            if (Request.QueryString["underlying"] != null)
                aUnderlying = Request.QueryString["underlying"].Trim();

            ReportViewer1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            SeriesReport report = new SeriesReport() { aSeries = aSeries, aUnderlying = aUnderlying, Name = ImqSession.getFormatFileSave("Series Contract") };
            return report;
        }
    }
}
