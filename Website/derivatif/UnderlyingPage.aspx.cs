﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.underlyingWS;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class UnderlyingPage : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                gvUnderlying.DataBind();
                Permission();
            }
        }

        protected void gvUnderlying_DataBinding(object sender, EventArgs e)
        {
            gvUnderlying.DataSource = getUnderlying();
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Underlying");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        private static underlying[] getUnderlying()
        {
            underlying[] list = null;
            UnderlyingWSService uws = null;
            try
            {
                uws = ImqWS.GetUnderlyingWebService();
                list = uws.get();
            }
            catch (Exception e)
            {
                uws.Abort();
                //lblError.Text = "Error On Fetch Data";
                Debug.WriteLine(e.Message);
            }
            finally
            {
                uws.Dispose();
            }
            return list;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("UnderlyingEdit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string code         = hfUnderlying.Get("code").ToString();
            string description  = hfUnderlying.Get("description").ToString();
            string idx          = hfUnderlying.Get("idx").ToString();
            if (code.Length <= 0)
                return;
            string strRedirect = String.Format("UnderlyingEdit.aspx?stat=1&code={0}&description={1}&idx={2}", Server.UrlEncode(code.Trim()), Server.UrlEncode(description.Trim()), Server.UrlEncode(idx.Trim()));
            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfUnderlying.Get("code").ToString();
            string description = hfUnderlying.Get("description").ToString();
            string idx = hfUnderlying.Get("idx").ToString();
            if (code.Length <= 0)
                return;

            string strRedirect = String.Format("UnderlyingEdit.aspx?stat=4&code={0}&description={1}&idx={2}", Server.UrlEncode(code.Trim()), Server.UrlEncode(description.Trim()), Server.UrlEncode(idx.Trim()));
            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
            //UnderlyingWSService uws = null;
            //try
            //{
            //    uws = new UnderlyingWSService();
            //    string strRet = uws.Delete(hfUnderlying.Get("code").ToString().Trim());
            //    if (strRet.Equals("0"))
            //    {
            //        lblError.Text = "";
            //        gvUnderlying.DataBind();
            //    }
            //    else                
            //        lblError.Text = strRet;
            //}
            //catch (Exception ex)
            //{
            //    uws.Abort();
            //    Debug.WriteLine(ex.Message);
            //}
            //finally
            //{
            //    uws.Dispose();
            //}
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string code = txtSearch.Text.Trim();
            code = code.Replace("'", "''");
            code = code.Replace("--", "-");
            gvUnderlying.FilterExpression = String.Format("[code] like '%{0}%'", code);
        }
    }
}
