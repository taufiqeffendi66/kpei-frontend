﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.bankWS;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class bankpage : System.Web.UI.Page
    {
        public string stataddedit = "0";
        private bank[] banks;
        private String aUserLogin;
        private String aUserMember;
        public String aBankCode;
        public String aBankName;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                Permission();
                gvBank.DataBind();
            }
        }

        private bank[] getWSBank()
        {
            BankWSService bs = null;
            try
            {
                bs = ImqWS.GetBankWebService();
                banks = bs.get();
                if (!IsPostBack)
                {
                    cmbID.Items.Clear();
                    cmbName.Items.Clear();
                    foreach (bank b in banks)
                    {
                        cmbID.Items.Add(b.bankCode, b.bankCode);
                        cmbName.Items.Add(b.name, b.name);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                bs.Abort();
            }
            finally
            {
                bs.Dispose();
            }
            return banks;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string bankCode = cmbID.Text.Trim();
            string bankName = cmbName.Text.Trim();
            string search = String.Empty;

            if (bankCode != String.Empty)
            {
                bankCode = bankCode.Replace("'", "''");
                bankCode = bankCode.Replace("--", "-");
                aBankCode = bankCode;
                bankCode = String.Format("[bankCode] like '%{0}%'", bankCode);
            }

            if (bankName != String.Empty)
            {
                bankName = bankName.Replace("'", "''");
                bankName = bankName.Replace("--", "-");
                aBankName = bankName;
                bankName = String.Format("[name] like '%{0}%'", bankName);
            }

            if (bankCode != String.Empty)
            {
                if (bankName != String.Empty)
                    search = String.Format("{0} and {1}", bankCode, bankName);
                else
                    search = bankCode;
            }
            else
            {
                if (bankName != String.Empty)
                    search = bankName;
                else
                    search = String.Empty;
            }
            gvBank.FilterExpression = search;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("BankEdit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string strCode = hfBank.Get("code") == null ? " " : hfBank.Get("code").ToString().Trim();
            string strName = hfBank.Get("name") == null ? " " : hfBank.Get("name").ToString().Trim();
            string strContact = hfBank.Get("contact") == null ? " " : hfBank.Get("contact").ToString().Trim();
            string strBranch = hfBank.Get("branch") == null ? " " : hfBank.Get("branch").ToString().Trim();
            string strAddress = hfBank.Get("address") == null ? " " : hfBank.Get("address").ToString().Trim();
            string strPhone = hfBank.Get("phone") == null ? " " : hfBank.Get("phone").ToString().Trim();
            string strFax = hfBank.Get("fax") == null ? " " : hfBank.Get("fax").ToString().Trim();
            string strEmail = hfBank.Get("email") == null ? " " : hfBank.Get("email").ToString().Trim();
            string strIdx = hfBank.Get("idx") == null ? " " : hfBank.Get("idx").ToString().Trim();
            string strRedirect = String.Format("BankEdit.aspx?stat=1&code={0}&bankName={1}&contact={2}&branch={3}&address={4}&phone={5}&fax={6}&email={7}&idx={8}", Server.UrlEncode(strCode), Server.UrlEncode(strName), Server.UrlEncode(strContact), Server.UrlEncode(strBranch), Server.UrlEncode(strAddress), Server.UrlEncode(strPhone), Server.UrlEncode(strFax), Server.UrlEncode(strEmail), Server.UrlEncode(strIdx));


            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void gvBank_DataBinding(object sender, EventArgs e)
        {
            gvBank.DataSource = getWSBank();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strCode = hfBank.Get("code") == null ? " " : hfBank.Get("code").ToString().Trim();
            string strName = hfBank.Get("name") == null ? " " : hfBank.Get("name").ToString().Trim();
            string strContact = hfBank.Get("contact") == null ? " " : hfBank.Get("contact").ToString().Trim();
            string strBranch = hfBank.Get("branch") == null ? " " : hfBank.Get("branch").ToString().Trim();
            string strAddress = hfBank.Get("address") == null ? " " : hfBank.Get("address").ToString().Trim();
            string strPhone = hfBank.Get("phone") == null ? " " : hfBank.Get("phone").ToString().Trim();
            string strFax = hfBank.Get("fax") == null ? " " : hfBank.Get("fax").ToString().Trim();
            string strEmail = hfBank.Get("email") == null ? " " : hfBank.Get("email").ToString().Trim();
            string strIdx = hfBank.Get("idx") == null ? " " : hfBank.Get("idx").ToString().Trim();

            string strRedirect = String.Format("BankEdit.aspx?stat=4&code={0}&bankName={1}&contact={2}&branch={3}&address={4}&phone={5}&fax={6}&email={7}&idx={8}", Server.UrlEncode(strCode), Server.UrlEncode(strName), Server.UrlEncode(strContact), Server.UrlEncode(strBranch), Server.UrlEncode(strAddress), Server.UrlEncode(strPhone), Server.UrlEncode(strFax), Server.UrlEncode(strEmail), Server.UrlEncode(strIdx));


            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Bank");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }
    }
}
