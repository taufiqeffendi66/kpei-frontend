﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.bankWS;
using Common.memberInfoWS;
using Common.clientAccountWS;
using Common.wsUserGroupPermission;
using System.Diagnostics;

namespace imq.kpei.derivatif
{
    public partial class ClientAccountPage : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                Server.ScriptTimeout = 4000;
                Permission();
                String member = aUserMember.ToLower();
                if (!member.Contains("kpei"))
                {
                    cmbMember.Text = aUserMember;
                    cmbMember.Enabled = false;
                    cmbMember.DropDownButton.Visible = false;
                }
                else
                    FillMember();

                FillBank();
                gvMember.DataBind();
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Client Account");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        protected void FillBank()
        {
            BankWSService bws = null;
            try
            {
                bws = ImqWS.GetBankWebService();
                Common.bankWS.bank[] banks = bws.get();
                foreach (Common.bankWS.bank bk in banks)
                {
                    cmbBank.Items.Add(bk.name, bk.bankCode);
                }
            }
            catch (Exception ex)
            {
                bws.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                bws.Dispose();
            }
        }

        protected void FillMember()
        {
            MemberInfoWSService mis = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                Common.memberInfoWS.memberInfo[] members = mis.get();
                foreach (Common.memberInfoWS.memberInfo member in members)
                {
                    cmbMember.Items.Add(member.memberId, member.memberId);
                }
            }
            catch (Exception ex)
            {
                mis.Abort();
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                mis.Dispose();
            }
        }

        private clientAccount[] getClientAccouunt()
        {
            clientAccount[] list = null;
            ClientAccountWSService cas = null;

            try
            {
                cas = ImqWS.GetClientAccountService();
                list = cas.get();

                if (!IsPostBack)
                {
                    foreach (clientAccount ca in list)
                    {
                        cmbSID.Items.Add(ca.sid, ca.sid);
                    }
                }
            }
            catch (Exception e)
            {
                cas.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                cas.Dispose();
            }

            return list;
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            string bank = cmbBank.Text.Trim();
            string member = cmbMember.Text.Trim();
            string sid = cmbSID.Text.Trim();
            string search = String.Empty;

            if (bank != String.Empty)
            {
                bank = bank.Replace("'", "''");
                bank = bank.Replace("--", "-");
                bank = String.Format("[bank.bankName] like '%{0}%'", bank);
            }

            if (member != String.Empty)
            {
                member = member.Replace("'", "''");
                member = member.Replace("--", "-");
                member = String.Format("[id.memberId] like '%{0}%'", member);
            }

            if (sid != String.Empty)
            {
                sid = sid.Replace("'", "''");
                sid = sid.Replace("--", "-");
                sid = String.Format("[sid] like '%{0}%'", sid);
            }

            if (bank != String.Empty)
            {
                if (member != String.Empty)
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1} and {2}", bank, member, sid);
                    else
                        search = String.Format("{0} and {1}", bank, member);
                }
                else
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1}", bank, sid);
                    else
                        search = bank;
                }
            }
            else
            {
                if (member != String.Empty)
                {
                    if (sid != String.Empty)
                        search = String.Format("{0} and {1}", member, sid);
                    else
                        search = member;
                }
                else
                {
                    if (sid != String.Empty)
                        search = sid;
                    else
                        search = String.Empty;
                }
            }
            gvMember.FilterExpression = search;
        }


        protected void gvMember_DataBinding(object sender, EventArgs e)
        {
            gvMember.DataSource = getClientAccouunt();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SendResponse("4");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClientAccountEdit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            SendResponse("1");
        }

        private void SendResponse(string stat)
        {
            try
            {
                string bc;
                if (hfClient.Get("bankCode") != null)
                    bc = hfClient.Get("bankCode").ToString().Trim();
                else
                    bc = string.Empty;

                string strResp = String.Format("ClientAccountEdit.aspx?stat={0}&member_id={1}&sid={2}&trading_id={3}&account_id={4}&role={5}&bankCode={6}&idx={7}&collateral_acc_no={8}", stat, hfClient.Get("member_id").ToString().Trim(), hfClient.Get("sid").ToString().Trim(), hfClient.Get("trading_id").ToString().Trim(), hfClient.Get("account_id").ToString().Trim(), hfClient.Get("role").ToString().Trim(), bc, hfClient.Get("idx").ToString().Trim(), hfClient.Get("collateral_acc_no"));
                Response.Redirect(strResp, false);
                Context.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
        }
    }
}
