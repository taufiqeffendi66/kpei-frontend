﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Common;
using Common.memberStatusWS;
using System.Diagnostics;

namespace imq.kpei.derivatif
{
    public partial class MemberStatusReport : DevExpress.XtraReports.UI.XtraReport
    {
        private memberStatus[] listStatus;
        public String aMember;

        public MemberStatusReport()
        {
            InitializeComponent();
        }

        public void GetMemberStatus()
        {
            MemberStatusWSService mss = null;
            try
            {
                mss = ImqWS.GetMemberStatusService();
                listStatus = mss.Search(aMember);
                if (listStatus != null)
                {
                    DataSource = listStatus;

                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "statusDate");
                    binding.FormatString = "{0:dd/MM/yyyy}";
                    cellCreate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "statusDate");
                    binding.FormatString = "{0:HH:mm}";
                    cellTime.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "memberId");
                    cellMember.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "statusBeforeName");
                    cellBefore.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "statusCurrentName");
                    cellCurrent.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "positionLimitDesc");
                    cellCurrent.DataBindings.Add(binding);
                }
            }
            catch (Exception e)
            {
                if (mss != null)
                    mss.Abort();
                Debug.WriteLine(e.Message);
            }
            finally
            {
                mss.Dispose();
            }
        }

        private void MemberStatusReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetMemberStatus();
        }
    }
}
