﻿namespace imq.kpei.derivatif
{
    partial class GiveUpReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGuMemberID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTuMemberID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTuApproval = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellKpeiApproval = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellExecution = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.header1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xtraReport1 = new DevExpress.XtraReports.UI.XtraReport();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(776F, 31.25F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellDate,
            this.cellTime,
            this.cellNo,
            this.cellGuMemberID,
            this.cellTuMemberID,
            this.cellTuApproval,
            this.cellKpeiApproval,
            this.cellExecution,
            this.cellStatus});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellDate
            // 
            this.cellDate.Name = "cellDate";
            this.cellDate.StylePriority.UseTextAlignment = false;
            this.cellDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellDate.Weight = 0.30227796395844453D;
            // 
            // cellTime
            // 
            this.cellTime.Name = "cellTime";
            this.cellTime.StylePriority.UseTextAlignment = false;
            this.cellTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellTime.Weight = 0.24561500962352273D;
            // 
            // cellNo
            // 
            this.cellNo.Name = "cellNo";
            this.cellNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.cellNo.StylePriority.UsePadding = false;
            this.cellNo.StylePriority.UseTextAlignment = false;
            this.cellNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellNo.Weight = 0.38842450958591912D;
            // 
            // cellGuMemberID
            // 
            this.cellGuMemberID.Name = "cellGuMemberID";
            this.cellGuMemberID.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.cellGuMemberID.StylePriority.UsePadding = false;
            this.cellGuMemberID.StylePriority.UseTextAlignment = false;
            this.cellGuMemberID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellGuMemberID.Weight = 0.31631168877053878D;
            // 
            // cellTuMemberID
            // 
            this.cellTuMemberID.Name = "cellTuMemberID";
            this.cellTuMemberID.StylePriority.UseTextAlignment = false;
            this.cellTuMemberID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellTuMemberID.Weight = 0.3128963831777829D;
            // 
            // cellTuApproval
            // 
            this.cellTuApproval.Name = "cellTuApproval";
            this.cellTuApproval.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellTuApproval.StylePriority.UsePadding = false;
            this.cellTuApproval.Weight = 0.3128963831777829D;
            // 
            // cellKpeiApproval
            // 
            this.cellKpeiApproval.Name = "cellKpeiApproval";
            this.cellKpeiApproval.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellKpeiApproval.StylePriority.UsePadding = false;
            this.cellKpeiApproval.Weight = 0.29154674048358109D;
            // 
            // cellExecution
            // 
            this.cellExecution.Name = "cellExecution";
            this.cellExecution.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.cellExecution.StylePriority.UsePadding = false;
            this.cellExecution.Weight = 0.54388153184402332D;
            // 
            // cellStatus
            // 
            this.cellStatus.Name = "cellStatus";
            this.cellStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.cellStatus.StylePriority.UsePadding = false;
            this.cellStatus.Weight = 0.28614978937840435D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 53.95834F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(625F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 31.25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.tblHeader.SizeF = new System.Drawing.SizeF(776F, 31.25F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.header1,
            this.header2,
            this.header3,
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell4,
            this.xrTableCell3,
            this.xrTableCell6});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // header1
            // 
            this.header1.Name = "header1";
            this.header1.Text = "Date";
            this.header1.Weight = 0.30227796395844453D;
            // 
            // header2
            // 
            this.header2.Name = "header2";
            this.header2.Text = "Time";
            this.header2.Weight = 0.24561500962352273D;
            // 
            // header3
            // 
            this.header3.Name = "header3";
            this.header3.Text = "No";
            this.header3.Weight = 0.38842450958591912D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Give Up Member ID";
            this.xrTableCell1.Weight = 0.31631168877053878D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Take Up Member ID";
            this.xrTableCell2.Weight = 0.3128963831777829D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Take Up Approval";
            this.xrTableCell5.Weight = 0.3128963831777829D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "KPEI Approval";
            this.xrTableCell4.Weight = 0.29154674048358109D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Execution Time";
            this.xrTableCell3.Weight = 0.54388153184402332D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Status";
            this.xrTableCell6.Weight = 0.28614978937840435D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lbTitle,
            this.xrPageInfo2});
            this.ReportHeader.HeightF = 69.00001F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.00001F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(775.9999F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle
            // 
            this.lbTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lbTitle.ForeColor = System.Drawing.Color.Black;
            this.lbTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle.SizeF = new System.Drawing.SizeF(196.93F, 38F);
            this.lbTitle.StylePriority.UseForeColor = false;
            this.lbTitle.Text = "Give Up";
            this.lbTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.Black;
            this.xrPageInfo2.Format = "{0:\"Current Date: \" dddd, dd MMMM yyyy}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(483.9999F, 36F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(292F, 23F);
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // GiveUpReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(40, 34, 54, 51);
            this.Version = "11.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GiveUpReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell header1;
        private DevExpress.XtraReports.UI.XRTableCell header2;
        private DevExpress.XtraReports.UI.XRTableCell header3;
        private DevExpress.XtraReports.UI.XtraReport xtraReport1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellDate;
        private DevExpress.XtraReports.UI.XRTableCell cellTime;
        private DevExpress.XtraReports.UI.XRTableCell cellNo;
        private DevExpress.XtraReports.UI.XRTableCell cellGuMemberID;
        private DevExpress.XtraReports.UI.XRTableCell cellTuMemberID;
        private DevExpress.XtraReports.UI.XRTableCell cellTuApproval;
        private DevExpress.XtraReports.UI.XRTableCell cellKpeiApproval;
        private DevExpress.XtraReports.UI.XRTableCell cellExecution;
        private DevExpress.XtraReports.UI.XRTableCell cellStatus;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
    }
}
