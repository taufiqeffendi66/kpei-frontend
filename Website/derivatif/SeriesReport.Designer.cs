﻿namespace imq.kpei.derivatif
{
    partial class SeriesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellUnderlying = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellType = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellPeriod = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOpenPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOption = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellMethod = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellLastDay = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellMultiplier = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTimeFrom = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTimeTo = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellStyle = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellMargining = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.header1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.header5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellSeriesStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellInstrument = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellRemark = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellRemark2 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableDetail});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableDetail
            // 
            this.tableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableDetail.Font = new System.Drawing.Font("Tahoma", 7F);
            this.tableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableDetail.Name = "tableDetail";
            this.tableDetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.tableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tableDetail.SizeF = new System.Drawing.SizeF(1366F, 31.25F);
            this.tableDetail.StylePriority.UseBorders = false;
            this.tableDetail.StylePriority.UseFont = false;
            this.tableDetail.StylePriority.UsePadding = false;
            this.tableDetail.StylePriority.UseTextAlignment = false;
            this.tableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellCode,
            this.cellUnderlying,
            this.cellType,
            this.cellDescription,
            this.cellSeriesStatus,
            this.cellInstrument,
            this.cellRemark,
            this.cellRemark2,
            this.cellStart,
            this.cellEnd,
            this.cellPeriod,
            this.cellPrice,
            this.cellOpenPrice,
            this.cellOption,
            this.cellMethod,
            this.cellLastDay,
            this.cellMultiplier,
            this.cellTimeFrom,
            this.cellTimeTo,
            this.cellStyle,
            this.cellMargining});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellCode
            // 
            this.cellCode.Name = "cellCode";
            this.cellCode.StylePriority.UseFont = false;
            this.cellCode.StylePriority.UseTextAlignment = false;
            this.cellCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellCode.Weight = 0.30227797708282611D;
            // 
            // cellUnderlying
            // 
            this.cellUnderlying.Name = "cellUnderlying";
            this.cellUnderlying.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellUnderlying.StylePriority.UseFont = false;
            this.cellUnderlying.StylePriority.UsePadding = false;
            this.cellUnderlying.Weight = 0.237596575204991D;
            // 
            // cellType
            // 
            this.cellType.Name = "cellType";
            this.cellType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellType.StylePriority.UseFont = false;
            this.cellType.StylePriority.UsePadding = false;
            this.cellType.StylePriority.UseTextAlignment = false;
            this.cellType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellType.Weight = 0.1384951492425123D;
            // 
            // cellDescription
            // 
            this.cellDescription.Name = "cellDescription";
            this.cellDescription.StylePriority.UseFont = false;
            this.cellDescription.Weight = 0.38498709070028431D;
            // 
            // cellStart
            // 
            this.cellStart.Name = "cellStart";
            this.cellStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellStart.StylePriority.UseFont = false;
            this.cellStart.StylePriority.UsePadding = false;
            this.cellStart.StylePriority.UseTextAlignment = false;
            this.cellStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellStart.Weight = 0.23155572473266017D;
            // 
            // cellEnd
            // 
            this.cellEnd.Name = "cellEnd";
            this.cellEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellEnd.StylePriority.UseFont = false;
            this.cellEnd.StylePriority.UsePadding = false;
            this.cellEnd.StylePriority.UseTextAlignment = false;
            this.cellEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellEnd.Weight = 0.21522439373601912D;
            // 
            // cellPeriod
            // 
            this.cellPeriod.Name = "cellPeriod";
            this.cellPeriod.StylePriority.UseFont = false;
            this.cellPeriod.StylePriority.UseTextAlignment = false;
            this.cellPeriod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellPeriod.Weight = 0.18819063664954522D;
            // 
            // cellPrice
            // 
            this.cellPrice.Name = "cellPrice";
            this.cellPrice.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.cellPrice.StylePriority.UseFont = false;
            this.cellPrice.StylePriority.UsePadding = false;
            this.cellPrice.StylePriority.UseTextAlignment = false;
            this.cellPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellPrice.Weight = 0.38001600740704278D;
            // 
            // cellOpenPrice
            // 
            this.cellOpenPrice.Name = "cellOpenPrice";
            this.cellOpenPrice.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.cellOpenPrice.StylePriority.UsePadding = false;
            this.cellOpenPrice.StylePriority.UseTextAlignment = false;
            this.cellOpenPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellOpenPrice.Weight = 0.34770260455200896D;
            // 
            // cellOption
            // 
            this.cellOption.Name = "cellOption";
            this.cellOption.StylePriority.UseFont = false;
            this.cellOption.StylePriority.UseTextAlignment = false;
            this.cellOption.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellOption.Weight = 0.17453865212442812D;
            // 
            // cellMethod
            // 
            this.cellMethod.Name = "cellMethod";
            this.cellMethod.StylePriority.UseFont = false;
            this.cellMethod.StylePriority.UseTextAlignment = false;
            this.cellMethod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellMethod.Weight = 0.23259228815643995D;
            // 
            // cellLastDay
            // 
            this.cellLastDay.Name = "cellLastDay";
            this.cellLastDay.StylePriority.UseFont = false;
            this.cellLastDay.StylePriority.UseTextAlignment = false;
            this.cellLastDay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellLastDay.Weight = 0.25404796377882288D;
            // 
            // cellMultiplier
            // 
            this.cellMultiplier.Name = "cellMultiplier";
            this.cellMultiplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.cellMultiplier.StylePriority.UseFont = false;
            this.cellMultiplier.StylePriority.UsePadding = false;
            this.cellMultiplier.StylePriority.UseTextAlignment = false;
            this.cellMultiplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellMultiplier.Weight = 0.2416246276896592D;
            // 
            // cellTimeFrom
            // 
            this.cellTimeFrom.Name = "cellTimeFrom";
            this.cellTimeFrom.StylePriority.UseFont = false;
            this.cellTimeFrom.StylePriority.UseTextAlignment = false;
            this.cellTimeFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellTimeFrom.Weight = 0.20392896549879636D;
            // 
            // cellTimeTo
            // 
            this.cellTimeTo.Name = "cellTimeTo";
            this.cellTimeTo.StylePriority.UseFont = false;
            this.cellTimeTo.StylePriority.UseTextAlignment = false;
            this.cellTimeTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellTimeTo.Weight = 0.21407951920248847D;
            // 
            // cellStyle
            // 
            this.cellStyle.Name = "cellStyle";
            this.cellStyle.StylePriority.UseFont = false;
            this.cellStyle.StylePriority.UseTextAlignment = false;
            this.cellStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellStyle.Weight = 0.21552880196262181D;
            // 
            // cellMargining
            // 
            this.cellMargining.Name = "cellMargining";
            this.cellMargining.StylePriority.UseFont = false;
            this.cellMargining.StylePriority.UseTextAlignment = false;
            this.cellMargining.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellMargining.Weight = 0.23727495608832866D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 54F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1215F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 31.25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.tblHeader.SizeF = new System.Drawing.SizeF(1366F, 31.25F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.header1,
            this.header2,
            this.header3,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.header4,
            this.header5,
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell11,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell10,
            this.xrTableCell6,
            this.xrTableCell8,
            this.xrTableCell7,
            this.xrTableCell9});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // header1
            // 
            this.header1.Name = "header1";
            this.header1.StylePriority.UseFont = false;
            this.header1.Text = "Series Code";
            this.header1.Weight = 0.30227797708282617D;
            // 
            // header2
            // 
            this.header2.Name = "header2";
            this.header2.StylePriority.UseFont = false;
            this.header2.Text = "Underlying";
            this.header2.Weight = 0.23759664373614686D;
            // 
            // header3
            // 
            this.header3.Name = "header3";
            this.header3.StylePriority.UseFont = false;
            this.header3.Text = "Type";
            this.header3.Weight = 0.13849515705014931D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.Text = "Description";
            this.xrTableCell12.Weight = 0.3849871449156102D;
            // 
            // header4
            // 
            this.header4.Name = "header4";
            this.header4.StylePriority.UseFont = false;
            this.header4.Text = "Start Date";
            this.header4.Weight = 0.23155597436047526D;
            // 
            // header5
            // 
            this.header5.Name = "header5";
            this.header5.StylePriority.UseFont = false;
            this.header5.Text = "End Date";
            this.header5.Weight = 0.21522418596880927D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Contract Period";
            this.xrTableCell1.Weight = 0.18819064966158605D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "Strike Price";
            this.xrTableCell2.Weight = 0.38001603516659754D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "Opening Price";
            this.xrTableCell11.Weight = 0.34770241435910482D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "Options Type";
            this.xrTableCell3.Weight = 0.1745389151960032D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Settlement Method";
            this.xrTableCell4.Weight = 0.23259231418041959D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.Text = "Last Day Transaction";
            this.xrTableCell5.Weight = 0.25404845109514995D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "multiplier";
            this.xrTableCell10.Weight = 0.24162417897335375D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = "Exercise Time From";
            this.xrTableCell6.Weight = 0.20392899278045551D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Exercise Time To";
            this.xrTableCell8.Weight = 0.21407954824319742D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Tahoma", 6.5F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "Exercise Style";
            this.xrTableCell7.Weight = 0.21552881847475019D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "Margining Type";
            this.xrTableCell9.Weight = 0.23727448015668279D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lbTitle,
            this.xrPageInfo2});
            this.ReportHeader.HeightF = 84F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 75F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(1366F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle
            // 
            this.lbTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lbTitle.ForeColor = System.Drawing.Color.Black;
            this.lbTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle.SizeF = new System.Drawing.SizeF(196.93F, 38F);
            this.lbTitle.StylePriority.UseForeColor = false;
            this.lbTitle.Text = "Series Contract";
            this.lbTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.Black;
            this.xrPageInfo2.Format = "{0:\"Current Date: \" dddd, dd MMMM yyyy}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(1074F, 50.99999F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(292F, 23F);
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "Series Status";
            this.xrTableCell13.Weight = 0.2003462845261505D;
            // 
            // cellSeriesStatus
            // 
            this.cellSeriesStatus.Name = "cellSeriesStatus";
            this.cellSeriesStatus.Weight = 0.20034624645979204D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Instrument Code";
            this.xrTableCell14.Weight = 0.32166144175686107D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "Remark";
            this.xrTableCell15.Weight = 0.23130432166341208D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "Remark 2";
            this.xrTableCell16.Weight = 0.3279538043444552D;
            // 
            // cellInstrument
            // 
            this.cellInstrument.Name = "cellInstrument";
            this.cellInstrument.Weight = 0.32166139886035816D;
            // 
            // cellRemark
            // 
            this.cellRemark.Name = "cellRemark";
            this.cellRemark.Weight = 0.23130429262227936D;
            // 
            // cellRemark2
            // 
            this.cellRemark2.Name = "cellRemark2";
            this.cellRemark2.Weight = 0.32795376489023109D;
            // 
            // SeriesReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(14, 10, 54, 51);
            this.PageHeight = 850;
            this.PageWidth = 1400;
            this.PaperKind = System.Drawing.Printing.PaperKind.Legal;
            this.Version = "11.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.SeriesReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell header1;
        private DevExpress.XtraReports.UI.XRTableCell header2;
        private DevExpress.XtraReports.UI.XRTableCell header3;
        private DevExpress.XtraReports.UI.XRTableCell header4;
        private DevExpress.XtraReports.UI.XRTableCell header5;
        private DevExpress.XtraReports.UI.XRTable tableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellCode;
        private DevExpress.XtraReports.UI.XRTableCell cellUnderlying;
        private DevExpress.XtraReports.UI.XRTableCell cellType;
        private DevExpress.XtraReports.UI.XRTableCell cellStart;
        private DevExpress.XtraReports.UI.XRTableCell cellEnd;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell cellPeriod;
        private DevExpress.XtraReports.UI.XRTableCell cellPrice;
        private DevExpress.XtraReports.UI.XRTableCell cellOption;
        private DevExpress.XtraReports.UI.XRTableCell cellMethod;
        private DevExpress.XtraReports.UI.XRTableCell cellLastDay;
        private DevExpress.XtraReports.UI.XRTableCell cellTimeFrom;
        private DevExpress.XtraReports.UI.XRTableCell cellTimeTo;
        private DevExpress.XtraReports.UI.XRTableCell cellStyle;
        private DevExpress.XtraReports.UI.XRTableCell cellMargining;
        private DevExpress.XtraReports.UI.XRTableCell cellMultiplier;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell cellDescription;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell cellOpenPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell cellSeriesStatus;
        private DevExpress.XtraReports.UI.XRTableCell cellInstrument;
        private DevExpress.XtraReports.UI.XRTableCell cellRemark;
        private DevExpress.XtraReports.UI.XRTableCell cellRemark2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
    }
}
