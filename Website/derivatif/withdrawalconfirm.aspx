﻿<%@ Page Title="Withdrawal" Language="C#"  AutoEventWireup="true" CodeBehind="withdrawalconfirm.aspx.cs" Inherits="imq.kpei.derivatif.withdrawalconfirm" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <dx:ASPxHiddenField ID="hdWithdraw" runat="server"></dx:ASPxHiddenField>
    <div class="content">
        <div class="title"><h3>Balance Withdrawal Confirmation Form</h3></div>
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#ff0000" />
        <p>Please debit:</p>
        <table style="margin-left: 30px">
            <tr>
                <td style="width:128px">Account No</td>
                <td><dx:ASPxTextBox ID="txtAccNoDb" runat="server" Width="170px" ReadOnly="true"/> </td>
            </tr>
            <tr>
                <td>Account Name</td>
                <td><dx:ASPxTextBox ID="txtAccNameDb" runat="server" Width="170px" ReadOnly="true" /> </td>
            </tr>
            <tr>
                <td>Value</td>
                <td><dx:ASPxTextBox ID="txtValueDb" runat="server" Width="170px" MaskSettings-Mask="<0..999999999999999999999999999g>"
                    HorizontalAlign="Right" ReadOnly="true"/> </td>
            </tr>
        </table>
        <br />
        <p>Credit to:</p>
        <table style="margin-left: 30px">
            <tr>
                <td style="width:128px">Account No</td>
                <td><dx:ASPxTextBox ID="txtAccNoCr" runat="server" Width="170px" ReadOnly="true"/> </td>
            </tr>
            <tr>
                <td>Account Name</td>
                <td><dx:ASPxTextBox ID="txtAccNameCr" runat="server" Width="170px" ReadOnly="true"/> </td>
            </tr>
            <tr>
                <td>Value</td>
                <td><dx:ASPxTextBox ID="txtValueCr" runat="server" Width="170px" MaskSettings-Mask="<0..999999999999999999999999999g>"
                    HorizontalAlign="Right" ReadOnly="true"/> </td>
            </tr>
        </table>
        <br />
        <br />
        <p>Are you sure ?</p>
        <br />
        <table>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" />                        
                </td>
                <td>
                    <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" />
                </td>
                           
            </tr>
        </table>
    </div>
</asp:Content>
