﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.balanceWithdrawalWS;
using System.Diagnostics;
using Common.wsUserGroupPermission;
using log4net;
using log4net.Config;

namespace imq.kpei.derivatif
{
    public partial class withdrawal : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(_Default));
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();
            log.Debug("USER : " + aUserLogin);
            log.Debug("MEMBER : " + aUserMember);
            if (!IsPostBack)
            {
                hdWithdrawal.Set("stat", "");
                gvWithdrawal.DataBind();

                //                UiDataService uds = ImqWS.GetTransactionService();
                string memberId = Session["SESSION_USERMEMBER"].ToString().Trim().ToLower();
                hdWithdrawal.Set("srcmember", "");
                if (memberId.Equals("kpei"))
                {
                    btnNew.Visible = false;
                    btnCancel.Visible = false;
                    hdWithdrawal.Set("srcmember", "");
                }
                else
                {
                    hdWithdrawal.Set("srcmember", aUserMember);
                    cmbMember.Text = memberId.ToUpper();
                    cmbMember.Enabled = false;
                }
                //                    {
                //                        hdWithdrawal.Set("srcmember", aUserMember);
                //                        try
                //                        {
                //                            bool validate = uds.validateBWTime();
                //                            if (validate)
                //                            {
                //                                //btnNew.Enabled = true;
                //                                Permission();
                //                                btnCancel.Enabled = true;
                //                            }
                //                            else
                //                            {
                //                                btnNew.Enabled = false;
                //                                btnCancel.Enabled = false;
                //                            }
                //                            cmbMember.Text = aUserMember;
                //                            cmbMember.Enabled = false;
                //                            cmbMember.DropDownButton.Visible = false;
                //                        }
                //                        catch (Exception ex)
                //                        {

                //                            string mystring = @"<script type='text/javascript'>
                //                                jAlert('" + ex.Message + @"','Information');
                //                                </script>";
                //                            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                //                                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
                //                            btnNew.Enabled = false;
                //                            btnCancel.Enabled = false;
                //                        }
                //                    }
            }
        }

        private balanceWithdrawal[] getBalanceWithdrawal()
        {
            log.Info("Get Balance Withdrawal");
            BalanceWithdrawalWSService bws = null;
            balanceWithdrawal[] list = null;

            try
            {
                bws = ImqWS.GetBalanceWithdrawalService();
                if (aUserMember.Contains("kpei"))
                    list = bws.get();
                else
                    list = bws.getByMemberId(aUserMember);

                if (list != null)
                {
                    balanceWithdrawal[] listTemp = (balanceWithdrawal[]) list.Clone();
                    cmbMember.DataSource = listTemp.GroupBy(p => p.id.memberId)
                    .Select(g => g.First()).ToArray();
                    cmbMember.DataBind();

                    cmbSID.DataSource = listTemp.GroupBy(p => p.sid)
                    .Select(g => g.First()).ToArray();
                    cmbSID.DataBind();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                log.Error(e.Message);
                if (bws != null)
                    bws.Abort();
            }
            finally
            {
                if (bws != null)
                    bws.Dispose();
            }

            return list;
        }

        //private void Permission()
        //{
        //    dtFormMenuPermission[] dtFMP;

        //    UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
        //    dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Balance Withdrawal");
        //    for (int n = 0; n < dtFMP.Length; n++)
        //    {
        //        if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
        //            EnableButton(true);
        //        else
        //            EnableButton(false);
        //    }
        //}

        //private void EnableButton(bool enable)
        //{
        //    btnNew.Enabled = enable;
        //}

        protected void btnGO_Click(object sender, EventArgs e)
        {
            log.Info("Button Go Click");
            string member = cmbMember.Text.Trim();
            if (!aUserMember.Contains("kpei"))
                member = aUserMember;

            string sid = cmbSID.Text.Trim();
            string search = string.Empty;
            string date = string.Empty;
            if (deDate.Date.ToString() != "1/1/0001 12:00:00 AM")
            {
                DateTime dttemp = deDate.Date.AddDays(1);
                date = string.Format("[bwDate] >= #{0}# and [bwDate] < #{1}#", 
                    deDate.Date.ToString(System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat.ShortDatePattern),
                    dttemp.Date.ToString(System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat.ShortDatePattern));
            }
            string stat;
            if (cmbStatus.Value != null)
                stat = cmbStatus.Value.ToString();
            else
                stat = string.Empty;

            hdWithdrawal.Set("srcmember", member);
            hdWithdrawal.Set("stat", stat);
            if (member != string.Empty)
            {
                member = member.Replace("'", "''");
                member = member.Replace("--", "");
                member = String.Format("[id.memberId] like '%{0}%'", member);
            }

            if (sid != string.Empty)
            {
                sid = sid.Replace("'", "''");
                sid = sid.Replace("--", "");
                sid = String.Format("[sid] like '%{0}%'", sid);
            }

            if (stat != string.Empty)
            {
                stat = stat.Replace("'", "''");
                stat = stat.Replace("--", "");
                stat = String.Format("[status] = '{0}'", stat);
            }

            if (member != string.Empty)
            {
                if (sid != string.Empty)
                {
                    if (stat != string.Empty)
                    {
                        if (date != string.Empty)
                            search = String.Format("{0} and {1} and {2} and {3}", member, sid, stat, date);
                        else
                            search = String.Format("{0} and {1} and {2}", member, sid, stat);
                    }
                    else
                    {
                        if (date != string.Empty)
                            search = String.Format("{0} and {1} and {2}", member, sid, date);
                        else
                            search = String.Format("{0} and {1} ", member, sid);
                    }
                }
                else
                {
                    if (stat != string.Empty)
                    {
                        if (date != string.Empty)
                            search = String.Format("{0} and {1} and {2} ", member, stat, date);
                        else
                            search = String.Format("{0} and {1} ", member, stat);
                    }
                    else
                    {
                        if (date != string.Empty)
                            search = String.Format("{0} and {1}", member, date);
                        else
                            search = member;
                    }
                    
                }
            }
            else
            {
                if (sid != string.Empty)
                {
                    if (stat != string.Empty)
                    {
                        if (date != string.Empty)
                            search = String.Format("{0} and {1} and {2}", sid, stat,date);
                        else
                            search = String.Format("{0} and {1}", sid, stat);
                    }
                    else
                        if (date != string.Empty)
                            search = String.Format("{0} and {1}", sid, date);
                        else
                            search = sid;                        
                }
                else
                {
                    if (stat != string.Empty)
                    {
                        if (date != string.Empty)
                            search = String.Format("{0} and {1}", stat, date);
                        else
                            search = stat;
                    }
                    else
                    {
                        if (date != string.Empty)
                            search = date;
                        else
                            search = string.Empty;
                    }
                }
            }

            log.Debug(search);
            gvWithdrawal.FilterExpression = search;
        }

        protected void gvWithdrawal_DataBinding(object sender, EventArgs e)
        {
            gvWithdrawal.DataSource = getBalanceWithdrawal();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            log.Info("btnNew_Click");
            int rowCount = gvWithdrawal.VisibleRowCount;
            string redirect = string.Concat("withdrawalform.aspx?stat=0&rowcount=", rowCount);
            log.Debug("Response Redirect : " + redirect);
            Response.Redirect(redirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            log.Info("btnCancel_Click");
            if (!hdWithdrawal.Get("status").ToString().Equals("W") || !hdWithdrawal.Get("status").ToString().Equals("C"))
            {
                string strRedirect = String.Format("withdrawalform.aspx?stat=4&bwNo={0}", hdWithdrawal.Get("bwNo").ToString().Trim());
                log.Info("Response Redirect : " + strRedirect);
                Response.Redirect(strRedirect, false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }

        protected void gvWithdrawal_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.VisibleIndex == 0)
                e.DisplayText = (e.VisibleRowIndex + 1).ToString();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }
    }
}
