﻿<%@ Page Title="Liquidation" Language="C#" AutoEventWireup="true" CodeBehind="LiquidationPage.aspx.cs" Inherits="imq.kpei.derivatif.liquidationPage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
    //<![CDATA[
        function view(s, e) {        
            window.open("LiquidationView.aspx?series=" + cmbContract.GetText() + "&member=" + hfliquidation.Get("UserMember") + "&sid=" + cmbSid.GetText(), "", "fullscreen=yes;scrollbars=auto");
        }
    //]]>
    </script>
    <div class="content">
        <div class="title">Liquidation</div>
        <table>
            <tr>                
                <td nowrap="nowrap">Series Contract</td>
                <td>
                    <dx:ASPxComboBox ID="cmbContract" runat="server" IncrementalFilteringMode="StartsWith"
                    DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbContract"
                    TextField="series" ValueField="series" EnableCallbackMode="true" CallbackPageSize="10">
                        
                        <DropDownButton Visible="False">
                        </DropDownButton>
                    </dx:ASPxComboBox>
                </td>
                <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" 
                        onclick="btnSearch_Click" />                        
                </td>
                <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                        <ClientSideEvents Click="view"/>
                    </dx:ASPxButton></td> 
            </tr>
            <tr>
                <td nowrap="nowrap">Member ID </td>
                <td><dx:ASPxComboBox ID="cmbMember" runat="server" DropDownRows="10" IncrementalFilteringMode="StartsWith"
                    DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbMember" 
                        ClientEnabled="True" EnableClientSideAPI="True">
                    <DropDownButton Visible="false" />
                    </dx:ASPxComboBox>
                </td>
                
            </tr>
            <tr>
                <td>SID</td>
                <td><dx:ASPxComboBox ID="cmbSID" runat="server" DropDownRows="10" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                     Width="170px" ClientInstanceName="cmbSid" >
                     <DropDownButton Visible="false" />
                    </dx:ASPxComboBox> </td>
            </tr>
        </table>

        <br />     

        <dx:ASPxGridView ID="gvLiquidation" runat="server" AutoGenerateColumns="False" CssClass="grid"
        KeyFieldName="id.liquidationNo" ondatabinding="gvLiquidation_DataBinding">
            <Columns>
                <dx:GridViewBandColumn Caption="Default" VisibleIndex="0" Name="colDefault">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" FieldName="defMemberId"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Role" VisibleIndex="1" FieldName="defRole"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="2" FieldName="defSID"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Buy/<br />Sell" VisibleIndex="3" FieldName="defPos"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="Opposite" VisibleIndex="1" Name="colOpposite">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" FieldName="oppMemberId"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Role" VisibleIndex="1" FieldName="oppRole"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="2" FieldName="oppSID"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Buy/<br />Sell" VisibleIndex="3" FieldName="oppPos"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="2" 
                    FieldName="memberId" Name="colMemberId" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Role" VisibleIndex="3" FieldName="role" 
                    Name="colRole" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="4" FieldName="sid" 
                    Name="colSid" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Buy/<br /> Sell" VisibleIndex="5" FieldName="pos" Name="colPos" Visible="false"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="6" FieldName="id.liquidationDate">
                    <PropertiesDateEdit EditFormat="Custom" DisplayFormatString="dd/MM/yyyy" ></PropertiesDateEdit>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTimeEditColumn Caption="Time" VisibleIndex="7" FieldName="id.liquidationDate">
                    <PropertiesTimeEdit DisplayFormatString="HH:mm"></PropertiesTimeEdit>
                </dx:GridViewDataTimeEditColumn>  
                <dx:GridViewDataTextColumn Caption="Contract ID" VisibleIndex="8" FieldName="series"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Contract <br />Type(O/F)" VisibleIndex="9" FieldName="contractType"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Quantity" VisibleIndex="10" FieldName="quantity"></dx:GridViewDataTextColumn>  
                <dx:GridViewDataTextColumn Caption="liquidationNo" VisibleIndex="11" FieldName="id.liquidationNo" Visible="false"></dx:GridViewDataTextColumn>                       
            </Columns>
            <Styles>
                <AlternatingRow Enabled="True" />
                <Header CssClass="gridHeader" />
            </Styles>    
            <SettingsPager PageSize="20">
            </SettingsPager>
                
        </dx:ASPxGridView>
        <dx:ASPxHiddenField ID="hfLiquidation" runat="server" ClientInstanceName="hfliquidation">
        </dx:ASPxHiddenField>
    </div>
        
</asp:Content>