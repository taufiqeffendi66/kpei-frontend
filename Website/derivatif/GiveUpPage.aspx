﻿<%@ Page Title="Give Up" Language="C#" AutoEventWireup="true" CodeBehind="GiveUpPage.aspx.cs" Inherits="imq.kpei.derivatif.giveUpPage" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
   <script language="javascript" type="text/javascript">
        // <![CDATA[
           function OnGridFocusedRowChanged() {
               grid.GetRowValues(grid.GetFocusedRowIndex(), 'guMemberId;tuMemberId;', OnGetRowValues);

           }
           function OnGetRowValues(values) {
               hfGu.Set("guMemberId", values[0]);
               hfGu.Set("tuMemberId", values[1]);
           }
           function view(s, e) {
               window.open("GiveUpView.aspx?member=" + cmbMember.GetText(), "", "fullscreen=yes;scrollbars=auto");
           }
        //]]>
   </script>  

    <div class="content">
    <div class="title"><h3>Give Up</h3></div>
    <table id="tblsearch">
            <tr>
                <td>
                    <dx:ASPxComboBox ID="cmbMember" runat="server" IncrementalFilteringMode="StartsWith"
                    DropDownRows="10" DropDownStyle="DropDown" Width="170px" ClientInstanceName="cmbMember"                    
                    EnableClientSideAPI="true" MaxLength="5" EnableCallbackMode="true" CallbackPageSize="10">
                    <DropDownButton Visible="false" />
                    </dx:ASPxComboBox> 
                </td>
                <td><dx:ASPxButton ID="btnSearch" runat="server" Text="Search" 
                        onclick="btnSearch_Click" /></td>
                
            </tr>                    
        </table>           
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />                        
            </td>
            <td>
                <dx:ASPxButton ID="btnDetail" runat="server" Text="Detail" Visible="false"
                    onclick="btnDetail_Click" />
            </td>
            <td><dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                        <ClientSideEvents Click="view"/>
                    </dx:ASPxButton>
                </td>
        </tr>
    </table>
            
    <br />
    
    <dx:ASPxGridView ID="gvGiveUp" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" 
            CssClass="grid" KeyFieldName="guNo" ondatabinding="gvGiveUp_DataBinding" EnableCallBacks="true">
        <Columns>
            <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="0" FieldName="guDate">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTimeEditColumn Caption="Time" VisibleIndex="1" FieldName="guDate">
                <PropertiesTimeEdit DisplayFormatString="HH:mm" />
            </dx:GridViewDataTimeEditColumn>
            <dx:GridViewBandColumn Caption="Give Up" VisibleIndex="2">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" FieldName="guNo" />
                    <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="1" FieldName="guMemberId"/>                
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="Take Up" VisibleIndex="3">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" FieldName="tuMemberId" />
                    <dx:GridViewDataTextColumn Caption="Approval" VisibleIndex="1" FieldName="tuApprovalDescription"/>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="KPEI <br />Approval" VisibleIndex="4" FieldName="kpeiApprovalDescription" />
            <dx:GridViewBandColumn Caption="Execution" VisibleIndex="5">
                <Columns>
                    <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="0" FieldName="executionTime">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTimeEditColumn Caption="Time" VisibleIndex="1" FieldName="executionTime">
                        <PropertiesTimeEdit DisplayFormatString="HH:mm" />
                    </dx:GridViewDataTimeEditColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="6" FieldName="statusDescription" />
            <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="7" FieldName="status" Visible="false" />
            <dx:GridViewDataTextColumn Caption="Kpei Approval" VisibleIndex="8" FieldName="kpeiApproval" Visible="false" />
            <dx:GridViewDataTextColumn Caption="Take Up Approval" VisibleIndex="7" FieldName="tuApproval" Visible="false" />
        </Columns>
        <Settings ShowVerticalScrollBar="True" />
        <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="true" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="detailGrid" runat="server" KeyFieldName="id.sid" Width="100%"
                    ondatabinding="detailGrid_DataBinding" OnLoad="detailGrid_OnLoad" OnBeforePerformDataSelect="detailGrid_DataSelect">
                   <Columns>
                        <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="0" FieldName="id.sid"/>
                        <dx:GridViewDataTextColumn Caption="Trading ID" VisibleIndex="1" FieldName="tradingId"/>
                        <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" FieldName="guNo" Visible="false"/>
                   </Columns>
                   <Templates>                   
                        <DetailRow>
                            <dx:ASPxGridView ID="detailNet" runat="server" KeyFieldName="sid" Width="100%"
                            ondatabinding="detailNet_DataBinding" OnLoad="detailNet_OnLoad" OnBeforePerformDataSelect="detailNet_DataSelect">
                            <Columns>                        
                                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="0" FieldName="sid" Visible="false"/>
                                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="1" FieldName="memberID" Visible="false"/>
                                <dx:GridViewDataTextColumn Caption="Series Contract" VisibleIndex="2" FieldName="series"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Net Buy" VisibleIndex="3" FieldName="netB"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Net Sell" VisibleIndex="3" FieldName="netS"></dx:GridViewDataTextColumn>
                           </Columns>
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="true" />
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True"  />
        <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" /> 
        <Styles>
            <AlternatingRow Enabled="True" />
            <Header CssClass="gridHeader" />
        </Styles>
        <SettingsPager PageSize="20">
        </SettingsPager>
    </dx:ASPxGridView>
    

    </div>
    <dx:ASPxHiddenField ID="hfGu" runat="server" ClientInstanceName="hfGu" />
    
</asp:Content>
