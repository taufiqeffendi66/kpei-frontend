﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using Common.memberInfoWS;
using Common.wsUserGroupPermission;

namespace imq.kpei.derivatif
{
    public partial class member : System.Web.UI.Page
    {
        private memberInfo[] memberInfos;
        private String aUserLogin;
        private String aUserMember;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                gvMemberList.DataBind();
                Permission();
            }
        }

        protected void gvMemberList_DataBinding(object sender, EventArgs e)
        {
            gvMemberList.DataSource = getMember();
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Member");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editMaker || dtFMP[n].editDirectChecker || dtFMP[n].editDirectApproval)
                    EnableButton(true);
                else
                    EnableButton(false);
                btnDelete.Enabled = dtFMP[n].removeing;
            }
        }

        private void EnableButton(bool enable)
        {
            btnEdit.Enabled = enable;
            btnNew.Enabled = enable;
        }

        private memberInfo[] getMember()
        {
            MemberInfoWSService mis = null;
            try
            {
                mis = ImqWS.GetMemberInfoWebService();
                memberInfos = mis.get();
                if (memberInfos.Length > 0)
                {
                    foreach (memberInfo m in memberInfos)
                    {
                        if (!IsPostBack)
                        {
                            cmbMember.Items.Add(m.memberId, m.memberId);
                            cmbName.Items.Add(m.memberName, m.memberName);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                mis.Abort();
                Debug.WriteLine(e.ToString());
                //lblError.Text = e.Message;
            }
            finally
            {
                mis.Dispose();
            }
            return memberInfos;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("MemberEdit.aspx?stat=0", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            SendResponse("1");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SendResponse("4");
        }

        private void SendResponse(string stat)
        {
            string strRedirect = String.Format("MemberEdit.aspx?stat={0}&memberId={1}&idx={2}&name={3}&npwp={4}&address={5}&phone={6}&fax={7}&contactPerson={8}&email={9}&adminUser={10}&sktNo={11}&sktDate={12}&activeDate={13}&memberType={14}&status={15}&clearingMethod={16}&contractGroup={17}", stat, Server.UrlEncode(hfMember.Get("memberId").ToString().Trim()), Server.UrlEncode(hfMember.Get("idx").ToString().Trim()), Server.UrlEncode(hfMember.Get("name").ToString().Trim()), Server.UrlEncode(hfMember.Get("npwp").ToString().Trim()), Server.UrlEncode(hfMember.Get("address").ToString().Trim()), Server.UrlEncode(hfMember.Get("phone").ToString().Trim()), Server.UrlEncode(hfMember.Get("fax").ToString().Trim()), Server.UrlEncode(hfMember.Get("contactPerson").ToString().Trim()), Server.UrlEncode(hfMember.Get("email").ToString().Trim()), Server.UrlEncode(hfMember.Get("adminUser").ToString()), Server.UrlEncode(hfMember.Get("sktNo").ToString().Trim()), Server.UrlEncode(hfMember.Get("sktDate").ToString().Trim()), Server.UrlEncode(hfMember.Get("activeDate").ToString().Trim()), Server.UrlEncode(hfMember.Get("memberType").ToString().Trim()), Server.UrlEncode(hfMember.Get("status").ToString().Trim()), Server.UrlEncode(hfMember.Get("clearingMethod").ToString().Trim()), Server.UrlEncode(hfMember.Get("contractGroup").ToString().Trim()));
            Response.Redirect(strRedirect, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string memberId = cmbMember.Text.Trim();
            string memberName = cmbName.Text.Trim();
            string search = String.Empty;

            if (memberId != String.Empty)
            {
                memberId = memberId.Replace("'", "''");
                memberId = memberId.Replace("-", "--");
                memberId = String.Format("[memberId] like '%{0}%'", memberId);
            }
            if (memberName != String.Empty)
            {
                memberName = memberName.Replace("'", "''");
                memberName = memberName.Replace("-", "--");
                memberName = String.Format("[memberName] like '%{0}%'", memberName);
            }

            if (memberId != String.Empty)
            {
                if (memberName != String.Empty)
                    search = String.Format("{0} and {1}", memberId, memberName);
                else
                    search = memberId;
            }
            else
            {
                if (memberName != String.Empty)
                    search = memberName;
                else
                    search = String.Empty;
            }

            gvMemberList.FilterExpression = search;
        }

    }
}
