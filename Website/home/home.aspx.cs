﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Common;
using DevExpress.Web.ASPxTreeView;
using imq.kpei.wsUser;
using imq.kpei.wsUserGroupPermission;

namespace imq.kpei
{
    public partial class home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ImqSession.ValidateAction(this);
                //dtUserSkdMain  dtUSM = new wsUser.UserService().SearchByUserAndMember(Session["USER_ID"].ToString(), Session["USER_MEMBER"].ToString()  ) ;

                string jmsServer = ImqSession.GetConfig("JMS_SERVER_WS");
                string infoTopic = ImqSession.GetConfig("JMS_TOPIC_INFO");
                string user = Session["SESSION_USER"].ToString();
                string password = Session["SESSION_USERPASSWORD"].ToString();
                string member = Session["SESSION_USERMEMBER"].ToString();

                if (user != "" & member != "" & password !="")
                {
                    UserService wsU = new UserService();
                    dtUserSkdMain dtUSM = wsU.SearchByUserAndMember(user, member);
 
                    UserGroupPermissionService wsUGP = new UserGroupPermissionService();

                    dtMenuList[] dtML = wsUGP.getMenu();
                    for (int n = 0; n < dtML.Length; n++)
                    {
                        dtUserGroupPermission[] dtUGP = wsUGP.ViewByGroupIdAndMenuId(dtUSM.groupId, dtML[n].idMenu);
                        TreeViewNode parentNode;
                        TreeViewNode newNode;
                        if (dtUGP != null)
                        {
                            parentNode = treeMenu.Nodes.Add();
                            parentNode.Text = dtML[n].nameMenu;
                            for (int m = 0; m < dtUGP.Length; m++)
                            {
                                if (dtUGP[m].reading)
                                {
                                    newNode = parentNode.Nodes.Add();
                                    newNode.Text = dtUGP[m].formList.name;
                                    newNode.Name = dtUGP[m].formList.url+"?id="+dtUGP[m].id;
                                }
                            }

                        }
                    }
                }


                ScriptManager.RegisterStartupScript(this, this.GetType(), "initPage", @"                    
<script type=""text/javascript"" src=""../../js/util/page.js""></script>
<script type=""text/javascript"" src=""js/amq/stomp.js""></script>
<script type=""text/javascript"">
    function log(s) {
        infoList.InsertItem(0, s);
        if (infoList.GetItemCount() > 10)
            infoList.RemoveItem(infoList.GetItemCount() - 1);
    }

    window.onbeforeunload = function() {
        client.disconnect(function() {
            log('Disconnect client from JMS Server');
        });
    }

    log('Start');

    var url = """ + jmsServer + @""";
    var login = """ + user + @""";
    var passcode = """ + password + @""";
    var destination = """ + infoTopic + @""";

    log('Init client: ' + url);
    var client = Stomp.client(url);
    client.debug = function(str) {
        log(str);
    };

    var onconnect = function(frame) {
        client.debug('Connected to Stomp');
        client.subscribe(destination, function(messagexxx) {
            log(messagexxx.body);
          });
        };
    log('Client connect: ' + url);
    client.connect(login, passcode, onconnect);
</script>

                    }", false);
            }
        }

        protected void treeMenu_NodeClick(object source, DevExpress.Web.ASPxTreeView.TreeViewNodeEventArgs e)
        {
            Session["MENU_CLICK"] = true;

            //TODO get Content URL from database or something
            if (e.Node.Text == "Welcome")
                mainSplitter.GetPaneByName("contentPane").ContentUrl = "~/Welcome.aspx";
            //Navigation.Navigate(this, "Welcome.aspx");
            else if (e.Node.Text == "Trade")
                mainSplitter.GetPaneByName("contentPane").ContentUrl = "~/transaction/trade/trade.aspx";
        }

        protected void buttonLogout_Click(object sender, EventArgs e)
        {
            ImqSession.Logout(this);
        }
    }
}
