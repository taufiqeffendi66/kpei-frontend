﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="imq.kpei.home"
    StylesheetTheme="" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sistem Kliring Derivatif</title>
    <link href="Styles/home/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:ASPxSplitter ID="mainSplitter" runat="server" FullscreenMode="True" Width="100%"
            Height="100%" Orientation="Vertical" SeparatorSize="0px" SeparatorVisible="False"
            ShowSeparatorImage="False" ClientInstanceName="mainSplitter">
            <Panes>
                <dx:SplitterPane Name="headerPane" Size="50px" PaneStyle-BackColor="#DBF4DE" AllowResize="False">
                    <PaneStyle BackColor="#DBF4DE" Border-BorderStyle="None" CssClass="header_pane">
                        <Paddings Padding="0px" />
                        <Border BorderStyle="None"></Border>
                    </PaneStyle>
                    <ContentCollection>
                        <dx:SplitterContentControl ID="headerPane" runat="server">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="logo">
                                            <img src="Styles/home/images/logo.png" alt="logo" />
                                        </div>
                                        <div class="user_menu">
                                            <ul>
                                                <li>
                                                    <img src="Styles/home/images/acc_icon.png" alt="acc" /><a href="#">My Account</a></li>
                                                <li>
                                                    <img src="Styles/home/images/logout_icon.png" alt="acc" /><asp:LinkButton ID="buttonLogout"
                                                        runat="server" OnClick="buttonLogout_Click">Logout</asp:LinkButton></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
                <dx:SplitterPane>
                    <Panes>
                        <dx:SplitterPane Name="menuPane" Size="200px" AllowResize="False" ScrollBars="Auto">
                            <PaneStyle Border-BorderStyle="None">
                                <Paddings Padding="5px" />
                                <Border BorderStyle="None"></Border>
                            </PaneStyle>
                            <ContentCollection>
                                <dx:SplitterContentControl ID="menuPane" runat="server">
                                    <dx:ASPxTreeView ID="treeMenu" runat="server" AllowSelectNode="True" Width="100%"
                                        ClientInstanceName="treeMenu" Height="100%">
                                        <ClientSideEvents NodeClick="
function(s, e) {
    var selectedURL = e.node.name;
    if ((selectedURL != null) || (selectedURL != '')) {
        var paneContent = mainSplitter.GetPaneByName('contentPane');
        paneContent.SetContentUrl(selectedURL.toString());
        paneContent.RefreshContentUrl();                   
    }                     
}" />
                                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                            <NodeLoadingPanel Url="~/App_Themes/DevEx/Web/tvNodeLoading.gif">
                                            </NodeLoadingPanel>
                                            <LoadingPanel Url="~/App_Themes/DevEx/Web/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                        </Styles>
                                    </dx:ASPxTreeView>
                                </dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>
                        <dx:SplitterPane>
                            <Panes>
                                <dx:SplitterPane Name="contentPane" ContentUrl="~/welcome.aspx" ContentUrlIFrameName="contentPane"
                                    ScrollBars="Auto">
                                    <PaneStyle Border-BorderStyle="None">
                                        <Paddings Padding="0px" PaddingLeft="5px" />
                                        <Border BorderStyle="None"></Border>
                                    </PaneStyle>
                                    <ContentCollection>
                                        <dx:SplitterContentControl ID="contentPane" runat="server">
                                        </dx:SplitterContentControl>
                                    </ContentCollection>
                                </dx:SplitterPane>
                            </Panes>
                            <PaneStyle>
                                <Paddings PaddingBottom="0px" PaddingLeft="5px" PaddingRight="5px" PaddingTop="0px" />
                            </PaneStyle>
                            <ContentCollection>
                                <dx:SplitterContentControl runat="server">
                                </dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>
                    </Panes>
                    <ContentCollection>
                        <dx:SplitterContentControl runat="server">
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
                <dx:SplitterPane Name="footerPane" Size="120px">
                    <PaneStyle>
                        <Paddings Padding="0px" PaddingLeft="5px" PaddingRight="5px" />
                        <Border BorderStyle="None" />
                    </PaneStyle>
                    <ContentCollection>
                        <dx:SplitterContentControl runat="server">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td width="50%">
                                        <div style="border: 1px solid #d2d2d2; height: 115px; background-color: #efefef;">
                                            <div>
                                                <h3>
                                                    News</h3>
                                                <dx:ASPxListBox ID="newsList" runat="server" BackColor="#EFEFEF" ClientInstanceName="newsList"
                                                    EnableCallbackMode="True" EnableClientSideAPI="True" Height="83px" Width="100%">
                                                    <Border BorderStyle="None" />
                                                </dx:ASPxListBox>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td width="50%">
                                        <div style="border: 1px solid #d2d2d2; height: 115px; background-color: #efefef;">
                                            <div>
                                                <h3>
                                                    Informasi</h3>
                                                <dx:ASPxListBox ID="infoList" runat="server" BackColor="#EFEFEF" ClientInstanceName="infoList"
                                                    EnableCallbackMode="True" EnableClientSideAPI="True" Height="83px" Width="100%">
                                                    <Border BorderStyle="None" />
                                                </dx:ASPxListBox>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
            </Panes>
        </dx:ASPxSplitter>
    </div>
    </form>
</body>
</html>
