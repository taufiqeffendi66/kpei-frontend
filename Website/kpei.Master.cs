﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace imq.kpei
{
    public partial class kpei : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SESSION_USERMEMBER"] == null)
            {
                Response.Write("<script>window.open('login.aspx','_top')</script>");
            }
        }
    }
}
