﻿using System;
using System.IO;
using Common;
using Common.wsUser;
using log4net;

namespace imq.kpei
{
    public partial class loginPinCode : System.Web.UI.Page
    {
        private string userId;
        private string memberId;
        private const string directory = "keyFile";
        private bool isKeyFileDownload = false;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            //ImqSession.ValidateAction(this);
            
            if (!IsPostBack)
            {
                userId = Session["SESSION_USER"].ToString();
                memberId = Session["SESSION_USERMEMBER"].ToString();
                txtUser.Value = userId + memberId;

                txtPinCode.Value = "";
                txtPinCodeRe.Value = "";
                txtPassword.Value = "";
                txtPasswordConfrm.Value = "";
                Session["DowonloadedKeyFile"] = isKeyFileDownload;
                //isKeyFileDownload = Convert.ToBoolean(Session["DowonloadedKeyFile"].ToString());
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              alert(""{0}"");
                                                              window.location='{1}'
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }

        private static Boolean ValidPassword(string input)
        {
            if (input.Length < 9)
                return false;
            if ((input.ToLower() == input) || (input.ToUpper() == input))
                return false;
            foreach (char c in input)
                if (char.IsNumber(c))
                    return true;
            return false;
        }

        protected void btnKeyFile_Click(object sender, EventArgs e)
        {
            if (txtPinCode.Value.Trim() == txtPinCodeRe.Value.Trim() )
            {
                if (ValidPassword(txtPassword.Value))
                {
                    if (txtPassword.Value == txtPasswordConfrm.Value)
                    {
                        userId = Session["SESSION_USER"].ToString();
                        memberId = Session["SESSION_USERMEMBER"].ToString();

                        UserService wsU = ImqWS.GetUserService();
                        string dtKeyFile = wsU.getKeyPass(userId, memberId, txtPinCode.Value.Trim());
                        if (dtKeyFile.Equals(""))
                        {
                            ShowMessage("Can't make KeyFile", "loginPinCode.aspx");
                            log.Error("Cannot make KeyFile");
                        }
                        else
                        {
                            string fileName = CreateKeyFile(txtUser.Value, dtKeyFile);
                            
                            wsU.changePassWord(userId, memberId, txtPassword.Value.Trim());
                            if (wsU != null)
                                wsU.Dispose();

                            SendToClient(fileName);
                            
                            //Response.Redirect("loginKeyFile.aspx");
                        }
                    }
                    else
                    {
                        txtPassword.Focus();
                        ShowMessage("Password and Confrm Password not same !", "loginPinCode.aspx");
                    }
                }
                else
                    ShowMessage(" Password must be at least 9 characters long and must contain combination uppercase letter, lowercase letter and number", "loginPinCode.aspx");
            }
            else
            {
                txtPinCode.Focus();
                ShowMessage("Pincode and Confrm Pincode not same !", "loginPinCode.aspx");
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            isKeyFileDownload = Convert.ToBoolean(Session["DowonloadedKeyFile"].ToString());
            if (isKeyFileDownload)
                Response.Redirect("loginKeyFile.aspx");
            
            //UserService wsU = null;

            //try
            //{
                
                //if (txtPassword.Value.Trim() == txtPasswordConfrm.Value.Trim())
                //{
                //    Session["SESSION_CountLogin"] = 0;
                //    wsU = ImqWS.GetUserService();
                //    wsU.changePassWord(userId, memberId, txtPassword.Value.Trim());
                //    Response.Redirect("loginKeyFile.aspx");
                //}
                //else
                //    ShowMessage("Password and Confrm Password not same !", "loginPinCode.aspx");
            //}
            //catch (Exception ex)
            //{
            //    log.Error(String.Format("{0}; inner eception : {1}", ex.Message, ex.InnerException.Message));
            //    if (wsU != null)
            //        wsU.Abort();
            //}
            //finally
            //{
            //    if(wsU != null)
            //        wsU.Dispose();
            //}
        }

        protected void SendToClient(string fileName)
        {            
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
            
            //log.Info("SendToClient : " + String.Format("{0}\\{1}\\{2}", Request.PhysicalApplicationPath, directory, fileName));
            log.Info("SendToClient : " + String.Format(@"~/{0}/{1}", directory, fileName));
            Response.TransmitFile(Server.MapPath(String.Format(@"~/{0}/{1}", directory, fileName)));
            Session["DowonloadedKeyFile"] = true;
            Response.End();
        }

        protected string CreateKeyFile(string userIdMemberId, string aKeyFile)
        {            
            if (!Directory.Exists(Server.MapPath(String.Format(@"~/{0}/", directory))))
                Directory.CreateDirectory(Server.MapPath(String.Format(@"~/{0}/", directory)));

            string fileName = String.Format("{0}.ikf", userIdMemberId);

            using (StreamWriter _testData = new StreamWriter(Server.MapPath(
                            String.Format(@"~/{0}/{1}", directory, fileName)
                            )
                            )
                            )
            {
                log.Info("CreateKeyFile :" + String.Format(@"~/{0}/{1}", directory, fileName));
                _testData.WriteLine(aKeyFile); // Write the file.
            }
           
            return fileName;
        }

        private static void putAuditTrail(String aUser, String aModule, String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUser, aModule, aActifity, aMemo);
        }
    }
}
