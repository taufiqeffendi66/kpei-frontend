﻿using System;
using System.IO;
using System.Web.UI;
using Common;
using Common.wsUser;
namespace imq.kpei
{
    public partial class loginKeyFile : Page
    {
        private string userId;
        private string memberId;
        private int countLogin;
        //private string pinCode;
        protected void Page_Load(object sender, EventArgs e)
        {
            userId = Session["SESSION_USER"].ToString();
            memberId = Session["SESSION_USERMEMBER"].ToString();
            txtUser.Value = userId + memberId;
            if (Session["SESSION_CountLogin"] == null)
                countLogin = 1;
            else
                countLogin = (int)Session["SESSION_CountLogin"];

            if (!IsPostBack)
            {
                lblError.Text = "";
                txtPassword.Value = "";
                txtPasswordNew.Value = "";
            }
        }

//        protected void fileUpload()
//        {
//            if (FileUpload1.HasFile)
//                try
//                {
//
//                    FileUpload1.SaveAs(String.Format("{0}//{1}", Server.MapPath("Uploads"), FileUpload1.FileName));
//                    //Label1.Text = "File name: " +FileUpload1.PostedFile.FileName + "<br>" +
//                    //     FileUpload1.PostedFile.ContentLength + " kb<br>" +
//                    //     "Content type: " +
//                    //     FileUpload1.PostedFile.ContentType;
//                }
//                catch (Exception)
//                {
//                    //Label1.Text = "ERROR: " + ex.Message.ToString();
//                }
//            else
//            {
//                Label1.Text = "You have not specified a file.";
//            }
//        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              alert('{0}');
                                                              window.location='{1}'
                                                              </script>", info, targetPage);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }
                
        private static Boolean ValidPassword(string input)
        {
            if (input.Length < 9 || input.ToLower() == input || input.ToUpper() == input)
                return false;
            foreach (char c in input)
                if (char.IsNumber(c))
                    return true;
            return false;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            UserService wsU = null;
            //fileUpload();
            if (!FileUpload1.HasFile)
            {
                ShowMessage(" You have not specified a file.", "loginKeyFile.aspx");
            }
            else
            {
                try
                {
                    StreamReader sr = new StreamReader(FileUpload1.FileContent);
                    //StreamReader sr = new StreamReader(String.Format("{0}//{1}", Server.MapPath("Uploads"), FileUpload1.FileName));

                    //string keyFile = sr.ReadLine();
                    string keyFile = sr.ReadToEnd();
                    sr.Close();
                    sr.Dispose();

                    if (ImqSession.checkKeyfile(userId,
                            memberId,
                            keyFile))
                    {
                        userId = Session["SESSION_USER"].ToString();
                        memberId = Session["SESSION_USERMEMBER"].ToString();

                        if (ImqSession.Login(this, userId, memberId, txtPassword.Value.Trim(), keyFile))
                        {
                            if (txtPasswordNew.Value.Trim() != "")
                            {
                                if (ValidPassword(txtPasswordNew.Value))
                                {
                                    if (txtPasswordNew.Value == txtPasswordConfrm.Value)
                                    {
                                        wsU = ImqWS.GetUserService();
                                        wsU.changePassWord(userId, memberId, txtPasswordNew.Value);
                                        Response.Redirect("home.aspx");
                                    }
                                    else
                                        ShowMessage(ImqSession.GetConfig("msgLoginKeyFileChangePass"), "loginKeyFile.aspx");
                                }
                                else
                                    ShowMessage(" Password must be at least 9 characters long and must contain combination uppercase letter, lowercase letter and number", "loginKeyFile.aspx");
                            }
                            else
                                Response.Redirect("home.aspx");
                        }

                        else
                        {
                            if (countLogin == 2)
                            {
                                wsU = ImqWS.GetUserService();
                                wsU.BlockedUser(memberId, userId);
                                ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Alert", "alert('User Blocked')", true);
                                ShowMessage(ImqSession.GetConfig("msgLoginKeyFileUserBlocked"), "login.aspx");
                                //Response.Redirect("login.aspx");
                            }
                            else
                            {
                                countLogin += 1;
                                //Session["SESSION_USER"] = userId;
                                //Session["SESSION_USERMEMBER"] = memberId;
                                Session["SESSION_CountLogin"] = countLogin;
                                ShowMessage(ImqSession.GetConfig("msgLoginKeyFilePasswordWrong"), "loginKeyFile.aspx");
                            }
                            txtUser.Focus();
                        }
                    }
                    else
                        ShowMessage(ImqSession.GetConfig("msgLoginKeyFileKeyfileWrong"), "loginKeyFile.aspx");
                }
                catch (IOException)
                {
                    if (wsU != null)
                        wsU.Abort();
                }
                finally
                {
                    //File.Delete(String.Format("{0}//{1}", Server.MapPath("Uploads"), FileUpload1.FileName));
                    if (wsU != null)
                        wsU.Dispose();
                }
            }
        }

      
    }
}
