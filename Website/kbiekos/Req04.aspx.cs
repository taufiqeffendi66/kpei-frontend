﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Collections;
using System.Web.Script;
using System.Web.Script.Serialization;

namespace imq.kpei.kbiekos
{

    public partial class Req04 : System.Web.UI.Page
    {
        private indikator[] chart;
        int x = 0;
        public int temp = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonPostback_Click(object sender, EventArgs e)
        {
        }

        protected void dropdownlistOptions_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btCancel_Click(object sender, EventArgs e)
        {

        }

        protected void btOK_Click(object sender, EventArgs e)
        {

        }

        protected void btShowModal_Click(object sender, EventArgs e)
        {

        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {


            ClientScriptManager cs = Page.ClientScript;
            string csName = "notifyMe";
            Type csType = this.GetType();
            for (int i = 0; i < 2; i++)
            {
                string currentName = string.Format("{0}{1}", csName, i);
                if (!cs.IsStartupScriptRegistered(csType, currentName))
                {
                    x = i + 1;
                    //string csText = string.Format("notifyMe('" + str1[i] + "','" + str2[i] + "','" + str3[i] + "','" + x + "');");
                    //cs.RegisterStartupScript(csType, currentName, csText, true);
                }
            }
        }

        private indikator[] getIndikator()
        //private void getData()
        {
            var url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "indikatoralert";
            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/json";
            // invoke the REST method
            byte[] data = client.DownloadData(url);
            // put the downloaded data in a memory stream
            MemoryStream ms = new MemoryStream();
            ms = new MemoryStream(data);
            // deserialize from json
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<indikator>));
            List<indikator> result = ser.ReadObject(ms) as List<indikator>;

            chart = result.ToArray();

            temp = chart.Length;
           // Label1.Text = result[0].actualValue.ToString();

            return chart;
        }

        protected void ASPxTimer1_Tick1(object sender, EventArgs e)
        {
            
            getNotify();
        }

        protected void tim_Tick(object sender, EventArgs e)
        {
        }

        private void getNotify()
        {
            getIndikator();
            temp = 3;
            
            
            string[] str1 = new string[temp];
            string[] str2 = new string[temp];
            string[] str3 = new string[temp];


            for (x = 0; x < temp; x++)
            {
                str1[x] = chart[x].memberId.ToString();
                str2[x] = chart[x].sid.ToString();
                str3[x] = chart[x].actualValue.ToString();
            }

            for (int i = 0; i < temp; i++)
            {
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify" + i, "notifyMe('Req04','" + str2[i] + "','Req04','" + x + "');", true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "notifyMe", "notifyMe('test2');", true);
            }

             
        }
    }
}