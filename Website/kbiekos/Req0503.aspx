﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true"
    CodeBehind="Req0503.aspx.cs" Inherits="imq.kpei.kbiekos.Req0503" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">
            <dx:ASPxLabel ID="lbltitle" runat="server" Text="Broadcast E-Mail" />
        </div>
        <table>
            <tr>
                <td style="width: 128px">
                    Penerima<span style="color: Red">*</span>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txtPenerima" runat="server" Width="170px" EnableClientSideAPI="true"
                        ClientInstanceName="txpenerima" MaxLength="10">
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 128px">
                    Subjek<span style="color: Red">*</span>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txtSubjek" runat="server" Width="170px" EnableClientSideAPI="true"
                        ClientInstanceName="txtsubjek">
                        <ValidationSettings ErrorText="Required">
                            <RequiredField IsRequired="true" ErrorText="Required" />
                        </ValidationSettings>
                        <ClientSideEvents Validation="OnValidation" />
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Isi Pesan
                </td>
                <td>
                    <dx:ASPxMemo ID="memoBranch" runat="server" Width="170px" Height="60px" />
                </td>
            </tr>
        </table>
        <br />
        <div>
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnSave" runat="server" Text="Save" AutoPostBack="false"
                            Width="70px" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
        
        </div>
    </div>
</asp:Content>
