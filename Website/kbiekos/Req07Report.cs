﻿using System;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.bankWS;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace imq.kpei.kbiekos
{
    public partial class Req07Report : DevExpress.XtraReports.UI.XtraReport
    {
        private AccountSidAudittrail[] audit;
        public String hari;
        public String broker;
        public String status;
        private String url;

        public Req07Report()
        {
            InitializeComponent();
        }

        private AccountSidAudittrail[] getAudit()
        {
            WebClient client = null;
            try
            {
                string date = hari;
                DateTime? dt;
                if (hari != "")
                    dt = Convert.ToDateTime(date);
                else
                    dt = null;


                if (broker != "null")
                {
                    if(hari != "")
                        url = "http://192.168.138.19:8090/accountsidaudittrail?logdate=" + hari + "&sid=" + broker;
                    else
                        url = "http://192.168.138.19:8090/accountsidaudittrail?sid=" + broker;
                }
                else if (hari != "")
                {
                    url = "http://192.168.138.19:8090/accountsidaudittrail?logdate=" + hari;
                }
                else
                {
                    url = "http://192.168.138.19:8090/accountsidaudittrail";
                }

                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<AccountSidAudittrail>));
                List<AccountSidAudittrail> result = ser.ReadObject(ms) as List<AccountSidAudittrail>;

                audit = result.ToArray();

                string str = dt != null ? dt.Value.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) : "n/a";

                if (str == "n/a" )
                    lbTitle.Text = "Validasi, Rekonsiliasi Data Account, SID Mapping Terbaru";
                else
                    lbTitle.Text = "Validasi, Rekonsiliasi Data Account, SID Mapping Terbaru " + str;

                DataSource = audit;

                if (client != null)
                {
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "newStr");
                    cellWaktu.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "sid");
                    cellSID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "cmCode");
                    cellcmCode.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "accountId");
                    cellAccID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "currentId");
                    cellcurId.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "status");
                    cellStatus.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "action");
                    cellAction.DataBindings.Add(binding);


                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                client.Dispose();
            }

            return audit;
        }

        private void Req0202Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            getAudit();
        }
    }
}
