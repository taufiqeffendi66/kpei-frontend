﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraReports.UI;
using Common;

namespace imq.kpei.kbiekos
{
    public partial class Req0601View : System.Web.UI.Page
    {
        public String aHari;
        public String aTitle;
        public String idReq;
        public String aKbie1;
        public String aKbie2;
        public String aKbie3;
        public String aKbie4;
        public String tgl;
        public String aBulan;
        public String aBulan2;
        public String aTahun;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            ImqSession.ValidateAction(this);
            
            if (Request.QueryString["hari"] != null)
                aHari = Request.QueryString["hari"].ToString().Trim();
            if (Request.QueryString["title"] != null)
                aTitle = Request.QueryString["title"].ToString().Trim();
            if (Request.QueryString["req"] != null)
                idReq = Request.QueryString["req"].ToString().Trim();
            if (Request.QueryString["kbie1"] != null)
                aKbie1 = Request.QueryString["kbie1"].ToString().Trim();
            if (Request.QueryString["kbie2"] != null)
                aKbie2 = Request.QueryString["kbie2"].ToString().Trim();
            if (Request.QueryString["kbie3"] != null)
                aKbie3 = Request.QueryString["kbie3"].ToString().Trim();
            if (Request.QueryString["kbie4"] != null)
                aKbie4 = Request.QueryString["kbie4"].ToString().Trim();
            if (Request.QueryString["bulan"] != null)
                aBulan = Request.QueryString["bulan"].ToString().Trim();
            if (Request.QueryString["bulan2"] != null)
                aBulan2 = Request.QueryString["bulan2"].ToString().Trim();
            if (Request.QueryString["tahun"] != null)
                aTahun = Request.QueryString["tahun"].ToString().Trim();

            DateTime dt = Convert.ToDateTime(aHari);
            tgl = dt.ToString("yyyyMMdd");

            ReportV1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            Req0601report report;
            Req0602Report report2;

            if (idReq == "060101")
            {
                idReq = "1";
                report = new Req0601report() { 
                    hari = aHari, 
                    title = aTitle, 
                    id=idReq,
                    kbie1=aKbie1,
                    kbie2=aKbie2,
                    bulan="1",
                    Name = "Rekapitulasi Perdagangan KBIE_" + tgl
                };

                return report;
            }
            else if (idReq == "060102")
            {
                idReq = "1";
                report2 = new Req0602Report() { 
                    hari = aHari, 
                    title = aTitle, 
                    id = idReq,
                    kbie1=aKbie3,
                    kbie2=aKbie4,
                    bulan="1",
                    Name = "Rekapitulasi Perdagangan KOS_" + tgl
                
                };

                return report2;
            }
            else if (idReq == "060201")
            {
                idReq = "2";
                report = new Req0601report()
                {
                    //hari = "2016-03-07",
                    title = aTitle,
                    id = idReq,
                    kbie1=aKbie1,
                    kbie2=aKbie2,
                    bulan = aBulan2,
                    tahun = aTahun,
                    Name = "Rekapitulasi Perdagangan KBIE Bulan " + aBulan + " Tahun " + aTahun
                };

                return report;
            }
            else
            {
                idReq = "2";
                report2 = new Req0602Report()
                {
                    //hari = "2016-03-07",
                    title = aTitle,
                    id = idReq,
                    kbie1 = aKbie3,
                    kbie2 = aKbie4,
                    bulan = aBulan,
                    bulan2=aBulan2,
                    tahun = aTahun,
                    Name = "Rekapitulasi Perdagangan KOS Bulan " + aBulan2 + " Tahun " + aTahun
                };

                return report2;
            }
            
            
           
        }

        
    }
}
