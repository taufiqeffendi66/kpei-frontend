﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="atest.aspx.cs" Inherits="imq.kpei.kbiekos.atest" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="up1" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="test" EventName="Tick" />
            </Triggers>
            <ContentTemplate>
                <dx:ASPxTimer ID="test" runat="server" Interval="5000" ontick="test_Tick">
                </dx:ASPxTimer>

                <asp:Label ID="label" runat="server" Text="DD/MM/YY"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
