﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true"
    CodeBehind="Req0202.aspx.cs" Inherits="imq.kpei.kbiekos.Req0202" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script language="javascript" type="text/javascript">
        // <![CDATA[
        function view(s, e) {
            window.open("reportView1.aspx" + "?kbroker=" + cmBroker.GetValue() + "&selisih=" + cmSelisih.GetValue() + "&req=0202" + "&hari=" + cmDate.GetText(), "", "");
        }

        // ]]>
    </script>
    <div id="content">
        <div class="title">
            Laporan Informasi Kewajiban Serah</div>
        <div style="float: left; margin-top: 5px">
            <table>
                <tr>
                    <td>
                        Broker Code
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbKbroker" runat="server" ValueType="System.String" DropDownStyle="DropDownList"
                            TextField="Kode Broker" OnSelectedIndexChanged="cmbKbroker_SelectedIndexChanged"
                            ClientInstanceName="cmBroker">
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <dx:ASPxDateEdit ID="dateEdit" runat="server" EditFormat="Custom" EditFormatString="yyyy-MM-dd"
                            Width="200" OnDateChanged="dateEdit_DateChanged" AllowNull="False" AllowUserInput="False"
                            ClientInstanceName="cmDate" Visible="true">
                            <TimeSectionProperties>
                                <TimeEditProperties EditFormatString="hh:mm tt" />
                            </TimeSectionProperties>
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <%-- <dx:ASPxLabel ID="dtID" ClientInstanceName="clID" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>--%>
                        <dx:ASPxLabel ID="Label1" runat="server" Text="ASPxLabel" Visible="false" ClientInstanceName="cmDate">
                        </dx:ASPxLabel>
                    </td>
                     <td>
                         &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Selisih
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cmbSelisih" runat="server" ValueType="System.String" DropDownStyle="DropDownList"
                            TextField="Selisih" OnSelectedIndexChanged="cmbSelisih_SelectedIndexChanged"
                            ClientInstanceName="cmSelisih">
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click">
                        </dx:ASPxButton>
                    </td>
                    <td>
                        <%--<dx:ASPxLabel ID="ASPxLabel1" ClientInstanceName="clID" runat="server" Text="" Visible="False">
                    </dx:ASPxLabel>--%>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right; margin-top: 30px">
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnExport" runat="server" Text="Export">
                            <ClientSideEvents Click="view" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 65px">
            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView">
            </dx:ASPxGridViewExporter>
            <dx:ASPxTimer ID="ASPxTimer1" runat="server" OnTick="ASPxTimer1_Tick" Interval="60000">
            </dx:ASPxTimer>
            <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" KeyFieldName="kodeAk"
                AutoGenerateColumns="False" SettingsPager-PageSize="20" CssClass="grid" OnDataBinding="gridView_DataBinding">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Broker Code" FieldName="kodeAk">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Settlement Date" FieldName="strDateSerah" VisibleIndex="2">
                        <PropertiesDateEdit DisplayFormatString="dd-MM-yyyy">
                        </PropertiesDateEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Balance" FieldName="balance">
                        <PropertiesTextEdit DisplayFormatString="n0">
                        </PropertiesTextEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Obligation" FieldName="obligation">
                        <PropertiesTextEdit DisplayFormatString="n0">
                        </PropertiesTextEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Selisih" FieldName="selisih">
                        <PropertiesTextEdit DisplayFormatString="#,##0.00;(#,##0.00)">
                        </PropertiesTextEdit>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" VerticalScrollableHeight="300" />
                <Styles>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
