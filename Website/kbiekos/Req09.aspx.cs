﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;

namespace imq.kpei.kbiekos
{
    public partial class Req09 : System.Web.UI.Page
    {
        private logLikuidasi[] likuidasi;
        private String aUserLogin;
        private String aUserMember;
        private String hari = DateTime.Now.ToString("yyyy-MM-dd");
        //private String hari = "2016-03-08";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();


            if (!IsPostBack)
            {
                dateEdit.Value = DateTime.Now;
                Session["dateChange"] = dateEdit.Text.ToString();
                gridView.DataBind();
            }

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            getData();
        }

        private void getData()
        {
            string kDate = Session["dateChange"].ToString();

            gridView.DataBind();
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = getLikuidasi();
        }
        private logLikuidasi[] getLikuidasi()
        {
            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "loglikuidasi?logdate=" + Session["dateChange"].ToString(); 
           
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                // invoke the REST method
                byte[] data = client.DownloadData(url);
                // put the downloaded data in a memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                // deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<logLikuidasi>));
                List<logLikuidasi> result = ser.ReadObject(ms) as List<logLikuidasi>;

                likuidasi = result.ToArray();
                //if (cmbKbroker != null)
                //{ 
                //    if(cmbKbroker.Text )
                //}

                return likuidasi;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            getData();
        }


        protected void btnCsvExport_Click(object sender, EventArgs e)
        {

        }
        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
        }
    }
}