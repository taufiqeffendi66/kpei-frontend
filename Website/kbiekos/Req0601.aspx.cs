﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;

namespace imq.kpei.kbiekos
{
    public partial class Req0601 : System.Web.UI.Page
    {
        private kontrakBerjangka[] kontrak;
        private kos[] dataKos;
        private String aUserLogin;
        private String aUserMember;
        private String hari = DateTime.Now.ToString("yyyy-MM-dd");
        //private String hari = "2016-03-07";
        

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                
                lbl_titleLabel2.Text = DateTime.Now.ToString("dd MMMM yyyy");
                lbl_titleLabel4.Text = DateTime.Now.ToString("dd MMMM yyyy");
                dateEdit.Value = DateTime.Now;
                Session["dateChange"] = dateEdit.Text.ToString();
                gridView.DataBind();
                gridView2.DataBind();
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            getData();
        }

        private string getFooter()
        {
            jumlahColHarian result;
            try
            {
                // Statements that are can cause exception
                String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "summarykbieharian?datadate=" + Session["dateChange"].ToString();
                WebClient client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(jumlahColHarian));
                
               // if (ser != null)
                    result = ser.ReadObject(ms) as jumlahColHarian;
                //else
                    //result = null;
            }
            catch (Exception x)
            {
                // Statements to handle exception
                result = null;
            }

            

            if (result != null)
            {
                lbl_jmlCol1.Text = " " + result.akSetorCollateral.ToString();
                lbl_jmlPengaman.Text = " " + result.akSetorDanaPengaman.ToString();
            }
            else
            {
                lbl_jmlCol1.Text = " -";
                lbl_jmlPengaman.Text = " -";
            }

            return "";
          
        }

        private string getFooter2()
        {
            jumlahKosHarian result2;
            try
            {
                // Statements that are can cause exception
                String url2 = ImqSession.GetConfig("TOOLS_REST_SERVER") + "summarykosharian?datadate=" + Session["dateChange"].ToString();
                WebClient client2 = new WebClient();
                client2.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data2 = client2.DownloadData(url2);
                //put the downloaded data in memory stream
                MemoryStream ms2 = new MemoryStream();
                ms2 = new MemoryStream(data2);
                //deserialize from json
                DataContractJsonSerializer ser2 = new DataContractJsonSerializer(typeof(jumlahKosHarian));
                 result2= ser2.ReadObject(ms2) as jumlahKosHarian;
            }
            catch (Exception x)
            {
                // Statements to handle exception
                result2 = null;
            }

            

            if (result2 != null)
            {
                lbl_jmlCol2.Text = " " + result2.akSetorCollateral.ToString();
                lbl_shmInduk.Text = " " + result2.sahamInduk;
            }
            else
            {
                lbl_jmlCol2.Text = " -";
                lbl_shmInduk.Text = " -";
            }

            return "";
        }

        private void getData()
        {
            string kDate = Session["dateChange"].ToString();
            hari = kDate;
            string search = String.Empty;

            string date = kDate;
            DateTime dt = Convert.ToDateTime(date);

            lbl_titleLabel2.Text = dt.ToString("dd MMMM yyyy");
            lbl_titleLabel4.Text = dt.ToString("dd MMMM yyyy"); 

            if (kDate != String.Empty)
            {
                kDate = kDate.Replace("'", "''");
                //kDate = kDate.Replace("-", "--");
                kDate = String.Format("[tanggalData] = '{0}'", kDate);
            }

            if (kDate != String.Empty)
            {
                search = kDate;
            }
            else
            {
                search = String.Empty;
            }

            gridView.FilterExpression = search;
            gridView2.FilterExpression = search;
        }       

        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
        }

        protected void tbNoSurat_TextChanged(object sender, EventArgs e)
        {

        }

        private kontrakBerjangka[] getKontrak()
        {
            int i;

            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "rekapperdagangankontrakberjangka?tanggaldata=" + Session["dateChange"].ToString();
            
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<kontrakBerjangka>));
                List<kontrakBerjangka> result = ser.ReadObject(ms) as List<kontrakBerjangka>;

                kontrak = result.ToArray();
                i = kontrak.Length;

                if (kontrak.Length > 0)
                {
                    getFooter();
                }
                else
                {
                    lbl_jmlCol1.Text = " -";
                    lbl_jmlPengaman.Text = " -";
                }

                return kontrak;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        private kos[] getKos()
        {
            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "rekapperdagangankos?tanggaldata=" + Session["dateChange"].ToString();
            WebClient client = new WebClient();
            client.Headers["Content-Type"] = "application/json";
            //invoker the rest method
            byte[] data = client.DownloadData(url);
            //put the downloaded data in memory stream
            MemoryStream ms = new MemoryStream();
            ms = new MemoryStream(data);
            //deserialize from json
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<kos>));
            List<kos> result = ser.ReadObject(ms) as List<kos>;

            dataKos = result.ToArray();

            if (dataKos.Length > 0)
            {
                getFooter2();
            }
            else
            {
                lbl_jmlCol2.Text = " -";
                lbl_shmInduk.Text = " -";
            }

            return dataKos;
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = getKontrak();
        }

        protected void gridView2_DataBinding(object sender, EventArgs e)
        {
            gridView2.DataSource = getKos();
        }

        protected void btnExportPdf_Click(object sender, EventArgs e)
        {
            getData();
            //GridExporter1.FileName = tbNoSurat.Text + DateTime.Now.ToString("yyyy-MM-dd");
            GridExporter1.ReportHeader = tbNoSurat.Text.ToString() + "tes";
            PrintingSystem ps = new PrintingSystem();

            PrintableComponentLink link1 = new PrintableComponentLink(ps);
            link1.Component = GridExporter1;

            PrintableComponentLink link2 = new PrintableComponentLink(ps);
            link2.Component = GridExporter2;

            CompositeLink compositeLink = new CompositeLink(ps);
            compositeLink.Links.AddRange(new object[] { link1, link2 });

            compositeLink.CreateDocument();
            using (MemoryStream stream = new MemoryStream())
            {
                if (tbNoSurat.Text != String.Empty)
                {
                    compositeLink.PageHeaderFooter = tbNoSurat.Text.ToString() + "";
                    compositeLink.RtfReportFooter = lbl_Col1.Text.ToString() + "";
                    compositeLink.PrintingSystem.ExportToPdf(stream);
                    WriteToResponse("Req0601", true, "pdf", stream);
                }
            }
            ps.Dispose();
        }

        protected void btnCsvExport_Click(object sender, EventArgs e)
        {
            getData();
            //GridExporter1.FileName = tbNoSurat.Text + DateTime.Now.ToString("yyyy-MM-dd");
            //GridExporter1.ReportHeader = tbNoSurat.Text.ToString() + "tes";
            PrintingSystem ps = new PrintingSystem();

            PrintableComponentLink link1 = new PrintableComponentLink(ps);
            link1.Component = GridExporter1;

            PrintableComponentLink link2 = new PrintableComponentLink(ps);
            link2.Component = GridExporter2;

            CompositeLink compositeLink = new CompositeLink(ps);
            compositeLink.Links.AddRange(new object[] { link1, link2 });

            compositeLink.CreateDocument();
            using (MemoryStream stream = new MemoryStream())
            {
                if (tbNoSurat.Text != String.Empty)
                {
                    //compositeLink.RtfReportHeader = tbNoSurat.Text.ToString() + "";
                    compositeLink.PrintingSystem.ExportToCsv(stream);
                    WriteToResponse("Req0601", true, "csv", stream);
                }                
            }
            ps.Dispose();
        }

        void WriteToResponse(string fileName, bool saveAsFile, string fileFormat, MemoryStream stream)
        {
            if (Page == null || Page.Response == null)
                return;
            string disposition = saveAsFile ? "attachment" : "inline";
            Page.Response.Clear();
            Page.Response.Buffer = false;
            Page.Response.AppendHeader("Content-Type", string.Format("application/{0}", fileFormat));
            Page.Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Page.Response.AppendHeader("Content-Disposition",
                string.Format("{0}; filename={1}.{2}", disposition, fileName, fileFormat));
            Page.Response.BinaryWrite(stream.GetBuffer());
            Page.Response.End();
        }

        protected void gridView_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
        {
            //getData();
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            gridView.DataBind();
            gridView2.DataBind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }

    }
}