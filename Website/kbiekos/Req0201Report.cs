﻿using System;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.bankWS;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace imq.kpei.kbiekos
{
    public partial class Req0201Report : DevExpress.XtraReports.UI.XtraReport
    {
        private statusIntruksi[] statusIntruksi;
        public String hari;
        public String broker;
        public String status;
        private String url;

        public Req0201Report()
        {
            InitializeComponent();
        }

        private statusIntruksi[] getStatus()
        {
            WebClient client = null;
            try
            {
                string date = hari;
                DateTime dt = Convert.ToDateTime(date);


                if (broker == "null")
                {
                    if (status == "null")
                        url = "http://192.168.138.19:8090/statusinstruksi?settlementdate=" + hari;
                    else
                    {
                        url = "http://192.168.138.19:8090/statusinstruksi?settlementdate=" + hari +"&settlementstatus=" + status;
                    }
                }
                else
                {
                    if (status != "null")
                        url = "http://192.168.138.19:8090/statusinstruksi?settlementdate=" + hari + "&membercode=" + broker + "&settlementstatus=" + status;
                    else
                    {
                        url = "http://192.168.138.19:8090/statusinstruksi?settlementdate=" + hari + "&membercode=" + broker;
                    }

                }

                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<statusIntruksi>));
                List<statusIntruksi> result = ser.ReadObject(ms) as List<statusIntruksi>;

                statusIntruksi = result.ToArray();

                lbTitle.Text = "Status Settle dan Pemantauan Penyelesaian Transaksi Kontrak Berjangka Tanggal " 
                    + dt.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));

                DataSource = statusIntruksi;

                if (client != null)
                {
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "codeOfBroker");
                    cellBK.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "instructionType");
                    cellInstruction.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "strDate");
                    cellSettleDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "debitAccNumber");
                    cellDebitAcc.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "creditAccNumber");
                    cellCreditAcc.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "settlementStatus");
                    cellSettleStatus.DataBindings.Add(binding);


                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                client.Dispose();
            }

            return statusIntruksi;
        }

        private void Req0201Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            getStatus();
        }
    }
}
