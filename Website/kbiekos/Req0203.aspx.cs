﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;

namespace imq.kpei.kbiekos
{
    public partial class Req0203 : System.Web.UI.Page
    {

        private infoTerima[] terima;
        private String aUserLogin;
        private String aUserMember;
        //private String hari = DateTime.Now.ToString("yyyy-MM-dd");
        private String hari;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                dateEdit.Text = DateTime.Now.ToString("yyyy-MM-dd");
                hari = dateEdit.Text;

                gridView.DataBind();
                if (cmbSelisih != null)
                {
                    cmbSelisih.Items.Add("Semua", "");
                    cmbSelisih.Items.Add("Selisih = 0", "true");
                    cmbSelisih.Items.Add("Selisih > 0", "false");
                }

                var url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "memberinfo";
                
                try
                {
                    WebClient client = new WebClient();
                    client.Headers["Content-type"] = "application/json";
                    // invoke the REST method
                    byte[] data = client.DownloadData(url);
                    // put the downloaded data in a memory stream
                    MemoryStream ms = new MemoryStream();
                    ms = new MemoryStream(data);
                    // deserialize from json
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<memberBroker>));
                    List<memberBroker> result = ser.ReadObject(ms) as List<memberBroker>;

                    foreach (memberBroker m in result)
                    {
                        cmbKbroker.Items.Add(m.memberId + " - " + m.memberName, m.memberId);
                    }
                }
                catch (Exception ex)
                {
                    string msg;
                    msg = ". Web Service is Offline!";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                    //return null;
                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hari = dateEdit.Text;
            gridView.DataBind();
        }

        //private void getData()
        //{
        //    String kBroker = "";
        //    String kDate = hari;
        //    String kSelisih = "";
        //    //String kDate = DateTime.Now.ToString("yyyy-MM-dd");
        //    //hari = kDate;
        //    // String kStatus = "";

        //    if (cmbKbroker.Text != String.Empty)
        //    {
        //        kBroker = cmbKbroker.Value.ToString();
        //    }

        //    if (cmbSelisih.Text != String.Empty)
        //    {
        //        kSelisih = cmbSelisih.Text.ToString();
        //    }

        //    string search = String.Empty;

        //    if (kBroker != String.Empty)
        //    {
        //        if (kSelisih != String.Empty)
        //        {
        //            kBroker = kBroker.Replace("'", "''");
        //            kBroker = kBroker.Replace("-", "--");
        //            kBroker = String.Format("[kodeAk] like '%{0}%'", kBroker);
        //            if (kSelisih == "Selisih > 0")
        //            {
        //                search = String.Format("[selisih] > 0 and {0}", kBroker);
        //            }
        //            else
        //            {
        //                search = String.Format("[selisih] = 0 and {0}", kBroker);
        //            }
        //        }
        //        else
        //        {
        //            search = kBroker;
        //        }
        //    }
        //    else
        //    {
        //        if (kSelisih == "Selisih > 0")
        //        {
        //            search = String.Format("[selisih] > 0");
        //        }
        //        else if (kSelisih == "Selisih = 0")
        //        {
        //            search = String.Format("[selisih] = 0");
        //        }
        //        else
        //        {
        //            search = String.Empty;
        //        }
        //    }
        //    Label1.Text = search;
        //    //Label1.Visible = true;

        //    gridView.FilterExpression = search;
        //}

        private infoTerima[] getSerah()
        {
            string url;

            if (cmbKbroker.Text != String.Empty)
            {
                if (cmbSelisih.Text != String.Empty && cmbSelisih.Text != "Semua")
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "infokewajibanterima?settlementdate=" + hari + "&membercode=" +
                        cmbKbroker.Value.ToString() + "&selisihis0=" + cmbSelisih.Value.ToString();
                else
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "infokewajibanterima?settlementdate=" + hari + "&membercode=" + cmbKbroker.Value.ToString();
            }
            else
            {
                if (cmbSelisih.Text != String.Empty && cmbSelisih.Text != "Semua")
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "infokewajibanterima?settlementdate=" + hari + "&selisihis0=" + cmbSelisih.Value.ToString();
                else
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "infokewajibanterima?settlementdate=" + hari;

            }

            
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                // invoke the REST method
                byte[] data = client.DownloadData(url);
                // put the downloaded data in a memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                // deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<infoTerima>));
                List<infoTerima> result = ser.ReadObject(ms) as List<infoTerima>;

                terima = result.ToArray();

                return terima;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            hari = dateEdit.Text;
            gridView.DataSource = null;
            gridView.DataSource = getSerah();
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            hari = dateEdit.Text;
            gridView.DataBind();
        }

        protected void cmbKbroker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmbSelisih_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {

        }

        //protected void btnCsvExport_Click(object sender, EventArgs e)
        //{
        //    getData();
        //    gridExport.FileName = "Req02-Kewajiban Terima-" + DateTime.Now.ToString("yyyy-MM-dd");
        //    gridExport.WriteCsvToResponse();
        //}

        //protected void btnExportPdf_Click(object sender, EventArgs e)
        //{
        //    getData();
        //    gridExport.FileName = "Req02-Kewajiban Terima-" + DateTime.Now.ToString("yyyy-MM-dd");
        //    gridExport.WritePdfToResponse();
        //}
    }
}