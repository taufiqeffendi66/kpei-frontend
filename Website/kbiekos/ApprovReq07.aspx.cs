﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Common;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Common.clientAccountWS;
using Common.wsApproval;
using Common.wsUserGroupPermission;
using System.Text;

namespace imq.kpei.kbiekos
{
    public partial class ApprovReq07 : System.Web.UI.Page
    {
        private int addedit = 0;
        private String aUserLogin;
        private String aUserMember;
        private sidTmp asidTmp;
        private const String module = "Data SID";

        protected void Page_Load(object sender, EventArgs e)
        {
            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            ImqSession.ValidateAction(this);

            if (!IsPostBack)
            {
                DisabledControl();
                if (Request.QueryString["stat"] != null)
                {
                    addedit = int.Parse(Server.UrlDecode(Request.QueryString["stat"].ToString()));
                    Session["stat"] = addedit;
                }
                else
                {
                    addedit = int.Parse(Session["stat"].ToString());
                }
                if (Session["stat"] == null)
                {
                    Response.Redirect("Req07.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                }

                btnSave.Visible = false;
                btnApproval.Visible = false;
                btnChecker.Visible = false;
                btnReject.Visible = false;
                

                switch (addedit)
                {
                    case 0:
                        lbltitle.Text = "New " + module; //New Direct
                        btnSave.Visible = true;
                        Permission(true);
                        break;
                    case 1:
                        lbltitle.Text = "Edit " + module; //Edit Direct
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        break;
                    case 2:
                        lbltitle.Text = "New " + module; //New From Temporary
                        Permission(false);
                        GetTemporary();
                        //DisabledControl();

                        break;
                    case 3:
                        lbltitle.Text = "Edit " + module;
                        Permission(false);
                        GetTemporary();
                        //DisabledControl();
                        break;
                    case 4:
                        lbltitle.Text = "Delete " + module;
                        btnSave.Visible = true;
                        Permission(true);
                        ParseQueryString();
                        //DisabledControl();
                        btnSave.Text = "Delete";
                        break;
                    default: break;
                }

                Session["temp"] = "1";
            }

        }

        private void ExecCheckerApproval(int cp)
        {
            ApprovalService aps = null;
            WebClient client = null;
            ClientAccountWSService cws = null;
            //DataRow row = null;
            dtApproval[] dtA = null;

            const string target = @"../administration/approval.aspx";
            try
            {
                //row = (DataRow)Session["row"];
                aps = ImqWS.GetApprovalWebService();
                String url = ImqSession.GetConfig("TOOLS_WS_REST") + "accountsidaudittrailtmp_byidx?idx=" + Request.QueryString["idxTmp"].ToString();
                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoke rest method
                byte[] data = client.DownloadData(url);
                //put downloaded data to an memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(sidTmp));
                sidTmp result = ser.ReadObject(ms) as sidTmp;

                //dtA = aps.Search(row["reffNo"].ToString());
                dtA = aps.Search(Request.QueryString["reffNo"].ToString());
                asidTmp = result;

                string activity;
                if (cp == 0)
                {
                    activity = "Checker";
                    dtA[0].checkerName = aUserLogin;
                    dtA[0].checkerStatus = "Checked";
                    dtA[0].approvelStatus = "To Be Approved";
                    dtA[0].memberId = aUserMember;
                    dtA[0].status = "C";
                    aps.UpdateByChecker(dtA[0]);
                }
                else
                    if (cp == 1)
                    {
                        activity = "Approval";
                    }
                    else
                    {
                        if (btnChecker.Visible == true)
                        {
                            activity = "Reject Checker";
                            dtA[0].checkerName = aUserLogin;
                            dtA[0].checkerStatus = "Rejected";
                            dtA[0].approvelStatus = "";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RC";
                            aps.UpdateByChecker(dtA[0]);
                        }
                        else
                        {
                            activity = "Reject Approval";
                            dtA[0].approvelName = aUserLogin;
                            dtA[0].approvelStatus = "Rejected";
                            dtA[0].memberId = aUserMember;
                            dtA[0].status = "RA";
                            aps.UpdateByApprovel(dtA[0]);
                        }
                    }

                string ret = String.Empty;
                //string message = String.Empty;
                switch (dtA[0].insertEdit)
                {
                    case "E":
                        WriteAuditTrail(String.Format("{0} Edit {1}", activity, module));
                        if (cp == 1)
                        {
                            String url2 = ImqSession.GetConfig("TOOLS_WS_REST") + "/accountsidaudittrail_update?id=" + asidTmp.id.ToString() + "&action=Approved";
                            client = new WebClient();
                            client.Headers["Content-Type"] = "application/json";
                            //invoke rest method
                            byte[] dataUpdate = client.DownloadData(url2);
                            //put downloaded data to an memory stream
                            MemoryStream msUp = new MemoryStream();
                           msUp = new MemoryStream(dataUpdate);
                            //deserialize from json
                            DataContractJsonSerializer serUp = new DataContractJsonSerializer(typeof(AccountSidAudittrail));
                            //serUp.ReadObject(msUp);
                            //AccountSidAudittrail r2 = serUp.ReadObject(msUp) as AccountSidAudittrail;

                            //MemoryStream msUp = new MemoryStream();
                            //msUp = new MemoryStream();
                            //serUp.WriteObject(msUp, asidTmp);
                            string jsonString = Encoding.UTF8.GetString(msUp.ToArray());
                            //msUp = new MemoryStream(client.UploadData(url, msUp.ToArray()));

                            //serUp.WriteObject(msUp, asidTmp);
                            ret = "0";
                            if (!ret.Equals("0"))
                            {
                                //lblError.Text = ret;
                                //btnApproval.Visible = false;
                                //btnReject.Visible = false;
                                dtA[0].approvelStatus = "Failed";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                ShowMessage(ret, target);
                            }
                            else
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].approvelName = aUserLogin;
                                dtA[0].memberId = aUserMember;
                                dtA[0].status = "A";
                                aps.UpdateByApprovel(dtA[0]);
                                //ret = bws.deleteTempById(aBankTemp.idx);
                                if (!ret.Equals("0"))
                                    lblError.Text = ret;

                                ShowMessage(String.Format("Succes Edit {0} as Approval", module), target);
                            }
                        }
                        else
                            if (cp == 0)
                            {
                                ShowMessage(String.Format("Succes Edit {0} as Checker", module), target);
                            }
                            else
                            {
                                if (btnChecker.Visible == true)
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Checker", module), target);
                                }
                                else
                                {
                                    ShowMessage(String.Format("Rejected Edit {0} as Approval", module), target);
                                }
                            }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
            }
        }

        private void GetTemporary()
        {
            WebClient client = null;
            try
            {
                String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrailtmp_byidx?idx=" + Request.QueryString["idxTmp"].ToString();
                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoke rest method
                byte[] data = client.DownloadData(url);
                //put downloaded data to an memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(sidTmp));
                sidTmp result = ser.ReadObject(ms) as sidTmp;

                txtWaktu.Text = result.newStr.ToString();
                txtSID.Text = result.sid;
                txtCmCode.Text = result.cmCode;
                txtAccID.Text = result.accountId;
                txtCurrentID.Text = result.currentId;
                txtStatus.Text = result.status;

            }
            catch (Exception e)
            {
                lblError.Text = e.Message;
            }
            finally
            {
                client.Dispose();
            }
        }

        private void DisabledControl()
        {
            txtWaktu.Enabled = false;
            txtSID.Enabled = false;
            txtCmCode.Enabled = false;
            txtAccID.Enabled = false;
            txtCurrentID.Enabled = false;
            txtStatus.Enabled = false;
        }

        private void ParseQueryString()
        {
            string strID = Server.UrlDecode(Request.QueryString["id"].ToString().Trim());
            string strWaktu = Server.UrlDecode(Request.QueryString["newStr"].ToString().Trim());
            string strSid = Server.UrlDecode(Request.QueryString["sid"].ToString().Trim());
            string strCmCode = Server.UrlDecode(Request.QueryString["cmCode"].ToString().Trim());
            string strAccId = Server.UrlDecode(Request.QueryString["accountId"].ToString().Trim());
            string strCurrID = Server.UrlDecode(Request.QueryString["currentId"].ToString().Trim());
            string strStatus = Server.UrlDecode(Request.QueryString["status"].ToString().Trim());
            string strAction = Server.UrlDecode(Request.QueryString["action"].ToString().Trim());
            string strHari = Server.UrlDecode(Request.QueryString["hari"].ToString().Trim());
            hfAudit.Set("id", strID);
            hfAudit.Set("hari", strHari);
            hfAudit.Set("action", strAction);
            //hfBank.Set("idx", strIdx);


            txtWaktu.Text = strWaktu.Length > 0 ? strWaktu : "";
            txtSID.Text = strSid.Length > 0 ? strSid : "";
            txtCmCode.Text = strCmCode.Length > 0 ? strCmCode : "";
            txtAccID.Text = strAccId.Length > 0 ? strAccId : "";
            txtCurrentID.Text = strCurrID.Length > 0 ? strCurrID : "";
            txtStatus.Text = strStatus.Length > 0 ? strStatus : "";
        }

        private void WriteAuditTrail(string Activity)
        {
            ImqWS.putAuditTrail(aUserLogin, module, Activity, Log(asidTmp));
        }

        private String Log(sidTmp bt)
        {
            string strLog = String.Empty;
            strLog = String.Format("[id : {0}, sid : {1}, cmCode : {2}, status : {3}, action {4}", bt.id, bt.sid, bt.cmCode,
                bt.status, bt.action);
            return strLog;
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information',function()
                                {window.location='" + targetPage + @"'});
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        protected String rblApprovalChanged(dtApproval dtA, ApprovalService wsA, sidTmp bt)
        {
            string ret = "0";
            string target = @"../kbiekos/Req07.aspx";

            if (!sidTmp.ReferenceEquals(asidTmp, bt))
                asidTmp = bt;

            int selected = int.Parse(rblApproval.SelectedItem.Value.ToString());
            switch (selected)
            {
                //Maker
                case 0:
                    wsA.AddMaker(dtA);
                    dtA.checkerStatus = "To Be Check";
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("New " + module);
                            ShowMessage(String.Format("Succes New {0} as Maker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Maker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Maker", module), target);
                            break;
                    }
                    break;
                case 1:
                    wsA.AddDirectChecker(dtA);
                    switch (dtA.insertEdit)
                    {
                        case "I":
                            WriteAuditTrail("Direct Checker New " + module);
                            ShowMessage(String.Format("Succes New {0} as Direct Checker", module), target);
                            break;
                        case "E":
                            WriteAuditTrail("Direct Checker Edit " + module);
                            ShowMessage(String.Format("Succes Edit {0} as Direct Checker", module), target);
                            break;
                        case "R":
                            WriteAuditTrail("Direct Checker Delete " + module);
                            ShowMessage(String.Format("Succes Delete {0} as Direct Checker", module), target);
                            break;
                    }
                    break;
                case 2:

                    String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail_update?id=" + hfAudit.Get("id").ToString() + "&action=Approved";
                    WebClient client = new WebClient();
                    client.Headers["Content-Type"] = "application/json";
                    
                    //invoke rest method
                    //byte[] data = client.DownloadData(url);
                    //put downloaded data to an memory stream
                    //MemoryStream ms = new MemoryStream();
                    //ms = new MemoryStream(data);
                    ////deserialize from json
                    //var obj = new AccountSidAudittrail();
                    //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(AccountSidAudittrail));

                    //Common.AccountSidAudittrail bk = new Common.AccountSidAudittrail()
                    //{
                    //    id = asidTmp.id,
                    //    sid = asidTmp.sid,
                    //    waktu = asidTmp.waktu,
                    //    cmCode = asidTmp.cmCode,
                    //    accountId = asidTmp.accountId,
                    //    currentId = asidTmp.currentId,
                    //    status = asidTmp.status,
                    //    action = asidTmp.action
                    //};

                    switch (dtA.insertEdit)
                    {
                        case "I":
                            client.DownloadData(url);
                            //ser.WriteObject(ms, obj);
                            ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            break;
                        case "E":
                            client.DownloadData(url);
                            //ser.WriteObject(ms, obj);
                            ShowMessage(String.Format("Succes New {0} as Direct Approval", module), target);
                            break;
                    }
                    break;
            }
            return ret;
        }

        private void Permission(bool isEditor)
        {
            dtFormMenuPermission[] listdtFMP = null;
            try
            {
                listdtFMP = ImqWS.GetFormMenuPermission(aUserLogin, aUserMember, module);
                foreach (dtFormMenuPermission dtFMP in listdtFMP)
                {
                    if (isEditor)
                    {
                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Maker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editMaker)
                                rblApproval.Items.Add("Maker", 0);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Checker");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectChecker)
                                rblApproval.Items.Add("Direct Checker", 1);

                        rblApproval.SelectedItem = rblApproval.Items.FindByText("Direct Approval");
                        if (rblApproval.SelectedItem == null)
                            if (dtFMP.editDirectApproval)
                                rblApproval.Items.Add("Direct Approval", 2);

                        if (rblApproval.Items.Count > 0)
                            rblApproval.SelectedIndex = 0;

                        //if (dtFMP.editMaker) rblApproval.Items.Add("Maker", 0);
                        //if (dtFMP.editDirectChecker) rblApproval.Items.Add("Direct Checker", 1);
                        //if (dtFMP.editDirectApproval) rblApproval.Items.Add("Direct Approval", 2);
                        //if (rblApproval.Items.Count > 0)
                        //    rblApproval.SelectedIndex = 0;
                    }
                    else
                    {
                        bool make = Request.QueryString["makerName"].Equals(aUserLogin);
                        bool che = Request.QueryString["checkerName"].Equals(aUserLogin);
                        bool mem = Request.QueryString["memberId"].Equals(aUserMember);
                        //lbltitle.Text = "Maker : " + make.ToString() + " Checker : " + che.ToString() + " member : " + mem.ToString();
                        //if (!(row["makerName"].Equals(aUserLogin) || row["checkerName"].Equals(aUserLogin)) && !(row["memberId"].Equals(aUserMember)))
                        //if ( (!make || !che) && mem)
                        //{
                        if (!make && mem && (Request.QueryString["status"].ToString() == "M") && (dtFMP.editChecker))
                        {
                            btnChecker.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                            if (!che && mem && (Request.QueryString["status"].ToString() == "C") && (dtFMP.editApproval))
                            {
                                btnApproval.Visible = true;
                                btnReject.Visible = true;
                            }
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            WebClient client = null;
            ApprovalService aps = null;

            try
            {
                String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrailtmp_insert?id=" + hfAudit.Get("id");
                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoke rest method

                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(sidTmp));

                aps = ImqWS.GetApprovalWebService();

                addedit = (int)Session["stat"];
                int idRec;

                sidTmp dt = new sidTmp()
                {
                    log_timestamp = txtWaktu.Text.Trim(),
                    sid = txtSID.Text.Trim(),
                    cmCode = txtCmCode.Text.Trim(),
                    accountId = txtAccID.Text.Trim(),
                    currentId = txtCurrentID.Text.Trim(),
                    status = txtStatus.Text.Trim(),
                    action = hfAudit.Get("action").ToString()
                };

                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream();
                ser.WriteObject(ms, dt);
                string jsonString = Encoding.UTF8.GetString(ms.ToArray());
                ms = new MemoryStream(client.UploadData(url, ms.ToArray()));

                DataContractJsonSerializer serInt = new DataContractJsonSerializer(typeof(int));
                idRec = (int)serInt.ReadObject(ms);
                asidTmp = dt;

                if (idRec > -1)
                {
                    dtApproval dtA = new dtApproval() { type = "c", makerDate = DateTime.Now, makerName = aUserLogin, makerStatus = "Maked", checkerStatus = "To Be Check", idxTmp = idRec, form = "~/kbiekos/ApprovReq07.aspx", status = "M", memberId = aUserMember };
                    int idTable;
                    switch (addedit)
                    {
                        case 0:
                            dtA.topic = "Add " + module;
                            dtA.insertEdit = "I";
                            asidTmp.idx = idRec;
                            break;

                        case 1:
                            dtA.topic = "Edit " + module;
                            dtA.insertEdit = "E";
                            idTable = int.Parse(hfAudit.Get("id").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;

                        case 4:
                            dtA.topic = "Delete " + module;
                            dtA.insertEdit = "R";
                            idTable = int.Parse(hfAudit.Get("id").ToString());
                            dtA.idTable = idTable;
                            dtA.idxTmp = idRec;
                            break;
                        default:
                            break;
                    }
                    string retVal = "0";
                    retVal = rblApprovalChanged(dtA, aps, asidTmp);
                    if (!retVal.Equals("0"))
                    {
                        ShowMessage(retVal, @"../kbiekos/Req07.aspx");
                    }
                }
                else
                {
                    ShowMessage("Failed On Save To Temporary Status Action", @"../kbiekos/Req07.aspx");
                }
            }
            catch (Exception ex)
            {
                if (aps != null)
                    aps.Abort();
                Debug.WriteLine(ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (aps != null)
                    aps.Dispose();
            }

        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(0);
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ExecCheckerApproval(2);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            switch (addedit)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect("Req07.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
                default: break;
            }
        }

        protected void rblApproval_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}