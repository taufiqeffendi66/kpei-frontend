﻿namespace imq.kpei.kbiekos
{
    partial class Req01report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellBK = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellBank = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAccNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellBalance = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDT = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xtraReport1 = new DevExpress.XtraReports.UI.XtraReport();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableDetail});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableDetail
            // 
            this.tableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableDetail.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableDetail.Name = "tableDetail";
            this.tableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tableDetail.SizeF = new System.Drawing.SizeF(776F, 31.25F);
            this.tableDetail.StylePriority.UseBorders = false;
            this.tableDetail.StylePriority.UseFont = false;
            this.tableDetail.StylePriority.UseTextAlignment = false;
            this.tableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellBK,
            this.cellBank,
            this.cellAccNo,
            this.cellBalance,
            this.cellDT});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellBK
            // 
            this.cellBK.Name = "cellBK";
            this.cellBK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellBK.StylePriority.UsePadding = false;
            this.cellBK.StylePriority.UseTextAlignment = false;
            this.cellBK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellBK.Weight = 0.45933334683449661D;
            // 
            // cellBank
            // 
            this.cellBank.Name = "cellBank";
            this.cellBank.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellBank.StylePriority.UsePadding = false;
            this.cellBank.StylePriority.UseTextAlignment = false;
            this.cellBank.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellBank.Weight = 0.52708135604963124D;
            // 
            // cellAccNo
            // 
            this.cellAccNo.Multiline = true;
            this.cellAccNo.Name = "cellAccNo";
            this.cellAccNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellAccNo.StylePriority.UsePadding = false;
            this.cellAccNo.StylePriority.UseTextAlignment = false;
            this.cellAccNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellAccNo.Weight = 0.6487116522832661D;
            // 
            // cellBalance
            // 
            this.cellBalance.Name = "cellBalance";
            this.cellBalance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellBalance.StylePriority.UsePadding = false;
            this.cellBalance.StylePriority.UseTextAlignment = false;
            this.cellBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellBalance.Weight = 0.68243673654280546D;
            this.cellBalance.XlsxFormatString = "";
            // 
            // cellDT
            // 
            this.cellDT.Multiline = true;
            this.cellDT.Name = "cellDT";
            this.cellDT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellDT.StylePriority.UsePadding = false;
            this.cellDT.StylePriority.UseTextAlignment = false;
            this.cellDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellDT.Weight = 0.68243690828980053D;
            this.cellDT.XlsxFormatString = "";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 54F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(625F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 31.25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.tblHeader.SizeF = new System.Drawing.SizeF(776F, 31.25F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cell1,
            this.cell2,
            this.cell3,
            this.cell4,
            this.cell5});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // cell1
            // 
            this.cell1.Name = "cell1";
            this.cell1.Text = "Broker Code";
            this.cell1.Weight = 0.45933334683449661D;
            // 
            // cell2
            // 
            this.cell2.Name = "cell2";
            this.cell2.Text = "Bank";
            this.cell2.Weight = 0.52708138554471329D;
            // 
            // cell3
            // 
            this.cell3.Name = "cell3";
            this.cell3.Text = "Account No";
            this.cell3.Weight = 0.64871169648006877D;
            // 
            // cell4
            // 
            this.cell4.Name = "cell4";
            this.cell4.Text = "Balance";
            this.cell4.Weight = 0.68243691108482951D;
            // 
            // cell5
            // 
            this.cell5.Name = "cell5";
            this.cell5.Text = "Data Timestamp";
            this.cell5.Weight = 0.68243666005589187D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xtraReport1
            // 
            this.xtraReport1.Name = "xtraReport1";
            this.xtraReport1.PageHeight = 1100;
            this.xtraReport1.PageWidth = 850;
            this.xtraReport1.Version = "12.2";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lbTitle});
            this.ReportHeader.HeightF = 59.375F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(775.9999F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle
            // 
            this.lbTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbTitle.ForeColor = System.Drawing.Color.Black;
            this.lbTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle.SizeF = new System.Drawing.SizeF(541.7028F, 37.99999F);
            this.lbTitle.StylePriority.UseFont = false;
            this.lbTitle.StylePriority.UseForeColor = false;
            this.lbTitle.Text = "Status Balance Anggota Kliring Derivatif";
            this.lbTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Req01report
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader,
            this.ReportFooter});
            this.Margins = new System.Drawing.Printing.Margins(40, 34, 54, 51);
            this.Version = "12.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Req01Report_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell cell2;
        private DevExpress.XtraReports.UI.XRTableCell cell4;
        private DevExpress.XtraReports.UI.XRTableCell cell5;
        private DevExpress.XtraReports.UI.XRTable tableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellBK;
        private DevExpress.XtraReports.UI.XRTableCell cellBank;
        private DevExpress.XtraReports.UI.XRTableCell cellAccNo;
        private DevExpress.XtraReports.UI.XRTableCell cellBalance;
        private DevExpress.XtraReports.UI.XRTableCell cellDT;
        private DevExpress.XtraReports.UI.XtraReport xtraReport1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTableCell cell1;
        private DevExpress.XtraReports.UI.XRTableCell cell3;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
    }
}
