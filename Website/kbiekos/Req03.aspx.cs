﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using Common;
using System.Text.RegularExpressions;

namespace imq.kpei.kbiekos
{
    public partial class Req03 : System.Web.UI.Page
    {
        private string name;
        private hphhpf[] hphhpf;

        protected void Page_Load(object sender, EventArgs e)
        {
            var url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "ftpfilelist";
            
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                // invoke the REST method
                byte[] data = client.DownloadData(url);
                // put the downloaded data in a memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                // deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<filelist>));
                List<filelist> result = ser.ReadObject(ms) as List<filelist>;

                foreach (filelist m in result)
                {
                    if (!IsPostBack)
                    {
                        cmbFile.Items.Add(m.filename, m.filename);
                    }
                }
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                //return null;
            }
        }

        private hphhpf[] getHphhpf()
        {
            if (cmbFile.Text != String.Empty)
                name = cmbFile.Value.ToString();
            else
                name = "";
            string url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "validasihphhpf?filename=" + name;
            

            try {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                //invoker the Rest method
                byte[] data = client.DownloadData(url);
                //put downloaded data to memory stream
                MemoryStream ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<hphhpf>));
                List<hphhpf> result = ser.ReadObject(ms) as List<hphhpf>;

                hphhpf = result.ToArray();

                if (hphhpf.Length == 1)
                {
                    if (hphhpf[0].series.StartsWith("ERROR"))
                    {
                        char[] delimiterChars = { '-' };
                        String[] elements = hphhpf[0].series.Split(delimiterChars);
                        ShowMessage(elements[1]);

                        return null;
                    }

                }
                return hphhpf;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }


        protected void btnValidasi_Click(object sender, EventArgs e)
        {
            gridView.DataBind();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

        }

        protected void ASPxUploadControl1_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
        {

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }

        protected void GridExporter1_DataBinding(object sender, EventArgs e)
        {

        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = null;
            gridView.DataSource = getHphhpf();
        }

        private void ShowMessage(string info)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information');
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }
    }
}