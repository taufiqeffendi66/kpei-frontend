﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;

namespace imq.kpei.kbiekos
{
    public partial class Req0602 : System.Web.UI.Page
    {
        private kontrakBerjangka[] kontrak;
        private kos[] dataKos;
        private String aUserLogin;
        private String aUserMember;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if(!IsPostBack)
            {               

                cmbBulan.Text = DateTime.Now.ToString("MMMMM");

                string strBulan = DateTime.Now.ToString("MMMMM");
                cmbTahun.Text = DateTime.Now.Year.ToString();
                cmbBulan.Value = DateTime.Now.Month.ToString();
                cmbTahun.Value = DateTime.Now.Year.ToString();

                lbl_titleLabel2.Text = strBulan + " Tahun " + cmbTahun.Text;
                lbl_titleLabel4.Text = strBulan + " Tahun " + cmbTahun.Text;

                if (cmbBulan != null)
                {
                    //cmbBulan.Items.Add("*", "");
                    cmbBulan.Items.Add("Januari", "1");
                    cmbBulan.Items.Add("Februari", "2");
                    cmbBulan.Items.Add("Maret", "3");
                    cmbBulan.Items.Add("April", "4");
                    cmbBulan.Items.Add("Mei", "5");
                    cmbBulan.Items.Add("Juni", "6");
                    cmbBulan.Items.Add("Juli", "7");
                    cmbBulan.Items.Add("Agustus", "8");
                    cmbBulan.Items.Add("September", "9");
                    cmbBulan.Items.Add("Oktober", "10");
                    cmbBulan.Items.Add("November", "11");
                    cmbBulan.Items.Add("Desember", "12");
                }

                if (cmbTahun != null)
                {
                    //cmbTahun.Items.Add("*", "");
                    cmbTahun.Items.Add("2015", "2015");
                    cmbTahun.Items.Add("2016", "2016");
                    cmbTahun.Items.Add("2017", "2017");
                    cmbTahun.Items.Add("2018", "2018");
                    cmbTahun.Items.Add("2019", "2019");
                }

                gridView.DataBind();
                gridView2.DataBind();
            }           
        }

        private void getFooter()
        {
            jumlahColHarian result;
            try
            {
                // Statements that are can cause exception
                String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "summarykbiebulanan?bulan=" + cmbBulan.Value.ToString() + "&tahun=" + cmbTahun.Value.ToString();
                WebClient client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(jumlahColHarian));
                
               // if (ser != null)
                    result = ser.ReadObject(ms) as jumlahColHarian;
               // else
                    //result = null;
            }
            catch (Exception e)
            {
                result = null;
                // Statements to handle exception
            }


            if (result != null)
            {
                lbl_jmlCol1.Text = " " + result.akSetorCollateral.ToString();
                lbl_jmlPengaman.Text = " " + result.akSetorDanaPengaman.ToString();
            }
            else
            {
                lbl_jmlCol1.Text = " -";
                lbl_jmlPengaman.Text = " -";
            }
           
        }

        private void getFooter2()
        {
            jumlahKosHarian result2;
            try
            {
                // Statements that are can cause exception
                String url2 = ImqSession.GetConfig("TOOLS_REST_SERVER") + "summarykosbulanan?bulan=" + cmbBulan.Value.ToString() + "&tahun=" + cmbTahun.Value.ToString();
                WebClient client2 = new WebClient();
                client2.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data2 = client2.DownloadData(url2);
                //put the downloaded data in memory stream
                MemoryStream ms2 = new MemoryStream();
                ms2 = new MemoryStream(data2);
                //deserialize from json
                DataContractJsonSerializer ser2 = new DataContractJsonSerializer(typeof(jumlahKosHarian));
                
               // if (ser2 != null)
                    result2 = ser2.ReadObject(ms2) as jumlahKosHarian;
               // else
                    //result2 = null;
            }
            catch (Exception x)
            {
                // Statements to handle exception
                result2 = null;
            }

            

            if (result2 != null)
            {
                lbl_jmlCol2.Text = " " + result2.akSetorCollateral.ToString();
                lbl_shmInduk.Text = " " + result2.sahamInduk;
            }
            else
            {
                lbl_jmlCol2.Text = " -";
                lbl_shmInduk.Text = " -";
            }

        }

        private kontrakBerjangka[] getKontrak()
        {
            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "rekapperdagangankbiebulanan?bulan="+cmbBulan.Value.ToString()+"&tahun="+cmbTahun.Value.ToString();
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<kontrakBerjangka>));
                List<kontrakBerjangka> result = ser.ReadObject(ms) as List<kontrakBerjangka>;

                kontrak = result.ToArray();


                if (kontrak.Length > 0)
                {
                    getFooter();
                }
                else
                {
                    lbl_jmlCol1.Text = " -";
                    lbl_jmlPengaman.Text = " -";
                }


                return kontrak;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        private kos[] getKos()
        {
            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "rekapperdagangankosbulanan?bulan="+cmbBulan.Value.ToString()+"&tahun="+cmbTahun.Value.ToString();
            WebClient client = new WebClient();
            client.Headers["Content-Type"] = "application/json";
            //invoker the rest method
            byte[] data = client.DownloadData(url);
            //put the downloaded data in memory stream
            MemoryStream ms = new MemoryStream();
            ms = new MemoryStream(data);
            //deserialize from json
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<kos>));
            List<kos> result = ser.ReadObject(ms) as List<kos>;

            dataKos = result.ToArray();

            if (dataKos.Length > 0)
            {
                getFooter2();
            }
            else
            {
                lbl_jmlCol2.Text = " -";
                lbl_shmInduk.Text = " -";
            }

            return dataKos;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            getData();
            lbl_titleLabel2.Text = cmbBulan.Text + " Tahun " + cmbTahun.Text;
            lbl_titleLabel4.Text = cmbBulan.Text + " Tahun " + cmbTahun.Text;
        }

        private void getData()
        {
            
            gridView2.DataBind();
            gridView.DataBind();
        }
        protected void tbNoSurat_TextChanged(object sender, EventArgs e)
        {

        }

        protected void cmbBulan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmbTahun_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = getKontrak();
        }

        protected void gridView2_DataBinding(object sender, EventArgs e)
        {
            gridView2.DataSource = getKos();
        }

        protected void gridView_ClientLayout(object sender, DevExpress.Web.ASPxClasses.ASPxClientLayoutArgs e)
        {

        }

        protected void gridView_BeforeGetCallbackResult(object sender, EventArgs e)
        {

        }

        protected void gridView_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
        {
            getData();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            gridView.DataBind();
            gridView2.DataBind();
        }
    }
}