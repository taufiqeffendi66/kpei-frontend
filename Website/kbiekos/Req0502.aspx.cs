﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;

namespace imq.kpei.kbiekos
{
    public partial class Req0502 : System.Web.UI.Page
    {
        private kliringTransaksiDer[] kliDerivatif;
        String hari = DateTime.Now.ToString("yyyy-MM-dd");
        //String hari = "2016-03-23";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dateEdit.Text = DateTime.Now.ToString("yyyy-MM-dd");
                hari = dateEdit.Text.ToString();

               // gridView.Columns["totalHarian"].Caption = "Total Transaksi " + DateTime.Now.Day.ToString() + " " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year.ToString();
               // gridView.Columns["totalBulanan"].Caption = "Total Transaksi Bulan " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year.ToString();
                //gridView.Columns["totalTahun"].Caption = "Total Transaksi Tahun " + DateTime.Now.Year.ToString();
                gridView.DataBind();
                ApplyLayout();
            }
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = getDerivatif();
        }

        private kliringTransaksiDer[] getDerivatif()
        {
            Session["dateChange"] = dateEdit.Text.ToString();
            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "kliringtransaksiderivatif?tanggaldata=" + Session["dateChange"];
            
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                //invoker the Rest method
                byte[] data = client.DownloadData(url);
                //put downloaded data to memory stream
                MemoryStream ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<kliringTransaksiDer>));
                List<kliringTransaksiDer> result = ser.ReadObject(ms) as List<kliringTransaksiDer>;

                kliDerivatif = result.ToArray();
                return kliDerivatif;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        void ApplyLayout()
        {
            DateTime dt = Convert.ToDateTime(dateEdit.Text);

            gridView.Columns["totalHarian"].Caption = "Total Transaksi " + dt.Day.ToString() + " " + dt.ToString("MMMM") + " " + dt.Year.ToString();
            gridView.Columns["totalBulanan"].Caption = "Total Transaksi Bulan " + dt.ToString("MMMM") + " " + dt.Year.ToString();
            gridView.Columns["totalTahun"].Caption = "Total Transaksi Tahun " + dt.Year.ToString();

            gridView.BeginUpdate();
            try
            {
                gridView.ClearSort();
                gridView.GroupBy(gridView.Columns["kategori"]);
            }
            finally
            {
                gridView.EndUpdate();
            }
            gridView.ExpandAll();
        }

        protected void gridView_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ApplyLayout();
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
            hari = dateEdit.Text.ToString();


            gridView.DataBind();
            ApplyLayout();
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
            ApplyLayout();
        }

        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
        }
    }
}