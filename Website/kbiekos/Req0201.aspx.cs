﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;

namespace imq.kpei.kbiekos
{
    public partial class Req0201 : System.Web.UI.Page
    {
        private statusIntruksi[] status;
        private String aUserLogin;
        private String aUserMember;
        private String hari;
        //private String hari = "2015-04-24";

        protected void Page_Load(object sender, EventArgs e)
        {

            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!Page.IsPostBack)
            {
                if (cmbStatus.Text != null)
                {
                    cmbStatus.Items.Add("", "");
                    cmbStatus.Items.Add("Settled", "S");
                    cmbStatus.Items.Add("Failed", "F");
                    cmbStatus.Items.Add("Pending", "P");
                }

                dateEdit.Value = DateTime.Now;
                Session["dateChange"] = dateEdit.Text.ToString();
                getData();
                //gridView.DataBind();

                var url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "memberinfo";
               
                try
                {
                    WebClient client = new WebClient();
                    client.Headers["Content-type"] = "application/json";
                    // invoke the REST method
                    byte[] data = client.DownloadData(url);
                    // put the downloaded data in a memory stream
                    MemoryStream ms = new MemoryStream();
                    ms = new MemoryStream(data);
                    // deserialize from json
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<memberBroker>));
                    List<memberBroker> result = ser.ReadObject(ms) as List<memberBroker>;
                    foreach (memberBroker m in result)
                    {
                        cmbKbroker.Items.Add(m.memberId + " - " + m.memberName, m.memberId);
                    }
                }
                catch (Exception ex)
                {
                    string msg;
                    msg = ". Web Service is Offline!";
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                   // return null;
                }

                
            }         
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {            
                getData();           
        }

        private void getData()
        {
            string kDate = Session["dateChange"].ToString();
            String kBroker = "";
            String kStatus = "";

            if (cmbStatus.Text != String.Empty)
            {
                kStatus = cmbStatus.Value.ToString();
            }
            if (cmbKbroker.Text != String.Empty)
            {
                kBroker = cmbKbroker.Value.ToString();
            }

            string search = String.Empty;


            if (kStatus != String.Empty)
            {
                kStatus = kStatus.Replace("'", "''");
                kStatus = kStatus.Replace("-", "--");
                kStatus = String.Format("[settlementStatus] like '%{0}%'", kStatus);
            }
            if (kBroker != String.Empty)
            {
                kBroker = kBroker.Replace("'", "''");
                kBroker = kBroker.Replace("-", "--");
                kBroker = String.Format("[codeOfBroker] like '%{0}%'", kBroker);
            }
            if (kDate != String.Empty)
            {
                kDate = kDate.Replace("'", "''");
                //kDate = kDate.Replace("-", "--");
                kDate = String.Format("[settlementDate] = '{0}'", kDate);
            }

            if (kStatus != String.Empty)
            {
                if (kBroker != String.Empty)
                    if (kDate != String.Empty)
                        search = String.Format("{0} and {1} and {2}", kStatus, kBroker, kDate);
                    else
                        search = String.Format("{0} and {1}", kBroker, kDate);
                else
                    if (kDate != String.Empty)
                        search = search = String.Format("{0} and {1}", kStatus, kDate);
                    else
                        search = kStatus;
            }
            else if (kBroker != String.Empty)
            {
                if (kDate != String.Empty)
                    search = String.Format("{0} and {1}", kBroker, kDate);
                else
                    search = kBroker;
            }
            else
            {
                if (kDate != String.Empty)
                    search = kDate;
                else
                    search = String.Empty;
            }
            Label1.Text = search;
           // Label1.Visible = true;

            gridView.FilterExpression = search;
        }

        private statusIntruksi[] getStatus()
        {
            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "statusinstruksi?settlementdate=" + Session["dateChange"].ToString();
            

            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                // invoke the REST method
                byte[] data = client.DownloadData(url);
                // put the downloaded data in a memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                // deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<statusIntruksi>));
                List<statusIntruksi> result = ser.ReadObject(ms) as List<statusIntruksi>;

                status = result.ToArray();

                return status;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = getStatus();
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            getData();
        }

        protected void cmbKbroker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString() ;
        }

        protected void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //protected void btnExportPdf_Click(object sender, EventArgs e)
        //{
        //    getData();
        //    gridExport.FileName = "Req02-Status Instruksi-" + DateTime.Now.ToString("yyyy-MM-dd");
        //    gridExport.WritePdfToResponse();
        //}
        //protected void btnCsvExport_Click(object sender, EventArgs e)
        //{
        //    getData();
        //    gridExport.FileName = "Req02-Status Instruksi-" + DateTime.Now.ToString("yyyy-MM-dd");
        //    gridExport.WriteCsvToResponse();
        //}
    }
}