﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;
using DevExpress.XtraBars.Alerter;
using DevExpress.XtraExport;
using DevExpress.XtraPrinting;
using System.Net.Mime;

namespace imq.kpei.kbiekos
{
    public partial class Req0101 : System.Web.UI.Page
    {
        private statusBalance[] balances;
        private String aUserLogin;
        private String aUserMember;
        private String hari = DateTime.Now.ToString("yyyy-MM-dd");
        //private String hari = "2015-03-24";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                dateEdit.Value = DateTime.Now;
                Session["dateChange"] = dateEdit.Text.ToString();
                gridView.DataBind();
            }

            var url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "memberinfo";
            
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                // invoke the REST method
                byte[] data = client.DownloadData(url);
                // put the downloaded data in a memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                // deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<memberBroker>));
                List<memberBroker> result = ser.ReadObject(ms) as List<memberBroker>;
                foreach (memberBroker m in result)
                {
                    if (!IsPostBack)
                    {
                        cmbKbroker.Items.Add(m.memberId + " - " + m.memberName, m.memberId);
                    }
                }
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
               // return null;
            }

            
        }       

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            getData();            
        }

        private void getData()
        {
            String kDate = Session["dateChange"].ToString();
            //dateEdit.Value = kDate;
            //hari = kDate;
            String kBroker = "";

            if (kDate != String.Empty)
            {
                hari = kDate;
            }
            else
            {
                hari = DateTime.Now.ToString("yyyy-MM-dd");
            }

            if (cmbKbroker.Text != String.Empty)
            {
                kBroker = cmbKbroker.Value.ToString();
            }

            string search = String.Empty;

            if (kBroker != String.Empty)
            {
                kBroker = kBroker.Replace("'", "''");
                kBroker = kBroker.Replace("-", "--");
                kBroker = String.Format("[kodeBroker] like '%{0}%'", kBroker);
                search = kBroker;

                gridView.FilterExpression = search;
            }
            else
            {
                gridView.DataBind();
            }

            Label1.Text = search + "   " + hari + "   " + kDate;
            //Label1.Visible = true;            
        }
        private statusBalance[] getBalance()
        {
            var url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "statusbalance?datatimestamp=" + Session["dateChange"].ToString();
            try {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                // invoke the REST method
                byte[] data = client.DownloadData(url);
                // put the downloaded data in a memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                // deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<statusBalance>));
                List<statusBalance> result = ser.ReadObject(ms) as List<statusBalance>;

                balances = result.ToArray();
                return balances;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = getBalance();
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            getData();
        }

        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

        }

        protected void cmbKbroker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        //------------------------------------------------------------//

        //protected void btnCsvExport_Click(object sender, EventArgs e)
        //{
        //    String title = dateEdit.Text.ToString();
        //    String aTitle = title.Replace("-", "");
        //    getData();
        //    if (cmbKbroker.Text != String.Empty)
        //        gridExport.FileName = "Req01-" + aTitle + "-" + cmbKbroker.Value.ToString();
        //    else
        //        gridExport.FileName = "Req01-" + aTitle;
        //    gridExport.WriteCsvToResponse();
        //}

        //protected void btnExportPdf_Click(object sender, EventArgs e)
        //{
        //    String title = dateEdit.Text.ToString();
        //    String aTitle = title.Replace("-", "");
        //    getData();
        //    //gridExport.ReportHeader = "tes1"+aTitle;
        //    //gridExport.ReportFooter = "tes2"+aTitle;
        //    //if (cmbKbroker.Text != String.Empty)
        //    //    gridExport.FileName = "Req01-" + aTitle + "-" + cmbKbroker.Value.ToString();
        //    //else
        //    //    gridExport.FileName = "Req01-" + aTitle;
        //    ////gridExport.WritePdfToResponse();

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
        //        pcl.Component = gridExport;
        //        pcl.Margins.Left = pcl.Margins.Right = 50;
        //        pcl.Landscape = false;
        //        pcl.CreateDocument(false);
        //        pcl.PrintingSystem.Document.AutoFitToPagesWidth = 1;
        //        pcl.ExportToPdf(ms);
        //        WriteResponse(this.Response, ms.ToArray(), System.Net.Mime.DispositionTypeNames.Inline.ToString());
        //    }
        //}

        //public static void WriteResponse(HttpResponse response, byte[] filearray, string type)
        //{
        //    response.ClearContent();
        //    response.Buffer = true;
        //    response.Cache.SetCacheability(HttpCacheability.Private);
        //    response.ContentType = "application/pdf";
        //    ContentDisposition contentDisposition = new ContentDisposition();
        //    contentDisposition.FileName = "test.pdf";
        //    contentDisposition.DispositionType = type;
        //    response.AddHeader("Content-Disposition", contentDisposition.ToString());
        //    response.BinaryWrite(filearray);
        //    HttpContext.Current.ApplicationInstance.CompleteRequest();
        //    try
        //    {
        //        response.End();
        //    }
        //    catch (System.Threading.ThreadAbortException)
        //    {
        //    }
        //}
    }
}