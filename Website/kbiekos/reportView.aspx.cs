﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using Common;

namespace imq.kpei.kbiekos
{
    public partial class reportView : System.Web.UI.Page
    {
        public String aHari;
        public String aBroker;
        public String tgl;
        public String idReq;
        public String aSettle;
        public String aSelisih;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            if (Request.QueryString["hari"] != null)
                aHari = Request.QueryString["hari"].ToString().Trim();
            if (Request.QueryString["kbroker"] != null)
                aBroker = Request.QueryString["kbroker"].ToString().Trim();
            if (Request.QueryString["settle"] != null)
                aSettle = Request.QueryString["settle"].ToString().Trim();
            if (Request.QueryString["req"] != null)
                idReq = Request.QueryString["req"].ToString().Trim();
            if (Request.QueryString["selisih"] != null)
                aSelisih = Request.QueryString["selisih"].ToString().Trim();

            DateTime dt = Convert.ToDateTime(aHari);

            tgl = dt.ToString("yyyyMMdd");

            ReportV1.Report = CreateReport();
        }

        XtraReport CreateReport()
        {
            Req01report report;
            Req0201Report report2;
            Req0202Report report3;
            Req0203Report report4;
            Req07Report report5;

            if (idReq == "01")
            {
                if (aBroker == "null")
                    report = new Req01report() { hari = aHari, broker = aBroker, Name = "Req01-" + tgl };
                else
                    report = new Req01report() { hari = aHari, broker = aBroker, Name = "Req01-" + tgl + "_" + aBroker };

                return report;
            }
            else if (idReq == "0201")
            {
                if (aBroker == "null")
                    report2 = new Req0201Report() { hari = aHari, broker = aBroker, status = aSettle, Name = "Req02-Status Intruksi-" + tgl };
                else
                    report2 = new Req0201Report() { hari = aHari, broker = aBroker, status = aSettle, Name = "Req02-Status Intruksi-" + tgl + "_" + aBroker };

                return report2;
            }
            else if (idReq == "0202")
            {
                if (aBroker == "null")
                    //report3 = new Req0202Report() { hari = DateTime.Now.ToString("yyyy-MM-dd"), broker = aBroker, Name = "Req02-Kewajiban Serah-" + tgl };
                    report3 = new Req0202Report() { hari = "2015-04-24", broker = aBroker, Name = "Req02-Kewajiban Serah-" + tgl };
                else
                    report3 = new Req0202Report() { hari = "2015-04-24", broker = aBroker, Selisih = aSelisih, Name = "Req02-Kewajiban Serah-" + tgl + "_" + aBroker };

                return report3;
            }
            else if (idReq == "0203")
            {
                if (aBroker == "null")
                    //report3 = new Req0202Report() { hari = DateTime.Now.ToString("yyyy-MM-dd"), broker = aBroker, Name = "Req02-Kewajiban Serah-" + tgl };
                    report4 = new Req0203Report() { hari = "2015-04-24", broker = aBroker, Name = "Req02-Kewajiban Terima-" + tgl };
                else
                    report4 = new Req0203Report() { hari = "2015-04-24", broker = aBroker, Selisih = aSelisih, Name = "Req02-Kewajiban Terima-" + tgl + "_" + aBroker };

                return report4;
            }
            else if (idReq == "07")
            {
                if (aBroker == "null")
                    report5 = new Req07Report() { hari = aHari, broker = aBroker, Name = "Req07-" + tgl };
                else
                    report5 = new Req07Report() { hari = aHari, broker = aBroker, Name = "Req07-" + tgl + "_" + aBroker };

                return report5;
            }
            else
            {
                report = new Req01report() { hari = "", broker = "", Name = "-" + tgl };
                return report;
            }

        }
    }
}