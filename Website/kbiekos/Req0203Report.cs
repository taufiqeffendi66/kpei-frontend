﻿using System;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.bankWS;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace imq.kpei.kbiekos
{
    public partial class Req0203Report : DevExpress.XtraReports.UI.XtraReport
    {
        private infoTerima[] terima;
        public String hari;
        public String broker;
        public String status;
        private String url;
        public String Selisih;

        public Req0203Report()
        {
            InitializeComponent();
        }

        private infoTerima[] getTerima()
        {
            WebClient client = null;
            try
            {
                string date = hari;
                DateTime dt = Convert.ToDateTime(date);


                if (broker == "null")
                {
                    if (Selisih == null || Selisih == "null")
                        url = "http://192.168.138.19:8090/infokewajibanterima?settlementdate=" + hari;
                    else
                        url = "http://192.168.138.19:8090/infokewajibanterima?settlementdate=" + hari + "&selisihis0=" + Selisih;
                }
                else
                {
                    if (Selisih != "null")
                        url = "http://192.168.138.19:8090/infokewajibanterima?settlementdate=" + hari + "&membercode=" + broker + "&selisihis0=" + Selisih;
                    else
                        url = "http://192.168.138.19:8090/infokewajibanterima?settlementdate=" + hari + "&membercode=" + broker;
                }

                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<infoTerima>));
                List<infoTerima> result = ser.ReadObject(ms) as List<infoTerima>;

                terima = result.ToArray();

                lbTitle.Text = "Laporan Informasi Hak Terima tanggal " 
                    + dt.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));

                DataSource = terima;

                if (client != null)
                {
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "kodeAk");
                    cellBK.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "strDate");
                    cellSettleDate.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "strBalance");
                    cellObligation.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "strObligation");
                    cellBalance.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "strSelisih");
                    cellSelisih.DataBindings.Add(binding);


                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                client.Dispose();
            }

            return terima;
        }

        private void Req0202Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            getTerima();
        }
    }
}
