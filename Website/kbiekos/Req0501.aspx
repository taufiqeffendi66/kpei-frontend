﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kpei.Master" AutoEventWireup="true" CodeBehind="Req0501.aspx.cs" Inherits="imq.kpei.kbiekos.Req0501" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div id="content">
        <div class="title">Laporan Transaksi Derivatif</div>
        <table>
            <tr>
                <td>
                <dx:ASPxDateEdit ID="dateEdit" runat="server" EditFormat="Custom" EditFormatString="yyyy-MM-dd"
                    Width="200" OnDateChanged="dateEdit_DateChanged" AllowUserInput="False" AllowNull="False"
                    ClientInstanceName="cmDate">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                    
                </td>
                <td>
                    <dx:ASPxButton ID="btnEmail" runat="server" Text="Search" AutoPostBack="false"
                        Visible="true" OnClick="btnEmail_Click">
                    </dx:ASPxButton>
                </td>
                
            </tr>
        </table>
        </div>
        <div style="margin-top: 10px">
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridView"></dx:ASPxGridViewExporter>
        <dx:ASPxTimer ID="ASPxTimer1" runat="server" OnTick="ASPxTimer1_Tick" Interval="60000">
        </dx:ASPxTimer>
        <dx:ASPxGridView ID="gridView" ClientInstanceName="grid" runat="server" KeyFieldName="contractType"
            AutoGenerateColumns="False" SettingsPager-PageSize="20" CssClass="grid" 
            OnDataBinding="gridView_DataBinding" 
            oncustomcallback="gridView_CustomCallback" >
            <Columns>
                <dx:GridViewDataTextColumn VisibleIndex="1" Caption="" FieldName="kategori">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Contract Type" FieldName="contractType">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Total Transaksi" FieldName="totalHarian">
                <PropertiesTextEdit DisplayFormatString="n0" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Total Transaksi Bulan" FieldName="totalBulanan">
                <PropertiesTextEdit DisplayFormatString="n0" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Total Transaksi Tahun" FieldName="totalTahun">
                <PropertiesTextEdit DisplayFormatString="n0" />
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior AllowFocusedRow="false" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
            <SettingsPager PageSize="20">
            </SettingsPager>
            <Settings ShowVerticalScrollBar="True" UseFixedTableLayout="True" 
                VerticalScrollableHeight="350" GroupFormat="{1} {2}" />
            <Styles>
                <AlternatingRow Enabled="True">
                </AlternatingRow>
            </Styles>
        </dx:ASPxGridView>
    </div>


</asp:Content>
