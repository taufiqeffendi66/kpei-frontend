﻿namespace imq.kpei.kbiekos
{
    partial class Req0601report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.tableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellKontrakID = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellFrekuensi = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellKaShort = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellKaLong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellKbShort = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellKbLong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTkShort = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTkLong = new DevExpress.XtraReports.UI.XRTableCell();
            this.LkShort = new DevExpress.XtraReports.UI.XRTableCell();
            this.LkLong = new DevExpress.XtraReports.UI.XRTableCell();
            this.netShort = new DevExpress.XtraReports.UI.XRTableCell();
            this.netLong = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.tblHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellCodeHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellNameHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellContactHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellPhoneHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellEmailHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xtraReport1 = new DevExpress.XtraReports.UI.XtraReport();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lbTitle2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lbTitle1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.lbl_footer2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_footer1 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tableDetail});
            this.Detail.HeightF = 31.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // tableDetail
            // 
            this.tableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableDetail.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tableDetail.Name = "tableDetail";
            this.tableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.tableDetail.SizeF = new System.Drawing.SizeF(776F, 31.25F);
            this.tableDetail.StylePriority.UseBorders = false;
            this.tableDetail.StylePriority.UseFont = false;
            this.tableDetail.StylePriority.UseTextAlignment = false;
            this.tableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellKontrakID,
            this.cellFrekuensi,
            this.cellKaShort,
            this.cellKaLong,
            this.cellKbShort,
            this.cellKbLong,
            this.cellTkShort,
            this.cellTkLong,
            this.LkShort,
            this.LkLong,
            this.netShort,
            this.netLong});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellKontrakID
            // 
            this.cellKontrakID.Name = "cellKontrakID";
            this.cellKontrakID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellKontrakID.StylePriority.UsePadding = false;
            this.cellKontrakID.Weight = 0.36671094381747221D;
            // 
            // cellFrekuensi
            // 
            this.cellFrekuensi.Name = "cellFrekuensi";
            this.cellFrekuensi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellFrekuensi.StylePriority.UsePadding = false;
            this.cellFrekuensi.Weight = 0.32212911605939687D;
            // 
            // cellKaShort
            // 
            this.cellKaShort.Multiline = true;
            this.cellKaShort.Name = "cellKaShort";
            this.cellKaShort.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellKaShort.StylePriority.UsePadding = false;
            this.cellKaShort.Weight = 0.23716872050043558D;
            // 
            // cellKaLong
            // 
            this.cellKaLong.Name = "cellKaLong";
            this.cellKaLong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellKaLong.StylePriority.UsePadding = false;
            this.cellKaLong.Weight = 0.21716680227200558D;
            // 
            // cellKbShort
            // 
            this.cellKbShort.Multiline = true;
            this.cellKbShort.Name = "cellKbShort";
            this.cellKbShort.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellKbShort.StylePriority.UsePadding = false;
            this.cellKbShort.Weight = 0.22213749203689873D;
            // 
            // cellKbLong
            // 
            this.cellKbLong.Name = "cellKbLong";
            this.cellKbLong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellKbLong.StylePriority.UsePadding = false;
            this.cellKbLong.Weight = 0.20940752709651711D;
            // 
            // cellTkShort
            // 
            this.cellTkShort.Name = "cellTkShort";
            this.cellTkShort.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellTkShort.StylePriority.UsePadding = false;
            this.cellTkShort.Weight = 0.22148835983531895D;
            // 
            // cellTkLong
            // 
            this.cellTkLong.Name = "cellTkLong";
            this.cellTkLong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellTkLong.StylePriority.UsePadding = false;
            this.cellTkLong.Weight = 0.22273647664908886D;
            // 
            // LkShort
            // 
            this.LkShort.Name = "LkShort";
            this.LkShort.Weight = 0.23015791118919837D;
            // 
            // LkLong
            // 
            this.LkLong.Name = "LkLong";
            this.LkLong.Weight = 0.26362232328850571D;
            // 
            // netShort
            // 
            this.netShort.Name = "netShort";
            this.netShort.Weight = 0.24363716362758081D;
            // 
            // netLong
            // 
            this.netLong.Name = "netLong";
            this.netLong.Weight = 0.24363716362758081D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 54F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.BottomMargin.HeightF = 51F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0} of {1} pages";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(625F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(151F, 17F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.Text = "Total";
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHeader});
            this.PageHeader.HeightF = 62.5F;
            this.PageHeader.Name = "PageHeader";
            // 
            // tblHeader
            // 
            this.tblHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow3});
            this.tblHeader.SizeF = new System.Drawing.SizeF(776F, 62.5F);
            this.tblHeader.StylePriority.UseBorders = false;
            this.tblHeader.StylePriority.UseFont = false;
            this.tblHeader.StylePriority.UseTextAlignment = false;
            this.tblHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellCodeHeader,
            this.cellNameHeader,
            this.cellContactHeader,
            this.cellPhoneHeader,
            this.cellEmailHeader,
            this.xrTableCell1,
            this.xrTableCell10});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // cellCodeHeader
            // 
            this.cellCodeHeader.Name = "cellCodeHeader";
            this.cellCodeHeader.Text = "Kontrak ID";
            this.cellCodeHeader.Weight = 0.36671091432239028D;
            // 
            // cellNameHeader
            // 
            this.cellNameHeader.Name = "cellNameHeader";
            this.cellNameHeader.Text = "Frekuensi";
            this.cellNameHeader.Weight = 0.32212929302988857D;
            // 
            // cellContactHeader
            // 
            this.cellContactHeader.Name = "cellContactHeader";
            this.cellContactHeader.Text = "Kontrak Awal";
            this.cellContactHeader.Weight = 0.45433539004457241D;
            // 
            // cellPhoneHeader
            // 
            this.cellPhoneHeader.Name = "cellPhoneHeader";
            this.cellPhoneHeader.Text = "Kontrak Baru";
            this.cellPhoneHeader.Weight = 0.43154501913341586D;
            // 
            // cellEmailHeader
            // 
            this.cellEmailHeader.Name = "cellEmailHeader";
            this.cellEmailHeader.Text = "Tutup Kontrak";
            this.cellEmailHeader.Weight = 0.44422463886866809D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Likuidasi";
            this.xrTableCell1.Weight = 0.49378070933477747D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "Net Open";
            this.xrTableCell10.Weight = 0.48727403526628721D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell16,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell11,
            this.xrTableCell17});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.68884020735227891D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Short";
            this.xrTableCell3.Weight = 0.23716858777256678D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Long";
            this.xrTableCell4.Weight = 0.21716692025233342D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Short";
            this.xrTableCell5.Weight = 0.22213724132870197D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Long";
            this.xrTableCell6.Weight = 0.20940765445688328D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "Short";
            this.xrTableCell16.Weight = 0.22148836810258235D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Long";
            this.xrTableCell7.Weight = 0.22273648847068645D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Short";
            this.xrTableCell8.Weight = 0.23015814717603683D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "Long";
            this.xrTableCell9.Weight = 0.26362229379342378D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "Short";
            this.xrTableCell11.Weight = 0.24363704564725303D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Text = "Long";
            this.xrTableCell17.Weight = 0.24363704564725303D;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 47.91667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xtraReport1
            // 
            this.xtraReport1.Name = "xtraReport1";
            this.xtraReport1.PageHeight = 1100;
            this.xtraReport1.PageWidth = 850;
            this.xtraReport1.Version = "12.2";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbTitle2,
            this.xrLine1,
            this.lbTitle1});
            this.ReportHeader.HeightF = 59.375F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lbTitle2
            // 
            this.lbTitle2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbTitle2.ForeColor = System.Drawing.Color.Black;
            this.lbTitle2.LocationFloat = new DevExpress.Utils.PointFloat(201.9865F, 3.250011F);
            this.lbTitle2.Name = "lbTitle2";
            this.lbTitle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle2.SizeF = new System.Drawing.SizeF(574.0134F, 22.37499F);
            this.lbTitle2.StylePriority.UseFont = false;
            this.lbTitle2.StylePriority.UseForeColor = false;
            this.lbTitle2.StylePriority.UseTextAlignment = false;
            this.lbTitle2.Text = "Report Req0601";
            this.lbTitle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine1
            // 
            this.xrLine1.ForeColor = System.Drawing.Color.Black;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.SizeF = new System.Drawing.SizeF(775.9999F, 9F);
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lbTitle1
            // 
            this.lbTitle1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTitle1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbTitle1.ForeColor = System.Drawing.Color.Black;
            this.lbTitle1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 27.00001F);
            this.lbTitle1.Name = "lbTitle1";
            this.lbTitle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTitle1.SizeF = new System.Drawing.SizeF(747.9717F, 22.37499F);
            this.lbTitle1.StylePriority.UseFont = false;
            this.lbTitle1.StylePriority.UseForeColor = false;
            this.lbTitle1.Text = "Report Req0601";
            this.lbTitle1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_footer2,
            this.lbl_footer1});
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lbl_footer2
            // 
            this.lbl_footer2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_footer2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_footer2.ForeColor = System.Drawing.Color.Black;
            this.lbl_footer2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 29.66667F);
            this.lbl_footer2.Name = "lbl_footer2";
            this.lbl_footer2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_footer2.SizeF = new System.Drawing.SizeF(766F, 29.66667F);
            this.lbl_footer2.StylePriority.UseFont = false;
            this.lbl_footer2.StylePriority.UseForeColor = false;
            this.lbl_footer2.Text = "Report Req0601";
            this.lbl_footer2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // lbl_footer1
            // 
            this.lbl_footer1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_footer1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_footer1.ForeColor = System.Drawing.Color.Black;
            this.lbl_footer1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lbl_footer1.Name = "lbl_footer1";
            this.lbl_footer1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_footer1.SizeF = new System.Drawing.SizeF(766F, 29.66667F);
            this.lbl_footer1.StylePriority.UseFont = false;
            this.lbl_footer1.StylePriority.UseForeColor = false;
            this.lbl_footer1.Text = "Report Req0601";
            this.lbl_footer1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // Req0601report
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader,
            this.ReportFooter});
            this.Margins = new System.Drawing.Printing.Margins(40, 34, 54, 51);
            this.Version = "12.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Req0601Report_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.tableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable tblHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell cellNameHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellPhoneHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellEmailHeader;
        private DevExpress.XtraReports.UI.XRTable tableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellKontrakID;
        private DevExpress.XtraReports.UI.XRTableCell cellFrekuensi;
        private DevExpress.XtraReports.UI.XRTableCell cellKaShort;
        private DevExpress.XtraReports.UI.XRTableCell cellKaLong;
        private DevExpress.XtraReports.UI.XRTableCell cellKbShort;
        private DevExpress.XtraReports.UI.XRTableCell cellKbLong;
        private DevExpress.XtraReports.UI.XRTableCell cellTkShort;
        private DevExpress.XtraReports.UI.XRTableCell cellTkLong;
        private DevExpress.XtraReports.UI.XtraReport xtraReport1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lbTitle1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTableCell cellCodeHeader;
        private DevExpress.XtraReports.UI.XRTableCell cellContactHeader;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell LkShort;
        private DevExpress.XtraReports.UI.XRTableCell LkLong;
        private DevExpress.XtraReports.UI.XRTableCell netShort;
        private DevExpress.XtraReports.UI.XRTableCell netLong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lbl_footer1;
        private DevExpress.XtraReports.UI.XRLabel lbl_footer2;
        private DevExpress.XtraReports.UI.XRLabel lbTitle2;
    }
}
