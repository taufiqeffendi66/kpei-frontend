﻿using System;
using System.Web.UI;
using DevExpress.Web.ASPxPopupControl;

namespace imq.kpei.kbiekos
{
    public static class Utilities
    {

        /// <summary>
        /// This will show a message dialog with the message provided.
        /// </summary>
        /// <param name="message">Message to display in the dialog box</param>
        /// <param name="title">The title of the popup control</param>
        /// <param name="buttonText">The text to display in the button</param>
        /// <param name="p">The current page</param>
        public static void ShowMessage(string message, string title, string buttonText, Page p)
        {
            ASPxPopupControl popup = new ASPxPopupControl
            {
                AllowDragging = true,
                AutoUpdatePosition = true,
               // CssFilePath = "~/App_Themes/Aqua/{0}/styles.css",
                CssPostfix = "Aqua",
                EncodeHtml = false,
                HeaderText = title,
               // ImageFolder = "~/App_Themes/Aqua/{0}/",
                Modal = true,
                ShowFooter = true,
                //ShowOnPageLoad = true,
                Text = message,
                Width = new System.Web.UI.WebControls.Unit(300)
            };

            p.Form.Controls.Add(popup);
            popup.ShowOnPageLoad = true;
            popup.ClientInstanceName = String.Format("modalPopup_{0}", popup.ID);

            #region Attempt at adding a Dev Express button to the footer

            //DevExpress.Web.ASPxEditors.ASPxButton button = new DevExpress.Web.ASPxEditors.ASPxButton();
            //button.ClientSideEvents.Click = String.Format("{0}.Hide();", popup.ClientInstanceName);
            //button.Text = buttonText;
            //button.EnableClientSideAPI = false;

            //using (System.IO.StringWriter writer = new System.IO.StringWriter())
            //using (System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(writer))
            //{
            //    button.RenderControl(htw);
            //    popup.FooterText = String.Format("<div style='width:100%; text-align:right;'>{0}</div>", writer.ToString());
            //}

            #endregion

            popup.FooterText = String.Format("<div style='width:100%; text-align:right;'><input type='button' value='{0}' onclick='{1}.Hide();'></input></div>", buttonText, popup.ClientInstanceName);
            popup.FooterStyle.Paddings.Padding = new System.Web.UI.WebControls.Unit(5);
            popup.ModalBackgroundStyle.CssClass = "modalPopupBackground";
            popup.PopupHorizontalAlign = DevExpress.Web.ASPxClasses.PopupHorizontalAlign.WindowCenter;
            popup.PopupVerticalAlign = DevExpress.Web.ASPxClasses.PopupVerticalAlign.WindowCenter;
        }
    }
}
