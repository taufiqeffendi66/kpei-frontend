﻿using System;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.bankWS;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;


namespace imq.kpei.kbiekos
{
    public partial class Req01report : XtraReport
    {
        private statusBalance[] balance;
        public String hari;
        public String broker;
        private String url;

        public Req01report()
        {
            InitializeComponent();
        }

        private statusBalance[] getBalance()
        {
            WebClient client = null;
            try
            {
                string date = hari;
                DateTime dt = Convert.ToDateTime(date);

                
                if (broker == "null")
                {
                    url = "http://192.168.138.19:8090/statusbalance?datatimestamp=" + hari;
                }
                else
                {
                    
                    url = "http://192.168.138.19:8090/statusbalance?datatimestamp=" + hari + "&kodebroker=" + broker;
                }

                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<statusBalance>));
                List<statusBalance> result = ser.ReadObject(ms) as List<statusBalance>;

                balance = result.ToArray();

                lbTitle.Text = "Status Balance Anggota Kliring Derivatif tanggal " + dt.ToString("dd MMMM yyyy",
                    new System.Globalization.CultureInfo("id-ID"));

                DataSource = balance;

                if (client != null)
                {
                   XRBinding binding = null;
                        binding = new XRBinding("Text", DataSource, "kodeBroker");
                        cellBK.DataBindings.Add(binding);
                        binding = new XRBinding("Text", DataSource, "namaBank");
                        cellBank.DataBindings.Add(binding);
                        binding = new XRBinding("Text", DataSource, "nomerRekening");
                        cellAccNo.DataBindings.Add(binding);
                        binding = new XRBinding("Text", DataSource, "strBalance");
                        cellBalance.DataBindings.Add(binding);
                        binding = new XRBinding("Text", DataSource, "strDataTimeStamp2");
                        cellDT.DataBindings.Add(binding);
                    
                   
                }              
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
               client.Dispose();
            }

            return balance;
            
        }

        private void Req01Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            getBalance();
        }
    }
}
