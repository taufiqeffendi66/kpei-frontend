﻿using System;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.bankWS;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Globalization;


namespace imq.kpei.kbiekos
{
    public partial class Req0601report : XtraReport
    {
        private kontrakBerjangka[] kontrak;
        public String hari;
        public String title;
        public String id;
        public String kbie1;
        public String kbie2;
        public String bulan;
        public String tahun;

        public Req0601report()
        {
            InitializeComponent();
        }

        private kontrakBerjangka[] getKontrak()
        {
            WebClient client = null;
            try
            {
                string date = hari;
                DateTime dt = Convert.ToDateTime(date);
                String url;

                if (id == "1")
                    url = "http://192.168.138.19:8090/rekapperdagangankontrakberjangka?tanggaldata=" + hari;
                else
                {
                   // int iMonthNo = Convert.ToDateTime("01-" + bulan + "-2011").Month;


                   // int iMonthNo = DateTime.ParseExact(bulan, "MMMM", System.Globalization.CultureInfo.InvariantCulture).Month;



                    url = "http://192.168.138.19:8090/rekapperdagangankbiebulanan?bulan=" + bulan + "&tahun=" + tahun;
                }
                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<kontrakBerjangka>));
                List<kontrakBerjangka> result = ser.ReadObject(ms) as List<kontrakBerjangka>;
                kontrak = result.ToArray();

                DataSource = kontrak;

                if (client != null)
                {
                    XRBinding binding = null;
                    binding = new XRBinding("Text", DataSource, "kontrakId");
                    cellKontrakID.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "frekuensi");
                    cellFrekuensi.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "kontrakAwalShort");
                    cellKaShort.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "kontrakAwalLong");
                    cellKaLong.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "kontrakBaruShort");
                    cellKbShort.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "kontrakBaruLong");
                    cellKbLong.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tutupKontrakShort");
                    cellTkShort.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "tutupKontrakLong");
                    cellTkLong.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "likuidasiShort");
                    LkShort.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "likuidasiLong");
                    LkLong.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "netOpenShortManual");
                    netShort.DataBindings.Add(binding);
                    binding = new XRBinding("Text", DataSource, "netOpenLongManual");
                    netLong.DataBindings.Add(binding);
                }

                string fullMonthName = new DateTime(2015, int.Parse(bulan), 1).ToString("MMMM", CultureInfo.CreateSpecificCulture("id-ID"));

                if (title != "")
                {
                    if (id == "1")
                    {
                        lbTitle2.Text = title.ToUpper();
                        lbTitle1.Text = "Rekapitulasi Perdangangan KBIE Tanggal " + dt.ToString("dd MMMM yyyy",
                            new System.Globalization.CultureInfo("id-ID"));
                    }
                    else
                    {
                        lbTitle2.Text = title.ToUpper();
                        lbTitle1.Text = "Rekapitulasi Perdangangan KBIE Bulan " + fullMonthName + " Tahun " + tahun;
                    }
                }
                else
                {
                    lbTitle2.Text = "";
                    if (id == "1")
                        lbTitle1.Text = "Rekapitulasi Perdangangan KBIE Tanggal " + dt.ToString("dd MMMM yyyy",
                            new System.Globalization.CultureInfo("id-ID"));
                    else
                        lbTitle1.Text = "Rekapitulasi Perdangangan KBIE Bulan " + fullMonthName + " Tahun " + tahun;
                }


                lbl_footer1.Text = "Jumlah AK Setor Collateral : " + kbie1;
                lbl_footer2.Text = "Jumlah AK Setor Dana Pengaman : " + kbie2;
            }
            catch(Exception e)
            {
                e.Message.ToString();
            }
            finally
            {

            }

            return kontrak;

        }


        private void Req0601Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            getKontrak();
        }
    }
}
