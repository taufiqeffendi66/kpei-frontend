﻿using System;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using Common;
using Common.bankWS;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using System.Collections.Generic;


namespace imq.kpei.kbiekos
{
    public partial class Req0602Report : XtraReport
    {
        private kos[] kos;
        public String hari;
        public String title;
        public String id;
        public String kbie1;
        public String kbie2;
        public String bulan;
        public String bulan2;
        public String tahun;


        public Req0602Report()
        {
            InitializeComponent();
        }

        private kos[] getKos()
        {
            WebClient client = null;
            try
            {
                
                string date = hari;
                DateTime dt = Convert.ToDateTime(date);
                String url;

                if (id == "1")
                    url = "http://192.168.138.19:8090/rekapperdagangankos?tanggaldata=" + hari;
                else
                {
                    int iMonthNo = Convert.ToDateTime("01-" + bulan + "-2011").Month;
                    url = "http://192.168.138.19:8090/rekapperdagangankosbulanan?bulan=" + iMonthNo + "&tahun=" + tahun;
                }
                client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoker the rest method
                byte[] data = client.DownloadData(url);
                //put the downloaded data in memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<kos>));
                List<kos> result = ser.ReadObject(ms) as List<kos>;
                kos = result.ToArray();

                DataSource = kos;

                XRBinding binding = null;
                binding = new XRBinding("Text", DataSource, "series");
                cellKontrakID.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "frekuensi");
                cellFrekuensi.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "kontrakAwalShort");
                cellKaShort.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "kontrakAwalLong");
                cellKaLong.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "kontrakBaruShort");
                cellKbShort.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "kontrakBaruLong");
                cellKbLong.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "tutupKontrakShort");
                cellTkShort.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "tutupKontrakLong");
                cellTkLong.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "likuidasiShort");
                LkShort.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "likuidasiLong");
                LkLong.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "exerciseShort");
                cellExerShort.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "exerciseLong");
                cellExerLong.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "netOpenShortManual");
                netShort.DataBindings.Add(binding);
                binding = new XRBinding("Text", DataSource, "netOpenLongManual");
                netLong.DataBindings.Add(binding);

                if (title != "")
                {
                    if (id == "1")
                    {
                        lbTitle2.Text = title.ToUpper();
                        lbTitle.Text = "Rekapitulasi Perdangangan KOS Tanggal " + dt.ToString("dd MMMM yyyy",
                            new System.Globalization.CultureInfo("id-ID"));
                    }
                    else
                    {
                        lbTitle2.Text = title.ToUpper();
                        lbTitle.Text = "Rekapitulasi Perdangangan KOS Bulan " + bulan2 + " Tahun " + tahun;
                    }
                }
                else
                {
                    if (id == "1")
                        lbTitle.Text = "Rekapitulasi Perdangangan KOS Tanggal " + dt.ToString("dd MMMM yyyy",
                            new System.Globalization.CultureInfo("id-ID"));
                    else
                        lbTitle.Text = "Rekapitulasi Perdangangan KOS Bulan " + bulan2 + " Tahun " + tahun;
                }

                lbl_footer1.Text = "Jumlah AK Setor Collateral : " + kbie1;
                lbl_footer2.Text = "Saham Induk : " + kbie2;
            }
            catch
            {

            }
            finally
            { 
            
            }

            return kos;
            
        }


        private void Req0601Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            getKos();
        }
    }
}
