﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeView;
using Common;
using Common.wsUser;
using Common.wsUserGroupPermission;
using System.Web;
using log4net;
using log4net.Config;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Net;
using System.IO;
using System.Collections.Generic;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Collections;
using System.Web.Script;
using System.Web.Script.Serialization;

namespace imq.kpei.kbiekos
{
    public partial class atest : System.Web.UI.Page
    {
        private indikator[] chart;
        int x = 0;
        public int temp = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private indikator[] getIndikator()
        //private void getData()
        {
            var url = "http://localhost:8080/indikatoralert";
            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/json";
            // invoke the REST method
            byte[] data = client.DownloadData(url);
            // put the downloaded data in a memory stream
            MemoryStream ms = new MemoryStream();
            ms = new MemoryStream(data);
            // deserialize from json
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<indikator>));
            List<indikator> result = ser.ReadObject(ms) as List<indikator>;

            chart = result.ToArray();

            temp = chart.Length;
            // Label1.Text = result[0].actualValue.ToString();

            return chart;
        }

        protected void test_Tick(object sender, EventArgs e)
        {
            label.Text = "test";

            getIndikator();

            temp = 2;
            string[] str1 = new string[temp];
            string[] str2 = new string[temp];
            string[] str3 = new string[temp];


            for (x = 0; x < temp; x++)
            {
                str1[x] = chart[x].memberId.ToString();
                str2[x] = chart[x].sid.ToString();
                str3[x] = chart[x].actualValue.ToString();
            }


            ClientScriptManager cs = Page.ClientScript;
            string csName = "notifyMe";
            Type csType = this.GetType();

            for (int i = 0; i < 2; i++)
            {
                string currentName = string.Format("{0}{1}", csName, i);
                if (!cs.IsStartupScriptRegistered(csType, currentName))
                {
                    x = i + 1;
                    string csText = string.Format("notifyMe('" + str1[i] + "','" + str2[i] + "','" + str3[i] + "','" + x + "');");
                    cs.RegisterStartupScript(csType, currentName, csText, true);
                }
            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "notifyMe();", true);
        }
    }
}