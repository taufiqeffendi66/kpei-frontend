﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;

namespace imq.kpei.kbiekos
{
    public partial class Req07 : System.Web.UI.Page
    {
        private AccountSidAudittrail[] audit;
        private String url;
        private String aUserLogin;
        private String aUserMember;
        private String hari = DateTime.Now.ToString("yyyy-MM-dd");
        //private String hari = "2016-03-07";

        protected void Page_Load(object sender, EventArgs e)
        {
            //strHari = "0";
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            if (!IsPostBack)
            {
                btnApprove.Enabled = false;

                //dateEdit.Value = DateTime.Now;
                Session["dateChange"] = dateEdit.Text.ToString();
                Session["sidChange"] = "";
                //dateEdit.Value = "2016-03-07";
                gridView.DataBind();

                
            }

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            
            DateTime fromDateValue;
            var formats = new[] { "yyyy-MM-dd" };
            if (dateEdit.Text != String.Empty)
            {
                if (DateTime.TryParseExact(dateEdit.Text, formats, CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateValue))
                {
                    Session["dateChange"] = dateEdit.Text.ToString();
                    Session["sidChange"] = tbSid.Text.ToString();
                    //getData();
                    //gridView.DataBind();
                    if(tbSid.Text != String.Empty)
                        gridView.FilterExpression = String.Format("[sid] = '{0}' and [newStr2] = '{1}'", Session["sidChange"].ToString(),Session["dateChange"].ToString());
                    else
                        gridView.FilterExpression = String.Format("[newStr2] = '{0}'", Session["dateChange"].ToString());
                
                }
                else
                {
                    // do for invalid date
                    ShowMessage("Invalid Date!");
                }
            }
            else
            {
                Session["dateChange"] = dateEdit.Text.ToString();
                Session["sidChange"] = tbSid.Text.ToString();
                //gridView.DataBind();
                gridView.FilterExpression = String.Format("[sid] = '{0}'", Session["sidChange"].ToString());
            }                
                     
        }

       

        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
        }

        protected void tbSid_TextChanged(object sender, EventArgs e)
        {

        }

        private AccountSidAudittrail[] getAudit()
        {
            if (Session["temp"] == "1")
            {
                //String test123 = Session["sidChange"].ToString();
                dateEdit.Value = Session["dateChange"].ToString();
                if (Session["sidChange"] != "" || Session["sidChange"] != String.Empty)
                {
                    if (Session["dateChange"] != String.Empty)
                    {
                        url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail?logdate=" + Session["dateChange"].ToString() + "&sid=" + Session["sidChange"].ToString();
                        tbSid.Text = Session["sidChange"].ToString();
                        dateEdit.Text = Session["dateChange"].ToString();
                    }
                    else
                    {
                        url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail?sid=" + Session["sidChange"].ToString();
                        tbSid.Text = Session["sidChange"].ToString();
                    }
                    
                }
                else if (Session["dateChange"] != String.Empty)
                {
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail?logdate=" + Session["dateChange"].ToString();
                    dateEdit.Text = Session["dateChange"].ToString();
                }
                else
                {
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail";
                }
                Session["temp"] = "0";
            }
            else
            {
                if (tbSid.Text != String.Empty)
                {
                    if(dateEdit.Text != String.Empty)
                        url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail?logdate=" + dateEdit.Text.ToString() + "&sid=" + tbSid.Text.ToString();
                    else
                        url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail?sid=" + tbSid.Text; 
                }
                else if(dateEdit.Text != String.Empty)
                {
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail?logdate=" + dateEdit.Text.ToString();
                }
                else
                {
                    url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "accountsidaudittrail";
                }
            }

            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-Type"] = "application/json";
                //invoke rest method
                byte[] data = client.DownloadData(url);
                //put downloaded data to an memory stream
                MemoryStream ms = new MemoryStream();
                ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<AccountSidAudittrail>));
                List<AccountSidAudittrail> result = ser.ReadObject(ms) as List<AccountSidAudittrail>;

                audit = result.ToArray();

                if (audit.Length > 0)
                {
                    btnApprove.Enabled = true;
                }
                else
                {
                    btnApprove.Enabled = false;
                }
                return audit;
            }
            catch(Exception ex) {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg +"\");", true);
                return null;
            }
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = null;
            gridView.DataSource = getAudit();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            string strID = hfAudit.Get("id") == null ? "-" : hfAudit.Get("id").ToString().Trim();
            string strWaktu = hfAudit.Get("newStr") == null ? "-" : hfAudit.Get("newStr").ToString().Trim();
            string strSid = hfAudit.Get("sid") == null ? "-" : hfAudit.Get("sid").ToString().Trim();
            string strCmCode = hfAudit.Get("cmCode") == null ? "-" : hfAudit.Get("cmCode").ToString().Trim();
            string strAccId = hfAudit.Get("accountId") == null ? "-" : hfAudit.Get("accountId").ToString().Trim();
            string strCurrID = hfAudit.Get("currentId") == null ? "-" : hfAudit.Get("currentId").ToString().Trim();
            string strStatus = hfAudit.Get("status") == null ? "-" : hfAudit.Get("status").ToString().Trim();
            string strAction = hfAudit.Get("action") == null ? "-" : hfAudit.Get("action").ToString().Trim();
            string strRedirect = String.Format("ApprovReq07New.aspx?stat=1&id={0}&newStr={1}&sid={2}&cmCode={3}&accountId={4}&currentId={5}&status={6}&action={7}&hari={8}",
                Server.UrlEncode(strID), Server.UrlEncode(strWaktu), Server.UrlEncode(strSid),
                Server.UrlEncode(strCmCode), Server.UrlEncode(strAccId), Server.UrlEncode(strCurrID), Server.UrlEncode(strStatus),
                Server.UrlEncode(strAction), dateEdit.Text.ToString());
            if (strID != "-" & strWaktu == "-" & strSid == "-" & strCmCode == "-" & strAccId == "-" & strCurrID == "-" & strStatus == "-" & strAction == "-")
            {
                ShowMessage("Silakan Pilih Data Terlebih Dahulu");
            }
            else if (strAction == "approve")
            {
                //btnApprove.AutoPostBack = false;
                ShowMessage("Data yang anda pilih telah di Approve");
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Data sudah Approved!');", true);
            } 
            //else if (strID != "-" & strWaktu != "-" & strSid != "-" & strCmCode != "-" & strAccId != "-" & strCurrID != "-" & strStatus != "-" & strAction != "-")
            else
            {
                Session["dateChange"] = dateEdit.Text.ToString();
                Session["sidChange"] = tbSid.Text.ToString();
                btnApprove.AutoPostBack = true;
                Response.Redirect(strRedirect, false);
                Context.ApplicationInstance.CompleteRequest();
            }
           // else
           // {
             //   ShowMessage("Silakan Pilih Data Terlebih Dahulu");
                //btnApprove.AutoPostBack = false;
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Data Belum di pilih asfdsafadfdfadfafddsfdasfafds!');", true);
            //}           
        }

        protected void gridView_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
        {
            //getData();
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            gridView.DataBind();
            
        }

        protected void btnCsv_Click(object sender, EventArgs e)
        {
            //getData();
            gridExport.FileName = "Req07-" + dateEdit.Text.ToString();
            gridExport.WriteCsvToResponse();
        }

        protected void btnPdf_Click(object sender, EventArgs e)
        {
            // getData();
            gridExport.FileName = "Req07-" + dateEdit.Text.ToString();
            gridExport.WritePdfToResponse();
        }

        private void ShowMessage(string info)
        {
            string mystring = @"<script type='text/javascript'>
                                jAlert('" + info + @"','Information');
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        //private void getData()
        //{
        //    String kDate = Session["dateChange"].ToString();
        //    hari = kDate;
        //    String kSid = "";

        //    if (tbSid.Text != String.Empty)
        //    {
        //        kSid = tbSid.Text.ToString();
        //    }
        //    else
        //    {
        //        kSid = "";
        //    }

        //    string search = String.Empty;


        //    if (kSid != String.Empty)
        //    {
        //        if (kDate != String.Empty)
        //        {
        //            kSid = kSid.Replace("'", "''");
        //            kSid = kSid.Replace("-", "--");
        //            kSid = String.Format("[sid] = '{0}'", kSid);
        //            search = kSid;
        //        }
        //    }
        //    else
        //    {
        //        if (kDate != String.Empty)
        //        {
        //            if (kSid == String.Empty)
        //                search = String.Format("[sid] like '%%'");
        //            //search = String.Format("[sid] like '%I%' or [sid] like '%1%' or [sid] like '%0%' or [sid] like '%2%' or [sid] like '%3%' or [sid] like '%4%' or [sid] like '%5%' or [sid] like '%6%' or [sid] like '%7%' or [sid] like '%8%' or [sid] like '%9%' or [sid] like '%D%' or [sid] like '%%'");
        //        }
        //        else
        //            search = String.Empty;
        //    }
        //    Label1.Text = search;
        //    //Label1.Visible = true;
        //    gridView.FilterExpression = search;


        //}
    }
}