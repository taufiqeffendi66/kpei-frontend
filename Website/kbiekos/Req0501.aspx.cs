﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.wsUser;

namespace imq.kpei.kbiekos
{
    public partial class Req0501 : System.Web.UI.Page
    {
        private transaksiDerivatif[] derivatif;
        String hari = DateTime.Now.ToString("yyyy-MM-dd");
        //String hari = "2016-03-23";

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                dateEdit.Value = DateTime.Now;
                Session["dateChange"] = dateEdit.Text.ToString();

                //gridView.Columns["totalHarian"].Caption = "Total Transaksi " + DateTime.Now.Day.ToString() + " " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year.ToString();
                //gridView.Columns["totalBulanan"].Caption = "Total Transaksi Bulan " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year.ToString();
                //gridView.Columns["totalTahun"].Caption = "Total Transaksi Tahun " + DateTime.Now.Year.ToString();
                gridView.DataBind();
                ApplyLayout();
            }
        }

        protected void gridView_DataBinding(object sender, EventArgs e)
        {
            gridView.DataSource = getDerivatif();
        }

        private transaksiDerivatif[] getDerivatif()
        {
            String url = ImqSession.GetConfig("TOOLS_REST_SERVER") + "transaksiderivatif?tanggaldata=" + Session["dateChange"];
           
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                //invoker the Rest method
                byte[] data = client.DownloadData(url);
                //put downloaded data to memory stream
                MemoryStream ms = new MemoryStream(data);
                //deserialize from json
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<transaksiDerivatif>));
                List<transaksiDerivatif> result = ser.ReadObject(ms) as List<transaksiDerivatif>;

                derivatif = result.ToArray();
                return derivatif;
            }
            catch (Exception ex)
            {
                string msg;
                msg = ". Web Service is Offline!";
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"" + string.Format(ex.Message) + msg + "\");", true);
                return null;
            }
        }

        void ApplyLayout()
        {
            DateTime dt = Convert.ToDateTime(dateEdit.Text);

            gridView.Columns["totalHarian"].Caption = "Total Transaksi " + dt.Day.ToString() + " " + dt.ToString("MMMM") + " " + dt.Year.ToString();
            gridView.Columns["totalBulanan"].Caption = "Total Transaksi Bulan " + dt.ToString("MMMM") + " " + dt.Year.ToString();
            gridView.Columns["totalTahun"].Caption = "Total Transaksi Tahun " + dt.Year.ToString();
            gridView.BeginUpdate();
            try
            {
                gridView.ClearSort();
                gridView.GroupBy(gridView.Columns["kategori"]);
            }
            finally
            {
                gridView.EndUpdate();
            }
            gridView.ExpandAll();
        }

        protected void gridView_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ApplyLayout();
        }

        //private void getData()
        //{
        //    string kDate = dateEdit.Text.Trim();
        //    // hari = kDate;
        //    String kBroker = "";
        //    String kStatus = "";

        //    if (cmbStatus.Text != String.Empty)
        //    {
        //        kStatus = cmbStatus.Value.ToString();
        //    }
        //    if (cmbKbroker.Text != String.Empty)
        //    {
        //        kBroker = cmbKbroker.Value.ToString();
        //    }
        //    if (kDate != String.Empty)
        //    {
        //        hari = kDate;
        //    }
        //    else
        //    {
        //        hari = DateTime.Now.ToString("yyyy-MM-dd");
        //    }

        //    string search = String.Empty;


        //    if (kStatus != String.Empty)
        //    {
        //        kStatus = kStatus.Replace("'", "''");
        //        kStatus = kStatus.Replace("-", "--");
        //        kStatus = String.Format("[settlementStatus] like '%{0}%'", kStatus);
        //    }
        //    if (kBroker != String.Empty)
        //    {
        //        kBroker = kBroker.Replace("'", "''");
        //        kBroker = kBroker.Replace("-", "--");
        //        kBroker = String.Format("[codeOfBroker] like '%{0}%'", kBroker);
        //    }
        //    if (kDate != String.Empty)
        //    {
        //        kDate = kDate.Replace("'", "''");
        //        //kDate = kDate.Replace("-", "--");
        //        kDate = String.Format("[settlementDate] = '{0}'", kDate);
        //    }

        //    if (kStatus != String.Empty)
        //    {
        //        if (kBroker != String.Empty)
        //            if (kDate != String.Empty)
        //                search = String.Format("{0} and {1} and {2}", kStatus, kBroker, kDate);
        //            else
        //                search = kStatus;
        //    }
        //    else if (kBroker != String.Empty)
        //    {
        //        if (kDate != String.Empty)
        //            search = String.Format("{0} and {1}", kBroker, kDate);
        //        else
        //            search = kBroker;
        //    }
        //    else
        //    {
        //        if (kDate != String.Empty)
        //            search = kDate;
        //        else
        //            search = String.Empty;
        //    }
        //    Label1.Text = search;
        //    Label1.Visible = true;

        //    gridView.FilterExpression = search;
        //}

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();

            
            gridView.DataBind();
            ApplyLayout();
        }

        protected void ASPxTimer1_Tick(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
            ApplyLayout();
        }

        protected void dateEdit_DateChanged(object sender, EventArgs e)
        {
            Session["dateChange"] = dateEdit.Text.ToString();
        }

    }
}