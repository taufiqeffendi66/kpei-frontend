﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Common.wsCommonList;
using System.Text;

namespace imq.kpei.transaction.blocked_collateral
{
    public partial class blocked_collateral : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                ////redirect to login page if not logged in
                //ImqSession.Validate(this);
                ////redirect to blank page if accessed directly
                //ImqSession.ValidatedAction(this);           

                string jmsServer = HttpContext.Current.Request.Url.Host; //ImqSession.GetConfig("JMS_SERVER_WS");
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string blockedCollateralTopic = ImqSession.GetConfig("JMS_TOPIC_BLOCKED_COLLATERAL");
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string user = Session["SESSION_USER"].ToString();
                string password = Session.SessionID;
                string member = Session["SESSION_USERMEMBER"].ToString();
                string userJMS = user + "-" + member;

                //UiCommonListService ws = ImqWS.GetCommonListService();
                //commonListDto[] membersDto;
                //try
                //{
                //    membersDto = ws.listMember(Session["SESSION_USERMEMBER"].ToString());
                //}
                //catch
                //{
                //    membersDto = new commonListDto[0];
                //}
                //StringBuilder membersSb = new StringBuilder();
                //for (int i = 0; i < membersDto.Length; i++)
                //{
                //    if (i > 0)
                //        membersSb.Append(",");
                //    membersSb.Append("{");
                //    membersSb.Append("\"abbr\":");
                //    membersSb.Append("\"" + membersDto[i].id + "\"");
                //    membersSb.Append(",\"name\":");
                //    membersSb.Append("\"" + membersDto[i].name + "\"");
                //    membersSb.Append("}");
                //}

                //commonListDto[] contractsDto;
                //try
                //{
                //    contractsDto = ws.listContract();
                //}
                //catch
                //{
                //    contractsDto = new commonListDto[0];
                //}
                //StringBuilder contractsSb = new StringBuilder();
                //for (int i = 0; i < contractsDto.Length; i++)
                //{
                //    if (i > 0)
                //        contractsSb.Append(",");
                //    contractsSb.Append("{");
                //    contractsSb.Append("\"abbr\":");
                //    contractsSb.Append("\"" + contractsDto[i].id + "\"");
                //    contractsSb.Append(",\"name\":");
                //    contractsSb.Append("\"" + contractsDto[i].name + "\"");
                //    contractsSb.Append("}");
                //}

                //string sessionId = Session["SESSION_USERMEMBER"].ToString();
                //string memberId;
                //if (sessionId.Equals("kpei"))
                //{

                //    memberId = "";

                //}
                //else
                //    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];
                //commonListDto[] sidDto;

                //try
                //{
                //    sidDto = ws.listSid(sessionId, memberId);
                //}
                //catch
                //{
                //    sidDto = new commonListDto[0];
                //}
                //StringBuilder sidSb = new StringBuilder();
                //for (int i = 0; i < sidDto.Length; i++)
                //{
                //    if (i > 0)
                //        sidSb.Append(",");
                //    sidSb.Append("{");
                //    sidSb.Append("\"abbr\":");
                //    sidSb.Append("\"" + sidDto[i].id + "\"");
                //    sidSb.Append(",\"name\":");
                //    sidSb.Append("\"" + sidDto[i].name + "\"");
                //    sidSb.Append("}");
                //}



                ScriptManager.RegisterStartupScript(this, this.GetType(), "initPage", @"                    
<link rel=""stylesheet"" type=""text/css"" href=""../../js/extjs/resources/css/ext-all-gray.css"" />
<script type=""text/javascript"" src=""../../js/util/page.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ext-all.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ux/data/PagingMemoryProxy.js""></script>
<script type=""text/javascript"" src=""../../js/amq/stomp.js""></script>

<script type=""" + "text/javascript" + @""">
    var url = """ + jmsServer + @""";
    var login = """ + userJMS + @""";
    var passcode = """ + password + @""";
    var blockedCollateralTopic = """ + blockedCollateralTopic + @""";    
    var pingTopic = """ + pingTopic + @""";    
        var contractTypes = [
            {""abbr"":""O"",""name"":""Option""},
            {""abbr"":""F"",""name"":""Future""}
        ];

</script>

<script type=""text/javascript"" src=""blocked_collateral.js""></script>

", false);
            }
        }
    }
}
