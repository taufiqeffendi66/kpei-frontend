﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.blocked_collateral
{
    public partial class blockedCollateralReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public blockedCollateralReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "memberId");
            cl02.DataBindings.Add("Text", DataSource, "sid");
            cl03.DataBindings.Add("Text", DataSource, "role");
            cl04.DataBindings.Add("Text", DataSource, "netSettlement");
            cl05.DataBindings.Add("Text", DataSource, "mtm");
            cl06.DataBindings.Add("Text", DataSource, "initialMargin");
        }
    }
}
