﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var exerciseSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var exerciseInitialData = [];
var sss;
var idTrading;
var cb;
var storeCb;
var recordIndex = -1;

var arrExercise = [];
var endProses = true;
var pageSize = 20;
var tick = 200;

function addArrExercise(s, force) {
    data = eval('(' + s + ')');
    arrExercise.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrExercise.length > 0) {
        endProses = false;
        if (arrExercise.length > pageSize)
            max = pageSize;
        else
            max = arrExercise.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrExercise.shift()
            addToExercise(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToExercise(data) {
    if (exerciseInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.exerciseNo === "undefined") || (data.exerciseNo == null)) {
        return;
    }

    var aStatus;
    switch (data.status) {
        case "0": aStatus = "New";
            break;
        case "1": aStatus = "Executed";
            break;
        case "2": aStatus = "Rejected";
            break;
    }
    data.status = aStatus;

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.buyMemberId);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.buyMemberId, name: data.buyMemberId });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sId);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sId, name: data.sId });


    //is data exists
    var recordIndex = -1;
    for (i = 0; i < exerciseInitialData.length; i++) {
        if (data.exerciseNo == exerciseInitialData[i].exerciseNo) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        exerciseInitialData.push(data);
    } else {
        exerciseInitialData[recordIndex].exerciseNo = data.exerciseNo;
        exerciseInitialData[recordIndex].buyMemberId = data.buyMemberId;
        exerciseInitialData[recordIndex].exerciseTime = data.exerciseTime;
        exerciseInitialData[recordIndex].role = data.role;
        exerciseInitialData[recordIndex].sId = data.sId;
        exerciseInitialData[recordIndex].tradingId = data.tradingId;
        exerciseInitialData[recordIndex].series = data.series;
        exerciseInitialData[recordIndex].volume = data.volume;
        exerciseInitialData[recordIndex].status = data.status;
        exerciseInitialData[recordIndex].fee = data.fee;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}

function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addexercise(s, force) {
    if (exerciseInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.exerciseNo === "undefined") || (data.exerciseNo == null)) {
        return;
    }

    var aStatus;
    switch(data.status)
    {
        case "0" : aStatus = "New";
            break;
        case "1" : aStatus = "Executed";
            break;
        case "2" : aStatus = "Rejected";
            break;
    }
    data.status = aStatus;

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.buyMemberId);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.buyMemberId, name: data.buyMemberId });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sId);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sId, name: data.sId });


    //is data exists
    var recordIndex = -1;
    for (i = 0; i < exerciseInitialData.length; i++) {
        if (data.exerciseNo == exerciseInitialData[i].exerciseNo) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        exerciseInitialData.push(data);
    } else {
        exerciseInitialData[recordIndex].exerciseNo = data.exerciseNo;
        exerciseInitialData[recordIndex].buyMemberId = data.buyMemberId;
        exerciseInitialData[recordIndex].exerciseTime = data.exerciseTime;
        exerciseInitialData[recordIndex].role = data.role;
        exerciseInitialData[recordIndex].sId = data.sId;
        exerciseInitialData[recordIndex].tradingId = data.tradingId;
        exerciseInitialData[recordIndex].series = data.series;
        exerciseInitialData[recordIndex].volume = data.volume;
        exerciseInitialData[recordIndex].status = data.status;
        exerciseInitialData[recordIndex].fee = data.fee;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeexerciseTopic(direction) {
    if ((!(typeof exerciseSubscriptionId === "undefined")) && (exerciseSubscriptionId != null)) {
        client.unsubscribe(exerciseSubscriptionId);
        exerciseSubscriptionId = null;
    }

    for (; exerciseInitialData.length > 0; )
        exerciseInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var exerciseSubscritptionHeader = { 'selector': selector };
        exerciseSubscriptionId = client.subscribe(exerciseTopic, function (message) {
            addArrExercise(message.body, true);
        }, exerciseSubscritptionHeader);
    } else {
        exerciseSubscriptionId = client.subscribe(exerciseTopic, function (message) {
            addArrExercise(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=exercise&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue(),
        success: function (response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrExercise(obj[ia].exerciseData, false);
            }
        },
        failure: function (response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof exerciseSubscriptionId === "undefined")) && (exerciseSubscriptionId != null)) {
        client.unsubscribe(exerciseSubscriptionId);
        exerciseSubscriptionId = null;
    }

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    window.open('./exerciseReportView.aspx?type=exercise&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}

function putExercise(Role, SID, ContractId, TradeID, Quantity, permission) {
    Ext.Ajax.request({
        url: '../request.aspx?type=exerciseNew&permission=' + permission + '&maxId=' + Role + '&contractId=' + ContractId + '&sid=' + SID + '&contractType=' + TradeID + '&minId=' + Quantity,
        success: function (response, opts) {
            var obj = eval('(' + response.responseText + ')');
            if (obj.length > 0) {
                newData = eval('(' + obj[0].exerciseNew + ')');
                if (newData.result == -2)
                    alert('Failed on store exercise to database');
                else if (newData.result == -1) {
                    Ext.getCmp('btnNew').disable();
                    alert('Failed on time validation');
                }
                else
                    alert('Succes');

            }
            //            for (i = 0; i < obj.length; i++) {
            //                addArrExercise(obj[i].exerciseData, false);
            //            }
        },
        failure: function (response, opts) {
        }
    });
}

var exerciseFormWin;
var exerciseForm;

var exerciseConfirmFormWin;
var exerciseConfirmForm;

var exerciseValidateFormWin;
var exerciseValidateForm;

function createExerciseFormWin() {
    if (!exerciseFormWin) {
        exerciseForm = Ext.widget('form', {
            border: false,
            //bodyPadding: 10,
            bodyStyle: 'padding:5px 5px 0',
            width: 288,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaults: {
                margins: '0 0 10 0',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'combobox',
                    name: 'comboSid',
                    id: 'comboSid',
                    fieldLabel: 'SID',
                    displayField: 'name',
                    valueField: 'name',
                    store: new Ext.create('Ext.data.Store', { model: 'Member', data: tradingIds }),
                    queryMode: 'local',
                    typeAhead: false,
                    editable: false,
                    listeners: { select:
                        { fn: function (combo, value) {
                            var sidstore = Ext.getCmp('comboSid').getStore();
                            var sidValue = Ext.getCmp('comboSid').getValue();
                            var ss = sidValue.substr(0, 3);
                            switch (ss) {
                                case 'SCD':
                                    Ext.getCmp('displayfieldRole').setValue('H');
                                    break;
                                case 'IDD':
                                    Ext.getCmp('displayfieldRole').setValue('C');
                                    break;
                            }

                            combo = Ext.getCmp('comboContractId');
                            combo.clearValue();
                            combo.getStore().removeAll();
                            var index = sidstore.find('name', Ext.getCmp('comboSid').getValue());
                            var record = sidstore.getAt(index);
                            idTrading = record.data.abbr;
                            Ext.Ajax.request({
                                url: '../request.aspx?type=contractId&sid=' + idTrading, // Ext.getCmp('comboSid').getValue(),
                                success: function (response, opts) {
                                    var obj = eval('(' + response.responseText + ')');
                                    sss = Ext.getCmp('comboSid').getValue();
                                    for (i = 0; i < obj.length; i++) {
                                        newData = eval('(' + obj[i].contractIdData + ')');
                                        combo.getStore().add({ abbr: newData.abbr, name: newData.name });
                                    }
                                },
                                failure: function (response, opts) {
                                }
                            });
                        }
                        }
                    }
                },

                {
                    xtype: 'displayfield',
                    name: 'displayfieldRole',
                    id: 'displayfieldRole',
                    fieldLabel: 'Role',
                    value: ''
                    //                    xtype: 'combobox',
                    //                    name: 'comboRole',
                    //                    id: 'comboRole',
                    //                    fieldLabel: 'Role',
                    //                    displayField: 'name',
                    //                    valueField: 'id',
                    //                    queryMode: 'local',
                    //                    typeAhead: false,
                    //                    editable: false,
                    //                    store:  new Ext.data.SimpleStore({
                    //                        fields: ['id','name'],
                    //                        data: [ ["C","C"],["H","H"]  ]
                    //                        })
                },
                {
                    xtype: 'combobox',
                    name: 'comboContractId',
                    id: 'comboContractId',
                    fieldLabel: 'Contract ID',
                    displayField: 'abbr',
                    valueField: 'abbr',
                    store: new Ext.create('Ext.data.Store', { model: 'Member' }),
                    queryMode: 'local',
                    typeAhead: false,
                    editable: false
                },
                {
                    xtype: 'numberfield',
                    name: 'numberfieldQuantity',
                    id: 'numberfieldQuantity',
                    fieldLabel: 'Quantity',
                    minValue: 1,
                    value: ''
                }
            ],

            buttons:
                [
                    {
                        text: 'Continue',
                        handler: function () {
                            if (this.up('form').getForm().isValid()) {
                                this.up('window').hide();
                                Ext.Ajax.request({
                                    url: '../request.aspx?type=exerciseValidate&maxId=' + Ext.getCmp('displayfieldRole').getValue() + '&contractId=' + Ext.getCmp('comboContractId').getValue() + '&sid=' + sss + '&contractType=' + idTrading + '&minId=' + Ext.getCmp('numberfieldQuantity').getValue(),
                                    success: function (response, opts) {
                                        var obj = eval('(' + response.responseText + ')');
                                        if (obj.length > 0) {
                                            newData = eval('(' + obj[0].exerciseValidate + ')');
                                            if (newData.validate >= 0) {
                                                createExerciseConfirmFormWin();
                                                Ext.getCmp('displayfieldConfirmRole').setValue(Ext.getCmp('displayfieldRole').getValue());
                                                Ext.getCmp('displayfieldConfirmSid').setValue(Ext.getCmp('comboSid').getValue());
                                                Ext.getCmp('displayfieldConfirmContractId').setValue(Ext.getCmp('comboContractId').getValue());
                                                Ext.getCmp('displayfieldConfirmQuantity').setValue(Ext.getCmp('numberfieldQuantity').getValue());
                                                exerciseConfirmFormWin.show();
                                            }
                                            else {
                                                var minta = parseInt(Ext.getCmp('numberfieldQuantity').getValue());
                                                var hasil = newData.validate + minta;
                                                createExerciseValidateFormWin();
                                                Ext.getCmp('displayfieldValidateRole').setValue(Ext.getCmp('displayfieldRole').getValue());
                                                Ext.getCmp('displayfieldValidateSid').setValue(Ext.getCmp('comboSid').getValue());
                                                Ext.getCmp('displayfieldValidateContractId').setValue(Ext.getCmp('comboContractId').getValue());
                                                Ext.getCmp('displayfieldValidateAvailable').setValue(hasil);
                                                Ext.getCmp('numberfieldValidateQuantity').setMaxValue(hasil);
                                                Ext.getCmp('numberfieldValidateQuantity').setValue(Ext.getCmp('numberfieldQuantity').getValue());
                                                exerciseValidateFormWin.show();
                                            }
                                        }
                                    },
                                    failure: function (response, opts) {
                                    }
                                });

                            }
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: function () {
                            this.up('form').getForm().reset();
                            this.up('window').hide();
                        }
                    }
                ]
        });

        exerciseFormWin = Ext.widget('window', {
            title: 'Exercise Form',
            closeAction: 'hide',
            width: 300,
            height: 205,
            minHeight: 205,
            layout: 'fit',
            resizable: false,
            modal: true,
            items: exerciseForm
        });
    }
    exerciseForm.getForm().reset();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function createExerciseValidateFormWin() {
    if (!exerciseValidateFormWin) {
        exerciseValidateForm = Ext.widget('form', {
            border: false,
            //bodyPadding: 10,
            bodyStyle: 'padding:5px 5px 0',
            width: 288,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaults: {
                margins: '0 0 10 0',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateSid',
                    id: 'displayfieldValidateSid',
                    fieldLabel: 'SID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateRole',
                    id: 'displayfieldValidateRole',
                    fieldLabel: 'Role',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateContractId',
                    id: 'displayfieldValidateContractId',
                    fieldLabel: 'Contract ID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateAvailable',
                    id: 'displayfieldValidateAvailable',
                    fieldLabel: 'Available',
                    value: ''
                },
                {
                    xtype: 'numberfield',
                    name: 'numberfieldValidateQuantity',
                    id: 'numberfieldValidateQuantity',
                    fieldLabel: 'Quantity',
                    minValue: 1,
                    value: ''
                }
            ],

            buttons:
                [
                    {
                        text: 'Submit',
                        handler: function () {
                            var minta = parseInt(Ext.getCmp('numberfieldValidateQuantity').getValue());
                            var available = parseInt(Ext.getCmp('displayfieldValidateAvailable').getValue());
                            if ((minta > 0) && (minta <= available)) {
                                if (this.up('form').getForm().isValid()) {
                                    this.up('window').hide();
                                    createExerciseConfirmFormWin();
                                    Ext.getCmp('displayfieldConfirmRole').setValue(Ext.getCmp('displayfieldValidateRole').getValue());
                                    Ext.getCmp('displayfieldConfirmSid').setValue(Ext.getCmp('displayfieldValidateSid').getValue());
                                    Ext.getCmp('displayfieldConfirmContractId').setValue(Ext.getCmp('displayfieldValidateContractId').getValue());
                                    Ext.getCmp('displayfieldConfirmQuantity').setValue(Ext.getCmp('numberfieldValidateQuantity').getValue());
                                    exerciseConfirmFormWin.show();
                                }
                            }
                            else {
                                alert('Quantity value is bigger then Available value');
                            }
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: function () {
                            this.up('form').getForm().reset();
                            this.up('window').hide();
                        }
                    }
                ]
        });
        
        exerciseValidateFormWin = Ext.widget('window', {
            title: 'Exercise Validate',
            closeAction: 'hide',
            width: 300,
            height: 205,
            minHeight: 205,
            layout: 'fit',
            resizable: false,
            modal: true,
            items: exerciseValidateForm
        });
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
function createExerciseConfirmFormWin() {
    if (!exerciseConfirmFormWin) {
        roleItems = [];
        if (isMaker == '1')
            roleItems.push({ boxLabel: 'Maker', name: 'rb-col', inputValue: 1 });
        if (isDirectChecker == '1')
            roleItems.push({ boxLabel: 'Direct Checker', name: 'rb-col', inputValue: 2 });
        if (isDirectApprover == '1')
            roleItems.push({ boxLabel: 'Direct Approve', name: 'rb-col', inputValue: 3 });
        exerciseConfirmForm = Ext.widget('form', {
            border: false,
            //bodyPadding: 10,
            bodyStyle: 'padding:5px 5px 0',
            width: 288,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaults: {
                margins: '0 0 10 0',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Permission',
                    name: 'radiogroupRole',
                    id: 'radiogroupRole',
                    columns: 1,
                    items: roleItems
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmSid',
                    id: 'displayfieldConfirmSid',
                    fieldLabel: 'SID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmRole',
                    id: 'displayfieldConfirmRole',
                    fieldLabel: 'Role',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmContractId',
                    id: 'displayfieldConfirmContractId',
                    fieldLabel: 'Contract ID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmQuantity',
                    id: 'displayfieldConfirmQuantity',
                    fieldLabel: 'Quantity',
                    value: ''
                }
            ],

            buttons:
                [
                    {
                        text: 'Submit',
                        handler: function () {
                            var aPermission = Ext.getCmp('radiogroupRole').getValue()['rb-col'];
                            if (aPermission != undefined) {                                
                                putExercise(Ext.getCmp('displayfieldConfirmRole').getValue(),
                                        Ext.getCmp('displayfieldConfirmSid').getValue(),
                                        Ext.getCmp('displayfieldConfirmContractId').getValue(),
                                        idTrading,
                                        Ext.getCmp('displayfieldConfirmQuantity').getValue(),
                                        aPermission);

                                this.up('form').getForm().reset();
                                this.up('window').hide();
                            }else
                                alert('Select Permission first');
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: function () {
                            this.up('form').getForm().reset();
                            this.up('window').hide();
                        }
                    }
                ]
        });

        exerciseConfirmFormWin = Ext.widget('window', {
            title: 'Exercise Confirmation',
            closeAction: 'hide',
            width: 300,
            height: 260,
            minHeight: 260,
            layout: 'fit',
            resizable: false,
            modal: true,
            items: exerciseConfirmForm
        });
    }
}

Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var exerciseModel = Ext.define('exerciseModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'exerciseNo' },
               { name: 'buyMemberId' },
               { name: 'exerciseTime' },
               { name: 'role' },
               { name: 'sId' },
               { name: 'tradingId' },
               { name: 'series' },
               { name: 'volume' },
               { name: 'status' },
               { name: 'fee' }
            ]
    });

    var exerciseReader = new Ext.data.reader.Json({
        model: 'exerciseModel'
    }, exerciseModel);

    var exerciseStore = Ext.create('Ext.data.Store', {
        id: 'exerciseStore',
        model: 'exerciseModel',
        data: exerciseInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(exerciseInitialData),
        autoLoad: true,
        autoSync: true,
        reader: exerciseReader
    });

    // create the Grid
    var exerciseGrid = Ext.create('Ext.grid.Panel', {
        store: exerciseStore,
        id: 'exerciseGrid',
        stateful: false,
        stateId: 'exerciseGrid',
        columns: [
                    new Ext.grid.RowNumberer({header: "No"}),
                    { text: 'Exercise<br>No', dataIndex: 'exerciseNo', width: 70, sortable: true, align: 'right', draggable: false },
                    { text: 'Time', dataIndex: 'exerciseTime', width: 100, sortable: true, align: 'center', draggable: false },
                    { text: 'Buy Member ID', dataIndex: 'buyMemberId', width: 150, sortable: true, align: 'center', draggable: false },
                    { text: 'Role', dataIndex: 'role', width: 75, sortable: true, align: 'center', draggable: false },
                    { text: 'SID', dataIndex: 'sId', width: 150, sortable: true, align: 'left', draggable: false },
                    { text: 'Contract ID', dataIndex: 'series', width: 150, sortable: true, align: 'left', draggable: false },
                    { text: 'Quantity', dataIndex: 'volume', width: 100, sortable: true, align: 'right', draggable: false },
                    { text: 'Status', dataIndex: 'status', width: 100, sortable: true, align: 'center', draggable: false }
            ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: exerciseStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    exerciseGrid.setLoading(false, false);
    exerciseStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
                { fn: function (combo, value) {
                    combo = Ext.getCmp('sidCombo');
                    combo.clearValue();
                    combo.getStore().removeAll();
                    Ext.Ajax.request({
                        url: '../request.aspx?type=sid&memberId=' + Ext.getCmp('memberCombo').getValue(),
                        success: function (response, opts) {
                            var obj = eval('(' + response.responseText + ')');
                            for (i = 0; i < obj.length; i++) {
                                newData = eval('(' + obj[i].sidData + ')');
                                combo.getStore().add({ abbr: newData.abbr, name: newData.name });
                            }
                        },
                        failure: function (response, opts) {
                        }
                    });
                }
                }
        }
    });

    memberCombo.on('select', function (box, record, index) {

    });

    Ext.define('ContractId', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

//    var contractIdFormStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: []
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
                    { xtype: 'label', text: 'Contract ID: ' },
                    contractIdCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
        //{xtype: 'label', text: 'Contract Type: ' },
        //contractTypeCombo,
                    {xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function () {
                            subscribeexerciseTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button',
                        text: ' New ',
                        id: 'btnNew',
                        handler: function () {
                            createExerciseFormWin();
                            exerciseFormWin.show();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Exercise List',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: exerciseGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });
    if ((memberId == 'kpei') || ( bNew==0 )) {
        Ext.getCmp('btnNew').disable();
    } else if ((isMaker == '0') && (isDirectChecker == '0') && (isDirectApprover == '0')) {
        Ext.getCmp('btnNew').disable();
    }
    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function (str) {
    };
    var onconnect = function (frame) {
        Ext.getCmp('exerciseGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function (message) {
        });

        subscribeexerciseTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function () {
        if ((!(typeof exerciseSubscriptionId === "undefined")) && (exerciseSubscriptionId != null)) {
            client.unsubscribe(exerciseSubscriptionId);
            exerciseSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function () {
        });
    }
});
