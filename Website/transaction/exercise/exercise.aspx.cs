﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Common;
using Common.wsCommonList;
using System.Text;
using Common.wsUserGroupPermission;
using Common.wsTransaction;
using System.Web;

namespace imq.kpei.transaction.exercise
{
    public partial class exercise : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private int isMaker = 0;
        private int isDirectChecker = 0;
        private int isDirectApprover = 0;
        private int bNew = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                //ImqSession.ValidateAction(this);

                aUserLogin = Session["SESSION_USER"].ToString();
                aUserMember = Session["SESSION_USERMEMBER"].ToString();

                ////redirect to login page if not logged in
                //ImqSession.Validate(this);
                ////redirect to blank page if accessed directly
                //ImqSession.ValidatedAction(this);           

                string jmsServer = HttpContext.Current.Request.Url.Host;
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string exerciseTopic = ImqSession.GetConfig("JMS_TOPIC_EXERCISE");
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string user = aUserLogin;
                string password = Session.SessionID;
                string member = aUserMember;
                string userJMS = user + "-" + member;
                UiCommonListService ws = ImqWS.GetCommonListService();
                //ws.Url = ImqSession.GetConfig("WS_UI") + "/UiCommonList";

                //commonListDto[] membersDto;
                //try
                //{
                //    membersDto = ws.listMember(aUserMember);
                //}
                //catch
                //{
                //    membersDto = new commonListDto[0];
                //}
                //StringBuilder membersSb = new StringBuilder();
                //for (int i = 0; i < membersDto.Length; i++)
                //{
                //    if (i > 0)
                //        membersSb.Append(",");
                //    membersSb.Append("{");
                //    membersSb.Append("\"abbr\":");
                //    membersSb.Append("\"" + membersDto[i].id + "\"");
                //    membersSb.Append(",\"name\":");
                //    membersSb.Append("\"" + membersDto[i].name + "\"");
                //    membersSb.Append("}");
                //}

                //commonListDto[] contractsDto;
                //try
                //{
                //    contractsDto = ws.listContract();
                //}
                //catch
                //{
                //    contractsDto = new commonListDto[0];
                //}
                //StringBuilder contractsSb = new StringBuilder();
                //for (int i = 0; i < contractsDto.Length; i++)
                //{
                //    if (i > 0)
                //        contractsSb.Append(",");
                //    contractsSb.Append("{");
                //    contractsSb.Append("\"abbr\":");
                //    contractsSb.Append("\"" + contractsDto[i].id + "\"");
                //    contractsSb.Append(",\"name\":");
                //    contractsSb.Append("\"" + contractsDto[i].name + "\"");
                //    contractsSb.Append("}");
                //}

                string sessionId = aUserMember;
                string memberId;
                if (sessionId.Equals("kpei"))
                {
                    memberId = "";
                }
                else
                    memberId = aUserMember;// Request.QueryString["memberId"];

                //commonListDto[] sidDto;
                //try
                //{
                //    sidDto = ws.listSid(sessionId, memberId);
                //}
                //catch
                //{
                //    sidDto = new commonListDto[0];
                //}
                //StringBuilder sidSb = new StringBuilder();
                //for (int i = 0; i < sidDto.Length; i++)
                //{
                //    if (i > 0)
                //        sidSb.Append(",");
                //    sidSb.Append("{");
                //    sidSb.Append("\"abbr\":");
                //    sidSb.Append("\"" + sidDto[i].id + "\"");
                //    sidSb.Append(",\"name\":");
                //    sidSb.Append("\"" + sidDto[i].name + "\"");
                //    sidSb.Append("}");
                //}

                commonListDto[] tradingIdDto = null;
                StringBuilder tradingIdSb = new StringBuilder();
                if (memberId != "")
                {
                    try
                    {
                        tradingIdDto = ws.listTradingIdFromMemberPosition(sessionId, memberId);
                    }
                    catch
                    {
                        tradingIdDto = new commonListDto[0];
                    }

                    if ( tradingIdDto != null)
                        for (int i = 0; i < tradingIdDto.Length; i++)
                        {
                            if (i > 0)
                                tradingIdSb.Append(",");
                            tradingIdSb.Append("{");
                            tradingIdSb.Append("\"abbr\":");
                            tradingIdSb.Append("\"" + tradingIdDto[i].id + "\"");
                            tradingIdSb.Append(",\"name\":");
                            tradingIdSb.Append("\"" + tradingIdDto[i].name + "\"");
                            tradingIdSb.Append("}");
                        }
                }

                UiDataService wst = ImqWS.GetTransactionService();
                if (wst.validateExerciseTime())
                    bNew = 1;
                else
                    bNew = 0;

                Permission();


                ScriptManager.RegisterStartupScript(this, this.GetType(), "initPage", @"                    
<link rel=""stylesheet"" type=""text/css"" href=""../../js/extjs/resources/css/ext-all-gray.css"" />
<script type=""text/javascript"" src=""../../js/util/page.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ext-all.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ux/data/PagingMemoryProxy.js""></script>
<script type=""text/javascript"" src=""../../js/amq/stomp.js""></script>

<script type=""" + "text/javascript" + @""">

    var bNew = " + bNew + @";
    var isMaker = " + isMaker + @";
    var isDirectChecker = " + isDirectChecker + @";
    var isDirectApprover = " + isDirectApprover + @";
    var url = """ + jmsServer + @""";
    var login = """ + userJMS + @""";
    var passcode = """ + password + @""";
    var exerciseTopic = """ + exerciseTopic + @""";    
    var pingTopic = """ + pingTopic + @""";    
    var memberId = """ + member + @""";    
        var contractTypes = [
            {""abbr"":""O"",""name"":""Option""},
            {""abbr"":""F"",""name"":""Future""}
        ];


        var tradingIds = [" + tradingIdSb.ToString() + @"];


</script>

<script type=""text/javascript"" src=""exercise.js""></script>

", false);
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Exercise");
            if (dtFMP != null)
                for (int n = 0; n < dtFMP.Length; n++)
                {
                    if (dtFMP[n].editMaker)
                        isMaker = 1;
                    if (dtFMP[n].editDirectChecker)
                        isDirectChecker = 1;
                    if (dtFMP[n].editDirectApproval)
                        isDirectApprover = 1;
                }
        }
    }
}

