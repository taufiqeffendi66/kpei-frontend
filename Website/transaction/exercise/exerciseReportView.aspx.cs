﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using System.Text;
using Common.wsTransaction;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.exercise
{
    public partial class exerciseReportView : System.Web.UI.Page
    {
        exerciseDto[] exercises;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            //if (!ImqSession.ValidateAction(this))
            //{
            //    responseEmpty();
            //    return;
            //}

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ReportViewer1.Report = CreateReport();
        }

        private void getData()
        {
            try
            {
                string dataType = Request.QueryString["type"];
                string sessionId = Session["SESSION_USERMEMBER"].ToString();
                string memReq = Request.QueryString["memberId"];
                string memberId;
                if (sessionId.Equals("kpei"))
                {
                    if (memReq.Equals("null"))
                        memberId = "";
                    else
                        memberId = memReq;
                }
                else
                    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];

                string contractId = Request.QueryString["contractId"];
                if (contractId != null && contractId.Equals("null"))
                    contractId = "";

                string sid = Request.QueryString["sid"];
                if (sid != null && sid.Equals("null"))
                    sid = "";

                string contractType = Request.QueryString["contractType"];
                if (contractType != null && contractType.Equals("null"))
                    contractType = "";

                string minId = Request.QueryString["minId"];
                string maxId = Request.QueryString["maxId"];
                string direction = Request.QueryString["direction"];
                if (string.IsNullOrEmpty(dataType))
                {
                    responseEmpty();
                    return;
                }
                UiDataService ws = new UiDataService();
                ws.Url = ImqSession.GetConfig("WS_TRANSACTION") + "/UiData";
                switch (dataType)
                {
                    case "exercise":
                        exercises = ws.getExercise(sessionId, memberId, contractId, sid, contractType, minId, maxId, "all");
                        if (exercises != null)
                            for (int i = 0; i < exercises.Length; i++)
                                switch (exercises[i].status)
                                {
                                    case "0":
                                        exercises[i].status = "New";
                                        break;
                                    case "1":
                                        exercises[i].status = "Executed";
                                        break;
                                    case "2":
                                        exercises[i].status = "Rejected";
                                        break;
                                }
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }

        XtraReport CreateReport()
        {
            if (IsPostBack)
            {
                getData();
            }
            // Create a report. 
            exerciseReportForm report = new exerciseReportForm();

            // Bind the report to the list. 
            report.DataSource = exercises;
            report.Name = ImqSession.getFormatFileSave("ExerciseReport");
            report.SetBoundLabel();
            return report;
        }

        private void responseEmpty()
        {
            Response.Clear();
            Response.ContentType = "text/plain";
            Response.Write("[]");
            Response.Flush();
            Response.End();
        }

        private string IntDateToStr(int iDate)
        {
            int tahun, bulan, tgl;
            int tmp;
            tahun = iDate / 10000;
            tmp = iDate % 10000;
            bulan = tmp / 100;
            tgl = tmp % 100;
            return string.Format("{0}-", tahun) + string.Format("{0}-", bulan) + string.Format("{0}", tgl);
        }
    }
}
