﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.exercise
{
    public partial class exerciseReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public exerciseReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "exerciseNo");
            cl02.DataBindings.Add("Text", DataSource, "exerciseTime");
            cl03.DataBindings.Add("Text", DataSource, "buyMemberId");
            cl04.DataBindings.Add("Text", DataSource, "role");
            cl05.DataBindings.Add("Text", DataSource, "sid");
            cl06.DataBindings.Add("Text", DataSource, "series");
            cl07.DataBindings.Add("Text", DataSource, "volume");
            cl08.DataBindings.Add("Text", DataSource, "status");
        }
    }
}
