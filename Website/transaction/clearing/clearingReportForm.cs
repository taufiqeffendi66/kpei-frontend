﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.clearing
{
    public partial class clearingReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public clearingReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {

            cl01.DataBindings.Add("Text", DataSource, "memberID");
            cl02.DataBindings.Add("Text", DataSource, "sID");
            cl03.DataBindings.Add("Text", DataSource, "role");
            cl04.DataBindings.Add("Text", DataSource, "series");
            cl05.DataBindings.Add("Text", DataSource, "contractType");
            cl06.DataBindings.Add("Text", DataSource, "prevSell");
            cl07.DataBindings.Add("Text", DataSource, "prevBuy");
            cl08.DataBindings.Add("Text", DataSource, "openS");
            cl09.DataBindings.Add("Text", DataSource, "openB");
            //cl10.DataBindings.Add("Text", DataSource, "closeB");
            //cl11.DataBindings.Add("Text", DataSource, "closeS");
            cl10.DataBindings.Add("Text", DataSource, "liquidationS");
            cl11.DataBindings.Add("Text", DataSource, "liquidationB");
            cl12.DataBindings.Add("Text", DataSource, "exercise");
            cl13.DataBindings.Add("Text", DataSource, "assignment");
            cl14.DataBindings.Add("Text", DataSource, "netS");
            cl15.DataBindings.Add("Text", DataSource, "netB");
            cl16.DataBindings.Add("Text", DataSource, "premium");
            cl17.DataBindings.Add("Text", DataSource, "gainLoss");
            cl18.DataBindings.Add("Text", DataSource, "penalty");
            cl19.DataBindings.Add("Text", DataSource, "compensation");
            cl20.DataBindings.Add("Text", DataSource, "settlement");
        }

    }
}
