﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="trade.aspx.cs"
    Inherits="imq.kpei.transaction.sample.trade" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:content id="Content1" contentplaceholderid="main" runat="server">

    <div class="content">
        <div class="title">TRANSACTION LIST</div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Member ID"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxComboBox ID="dtMemberID" runat="server"></dx:ASPxComboBox></td>
                    <td><dx:ASPxButton ID="ASPxButton1" runat="server" Text="..."></dx:ASPxButton></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Contract ID"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxComboBox ID="dtContractID" runat="server"></dx:ASPxComboBox></td>
                    <td><dx:ASPxButton ID="ASPxButton2" runat="server" Text="..."></dx:ASPxButton></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Contract Type "></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxTextBox ID="dtContractType" runat="server" Width="170px"></dx:ASPxTextBox></td>
                    <td><dx:ASPxButton ID="ASPxButton3" runat="server" Text="Go"></dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gvApproval" runat="server" AutoGenerateColumns="False" Width="500px" ClientInstanceName="grid">
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Date" FieldName="date"><propertiestextedit displayformatstring="yyyy-MM-dd"></propertiestextedit></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Trade Time"> <PropertiesTextEdit DisplayFormatString="hh:mm:ss"> </PropertiesTextEdit> </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Trade No."> </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Member ID"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Trading Id"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Role (C/H)"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Contract ID" ></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Contract Type (O/F)"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="8" Caption="Buy / Sell"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="9" Caption="Price"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="10" Caption="Quantity"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="11" Caption="Premium"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="12" Caption="Fee"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="13" Caption="MTM Last Price"></dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:content>
