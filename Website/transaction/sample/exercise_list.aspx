﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="exercise_list.aspx.cs" Inherits="imq.kpei.transaction.exercise_list" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <div class="content">
        <div class="title">EXERCISE LIST</div>
        <div>
            <table>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Member ID"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxComboBox ID="dtMemberID" runat="server"></dx:ASPxComboBox></td>
                    <td><dx:ASPxButton ID="ASPxButton1" runat="server" Text="..."></dx:ASPxButton></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Contract ID"></dx:ASPxLabel></td>
                    <td>:</td>
                    <td><dx:ASPxComboBox ID="dtContractID" runat="server"></dx:ASPxComboBox></td>
                    <td><dx:ASPxButton ID="ASPxButton2" runat="server" Text="..."></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="ASPxButton3" runat="server" Text="Go"></dx:ASPxButton></td>
                    <td><dx:ASPxButton ID="ASPxButton4" runat="server" Text="New"></dx:ASPxButton></td>
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxGridView ID="gvApproval" runat="server" AutoGenerateColumns="False" Width="500px" ClientInstanceName="grid">
                <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Exercise No."><propertiestextedit displayformatstring="yyyy-MM-dd"></propertiestextedit></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Time"> <PropertiesTextEdit DisplayFormatString="hh:mm:ss"> </PropertiesTextEdit> </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Buy Member ID"> </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Role"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="SID"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Contract ID"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Quantity" ></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Status"></dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
        </div>
    </div>



</asp:Content>

