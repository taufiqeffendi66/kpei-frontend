﻿<%@ Page Title="Price" Language="C#" AutoEventWireup="true" CodeBehind="price.aspx.cs" Inherits="imq.kpei.transaction.sample.price" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">Price</div>
        <table>
            <tr>
                <td style="width:128px">Contract ID</td>
                <td><dx:ASPxComboBox ID="cmbContractID" runat="server" ClientInstanceName="cmbcontract">
                </dx:ASPxComboBox></td>
            </tr>
            <tr>
                <td style="width:128px">Contract Type</td>
                <td><dx:ASPxComboBox ID="cmbContractType" runat="server" ClientInstanceName="cmbct">
                </dx:ASPxComboBox></td>                        
            </tr>                       
        </table>
        <br />
        <dx:ASPxButton ID="btnGO" runat="server" Text="GO" />
        <br />
        <dx:ASPxGridView ID="gvPrice" ClientInstanceName="grid" runat="server" AutoGenerateColumns="false"
            CssClass="grid" >
            <Columns>
                <dx:GridViewDataTextColumn Caption="Contract ID" VisibleIndex="0" FieldName="contractID" />
                <dx:GridViewDataTextColumn Caption="Contract Type" VisibleIndex="1" FieldName="contractType" />                     
                <dx:GridViewDataTextColumn Caption="Closing Price/HPH" VisibleIndex="2" FieldName="closingPrice" />
                <dx:GridViewDataTextColumn Caption="Last Price" VisibleIndex="3" FieldName="lastPrice" />
                <dx:GridViewDataTextColumn Caption="+/-" VisibleIndex="4" FieldName="change" />
                <dx:GridViewDataDateColumn Caption="Maturity Date" VisibleIndex="5" FieldName="maturity" />
            </Columns>
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
           
            <Styles>                        
                <Header CssClass="gridHeader">
                </Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
</asp:Content>
