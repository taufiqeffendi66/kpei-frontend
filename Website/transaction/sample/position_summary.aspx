﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="position_summary.aspx.cs" Inherits="imq.kpei.transaction.position_summary" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <div class="content">
        <div class="title">POSITION SUMMARY</div>
        <div>
            <table>
                <tr>
                    <td style="width:128px">Member ID</td>
                    <td><dx:ASPxComboBox ID="dtMemberID" ClientInstanceName="clMemberID" runat="server" Width="120px" /></td>                        
                </tr>
                <tr>
                    <td style="width:128px">Contract ID</td>
                    <td><dx:ASPxComboBox ID="cmbContractID" ClientInstanceName="clcontractID" runat="server" 
                            Width="120px"></dx:ASPxComboBox></td>
                </tr>
                <tr>
                    <td style="width:128px">Contract type</td>
                    <td><dx:ASPxTextBox ID="txtContractType" ClientInstanceName="clcontracttype" runat="server" Width="120px"></dx:ASPxTextBox></td>
                </tr>                
            </table>
         </div>
         <div>
            <table width="100%">
                <tr>
                    <td><dx:ASPxButton ID="btnGo" runat="server" Text="Go" onclick="btnGo_Click" Width="60px"> </dx:ASPxButton></td>
                    <td></td>
                    
                    <td align="right"><dx:ASPxButton ID="btnExercise" runat="server" Text="Exercise"> </dx:ASPxButton></td>                    
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <dx:ASPxGridView ID="gridSchedule" runat="server" ClientInstanceName="grid" Width="100%">
                        <Columns>
                            <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Member ID" ></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="1" Caption="SID" ></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Role" ></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Contract ID" ></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Contract type" ></dx:GridViewDataTextColumn>
                            <dx:GridViewBandColumn VisibleIndex="5" Caption="Net">
                                <Columns>
                                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Sell" ></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Buy" ></dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="6" Caption="Freq" ></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Volume" ></dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    </td>
                </tr>
            </table>
       
            
        </div>

    </div>

</asp:Content>

