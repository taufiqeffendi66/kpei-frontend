﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="account_client.aspx.cs" Inherits="imq.kpei.transaction.account_client" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
            <div class="title">ACCOUNT - CLIENT</div>
            <div>
                <table>
                    <tr>
                        <td><dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Member ID"></dx:ASPxLabel></td>
                        <td>:</td>
                        <td><dx:ASPxComboBox ID="dtMemberID" runat="server"></dx:ASPxComboBox></td>
                        <td><dx:ASPxButton ID="ASPxButton3" runat="server" Text="Go"></dx:ASPxButton></td>
                    </tr>
                </table>
            </div>
            <div>
                <dx:ASPxGridView ID="gvApproval" runat="server" AutoGenerateColumns="False" Width="500px" ClientInstanceName="grid">
                    <Columns>
                        <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Member ID"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="1" Caption="SID"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Trading ID"></dx:GridViewDataTextColumn>
                        <dx:GridViewBandColumn VisibleIndex="3" Caption="Collateral">
                            <Columns>
                                <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Bank Account number"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Block"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Free"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Total"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Withdraw Request"></dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                    </Columns>             
                </dx:ASPxGridView>
            </div>
        </div>

</asp:Content>

