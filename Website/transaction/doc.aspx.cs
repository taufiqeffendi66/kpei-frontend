﻿using System;
using System.Collections.Generic;
using System.Linq;

using Common;

namespace imq.kpei.transaction
{
    public partial class doc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
        }
    }
}
