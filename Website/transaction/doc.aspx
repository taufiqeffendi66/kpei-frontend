﻿<%@ Page Title="Detail Offline Collateral" Language="C#" AutoEventWireup="true" CodeBehind="doc.aspx.cs" Inherits="imq.kpei.transaction.doc" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">Detail Offline Collateral</div>
        <table>
            <tr>
                <td style="width:128px">Member ID</td>
                <td><dx:ASPxComboBox ID="cmbMemberID" runat="server" ClientInstanceName="cmbmember">
                </dx:ASPxComboBox></td>                        
            </tr>
        </table>
        <br />
        <dx:ASPxButton ID="btnGO" runat="server" Text="GO" AutoPostBack="false" />
        <br />
        <dx:ASPxGridView ID="gvClearing" ClientInstanceName="grid" runat="server" AutoGenerateColumns="false"
            CssClass="grid" >
            <Columns>
                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" />
                <dx:GridViewDataTextColumn Caption="Instrument Type" VisibleIndex="1" />                     
                <dx:GridViewDataTextColumn Caption="Account No" VisibleIndex="2" />
                <dx:GridViewDataTextColumn Caption="Bank" VisibleIndex="3" />
                <dx:GridViewDataTextColumn Caption="Instrument No" VisibleIndex="4" />
                <dx:GridViewDataDateColumn Caption="Maturity Date" VisibleIndex="5" />
                <dx:GridViewDataTextColumn Caption="Currency" VisibleIndex="6" />                     
                <dx:GridViewDataTextColumn Caption="Nominal" VisibleIndex="7" />
                <dx:GridViewDataTextColumn Caption="Haircut" VisibleIndex="8" />
                <dx:GridViewDataTextColumn Caption="Currency Rate" VisibleIndex="9" />
                <dx:GridViewDataDateColumn Caption="Value*" VisibleIndex="10" />
            </Columns>
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
           
            <Styles>                        
                <Header CssClass="gridHeader">
                </Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
</asp:Content>
