﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.trade
{
    public partial class tradeReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public tradeReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "tradeNumber");
            cl02.DataBindings.Add("Text", DataSource, "tradeDate");
            cl03.DataBindings.Add("Text", DataSource, "tradeTime");
            cl04.DataBindings.Add("Text", DataSource, "memberID");
            cl05.DataBindings.Add("Text", DataSource, "sid");
            cl06.DataBindings.Add("Text", DataSource, "tradingID");
            cl07.DataBindings.Add("Text", DataSource, "role");
            cl08.DataBindings.Add("Text", DataSource, "contractID");
            cl09.DataBindings.Add("Text", DataSource, "contractType");
            cl10.DataBindings.Add("Text", DataSource, "buysellPosition");
            cl11.DataBindings.Add("Text", DataSource, "price");
            cl12.DataBindings.Add("Text", DataSource, "quantity");
            cl13.DataBindings.Add("Text", DataSource, "premium");
            cl14.DataBindings.Add("Text", DataSource, "fee");
            cl15.DataBindings.Add("Text", DataSource, "mtmLastPrice");
        }
    }
}
