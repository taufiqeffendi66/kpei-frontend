﻿function resizeFgHeight(tbl, newHeight) {
    grd = tbl.closest('.flexigrid');
    var heightWithChromes = grd.height();

    var contentHeight = $('.bDiv', grd).height();
    var chromesHeight = heightWithChromes - contentHeight;

    $('.bDiv', grd).height(newHeight - chromesHeight);
    grd.css('height', newHeight);
}

function resizeFgContentHeight(tbl, newHeight) {
    grd = tbl.closest('.flexigrid');
    $('.bDiv', grd).height(newHeight);
}

function createContent() {
    $("#tableTrade").flexigrid({
        url: 'post2.php',
        dataType: 'json',
        colModel: [
                { display: 'Date', name: 'date', width: 100, sortable: true, align: 'left' },
                { display: 'Trade Time', name: 'tradeTime', width: 100, sortable: false, align: 'left' },
                { display: 'Trade No.', name: 'tradeNo', width: 100, sortable: false, align: 'left' },
                { display: 'Member Id', name: 'memberId', width: 100, sortable: false, align: 'left' },
                { display: 'Trading Id', name: 'tradingId', width: 100, sortable: false, align: 'left' },
                { display: 'Role (C/H)', name: 'roleCh', width: 100, sortable: false, align: 'left' },
                { display: 'Contract Id', name: 'contractId', width: 100, sortable: false, align: 'left' },
                { display: 'Contract Type (O/F)', name: 'contracttype', width: 100, sortable: false, align: 'left' },
                { display: 'Buy / Sell', name: 'buySell', width: 100, sortable: false, align: 'left' },
                { display: 'Price', name: 'price', width: 100, sortable: false, align: 'right' },
                { display: 'Quantity', name: 'quantity', width: 100, sortable: false, align: 'right' },
                { display: 'Premium', name: 'premium', width: 100, sortable: false, align: 'left' },
                { display: 'Fee', name: 'fee', width: 100, sortable: false, align: 'right' },
                { display: 'MTM Last Price', name: 'mtmLastPrice', width: 100, sortable: false, align: 'right' },
                ],
        searchitems: [
                { display: 'ISO', name: 'iso' },
                { display: 'Name', name: 'name', isdefault: true }
                ],
        sortname: "iso",
        sortorder: "asc",
        usepager: false,
        title: 'Data Trading',
        useRp: false,
        resizeable: false,
        rp: 15,
        showTableToggleBtn: false,
        width: ($("table#mainPane").innerWidth() - 25),
        height: ($("table#mainPane").innerHeight() - 80)
    });
}

function connectJms() {
    var client, destination;

    $('#connect_form').submit(function() {
        var url = $("#connect_url").val();
        var login = $("#connect_login").val();
        var passcode = $("#connect_passcode").val();
        destination = $("#destination").val();

        client = Stomp.client(url);

        // this allows to display debug logs directly on the web page
        client.debug = function(str) {
            $("#debug").append(str + "\n");
        };
        // the client is notified when it is connected to the server.
        var onconnect = function(frame) {
            client.debug("connected to Stomp");
            $('#connect').fadeOut({ duration: 'fast' });
            $('#disconnect').fadeIn();
            $('#send_form_input').removeAttr('disabled');

            client.subscribe(destination, function(message) {
                $("#messages").append("<p>" + message.body + "</p>\n");
            });
        };
        client.connect(login, passcode, onconnect);

        return false;
    });

    $('#disconnect_form').submit(function() {
        client.disconnect(function() {
            $('#disconnect').fadeOut({ duration: 'fast' });
            $('#connect').fadeIn();
            $('#send_form_input').addAttr('disabled');
        });
        return false;
    });

    $('#send_form').submit(function() {
        var text = $('#send_form_input').val();
        if (text) {
            client.send(destination, { foo: 1 }, text);
            $('#send_form_input').val("");
        }
        return false;
    });
}

$(window).resize(function() {
    $('.flexigrid').css('width', $(window).width() - 30);
    resizeFgContentHeight($('#tableTrade'), ($(window).height() - 90));
});


$(document).ready(function() {
    createContent();
    connectJms();
});