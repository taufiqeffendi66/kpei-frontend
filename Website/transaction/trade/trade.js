﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var tradeSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var currentPage = 0;
var minId = 0;
var maxId = 0;
var maxRow = 30;
var stage = 'last';
var arrTrade;
var posArrTrade = 0;
var adaData = false;
var firstData = true;
var endProses = true;
var count = 0;

function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addArrTrade(s, f) {
    data = eval('(' + s + ')');

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) 
        recordIndex = storeCb.find('name', data.contractID);
    
    if (recordIndex===-1)
        storeCb.add({ abbr: data.contractID, name: data.contractID });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1)
        recordIndex = storeCb.find('name', data.sID);
    
    if ( (recordIndex===-1) && (data.sID !=='') )  
        storeCb.add({ abbr: data.sID, name: data.sID });

    if (minId > data.tradeNumber)
        minId = data.tradeNumber;
    if (maxId < data.tradeNumber)
        maxId = data.tradeNumber;

	if(f===0){
        	var nilaiMtmLastPrice = data.mtmLastPrice;
        	var price = data.price;
	} else {
        	nilaiMtmLastPrice = data.mtmLastPrice.replace(',','');
        	price = data.price.replace(',','');
	}
    if (data.buysellPosition==="S")
        data.mtmLastPrice = price - nilaiMtmLastPrice;
    else
        data.mtmLastPrice = nilaiMtmLastPrice - price;

	if(arrTrade.data.length===maxRow) 
	    arrTrade.removeAt(arrTrade.getCount()-1);
	

    arrTrade.insert(0, {
        tradeId: data.tradingID,
        tradeDate: data.tradeDate, 
        tradeTime: data.tradeTime,
        tradeNo: data.tradeNumber,
        sID:data.sID,
        memberId: data.memberID,
        roleCh: data.role,
        contractId: data.contractID,
        contractType: data.contractType,
        buySell: data.buysellPosition,
        price: data.price,
        quantity: data.quantity,
        premium: data.premium,
        fee: data.fee,
        lastPriceMtm: nilaiMtmLastPrice,
        mtmLastPrice: data.mtmLastPrice,
    });

    maxId = arrTrade.data.items[0].data.tradeNo;
    minId = arrTrade.data.items[arrTrade.data.length - 1].data.tradeNo;
}


function loadToStore(){
    
    if (adaData) {
        endProses = false;
        adaData = false;
        var store = Ext.getCmp('tradeGrid').getStore();
        store.data = arrTrade.data;
        Ext.getCmp('tradeGrid').getStore().loadData(store, true);
//        for (xx = 0; xx < maxRow; xx++) { store[xx] = arrTrade.getAt(xx); }
        //Ext.getCmp('tradeGrid').getView().ds.reload();
        //store.reload();
        endProses = true;
    }
}

function addTrade(s, f) {
    data = eval('(' + s + ')');

    //if(data.tradeNumber < 6000) return;
    var store = Ext.getCmp('tradeGrid').getStore();
    if (store!==null) {        
        if ((typeof data.tradeNumber === "undefined") || (data.tradeNumber===null)) {
            return;
        }

    //contractIdCombo
/*
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.contractID);
    }
    if (recordIndex===-1)
        storeCb.add({ abbr: data.contractID, name: data.contractID });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex===-1)
        storeCb.add({ abbr: data.sID, name: data.sID });

        if (minId > data.tradeNumber)
            minId = data.tradeNumber;
        if (maxId < data.tradeNumber)
            maxId = data.tradeNumber;
*/
	if(f===0){
        	var nilaiMtmLastPrice = data.mtmLastPrice;
        	var price = data.price;
	} else {
        	nilaiMtmLastPrice = data.mtmLastPrice.replace(',','');
        	price = data.price.replace(',','');
	}
        if (data.buysellPosition==="S")
            data.mtmLastPrice = price - nilaiMtmLastPrice;
        else
            data.mtmLastPrice = nilaiMtmLastPrice - price;

	if(store.data.length===maxRow) {
	    store.removeAt(store.getCount()-1);
	}

        store.insert(0, {
            tradeId: data.tradingID,
            tradeDate: data.tradeDate, 
            tradeTime: data.tradeTime,
            tradeNo: data.tradeNumber,
            sID:data.sID,
            memberId: data.memberID,
            roleCh: data.role,
            contractId: data.contractID,
            contractType: data.contractType,
            buySell: data.buysellPosition,
            price: data.price,
            quantity: data.quantity,
            premium: data.premium,
            fee: data.fee,
            lastPriceMtm: nilaiMtmLastPrice,
            mtmLastPrice: data.mtmLastPrice,
        });

        maxId = store.data.items[0].data.tradeNo;
        minId = store.data.items[store.data.length - 1].data.tradeNo;

    }
}

function subscribeTradeTopic(direction, from) {
    if ((!(typeof tradeSubscriptionId === "undefined")) && (tradeSubscriptionId!==null)) {
        client.unsubscribe(tradeSubscriptionId);
        tradeSubscriptionId = null;
    }

    stage = direction;

 
    //minId = 0;
    //maxId = 0;
    if (currentPage===0) {
        var selector = "";
        if (Ext.getCmp('memberCombo').getValue()!==null) 
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        
        if (Ext.getCmp('contractIdCombo').getValue()!==null) {
            if (selector==="")
                selector = selector + "contractID='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
            else
                selector = selector + " and contractID='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        }
        if (Ext.getCmp('sidCombo').getValue()!==null) {
            if (selector==="")
                selector = selector + "tradingID='" + Ext.getCmp('sidCombo').getValue() + "' ";
            else
                selector = selector + " and tradingID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        }
        if (Ext.getCmp('contractTypeCombo').getValue()!==null) {
            if (selector==="")
                selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
            else
                selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        }

        if (selector!=="") {
            var tradeSubscritptionHeader = { 'selector': selector };
            tradeSubscriptionId = client.subscribe(tt, function(message) {
                addArrTrade(message.body, 0);
                adaData = true;
            }, tradeSubscritptionHeader);
        } else {
            tradeSubscriptionId = client.subscribe(tt, function(message) {
                addArrTrade(message.body, 0);
                adaData = true;
            });
        }
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=trade&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction + '&minId=' + minId + '&maxId=' + maxId,
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')'); //Ext.decode(response.responseText);

            switch( from )
            {
                case 0 :
                    if ( obj.length > 0)
                        Ext.getCmp('tradeGrid').getStore().loadData([], false);
                    break;

                case 1 :
                    Ext.getCmp('tradeGrid').getStore().loadData([], false);
                    break;
            }

            //Ext.getCmp('mId').setText(count+=obj.length);

            for (ii = 0; ii < obj.length; ii++) {
                data = eval('(' + obj[ii].tradeData + ')');
                addArrTrade(obj[ii].tradeData, 1);
            }
            adaData = true;

        },
        failure: function(response, opts) {

        }
    });
}

function reportTopic(direction) {
    if ((!(typeof tradeSubscriptionId === "undefined")) && (tradeSubscriptionId != null)) {
        client.unsubscribe(tradeSubscriptionId);
        tradeSubscriptionId = null;
    }

    if (currentPage===0) {
        var selector = "";
        if (Ext.getCmp('memberCombo').getValue()!==null) {
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        }
        if (Ext.getCmp('contractIdCombo').getValue()!==null) {
            if (selector==="")
                selector = selector + "contractID='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
            else
                selector = selector + " and contractID='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        }
        if (Ext.getCmp('sidCombo').getValue()!==null) {
            if (selector==="")
                selector = selector + "tradingID='" + Ext.getCmp('sidCombo').getValue() + "' ";
            else
                selector = selector + " and tradingID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        }
        if (Ext.getCmp('contractTypeCombo').getValue()!==null) {
            if (selector==="")
                selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
            else
                selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        }

        if (selector!=="") {
            var tradeSubscritptionHeader = { 'selector': selector };
            tradeSubscriptionId = client.subscribe(tt, function(message) {
                addTrade(message.body, 0);
            }, tradeSubscritptionHeader);
        } else {
            tradeSubscriptionId = client.subscribe(tt, function(message) {
                addTrade(message.body, 0);
            });
        }
    }
    window.open('./tradeReportView.aspx?type=trade&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction + '&minId=' + minId + '&maxId=' + maxId);
}

//Ext.TaskMgr.start ( {
//    run: function() { loadToStore() },
//    interval: 1000
//});

function checkData() {
    if(endProses) loadToStore()        
}

Ext.onReady(function () {

    setInterval( function() {checkData()}, 500);

    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var tradeInitialData = [];

    var tradeStore = Ext.create('Ext.data.ArrayStore', {
        fields: [
           { name: 'tradeId' },
           { name: 'tradeDate' },
           { name: 'tradeTime' },
           { name: 'tradeNo' },
           { name: 'memberId' },
           { name: 'sID' },
           { name: 'roleCh' },
           { name: 'contractId' },
           { name: 'contractType' },
           { name: 'buySell' },
           { name: 'price' },
           { name: 'quantity' },
           { name: 'premium' },
           { name: 'fee' },
           { name: 'mtmLastPrice' },
           { name: 'lastPriceMtm' }
        ],
        autoLoad : false,
        data: tradeInitialData
    });

    var tmpTradeInitialData = [];
    arrTrade = Ext.create('Ext.data.ArrayStore', {
        fields: [
           { name: 'tradeId' },
           { name: 'tradeDate' },
           { name: 'tradeTime' },
           { name: 'tradeNo' },
           { name: 'memberId' },
           { name: 'sID' },
           { name: 'roleCh' },
           { name: 'contractId' },
           { name: 'contractType' },
           { name: 'buySell' },
           { name: 'price' },
           { name: 'quantity' },
           { name: 'premium' },
           { name: 'fee' },
           { name: 'mtmLastPrice' },
           { name: 'lastPriceMtm' }
        ],
        autoLoad : false,
        data: tmpTradeInitialData
    });

   // create the Grid
    var tradeGrid = Ext.create('Ext.grid.Panel', {
        store: tradeStore,
        id: 'tradeGrid',
        stateful: false,
        stateId: 'tradeGrid',
        columns: [
                { text: 'Trade', columns: [
                    { text: 'No.', dataIndex: 'tradeNo', width: 50, sortable: false, align: 'right', draggable: false },
                    { text: 'Date', dataIndex: 'tradeDate', width: 70, sortable: true, align: 'center', draggable: false },
                    { text: 'Time', dataIndex: 'tradeTime', width: 60, sortable: true, align: 'center', draggable: false }
                ]
                },
                { text: 'Member<br>ID', dataIndex: 'memberId', width: 50, sortable: false, align: 'center', draggable: false },
                { text: 'SID', dataIndex: 'sID', width: 110, sortable: false, align: 'left', draggable: false },
                { text: 'Trading ID', dataIndex: 'tradeId', width: 75, sortable: false, align: 'left', draggable: false },
                { text: 'Role<br/>(C/H)', dataIndex: 'roleCh', width: 40, sortable: false, align: 'center', draggable: false },
                { text: 'Contract ID', dataIndex: 'contractId', width: 100, sortable: false, align: 'left', draggable: false },
                { text: 'Contract<br/>Type (O/F)', dataIndex: 'contractType', width: 65, sortable: false, align: 'center', draggable: false },
                { text: 'Buy<br/>Sell', dataIndex: 'buySell', width: 40, sortable: false, align: 'center', draggable: false },
                { text: 'Price', dataIndex: 'price', width: 75, sortable: false, align: 'right', draggable: false, tdCls: 'x-change-cell' },
                { text: 'Quantity', dataIndex: 'quantity', width: 50, sortable: false, align: 'right', draggable: false },
                { text: 'Premium', dataIndex: 'premium', width: 100, sortable: false, align: 'right', draggable: false },
                { text: 'Fee', dataIndex: 'fee', width: 100, sortable: false, align: 'right', draggable: false },
                { text: 'Delta<br/>Price', dataIndex: 'mtmLastPrice', width: 100, sortable: false, align: 'right', draggable: false, xtype : 'numbercolumn', format : '0,000.00' }
        ],
        //renderTo: 'contentPanel',
        viewConfig: {
            stripeRows: true,
            forceFit: true,
            loadMask: false,
            getRowClass: function (record, index) {
                var c = record.get('Price');
                if (c < 0) {
                    return 'price-fall';
                } else if (c > 0) {
                    return 'price-rise';
                }
            }
        },
        autoWidth: true,
        autoHeight: true
    });
    tradeGrid.setLoading(false, false);
    //tradeGrid.reload(tradeGrid.lastOption);


    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var memberStore = Ext.create('Ext.data.Store', {
        model: 'Member',
        data: m
    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        store: memberStore,
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function (combo, value) {
                combo = Ext.getCmp('sidCombo');
                combo.clearValue();
                combo.getStore().removeAll();                
                Ext.Ajax.request({
                    url: '../request.aspx?type=sid&memberId=' + Ext.getCmp('memberCombo').getValue(),
                    success: function (response, opts) {                        
                        var obj = eval('(' + response.responseText + ')');
                        for (i = 0; i < obj.length; i++) {
                            newData = eval('(' + obj[i].sidData + ')');
                            combo.getStore().add({ abbr: newData.abbr, name: newData.name });
                        }
                    },
                    failure: function (response, opts) {                        
                    }
                });
            }
            }
        }
    });

    Ext.define('ContractId', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractIdStore = Ext.create('Ext.data.Store', {
        model: 'ContractId',
        data: ci
    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        store: contractIdStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('Sid', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var sidStore = Ext.create('Ext.data.Store', {
        model: 'Sid',
        data: sids
    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        store: sidStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: ct
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ', id:'mId' },
                    memberCombo,
                    { xtype: 'label', text: 'Contract ID: ' },
                    contractIdCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
                    { xtype: 'label', text: 'Contract Type: ' },
                    contractTypeCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function () {
                            currentPage = 0;
                            subscribeTradeTopic("last", 1);
                            //Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var pagingPanel = Ext.create('Ext.panel.Panel', {
        id: 'pagingPanel',
        frame: false,
        border: false,
        height: 30,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
            {
                xtype: 'button',
                disabled: true,
                id: 'btnFirst',
                text: ' First ',
                handler: function () {
                    stage = 'first';
                    Ext.getCmp('tradeGrid').getStore().loadData([], false); ;
                    currentPage--;
                    Ext.getCmp('labelPage').update(currentPage);
                    Ext.getCmp('btnPrev').enable();
                    Ext.getCmp('btnNext').enable();
                    Ext.getCmp('btnLast').enable();
                    subscribeTradeTopic("first", 0);
                }
            },{
                xtype: 'button',
                disabled: true,
                id: 'btnPrev',
                text: ' Prev ',
                handler: function () {
                    //Ext.getCmp('tradeGrid').getStore().loadData([], false); ;
                    currentPage--;
                    Ext.getCmp('labelPage').update(currentPage);
                    Ext.getCmp('btnLast').enable();
                    Ext.getCmp('btnNext').enable();
                    subscribeTradeTopic("prev", 0);
                }
            }, {
                xtype: 'label',
                id: 'labelPage',
                text: ' ' + currentPage + ' ',
                hidden: true
            }, {
                xtype: 'button',
                disabled: true,
                id: 'btnNext',
                text: ' Next ',
                handler: function () {
                    //Ext.getCmp('tradeGrid').getStore().loadData([], false); ;
                    currentPage++;
                    Ext.getCmp('labelPage').update(currentPage);
                    if (currentPage == 0) {
                        Ext.getCmp('labelPage').update('0');
                        Ext.getCmp('btnLast').disable();
                        Ext.getCmp('btnNext').disable();
                    }
                    subscribeTradeTopic("next", 0);
                }
            }, {
                xtype: 'button',
                disabled: true,
                id: 'btnLast',
                text: ' Last ',
                handler: function () {
                    stage = 'last';
                    Ext.getCmp('tradeGrid').getStore().loadData([], false); ;
                    currentPage = 0;
                    Ext.getCmp('labelPage').update('0');
                    Ext.getCmp('btnLast').disable();
                    Ext.getCmp('btnNext').disable();
                    subscribeTradeTopic("last", 0);
                }
            }
        ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Transaction List',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: tradeGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel

            }

        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    //Ext.getCmp('pagingPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(u);
    client.debug = function (str) {
    };
    var onconnect = function (frame) {
        Ext.getCmp('tradeGrid').getStore().loadData([], false); ;
        currentPage = 0;
        pingSubscriptionId = client.subscribe(pt, function (message) {
        });

        subscribeTradeTopic("last", 1);
    };
    client.connect(l, p, onconnect);


    window.onbeforeunload = function () {
        if ((!(typeof tradeSubscriptionId === "undefined")) && (tradeSubscriptionId !== null)) {
            client.unsubscribe(tradeSubscriptionId);
            tradeSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId !== null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function () {
        });
    }
});
