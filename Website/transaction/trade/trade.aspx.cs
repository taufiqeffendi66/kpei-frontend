﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Common;
using Common.wsCommonList;
using System.Text;
using System.Web;

namespace imq.kpei.transaction.trade
{
    public partial class trade : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                ////redirect to login page if not logged in
                //ImqSession.Validate(this);
                ////redirect to blank page if accessed directly
                //ImqSession.ValidatedAction(this);           

                string jmsServer = HttpContext.Current.Request.Url.Host; //ImqSession.GetConfig("JMS_SERVER_WS");
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string tradeTopic = ImqSession.GetConfig("JMS_TOPIC_TRADE");
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string user = Session["SESSION_USER"].ToString();
                string password = Session.SessionID;
                string sessionId = Session["SESSION_USERMEMBER"].ToString();
                string userJMS = String.Format("{0}-{1}", user, sessionId);

                string memberId;
                UiCommonListService ws = ImqWS.GetCommonListService();
                StringBuilder membersSb = new StringBuilder();
                if (sessionId.Equals("kpei"))
                {
                    memberId = "";
                    //UiDataService ws = ImqWS.GetTransactionService();
                    commonListDto[] membersDto;
                    try
                    {
                        membersDto = ws.listMemberFromTrade(sessionId);
                    }
                    catch
                    {
                        membersDto = new commonListDto[0];
                    }
                    if (membersDto != null)
                        for (int i = 0; i < membersDto.Length; i++)
                        {
                            if (i > 0)
                                membersSb.Append(",");
                            membersSb.Append("{");
                            membersSb.Append("\"abbr\":");
                            membersSb.Append(String.Format("\"{0}\"", membersDto[i].id));
                            membersSb.Append(",\"name\":");
                            membersSb.Append(String.Format("\"{0}\"", membersDto[i].name));
                            membersSb.Append("}");
                        }
                }
                else
                {
                    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];
                    membersSb.Append("{");
                    membersSb.Append("\"abbr\":");
                    membersSb.Append(String.Format("\"{0}\"", memberId));
                    membersSb.Append(",\"name\":");
                    membersSb.Append(String.Format("\"{0}\"", memberId));
                    membersSb.Append("}");
                }



                commonListDto[] contractsDto;
                try
                {
                    contractsDto = ws.listSeriesFromTrade(sessionId);
                }
                catch
                {
                    contractsDto = new commonListDto[0];
                }
                StringBuilder contractsSb = new StringBuilder();

                if (contractsDto != null )
                    for (int i = 0; i < contractsDto.Length; i++)
                    {
                        if (i > 0)
                            contractsSb.Append(",");
                        contractsSb.Append("{");
                        contractsSb.Append("\"abbr\":");
                        contractsSb.Append(String.Format("\"{0}\"", contractsDto[i].id));
                        contractsSb.Append(",\"name\":");
                        contractsSb.Append(String.Format("\"{0}\"", contractsDto[i].name));
                        contractsSb.Append("}");
                    }

                commonListDto[] sidDto;
                try
                {
                    sidDto = ws.listSidFromTrade(sessionId);
                }
                catch
                {
                    sidDto = new commonListDto[0];
                }
                StringBuilder sidSb = new StringBuilder();

                if (sidDto != null)
                    for (int i = 0; i < sidDto.Length; i++)
                    {
                        if (i > 0)
                            sidSb.Append(",");
                        sidSb.Append("{");
                        sidSb.Append("\"abbr\":");
                        sidSb.Append(String.Format("\"{0}\"", sidDto[i].id));
                        sidSb.Append(",\"name\":");
                        sidSb.Append(String.Format("\"{0}\"", sidDto[i].name));
                        sidSb.Append("}");
                    }


                ScriptManager.RegisterStartupScript(this, GetType(), "initPage", String.Format(@"                    
                                                                                      <link rel=""stylesheet"" type=""text/css"" href=""../../js/extjs/resources/css/ext-all-gray.css"" />
                                                                                      <script type=""text/javascript"" src=""../../js/util/page.js""></script>
                                                                                      <script type=""text/javascript"" src=""../../js/extjs/ext-all.js""></script>
                                                                                      <script type=""text/javascript"" src=""../../js/amq/stomp.js""></script>
                                                                                      
                                                                                      <script type=""text/javascript"">
                                                                                          var u = ""{0}"";
                                                                                          var l = ""{1}"";
                                                                                          var p = ""{2}"";
                                                                                          var tt = ""{3}"";    
                                                                                          var pt = ""{4}"";    
                                                                                          var m = [
                                                                                                  {5}
                                                                                              ];
                                                                                              var ci = [
                                                                                                  {6}
                                                                                              ];
                                                                                              var ct = [
                                                                                                  {{""abbr"":""O"",""name"":""Option""}},
                                                                                                  {{""abbr"":""F"",""name"":""Future""}}
                                                                                              ];
                                                                                      
                                                                                              var sids = [            
                                                                                                  {7}
                                                                                              ];
                                                                                      
                                                                                      
                                                                                      </script>
                                                                                      
                                                                                      <script type=""text/javascript"" src=""trade.js""></script>
                                                                                      
                                                                                      ", jmsServer, userJMS, password, tradeTopic, pingTopic, membersSb, contractsSb, sidSb), false);
            }
        }
    }
}
