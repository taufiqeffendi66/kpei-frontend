﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Common.wsCommonList;
using System.Text;

namespace imq.kpei.transaction.offline_collateral
{
    public partial class offline_collateral : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                ////redirect to login page if not logged in
                //ImqSession.Validate(this);
                ////redirect to blank page if accessed directly
                //ImqSession.ValidatedAction(this);           

                string jmsServer = HttpContext.Current.Request.Url.Host; //ImqSession.GetConfig("JMS_SERVER_WS");
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string offlineCollateralTopic = ImqSession.GetConfig("JMS_TOPIC_OFFLINE_COLLATERAL");
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string user = Session["SESSION_USER"].ToString();
                string password = Session.SessionID;
                string member = Session["SESSION_USERMEMBER"].ToString();
                string userJMS = user + "-" + member;

                //UiCommonListService ws = ImqWS.GetCommonListService();
                //commonListDto[] membersDto;
                //try
                //{
                //    membersDto = ws.listMember(Session["SESSION_USERMEMBER"].ToString());
                //}
                //catch
                //{
                //    membersDto = new commonListDto[0];
                //}
                //StringBuilder membersSb = new StringBuilder();
                //for (int i = 0; i < membersDto.Length; i++)
                //{
                //    if (i > 0)
                //        membersSb.Append(",");
                //    membersSb.Append("{");
                //    membersSb.Append("\"abbr\":");
                //    membersSb.Append("\"" + membersDto[i].id + "\"");
                //    membersSb.Append(",\"name\":");
                //    membersSb.Append("\"" + membersDto[i].name + "\"");
                //    membersSb.Append("}");
                //}

                //commonListDto[] contractsDto;
                //try
                //{
                //    contractsDto = ws.listContract();
                //}
                //catch
                //{
                //    contractsDto = new commonListDto[0];
                //}
                //StringBuilder contractsSb = new StringBuilder();
                //for (int i = 0; i < contractsDto.Length; i++)
                //{
                //    if (i > 0)
                //        contractsSb.Append(",");
                //    contractsSb.Append("{");
                //    contractsSb.Append("\"abbr\":");
                //    contractsSb.Append("\"" + contractsDto[i].id + "\"");
                //    contractsSb.Append(",\"name\":");
                //    contractsSb.Append("\"" + contractsDto[i].name + "\"");
                //    contractsSb.Append("}");
                //}



                ScriptManager.RegisterStartupScript(this, this.GetType(), "initPage", @"                    
<link rel=""stylesheet"" type=""text/css"" href=""../../js/extjs/resources/css/ext-all-gray.css"" />
<script type=""text/javascript"" src=""../../js/util/page.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ext-all.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ux/data/PagingMemoryProxy.js""></script>
<script type=""text/javascript"" src=""../../js/amq/stomp.js""></script>

<script type=""" + "text/javascript" + @""">
    var url = """ + jmsServer + @""";
    var login = """ + userJMS + @""";
    var passcode = """ + password + @""";
    var offlineCollateralTopic = """ + offlineCollateralTopic + @""";    
    var pingTopic = """ + pingTopic + @""";    
        var contractTypes = [
            {""abbr"":""O"",""name"":""Option""},
            {""abbr"":""F"",""name"":""Future""}
        ];

    var sids = [];
    var members = [];
    var contractIds = [];


</script>

<script type=""text/javascript"" src=""offline_collateral.js""></script>

", false);
            }
        }
    }
}
