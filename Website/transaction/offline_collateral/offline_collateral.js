﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var offlineCollateralSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var offlineCollateralInitialData = [];
var cb;
var storeCb;
var recordIndex = -1;

var arrOfflineCollateral = [];
var endProses = true;
var pageSize = 20;
var tick = 200;

function addArrOfflineCollateral(s, force) {
    data = eval('(' + s + ')');
    arrOfflineCollateral.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrOfflineCollateral.length > 0) {
        endProses = false;
        if (arrOfflineCollateral.length > pageSize)
            max = pageSize;
        else
            max = arrOfflineCollateral.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrOfflineCollateral.shift()
            addToOfflineCollateral(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToOfflineCollateral(data) {
    if (offlineCollateralInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.instrumentNo === "undefined") || (data.instrumentNo == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < offlineCollateralInitialData.length; i++) {
        if (data.memberID == offlineCollateralInitialData[i].memberID && data.instrumentNo == offlineCollateralInitialData[i].instrumentNo) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        offlineCollateralInitialData.push(data);
    } else {
        offlineCollateralInitialData[recordIndex].memberID = data.memberID;
        offlineCollateralInitialData[recordIndex].instrumentType = data.instrumentType;
        offlineCollateralInitialData[recordIndex].accountNo = data.accountNo;
        offlineCollateralInitialData[recordIndex].bank = data.bank;
        offlineCollateralInitialData[recordIndex].instrumentNo = data.instrumentNo;
        offlineCollateralInitialData[recordIndex].maturityDate = data.maturityDate;
        offlineCollateralInitialData[recordIndex].currency = data.currency;
        offlineCollateralInitialData[recordIndex].nominal = data.nominal;
        offlineCollateralInitialData[recordIndex].hairCut = data.hairCut;
        offlineCollateralInitialData[recordIndex].value = data.value;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}

function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addofflineCollateral(s, force) {
    if (offlineCollateralInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.instrumentNo === "undefined") || (data.instrumentNo == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < offlineCollateralInitialData.length; i++) {
        if (data.memberID == offlineCollateralInitialData[i].memberID && data.instrumentNo == offlineCollateralInitialData[i].instrumentNo) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        offlineCollateralInitialData.push(data);
    } else {
        offlineCollateralInitialData[recordIndex].memberID = data.memberID;
        offlineCollateralInitialData[recordIndex].instrumentType = data.instrumentType;
        offlineCollateralInitialData[recordIndex].accountNo = data.accountNo;
        offlineCollateralInitialData[recordIndex].bank = data.bank;
        offlineCollateralInitialData[recordIndex].instrumentNo = data.instrumentNo;
        offlineCollateralInitialData[recordIndex].maturityDate = data.maturityDate;
        offlineCollateralInitialData[recordIndex].currency = data.currency;
        offlineCollateralInitialData[recordIndex].nominal = data.nominal;
        offlineCollateralInitialData[recordIndex].hairCut = data.hairCut;
        offlineCollateralInitialData[recordIndex].value = data.value;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeofflineCollateralTopic(direction) {
    if ((!(typeof offlineCollateralSubscriptionId === "undefined")) && (offlineCollateralSubscriptionId != null)) {
        client.unsubscribe(offlineCollateralSubscriptionId);
        offlineCollateralSubscriptionId = null;
    }

    for (; offlineCollateralInitialData.length > 0; )
        offlineCollateralInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var offlineCollateralSubscritptionHeader = { 'selector': selector };
        offlineCollateralSubscriptionId = client.subscribe(offlineCollateralTopic, function(message) {
            addArrOfflineCollateral(message.body, true);
        }, offlineCollateralSubscritptionHeader);
    } else {
        offlineCollateralSubscriptionId = client.subscribe(offlineCollateralTopic, function(message) {
            addArrOfflineCollateral(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=offlineCollateral_client&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue(),
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrOfflineCollateral(obj[ia].offlineCollateralData, false);
            }
        },
        failure: function(response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof offlineCollateralSubscriptionId === "undefined")) && (offlineCollateralSubscriptionId != null)) {
        client.unsubscribe(offlineCollateralSubscriptionId);
        offlineCollateralSubscriptionId = null;
    }

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var offlineCollateralSubscritptionHeader = { 'selector': selector };
        offlineCollateralSubscriptionId = client.subscribe(offlineCollateralTopic, function (message) {
            addArrOfflineCollateral(message.body, true);
        }, offlineCollateralSubscritptionHeader);
    } else {
        offlineCollateralSubscriptionId = client.subscribe(offlineCollateralTopic, function (message) {
            addArrOfflineCollateral(message.body, true);
        });
    }
    window.open('./offlineCollateralReportView.aspx?type=offlineCollateral&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}


Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var offlineCollateralModel = Ext.define('offlineCollateralModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'memberID' },
               { name: 'instrumentType' },
               { name: 'accountNo' },
               { name: 'bank' },
               { name: 'instrumentNo' },
               { name: 'maturityDate' },
               { name: 'currency' },
               { name: 'nominal' },
               { name: 'hairCut' },
               { name: 'currencyRate' },
               { name: 'value' }
            ]
    });

    var offlineCollateralReader = new Ext.data.reader.Json({
        model: 'offlineCollateralModel'
    }, offlineCollateralModel);

    var offlineCollateralStore = Ext.create('Ext.data.ArrayStore', {
        id: 'offlineCollateralStore',
        model: 'offlineCollateralModel',
        data: offlineCollateralInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(offlineCollateralInitialData),
        autoLoad: true,
        autoSync: true,
        reader: offlineCollateralReader
    });

    // create the Grid
    var offlineCollateralGrid = Ext.create('Ext.grid.Panel', {
        store: offlineCollateralStore,
        id: 'offlineCollateralGrid',
        stateful: false,
        stateId: 'offlineCollateralGrid',
        columns: [
                { text: 'Member<br>ID', dataIndex: 'memberID', width: 50, sortable: true, align: 'center', draggable: false },
                { text: 'Instrument Type', dataIndex: 'instrumentType', width: 150, sortable: true, align: 'left', draggable: false },
                { text: 'Account No', dataIndex: 'accountNo', width: 120, sortable: true, align: 'left', draggable: false },
                { text: 'Bank', dataIndex: 'bank', width: 170, sortable: true, align: 'left', draggable: false },
                { text: 'Instrument No', dataIndex: 'instrumentNo', width: 100, sortable: true, align: 'left', draggable: false },
                { text: 'Maturity<br>Date', dataIndex: 'maturityDate', width: 75, sortable: true, align: 'center', draggable: false },
                { text: 'Currency', dataIndex: 'currency', width: 70, sortable: true, align: 'center', draggable: false },
                { text: 'Nominal', dataIndex: 'nominal', width: 110, sortable: true, align: 'right', draggable: false },
                { text: 'Hair<br>Cut', dataIndex: 'hairCut', width: 70, sortable: true, align: 'right', draggable: false },
                { text: 'Currency<br>Rate', dataIndex: 'currencyRate', width: 70, sortable: true, align: 'center', draggable: false },
                { text: 'Value', dataIndex: 'value', flex: 1, sortable: true, align: 'right', draggable: false }
        ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: offlineCollateralStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    offlineCollateralGrid.setLoading(false, false);
    offlineCollateralStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function(combo, value) {
                var sidCombo = Ext.getCmp('sidCombo');
                sidCombo.clearValue();
                //TODO reload/filter sid combobox data
                //sidCombo.store.filter('cid', combo.getValue());
            }
            }
        }
    });

    memberCombo.on('select', function(box, record, index) {

    });

    Ext.define('ContractId', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractIdStore = Ext.create('Ext.data.Store', {
        model: 'ContractId',
        data: contractIds
    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        store: contractIdStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('Sid', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var sidStore = Ext.create('Ext.data.Store', {
        model: 'Sid',
        data: sids
    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        store: sidStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
        //                    { xtype: 'label', text: 'Contract ID: ' },
        //                    contractIdCombo,
        //                    { xtype: 'label', text: 'SID: ' },
        //                    sidCombo,
        //                    { xtype: 'label', text: 'Contract Type: ' },
        //                    contractTypeCombo,
                    {xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function() {
                            subscribeofflineCollateralTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Detail Offline Collateral',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: offlineCollateralGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function(str) {
    };
    var onconnect = function(frame) {
        Ext.getCmp('offlineCollateralGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function(message) {
        });

        subscribeofflineCollateralTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function() {
        if ((!(typeof offlineCollateralSubscriptionId === "undefined")) && (offlineCollateralSubscriptionId != null)) {
            client.unsubscribe(offlineCollateralSubscriptionId);
            offlineCollateralSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function() {
        });
    }
});
