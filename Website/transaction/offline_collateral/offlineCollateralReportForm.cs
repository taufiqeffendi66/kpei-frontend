﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.offline_collateral
{
    public partial class offlineCollateralReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public offlineCollateralReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "memberId");
            cl02.DataBindings.Add("Text", DataSource, "instrumentType");
            cl03.DataBindings.Add("Text", DataSource, "accountNo");
            cl04.DataBindings.Add("Text", DataSource, "bankName");
            cl05.DataBindings.Add("Text", DataSource, "instrumentNo");
            cl06.DataBindings.Add("Text", DataSource, "maturityDate");
            cl07.DataBindings.Add("Text", DataSource, "currency");
            cl08.DataBindings.Add("Text", DataSource, "nominalValue");
            cl09.DataBindings.Add("Text", DataSource, "hairCut");
            cl10.DataBindings.Add("Text", DataSource, "exchangeRate");
            cl11.DataBindings.Add("Text", DataSource, "collateralValue");
        }
    }
}
