﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="account_kpei.aspx.cs" Inherits="imq.kpei.transaction.account_kpei" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
            <div class="title">ACCOUNT - KPEI</div>
            <div>KPEI Accounts</div>
            <div>
                <dx:ASPxGridView ID="gvApproval" runat="server" AutoGenerateColumns="False" Width="500px" ClientInstanceName="grid">
                    <Columns>
                        <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Bank"></dx:GridViewDataTextColumn>
                        <dx:GridViewBandColumn Caption="Settlement" VisibleIndex="4">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Acc. No" VisibleIndex="0"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="1"></dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Penalty" VisibleIndex="5">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Acc. No" VisibleIndex="0"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="1"></dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Guarantee Fund" VisibleIndex="6">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Acc. No" VisibleIndex="0"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="1"></dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Fee" VisibleIndex="6">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Acc. No" VisibleIndex="0"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="1"></dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                    </Columns>
                    <Settings ShowFooter="True" />
                    <TotalSummary>
                        <dx:ASPxSummaryItem FieldName="SettlementValue" SummaryType="Sum" />
                        <dx:ASPxSummaryItem FieldName="PenaltyValue" SummaryType="Sum" />
                        <dx:ASPxSummaryItem FieldName="GuaranteeFundValue" SummaryType="Sum" />
                        <dx:ASPxSummaryItem FieldName="FeeValue" SummaryType="Sum" />
                    </TotalSummary>                
                </dx:ASPxGridView>
            </div>
        </div>

</asp:Content>
