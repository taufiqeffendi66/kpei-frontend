﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using System.Text;
using Common.wsTransaction;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.assigment
{
    public partial class assigmentReportView : System.Web.UI.Page
    {
        assignmentDto[] assignments;
        string memberId;
        string sessionId;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            //if (!ImqSession.ValidateAction(this))
            //{
            //    responseEmpty();
            //    return;
            //}
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            sessionId = Session["SESSION_USERMEMBER"].ToString();
            if (sessionId.Equals("kpei"))
                ReportViewer1.Report = CreateReport();
            else
                ReportViewer1.Report = CreateReportAk();
        }

        private void getData()
        {
            try
            {
                string dataType = Request.QueryString["type"];
                string memReq = Request.QueryString["memberId"];
                if (sessionId.Equals("kpei"))
                {
                    if (memReq.Equals("null"))
                        memberId = "";
                    else
                        memberId = memReq;
                }
                else
                    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];

                string contractId = Request.QueryString["contractId"];
                if (contractId != null && contractId.Equals("null"))
                    contractId = "";

                string sid = Request.QueryString["sid"];
                if (sid != null && sid.Equals("null"))
                    sid = "";

                string contractType = Request.QueryString["contractType"];
                if (contractType != null && contractType.Equals("null"))
                    contractType = "";

                string minId = Request.QueryString["minId"];
                string maxId = Request.QueryString["maxId"];
                string direction = Request.QueryString["direction"];
                if (string.IsNullOrEmpty(dataType))
                {
                    responseEmpty();
                    return;
                }
                UiDataService ws = new UiDataService();
                ws.Url = ImqSession.GetConfig("WS_TRANSACTION") + "/UiData";
                switch (dataType)
                {
                    case "assigment":
                        assignments = ws.getAssignment(sessionId, memberId, contractId, sid, contractType, minId, maxId, "all");
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }

        XtraReport CreateReport()
        {
            if (IsPostBack)
            {
                getData();
            }
            // Create a report.
            assigmentReportForm report = new assigmentReportForm();
            // Bind the report to the list. 
            report.DataSource = assignments;
            report.Name = ImqSession.getFormatFileSave("AssigmentReport");
            report.SetBoundLabel();
            return report;
        }

        XtraReport CreateReportAk()
        {
            if (IsPostBack)
            {
                getData();
            }
            // Create a report.
            assigmentAkReportForm report = new assigmentAkReportForm();
            // Bind the report to the list. 
            report.DataSource = assignments;
            report.Name = ImqSession.getFormatFileSave("AssigmentReport");
            report.SetBoundLabel();
            return report;
        }

        private void responseEmpty()
        {
            Response.Clear();
            Response.ContentType = "text/plain";
            Response.Write("[]");
            Response.Flush();
            Response.End();
        }

        private string IntDateToStr(int iDate)
        {
            int tahun, bulan, tgl;
            int tmp;
            tahun = iDate / 10000;
            tmp = iDate % 10000;
            bulan = tmp / 100;
            tgl = tmp % 100;
            return string.Format("{0}-", tahun) + string.Format("{0}-", bulan) + string.Format("{0}", tgl);
        }
    }
}
