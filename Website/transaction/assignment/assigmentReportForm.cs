﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.assigment
{
    public partial class assigmentReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public assigmentReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "exerciseNo");
            cl02.DataBindings.Add("Text", DataSource, "buyMemberId");
            cl03.DataBindings.Add("Text", DataSource, "exeRole");
            cl04.DataBindings.Add("Text", DataSource, "exeSid");
            cl05.DataBindings.Add("Text", DataSource, "exeVolume");
            cl06.DataBindings.Add("Text", DataSource, "dataTime");
            cl07.DataBindings.Add("Text", DataSource, "sellMemberId");
            cl08.DataBindings.Add("Text", DataSource, "assignmentRole");
            cl09.DataBindings.Add("Text", DataSource, "assignmentSid");
            cl10.DataBindings.Add("Text", DataSource, "series");
            cl11.DataBindings.Add("Text", DataSource, "assignmentVolume");
        }
    }
}
