﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.assigment
{
    public partial class assigmentAkReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public assigmentAkReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl06.DataBindings.Add("Text", DataSource, "dataTime");
            cl07.DataBindings.Add("Text", DataSource, "sellMemberId");
            cl08.DataBindings.Add("Text", DataSource, "assignmentRole");
            cl09.DataBindings.Add("Text", DataSource, "assignmentSid");
            cl10.DataBindings.Add("Text", DataSource, "series");
            cl11.DataBindings.Add("Text", DataSource, "assignmentVolume");
        }
    }
}
