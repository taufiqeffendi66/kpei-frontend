﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using Common;
using Common.wsTransaction;
using System.Globalization;

namespace imq.kpei.transaction.settlementPrice
{
    public partial class settlementPriceEdit : Page
    {
        private DataTable tbl;
        private int addedit;
        private int type;

        private String aUserLogin;
        private String aUserMember;
        private const String target = @"../settlementPrice/settlementPrice.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            addedit = int.Parse(Session["stat"].ToString());
            type = int.Parse(Request.QueryString["type"]);
            switch (addedit)
            {
                case 0:
                    break;
                case 1:
                    //if (!IsPostBack && !IsCallback)
                    //{
                    if (tbl == null)
                        tbl = CreateTableHP();
                    tbl = (DataTable)Session["tbl"];
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["id"] };
                    Session["tbl"] = tbl;
                    //}
                    //else
                    //    Session["tbl"] = tbl;

                    dtContractId.Enabled = false;
                    dtPointNum.Enabled = false;
                    dtContractId.Text = tbl.Rows[0]["series"].ToString();
                    dtPointNum.Text = tbl.Rows.Count.ToString();
                    gvEdit.KeyFieldName = "id";
                    gvEdit.DataSource = tbl;
                    gvEdit.DataBind();
                    break;

                case 2 :
                    if (tbl == null)
                        tbl = CreateTableHP();
                    tbl = (DataTable)Session["tbl"];
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["id"] };
                    Session["tbl"] = tbl;
                    dtContractId.Enabled = false;
                    dtPointNum.Enabled = false;
                    dtContractId.Text = tbl.Rows[0]["series"].ToString();
                    dtPointNum.Text = tbl.Rows.Count.ToString();
                    gvEdit.KeyFieldName = "id";
                    gvEdit.DataSource = tbl;
                    gvEdit.DataBind();
                    break;
            }
        }

        static DataTable CreateTableHP()
        {
            DataTable table = new DataTable();
            table.Columns.Add("sequence", typeof(String));
            table.Columns.Add("series", typeof(String));
            table.Columns.Add("id", typeof(String));
            table.Columns.Add("price", typeof(String));
            table.Columns.Add("datetime", typeof(String));
            table.Columns.Add("method", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["id"] };
            return table;
        }

        private hphfDto[] getData()
        {
            hphfDto[] hphs = new hphfDto[gvEdit.VisibleRowCount];
            //UiDataService ws = ImqWS.GetTransactionService();
            for (int i = 0; i < gvEdit.VisibleRowCount; i++)
            {
                ASPxTextBox cb01 = (ASPxTextBox)gvEdit.FindRowCellTemplateControl(i, gvEdit.Columns["value"] as GridViewDataColumn, "textBox");
                int id = Convert.ToInt32(gvEdit.GetRowValues(i, gvEdit.KeyFieldName));

                if (cb01 != null)
                {
                    DataRow aRow = tbl.Rows.Find(id);
                    aRow["price"] = cb01.Text;
                    hphfDto hph = new hphfDto() { dataTime = aRow["datetime"].ToString(), sequence = aRow["sequence"].ToString(), series = aRow["series"].ToString(), underlyingCode = aRow["series"].ToString(), price = aRow["price"].ToString() };
                    if ((aRow["price"].ToString() != String.Empty) && (TryParseToInt(aRow["price"].ToString())))
                        hph.price = aRow["price"].ToString();
                    else
                    {
                        hphs = null;
                        ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Alert", String.Format("Price must be numbers (line {0})", id), true);
                        break;
                    }
                    hph.method = aRow["method"].ToString();
                    hphs[i] = hph;
                }
            }
            return hphs;
        }

        //private void UpDateHPHs()
        //{
        //    hphfDto[] hphs = new hphfDto[gvEdit.VisibleRowCount];
        //    UiDataService ws = ImqWS.GetTransactionService();
        //    for (int i = 0; i < gvEdit.VisibleRowCount; i++)
        //    {
        //        ASPxTextBox cb01 = (ASPxTextBox)gvEdit.FindRowCellTemplateControl(i, gvEdit.Columns["value"] as GridViewDataColumn, "textBox");
        //        int id = Convert.ToInt32(gvEdit.GetRowValues(i, gvEdit.KeyFieldName));

        //        if (cb01 != null)
        //        {
        //            DataRow aRow = tbl.Rows.Find(id);
        //            aRow["price"] = cb01.Text;
        //            hphfDto hph = new hphfDto() { dataTime = aRow["datetime"].ToString(), sequence = aRow["sequence"].ToString(), series = aRow["series"].ToString(), underlyingCode = aRow["series"].ToString() };
        //            //                    hph.price = aRow["price"].ToString();
        //            if ((aRow["price"].ToString() != String.Empty) && (TryParseToInt(aRow["price"].ToString())))
        //                hph.price = aRow["price"].ToString();
        //            else
        //            {
        //                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Price must be numbers (line " + id.ToString() + ")", true);
        //                break;
        //            }
        //            hph.method = aRow["method"].ToString();
        //            hphs[i] = hph;
        //            //ws.updateHph(hph);
        //            //ws.updateHpfs(
        //        }
        //    }
        //    ws.updateHphs(hphs);
        //}

        //private void UpDateHPFs()
        //{
        //    hphfDto[] hphs = new hphfDto[gvEdit.VisibleRowCount];
        //    UiDataService ws = ImqWS.GetTransactionService();
        //    for (int i = 0; i < gvEdit.VisibleRowCount; i++)
        //    {
        //        ASPxTextBox cb01 = (ASPxTextBox)gvEdit.FindRowCellTemplateControl(i, gvEdit.Columns["value"] as GridViewDataColumn, "textBox");
        //        int id = Convert.ToInt32(gvEdit.GetRowValues(i, gvEdit.KeyFieldName));

        //        if (cb01 != null)
        //        {
        //            DataRow aRow = tbl.Rows.Find(id);
        //            aRow["price"] = cb01.Text;
        //            hphfDto hph = new hphfDto();
        //            hph.dataTime = aRow["datetime"].ToString();
        //            hph.sequence = aRow["sequence"].ToString();
        //            hph.series = aRow["series"].ToString();
        //            hph.underlyingCode = aRow["series"].ToString();
        //            hph.price = aRow["price"].ToString();
        //            hph.method = aRow["method"].ToString();
        //            hphs[i] = hph;
        //            //ws.updateHph(hph);
        //            //ws.updateHpfs(
        //        }
        //    }
        //    ws.updateHpfs(hphs);
        //}

        protected void InitGrid()
        {
            UiDataService ws = null;
            try
            {
                 ws = ImqWS.GetTransactionService();
                hphfDto[] data = getData();
                if (data != null)
                {
                    switch (type)
                    {
                        case 0:
                            if (ws.updateHphs(data) > 0)
                                ShowMessage("Update success");
                            else
                                ShowMessage("Update Error");
                           
                            break;
                        case 1:
                            if (ws.updateHpfs(data) > 0)
                                ShowMessage("Update success");
                            else
                                ShowMessage("Update Error");
                           
                            break;
                    }
                }
                else
                    ShowMessage("Update Error");
            }catch (Exception ex)
            {
                if(ws != null)
                    ws.Abort();
            }
            finally
            {
                if(ws != null)
                    ws.Dispose();
            }

            
            

            //for (int i = 0; i < gvEdit.VisibleRowCount; i++)
            //{
            //    ASPxTextBox cb01 = (ASPxTextBox)gvEdit.FindRowCellTemplateControl(i, gvEdit.Columns["value"] as GridViewDataColumn, "textBox");
            //    int id = Convert.ToInt32(gvEdit.GetRowValues(i, gvEdit.KeyFieldName));

            //    if (cb01 != null)
            //    {
            //        DataRow aRow = tbl.Rows.Find(id);
            //        if ((String)aRow["price"] != cb01.Text)
            //        {
            //            aRow["price"] = cb01.Text;

            //            UiDataService ws = ImqWS.GetTransactionService();
            //            switch (type)
            //            {
            //                case 0 :
            //                    hphfDto hph = new hphfDto();
            //                    hph.dataTime = aRow["datetime"].ToString();
            //                    hph.sequence = aRow["sequence"].ToString();
            //                    hph.series = aRow["series"].ToString();
            //                    hph.underlyingCode = aRow["series"].ToString();
            //                    hph.price = aRow["price"].ToString();
            //                    hph.method = aRow["method"].ToString();
            //                    ws.updateHph(hph);
            //                    break;

            //                case 1 :
            //                    hphfDto hpf = new hphfDto();
            //                    hpf.dataTime = aRow["datetime"].ToString();
            //                    hpf.sequence = aRow["sequence"].ToString();
            //                    hpf.series = aRow["series"].ToString();
            //                    hpf.underlyingCode = aRow["series"].ToString();
            //                    hpf.price = aRow["price"].ToString();
            //                    hpf.method = aRow["method"].ToString();
            //                    ws.updateHpf(hpf);
            //                    break;
            //            }
            //        }
            //    }
            //}
        }

        public static bool TryParseToInt(string expression)
        {
            bool bRes;
            string[] xx = expression.Split(',');
            if (xx.Count() > 1) bRes = false;
            else
            {
                double dVal;
                bRes = double.TryParse(expression, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out dVal);
            }
            return bRes;
        }

        protected void saveData()
        {
            bool proses;
            for (int i = 0; i < gvEdit.VisibleRowCount; i++)
            {
                ASPxTextBox cb01 = (ASPxTextBox)gvEdit.FindRowCellTemplateControl(i, gvEdit.Columns["value"] as GridViewDataColumn, "textBox");
                int id = Convert.ToInt32(gvEdit.GetRowValues(i, gvEdit.KeyFieldName));
                proses = true;
                if (cb01 != null)
                {
                    DataRow aRow = tbl.Rows.Find(id);
                    aRow["price"] = cb01.Text;

                    UiDataService ws = ImqWS.GetTransactionService();
                    switch (type)
                    {
                        case 0:
                            hphfDto hph = new hphfDto() { dataTime = aRow["datetime"].ToString(), sequence = aRow["sequence"].ToString(), series = aRow["series"].ToString(), underlyingCode = aRow["series"].ToString() };
                            if ((aRow["price"].ToString() != String.Empty) && (TryParseToInt(aRow["price"].ToString())))
                                hph. price = aRow["price"].ToString();
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Alert", String.Format("Price must be numbers (line {0})", id), true);
                                proses = false;
                                break;
                            }
                            hph.method = aRow["method"].ToString();
                            ws.updateHph(hph);
                            putAuditTrail("Edit Settlement Price HPH", toString(hph));
                            break;

                        case 1:
                            hphfDto hpf = new hphfDto() { dataTime = aRow["datetime"].ToString(), sequence = aRow["sequence"].ToString(), series = aRow["series"].ToString(), underlyingCode = aRow["series"].ToString() };
                          
                            if ((aRow["price"].ToString() != String.Empty) && (TryParseToInt(aRow["price"].ToString())))
                                hpf.price = aRow["price"].ToString();
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Alert", String.Format("Price must be numbers (line {0})", id), true);
                                proses = false;
                                break;
                            }
                            hpf.method = aRow["method"].ToString();
                            ws.updateHpf(hpf);
                            putAuditTrail("Edit Settlement Price HPF", toString(hpf));
                            break;
                    }

                    if (!proses)
                        break;
                }
            }
        }


        protected void gvEdit_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (e.Parameters == "post")
            {
                addedit = int.Parse(Session["stat"].ToString());
                type = int.Parse(Request.QueryString["type"]);
                switch (addedit)
                {
                    case 0 :
                    case 1 :
                        InitGrid();
                        break;
                    case 2 :
                        saveData();
                        break;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/transaction/settlementPrice/settlementPrice.aspx");
        }

        public static String toString(hphfDto aDT)
        {
            return String.Format("[series={0}, underlyingCode={1}, sequence={2}, price={3}, method={4}, time={5}]", aDT.series, aDT.underlyingCode, aDT.sequence, aDT.price, aDT.method, String.Format("{0:HH:mm:ss}", aDT.dataTime));
        }

        private void putAuditTrail(String aActifity, String aMemo)
        {
            ImqWS.putAuditTrail(aUserLogin, "User", aActifity, aMemo);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            addedit = int.Parse(Session["stat"].ToString());
            type = int.Parse(Request.QueryString["type"]);
            switch (addedit)
            {
                case 0:
                case 1:
                case 2:
                    InitGrid();
                    break;
                case 3:
                    saveData();
                    break;
            }
        }

        private void ShowMessage(string info)
        {
            string mystring = String.Format(@"<script type='text/javascript'>
                                                              alert('{0}');
                                                              window.location='{1}'
                                                              </script>", info, target);
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(GetType(), "clientScript", mystring);
        }
    }
}
