﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Microsoft.VisualBasic;

using Common;
using Common.wsTransaction;
using Common.wsUserGroupPermission;
using System.IO;
using System.Globalization;

namespace imq.kpei.transaction.settlementPrice
{
    public partial class settlementPrice : System.Web.UI.Page
    {
        private priceHphfMasterDto[] stPrice;
        private DataTable tblHPH = null;
        private DataTable tblMeanDaily = null;
        private DataTable tblHPF = null;
        private DataTable tblMeanFinal = null;

        private String aUserLogin;
        private String aUserMember;
        private String target = @"../settlementPrice/settlementPrice.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            UiDataService ws = ImqWS.GetTransactionService();

            if (aUserMember.Equals("kpei") && ws.validateUploadHphfTime())
                enableBtn(true);
            else
                enableBtn(false);

            stPrice = getDataHPHFMaster();
            getDataHPH();
            getDataMeanDaily();

            getDataHPF();
            getDataMeanFinal();
        }

        private priceHphfMasterDto[] getDataHPHFMaster()
        {
            UiDataService ws = null;
            try
            {
                ws = ImqWS.GetTransactionService();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                if (ws != null)
                    ws.Abort();
                throw;
            }
            finally
            {
                if (ws != null)
                    ws.Dispose();
            }

            return ws.getSettlementPrice();
        }

        private void toTable(ref DataTable tbl, hphfDto[] row)
        {
            if (aUserMember.ToUpper().Equals("KPEI"))
                for (int m = 0; m < row.Length; m++)
                    tbl.Rows.Add(
                        row[m].sequence,
                        row[m].series,
                        m + 1,
                        row[m].price,
                        row[m].dataTime,
                        row[m].method);
            else
                for (int m = 0; m < row.Length; m++)
                    tbl.Rows.Add(
                        row[m].sequence,
                        row[m].series,
                        m + 1,
                        row[m].price,
                        row[m].samplingTime,
                        row[m].method);
        }

        private void getDataHPH()
        {
            if (!IsPostBack && !IsCallback)
            {
                tblHPH = CreateTableHP();

                if (tblHPH.Rows.Count > 0)
                    tblHPH.Clear();
                if (stPrice != null)
                    for (int n = 0; n < stPrice.Length; n++)
                    {
                        if (stPrice[n].hphList != null)
                            toTable(ref tblHPH, stPrice[n].hphList);
                    }
                Session["tblHPH"] = tblHPH;
            }
            else
                tblHPH = (DataTable)Session["tblHPH"];
            gridDaily.KeyFieldName = "sequence;series";
            gridDaily.DataSource = tblHPH;
            gridDaily.DataBind();
        }

        private void getDataHPF()
        {
            if (!IsPostBack && !IsCallback)
            {
                tblHPF = CreateTableHP();

                if (tblHPF.Rows.Count > 0)
                    tblHPF.Clear();
                if (stPrice != null)
                    for (int n = 0; n < stPrice.Length; n++)
                    {
                        if (stPrice[n].hpfList != null)
                            toTable(ref tblHPF, stPrice[n].hpfList);
                    }
                Session["tblHPF"] = tblHPF;
            }
            else
                tblHPF = (DataTable)Session["tblHPF"];
            gridFinal.KeyFieldName = "sequence;series";
            gridFinal.DataSource = tblHPF;
            gridFinal.DataBind();
        }

        private void getDataMeanDaily()
        {
            if (!IsPostBack && !IsCallback)
            {
                tblMeanDaily = CreateTableMean();

                if (tblMeanDaily.Rows.Count > 0)
                    tblMeanDaily.Clear();
                if (stPrice != null)
                    for (int n = 0; n < stPrice.Length; n++)
                        if (!stPrice[n].hph.Equals("0.00"))
                            tblMeanDaily.Rows.Add(stPrice[n].series, stPrice[n].hph);
                Session["tblMeanDaily"] = tblMeanDaily;
            }
            else
                tblMeanDaily = (DataTable)Session["tblMeanDaily"];
            gridMeanDaily.KeyFieldName = "series";
            gridMeanDaily.DataSource = tblMeanDaily;
            gridMeanDaily.DataBind();
        }

        private void getDataMeanFinal()
        {
            if (!IsPostBack && !IsCallback)
            {
                tblMeanFinal = CreateTableMean();

                if (tblMeanFinal.Rows.Count > 0)
                    tblMeanFinal.Clear();
                if (stPrice != null)
                    for (int n = 0; n < stPrice.Length; n++)
                        if (!stPrice[n].hpf.Equals("0.00"))
                            tblMeanFinal.Rows.Add(stPrice[n].series, stPrice[n].hpf);
                Session["tblMeanFinal"] = tblMeanFinal;
            }
            else
                tblMeanFinal = (DataTable)Session["tblMeanFinal"];
            gridMeanFinal.KeyFieldName = "series";
            gridMeanFinal.DataSource = tblMeanFinal;
            gridMeanFinal.DataBind();
        }



        DataTable CreateTableHP()
        {
            DataTable table = new DataTable();
            table.Columns.Add("sequence", typeof(String));
            table.Columns.Add("series", typeof(String));
            table.Columns.Add("id", typeof(String));
            table.Columns.Add("price", typeof(String));
            table.Columns.Add("datetime", typeof(String));
            table.Columns.Add("method", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["sequence"], table.Columns["series"] };
            return table;
        }

        DataTable CreateTableMean()
        {
            DataTable table = new DataTable();
            table.Columns.Add("series", typeof(String));
            table.Columns.Add("value", typeof(String));
            table.PrimaryKey = new DataColumn[] { table.Columns["series"] };
            return table;
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;
            enableBtn(false);
            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Settlement Price");
            for (int n = 0; n < dtFMP.Length; n++)
            {
                if (dtFMP[n].editDirectApproval)
                {
                    enableBtn(true);
                }
            }
        }

        private void enableBtn(bool aEnable)
        {
            btnEditHPF.Visible = aEnable;
            btnUploadHPF.Visible = aEnable;
            FileUploadHPF.Visible = aEnable;

            btnEditHPH.Visible = aEnable;
            btnUploadHPH.Visible = aEnable;
            FileUploadHPH.Visible = aEnable;
        }

        protected DataTable getHPHPrice()
        {
            DataRow row = gridDaily.GetDataRow(gridDaily.FocusedRowIndex);

            if (row != null)
            {
                //row = tblMeanDaily.Rows.Find(keys);
                for (int i = 0; i < stPrice.Length; i++)
                    if (stPrice[i].series.Equals(row["series"].ToString()))
                    {
                        DataTable tbl = CreateTableHP();
                        if(aUserMember.Equals("kpei"))
                            for (int j = 0; j < stPrice[i].hphList.Length; j++)
                            {
                                tbl.Rows.Add(
                                    stPrice[i].hphList[j].sequence,
                                    stPrice[i].hphList[j].series,
                                    j + 1,
                                    stPrice[i].hphList[j].price,
                                    stPrice[i].hphList[j].dataTime,
                                    stPrice[i].hphList[j].method);
                            }
                        else
                            for (int j = 0; j < stPrice[i].hphList.Length; j++)
                            {
                                tbl.Rows.Add(
                                    stPrice[i].hphList[j].sequence,
                                    stPrice[i].hphList[j].series,
                                    j + 1,
                                    stPrice[i].hphList[j].price,
                                    stPrice[i].hphList[j].samplingTime,
                                    stPrice[i].hphList[j].method);
                            } 
                        return tbl;
                    }
            }
            return null;
        }

        protected DataTable getHPFPrice()
        {
            DataRow row = gridFinal.GetDataRow(gridFinal.FocusedRowIndex);
            if (row != null)
            {
                for (int i = 0; i < stPrice.Length; i++)
                    if (stPrice[i].series.Equals(row["series"].ToString()))
                    {
                        DataTable tbl = CreateTableHP();
                        if (aUserMember.Equals("kpei"))
                            for (int j = 0; j < stPrice[i].hpfList.Length; j++)
                            {
                                tbl.Rows.Add(
                                    stPrice[i].hpfList[j].sequence,
                                    stPrice[i].hpfList[j].series,
                                    j + 1,
                                    stPrice[i].hpfList[j].price,
                                    stPrice[i].hpfList[j].dataTime,
                                    stPrice[i].hpfList[j].method);
                            }
                        else
                            for (int j = 0; j < stPrice[i].hpfList.Length; j++)
                            {
                                tbl.Rows.Add(
                                    stPrice[i].hpfList[j].sequence,
                                    stPrice[i].hpfList[j].series,
                                    j + 1,
                                    stPrice[i].hpfList[j].price,
                                    stPrice[i].hpfList[j].samplingTime,
                                    stPrice[i].hpfList[j].method);
                            }

                        return tbl;
                    }
            }
            return null;
        }

        protected void btnEditHPH_Click(object sender, EventArgs e)
        {
            DataTable dt = getHPHPrice();
            if (dt != null)
            {
                Session["stat"] = "1";
                Session["tbl"] = dt;
                Response.Redirect("~/transaction/settlementPrice/settlementPriceEdit.aspx?type=0&stat=1");
            }
        }

        protected void btnEditHPF_Click(object sender, EventArgs e)
        {
            DataTable dt = getHPFPrice();
            if (dt != null)
            {
                Session["stat"] = "1";
                Session["tbl"] = dt;
                Response.Redirect("~/transaction/settlementPrice/settlementPriceEdit.aspx?type=1&stat=1");
            }
        }

        protected void fileUploadHPH()
        {
            if (FileUploadHPH.HasFile)
                try
                {
                    FileUploadHPH.SaveAs("\\Uploads\\HPH" + aUserLogin + aUserMember + FileUploadHPH.FileName);
                }
                catch (Exception ex)
                {
                }
        }

        protected void fileUploadHPF()
        {
            if (FileUploadHPF.HasFile)
                try
                {
                    FileUploadHPF.SaveAs("\\Uploads\\HPF" + aUserLogin + aUserMember + FileUploadHPF.FileName);
                }
                catch (Exception ex)
                {
                }
        }

        public bool TryParseToInt(string expression)
        {
            double dVal;
            bool bRes;

            bRes = double.TryParse(expression, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out dVal);

            return bRes;
        }

        public bool TryParseToTime(string expression)
        {
            bool bRes = true;
            string[] hm = expression.Split(':');

            for (int x = 0; x < hm.Count(); x++)
            {
                for (int y = 0; y < hm[x].Length; y++)
                    if (!Char.IsDigit(hm[x], y))
                    {
                        bRes = false;
                        break;
                    }
                if (!bRes) break;
                if (x == 0)
                {
                    if ((0 > Convert.ToInt32(hm[0])) || (Convert.ToInt32(hm[0]) > 24))
                        bRes = false;

                }
                else
                {
                    if ((0 > Convert.ToInt32(hm[x])) || (Convert.ToInt32(hm[x]) > 59))
                        bRes = false;
                }
            }

            return bRes;
        }

        protected void checkValidasi(StreamReader sr)
        {
            DataTable dt = getHPHPrice();
            if (dt != null)
            {
                DataRow dr;
                int x = 0;
                String[] values = null;
                bool proses = true;

                List<string> lines = new List<string>();

                using(sr)
                {
                    while(!sr.EndOfStream) lines.Add(sr.ReadLine());
                }
                if ((lines.Count == 4) || (lines.Count == 8))
                {
                    for (int m = 0; m < lines.Count; m++)
                    {
                        values = lines[m].Split('\t');
                        dr = dt.Rows[x];
                        if (values[0] == dr["series"].ToString())
                        {
                            //if (values[0] != "") dr["series"] = values[0];
                            //if (values[1] != "") dr["price"] = values[1];
                            //if (values[2] != "") dr["datetime"] = values[2];
                            if ((values[1] != "") && (TryParseToInt(values[1])))
                                dr["price"] = values[1];
                            else
                            {
                                proses = false;
                                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Price must be numbers (line " + x.ToString() + ")", true);
                                break;
                            }

                            if ((values[2] != "") && (TryParseToTime(values[2])))
                                dr["datetime"] = values[2];
                            else
                            {
                                proses = false;
                                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Time must be numbers (line " + x.ToString() + ")", true);
                                break;
                            }
                        }
                        else
                        {
                            proses = false;
                            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Series is not the same (line " + x.ToString() + ")", true);
                            break;
                        }

                        x += 1;
                    }

                }
                else
                {
                    proses = false;
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Row Count must be 4 or 8 ", true);
                }
                sr.Close();
                if (proses)
                {
                    Session["stat"] = "2";
                    Session["tbl"] = dt;
                    Response.Redirect("~/transaction/settlementPrice/settlementPriceEdit.aspx?type=0&stat=2");
                }
            }
        }

        protected void btnUploadHPH_Click(object sender, EventArgs e)
        {
            fileUploadHPH();
            try
            {
                StreamReader sr = new System.IO.StreamReader("\\Uploads\\HPH" + aUserLogin + aUserMember + FileUploadHPH.FileName);
                if (sr != null)
                {
                    DataTable dt = getHPHPrice();
                    if (dt != null)
                    {
                        DataRow dr;
//                        string inputLine = "";
                        int x = 0;
                        String[] values = null;
                        bool proses = true;

                        List<string> lines = new List<string>();

                        using (sr)
                        {
                            while (!sr.EndOfStream) lines.Add(sr.ReadLine());
                        }

                        if ((lines.Count == 4) || (lines.Count == 8))
                        {
                            int pos = 0;
                            for (int m = 0; m < lines.Count; m++)
                            {
                                pos += 1;
                                values = lines[m].Split('\t');
                                dr = dt.Rows[x];
                                if (values[0] == dr["series"].ToString())
                                {
                                    //if (values[0] != "") dr["series"] = values[0];
                                    //if (values[1] != "") dr["price"] = values[1];

                                    if ((values[1] != "") && (TryParseToInt(values[1])))
                                        dr["price"] = values[1];
                                    else
                                    {
                                        proses = false;
                                        ShowMessage("Price must be numbers (line " + x.ToString() + ")");
                                        //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Price must be numbers (line " + x.ToString() + ")", true);
                                        break;
                                    }
                                    try
                                    {
                                        if ((values[2] != "") && (TryParseToTime(values[2])))
                                            dr["datetime"] = values[2];
                                        else
                                        {
                                            proses = false;
                                            ShowMessage("Time must be numbers (line " + pos.ToString() + ")");
                                            break;
                                        }
                                    }
                                    catch (IOException ex)
                                    {
                                        proses = false;
                                        ShowMessage("Time must be numbers (line " + pos.ToString() + ")");
                                    }
                                }
                                else
                                {
                                    proses = false;
                                    ShowMessage("Series is not the same (line " + pos.ToString() + ")");
                                    //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Series is not the same (line " + x.ToString() + ")", true);
                                    break;
                                }

                                x += 1;

                            }
                        }
                        else
                        {
                            proses = false;
                            ShowMessage("Row Count must be 4 or 8 ");
                            //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Row Count must be 4 or 8 ", true);
                        }

                        sr.Close();
                        if (proses)
                        {
                            ShowMessage("Upload HPH done ");
                            Session["stat"] = "2";
                            Session["tbl"] = dt;
                            Response.Redirect("~/transaction/settlementPrice/settlementPriceEdit.aspx?type=0&stat=2");
                        }
                    }

                    else // if table null
                    {
                        ShowMessage("No data in table");
                    }

                }
            }
            catch (IOException ex)
            {

            }
            finally
            {
                File.Delete("\\Uploads\\HPH" + aUserLogin + aUserMember + FileUploadHPH.FileName);
            }

        }

        protected void btnUploadHPF_Click(object sender, EventArgs e)
        {
            fileUploadHPF();
            try
            {
                StreamReader sr = new System.IO.StreamReader("\\Uploads\\HPF" + aUserLogin + aUserMember + FileUploadHPF.FileName);
                if (sr != null)
                {
                    DataTable dt = getHPFPrice();
                    if (dt != null)
                    {
                        DataRow dr;
//                        string inputLine = "";
                        int x = 0;
                        String[] values = null;
                        bool proses = true;

                        List<string> lines = new List<string>();

                        using (sr)
                        {
                            while (!sr.EndOfStream) lines.Add(sr.ReadLine());
                        }
                        if ((lines.Count == 4) )
                        {
                            for (int m = 0; m < lines.Count; m++)
                            {
                                values = lines[m].Split('\t');
                                dr = dt.Rows[x];
                                if (values[0] == dr["series"].ToString())
                                {
                                    //if (values[0] != "") dr["series"] = values[0];
                                    //if (values[1] != "") dr["price"] = values[1];
                                    //if (values[2] != "") dr["datetime"] = values[2];
                                    if ((values[1] != "") && (TryParseToInt(values[1])))
                                        dr["price"] = values[1];
                                    else
                                    {
                                        proses = false;
                                        ShowMessage("Price must be numbers (line " + x.ToString() + ")");
                                        //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Price must be numbers (line " + x.ToString() + ")", true);
                                        break;
                                    }

                                    if ((values[2] != "") && (TryParseToTime(values[2])))
                                        dr["datetime"] = values[2];
                                    else
                                    {
                                        proses = false;
                                        ShowMessage("Time must be numbers (line " + x.ToString() + ")");
                                        break;
                                    }

                                }
                                else
                                {
                                    proses = false;
                                    ShowMessage("Series is not the same (line " + x.ToString() + ")");
                                    //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Series is not the same (line "+ x.ToString() +")", true);
                                    break;
                                }
                                x += 1;
                            }
                        }
                        else
                        {
                            proses = false;
                            ShowMessage("Row Count must be 4 ");
                            //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "Row Count must be 4 or 8 ", true);
                        }
 
                        sr.Close();
                        if (proses)
                        {
                            ShowMessage("Upload HPH done ");
                            Session["stat"] = "2";
                            Session["tbl"] = dt;
                            Response.Redirect("~/transaction/settlementPrice/settlementPriceEdit.aspx?type=1&stat=2");
                        }
                    }

                    else // if table null
                    {
                        ShowMessage("No data in table");
                    }

                }
            }
            catch (IOException ex)
            {

            }
            finally
            {
                File.Delete("\\Uploads\\HPF" + aUserLogin + aUserMember + FileUploadHPH.FileName);
            }

        }

        private void ShowMessage(string info)
        {
            string mystring = @"<script type='text/javascript'>
                                alert('" + info + @"');
                                window.location='" + target + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }
    }
}