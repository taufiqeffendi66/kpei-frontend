﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var positionSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var positionInitialData = [];
var cb;
var storeCb;
var recordIndex = -1;

var arrPosition = [];
var endProses = true;
var pageSize = 20;
var tick = 200;

function addArrPosition(s, force) {
    data = eval('(' + s + ')');
    arrPosition.push(data);
}

function checkData() {
    if (endProses) loadToStore()
}

function loadToStore() {

    if (arrPosition.length > 0) {
        endProses = false;
        if (arrPosition.length > pageSize)
            max = pageSize;
        else
            max = arrPosition.length;

        for (xx = 0; xx < max; xx++) {
            dPop = arrPosition.shift()
            addToPosition(dPop);
        }
        Ext.getCmp('pagingToolbar').doRefresh();
        endProses = true;
    }
}

function addToPosition(data) {
    if (positionInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    //data = eval('(' + s + ')');
    if ((typeof data.series === "undefined") || (data.series == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < positionInitialData.length; i++) {
        if ((data.memberID == positionInitialData[i].memberID) && (data.sID == positionInitialData[i].sID) && (data.series == positionInitialData[i].series)) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        positionInitialData.push(data);
    } else {
        positionInitialData[recordIndex].memberID = data.memberID;
        positionInitialData[recordIndex].sID = data.sID;
        positionInitialData[recordIndex].role = data.role;
        positionInitialData[recordIndex].series = data.series;
        positionInitialData[recordIndex].contracType = data.contracType;
        positionInitialData[recordIndex].netB = data.netB;
        positionInitialData[recordIndex].netS = data.netS;
        positionInitialData[recordIndex].excercise = data.excercise;
        positionInitialData[recordIndex].freq = data.freq;
        positionInitialData[recordIndex].tradingId = data.tradingId;
        positionInitialData[recordIndex].volume = data.volume;
    }

    //Ext.getCmp('pagingToolbar').doRefresh();
}


function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addposition(s, force) {
    if (positionInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.series === "undefined") || (data.series == null)) {
        return;
    }

    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });

    //is data exists
    var recordIndex = -1;
    for (i = 0; i < positionInitialData.length; i++) {
        if ((data.memberID == positionInitialData[i].memberID) && (data.sID == positionInitialData[i].sID) && (data.series == positionInitialData[i].series)) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        positionInitialData.push(data);
    } else {
        positionInitialData[recordIndex].memberID = data.memberID;
        positionInitialData[recordIndex].sID = data.sID;
        positionInitialData[recordIndex].role = data.role;
        positionInitialData[recordIndex].series = data.series;
        positionInitialData[recordIndex].contracType = data.contracType;
        positionInitialData[recordIndex].netB = data.netB;
        positionInitialData[recordIndex].netS = data.netS;
        positionInitialData[recordIndex].excercise = data.excercise;
        positionInitialData[recordIndex].freq = data.freq;
        positionInitialData[recordIndex].tradingId = data.tradingId;
        positionInitialData[recordIndex].volume = data.volume;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function findTradingId(memberId, SID, series) {
    for (i = 0; i < positionInitialData.length; i++) {
        if ((memberId == positionInitialData[i].memberID) && (SID == positionInitialData[i].sID) && (series == positionInitialData[i].series)) {
            return positionInitialData[i].tradingId;
            break;
        }
    }
    return -1;
}

function subscribepositionTopic(direction) {
    if ((!(typeof positionSubscriptionId === "undefined")) && (positionSubscriptionId != null)) {
        client.unsubscribe(positionSubscriptionId);
        positionSubscriptionId = null;
    }

    for (; positionInitialData.length > 0; )
        positionInitialData.shift();

    Ext.getCmp('pagingToolbar').doRefresh();

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var positionSubscritptionHeader = { 'selector': selector };
        positionSubscriptionId = client.subscribe(positionTopic, function (message) {
            addArrPosition(message.body, true);
        }, positionSubscritptionHeader);
    } else {
        positionSubscriptionId = client.subscribe(positionTopic, function (message) {
            addArrPosition(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=position&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue(),
        success: function (response, opts) {
            var obj = eval('(' + response.responseText + ')');
            for (ia = 0; ia < obj.length; ia++) {
                addArrPosition(obj[ia].positionData, false);
            }
        },
        failure: function (response, opts) {
        }
    });
}

//function putExercise(Role, SID, ContractId, TradeID, Quantity, permission) {
//    Ext.Ajax.request({
//        url: '../request.aspx?type=exerciseNew&permission=' + permission + '&maxId=' + Role + '&contractId=' + ContractId + '&sid=' + SID + '&contractType=' + TradeID + '&minId=' + Quantity,
//        success: function (response, opts) {
//            alert(' b e r h a s i l ');
//        },
//        failure: function (response, opts) {
//        }
//    });
//}

function putExercise(Role, SID, ContractId, TradeID, Quantity, permission) {
    Ext.Ajax.request({
        url: '../request.aspx?type=exerciseNew&permission=' + permission + '&maxId=' + Role + '&contractId=' + ContractId + '&sid=' + SID + '&contractType=' + TradeID + '&minId=' + Quantity,
        success: function (response, opts) {
            var obj = eval('(' + response.responseText + ')');
            if (obj.length > 0) {
                newData = eval('(' + obj[0].exerciseNew + ')');
                if (newData.result == -2)
                    alert('Failed on store exercise to database');
                else if (newData.result == -1) {
                    alert('Failed on time validation');
                    Ext.getCmp('exebtn').disable();
                }
                else
                    alert('Succes');

            }
            //            for (i = 0; i < obj.length; i++) {
            //                addArrExercise(obj[i].exerciseData, false);
            //            }
        },
        failure: function (response, opts) {
        }
    });
}


var exerciseFormWin;
var exerciseForm;

var exerciseValidateFormWin;
var exerciseValidateForm;

var exerciseConfirmFormWin;
var exerciseConfirmForm;

function createExerciseFormWin() {
    if (!exerciseFormWin) {
        exerciseForm = Ext.widget('form', {
            border: false,
            //bodyPadding: 10,
            bodyStyle: 'padding:5px 5px 0',
            width: 288,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaults: {
                margins: '0 0 10 0',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'displayfield',
                    name: 'displayfieldSid',
                    id: 'displayfieldSid',
                    fieldLabel: 'SID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldRole',
                    id: 'displayfieldRole',
                    fieldLabel: 'Role',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldContractId',
                    id: 'displayfieldContractId',
                    fieldLabel: 'Contract ID',
                    value: ''
                },
                {
                    xtype: 'numberfield',
                    name: 'numberfieldQuantity',
                    id: 'numberfieldQuantity',
                    fieldLabel: 'Quantity',
                    minValue : 1,
                    value: ''
                }
            ],

            buttons:
                [
                    {
                        text: 'Continue',
                        handler: function () {
                            if (this.up('form').getForm().isValid()) {
                                this.up('window').hide();
                                selectedRecords = Ext.getCmp('positionGrid').getSelectionModel().getSelection();
                                Ext.Ajax.request({
                                    url: '../request.aspx?type=exerciseValidate&maxId=' + selectedRecords[0].get('role') + '&contractId=' + selectedRecords[0].get('series') + '&sid=' + selectedRecords[0].get('sID') + '&contractType=' + selectedRecords[0].get('tradingId') + '&minId=' + Ext.getCmp('numberfieldQuantity').getValue(),                                    
                                    success: function (response, opts) {
                                        var obj = eval('(' + response.responseText + ')');
                                        if (obj.length > 0) {
                                            newData = eval('(' + obj[0].exerciseValidate + ')');
                                            if (newData.validate >= 0) {
                                                createExerciseConfirmFormWin();
                                                Ext.getCmp('displayfieldConfirmRole').setValue(selectedRecords[0].get('role'));
                                                Ext.getCmp('displayfieldConfirmSid').setValue(selectedRecords[0].get('sID'));
                                                Ext.getCmp('displayfieldConfirmContractId').setValue(selectedRecords[0].get('series'));
                                                Ext.getCmp('displayfieldConfirmQuantity').setValue(Ext.getCmp('numberfieldQuantity').getValue());
                                                exerciseConfirmFormWin.show();
                                            }
                                            else {
                                                var minta = parseInt(Ext.getCmp('numberfieldQuantity').getValue());
                                                var hasil = newData.validate + minta;
                                                createExerciseValidateFormWin();
                                                Ext.getCmp('displayfieldValidateRole').setValue(selectedRecords[0].get('role'));
                                                Ext.getCmp('displayfieldValidateSid').setValue(selectedRecords[0].get('sID'));
                                                Ext.getCmp('displayfieldValidateContractId').setValue(selectedRecords[0].get('series'));
                                                Ext.getCmp('displayfieldValidateAvailable').setValue(hasil);
                                                Ext.getCmp('numberfieldValidateQuantity').setValue(Ext.getCmp('numberfieldQuantity').getValue());
                                                exerciseValidateFormWin.show();
                                            }
                                        }
                                    },
                                    failure: function (response, opts) {
                                    }
                                });
                            }
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: function () {
                            this.up('form').getForm().reset();
                            this.up('window').hide();
                        }
                    }
                ]
        });

        exerciseFormWin = Ext.widget('window', {
            title: 'Exercise Form',
            closeAction: 'hide',
            width: 300,
            height: 205,
            minHeight: 205,
            layout: 'fit',
            resizable: false,
            modal: true,
            items: exerciseForm
        });
    }
    exerciseForm.getForm().reset();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function createExerciseValidateFormWin() {
    if (!exerciseValidateFormWin) {
        exerciseValidateForm = Ext.widget('form', {
            border: false,
            //bodyPadding: 10,
            bodyStyle: 'padding:5px 5px 0',
            width: 288,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaults: {
                margins: '0 0 10 0',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateSid',
                    id: 'displayfieldValidateSid',
                    fieldLabel: 'SID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateRole',
                    id: 'displayfieldValidateRole',
                    fieldLabel: 'Role',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateContractId',
                    id: 'displayfieldValidateContractId',
                    fieldLabel: 'Contract ID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldValidateAvailable',
                    id: 'displayfieldValidateAvailable',
                    fieldLabel: 'Available',
                    value: ''
                },
                {
                    xtype: 'numberfield',
                    name: 'numberfieldValidateQuantity',
                    id: 'numberfieldValidateQuantity',
                    fieldLabel: 'Quantity',
                    value: ''
                }
            ],

            buttons:
                [
                    {
                        text: 'Submit',
                        handler: function () {

                            var minta = parseInt(Ext.getCmp('numberfieldValidateQuantity').getValue());
                            var available = parseInt(Ext.getCmp('displayfieldValidateAvailable').getValue());
                            if ((minta > 0) && (minta <= available)) {
                                if (this.up('form').getForm().isValid()) {
                                    this.up('window').hide();
                                    createExerciseConfirmFormWin();
                                    Ext.getCmp('displayfieldConfirmRole').setValue(Ext.getCmp('displayfieldValidateRole').getValue());
                                    Ext.getCmp('displayfieldConfirmSid').setValue(Ext.getCmp('displayfieldValidateSid').getValue());
                                    Ext.getCmp('displayfieldConfirmContractId').setValue(Ext.getCmp('displayfieldValidateContractId').getValue());
                                    Ext.getCmp('displayfieldConfirmQuantity').setValue(Ext.getCmp('numberfieldValidateQuantity').getValue());
                                    exerciseConfirmFormWin.show();
                                }
                            }
                            else {
                                alert('Quantity > Available');
                            }
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: function () {
                            this.up('form').getForm().reset();
                            this.up('window').hide();
                        }
                    }
                ]
        });

        exerciseValidateFormWin = Ext.widget('window', {
            title: 'Exercise Validate',
            closeAction: 'hide',
            width: 300,
            height: 205,
            minHeight: 205,
            layout: 'fit',
            resizable: false,
            modal: true,
            items: exerciseValidateForm
        });
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function createExerciseConfirmFormWin() {
    if (!exerciseConfirmFormWin) {
        roleItems = [];
        if (isMaker == '1')
            roleItems.push({ boxLabel: 'Maker', name: 'rb-col', inputValue: 1 });
        if (isDirectChecker == '1')
            roleItems.push({ boxLabel: 'Direct Checker', name: 'rb-col', inputValue: 2 });
        if (isDirectApprover == '1')
            roleItems.push({ boxLabel: 'Direct Approve', name: 'rb-col', inputValue: 3 });

        exerciseConfirmForm = Ext.widget('form', {
            border: false,
            //bodyPadding: 10,
            bodyStyle: 'padding:5px 5px 0',
            width: 288,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaults: {
                margins: '0 0 10 0',
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Role',
                    name: 'radiogroupRole',
                    id: 'radiogroupRole',
                    columns: 1,
                    items: roleItems
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmSid',
                    id: 'displayfieldConfirmSid',
                    fieldLabel: 'SID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmRole',
                    id: 'displayfieldConfirmRole',
                    fieldLabel: 'Role',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmContractId',
                    id: 'displayfieldConfirmContractId',
                    fieldLabel: 'Contract ID',
                    value: ''
                },
                {
                    xtype: 'displayfield',
                    name: 'displayfieldConfirmQuantity',
                    id: 'displayfieldConfirmQuantity',
                    fieldLabel: 'Quantity',
                    value: ''
                }
            ],

            buttons:
                [
                    {
                        text: 'Submit',
                        handler: function () {
                            var aPermission = Ext.getCmp('radiogroupRole').getValue()['rb-col'];
                            if (aPermission != undefined) {
                                selectedRecords = Ext.getCmp('positionGrid').getSelectionModel().getSelection();
                                putExercise(Ext.getCmp('displayfieldConfirmRole').getValue(),
                                        Ext.getCmp('displayfieldConfirmSid').getValue(),
                                        Ext.getCmp('displayfieldConfirmContractId').getValue(),
                                        selectedRecords[0].get('tradingId'),
                                        Ext.getCmp('displayfieldConfirmQuantity').getValue(),
                                        aPermission);

                                this.up('form').getForm().reset();
                                this.up('window').hide();
                            } else
                                alert('Select Permission first');
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: function () {
                            this.up('form').getForm().reset();
                            this.up('window').hide();
                        }
                    }
                ]
        });

        exerciseConfirmFormWin = Ext.widget('window', {
            title: 'Exercise Confirmation',
            closeAction: 'hide',
            width: 300,
            height: 260,
            minHeight: 260,
            layout: 'fit',
            resizable: false,
            modal: true,
            items: exerciseConfirmForm
        });
    }
}

function reportTopic(direction) {
    if ((!(typeof positionSubscriptionId === "undefined")) && (positionSubscriptionId != null)) {
        client.unsubscribe(positionSubscriptionId);
        positionSubscriptionId = null;
    }

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
        else
            selector = selector + " and memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var positionSubscritptionHeader = { 'selector': selector };
        positionSubscriptionId = client.subscribe(positionTopic, function (message) {
            addArrPosition(message.body, true);
        }, positionSubscritptionHeader);
    } else {
        positionSubscriptionId = client.subscribe(positionTopic, function (message) {
            addArrPosition(message.body, true);
        });
    }
    window.open('./positionReportView.aspx?type=position&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}

Ext.onReady(function () {
    setInterval(function () { checkData() }, tick);
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var positionModel = Ext.define('positionModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'memberID' },
               { name: 'sID' },
               { name: 'role' },
               { name: 'series' },
               { name: 'contractType' },
               { name: 'netB' },
               { name: 'netS' },
               { name: 'excercise' },
               { name: 'freq' },
               { name: 'tradingId' },
               { name: 'volume' }
            ]
    });

    var positionReader = new Ext.data.reader.Json({
        model: 'positionModel'
    }, positionModel);

    var positionStore = Ext.create('Ext.data.Store', {
        id: 'positionStore',
        model: 'positionModel',
        data: positionInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(positionInitialData),
        autoLoad: true,
        autoSync: true,
        reader: positionReader
    });

    // create the Grid
    var positionGrid = Ext.create('Ext.grid.Panel', {
        store: positionStore,
        id: 'positionGrid',
        stateful: false,
        stateId: 'positionGrid',
        columns: [
                { text: 'Member<br>ID', dataIndex: 'memberID', width: 60, sortable: true, align: 'center', draggable: false },
                { text: 'Role', dataIndex: 'role', width: 50, sortable: true, align: 'center', draggable: false },
                { text: 'SID', dataIndex: 'sID', width: 120, sortable: true, align: 'left', draggable: false },
                { text: 'Contract ID', dataIndex: 'series', flex: 1, sortable: true, align: 'left', draggable: false },
                { text: 'Contract<br/>Type(O/F)', dataIndex: 'contractType', width: 65, sortable: true, align: 'center', draggable: false },
                { text: 'Net', columns: [
                    { text: 'Sell', dataIndex: 'netS', width: 70, sortable: true, align: 'right', draggable: false },
                    { text: 'Buy', dataIndex: 'netB', width: 70, sortable: true, align: 'right', draggable: false }
                ]
                },
                { text: 'Freq', dataIndex: 'freq', width: 100, sortable: true, align: 'right', draggable: false },
                { text: 'Volume', dataIndex: 'volume', width: 100, sortable: true, align: 'right', draggable: false }
        ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom
        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: positionStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })
    });
    positionGrid.setLoading(false, false);
    positionStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var memberStore = Ext.create('Ext.data.Store', {
        model: 'Member',
        data: members
    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function (combo, value) {
                combo = Ext.getCmp('sidCombo');
                combo.clearValue();
                combo.getStore().removeAll();
                Ext.Ajax.request({
                    url: '../request.aspx?type=sid&memberId=' + Ext.getCmp('memberCombo').getValue(),
                    success: function (response, opts) {
                        var obj = eval('(' + response.responseText + ')');
                        for (i = 0; i < obj.length; i++) {
                            newData = eval('(' + obj[i].sidData + ')');
                            combo.getStore().add({ abbr: newData.abbr, name: newData.name });
                        }
                    },
                    failure: function (response, opts) {
                    }
                });
            }
            }
        }
    });

    memberCombo.on('select', function (box, record, index) {

    });

//    Ext.define('ContractId', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function () {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
                    { xtype: 'label', text: 'Contract ID: ' },
                    contractIdCombo,
                    { xtype: 'label', text: 'Contract Type: ' },
                    contractTypeCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function () {
                            subscribepositionTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { id : 'exebtn', xtype: 'button', text: ' Exercise ',
                        handler: function () {
                            selectedRecords = Ext.getCmp('positionGrid').getSelectionModel().getSelection();
                            if (selectedRecords.length > 0) {
                                var minta = parseInt(selectedRecords[0].get('netB'));
                                cType = selectedRecords[0].get('contractType');
                                if ((minta > 0) && ( cType == "O")) {
                                    createExerciseFormWin();
                                    Ext.getCmp('displayfieldRole').setValue(selectedRecords[0].get('role'));
                                    Ext.getCmp('displayfieldSid').setValue(selectedRecords[0].get('sID'));
                                    Ext.getCmp('displayfieldContractId').setValue(selectedRecords[0].get('series'));
                                    Ext.getCmp('numberfieldQuantity').setValue(selectedRecords[0].get('netB'));
                                    Ext.getCmp('numberfieldQuantity').setMaxValue(selectedRecords[0].get('netB'));
                                    exerciseFormWin.show();
                                } else {
                                    Ext.MessageBox.show({
                                        title: 'Error',
                                        msg: 'Exercise volume less or equal with Net Buy or available only option Contract',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                }

                            } else {
                                Ext.MessageBox.show({
                                    title: 'Error',
                                    msg: 'No data selected',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });
    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Position Summary',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: positionGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    if ((filt == '$#@') || ( bNew==0 )) {
        Ext.getCmp('exebtn').disable();
    } else if ((isMaker == '0') && (isDirectChecker == '0') && (isDirectApprover == '0')) {
        Ext.getCmp('exebtn').disable();
    }

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function (str) {
    };
    var onconnect = function (frame) {
        Ext.getCmp('positionGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function (message) {
        });

        subscribepositionTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function () {
        if ((!(typeof positionSubscriptionId === "undefined")) && (positionSubscriptionId != null)) {
            client.unsubscribe(positionSubscriptionId);
            positionSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function () {
        });
    }
});
