﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Common.wsCommonList;
using System.Text;
using Common.wsUserGroupPermission;
using Common.wsTransaction;

namespace imq.kpei.transaction.position
{
    public partial class position : System.Web.UI.Page
    {
        private String aUserLogin;
        private String aUserMember;
        private int isMaker = 0;
        private int isDirectChecker = 0;
        private int isDirectApprover = 0;
        private int bNew = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                ImqSession.ValidateAction(this);

                aUserLogin = Session["SESSION_USER"].ToString();
                aUserMember = Session["SESSION_USERMEMBER"].ToString();
                ////redirect to login page if not logged in
                //ImqSession.Validate(this);
                ////redirect to blank page if accessed directly
                //ImqSession.ValidatedAction(this);           

                string jmsServer = HttpContext.Current.Request.Url.Host; //ImqSession.GetConfig("JMS_SERVER_WS");
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string positionTopic = ImqSession.GetConfig("JMS_TOPIC_POSITION");
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string user = Session["SESSION_USER"].ToString();
                string password = Session.SessionID;
                string member = Session["SESSION_USERMEMBER"].ToString();
                string userJMS = user + "-" + member;
                var s = "@#$";

                //UiCommonListService ws = ImqWS.GetCommonListService();

                //commonListDto[] membersDto;
                //try
                //{
                //    membersDto = ws.listMember(Session["SESSION_USERMEMBER"].ToString());
                //}
                //catch
                //{
                //    membersDto = new commonListDto[0];
                //}
                //StringBuilder membersSb = new StringBuilder();
                //for (int i = 0; i < membersDto.Length; i++)
                //{
                //    if (i > 0)
                //        membersSb.Append(",");
                //    membersSb.Append("{");
                //    membersSb.Append("\"abbr\":");
                //    membersSb.Append("\"" + membersDto[i].id + "\"");
                //    membersSb.Append(",\"name\":");
                //    membersSb.Append("\"" + membersDto[i].name + "\"");
                //    membersSb.Append("}");
                //}

                //commonListDto[] contractsDto;
                //try
                //{
                //    contractsDto = ws.listContract();
                //}
                //catch
                //{
                //    contractsDto = new commonListDto[0];
                //}
                //StringBuilder contractsSb = new StringBuilder();
                //for (int i = 0; i < contractsDto.Length; i++)
                //{
                //    if (i > 0)
                //        contractsSb.Append(",");
                //    contractsSb.Append("{");
                //    contractsSb.Append("\"abbr\":");
                //    contractsSb.Append("\"" + contractsDto[i].id + "\"");
                //    contractsSb.Append(",\"name\":");
                //    contractsSb.Append("\"" + contractsDto[i].name + "\"");
                //    contractsSb.Append("}");
                //}

                //string sessionId = Session["SESSION_USERMEMBER"].ToString();
                //string memberId;
                //if (sessionId.Equals("kpei"))
                //{

                //    memberId = "";

                //}
                //else
                //    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];

                //commonListDto[] sidDto;
                //try
                //{
                //    sidDto = ws.listSid(sessionId, memberId);
                //}
                //catch
                //{
                //    sidDto = new commonListDto[0];
                //}
                //StringBuilder sidSb = new StringBuilder();
                //for (int i = 0; i < sidDto.Length; i++)
                //{
                //    if (i > 0)
                //        sidSb.Append(",");
                //    sidSb.Append("{");
                //    sidSb.Append("\"abbr\":");
                //    sidSb.Append("\"" + sidDto[i].id + "\"");
                //    sidSb.Append(",\"name\":");
                //    sidSb.Append("\"" + sidDto[i].name + "\"");
                //    sidSb.Append("}");
                //}

                if (member == "kpei")
                {
                    s = "$#@";
                }
                UiDataService wst = ImqWS.GetTransactionService();
                if (wst.validateExerciseTime())
                    bNew = 1;
                else
                    bNew = 0;

                Permission();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "initPage", @"                    
<link rel=""stylesheet"" type=""text/css"" href=""../../js/extjs/resources/css/ext-all-gray.css"" />
<script type=""text/javascript"" src=""../../js/util/page.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ext-all.js""></script>
<script type=""text/javascript"" src=""../../js/extjs/ux/data/PagingMemoryProxy.js""></script>
<script type=""text/javascript"" src=""../../js/amq/stomp.js""></script>

<script type=""" + "text/javascript" + @""">
    var bNew = " + bNew + @";
    var isMaker = " + isMaker + @";
    var isDirectChecker = " + isDirectChecker + @";
    var isDirectApprover = " + isDirectApprover + @";

    var url = """ + jmsServer + @""";
    var login = """ + userJMS + @""";
    var passcode = """ + password + @""";
    var positionTopic = """ + positionTopic + @""";    
    var pingTopic = """ + pingTopic + @""";    
    var filt = """ + s + @""";
    var members = [            
        ];
        var contractIds = [
        ];
        var contractTypes = [
            {""abbr"":""O"",""name"":""Option""},
            {""abbr"":""F"",""name"":""Future""}
        ];

        var sids = [
        ];


</script>

<script type=""text/javascript"" src=""position.js""></script>

", false);
            }
        }

        private void Permission()
        {
            dtFormMenuPermission[] dtFMP;

            UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
            dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Position");
            if (dtFMP != null)
                for (int n = 0; n < dtFMP.Length; n++)
                {
                    if (dtFMP[n].editMaker)
                        isMaker = 1;
                    if (dtFMP[n].editDirectChecker)
                        isDirectChecker = 1;
                    if (dtFMP[n].editDirectApproval)
                        isDirectApprover = 1;
                }
        }
    }
}
