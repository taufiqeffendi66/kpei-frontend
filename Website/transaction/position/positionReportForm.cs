﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.position
{
    public partial class positionReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public positionReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "memberId");
            cl02.DataBindings.Add("Text", DataSource, "sid");
            cl03.DataBindings.Add("Text", DataSource, "role");
            cl04.DataBindings.Add("Text", DataSource, "series");
            cl05.DataBindings.Add("Text", DataSource, "contractType");
            cl06.DataBindings.Add("Text", DataSource, "netSell");
            cl07.DataBindings.Add("Text", DataSource, "netBuy");
            cl08.DataBindings.Add("Text", DataSource, "freq");
            cl09.DataBindings.Add("Text", DataSource, "vol");
        }
    }
}
