﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detail_offline_collateral.aspx.cs" Inherits="imq.kpei.transaction.detail_offline_collateral" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <div id="content">
        <div class="title">DETAIL OFFLINE COLLATERAL</div>
        <div>
            <table>
                <tr>
                    <td style="width:128px">Member ID</td>
                    <td><dx:ASPxComboBox ID="cmbMemberID" ClientInstanceName="clMemberID" runat="server"  Width="120px"/></td>                        
                </tr>                                
            </table>
         </div>
         <div>
            <table>
                <tr>
                    <td></td>
                    <td><dx:ASPxButton ID="btnGo" runat="server" Text="Go" > </dx:ASPxButton></td>
                    <td></td>                    
                </tr>
            </table>
       
            <dx:ASPxGridView ID="gridSchedule" runat="server" ClientInstanceName="grid" 
                 AutoGenerateColumns="False">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="Member ID" ></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Sid" ></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Role" ></dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Variation Margin" VisibleIndex="3">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Net<br /> Settlement" VisibleIndex="0"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="MTM" VisibleIndex="1"></dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="4" Caption="Initial Margin" ></dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
        </div>

    </div>


</asp:Content>
