﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using System.Text;
using Common.wsTransaction;
using Common.wsCommonList;
using Common.wsApproval;

namespace imq.kpei.transaction
{
    public partial class request : System.Web.UI.Page
    {
        private String targetApproval = @"../account/approval.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            
            Response.Clear();
            Response.ContentType = "text/plain";
            StringBuilder sb = new StringBuilder();
            sb.Append("[");

            try
            {
                string dataType = Request.QueryString["type"];
                if (string.IsNullOrEmpty(dataType))
                {
                    responseEmpty();
                    return;
                }
                string sessionId = Session["SESSION_USERMEMBER"].ToString();
                string memReq = Request.QueryString["memberId"];
                string memberId;
                if (sessionId.Equals("kpei"))
                {
                    if (memReq.Equals("null"))
                        memberId = "";
                    //else if (memReq.Equals("kpei"))
                    //    memberId = "";
                    else
                        memberId = memReq;
                }
                else
                    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];

                string contractId = Request.QueryString["contractId"];
                if (contractId != null && contractId.Equals("null"))
                    contractId = "";

                string sid = Request.QueryString["sid"];
                if (sid != null && sid.Equals("null"))
                    sid = "";

                string contractType = Request.QueryString["contractType"];
                if (contractType != null && contractType.Equals("null"))
                    contractType = "";

                string minId = Request.QueryString["minId"];
                string maxId = Request.QueryString["maxId"];
                string direction = Request.QueryString["direction"];

                UiDataService ws = ImqWS.GetTransactionService();// new UiDataService();
                //UiDataService ws = new UiDataService();
                //ws.Url = ImqSession.GetConfig("WS_TRANSACTION") + "/UiData";
                switch (dataType)
                {
                    case "position_limit" :
                        clientPositionLimitMessage[] positionLimit = ws.getPositionLimit(sessionId, memberId, sid, contractId);
                        for (int i = 0; i < positionLimit.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");
                            sb.Append("{\"position_limitData\": '{"
                                + "\"memberID\":\"" + positionLimit[i].memberId + "\","
                                + "\"contractType\":\"" + positionLimit[i].contractType + "\","
                                + "\"series\":\"" + positionLimit[i].series + "\","
                                + "\"role\":\"" + positionLimit[i].role + "\","
                                + "\"sID\":\"" + positionLimit[i].sid + "\","
                                + "\"netB\":\"" + positionLimit[i].netBuy + "\","
                                + "\"netS\":\"" + positionLimit[i].netSell + "\"}'}");

                        }
                        break;
                    case "trade":
                        tradeDto[] trades = ws.getTrade(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < trades.Length; i++)
                        //for (int i = 0; i < 70 ; i++)
                        {
                            if (i > 0)
                                sb.Append(",");
                            //String sDate = IntDateToStr(trades[i].tradeDate);
                            sb.Append("{\"tradeData\": '{"
                                + "\"tradeNumber\":" + trades[i].tradeNumber + ","
                                + "\"tradeDate\":\"" + trades[i].tradeDate + "\", "
                                + "\"tradeTime\":\"" + trades[i].tradeTime + "\", "
                                + "\"memberID\":\"" + trades[i].memberID + "\", "
                                + "\"tradingID\":\"" + trades[i].tradingID + "\", "
                                + "\"sID\":\"" + trades[i].sid + "\", "
                                + "\"role\":\"" + trades[i].role + "\", "
                                + "\"contractID\":\"" + trades[i].contractID + "\", "
                                + "\"contractType\":\"" + trades[i].contractType + "\", "
                                + "\"buysellPosition\":\"" + trades[i].buysellPosition + "\", "
                                + "\"price\":\"" + trades[i].price + "\", "
                                + "\"quantity\":\"" + trades[i].quantity + "\", "
                                + "\"premium\":\"" + trades[i].premium + "\", "
                                + "\"mtmLastPrice\":\"" + trades[i].mtmLastPrice + "\", "
                                + "\"fee\":\"" + trades[i].fee + "\" }'}");
                        }
                        break;

                    case "trade_summary":
                        tradeSummaryDto[] tradeSummarys = ws.getTradeSummary(memberId);
                        for (int i = 0; i < tradeSummarys.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");
                            sb.Append("{\"tradeSummaryData\": '{"
                                + "\"category\":\"" + tradeSummarys[i].category + "\", "
                                + "\"freq\":\"" + tradeSummarys[i].freq + "\", "
                                + "\"vol\":\"" + tradeSummarys[i].vol + "\", "
                                + "\"val\":\"" + tradeSummarys[i].val + "\"}'}");
                        }
                        break;

                    case "contract_summary":
                        seriesSummaryDto[] contractSummarys = ws.getContractSummary(memberId, contractId);
                        for (int i = 0; i < contractSummarys.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");
                            sb.Append("{\"contractSummaryData\": '{"
                                + "\"series\":\"" + contractSummarys[i].series + "\", "
                                + "\"freq\":\"" + contractSummarys[i].freq + "\", "
                                + "\"vol\":\"" + contractSummarys[i].vol + "\", "
                                + "\"val\":\"" + contractSummarys[i].val+ "\"}'}");
                        }
                        break;

                    case "clearing":
                        clearingDto[] clearings = ws.getClearing(sessionId, memberId, contractId, sid, contractType, "", "", "last");
                        for (int i = 0; i < clearings.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"clearingData\": '{"
                                + "\"memberID\":\"" + clearings[i].memberID + "\", "
                                + "\"sID\":\"" + clearings[i].sID + "\", "
                                + "\"clientID\":\"" + clearings[i].clientID + "\", "
                                + "\"role\":\"" + clearings[i].role + "\", "
                                + "\"contractType\":\"" + clearings[i].contractType + "\", "
                                + "\"series\":\"" + clearings[i].series + "\", "
                                + "\"prevBuy\":\"" + clearings[i].prevBuy + "\", "
                                + "\"prevSell\":\"" + clearings[i].prevSell + "\", "
                                + "\"openB\":\"" + clearings[i].openB + "\", "
                                + "\"openS\":\"" + clearings[i].openS + "\", "
                                + "\"closeB\":\"" + clearings[i].closeB + "\", "
                                + "\"closeS\":\"" + clearings[i].closeS + "\", "
                                + "\"liquidationB\":\"" + clearings[i].liquidationB + "\", "
                                + "\"liquidationS\":\"" + clearings[i].liquidationS + "\", "
                                + "\"exercise\":\"" + clearings[i].exercise + "\", "
                                + "\"assignment\":\"" + clearings[i].assignment + "\", "
                                + "\"netB\":\"" + clearings[i].netB + "\", "
                                + "\"netS\":\"" + clearings[i].netS + "\", "
                                + "\"premium\":\"" + clearings[i].premium + "\", "
                                + "\"gainLoss\":\"" + clearings[i].gainLoss + "\", "
                                + "\"penalty\":\"" + clearings[i].penalty + "\", "
                                + "\"compensation\":\"" + clearings[i].compensation + "\", "
                                + "\"settlement\":\"" + clearings[i].settlement + "\", "
                                + "\"fee\":\"" + clearings[i].fee + "\"}'}");
                        }
                        break;

                    case "price":
                        priceDto[] prices = ws.getPrice(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < prices.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"priceData\": '{"
                                + "\"series\":\"" + prices[i].series+ "\", "
                                + "\"contractType\":\"" + prices[i].contractType + "\", "
                                + "\"open_price\":\"" + prices[i].opening_price + "\", "
                                + "\"last_price\":\"" + prices[i].last_price + "\", "
                                + "\"open_interest\":\"" + prices[i].open_interest + "\", "
                                + "\"hph\":\"" + prices[i].hph + "\", "
                                + "\"hpf\":\"" + prices[i].hpf + "\", "
                                + "\"change\":\"" + prices[i].change + "\", "
                                + "\"lastDayTransaction\":\"" + prices[i].maturityDate + "\"}'}");
                        }
                        break;

                    case "assignment":
                        assignmentDto[] assignments = ws.getAssignment(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < assignments.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"assignmentData\": '{ "
                                + "\"assignmentNo\":\"" + assignments[i].assignmentNo + "\", "
                                + "\"exerciseNo\":\"" + assignments[i].exerciseNo + "\", "
                                + "\"buyMemberId\":\"" + assignments[i].buyMemberId + "\", "
                                + "\"exeRole\":\"" + assignments[i].exeRole + "\", "
                                + "\"exeSid\":\"" + assignments[i].exeSid + "\", "
                                + "\"exeVolume\":\"" + assignments[i].exeVolume + "\", "
                                + "\"dataTime\":\"" + assignments[i].dataTime + "\", "
                                + "\"sellMemberId\":\"" + assignments[i].sellMemberId + "\", "
                                + "\"assignmentRole\":\"" + assignments[i].assignmentRole + "\", "
                                + "\"assignmentSid\":\"" + assignments[i].assignmentSid + "\", "
                                + "\"series\":\"" + assignments[i].series + "\", "
                                + "\"assignmentVolume\":\"" + assignments[i].assignmentVolume + "\" }'}");
                        }
                        break;

                    case "exercise":
                        exerciseDto[] exercises = ws.getExercise(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < exercises.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"exerciseData\": '{ "
                                + "\"exerciseNo\":\"" + exercises[i].exerciseNo + "\", "
                                + "\"buyMemberId\":\"" + exercises[i].buyMemberId + "\", "
                                + "\"exerciseTime\":\"" + exercises[i].exerciseTime + "\", "
                                + "\"role\":\"" + exercises[i].role + "\", "
                                + "\"tradingId\":\"" + exercises[i].tradingId + "\", "
                                + "\"sId\":\"" + exercises[i].sid + "\", "
                                + "\"series\":\"" + exercises[i].series + "\", "
                                + "\"volume\":\"" + exercises[i].volume + "\", "
                                + "\"status\":\"" + exercises[i].status + "\", "
                                + "\"fee\":\"" + exercises[i].fee + "\" }'}");
                        }
                        break;

                    case "exerciseNew":
                        String iPermission = Request.QueryString["permission"];
                                           //member,    role,     trading,    sid, series,       volume, fee
                        int idxExercise = ws.storeDataExerciseTemp(sessionId, maxId, contractType, sid, contractId, minId, "0");
                        if (idxExercise > -1)
                        {
                            ApprovalService wsA = ImqWS.GetApprovalWebService();
                            dtApproval dtA = new dtApproval();
                            dtA.type = "c";
                            dtA.makerDate = DateTime.Now;
                            dtA.makerName = Session["SESSION_USER"].ToString();
                            dtA.makerStatus = "Ok";
                            dtA.idxTmp = idxExercise;
                            dtA.form = "~/transaction/exerciseConfirmation.aspx";
                            dtA.memberId = memberId;
                            dtA.status = "M";
                            dtA.insertEdit = "I";
                            dtA.topic = "Add Exercise";
                            exerciseDto aDT = ws.getExerciseTemp(idxExercise);

                            switch (iPermission)
                            {
                                case "1":
                                    wsA.AddMaker(dtA);
                                    AuditTrail(dtA.insertEdit, dtA.topic, " Maker ", aDT, targetApproval);
                                    break;

                                case "2":
                                    wsA.AddDirectChecker(dtA);
                                    AuditTrail(dtA.insertEdit, dtA.topic, " Direct Checker ", aDT, targetApproval);
                                    break;

                                case "3":
                                    ws.storeDataExercise(sessionId, maxId, contractType, sid, contractId, minId, "0");
                                    AuditTrail(dtA.insertEdit, dtA.topic, " Direct Approval ", aDT, targetApproval);
                                    wsA.AddDirectApproval(dtA);
                                    break;
                            }
                        }
                        sb.Append("{\"exerciseNew\": '{ \"result\":" + idxExercise + "}'}");
                        break;

                    case "exerciseValidate":
                        int validate = ws.validateExerciseVolume(sessionId, contractType, contractId, minId);
                        sb.Append("{\"exerciseValidate\": '{ \"validate\":" + validate + "}'}");
                        break;

                    case "position":
                        positionDto[] positions = ws.getPosition(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < positions.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"positionData\": '{ "
                                + "\"memberID\":\"" + positions[i].memberId+ "\", "
                                + "\"sID\":\"" + positions[i].sid+ "\", "
                                + "\"role\":\"" + positions[i].role+ "\", "
                                + "\"series\":\"" + positions[i].series+ "\", "
                                + "\"contractType\":\"" + positions[i].contractType+ "\", "
                                + "\"netB\":\"" + positions[i].netBuy + "\", "
                                + "\"netS\":\"" + positions[i].netSell + "\", "
                                + "\"exercise\":\"" + positions[i].exercise + "\", "
                                + "\"freq\":\"" + positions[i].freq + "\", "
                                + "\"tradingId\":\"" + positions[i].tradingId + "\", "
                                + "\"volume\":\"" + positions[i].vol + "\" }'}");
                        }
                        break;

                    case "blockedCollateral_client":
                        blockedCollateralDto[] blockedCollateral = ws.getBlockCollateral(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < blockedCollateral.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"blockedCollateralData\": '{"
                                + "\"memberID\":\"" + blockedCollateral[i].memberId+ "\", "
                                + "\"sID\":\"" + blockedCollateral[i].sid+ "\", "
                                + "\"role\":\"" + blockedCollateral[i].role+ "\", "
                                + "\"netSettlement\":\"" + blockedCollateral[i].netSettlement+ "\", "
                                + "\"mtm\":\"" + blockedCollateral[i].mtm+ "\", "
                                + "\"initialMargin\":\"" + blockedCollateral[i].initialMargin + "\" }'}");
                        }
                        break;

                    case "offlineCollateral_client":
                        offlineCollateralDto[] offlineCollateral = ws.getOfflineCollateral(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < offlineCollateral.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"offlineCollateralData\": '{"
                                + "\"memberID\":\"" + offlineCollateral[i].memberId+ "\", "
                                + "\"instrumentType\":\"" + offlineCollateral[i].instrumentType+ "\", "
                                + "\"accountNo\":\"" + offlineCollateral[i].accountNo+ "\", "
                                + "\"bank\":\"" + offlineCollateral[i].bankName+ "\", "
                                + "\"instrumentNo\":\"" + offlineCollateral[i].instrumentNo+ "\", "
                                + "\"maturityDate\":\"" + offlineCollateral[i].maturityDate + "\", "
                                + "\"currency\":\"" + offlineCollateral[i].currency+ "\", "
                                + "\"nominal\":\"" + offlineCollateral[i].nominalValue+ "\", "
                                + "\"hairCut\":\"" + offlineCollateral[i].hairCut+ "\", "
                                + "\"currencyRate\":\"" + offlineCollateral[i].exchangeRate+ "\", "
                                + "\"value\":\"" + offlineCollateral[i].collateralValue + "\" }'}");
                        }
                        break;

                    case "account_kpei":
                        kpeiAccountDto[] accountKpei = ws.getKpeiAccountData(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < accountKpei.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"accountData\": '{"
                                + "\"bankCode\":\"" + accountKpei[i].bankCode + "\", "
                                + "\"bankName\":\"" + accountKpei[i].bankName + "\", "
                                + "\"settlementAccNo\":\"" + accountKpei[i].settlementAccountNo + "\", "
                                + "\"settlementAccValue\":\"" + accountKpei[i].settlement + "\", "
                                + "\"penaltyAccNo\":\"" + accountKpei[i].penaltyAccountNo + "\", "
                                + "\"penaltyAccValue\":\"" + accountKpei[i].penalty + "\", "
                                + "\"guaranteeAccNo\":\"" + accountKpei[i].guaranteeFundAccountNo + "\", "
                                + "\"guaranteeAccValue\":\"" + accountKpei[i].guarantee + "\", "
                                + "\"feeAccNo\":\"" + accountKpei[i].feeAccountNo + "\", "
                                + "\"feeAccValue\":\"" + accountKpei[i].fee + "\" }'}");
                        }
                        break;

                    case "account_client":
                        clientAccountDto[] accountClient = ws.getClientAccountData(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < accountClient.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"accountData\": '{"
                                + "\"memberID\":\"" + accountClient[i].memberId + "\", "
                                + "\"sID\":\"" + accountClient[i].sid + "\", "
                                + "\"tradingID\":\"" + accountClient[i].tradingId + "\", "
                                + "\"initialMargin\":\"" + accountClient[i].initialMargin + "\", "
                                + "\"rIinitialMargin\":\"" + accountClient[i].rInitialMargin + "\", "
                                + "\"marginCall\":\"" + accountClient[i].marginCall + "\", " 
                                + "\"exposure\":\"" + accountClient[i].exposure + "\", "
                                + "\"procentage\":\"" + accountClient[i].procentage + "\", "
                                + "\"indicator\":\"" + accountClient[i].indicator + "\", "
                                + "\"collateralAccountNo\":\"" + accountClient[i].collateralAccountNo + "\", "
                                + "\"collateralBlock\":\"" + accountClient[i].blockCollateral + "\", "
                                + "\"collateralTotal\":\"" + accountClient[i].collateral + "\", "
                                + "\"collateralWithdrawRequest\":\"" + accountClient[i].withdrawRequest + "\", "
                                + "\"collateralFree\":\"" + accountClient[i].freeCollateral + "\" }'}");
                        }
                        break;

                    case "account_member":
                        accountDto[] accountMember = ws.getMemberAccountData(sessionId, memberId, contractId, sid, contractType, minId, maxId, direction);
                        for (int i = 0; i < accountMember.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"positionData\": '{"
                                + "\"memberID\":\"" + accountMember[i].memberId + "\", "
                                + "\"tradingId\":\"" + accountMember[i].tradingId + "\", "
                                + "\"accountId\":\"" + accountMember[i].accountId + "\", "
                                + "\"availableMargin\":\"" + accountMember[i].availableMargin + "\", "
                                + "\"bankCode\":\"" + accountMember[i].bankCode + "\", "
                                + "\"collateralBlock\":\"" + accountMember[i].blockCollateral + "\", "
                                + "\"collateralTotal\":\"" + accountMember[i].collateral + "\", "
                                + "\"collateralAccountNo\":\"" + accountMember[i].collateralAccountNo + "\", "
                                + "\"collateralFree\":\"" + accountMember[i].freeCollateral + "\", "
                                + "\"collateralOffline\":\"" + accountMember[i].offlineCollateral + "\", "
                                + "\"collateralWithdrawRequest\":\"" + accountMember[i].caWithdrawRequest+ "\", "
                                + "\"initialMargin\":\"" + accountMember[i].initialMargin + "\", "
                                + "\"rInitialMargin\":\"" + accountMember[i].rInitialMargin + "\", "
                                + "\"exposure\":\"" + accountMember[i].exposure + "\", "
                                + "\"procentage\":\"" + accountMember[i].procentage + "\", "
                                + "\"indicator\":\"" + accountMember[i].indicator + "\", "
                                + "\"marginCall\":\"" + accountMember[i].marginCall + "\", "
                                + "\"role\":\"" + accountMember[i].role + "\", "
                                + "\"securityDepositOnline\":\"" + accountMember[i].securityDeposit + "\", "
                                + "\"securityDepositAccountNo\":\"" + accountMember[i].securityDepositAccountNo + "\", "
                                + "\"securityDepositWithdrawRequest\":\"" + accountMember[i].sdWithdrawRequest + "\", "
                                + "\"settlement\":\"" + accountMember[i].settlement + "\", "
                                + "\"settlementAccountNo\":\"" + accountMember[i].settlementAccountNo + "\", "
                                + "\"sid\":\"" + accountMember[i].sid + "\", "
                                + "\"tradingLimit\":\"" + accountMember[i].tradingLimit + "\", "
                                + "\"mtm\":\"" + accountMember[i].mtm + "\", "
                                + "\"netSettlement\":\"" + accountMember[i].netSettlement + "\", "
                                + "\"freeAccount\":\"" + accountMember[i].freeAccount + "\", "
                                + "\"freeAccountNo\":\"" + accountMember[i].freeAccountNo + "\" }'}");
                        }
                        break;

                    case "sid":
                        UiCommonListService wsx = ImqWS.GetCommonListService();
                        commonListDto[] sidsDto;
                        sidsDto = wsx.listSid(sessionId, memberId);
                        if (sidsDto == null)
                            sidsDto = new commonListDto[0];

                        for (int i = 0; i < sidsDto.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"sidData\" : '{"
                                + "\"abbr\":\"" + sidsDto[i].id + "\", "
                                + "\"name\":\"" + sidsDto[i].name + "\"}'}");
                        }
                        break;

                    case "contractId":
                        UiCommonListService wsy = ImqWS.GetCommonListService();
                        commonListDto[] contractsDto;
                        contractsDto = wsy.listSeriesFromMemberSidPosition(sessionId, memberId, sid);
                        if (contractsDto == null)
                            contractsDto = new commonListDto[0];

                        for (int i = 0; i < contractsDto.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(",");

                            sb.Append("{\"contractIdData\" : '{"
                                + "\"abbr\":\"" + contractsDto[i].id + "\", "
                                + "\"name\":\"" + contractsDto[i].name + "\"}'}");
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

            }
            sb.Append("]");
            Response.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        private void responseEmpty()
        {
            Response.Clear();
            Response.ContentType = "text/plain";
            Response.Write("[]");
            Response.Flush();
            Response.End();
        }

        private string IntDateToStr(int iDate)
        {
            int tahun, bulan, tgl;
            int tmp;
            tahun = iDate / 10000;
            tmp = iDate % 10000;
            bulan = tmp / 100;
            tgl = tmp % 100;
            return string.Format("{0}-", tahun) + string.Format("{0}-", bulan) + string.Format("{0}", tgl);
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                alert('" + info + @"');
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        void AuditTrail(string insertEdit, string actifity, string aS, exerciseDto dT, String aTarget)
        {
            String aUserLogin = Session["SESSION_USER"].ToString();
            String moduleName = "Exercise";
            switch (insertEdit)
            {
                case "I":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " New " + moduleName, toString(dT));
                    ShowMessage(actifity + " New " + moduleName + " as " + aS, aTarget);
                    break;
                case "E":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Edit " + moduleName, toString(dT));
                    ShowMessage(actifity + " Edit " + moduleName + " as " + aS, aTarget);
                    break;
                case "R":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Remove " + moduleName, toString(dT));
                    ShowMessage(actifity + " Remove " + moduleName + " as " + aS, aTarget);
                    break;
            }
        }

        private String toString(exerciseDto aDT)
        {
            return "[No=" + String.Format("{0}", aDT.exerciseNo) + ", ContractId=" + aDT.series + ", SID=" + aDT.sid + ", time=" + String.Format("{0:HH:mm:ss}", aDT.exerciseTime) + ", Volume=" + aDT.volume.ToString() + ", BuyMemberId=" + aDT.buyMemberId + "]";
        }


    }
}
