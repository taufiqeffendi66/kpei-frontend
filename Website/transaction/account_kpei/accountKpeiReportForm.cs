﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.account_kpei
{
    public partial class accountKpeiReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public accountKpeiReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "bankCode");
            cl02.DataBindings.Add("Text", DataSource, "bankName");
            cl03.DataBindings.Add("Text", DataSource, "settlementAccountNo");
            cl04.DataBindings.Add("Text", DataSource, "settlement");
            cl05.DataBindings.Add("Text", DataSource, "penaltyAccountNo");
            cl06.DataBindings.Add("Text", DataSource, "penalty");
            cl07.DataBindings.Add("Text", DataSource, "guaranteeFundAccountNo");
            cl08.DataBindings.Add("Text", DataSource, "guarantee");
            cl09.DataBindings.Add("Text", DataSource, "feeAccountNo");
            cl10.DataBindings.Add("Text", DataSource, "fee");
        }
    }
}
