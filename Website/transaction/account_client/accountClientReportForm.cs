﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.account_client
{
    public partial class accountClientReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public accountClientReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "memberID");
            cl02.DataBindings.Add("Text", DataSource, "sid");
            cl03.DataBindings.Add("Text", DataSource, "tradingID");
            cl04.DataBindings.Add("Text", DataSource, "initialMargin");
            cl05.DataBindings.Add("Text", DataSource, "marginCall");
            cl06.DataBindings.Add("Text", DataSource, "exposure");
            cl07.DataBindings.Add("Text", DataSource, "procentage");
            cl08.DataBindings.Add("Text", DataSource, "collateralAccountNo");
            cl09.DataBindings.Add("Text", DataSource, "freeCollateral");
            cl10.DataBindings.Add("Text", DataSource, "collateral");
            cl11.DataBindings.Add("Text", DataSource, "withdrawRequest");
        }
    }
}
