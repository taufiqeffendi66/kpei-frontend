﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.account_member
{
    public partial class accountMemberReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public accountMemberReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "memberId");
            cl02.DataBindings.Add("Text", DataSource, "sid");
            cl03.DataBindings.Add("Text", DataSource, "tradingId");
            cl04.DataBindings.Add("Text", DataSource, "tradingLimit");
            cl05.DataBindings.Add("Text", DataSource, "availableMargin");
            cl06.DataBindings.Add("Text", DataSource, "marginCall");
            cl07.DataBindings.Add("Text", DataSource, "initialMargin");
            cl08.DataBindings.Add("Text", DataSource, "exposure");
            cl09.DataBindings.Add("Text", DataSource, "procentage");
            cl10.DataBindings.Add("Text", DataSource, "collateralAccountNo");
            cl11.DataBindings.Add("Text", DataSource, "freeCollateral");
            cl12.DataBindings.Add("Text", DataSource, "offlineCollateral");
            cl13.DataBindings.Add("Text", DataSource, "collateral");
            cl14.DataBindings.Add("Text", DataSource, "caWithdrawRequest");
            cl15.DataBindings.Add("Text", DataSource, "securityDepositAccountNo");
            cl16.DataBindings.Add("Text", DataSource, "securityDeposit");
            cl17.DataBindings.Add("Text", DataSource, "sdWithdrawRequest");
            cl18.DataBindings.Add("Text", DataSource, "settlementAccountNo");
            cl19.DataBindings.Add("Text", DataSource, "settlement");
            cl20.DataBindings.Add("Text", DataSource, "freeAccountNo");
            cl21.DataBindings.Add("Text", DataSource, "freeAccount");
        }
    }
}
