﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Common;
using Common.wsSysParam;
using System.Web;

namespace imq.kpei.transaction.account_member
{
    public partial class account_member : Page
    {
        protected static string getValueByParam(String param)
        {
            string result = "";
            SysParamService wsSP = ImqWS.GetSysParamService();
            try
            {
                dtSysParamMain[] tmpSysParam = wsSP.Search("", param, "");
                result = tmpSysParam[0].value;
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsSP.Abort();
                throw;
            }
            return result;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                ////redirect to login page if not logged in
                //ImqSession.Validate(this);
                ////redirect to blank page if accessed directly
                //ImqSession.ValidatedAction(this);           

                string jmsServer = HttpContext.Current.Request.Url.Host; //ImqSession.GetConfig("JMS_SERVER_WS");
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string accountTopic = ImqSession.GetConfig("JMS_TOPIC_ACCOUNT_MEMBER");
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string user = Session["SESSION_USER"].ToString();
                string password = Session.SessionID;
                string member = Session["SESSION_USERMEMBER"].ToString();
                string userJMS = String.Format("{0}-{1}", user, member);

                //dtSysParamMain[] tmpSysParam;
                string pl_lv_1 = getValueByParam("POSITION_LIMIT_LV_1");
                string pl_lv_2 = getValueByParam("POSITION_LIMIT_LV_2");
                string pl_lv_3 = getValueByParam("POSITION_LIMIT_LV_3");
                string pl_lv_4 = getValueByParam("POSITION_LIMIT_LV_4");
                string pl_lv_5 = getValueByParam("POSITION_LIMIT_LV_5");

                //UiCommonListService ws = ImqWS.GetCommonListService();

                //commonListDto[] membersDto;
                //try
                //{
                //    membersDto = ws.listMember(Session["SESSION_USERMEMBER"].ToString());
                //}
                //catch
                //{
                //    membersDto = new commonListDto[0];
                //}
                //StringBuilder membersSb = new StringBuilder();
                //for (int i = 0; i < membersDto.Length; i++)
                //{
                //    if (i > 0)
                //        membersSb.Append(",");
                //    membersSb.Append("{");
                //    membersSb.Append("\"abbr\":");
                //    membersSb.Append("\"" + membersDto[i].id + "\"");
                //    membersSb.Append(",\"name\":");
                //    membersSb.Append("\"" + membersDto[i].name + "\"");
                //    membersSb.Append("}");
                //}

                //commonListDto[] contractsDto;
                //try
                //{
                //    contractsDto = ws.listContract();
                //}
                //catch
                //{
                //    contractsDto = new commonListDto[0];
                //}
                //StringBuilder contractsSb = new StringBuilder();
                //for (int i = 0; i < contractsDto.Length; i++)
                //{
                //    if (i > 0)
                //        contractsSb.Append(",");
                //    contractsSb.Append("{");
                //    contractsSb.Append("\"abbr\":");
                //    contractsSb.Append("\"" + contractsDto[i].id + "\"");
                //    contractsSb.Append(",\"name\":");
                //    contractsSb.Append("\"" + contractsDto[i].name + "\"");
                //    contractsSb.Append("}");
                //}

                //string sessionId = Session["SESSION_USERMEMBER"].ToString();
                //string memberId;
                //if (sessionId.Equals("kpei"))
                //{

                //    memberId = "";

                //}
                //else
                //    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];
                //commonListDto[] sidDto;

                //try
                //{
                //    sidDto = ws.listSidMember(sessionId, memberId);
                //}
                //catch
                //{
                //    sidDto = new commonListDto[0];
                //}
                //StringBuilder sidSb = new StringBuilder();
                //for (int i = 0; i < sidDto.Length; i++)
                //{
                //    if (i > 0)
                //        sidSb.Append(",");
                //    sidSb.Append("{");
                //    sidSb.Append("\"abbr\":");
                //    sidSb.Append("\"" + sidDto[i].id + "\"");
                //    sidSb.Append(",\"name\":");
                //    sidSb.Append("\"" + sidDto[i].name + "\"");
                //    sidSb.Append("}");
                //}

                ////<script type=""text/javascript"" src=""../../js/extjs/ext-all-debug.js""></script>


                ScriptManager.RegisterStartupScript(this, GetType(), "initPage", String.Format(@"                    
                                                                                 <link rel=""stylesheet"" type=""text/css"" href=""../../js/extjs/resources/css/ext-all-gray.css"" />
                                                                                 <script type=""text/javascript"" src=""../../js/util/page.js""></script>
                                                                                 <script type=""text/javascript"" src=""../../js/extjs/ext-all.js""></script>
                                                                                 <script type=""text/javascript"" src=""../../js/extjs/ux/data/PagingMemoryProxy.js""></script>
                                                                                 <script type=""text/javascript"" src=""../../js/amq/stomp.js""></script>
                                                                                 
                                                                                 <script type=""text/javascript"">
                                                                                     var url = ""{0}"";
                                                                                     var login = ""{1}"";
                                                                                     var passcode = ""{2}"";
                                                                                     var accountTopic = ""{3}"";    
                                                                                     var pingTopic = ""{4}"";
                                                                                     var pl_lv_1 =""{5}"";    
                                                                                     var pl_lv_2 =""{6}"";    
                                                                                     var pl_lv_3 =""{7}"";    
                                                                                     var pl_lv_4 =""{8}"";    
                                                                                     var pl_lv_5 =""{9}"";    
                                                                                     var contractTypes = [
                                                                                             {{""abbr"":""O"",""name"":""Option""}},
                                                                                             {{""abbr"":""F"",""name"":""Future""}}
                                                                                         ];
                                                                                 
                                                                                  </script>
                                                                                 
                                                                                 <script type=""text/javascript"" src=""account_member.js""></script>
                                                                                 
                                                                                 ", jmsServer, userJMS, password, accountTopic, pingTopic, pl_lv_1, pl_lv_2, pl_lv_3, pl_lv_4, pl_lv_5), false);
            }
        }
    }
}
