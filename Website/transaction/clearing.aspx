﻿<%@ Page Title="Clearing Result List" Language="C#" AutoEventWireup="true" CodeBehind="clearing.aspx.cs" Inherits="imq.kpei.transaction.clearing" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">Clearing Result List</div>
        <table>
            <tr>
                <td style="width:128px">Member ID</td>
                <td><dx:ASPxComboBox ID="cmbMemberID" runat="server" ClientInstanceName="cmbmember">
                </dx:ASPxComboBox></td>                        
            </tr>    
            <tr>
                <td>Contract ID</td>
                <td><dx:ASPxComboBox ID="cmbContractID" runat="server" ClientInstanceName="cmbcontract">
                </dx:ASPxComboBox></td>
            </tr>    
            <tr>
                <td>Contract Type</td>
                <td><dx:ASPxTextBox ID="txtContractType" runat="server" ClientInstanceName="txtcontracttype" /></td>
            </tr>      
        </table>
        <br />
        <dx:ASPxButton ID="btnGO" runat="server" Text="GO" AutoPostBack="false" />
        <br />
        <dx:ASPxGridView ID="gvClearing" ClientInstanceName="grid" runat="server" AutoGenerateColumns="false"
            CssClass="grid" >
            <Columns>
                <dx:GridViewDataTextColumn Caption="Member ID" VisibleIndex="0" />
                <dx:GridViewDataTextColumn Caption="SID" VisibleIndex="1" />                     
                <dx:GridViewDataTextColumn Caption="Role<br />(C/H)" VisibleIndex="2" />
                <dx:GridViewDataTextColumn Caption="Contract<br />ID" VisibleIndex="3" />
                <dx:GridViewDataTextColumn Caption="Contract<br />Type" VisibleIndex="4" />
                <dx:GridViewBandColumn Caption="Previous" VisibleIndex="5">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Sell" VisibleIndex="0" />
                        <dx:GridViewDataTextColumn Caption="Buy" VisibleIndex="1" />
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="Current" VisibleIndex="6">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Open S" VisibleIndex="0" />
                        <dx:GridViewDataTextColumn Caption="Open B" VisibleIndex="1" />
                        <dx:GridViewDataTextColumn Caption="Close S" VisibleIndex="2" />
                        <dx:GridViewDataTextColumn Caption="Close B" VisibleIndex="3" />
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="Liquidate" VisibleIndex="7">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Sell" VisibleIndex="0" />
                        <dx:GridViewDataTextColumn Caption="Buy" VisibleIndex="1" />
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewDataTextColumn Caption="Exercise" VisibleIndex="8" />
                <dx:GridViewDataDateColumn Caption="Assignment" VisibleIndex="9" />
                <dx:GridViewBandColumn Caption="Net" VisibleIndex="10">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Sell" VisibleIndex="0" />
                        <dx:GridViewDataTextColumn Caption="Buy" VisibleIndex="1" />
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewDataDateColumn Caption="Premium" VisibleIndex="10" />
                <dx:GridViewDataDateColumn Caption="Gain/<br />Loss" VisibleIndex="11" />
                <dx:GridViewDataDateColumn Caption="Penalty" VisibleIndex="12" />
                <dx:GridViewDataDateColumn Caption="Compansation" VisibleIndex="13" />
                <dx:GridViewDataDateColumn Caption="Settlement" VisibleIndex="14" />
            </Columns>
            <SettingsBehavior AllowSelectSingleRowOnly="True" AllowFocusedRow="True" AllowSelectByRowClick="True" />
           
            <Styles>                        
                <Header CssClass="gridHeader">
                </Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
</asp:Content>
