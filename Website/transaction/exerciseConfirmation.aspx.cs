﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using System.Data;
using Common.wsApproval;
using Common.wsTransaction;
using Common.wsUserGroupPermission;

namespace imq.kpei.transaction
{
    public partial class exerciseConfirmation : System.Web.UI.Page
    {
        private int stat;
        private String aUserLogin;
        private String aUserMember;
        //private DataRow row;
        private String aMainLink = "~/home.aspx";
        private String moduleName = "Exercise";
        private String targetApproval = @"../administration/approval.aspx";
        //private exerciseDto aDt;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);

            aUserLogin = Session["SESSION_USER"].ToString();
            aUserMember = Session["SESSION_USERMEMBER"].ToString();

            //row = (DataRow)Session["row"];

            if (Request.QueryString["stat"] == null)
            {
                Response.Redirect(aMainLink);
                stat = -1;
            }
            else
                stat = int.Parse(Request.QueryString["stat"].ToString());

            if (!IsPostBack)
            {
                btnOk.Visible = false;
                btnChecker.Visible = false;
                btnApprovel.Visible = false;
                btnReject.Visible = false;
                switch (stat)
                {
                    case 0:
                        break;

                    case 1:
                        break;

                    case 2:
                        Permission(false);
                        ToEditor(int.Parse(Request.QueryString["idxTmp"].ToString()));
                        /*
                        if (!(Request.QueryString["makerName"].Equals(aUserLogin) || Request.QueryString["checkerName"].Equals(aUserLogin)))
                        {
                            btnOk.Visible = false;
                            btnChecker.Visible = false;
                            btnApprovel.Visible = false;
                            btnReject.Visible = false;
                        }
                        */
                        break;

                    case 3:
                        break;

                    case 4: //remove
                        break;
                }
            }
        }

        private void ShowMessage(string info, string targetPage)
        {
            string mystring = @"<script type='text/javascript'>
                                alert('" + info + @"');
                                window.location='" + targetPage + @"'
                                </script>";
            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
                ClientScript.RegisterStartupScript(this.GetType(), "clientScript", mystring);
        }

        void AuditTrail(string insertEdit, string actifity, string aS, exerciseDto dT, String aTarget)
        {
            switch (insertEdit)
            {
                case "I":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " New " + moduleName, toString(dT));
                    ShowMessage(actifity + " New " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " New "+moduleName, toString(dT)); break;
                case "E":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Edit " + moduleName, toString(dT));
                    ShowMessage(actifity + " Edit " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Edit Calendar", toString(dT)); break;
                case "R":
                    ImqWS.putAuditTrail(aUserLogin, moduleName, aS + " Remove " + moduleName, toString(dT));
                    ShowMessage(actifity + " Remove " + moduleName + " as " + aS, aTarget);
                    break;//putAuditTrail(actifity + " Remove Calendar", toString(dT)); break;
            }
        }

        private String toString(exerciseDto aDT)
        {
            return "[No=" + String.Format("{0}", aDT.exerciseNo) + ", ContractId=" + aDT.series + ", SID=" + aDT.sid + ", time=" + String.Format("{0:HH:mm:ss}", aDT.exerciseTime) + ", Volume=" + aDT.volume.ToString() + ", BuyMemberId=" + aDT.buyMemberId + "]";
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //addedit = int.Parse(Session["stat"].ToString());
            switch (stat)
            {
                case 0:
                case 1:
                case 4:
                    Response.Redirect(aMainLink);
                    break;
                case 2:
                case 3:
                    Response.Redirect("~/administration/approval.aspx");
                    break;
            }
        }

        protected void btnChecker_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            UiDataService ws = ImqWS.GetTransactionService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].checkerName = aUserLogin;
                dtA[0].checkerStatus = "Checked";
                dtA[0].approvelStatus = "To Be Approved";
                dtA[0].status = "C";
                wsA.UpdateByChecker(dtA[0]);

                exerciseDto aDt = ws.getExerciseTemp(dtA[0].idxTmp);

                AuditTrail(dtA[0].insertEdit, "Succes ", "Checker ", aDt, targetApproval);
            }
            catch (TimeoutException te)
            {
                te.ToString();
                ws.Abort();
                wsA.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }


        protected void btnApprovel_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            UiDataService ws = ImqWS.GetTransactionService();
            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                dtA[0].approvelName = aUserLogin;

                exerciseDto aDt = ws.getExerciseTemp(dtA[0].idxTmp );

                switch (dtA[0].insertEdit)
                {
                    case "I":
                        int availabel = ws.validateExerciseVolume(aDt.buyMemberId, aDt.tradingId, aDt.series, aDt.volume.ToString());
                        if (availabel >= 0)
                        {
                            int status = ws.storeDataExercise(aDt.buyMemberId, aDt.role, aDt.tradingId, aDt.sid, aDt.series, aDt.volume.ToString(), aDt.fee.ToString());
                            
                            if (status == -1)
                            {
                                dtA[0].approvelStatus = " Validate Time Error ";
                                dtA[0].status = "RS";
                                wsA.UpdateByApprovel(dtA[0]);
                                ImqWS.putAuditTrail(aUserLogin, moduleName, "New " + moduleName, toString(aDt));
                                ShowMessage("Validate Time Error, Reject New " + moduleName + " by System ", targetApproval);

                            }
                            else if (status == -2)
                            {
                                dtA[0].approvelStatus = "Database Error";
                                dtA[0].status = "RS";
                                wsA.UpdateByApprovel(dtA[0]);
                                ImqWS.putAuditTrail(aUserLogin, moduleName, "New " + moduleName, toString(aDt));
                                ShowMessage("Database Error, Reject New " + moduleName + " by System ", targetApproval);

                            }
                            else if (status >= 1)
                            {
                                dtA[0].approvelStatus = "Approved";
                                dtA[0].status = "A";
                                wsA.UpdateByApprovel(dtA[0]);
                                ImqWS.putAuditTrail(aUserLogin, moduleName, "New " + moduleName, toString(aDt));
                                ShowMessage("Succes New " + moduleName + " as Approval ", targetApproval);
                            }
                        }
                        else
                        {
                            dtA[0].approvelStatus = " Reject by System ";
                            dtA[0].status = "RS";
                            wsA.UpdateByApprovel(dtA[0]);
                            ImqWS.putAuditTrail(aUserLogin, moduleName, "New " + moduleName, toString(aDt));
                            ShowMessage("Reject New " + moduleName + " by System ", targetApproval);
                        }
                        break;

                    case "E":
                        //ws.Edit(dtA[0].idxTmp, dtA[0].idTable);
                        //ImqWS.putAuditTrail(aUserLogin, moduleName, "Approval Edit " + moduleName, toString(aDT));
                        //ShowMessage("Succes Edit " + moduleName + " as Approval ", targetApproval);
                        break;// putAuditTrail("Approval Edit Calendar", toString(aDT)); break;

                    case "R":
                        //    ws.RemoveById(dtA[0].idTable);
                        //    ImqWS.putAuditTrail(aUserLogin, moduleName, "Approval Remove " + moduleName, toString(aDT));
                        //    ShowMessage("Succes Remove " + moduleName + " as Approval ", targetApproval);
                        break;// putAuditTrail("Approval Remove Calendar", toString(aDT)); break;
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
        }

        protected void ToEditor(int idx)
        {
            try
            {
                UiDataService ws = ImqWS.GetTransactionService();
                exerciseDto aDt = ws.getExerciseTemp(idx);
                dtRole.Value = aDt.role.ToString();
                dtSID.Text = aDt.sid;
                dtContractID.Text = aDt.series;
                dtQuantity.Text = aDt.volume.ToString();
            }
            catch (TimeoutException te)
            {
                te.ToString();
                throw;
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ApprovalService wsA = ImqWS.GetApprovalWebService();
            UiDataService ws = ImqWS.GetTransactionService();

            //DataRow row = (DataRow)Session["row"];
            try
            {
                dtApproval[] dtA;
                dtA = wsA.Search(Request.QueryString["reffNo"].ToString());
                exerciseDto aDt = ws.getExerciseTemp(dtA[0].idxTmp);
                switch (dtA[0].status)
                {
                    case "M":
                        dtA[0].checkerName = aUserLogin;
                        dtA[0].checkerStatus = "Reject";
                        dtA[0].status = "RC";
                        wsA.UpdateByChecker(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Checker", aDt, targetApproval);
                        break;

                    case "C":
                        dtA[0].approvelName = aUserLogin;
                        dtA[0].approvelStatus = "Reject";
                        dtA[0].status = "RA";
                        wsA.UpdateByApprovel(dtA[0]);
                        AuditTrail(dtA[0].insertEdit, "Succes Reject ", "Approval", aDt, targetApproval);
                        break;
                }
            }
            catch (TimeoutException te)
            {
                te.ToString();
                wsA.Abort();
                throw;
            }
            //Response.Redirect("~/account/approval.aspx");
        }

        private void Permission(bool isEditor)
        {
            if (aUserMember.Equals("kpei"))
            {
                ASPxLabel2.Visible = false;
            }
            else
            {
                dtFormMenuPermission[] dtFMP;

                UserGroupPermissionService wsUGP = ImqWS.GetUserGroupPermissionService();
                dtFMP = wsUGP.getPermissionByUserAndMemberAndMenuName(aUserLogin, aUserMember, "Exercise");
                for (int n = 0; n < dtFMP.Length; n++)
                {
                    if (isEditor)
                    {
                    }
                    else
                    {
                        if (!(Request.QueryString["makerName"].Equals(aUserLogin) || Request.QueryString["checkerName"].Equals(aUserLogin)) && (Request.QueryString["memberId"].Equals(aUserMember)))
                        {
                            if ((Request.QueryString["status"].ToString() == "M") && (dtFMP[n].editChecker))
                            {
                                btnReject.Visible = true;
                                btnChecker.Visible = true;
                            }
                            else
                                if ((Request.QueryString["status"].ToString() == "C") && (dtFMP[n].editApproval))
                                {
                                    btnReject.Visible = true;
                                    btnApprovel.Visible = true;
                                }
                        }
                    }
                }
            }
        }
    }
}
