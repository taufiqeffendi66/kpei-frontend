﻿<%@ Page Title="Editor" Language="C#" AutoEventWireup="true" CodeBehind="spedit.aspx.cs" Inherits="imq.kpei.transaction.spedit" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div class="content">
        <div class="title">Daily/Final Settlement Price</div>
        <table align="center">
            <tr>
                <td style="width:128px">Contract ID</td>
                <td><dx:ASPxTextBox ID="txtID" runat="server"  ClientInstanceName="clID" Width="120px" ReadOnly="true" /></td>
                        
            </tr>
            <tr>
                <td style="width:128px">Point Num</td>
                <td><dx:ASPxTextBox ID="txtPoint" runat="server"  ClientInstanceName="clName" Width="120px" ReadOnly="true" /></td>                        
            </tr>  
            <tr>
                <td colspan="2">
                    <dx:ASPxGridView ID="gvConfirm" runat="server" AutoGenerateColumns="false" CssClass="grid">
                        <Columns>               
                            <dx:GridViewDataTextColumn Caption="Point" VisibleIndex="0" />                     
                            <dx:GridViewDataTextColumn Caption="Value" VisibleIndex="1" />                    
                        </Columns>
                        <Styles>
                            <Header CssClass="gridHeader" />
                        </Styles>
                    </dx:ASPxGridView>
                </td>
            </tr> 
            <tr>
                <td><dx:ASPxButton ID="btnContinue" runat="server" Text="Continue" AutoPostBack="false" /></td>
                <td><dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="false" /></td>
            </tr>
            
        </table>
        
    </div>
</asp:Content>
