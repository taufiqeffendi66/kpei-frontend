﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.wsTransaction;
using Common;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.position_limit
{
    public partial class position_limitReportView : System.Web.UI.Page
    {
        clientPositionLimitMessage[] positionLimits;

        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ReportViewer1.Report = CreateReport();
        }

        private void getData()
        {
            try
            {
                string dataType = Request.QueryString["type"];
                string sessionId = Session["SESSION_USERMEMBER"].ToString();
                string memReq = Request.QueryString["memberId"];
                string memberId;
                if (sessionId.Equals("kpei"))
                {
                    if (memReq.Equals("null"))
                        memberId = "";
                    else
                        memberId = memReq;
                }
                else
                    memberId = Session["SESSION_USERMEMBER"].ToString();// Request.QueryString["memberId"];

                string sid = Request.QueryString["sid"];
                if (sid != null && sid.Equals("null"))
                    sid = "";

                string contractId = Request.QueryString["contractId"];
                if (contractId != null && contractId.Equals("null"))
                    contractId = "";

                string minId = Request.QueryString["minId"];
                string maxId = Request.QueryString["maxId"];
                string direction = Request.QueryString["direction"];
                if (string.IsNullOrEmpty(dataType))
                {
                    responseEmpty();
                    return;
                }
                UiDataService ws = ImqWS.GetTransactionService();
                switch (dataType)
                {
                    case "position_limit":
                        positionLimits = ws.getPositionLimit(sessionId, memberId, sid, contractId);
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }

        XtraReport CreateReport()
        {
            if (IsPostBack)
            {
                getData();
            }
            // Create a report. 
            positionLimitReportForm report = new positionLimitReportForm();

            // Bind the report to the list. 
            report.DataSource = positionLimits;
            report.Name = ImqSession.getFormatFileSave("positionLimitReport");
            report.SetBoundLabel();
            return report;
        }

        private void responseEmpty()
        {
            Response.Clear();
            Response.ContentType = "text/plain";
            Response.Write("[]");
            Response.Flush();
            Response.End();
        }

        private string IntDateToStr(int iDate)
        {
            int tahun, bulan, tgl;
            int tmp;
            tahun = iDate / 10000;
            tmp = iDate % 10000;
            bulan = tmp / 100;
            tgl = tmp % 100;
            return string.Format("{0}-", tahun) + string.Format("{0}-", bulan) + string.Format("{0}", tgl);
        }
    }
}
