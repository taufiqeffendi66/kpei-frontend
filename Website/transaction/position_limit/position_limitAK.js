﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

var position_limitSubscriptionId = null;
var pingSubscriptionId = null;
var client = null;
var position_limitInitialData = [];
var cb;
var storeCb;
var recordIndex = -1;

function change(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '</span>';
    }
    return val;
}

function pctChange(val) {
    if (val > 0) {
        return '<span style="color:green;">' + val + '%</span>';
    } else if (val < 0) {
        return '<span style="color:red;">' + val + '%</span>';
    }
    return val;
}

function addposition_limit(s, force) {
    if (position_limitInitialData.length == 0)
        initialLoad = true;
    else
        initialLoad = false;

    data = eval('(' + s + ')');
    if ((typeof data.series === "undefined") || (data.series == null)) {
        return;
    }
    //is data exists


    //memberCombo
    cb = Ext.getCmp('memberCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.memberID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.memberID, name: data.memberID });

    //contractIdCombo
    cb = Ext.getCmp('contractIdCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.series);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.series, name: data.series });

    //sidCombo
    cb = Ext.getCmp('sidCombo');
    storeCb = cb.getStore();
    recordIndex = -1;
    if (storeCb.data.length >= 1) {
        recordIndex = storeCb.find('name', data.sID);
    }
    if (recordIndex == -1)
        storeCb.add({ abbr: data.sID, name: data.sID });

    recordIndex = -1;
    for (i = 0; i < position_limitInitialData.length; i++) {
        if (data.series == position_limitInitialData[i].series && data.memberID == position_limitInitialData[i].memberID && data.sID == position_limitInitialData[i].sID) {
            recordIndex = i;
            break;
        }
    }
    if (recordIndex == -1) {
        position_limitInitialData.push(data);
    } else {
        position_limitInitialData[recordIndex].memberID = data.memberID;
        position_limitInitialData[recordIndex].sID = data.sID;
        position_limitInitialData[recordIndex].role = data.role;
        position_limitInitialData[recordIndex].series = data.series;
        position_limitInitialData[recordIndex].contractType = data.contractType;
        position_limitInitialData[recordIndex].netS = data.netS;
        position_limitInitialData[recordIndex].netB = data.netB;
    }

    Ext.getCmp('pagingToolbar').doRefresh();
}

function subscribeposition_limitTopic(direction) {
    if ((!(typeof position_limitSubscriptionId === "undefined")) && (position_limitSubscriptionId != null)) {
        client.unsubscribe(position_limitSubscriptionId);
        position_limitSubscriptionId = null;
    }

    for (; position_limitInitialData.length > 0; )
        position_limitInitialData.pop();

    Ext.getCmp('pagingToolbar').doRefresh();         

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var position_limitSubscritptionHeader = { 'selector': selector };
        position_limitSubscriptionId = client.subscribe(position_limitTopic, function (message) {
            addposition_limit(message.body, true);
        }, position_limitSubscritptionHeader);
    } else {
        position_limitSubscriptionId = client.subscribe(position_limitTopic, function (message) {
            addposition_limit(message.body, true);
        });
    }

    Ext.Ajax.request({
        url: '../request.aspx?type=position_limit&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=Last&minId=""&maxId=""',
        success: function(response, opts) {
            var obj = eval('(' + response.responseText + ')'); //Ext.decode(response.responseText);
            for (ia = 0; ia < obj.length; ia++) {
                addposition_limit(obj[ia].position_limitData, false);
            }
            //Ext.getCmp('clearingGrid').getStore().load();
            //Ext.getCmp('clearingGrid').getView().refresh();
        },
        failure: function(response, opts) {
        }
    });
}

function reportTopic(direction) {
    if ((!(typeof position_limitSubscriptionId === "undefined")) && (position_limitSubscriptionId != null)) {
        client.unsubscribe(position_limitSubscriptionId);
        position_limitSubscriptionId = null;
    }

    var selector = "";
    if (Ext.getCmp('memberCombo').getValue() != null) {
        selector = selector + "memberID='" + Ext.getCmp('memberCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractIdCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
        else
            selector = selector + " and series='" + Ext.getCmp('contractIdCombo').getValue() + "' ";
    }
    if (Ext.getCmp('sidCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
        else
            selector = selector + " and sID='" + Ext.getCmp('sidCombo').getValue() + "' ";
    }
    if (Ext.getCmp('contractTypeCombo').getValue() != null) {
        if (selector == "")
            selector = selector + "contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
        else
            selector = selector + " and contractType='" + Ext.getCmp('contractTypeCombo').getValue() + "' ";
    }

    if (selector != "") {
        var position_limitSubscritptionHeader = { 'selector': selector };
        position_limitSubscriptionId = client.subscribe(position_limitTopic, function (message) {
            addposition_limit(message.body, true);
        }, position_limitSubscritptionHeader);
    } else {
        position_limitSubscriptionId = client.subscribe(position_limitTopic, function (message) {
            addposition_limit(message.body, true);
        });
    }
    window.open('./position_limitReportView.aspx?type=position_limit&memberId=' + Ext.getCmp('memberCombo').getValue() + '&contractId=' + Ext.getCmp('contractIdCombo').getValue() + '&sid=' + Ext.getCmp('sidCombo').getValue() + '&contractType=' + Ext.getCmp('contractTypeCombo').getValue() + '&direction=' + direction);
}

Ext.onReady(function() {
    Ext.QuickTips.init();

    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

    var Position_limitModel = Ext.define('Position_limitModel', {
        extend: 'Ext.data.Model',
        fields: [
               { name: 'memberID' },
               { name: 'sID' },
               { name: 'role' },
               { name: 'series' },
               { name: 'contractType' },
               { name: 'netS' },
               { name: 'netB' }
            ]
    });

    var position_limitReader = new Ext.data.reader.Json({
        model: 'Position_limitModel'
    }, Position_limitModel);

    var position_limitStore = Ext.create('Ext.data.ArrayStore', {
        id: 'Position_limitStore',
        model: 'Position_limitModel',
        data: position_limitInitialData,
        proxy: new Ext.ux.data.PagingMemoryProxy(position_limitInitialData),
        autoLoad: true,
        autoSync: true,
        reader: position_limitReader
    });

    // create the Grid    flex: 1
    var position_limitGrid = Ext.create('Ext.grid.Panel', {
        store: position_limitStore,
        id: 'position_limitGrid',
        stateful: false,
        stateId: 'position_limitGrid',
        columns: [
                { text: 'Member<br>ID', dataIndex: 'memberID', width: 50, sortable: false, align: 'center', draggable: false },
                { text: 'SID', dataIndex: 'sID', width: 110, sortable: false, align: 'left', draggable: false },
                { text: 'Role<br/>(C/H)', dataIndex: 'role', width: 40, sortable: false, align: 'center', draggable: false },
                { text: 'Contract ID', dataIndex: 'series', width: 150, sortable: false, align: 'left', draggable: false },
                { text: 'Contract<br/>Type(O/F)', dataIndex: 'contractType', width: 65, sortable: false, align: 'center', draggable: false },
                { text: 'Net', columns: [
                    { text: 'Sell', dataIndex: 'netS', width: 50, sortable: true, align: 'right', draggable: false },
                    { text: 'Buy', dataIndex: 'netB', width: 50, sortable: false, align: 'right', draggable: false }
                ]
                }
        ],
        viewConfig: {
            stripeRows: true
            , forceFit: true
            , loadMask: false
        },
        autoWidth: true,
        autoHeight: true,
        // paging bar on the bottom

        bbar: Ext.create('Ext.PagingToolbar', {
            id: 'pagingToolbar',
            store: position_limitStore,
            pageSize: 20,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "No data to display"
        })

    });

    position_limitGrid.setLoading(false, false);
    position_limitStore.load({ params: { start: 0, limit: 20} });

    Ext.define('Member', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

//    var memberStore = Ext.create('Ext.data.Store', {
//        model: 'Member',
//        data: members
//    });

    var memberCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'memberCombo',
        displayField: 'abbr',
        width: 100,
        //labelWidth: 130,
        //store: memberStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        triggerAction: 'all',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true,
        listeners: { select:
            { fn: function(combo, value) {
                combo = Ext.getCmp('sidCombo');
                combo.clearValue();
                combo.getStore().removeAll();
                Ext.Ajax.request({
                    url: '../request.aspx?type=sid&memberId=' + Ext.getCmp('memberCombo').getValue(),
                    success: function (response, opts) {
                        var obj = eval('(' + response.responseText + ')');
                        for (i = 0; i < obj.length; i++) {
                            newData = eval('(' + obj[i].sidData + ')');
                            combo.getStore().add({ abbr: newData.abbr, name: newData.name });
                        }
                    },
                    failure: function (response, opts) {
                    }
                });
            }
            }
        }
    });

    memberCombo.on('select', function(box, record, index) {

    });

//    Ext.define('ContractId', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var contractIdStore = Ext.create('Ext.data.Store', {
//        model: 'ContractId',
//        data: contractIds
//    });

    var contractIdCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractIdCombo',
        displayField: 'abbr',
        width: 100,
        //store: contractIdStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

//    Ext.define('Sid', {
//        extend: 'Ext.data.Model',
//        fields: [
//            { type: 'string', name: 'abbr' },
//            { type: 'string', name: 'name' }
//        ]
//    });

//    var sidStore = Ext.create('Ext.data.Store', {
//        model: 'Sid',
//        data: sids
//    });

    var sidCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'sidCombo',
        displayField: 'abbr',
        width: 100,
        //store: sidStore,
        store: new Ext.create('Ext.data.Store', { model: 'Member' }),
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    Ext.define('ContractType', {
        extend: 'Ext.data.Model',
        fields: [
            { type: 'string', name: 'abbr' },
            { type: 'string', name: 'name' }
        ]
    });

    var contractTypeStore = Ext.create('Ext.data.Store', {
        model: 'ContractType',
        data: contractTypes
    });

    var contractTypeCombo = Ext.create('Ext.form.field.ComboBox', {
        id: 'contractTypeCombo',
        displayField: 'abbr',
        width: 50,
        store: contractTypeStore,
        queryMode: 'local',
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}">{abbr}</div>';
            }
        },
        typeAhead: true
    });

    var filterPanel = Ext.create('Ext.panel.Panel', {
        id: 'filterPanel',
        height: 40,
        frame: false,
        border: false,
        layout: {
            type: 'hbox',
            padding: '5 0 0 0',
            pack: 'start',
            align: 'middle'
        },
        defaults: { margins: '0 5 0 0' },
        items: [
                    { xtype: 'label', text: 'Member ID: ' },
                    memberCombo,
                    { xtype: 'label', text: 'Contract ID: ' },
                    contractIdCombo,
                    { xtype: 'label', text: 'SID: ' },
                    sidCombo,
                    { xtype: 'label', text: 'Contract Type: ' },
                    contractTypeCombo,
                    { xtype: 'label', text: '' },
                    { xtype: 'label', text: '' },
                    { xtype: 'button', text: ' Search ',
                        handler: function() {
                            subscribeposition_limitTopic("last");
                            Ext.getCmp('pagingToolbar').moveFirst();
                        }
                    },
                    { xtype: 'button', text: ' Export ',
                        handler: function () {
                            currentPage = 0;
                            reportTopic("last");
                        }
                    }
                ]
    });

    var contentPanel = Ext.create('Ext.panel.Panel', {
        id: 'contentPanel',
        bodyPadding: 5,
        title: 'Position Limit Result List',
        layout: 'border',
        items: [
            {
                region: 'center'
                , layout: 'fit'
                , frame: false
                , border: false
                , items: position_limitGrid
            }, {
                region: 'north'
                , height: 40
                , frame: false
                , border: false
                , items: filterPanel
            }
        ]
    });


    new Ext.Viewport({
        layout: 'fit',
        items: contentPanel
    });

    Ext.getCmp('filterPanel').body.setStyle('background', '#efefef');
    Ext.getCmp('contentPanel').body.setStyle('background', '#efefef');

    client = Stomp.client(url);
    client.debug = function(str) {
    };
    var onconnect = function(frame) {
        Ext.getCmp('position_limitGrid').getStore().loadData([], false); ;
        pingSubscriptionId = client.subscribe(pingTopic, function(message) {
        });

        subscribeposition_limitTopic("last");
    };
    client.connect(login, passcode, onconnect);


    window.onbeforeunload = function() {
        if ((!(typeof position_limitSubscriptionId === "undefined")) && (position_limitSubscriptionId != null)) {
            client.unsubscribe(position_limitSubscriptionId);
            position_limitSubscriptionId = null;
        }
        if ((!(typeof pingSubscriptionId === "undefined")) && (pingSubscriptionId != null)) {
            client.unsubscribe(pingSubscriptionId);
            pingSubscriptionId = null;
        }
        client.disconnect(function() {
        });
    }
});
