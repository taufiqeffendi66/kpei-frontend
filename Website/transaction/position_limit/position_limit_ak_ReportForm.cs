﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.position_limit
{
    public partial class positionLimitAKReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public positionLimitAKReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "sID");
            cl02.DataBindings.Add("Text", DataSource, "role");
            cl03.DataBindings.Add("Text", DataSource, "series");
            cl04.DataBindings.Add("Text", DataSource, "contractType");
            cl05.DataBindings.Add("Text", DataSource, "netSell");
            cl06.DataBindings.Add("Text", DataSource, "netBuy");
        }
    }
}
