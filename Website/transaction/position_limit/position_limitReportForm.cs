﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.position_limit
{
    public partial class positionLimitReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public positionLimitReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "memberID");
            cl02.DataBindings.Add("Text", DataSource, "sID");
            cl03.DataBindings.Add("Text", DataSource, "role");
            cl04.DataBindings.Add("Text", DataSource, "series");
            cl05.DataBindings.Add("Text", DataSource, "contractType");
            cl06.DataBindings.Add("Text", DataSource, "netSell");
            cl07.DataBindings.Add("Text", DataSource, "netBuy");
        }
    }
}
