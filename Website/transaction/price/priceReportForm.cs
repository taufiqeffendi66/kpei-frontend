﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace imq.kpei.transaction.price
{
    public partial class priceReportForm : DevExpress.XtraReports.UI.XtraReport
    {
        public priceReportForm()
        {
            InitializeComponent();
        }

        public void SetBoundLabel()
        {
            cl01.DataBindings.Add("Text", DataSource, "series");
            cl02.DataBindings.Add("Text", DataSource, "contractType");
            cl03.DataBindings.Add("Text", DataSource, "open_interest");
            cl04.DataBindings.Add("Text", DataSource, "last_price");
            cl05.DataBindings.Add("Text", DataSource, "change");
            cl06.DataBindings.Add("Text", DataSource, "maturityDate");
        }
    }
}
