﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Common;
using Common.wsCommonList;
using System.Text;
using System.Web;

namespace imq.kpei.transaction.trade_summary
{
    public partial class trade_summary : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImqSession.ValidateAction(this);
            if (!IsPostBack)
            {
                string jmsServer = HttpContext.Current.Request.Url.Host; 
                jmsServer = "wss://" + jmsServer + ":61614/stomp";
                string tradeSummaryTopic = ImqSession.GetConfig("JMS_TOPIC_TRADE_SUMMARY");
                string seriesSummaryTopic = ImqSession.GetConfig("JMS_TOPIC_SERIES_SUMMARY");
                string pingTopic = ImqSession.GetConfig("JMS_TOPIC_PING");
                string user = Session["SESSION_USER"].ToString();
                string password = Session.SessionID;
                string sessionId = Session["SESSION_USERMEMBER"].ToString();
                string userJMS = String.Format("{0}-{1}", user, sessionId);

                UiCommonListService ws = ImqWS.GetCommonListService();

                commonListDto[] contractsDto;
                try
                {
                    contractsDto = ws.listSeriesFromTrade(sessionId);
                }
                catch
                {
                    contractsDto = new commonListDto[0];
                }
                StringBuilder contractsSb = new StringBuilder();

                if (contractsDto != null )
                    for (int i = 0; i < contractsDto.Length; i++)
                    {
                        if (i > 0)
                            contractsSb.Append(",");
                        contractsSb.Append("{");
                        contractsSb.Append("\"abbr\":");
                        contractsSb.Append(String.Format("\"{0}\"", contractsDto[i].id));
                        contractsSb.Append(",\"name\":");
                        contractsSb.Append(String.Format("\"{0}\"", contractsDto[i].name));
                        contractsSb.Append("}");
                    }

                ScriptManager.RegisterStartupScript(this, GetType(), "initPage", String.Format(@"                    
                                                                                      <link rel=""stylesheet"" type=""text/css"" href=""../../js/extjs/resources/css/ext-all-gray.css"" />
                                                                                      <script type=""text/javascript"" src=""../../js/util/page.js""></script>
                                                                                      <script type=""text/javascript"" src=""../../js/extjs/ext-all.js""></script>
                                                                                      <script type=""text/javascript"" src=""../../js/amq/stomp.js""></script>
                                                                                      <script type=""text/javascript"" src=""../../js/extjs/ux/data/PagingMemoryProxy.js""></script>
                                                                                      <script type=""text/javascript"">
                                                                                          var u = ""{0}"";
                                                                                          var l = ""{1}"";
                                                                                          var p = ""{2}"";
                                                                                          var tt = ""{3}"";    
                                                                                          var pt = ""{4}"";    
                                                                                          var ci = [{5}];                                                                                     
                                                                                          var ss = ""{6}"";                                                                                     
                                                                                      </script>
                                                                                      
                                                                                      <script type=""text/javascript"" src=""trade_summary.js""></script>
                                                                                      
                                                                                      ", jmsServer, userJMS, password, tradeSummaryTopic, pingTopic, contractsSb, seriesSummaryTopic), false);
            }
        }
    }
}
