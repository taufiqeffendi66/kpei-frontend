﻿using System;


namespace BinomialTreeUI
{
    /// <summary>
    /// Represents a Cox-Ross-Rubenstein binomial tree option pricing calculator.  May be used for pricing European or American options
    /// </summary>
    public class BinomialTree
    {
        #region "Private Members"
        private double _stockPrice = 0.0;
        private double _strike = 0.0;
        private double _timeToExpiration = 0.0;
        private double _volatility = 0.0;
        private double _riskFreeRate = 0.0;

        private EPutCall _putCall = EPutCall.Call;
        private EnumStyle _style = EnumStyle.American;
        private int _steps = 0;
        private double _deltaT = 0;

        private double _dividendYield = 0;
        private bool _isFuture = false;

        private double _dividend1 = 0;
        private double _dividend1Date = 0;

        private double _dividend2 = 0;
        private double _dividend2Date = 0;

        private double _dividend3 = 0;
        private double _dividend3Date = 0;

        private double _dividend4 = 0;
        private double _dividend4Date = 0;

        #endregion

        double p;

        double u;
        double d;

        double a;
        double _discountFactor;

        TreeNodeFactory _myFactory = null;

        public BinomialTree(double stockPrice, double strike, double timeToExpiration, double volatility,
        double riskFreeRate, EPutCall eputCall, EnumStyle style, int steps, double divYield, bool isFuture,
        double dividend1, double dividend1Date, double dividend2, double dividend2Date,
        double dividend3, double dividend3Date, double dividend4, double dividend4Date)
        {
            _stockPrice = stockPrice;
            _strike = strike;
            _timeToExpiration = timeToExpiration;

            _volatility = volatility;
            _riskFreeRate = riskFreeRate;

            _putCall = eputCall;
            _style = style;
            _steps = steps;

            _dividendYield = divYield;
            _isFuture = isFuture;

            _dividend1 = dividend1;
            _dividend1Date = dividend1Date;
            _dividend2 = dividend2;
            _dividend2Date = dividend2Date;
            _dividend3 = dividend3;
            _dividend3Date = dividend3Date;
            _dividend4 = dividend4;
            _dividend4Date = dividend4Date;

            _deltaT = timeToExpiration / steps;

            ComputeVariables();
            CreateStockPriceTree();
            //ComputeOptionValues();
        }

        /// <summary>
        /// Create the Tree of Prices of Underlying
        /// </summary>
        /// <returns></returns>
        bool CreateStockPriceTree()
        {
            //if there are discrete dividends then create tree first using S*
            _stockPrice = GetDividendAdjustedUnderlying();
            //after that add present value of dividends to each node in another pass

            //_myFactory = TreeNodeFactory.GetTreeNodeFactory();
            _myFactory = new TreeNodeFactory();
            _myFactory.ResetNodes();
            //CREATE TREE OF PRICES OF UNDERLYING
            //loop once per step
            for (int jStep = 0; jStep <= _steps; jStep++)
            {
                //loop once per level
                for (int i = 0; i <= jStep; i++)
                {
                    _myFactory.createNode(jStep, i, u, d, _stockPrice, _deltaT);
                }
            }

            //now add PV of dividends to prices of underlying
            for (int jStep = 0; jStep <= _steps; jStep++)
            {
                for (int i = 0; i <= jStep; i++)
                {
                    _myFactory.getNode(jStep, i).AddDividendPresentValues(_deltaT, _riskFreeRate,
                    _dividend1, _dividend1Date, _dividend2, _dividend2Date,
                    _dividend3, _dividend3Date, _dividend4, _dividend4Date);
                }
            }

            return true;
        }

        public double ComputeOptionValues()
        {
            //for the last column set the option values to simple payoffs
            for (int ii = 0; ii <= _steps; ii++)
            {
                _myFactory.getNode(_steps, ii).SetSimple_optionValue(_strike, _putCall);
            }
            //for all other columns set payoff to calculated payoff 
            if (_steps > 0)
            {
                //Iterate backward through tree from second last column
                for (int jStep = _steps - 1; jStep >= 0; jStep--)
                {
                    //loop once per level
                    for (int i = 0; i <= jStep; i++)
                    {
                        _myFactory.getNode(jStep, i).SetCalculatedOptionValue(p,
                        _myFactory.getNode(jStep + 1, i + 1).OptionValue,
                        _myFactory.getNode(jStep + 1, i).OptionValue,
                        _style, _putCall, _strike, _discountFactor);
                    }
                }
            }
            return _myFactory.getNode(0, 0).OptionValue;
        }
        double GetDividendAdjustedUnderlying()
        {
            return _stockPrice - PresentValue(_dividend1, _dividend1Date)
            - PresentValue(_dividend2, _dividend2Date)
            - PresentValue(_dividend3, _dividend3Date)
            - PresentValue(_dividend4, _dividend4Date);
        }
        /// <summary>
        /// sets the value of u,d,a and p
        /// </summary>
        void ComputeVariables()
        {
            _discountFactor = Math.Exp(-1 * _riskFreeRate * _deltaT);

            u = OptionUp();
            d = OptionDown();
            a = Calculatea();
            p = (a - d) / (u - d);
        }

        #region PayOffs


        #endregion
        #region UtilityFunctions
        private double OptionUp()
        {
            return Math.Exp(_volatility * Math.Sqrt(_deltaT));
        }

        private double OptionDown()
        {
            return Math.Exp(-_volatility * Math.Sqrt(_deltaT));
        }
        private double Calculatea()
        {
            if (_isFuture)
                return 1.0;
            else
            {
                return Math.Exp((_riskFreeRate - _dividendYield) * _deltaT);
            }
        }
        /// <summary>
        /// Use this to calculate present value of dividends at nodes
        /// </summary>
        /// <param name="value">Dollar amount</param>
        private double PresentValue(Double value, double time)
        {
            return value * Math.Exp(-1 * _riskFreeRate * time);
        }
        #endregion
    }
}
