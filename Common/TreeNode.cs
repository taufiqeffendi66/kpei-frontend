﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace BinomialTreeUI
{
    public class TreeNode
    {
        const bool DEBUG = false;
        const int NODE_OPTIONVAL_NOTSET = -999;
        public int step;//j
        public int verticalRank;//i where i=0 is the lowest

        public double stockPrice = 0;
        private double _optionValue = NODE_OPTIONVAL_NOTSET;
        ////next 2 variables only needed for discrete dividend PV calculation
        //private double _deltaT;
        //private double _riskFreeRate;

        public double OptionValue
        {
            get
            {
                return _optionValue;
            }
        }
        public TreeNode(int stp, int vertRank)
        {
            step = stp;
            verticalRank = vertRank;
        }
        public bool AddDividendPresentValues(double deltaT, double riskfreeRate,
        double dividend1, double dividend1date,
        double dividend2, double dividend2date, double dividend3, double dividend3date,
        double dividend4, double dividend4date)
        {
            stockPrice += DividendPresentValue(dividend1, dividend1date, riskfreeRate, deltaT);
            stockPrice += DividendPresentValue(dividend2, dividend2date, riskfreeRate, deltaT);
            stockPrice += DividendPresentValue(dividend3, dividend3date, riskfreeRate, deltaT);
            stockPrice += DividendPresentValue(dividend4, dividend4date, riskfreeRate, deltaT);
            return true;
        }
        public bool SetCalculatedOptionValue(double p, double optionValUpChild, double _optionValueDownChild,
        EnumStyle style, EPutCall putOrCall, double strike, double discountFactor)
        {
            if (style == EnumStyle.European)
            {
                _optionValue = (p * (optionValUpChild) + (1 - p) * _optionValueDownChild) * discountFactor;
            }
            else//American option - return max of calculated or intrinsic value
            {
                double tmpOptionVal = (p * (optionValUpChild) + (1 - p) * _optionValueDownChild) * discountFactor;
                if (putOrCall == EPutCall.Call)
                {
                    _optionValue = Math.Max(SimpleCall(stockPrice, strike), tmpOptionVal);
                }
                else
                {
                    _optionValue = Math.Max(SimplePut(stockPrice, strike), tmpOptionVal);
                }
            }
            if (DEBUG)
                Debug.WriteLine(string.Format("{0},{1},{2},{3}", "Step-" + step,
                "Vertical-" + verticalRank, " stockprice=" + stockPrice, " optionValue = " + _optionValue));

            return true;
        }
        public bool SetSimple_optionValue(double strike, EPutCall putOrCall)
        {
            _optionValue = TreeNode.Simple_optionValue(stockPrice, strike, putOrCall);
            //        System.Diagnostics.Debug.WriteLine(string.Format("{0},{1},{2},{3}", "Step-" + step.ToString(),
            //"Vertical-" + verticalRank.ToString()," stockprice=" + stockPrice.ToString(), " optionValue = " + _optionValue.ToString()));

            return true;
        }
        private static double Simple_optionValue(double price, double strike, EPutCall putOrcall)
        {
            if (putOrcall == EPutCall.Call)
                return SimpleCall(price, strike);
            else
                return SimplePut(price, strike);
        }
        private static double SimpleCall(double price, double strike)
        {
            return Math.Max(0.0, price - strike);
        }

        private static double SimplePut(double price, double strike)
        {
            return Math.Max(0.0, strike - price);
        }

        private double DividendPresentValue(double dividendAmount, double dividendDate, double rate, double deltaT)
        {
            double pVDividend;
            double timePeriod = dividendDate - step * deltaT;
            if (timePeriod > 0)
                pVDividend = PresentValue(dividendAmount, timePeriod, rate);
            else
                pVDividend = 0;
            return pVDividend;
        }
        private static double PresentValue(Double value, double time, double riskFreeRate)
        {
            return value * Math.Exp(-1 * riskFreeRate * time);
        }
    }
}
