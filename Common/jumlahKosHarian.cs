﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class jumlahKosHarian
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string tanggalData { get; set; }
        [DataMember]
        public int bulan { get; set; }
        [DataMember]
        public int tahun { get; set; }
        [DataMember]
        public int akSetorCollateral { get; set; }
        [DataMember]
        public string sahamInduk { get; set; }
    }
}
