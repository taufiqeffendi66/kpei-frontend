﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class kontrakBerjangka
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string tanggalData { get; set; }
        [DataMember]
        public string kontrakId { get; set; }
        [DataMember]
        public int frekuensi { get; set; }
        [DataMember]
        public int kontrakAwalShort { get; set; }
        [DataMember]
        public int kontrakAwalLong { get; set; }
        [DataMember]
        public int kontrakBaruShort { get; set; }
        [DataMember]
        public int kontrakBaruLong { get; set; }
        [DataMember]
        public int tutupKontrakShort { get; set; }
        [DataMember]
        public int tutupKontrakLong { get; set; }
        [DataMember]
        public int likuidasiShort { get; set; }
        [DataMember]
        public int likuidasiLong { get; set; }
        [DataMember]
        public int netOpenShort { get; set; }
        [DataMember]
        public int netOpenLong { get; set; }

        public int netOpenShortManual {
            get { 
                int hasilShort = 0;
                hasilShort = (kontrakAwalShort + kontrakBaruShort) - (tutupKontrakShort + likuidasiShort);

                return hasilShort;
            }
        }

        public int netOpenLongManual
        {
            get
            {
                int hasilLong = 0;
                hasilLong = (kontrakAwalLong + kontrakBaruLong) - (tutupKontrakLong + likuidasiLong);

                return hasilLong;
            }
        }
    }
}
