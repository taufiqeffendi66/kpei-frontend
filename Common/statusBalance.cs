﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class statusBalance
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string kodeBroker { get; set; }
        [DataMember]
        public object namaBroker { get; set; }
        [DataMember]
        public object kodeBank { get; set; }
        [DataMember]
        public object namaBank { get; set; }
        [DataMember]
        public string nomerRekening { get; set; }
        [DataMember]
        public object tipeRekening { get; set; }
        [DataMember]
        public double balance { get; set; }
        [DataMember]
        public long dataTimestamp { get; set; }

        public DateTime strDataTimeStamp
        {
            get
            {
                long unixDate = dataTimestamp;
                DateTime start = new DateTime(1970, 1, 1, 0, 0 , 0, DateTimeKind.Local);
                DateTime date = start.AddMilliseconds(unixDate).ToLocalTime();

               // long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local).Ticks;

               // DateTime date = new DateTime(beginTicks + unixDate * 10000, DateTimeKind.Local);
                //date.ToString("yyyy-MM-dd");

                return date.AddHours(7);
            }
        }

        public string strDataTimeStamp2
        {
            get
            {
                String strNew;
                long unixDate2 = dataTimestamp;
                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime date2 = start.AddMilliseconds(unixDate2).ToLocalTime();
                //date.ToString("yyyy-MM-dd");
                strNew = date2.ToString("dd-MM-yyyy HH:mm:ss");

                return strNew;
            }
        }

        public string strBalance
        {
            get {
                string newBalance;
                string fmt1 = "#,##0";
                double posAmount = balance;

                newBalance = posAmount.ToString(fmt1);

                return newBalance;
            }
        }

        public string strKBroker
        {
            get {
                string newBroker = kodeBroker;
                newBroker = newBroker.Replace("001","");

                return newBroker;
            }
        }


        //public String strDate
        //{
        //    get
        //    {
        //        DateTime newDate = strDataTimeStamp;
        //        String dateString  = newDate.ToString("yyyy-MM-dd");

        //        return dateString;
        //    }
        //}
    }
}
