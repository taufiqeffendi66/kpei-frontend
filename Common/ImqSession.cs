﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Configuration;
using Common.wsSessionSkd;
using Common.wsSysParam;
using Common.wsUser;

namespace Common
{
    public class ImqSession
    {
        private static String BaseUrl(System.Web.UI.Page page)
        {
            String s = "";
            try
            {
                //HttpContext context = HttpContext.Current;
                //string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
                //s = baseUrl;// page.Session["APP_URL"].ToString();
                s = page.Session["APP_URL"].ToString();
                //s = GetConfig("URL");
            }
            catch
            {
                HttpContext context = HttpContext.Current;
                s = String.Format("{0}://{1}{2}/", context.Request.Url.Scheme, context.Request.Url.Authority, context.Request.ApplicationPath.TrimEnd('/'));
            }
            return s;
        }

        public static string GetConfig(string key)
        {
            return WebConfigurationManager.AppSettings.Get(key);
        }

        public static string getFormatFileSave(string fileName)
        {
            return String.Format("{0} - {1}", fileName, DateTime.Now.ToString(GetConfig("FileNameFormat")));
        }

        public static bool Validate(System.Web.UI.Page page)
        {
            try
            {
                if (page.Session["SESSION_ID"] == null)
                    return false;
                else
                {
                    SessionSkdService ws = ImqWS.GetSessionSkdService();
                    bool isValid = (bool)(fastJSON.JSON.Instance.Parse(ws.isValidSession(page.Session["SESSION_ID"].ToString(), Util.clientIP(page),page.Request.UserAgent)) as Dictionary<string, object>)["isValid"];
                    return isValid;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
          
        }
        public static bool checkKeyfile(string userName, 
            string member, 
            string keyFile)
        {
            bool result = false;
            try
            {
                UserService ws = ImqWS.GetUserService();
                //ws.Url = GetConfig("WS_SERVER") + "/wsSessionSkd/SessionSkd";
                result = ws.checkKeyfile(userName, member, keyFile);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public static bool Login(System.Web.UI.Page page, 
            string userName, 
            string member, 
            string passWord, 
            string keyFile)
        {
            bool result = false; // nilai awal
            int session_timeout = 0;
            try
            {
                
                 SysParamService wsSP = ImqWS.GetSysParamService();
                dtSysParamMain[] sysparams = wsSP.Search("", "SESSION_TIME_OUT", "");
                session_timeout = int.Parse(sysparams[0].value);
                page.Session["SESSION_TIMEOUT"] = session_timeout;
                wsSP.Abort();
                wsSP.Dispose();
                /*
                 * 
                 * Note: Login nilai Balik
                 * -1 : Error Login
                 * 0  : Ok
                 * 1  : Failed Login
                 * 2  : Already Login
                 * 
                 * 
                 */


                SessionSkdService ws = ImqWS.GetSessionSkdService();                

                int retu = ws.login(userName, member, passWord, keyFile, page.Session.SessionID, page.Request.UserHostAddress, page.Request.UserAgent, session_timeout);
                if (ws != null)
                    ws.Dispose();
                //Dictionary<string, object> response = fastJSON.JSON.Instance.Parse(ws.login(userName, member, passWord, keyFile)) as Dictionary<string, object>;

                //string sessionId = (fastJSON.JSON.Instance.Parse(ws.login(userName, member, passWord, keyFile)) as Dictionary<string, object>)["sessionId"].ToString();
                if (retu == 0 )
                {
                    page.Session.Timeout = session_timeout;

                    page.Session["SESSION_ID"] = page.Session.SessionID;
                    page.Session["SESSION_USER"] = userName;
                    page.Session["SESSION_USERMEMBER"] = member;
                    page.Session["SESSION_USERPASSWORD"] = passWord;
                    result = true; // user n member n pass punya
                }
                else
                {
                    page.Session["SESSION_ID"] = "";
                    //page.Session["SESSION_USER"] = "";
                    //page.Session["SESSION_USERMEMBER"] = "";
                    page.Session["SESSION_USERPASSWORD"] = "";
                    result = false;
                }
            }
            catch
            {
                result = false; // user or member error
            }
            return result;
        }
   
        public static void Logout(System.Web.UI.Page page, String sessionId)
        {
            SessionSkdService ws = null;
            try
            {
                //wsSessionSkd.SessionSkdService ws = new Common.wsSessionSkd.SessionSkdService();
                //ws.Url = GetConfig("WS_SERVER") + "/wsSessionSkd/SessionSkd";
                ws = ImqWS.GetSessionSkdService();
                ws.logout(sessionId);
                page.Session.Abandon();
            }
            catch (Exception)
            {
            }
            
            page.Response.Write(String.Format("<script>window.open('{0}login.aspx','_top')</script>", BaseUrl(page)));
        }

        public static void ValidateAction(System.Web.UI.Page page)
        {
            if (!Validate(page))
            {
                page.Response.Write(String.Format("<script>window.open('{0}login.aspx','_top')</script>", BaseUrl(page)));
                page.Response.End();
            }
            else
            {
                if (page.Session["SESSION_TIMEOUT"] != null)
                    page.Session.Timeout = int.Parse(page.Session["SESSION_TIMEOUT"].ToString());
                else
                {
                    page.Response.Write(String.Format("<script>window.open('{0}login.aspx','_top')</script>", BaseUrl(page)));
                    page.Response.End();
                }
            }            
        }

        public static string GetURLReport()
        {
            return WebConfigurationManager.AppSettings.Get("REPORT_SERVER");
        }

        public static string GetDIRReportBank()
        {
            return WebConfigurationManager.AppSettings.Get("REPORT_SERVER_BANK");
        }

        public static string GetDIRReportBroker()
        {
            return WebConfigurationManager.AppSettings.Get("REPORT_SERVER_BROKER");
        }
    }
}
