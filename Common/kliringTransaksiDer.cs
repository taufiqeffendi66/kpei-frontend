﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class kliringTransaksiDer
    {
        [DataMember]
        public string kategori { get; set; }
        [DataMember]
        public string contractType { get; set; }
        [DataMember]
        public double totalHarian { get; set; }
        [DataMember]
        public double totalBulanan { get; set; }
        [DataMember]
        public double totalTahun { get; set; }
    }
}
