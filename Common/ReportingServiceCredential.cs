﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Security.Principal;
using Microsoft.Reporting.WebForms;

namespace Common
{
    [Serializable]
    public sealed class ReportingServiceCredential : IReportServerCredentials
    {
        public WindowsIdentity ImpersonationUser
        {
            get
            {
                return null;
            }
        }

        public ICredentials NetworkCredentials
        {
            get
            {
                // User name
                string userName = ImqSession.GetConfig("REPORT_USER");

                if (string.IsNullOrEmpty(userName))
                    throw new Exception("Missing user name from web.config file");

                // Password
                string password = ImqSession.GetConfig("REPORT_PASSWORD");

                if (string.IsNullOrEmpty(password))
                    throw new Exception("Missing password from web.config file");

                // Domain
                string domain = ImqSession.GetConfig("REPORT_DOMAIN");

                if (string.IsNullOrEmpty(domain))
                    throw new Exception("Missing domain from web.config file");

                return new NetworkCredential(userName, password, domain);
            }
        }

        public bool GetFormsCredentials(out Cookie authCookie,
        out string userName, out string password,
        out string authority)
        {
            authCookie = null;
            userName = null;
            password = null;
            authority = null;

            // Not using form credentials
            return false;
        }
    }
}
