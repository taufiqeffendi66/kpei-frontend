﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class indikator
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public double thresholdValue { get; set; }
        [DataMember]
        public string memberId { get; set; }
        [DataMember]
        public string sid { get; set; }
        [DataMember]
        public double actualValue { get; set; }
    }
}
