﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class AccountSidAudittrail
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public long waktu { get; set; }
        [DataMember]
        public string sid { get; set; }
        [DataMember]
        public string cmCode { get; set; }
        [DataMember]
        public string accountId { get; set; }
        [DataMember]
        public string currentId { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string action { get; set; }
        [DataMember]
        public string log_timestamp { get; set; }

        public DateTime? strDataTimeAudit
        {
            get
            {
                
                    long unixDate = waktu;
                    DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
                    DateTime date = start.AddMilliseconds(unixDate).ToLocalTime();

                    return date.AddHours(7);
                
            }
        }

        public string newStr
        {
            get {
                string str = strDataTimeAudit != null ? strDataTimeAudit.Value.ToString("dd-MM-yyyy HH:mm:ss") : "n/a";

                return str;
            }
        }

        public string newStr2
        {
            get
            {
                string str = strDataTimeAudit != null ? strDataTimeAudit.Value.ToString("yyyy-MM-dd") : "n/a";

                return str;
            }
        }

    }
}
