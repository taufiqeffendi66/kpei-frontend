﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace BinomialTreeUI
{
    /// <summary>
    /// This should be a singleton class
    /// </summary>
    public class TreeNodeFactory
    {
        const bool DEBUG = false;
        System.Collections.ArrayList allNodesList = null;
        public TreeNodeFactory()
        {
            allNodesList = new System.Collections.ArrayList();
        }
        ////enforce a singleton pattern - only one factory ensures only one list of nodes
        ////??we could probably also have just used a static member ArrayList
        //public static TreeNodeFactory GetTreeNodeFactory()
        //{
        //    if (_treeNodeFactory == null)
        //    {
        //        _treeNodeFactory = new TreeNodeFactory();
        //        _treeNodeFactory.allNodesList = new System.Collections.ArrayList();
        //    }

        //    return _treeNodeFactory;
        //}

        public TreeNode createNode(int step, int verticalRank, double u, double d, double Szero, double deltaT)
        {
            TreeNode node = new TreeNode(step, verticalRank) { stockPrice = Szero * Math.Pow(u, verticalRank) * Math.Pow(d, (step - verticalRank)) };
            allNodesList.Add(node);
            if (DEBUG)
                Debug.WriteLine(string.Format("{0},{1},{2}", "Step-" + step,
                "Vertical-" + verticalRank, node.stockPrice));

            return node;
        }
        //fetchNode at specified step
        public TreeNode getNode(int step, int verticalRank)
        {
            TreeNode retNode = null;
            TreeNode tmpNode = null;
            //fetch node from arraylist
            foreach (Object o in allNodesList)
            {
                tmpNode = (TreeNode)o;
                if (tmpNode.step == step && tmpNode.verticalRank == verticalRank)
                    retNode = tmpNode;
            }
            return retNode;
        }
        public bool ResetNodes()
        {
            allNodesList = null;
            allNodesList = new System.Collections.ArrayList();
            return true;
        }
        //deleteNode is probably never needed - let's make it just in case it is
        bool deleteNode(int step, int verticalRank)
        {
            TreeNode tmpNode = null;
            bool retVal = false;
            //fetch node from arraylist
            foreach (Object o in allNodesList)
            {
                tmpNode = (TreeNode)o;

                if (tmpNode.step == step && tmpNode.verticalRank == verticalRank)
                {
                    allNodesList.Remove(o);
                    retVal = true;
                }
            }
            return retVal;
        }
    }
}
