﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class infoTerima
    {
        [DataMember]
        public string kodeAk { get; set; }
        [DataMember]
        public string settlementDate { get; set; }
        [DataMember]
        public double obligation { get; set; }
        [DataMember]
        public double balance { get; set; }
        [DataMember]
        public double selisih { get; set; }

        public DateTime strDateTerima
        {
            get
            {
                string date = settlementDate;
                DateTime dt = Convert.ToDateTime(date);

                return dt;
            }
        }

        public string strDate
        {
            get
            {
                string newStr = strDateTerima.ToString("dd-MM-yyyy");

                return newStr;
            }
        }

        public string strBalance
        {
            get
            {
                string newBalance;
                string fmt1 = "#,##0";
                double posAmount = balance;

                newBalance = posAmount.ToString(fmt1);

                return newBalance;
            }
        }

        public string strObligation
        {
            get
            {
                string newObligation;
                string fmt1 = "#,##0";
                double posAmount = obligation;

                newObligation = posAmount.ToString(fmt1);

                return newObligation;
            }
        }

        public string strSelisih
        {
            get
            {
                string newSelisih;
                string fmt1 = "#,##0";
                double posAmount = selisih;

                newSelisih = posAmount.ToString(fmt1);

                return newSelisih;
            }
        }
    }
}
