﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class statusIntruksi
    {
        [DataMember]
        public string codeOfBroker { get; set; }
        [DataMember]
        public string instructionType { get; set; }
        [DataMember]
        public string settlementDate { get; set; }
        [DataMember]
        public string debitAccNumber { get; set; }
        [DataMember]
        public string creditAccNumber { get; set; }
        [DataMember]
        public string settlementStatus { get; set; }

        public DateTime strDateStatus
        {
            get
            {
                string date = settlementDate;
                DateTime dt = Convert.ToDateTime(date);

                return dt;
            }
        }

        public string strDate
        {
            get {
                string newStr = strDateStatus.ToString("dd-MM-yyyy");

                return newStr;
            }
        }
    }
}
