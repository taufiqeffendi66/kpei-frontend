﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Common;
using Common.balanceWithdrawalWS;
using Common.bankWS;
using Common.clientAccountWS;
using Common.contractWS;
using Common.contractGroupWS;
using Common.kpeiAccountWS;
using Common.liquidationWS;
using Common.inquiryTradeWS;
using Common.memberAccountWS;
using Common.memberInfoWS;
using Common.memberStatusWS;
using Common.underlyingWS;
using Common.wsApproval;
using Common.wsApprovalManagement;
using Common.wsAuditTrail;
using Common.wsCalendar;
using Common.wsSchedule;
using Common.wsSysParam;
using Common.wsUser;
using Common.wsUserGroup;
using Common.wsUserGroupPermission;
using Common.wsTransaction;
using Common.wsSessionSkd;
using Common.wsCommonList;
using Common.giveUpWS;
using Common.memberLiquidationWS;
using Common.wsManualSettlement;

namespace Common
{
    public class ImqWS
    {
        public const string SERVER_URL = "WS_SERVER";

        #region "Web Service URL"

        public static MemberLiquidationWSService GetMemberLiquidationService()
        {
            MemberLiquidationWSService ws = new MemberLiquidationWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-MemberLiquidation/MemberLiquidationWS", Timeout = -1 };
            return ws;
        }

        public static InquiryTradeWSService GetInquiryTradeService()
        {
            InquiryTradeWSService ws = new InquiryTradeWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-InquiryTrade/InquiryTradeWS", Timeout = -1 };
            return ws;
        }

        public static BalanceWithdrawalWSService GetBalanceWithdrawalService()
        {
            BalanceWithdrawalWSService ws = new BalanceWithdrawalWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-BalanceWithdrawal/BalanceWithdrawalWS", Timeout = -1 };
            return ws;
        }

        public static BankWSService GetBankWebService()
        {
            BankWSService ws = new BankWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-Bank/BankWS", Timeout = -1 };
            return ws;
        }

        public static ClientAccountWSService GetClientAccountService()
        {
            ClientAccountWSService ws = new ClientAccountWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-ClientAccount/ClientAccountWS", Timeout = -1 };
            return ws;
        }

        public static ContractWSService GetContractService()
        {
            ContractWSService ws = new ContractWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-Contract/ContractWS", Timeout = -1 };
            return ws;
        }


        public static ContractGroupWSService GetContractGroupService()
        {
            ContractGroupWSService ws = new ContractGroupWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-ContractGroup/ContractGroupWS", Timeout = -1 };
            return ws;
        }

        public static KpeiAccountWSService GetKpeiAccountService()
        {
            KpeiAccountWSService ws = new KpeiAccountWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-KpeiAccount/KpeiAccountWS" };
            return ws;
        }

        public static LiquidationWSService GetLiquidationService()
        {
            LiquidationWSService ws = new LiquidationWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-Liquidation/LiquidationWS", Timeout = -1 };
            return ws;
        }

        public static MemberAccountWSService getMemberAccountService()
        {
            MemberAccountWSService ws = new MemberAccountWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-MemberAccount/MemberAccountWS", Timeout = -1 };
            return ws;
        }

        public static MemberInfoWSService GetMemberInfoWebService()
        {
            MemberInfoWSService ws = new MemberInfoWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-MemberInfo/MemberInfoWS", Timeout = -1 };
            return ws;
        }

        public static MemberStatusWSService GetMemberStatusService()
        {
            MemberStatusWSService ws = new MemberStatusWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-MemberStatus/MemberStatusWS", Timeout = -1 };
            return ws;
        }

        public static UnderlyingWSService GetUnderlyingWebService()
        {
            UnderlyingWSService ws = new UnderlyingWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-Underlying/UnderlyingWS", Timeout = -1 };
            return ws;
        }

        public static ApprovalService GetApprovalWebService()
        {
            ApprovalService aps = new ApprovalService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsApproval/Approval" };
            return aps;
        }

        public static ApprovalManagementService GetApprovalManagementWebService()
        {
            ApprovalManagementService ws = new ApprovalManagementService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsApprovalManagement/ApprovalManagement" };
            return ws;
        }

        public static AuditTrailService GetAuditTrailService()
        {
            AuditTrailService ats = new AuditTrailService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsAuditTrail/AuditTrail" };
            return ats;
        }

        public static CalendarService GetCalendarService()
        {
            CalendarService ws = new CalendarService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsCalendar/Calendar" };
            return ws;
        }

        public static ScheduleService GetScheduleService()
        {
            ScheduleService ws = new ScheduleService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsSchedule/Schedule" };
            return ws;
        }

        public static SysParamService GetSysParamService()
        {
            SysParamService ws = new SysParamService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsSysParam/SysParam" };
            return ws;
        }

        public static UserGroupService GetUserGroupService()
        {
            UserGroupService ws = new UserGroupService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsUserGroup/UserGroup" };
            return ws;
        }

        public static UserService GetUserService()
        {
            UserService ws = new UserService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsUser/User" };
            return ws;
        }

        public static UserGroupPermissionService GetUserGroupPermissionService()
        {
            UserGroupPermissionService ugps = new UserGroupPermissionService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsUserGroupPermission/UserGroupPermission" };
            return ugps;
        }

        public static UiDataService GetTransactionService()
        {
            UiDataService ws = new UiDataService() { Url = ImqSession.GetConfig("WS_TRANSACTION") + "/UiData" /*ImqSession.GetConfig(SERVER_URL) + "/wsUserGroupPermission/UserGroupPermission";*/ };
            return ws;
        }

        public static GiveUpWSService GetGiveUpService()
        {
            GiveUpWSService gs = new GiveUpWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/WS-GuTu/GiveUpWS" };
            return gs;
        }
        public static UiCommonListService GetCommonListService()
        {
            UiCommonListService ws = new UiCommonListService() { Url = ImqSession.GetConfig("WS_UI") + "/UiCommonList" /*ImqSession.GetConfig(SERVER_URL) + "/wsUserGroupPermission/UserGroupPermission";*/ };
            return ws;
        }

        public static SessionSkdService GetSessionSkdService()
        {
            SessionSkdService ws = new SessionSkdService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsUser/SessionSkd",Timeout=-1 };
            return ws;
        }

        public static ManualSettlementWSService GetManualSettlementService()
        {
            ManualSettlementWSService ws = new ManualSettlementWSService() { Url = ImqSession.GetConfig(SERVER_URL) + "/wsManualSettlement/ManualSettlementWS" };
            return ws;
        }
        #endregion


        #region "Web Service Function"
        public static dtFormMenuPermission[] GetFormMenuPermission(String user, String member, String menu)
        {
            dtFormMenuPermission[] dtFMP = null;
            UserGroupPermissionService ugps = null;
            try
            {
                ugps = GetUserGroupPermissionService();
                dtFMP = ugps.getPermissionByUserAndMemberAndMenuName(user, member, menu);
            }
            catch (Exception e)
            {
                ugps.Abort();
                Debug.WriteLine(e.Message);
                throw;
            }
            finally
            {
                ugps.Dispose();
            }

            return dtFMP;
        }

        public static void putAuditTrail(String User, String Module, String Activity, String Memo)
        {
            wsAuditTrail.AuditTrailService ats = null;
            wsAuditTrail.dtAuditTrail data = null;
            try
            {
                data = new wsAuditTrail.dtAuditTrail();
                data.userSkd = User;
                data.module = Module;
                data.actifity = Activity;
                data.memo = Memo;

                ats = GetAuditTrailService();
                ats.AddData(data);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                ats.Abort();
                throw;
            }
            finally
            {
                ats.Dispose();
            }
        }

        public static DateTime HplusOne(DateTime dStart)
        {
            DateTime h1 = dStart;
            // H+1
            int x = 0;
            CalendarService wsC = ImqWS.GetCalendarService();
            x++;
            while (true)
            {
                h1 = h1.AddDays(1);
                if (h1.DayOfWeek == DayOfWeek.Saturday)
                    x += 1;
                else
                    if (h1.DayOfWeek == DayOfWeek.Sunday)
                        x += 1;
                    else
                        if (wsC.SearchByDate(h1.Date.ToString("MM/dd/yyyy")) != null)
                            x += 1;
                        else
                            break;
            }
            return h1;
        }

        public static DateTime HplusTri(DateTime dStart)
        {
            DateTime h3 = dStart;
            //DateTime h3 = deDate.Date;// DateTime.Now;
            int x = 0;
            CalendarService wsC = ImqWS.GetCalendarService();
            for (int y = 0; y < 3; y++)
            {
                x++;
                while (true)
                {
                    //h3 = DateTime.Now.AddDays(x);
                    h3 = h3.AddDays(1);
                    if (h3.DayOfWeek == DayOfWeek.Saturday)
                        x += 1;
                    else
                        if (h3.DayOfWeek == DayOfWeek.Sunday)
                            x += 1;
                        else
                            if (wsC.SearchByDate(h3.Date.ToString("MM/dd/yyyy")) != null)
                                x += 1;
                            else
                                break;
                }
            }
            return h3;
        }

        #endregion
    }
}
