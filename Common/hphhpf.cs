﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class hphhpf
    {
        [DataMember]
        public string series { get; set; }
        [DataMember]
        public double? sampling1 { get; set; }
        [DataMember]
        public double? sampling2 { get; set; }
        [DataMember]
        public double? sampling3 { get; set; }
        [DataMember]
        public double? sampling4 { get; set; }
        [DataMember]
        public double? sampling5 { get; set; }
        [DataMember]
        public double? sampling6 { get; set; }
        [DataMember]
        public double? sampling7 { get; set; }
        [DataMember]
        public double? sampling8 { get; set; }
        [DataMember]
        public double? hphHpf { get; set; }

        public string str1
        {
            get
            {
                string newstr1 = string.Format("{0:0.00}", sampling1);
                if (newstr1 == "")
                    newstr1 = "-";

                return newstr1;
            }
        }
        public string str2
        {
            get
            {
                string newstr2 = string.Format("{0:0.00}", sampling2);
                if (newstr2 == "")
                    newstr2 = "-";

                return newstr2;
            }
        }
        public string str3
        {
            get
            {
                string newstr3 = string.Format("{0:0.00}", sampling3);
                if (newstr3 == "")
                    newstr3 = "-";

                return newstr3;
            }
        }
        public string str4
        {
            get
            {
                string newstr4 = string.Format("{0:0.00}", sampling4);
                if (newstr4 == "")
                    newstr4 = "-";

                return newstr4;
            }
        }
        public string str5
        {
            get
            {
                string newstr5 = string.Format("{0:0.00}", sampling5);
                if (newstr5 == "")
                    newstr5 = "-";

                return newstr5;
            }
        }
        public string str6
        {
            get
            {
                string newstr6 = string.Format("{0:0.00}", sampling6);
                if (newstr6 == "")
                    newstr6 = "-";

                return newstr6;
            }
        }
        public string str7
        {
            get
            {
                string newstr7 = string.Format("{0:0.00}", sampling7);
                if (newstr7 == "")
                    newstr7 = "-";

                return newstr7;
            }
        }
        public string str8
        {
            get
            {
                string newstr8 = string.Format("{0:0.00}", sampling8);
                if (newstr8 == "")
                    newstr8 = "-";

                return newstr8;
            }
        }
        public string str9
        {
            get
            {
                string newstr9 = string.Format("{0:0.00}", hphHpf);
                if (newstr9 == "")
                    newstr9 = "-";

                return newstr9;
            }
        }
    }
}
