﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class Util
    {
        public static string v2sq(object value)
        {
            try
            {
                if (value == null || string.IsNullOrEmpty(value.ToString().Trim()) || value.ToString().Equals("-"))
                    return "null";
                else
                    return string.Format("'{0}'", value.ToString().Trim().Replace("'", "''"));
            }
            catch
            {
                return "null";
            }
        }

        public static string f2sq(object value)
        {
            try
            {
                string stringValue = value.ToString().Replace(',', '.');
                return Convert.ToDouble(stringValue).ToString();
            }
            catch
            {
                return "null";
            }
        }

        public static string i2sq(object value)
        {
            try
            {
                string stringValue = value.ToString();
                return Convert.ToInt32(stringValue).ToString();
            }
            catch
            {
                return "null";
            }
        }

        public static string d2sq(object value)
        {
            try
            {
                return string.Format("'{0}'", Convert.ToDateTime(value).ToString("yyyy-MM-dd"));
            }
            catch
            {
                return "null";
            }
        }

        public static string t2sq(object value)
        {
            try
            {
                return string.Format("'{0}'", Convert.ToDateTime(value).ToString("HH:mm:ss"));
            }
            catch
            {
                return "null";
            }
        }

        public static string dt2sq(object value)
        {
            try
            {
                return string.Format("'{0}'", Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch
            {
                return "null";
            }
        }

        public static int v2i(object value)
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch
            {
                return 0;
            }
        }

        public static double v2f(object value)
        {
            try
            {
                return Convert.ToDouble(value);
            }
            catch
            {
                return 0;
            }
        }

        public static DateTime v2d(object value)
        {
            try
            {
                return Convert.ToDateTime(value);
            }
            catch
            {
                return new DateTime(1900, 1, 1);
            }
        }

        public static string v2s(object value)
        {
            try
            {
                if (string.IsNullOrEmpty(value.ToString().Trim()))
                    return "";
                else
                    return value.ToString().Trim();
            }
            catch
            {
                return "";
            }
        }

        public static string v2s(object value, string defaultValue)
        {
            try
            {
                if (string.IsNullOrEmpty(value.ToString().Trim()))
                    return defaultValue.Trim();
                else
                    return value.ToString().Trim();
            }
            catch
            {
                return defaultValue.Trim();
            }
        }

        public static string clientIP(System.Web.UI.Page page)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(page.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
                sb.Append(page.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(',')[0]);
            else
                sb.Append(page.Request.ServerVariables["REMOTE_ADDR"]);

            return sb.ToString();
        }
    }
}
