﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class memberBroker
    {
        [DataMember]
        public string memberId { get; set; }
        [DataMember]
        public string memberName { get; set; }
    }
}
