﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class logLikuidasi
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string memberId { get; set; }
        [DataMember]
        public string sid { get; set; }
        [DataMember]
        public double freeCollateral { get; set; }
        [DataMember]
        public double exposure { get; set; }
        [DataMember]
        public long dataTimestamp { get; set; }

        public DateTime strDataTimeLikuidasi
        {
            get
            {
                long unixDate = dataTimestamp;
                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
                DateTime date = start.AddMilliseconds(unixDate).ToLocalTime();
                date.ToString("yyyy-MM-dd");

                return date.AddHours(7);
            }
        }
    }
}
